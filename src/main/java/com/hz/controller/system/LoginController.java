package com.hz.controller.system;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.socket.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.Dept;
import com.hz.entity.system.Menu;
import com.hz.entity.system.Notification;
import com.hz.entity.system.TaskMenu;
import com.hz.entity.system.User;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.service.system.MenuService;
import com.hz.service.system.NotificationService;
import com.hz.service.system.UserService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.FuneralRecordService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;
import com.hz.socket.SystemWebSocketHandler;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.Endecrypt;
import com.hz.util.UuidUtil;

/**
 * 用户登陆
 * @author ljx
 *
 */
@Controller
@RequestMapping("/login.do")
public class LoginController extends BaseController{
	//火化委托单
	@Autowired
	private CommissionOrderService commissionService;
	//车辆派送
	@Autowired
	private CarSchedulRecordService carService;
	//礼仪出殡
	@Autowired
	private FuneralRecordService funeralService;
	//冰柜调度
	@Autowired
	private FreezerRecordService freezerService;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	private UserService userService;
	@Autowired
	private MenuService menuService;
	@Autowired
	private DeptService deptService;
	@Autowired
	private CommissionOrderService cs;
	//公开公告
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private SystemWebSocketHandler socketHandler;
	
	private static Logger logger = LoggerFactory.getLogger(LoginController.class); 
	/**
	 * 验证cookie进行跳转
	 * @param request
	 * @return
	 */
    @RequestMapping
    public ModelAndView showlogin(HttpServletRequest request,HttpServletResponse response) {
		Map<String,Object> model=new HashMap<String,Object>();
		//获得cookie中的用户信息
		String token = getToken(request);
		//没有，跳转到登陆页面
		if (token==null||token.equals("")) {
			return new ModelAndView("login/login",model);
		}
		String userName = Endecrypt.decrypt(token);
		//获取用户
		User user = userService.getUserByUserName(userName);
		//信息错误，跳转到登陆页面
		if(user==null){
			delCookie(request, response);
	    	getSession().setAttribute(Const.SESSION_USER, null);
			return new ModelAndView("login/login",model);
		}
		//当前登陆用户存入session
    	getSession().setAttribute(Const.SESSION_USER, user);
    	
    	//登录时直接判  是否有验尸解冻的消息提醒
    
		return showHome(request);
    }
	/**
	 * 登陆时验证用户
	 * @param response
	 * @return
	 */
    @RequestMapping(params = "method=login")
    @ResponseBody
    public JSONObject login(HttpServletResponse response) {
    	JSONObject json = new JSONObject();
    	String userName = getRequest().getParameter("userCode");
    	User user = userService.getUserByUserName(userName);
    	if(user==null){
        	json.put("status", "300");
        	json.put("message", "登录名错误");
    	}else{
    		String ps = getRequest().getParameter("password");
    		if(!Endecrypt.decrypt(user.getPassword()).equals(ps)){
            	json.put("status", "400");
    			json.put("message", "密码错误");
    		}else{
    			addCookie(response, Endecrypt.encrypt(user.getUserName()));
    			getSession().setAttribute(Const.SESSION_USER, user);
            	json.put("status", "200");
    			json.put("message", "登陆成功");
//    			setSocketNumber(1);
    			logger.info(user.getUserName() + "登录成功++++++++");
    		}
    	}
		return json;
    }
    /**
	 * 进入主界面
	 * @return
	 */
    @RequestMapping(params = "method=main")
    public ModelAndView showHome(HttpServletRequest request) {
		Map<String,Object> model=new HashMap<String,Object>();
		java.sql.Date date = DateTools.getThisDate();
		User u = getCurrentUser();
		String userName="";
		if(u==null){
			return new ModelAndView("login/login",model);
		}else{
			userName=u.getUserName();
		}
		List<Menu> list = new ArrayList<Menu>();
		List<TaskMenu> listTask = new ArrayList<TaskMenu>();//任务权限列表
		if(u.getDeptId().equals(deptService.getDeptIdByName("管理部"))){
			list = menuService.getMenuList(null,"father_id asc,index_flag asc,id asc");//这个排序不能动
			listTask = menuService.getTaskMenuList();			
		}else{
			list = menuService.getMenuListByUserDept(u.getUserId(),"m.father_id asc,m.index_flag asc,m.id asc");//这个排序不能动
			listTask = menuService.getTaskMenuListByUserDept(u.getUserId());
		}
		setNewNumber(listTask);
		//公开通告n 
		Map<String,Object> paramMap=new HashMap<String, Object>();
		paramMap.put("limit",4);
		List<Notification> nList=notificationService.getNotificationList(paramMap);
		String deptid=u.getDeptId();
		if (deptid.equals(deptService.getDeptIdByName("一科")) || deptid.equals(deptService.getDeptIdByName("管理部"))) {
//			if (deptid.equals(deptService.getDeptIdByName("一科")) || u.getDeptName().contains("管理")) {
			
			int farewellNumber=0;
			farewellNumber = userNoticeService.getCount(u.getUserId(), Const.NoticeType_FarewellTask,date);
			int mourningNumber = userNoticeService.getCount(u.getUserId(), Const.NoticeType_MourningTask,date);
			int funeralNumber = userNoticeService.getCount(u.getUserId(), Const.NoticeType_FuneralTask,date);
			model.put("mourningNumber", mourningNumber);
			model.put("funeralNumber", funeralNumber);
			model.put("farewellNumber", farewellNumber);
		}
		if (u.getDeptId().equals(deptService.getDeptIdByName("二科")) || u.getDeptName().contains("管理部")) {
			int carNumber1 = userNoticeService.getCount(u.getUserId(), Const.NoticeType_CarTask,date);
			java.sql.Date nextDay = DateTools.getDayAfterDay(DateTools.utilDateToSqlDate(date), 1, Calendar.DATE);
			int carNumber2 = userNoticeService.getCount(u.getUserId(), Const.NoticeType_CarTask,nextDay);
			model.put("carNumber", carNumber1+carNumber2);
			int autoFreezerNumber = userNoticeService.getCount(u.getUserId(), Const.NoticeType_YsjdTask,date);
			model.put("autoFreezerNumber", autoFreezerNumber);
			int freezerThawNumber = userNoticeService.getCount(u.getUserId(), Const.NoticeType_FysytjdTask,date);
			model.put("freezerThawNumber", freezerThawNumber);
			
			getSession().setAttribute("autoFreezerNumber", autoFreezerNumber);
		}
//		String reportPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/"; 
		String reportPath = "http://192.168.6.5:8080/birt";
		getSession().setAttribute("reportPath", reportPath);
		model.put("u", u);
		model.put("nList", nList);
		model.put("url", "login.do");
		model.put("menuList", list);
		model.put("listTask", listTask);
		model.put("userName", userName);
		return new ModelAndView("home/home",model);
    }
    @RequestMapping(params = "method=content")
    public ModelAndView content() {
    	Map<String,Object> model=new HashMap<String,Object>();
    	User u = getCurrentUser();
    	List<TaskMenu> listTask = new ArrayList<TaskMenu>();//任务权限列表
    	if(u.getDeptId().equals(deptService.getDeptIdByName("管理部"))){
			listTask = menuService.getTaskMenuList();			
		}else{
			listTask = menuService.getTaskMenuListByUserDept(u.getUserId());
		}
		setNewNumber(listTask);
		//公开通告n 
		Map<String,Object> paramMap=new HashMap<String, Object>();
		paramMap.put("limit",4);
		List<Notification> nList=notificationService.getNotificationList(paramMap);
		model.put("u", u);
		model.put("nList", nList);
		model.put("listTask", listTask);
    	return new ModelAndView("../../common/content",model);
    }
    
    @RequestMapping(params = "method=refresh")
    @ResponseBody
    public JSONObject saveCommissionOrder(){
		JSONObject json = new JSONObject();
    	try {
    		User u = getCurrentUser();
    		if(u==null){
    			
    		}
//    		setSocketNumber(1);
    		if (u.getDeptId().equals(deptService.getDeptIdByName("二科")) || u.getDeptName().contains("管理部")) {	
    			String deptId = deptService.getDeptIdByName("二科");
    				//int autoFreezerNumber = userNoticeService.getCount(u.getUserId(), Const.NoticeType_YsjdTask);
    				socketHandler.sendMessageToUsers(deptId,new TextMessage("autoFreezerNumber"));
    				socketHandler.sendMessageToUsers(deptId,new TextMessage("freezerThawNumber"));		
    			}
    		
    		setJsonBySuccess(json, "刷新数据", false);
    	}
     catch (Exception e) {
		e.printStackTrace();
		setJsonByFail(json, "刷新数据成功，错误"+e.getMessage());
	}
	return json;
}
    	
    /**
     * 封装任务列表未读数量
     * @param listTask
     * @param model 
     * @param list 
     */
    public void setNewNumber(List<TaskMenu> listTask){
    	String time=DateTools.getTimeFormatString("yyyy-MM-dd",DateTools.getThisDateTimestamp());
		int newNumber=0;
    	for(int i=0;i<listTask.size();i++){	
			TaskMenu task=listTask.get(i);
			if("超期停尸提醒".equals(task.getName())){//超期停尸
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("cremationTime", time);
				map.put("flag", Const.IsFlag_Yse);//入柜状态
				map.put("viewsOut", Const.Zero);
				newNumber=freezerService.getNumber(map);
			}else if("业务未收费列表".equals(task.getName())){//业务未收费
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("checkFlag", Const.Check_Yes);
				map.put("payFlag", Const.Pay_No);
				map.put("creatTime",time );
				map.put("viewsUnpay",  Const.Zero);
				newNumber=commissionService.getNumber(map);
				
			}else if("委托单任务列表".equals(task.getName())){//委托单列表
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("checkFlag", Const.Check_No);
				map.put("creatTime", time);
				map.put("viewsUncheck",Const.Zero);
				newNumber=commissionService.getNumber(map);
				
			}else if("礼仪出殡任务列表".equals(task.getName())){//礼仪出殡
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("checkFlag", Const.Check_Yes);
				map.put("payFlag", Const.Pay_Yes);
				map.put("cremaTionTime", time);			
				map.put("viewsFuneral",Const.Zero);
				newNumber=funeralService.getNumber(map);
			}else if("告别厅任务列表".equals(task.getName())){//告别厅任务
				Map<String,Object> map=new HashMap<String, Object>();
				String tomorrow=DateTools.getTomorrowDate();
				map.put("tomorrow", tomorrow);
				map.put("farewellTime", time);
				map.put("checkFlag", Const.Check_Yes);
//				map.put("payFlag", Const.Pay_Yes);
				map.put("viewsFarewell",Const.Zero);
				newNumber=commissionService.getNumber(map);
			}else if("灵堂任务列表".equals(task.getName())){//灵堂任务
				Map<String,Object> map=new HashMap<String, Object>();
				String tomorrow=DateTools.getTomorrowDate();
				map.put("tomorrow", tomorrow);
				map.put("mourningTime", time);
				map.put("checkFlag", Const.Check_Yes);
//				map.put("payFlag", Const.Pay_Yes);
				map.put("viewsMourning",Const.Zero);
				newNumber=commissionService.getNumber(map);
			}else if("冰柜调度任务列表".equals(task.getName())){//冰柜调度
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("arriveTime", time);
				map.put("checkFlag", Const.Check_Yes);
//				map.put("payFlag", Const.Pay_Yes);
				map.put("flag", Const.IsFlag_Lock);//预定状态
				map.put("viewsDispatch",Const.Zero);
				newNumber=freezerService.getNumber(map);
			}else if("车辆任务列表".equals(task.getName())){//车辆任务
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("sentState", Const.CarSFlag_NO);
				map.put("pickStartTime", DateTools.getThisDate());
				map.put("pickEndTime", DateTools.getDayEndTime(DateTools.getDayAfterDay(DateTools.getThisDate(), 1, Calendar.DATE)));
				map.put("viewsCar",  Const.Zero);
				newNumber=carService.getNumber(map);
			}else if("普通炉任务列表".equals(task.getName())){//普通炉任务
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("cremationTime", time);
				map.put("checkFlag", Const.Check_Yes);
				map.put("payFlag", Const.Pay_Yes);
				map.put("furnaceFlag", Const.furnace_pt);
				map.put("viewsPT",Const.Zero);
				newNumber=commissionService.getNumberCremator(map);
			}else if("特约炉任务列表".equals(task.getName())){//特约炉任务
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("cremationTime", time);
				map.put("checkFlag", Const.Check_Yes);
				map.put("payFlag", Const.Pay_Yes);
				map.put("furnaceFlag", Const.furnace_ty);
				map.put("viewsTY",Const.Zero);
				newNumber=commissionService.getNumberCremator(map);					
			}
			task.setNewNumber(newNumber);
    	}
    }

    /**
	 * 连接到到用户修改添加修改密码页面
	 * @return
	 */
	@RequestMapping(params="method=edit")
	//刷新局部
	public ModelAndView editUser(){
		Map<String,Object> model=new HashMap<String, Object>();
		String userId=getString("userId");
		User user=new User();
		if(!userId.equals("")&&userId!=null){
			user=userService.getUserById(userId);
		}
		model.put("user", user);
		model.put("method", "savepwd");
		model.put("url", "login.do");
		return new ModelAndView("login/editPassword",model);
	}
	/**
	 * 连接到到用户修改添加修改信息
	 * @return
	 */
	@RequestMapping(params="method=editInfo")
	//刷新局部
	public ModelAndView editUsers(){
		Map<String,Object> model=new HashMap<String, Object>();
		User user = (User) getSession().getAttribute(Const.SESSION_USER);
		model.put("user", user);
		model.put("dname", user.getDeptName());
		model.put("method", "edituser");
		model.put("url", "user.do");
		model.put("method", "save");
		model.put("tp", getString("tp"));
		String option = Const.getSexOption(user.getSex(),false);
		model.put("sexOption", option);
		List<Dept> dList =  deptService.getDeptList(null);
		model.put("dList", dList);
		return new ModelAndView("login/editUser",model);
	}
	
	/**
	 * 连接到火化委托单
	 * @return
	 */
	@RequestMapping(params="method=check")
	//刷新局部
	public ModelAndView commssionOrder(){
		Map<String,Object> model=new HashMap<String, Object>();
//		Map<String,Object> map=new HashMap<String, Object>();
		String id=getString("id");
		CommissionOrder aa=new CommissionOrder();
		if(id!=null&&!id.equals("")){
			aa=cs.getCommissionOrderById(id);
		}
		model.put("aa", aa);
		model.put("method", "check");
		return new ModelAndView("content/content",model);
	}
	/**
	 * 登陆时验证用户修改用户信息
	 * @param response
	 * @return
	 */
	 @RequestMapping(params="method=edituser")
	 @SystemControllerLog(description = "登陆时验证用户修改用户信息")
	 @ResponseBody
	 public JSONObject editUserInfo(){
		 JSONObject json = new JSONObject();
		 try {
				User user=getCurrentUser();
				String userId=user.getUserId();
				user=userService.getUserById(userId);
				user.setCode(getString("code"));
				user.setUserName(getString("userName"));
				user.setName(getString("name"));
				user.setPhone(getString("phone"));
				user.setTel(getString("tel"));
				user.setSex(getByte("sex"));
				userService.updateUser(user);				
				setJsonBySuccess(json, "操作成功", true);
			} catch (Exception e) {
				e.printStackTrace();
				setJsonByFail(json, "操作失败，错误"+e.getMessage());
			}
		return json;
		
	 }
	/**
	 * 登陆时验证用户修改密码
	 * @param response
	 * @return
	 */
    @RequestMapping(params="method=savepwd")
    @SystemControllerLog(description = "验证用户修改密码")
    @ResponseBody
    public JSONObject editPassword(HttpServletRequest request) {
    	JSONObject json = new JSONObject();
    	//找到保存在session中的登录
    	User user = getCurrentUser();
    	String pwd=user.getPassword();
    	String password=getRequest().getParameter("password");//旧密码	
    	String pass1=getString("pass1");//新密码
    	String pass2=getString("pass2");//确认新密码
    	if(!Endecrypt.decrypt(pwd).equals(password)){
    			setJsonByFail(json, "与原密码不匹配");
    	}else{
	    	if(pass1.equals(pass2)&&!pass1.equals(password)){
	    		user.setPassword(Endecrypt.encrypt(pass1));
	    		userService.updateUser(user);
	    		setJsonBySuccess(json, "修改成功", true);
	    	}else{
	    	        setJsonByFail(json, "两次密码不一致或不能与旧密码相同");
	    		}	
    	}    		
    	
		return json;
    }
	/**
	 * 保存用户
	 * @return
	 */
	@RequestMapping(params="method=save")
	@SystemControllerLog(description = "保存用户登陆信息")
	@ResponseBody
	public JSONObject saveUser(HttpServletRequest request){
		JSONObject json=new JSONObject();
		try {
			User user=new User();
			String userId=getString("userId");
			if(userId!=null&&!userId.equals("")){
				user=userService.getUserById(userId);
			}
			user.setCode(getString("code"));
			user.setUserName(getString("userName"));
			user.setPassword(getString("password"));
			user.setName(getString("name"));
			user.setPhone(getString("phone"));
			user.setTel(getString("tel"));
			user.setSex(getByte("sex"));
			
			//新增
			if(userId!=null&&!userId.equals("")){
				userService.updateUser(user);
			//修改
			}else{
				user.setUserId(UuidUtil.get32UUID());
				user.setIsdel(Const.Isdel_No);
				user.setPassword(Endecrypt.encrypt("000000"));
				userService.addUser(user);
			}
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}

	/**
	 * 退出登陆
	 * @param request
	 * @return
	 */
    @RequestMapping(params = "method=loginOut")
    public ModelAndView loginOut(HttpServletRequest request,HttpServletResponse response) {
		Map<String,Object> model=new HashMap<String,Object>();
		//清除cookie
		delCookie(request, response);
		//清楚session
		getSession().setAttribute(Const.SESSION_USER, null);
		return new ModelAndView("login/login",model);
    }
	

}