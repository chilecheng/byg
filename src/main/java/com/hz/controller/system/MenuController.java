package com.hz.controller.system;

import java.util.HashMap;
import java.util.Map;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.Menu;
import com.hz.service.system.MenuService;
import com.hz.util.UuidUtil;

/**
 * 菜单管理
 * @author ljx
 *
 */
@Controller
@RequestMapping("/menu.do")
public class MenuController extends BaseController{
	@Autowired
	private MenuService menuService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "menu.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listMenu();
    }
	
	/**
	 * 获取菜单列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
    public ModelAndView listMenu() {
		Map<String,Object> model=new HashMap<String,Object>();
		int[] pages = getPage();
		//设置参数
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		PageInfo<Menu> page =menuService.getMenuPageInfo(maps,pages[0],pages[1],"father_id asc,index_flag asc");
        model.put("page", page);
		return new ModelAndView("system/menu/listMenu",model);
    }
	 
	/**
	 * 链接到菜单修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editMenu(){
		Map<String,Object> model=new HashMap<String,Object>();
		model = getModel(model);
		String id = getString("id");
		String fatherId=getString("fatherId");
		Menu menu=new Menu();
		Menu fatherMenu=new Menu();
		if(id!=null&&!id.equals("")){
			menu=menuService.getMenuById(id);
			if (menu.getMenuId()!=null&&!menu.getMenuId().equals("")) {
				fatherMenu=menuService.getMenuById(menu.getMenuId());
			}
		}
		if(fatherId!=null&&!fatherId.equals("")){
			fatherMenu=menuService.getMenuById(fatherId);
		}
        model.put("menu", menu);
        model.put("fatherMenu", fatherMenu);
        model.put("method", "save");
		return new ModelAndView("system/menu/editMenu",model);
    }

    /**
     * 保存菜单
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存菜单信息")
    @ResponseBody
    public JSONObject saveMenu(){
		JSONObject json = new JSONObject();
    	try {
    		Menu menu=new Menu();
    		String id =getString("id");
    		if (id!=null&&!id.equals("")) {
    			menu = menuService.getMenuById(id);
    		}
    		menu.setIndexFlag(getInt("indexFlag"));
    		menu.setMenuId(getString("menuId"));
    		menu.setName(getString("name"));
    		menu.setUrl(getString("url"));
    		menu.setIcon(getString("icon"));
    		menu.setPowerStr(getString("powerStr"));
    		if (id==null||id.equals("")) {
    			menu.setId(UuidUtil.get32UUID());
    			menuService.addMenu(menu);
    		}else {
    			menuService.updateMenu(menu);
    		}
    		setJsonBySuccess(json, "添加成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "添加失败，错误");
		}
		return json;
    }
    
    /**
     * 菜单删除
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "主菜单删除")
    @ResponseBody
    public JSONObject deleteDept(){
		JSONObject json = new JSONObject();
    	try {
//	    	String id = getRequest().getParameter("ids");
	    	String id = getString("ids");
		    menuService.deleteMenu(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误："+e.getMessage());
		}
		return json;
    }
    
}
