package com.hz.controller.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.Dept;
import com.hz.entity.system.Menu;
import com.hz.entity.system.TaskMenu;
import com.hz.service.system.DeptService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 部门管理
 * @author ljx
 *
 */
@Controller
@RequestMapping("/dept.do")
public class DeptController extends BaseController{
	@Autowired
	private DeptService deptService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "dept.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listDept();
    }

	/**
	 * 部门列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listDept(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		maps.put("id", getString("id"));
		PageInfo<Dept> page=deptService.getDeptPageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		String id = getString("depOption");
		String  depOption =  deptService.getDeptOption2(id,false);
		model.put("depOption", depOption);
	/*	model.put("depOption", deptService.getDeptOption2(deptId,false));*/
        model.put("page", page);
     /*   model.put("method", "list");*/
		return new ModelAndView("system/dept/listDept",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editDept(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		String fatherId=getString("fatherId");
		Dept dept=new Dept();
		Dept fatherDept=new Dept();
		if(id!=null&&!id.equals("")){
			dept=deptService.getDeptById(id);
			if (dept.getFatherId()!=null&&!dept.getFatherId().equals("")) {
				fatherDept=deptService.getDeptById(dept.getFatherId());
			}
		}
		if(fatherId!=null&&!fatherId.equals("")){
			fatherDept=deptService.getDeptById(fatherId);
		}
        model.put("dept", dept);
        model.put("fatherDept", fatherDept);
        model.put("method", "save");
		return new ModelAndView("system/dept/editDept",model);
    }

    /**
     * 保存部门
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存部门信息")
    @ResponseBody
    public JSONObject saveDept(){
		JSONObject json = new JSONObject();
    	try {
    		Dept dept=new Dept();
    		String id =getString("id");
    		if (id!=null&&!id.equals("")) {
    			dept=deptService.getDeptById(id);
    		}
    		dept.setIndexFlag(getInt("indexFlag"));
    		dept.setFatherId(getString("deptId"));
    		dept.setName(getString("name"));
    		if (id==null||id.equals("")) {
    			dept.setId(UuidUtil.get32UUID());
    			dept.setIsdel(Const.Isdel_No);
    			deptService.addDept(dept);
    		}else {
    			deptService.updateDept(dept);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除部门
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除部门信息")
    @ResponseBody
    public JSONObject deleteDept(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    deptService.deleteDept(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改部门信息是否有效")
    @ResponseBody
    public JSONObject isdelDept(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	deptService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
    
    
    /**
	 * 链接到部门菜单设置页面
	 * @return
	 */
    @RequestMapping(params = "method=listDeptMenu")
    public ModelAndView listDeptMenu(){
		Map<String,Object> model=new HashMap<String,Object>();
		model = getModel(model);
		String deptId = getString("deptId");
		Dept dept = deptService.getDeptById(deptId);
		model.put("dept", dept);
		List<Menu> list = deptService.getMenuList(deptId, "m.father_id asc,m.index_flag asc,m.id asc");
		StringBuilder content = new StringBuilder("");
		int index = 0;
		for(int i=0;i<list.size();i++){
			Menu menu = list.get(i);
			if(menu.getMenuId().equals("")){
				if(index!=0){
					content.append(",");
				}
				content=getTree(content, menu, list);
				index++;
			}
		}
		model.put("content", content.toString());
        model.put("method", "saveDeptMenu");
		return new ModelAndView("system/dept/editDeptMenu",model);
    }
    /**
	 * 链接到部门任务设置页面
	 * @return
	 */
    @RequestMapping(params = "method=listDeptTaskMenu")
    public ModelAndView listDeptTaskMenu(){
		Map<String,Object> model=new HashMap<String,Object>();
		model = getModel(model);
		String deptId = getString("deptId");
		Dept dept = deptService.getDeptById(deptId);
		model.put("dept", dept);
		List<TaskMenu> list = deptService.getTaskMenuList(deptId);
        model.put("method", "addDeptTaskMenu");
        model.put("list", list);
		return new ModelAndView("system/dept/editDeptTaskMenu",model);
    }
    /**
     * 获得tree结构
     * @param content
     * @param menu
     * @param list
     * @return
     */
	public StringBuilder getTree(StringBuilder content, Menu menu, List<Menu> list) {
		boolean isFirst = false;
		if (content.length() == 0) {
			isFirst = true;
		}
		content.append("{");
		content.append("text:'" + menu.getName() + "'");
		content.append(",tags:['" + menu.getId() + "']");
		content.append(",selectable: false");
		content.append(",state: {");
		if (menu.getDeptMenuNum() > 0) {
			content.append("checked: true,");
		}
		if (isFirst) {
			content.append("expanded: false}");
		} else {
			content.append("expanded: false}");
		}
		List<Menu> childs = new ArrayList<Menu>();
		for (int i = 0; i < list.size(); i++) {
			Menu c = list.get(i);
			if (c.getMenuId().equals(menu.getId())) {
				childs.add(c);
			}
		}
		if (childs.size() > 0) {
			content.append(",nodes: [");
			for (int j = 0; j < childs.size(); j++) {
				if (j != 0) {
					content.append(",");
				}
				Menu m = childs.get(j);
				content = getTree(content, m, list);
			}
			content.append("]");
		}
		content.append("}");
		return content;
	}
    /**
     * ajax保存部门菜单
     * @return
     */
    @RequestMapping(params = "method=addDeptMenu")
    @SystemControllerLog(description = "保存部门菜单")
    @ResponseBody
    public JSONObject addDeptMenu(){
		JSONObject json = new JSONObject();
    	try {
    		String menuId =getString("menuId");
    		String deptId =getString("deptId");
    		deptService.addDeptMenu(deptId, menuId);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    

    /**
     * ajax删除部门菜单
     * @return
     */
    @RequestMapping(params = "method=delDeptMenu")
    @SystemControllerLog(description = "删除部门菜单")
    @ResponseBody
    public JSONObject delDeptMenu(){
		JSONObject json = new JSONObject();
    	try {
    		String menuId =getString("menuId");
    		String deptId =getString("deptId");
    		deptService.delDeptMenu(deptId, menuId);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * ajax保存部门菜单
     * @return
     */
    @RequestMapping(params = "method=addDeptTaskMenu")
    @SystemControllerLog(description = "保存部门菜单")
    @ResponseBody
    public JSONObject addDeptTaskMenu(){
		JSONObject json = new JSONObject();
    	try {
//    		String menuId =getString("menuId");
    		String deptId =getString("deptId");
    		String[] ids = getArrayString("menuId");
    		deptService.deleteDeptTaskMenu(deptId);
    		deptService.addDeptTaskMenu(deptId,ids);
//    		deptService.addDeptMenu(deptId, menuId);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
}
