package com.hz.controller.system;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.TaskMenu;
import com.hz.service.system.TaskMenuService;
import com.hz.util.UuidUtil;

/**
 * 首页任务管理
 * @author cjb
 *
 */
@Controller
@RequestMapping("/task.do")
public class TaskController extends BaseController {

	@Autowired
	private TaskMenuService taskMenuService;

	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "task.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listTask();
	}

	/**
	 * 任务列表页
	 */
	@RequestMapping(params = "method=list")
	private ModelAndView listTask() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获取页码和每页数
		int[] pages = getPage();
		// 设置参数
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("name", getString("name"));
		PageInfo<TaskMenu> page = taskMenuService.getTaskMenuPageInfo(maps, pages[0], pages[1],"");
		model.put("page", page);
		return new ModelAndView("system/menu/listTaskMenu", model);
	}

	/**
	 * 链接到任务菜单修改添加页面
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView editMenu() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String id = getString("id");
		TaskMenu menu = new TaskMenu();
		if (id != null && !id.equals("")) {
			menu = taskMenuService.getTaskMenuById(id);
		}
		model.put("menu", menu);
		model.put("method", "save");
		return new ModelAndView("system/menu/editTaskMenu", model);
	}

	/**
	 * 保存菜单
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存首页任务菜单")
	@ResponseBody
	public JSONObject saveMenu() {
		JSONObject json = new JSONObject();
		try {
			TaskMenu menu = new TaskMenu();
			String id = getString("id");
			if (id != null && !id.equals("")) {
				menu = taskMenuService.getTaskMenuById(id);
			}
			menu.setName(getString("name"));
			menu.setUrl(getString("url"));
			if (id == null || id.equals("")) {
				menu.setId(UuidUtil.get32UUID());
				taskMenuService.addTaskMenu(menu);
			} else {
				taskMenuService.updateTaskMenu(menu);
			}
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败");
		}
		return json;
	}

	/**
	 * 菜单删除
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除首页菜单")
	@ResponseBody
	public JSONObject deleteDept() {
		JSONObject json = new JSONObject();
		try {
			String id = getString("ids");
			taskMenuService.deleteTaskMenu(id);
			setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败，错误：" + e.getMessage());
		}
		return json;
	}

}
