package com.hz.controller.system;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.system.Dept;
import com.hz.entity.system.Notification;
import com.hz.entity.system.User;
import com.hz.service.system.DeptService;
import com.hz.service.system.NotificationService;
import com.hz.service.system.UserService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 公开公告管理
 * @author rgy
 *
 */
@Controller
@RequestMapping("/notification.do")
public class NotificationController extends BaseController {
	/*@Autowired
	//Spring这里是通过实现ServletContextAware接口来注入ServletContext对象  
    private ServletContext servletContext; */ 
	@Autowired
	private NotificationService ns;
	
	@Autowired
	private DeptService deptService;
	
	@Autowired 
	private UserService us;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "notification.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listNotification();
		
	}
	/**
	 * 公开公告列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listNotification(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		//预设当前日期
		Date beginDate=getDate("beginDate");
		Date endDate=getDate("endDate");
		int []pages=getPage();
		if(beginDate == null){
//			beginDate=DateTools.getDayFirstTime(new Date());
		}else{
			beginDate=DateTools.getDayFirstTime(beginDate);
		}
		if(endDate == null){
//			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		}else{
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}
		map.put("beginDate",beginDate);
		map.put("endDate",endDate);
		PageInfo<Notification> page=ns.getNotificationPageInfo(map, pages[0], pages[1], "create_time desc");
		model.put("Type_Add", Const.Is_Yes);
		model.put("Type_Edit",Const.Is_No);
		model.put("page", page);
		model.put("beginDate",beginDate);
		model.put("endDate", endDate);
		model.put("method", "list");		
		return new ModelAndView("system/notification/listNotification",model);
	}
	/**
	 * 连接到页面添加修改公开公告
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editNotification(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		//获取当前登录人员及科室
		User user=getCurrentUser();
		String userName=user.getName();
		String deptName=user.getDeptName();
		Notification notification=new Notification();
		if(id!=null&&!id.equals("")){
			notification=ns.getNotificationById(id);
		}
		Date nowTime=DateTools.getDayFirstTime(new Date());
		model.put("type", getByte("type"));
		model.put("Type_Add", Const.Is_Yes);
		model.put("Type_Edit",Const.Is_No);
		model.put("nowTime", nowTime);
		model.put("userName", userName);
		model.put("deptName", deptName);
		model.put("deptId", user.getDeptId());
		model.put("userId", user.getUserId());
		model.put("method", "upload");
		model.put("notification", notification);
		return new ModelAndView("system/notification/editNotification",model);
	}
	
	/**
	 * 点击标题查看公开公告
	 * @return
	 */
	@RequestMapping(params ="method=listedit")
	public ModelAndView Notification(){
		Map<String,Object> model=new HashMap<String, Object>();
		String position =getString("position");
		//获取登录客户人员
		User user=(User) getSession().getAttribute(Const.SESSION_USER);
		//获取当前日期
		Date date=DateTools.getThisDate();
		
		String id=getString("id");
		int  number=ns.getNotificationNumber(id);
		number +=1;
		Notification notification=new Notification();
		if(id!=null&&!id.equals("")){
			notification=ns.getNotificationById(id);
		}
		notification.setNumber(number);
		ns.updateNotification(notification);
		model.put("date", date);
		model.put("user", user);
		model.put("position", position);
		model.put("method", "listedit");
		model.put("method", "addNumber");
		model.put("notification", notification);
		return new ModelAndView("system/notification/Notification",model);
	}
	/**
	 * 选择部门
	 * @return
	 */
	@RequestMapping(params ="method=choose")
	public ModelAndView NotificationChoose(){
		Map<String,Object> model=new HashMap<String, Object>();
		String groupType=getString("groupType");
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("groupType", groupType);
		List<Dept> dept=deptService.getDeptList(map);
		model.put("groupType", groupType);
    	model.put("dept",dept);
    	return new ModelAndView("../../common/worker2",model);
	}
	/**
	 * 选择发布人员
	 * @return
	 */
	@RequestMapping(params ="method=chooseFb")
	public ModelAndView NotificationChoose1(){
		Map<String,Object> model=new HashMap<String, Object>();
		String deptId=getString("deptId");
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("deptId", deptId);
		List<User> list=us.getUserList(map);
		model.put("deptId", deptId);
    	model.put("list",list);
    	return new ModelAndView("../../common/worker3",model);
	}
	
	/**
	 * 上传附件
	 * @param file
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存公开公告")
	@ResponseBody
	public JSONObject upload(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request, ModelMap model) {
		JSONObject json=new JSONObject();
		//上传文件的保存目录
		ServletContext context =getRequest().getSession().getServletContext();
		String savePath=context.getRealPath("");
		String fp = File.separator + "upload"+ File.separator + "file";
		int i = 0;
		while (i < 3) {
			int lastFitst = savePath.lastIndexOf(File.separator);
			savePath = savePath.substring(0, lastFitst);
			i++;
		}
		savePath += fp;
		String fileName = file.getOriginalFilename();
		System.out.println(savePath);
		System.out.println(fileName);
        File targetFile = new File(savePath, fileName);
		//判断上传文件的保存目录是否存在
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        //保存
        try {
        	if(fileName !=null && !"".equals(fileName)){
        		file.transferTo(targetFile);
        		model.addAttribute("fileUrl", request.getContextPath()+"/upload/"+fileName);
        	}
	        System.out.println("ok");
//	        System.out.println(request.getParameter("title"));
	        Notification notification=new Notification();
			String id=request.getParameter("id");
			if(id!=null&&!id.equals("")){
				notification=ns.getNotificationById(id);
			}
			//此处因为有文件上传,getString方法不可再使用了,否则数据为空,因为上传的方式不同!
			notification.setCreateUserId(request.getParameter("assiginUserId"));
			notification.setDeptId(request.getParameter("deptId"));
			notification.setTitle(request.getParameter("title"));
			notification.setContent(request.getParameter("content"));
			System.out.println(request.getParameter("createTime"));
			notification.setCreateTime(DateTools.stringToSqlDate(request.getParameter("createTime"), "yyyy-MM-dd"));
			
//			notification.setDate(request.getParameter("date"));//不懂这个字段是干嘛的,先放着
			System.out.println(savePath+File.separator+fileName);//测试一下地址
			//附件保存地址
			if(fileName !=null && !"".equals(fileName)){
				notification.setSaveAddress(savePath);
				notification.setFileName(fileName);				
			}
			ns.addUpdate(id,notification);
	        setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonBySuccess(json, "操作失败", true);
		}
		return json;
	}
	
	@RequestMapping(params = "method=downLoad")
	@ResponseBody
	public void downloadNet(String fileName,String saveAddress,HttpServletRequest request, HttpServletResponse response) throws MalformedURLException {
		//获取网站部署路径(通过ServletContext对象)，用于确定下载文件位置，从而实现下载  
//        String path = servletContext.getRealPath("/upload");  
        System.out.println(fileName);
//        //1.设置文件ContentType类型，这样设置，会自动判断下载文件类型  
//        response.setContentType("multipart/form-data");  
//        //2.设置文件头：最后一个参数是设置下载文件名(假如我们叫a.pdf)  
//        response.setHeader("Content-Disposition", "attachment;fileName="+"abcdefg.jpg");  
//        //通过文件路径获得File对象(假如此路径中有一个download.pdf文件)  
//        File file = new File(path + File.separator + fileName);
//        System.out.println(path + File.separator + fileName);
//        try {  
//            FileInputStream inputStream = new FileInputStream(file);  
//            //3.通过response获取ServletOutputStream对象(out)  
//            OutputStream os = response.getOutputStream();
//			//缓冲区
//			byte[] b = new byte[2048];
//			//读写文件
//			int length;
//			while ((length = inputStream.read(b)) > 0) {
//				System.out.println(length);
//				os.write(b, 0, length);
//			}
//			//关闭
//			os.close();
//			inputStream.close();
//			System.out.println("下载成功!");
//        } catch (IOException e) {  
//        	System.out.println("失败");
//            e.printStackTrace();  
//        }  
        ServletContext context =getRequest().getSession().getServletContext();
        //设置文件MIME类型 
        response.setContentType(context.getMimeType(fileName)); 
        //设置Content-Disposition 
        response.setHeader("Content-Disposition", "attachment;filename="+fileName); 
        //读取目标文件，通过response将目标文件写到客户端 
        //获取目标文件的绝对路径 
        String fullFileName = saveAddress + File.separator + fileName; 
        //System.out.println(fullFileName); 
        //读取文件 
        InputStream in;
		try {
			in = new FileInputStream(fullFileName);
			OutputStream out = response.getOutputStream();           
	        //写文件 
	        int b; 
	        while((b=in.read())!= -1) 
	        { 
	          out.write(b); 
	        } 	          
	        in.close(); 
	        out.close(); 
	        System.out.println("ok");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error");
		} 
      }
    
		 
	
	
	
	
	/**
	 * 保存公开公告
	 * @return
	 */
	@RequestMapping(params = "method=save324")
	@ResponseBody
	public JSONObject addNotification(){
		JSONObject json=new JSONObject();
		try {
			Notification notification=new Notification();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				notification=ns.getNotificationById(id);
			}
			notification.setCreateUserId(getString("assiginUserId"));
			notification.setDeptId(getString("deptId"));
			notification.setTitle(getString("title"));
			notification.setContent(getString("content"));
//			notification.setCreateTime(getDate("createTime"));
			notification.setDate(getDate("date"));
			System.out.println(getString("saveAddress"));//测试一下地址
			//附件保存地址
//			notification.setSaveAddress(getString("saveAddress"));
			ns.addUpdate(id,notification);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除公开公告
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除公开公告")
	@ResponseBody
	public JSONObject deleteNotification(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			ns.deleteNotification(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}


}
