package com.hz.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.Dept;
import com.hz.entity.system.Role;
import com.hz.entity.system.User;
import com.hz.service.system.DeptService;
import com.hz.service.system.RoleService;
import com.hz.service.system.UserService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.Endecrypt;
import com.hz.util.UuidUtil;

/**
 * 用户管理
 * @author rgy
 *
 */
@Controller
@RequestMapping("/user.do")
public class UserController extends BaseController{
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private DeptService deptService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url","user.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listUser();
		
	}
	
	/**
	 * 用户列表
	 * @author rgy 
	 */
	@RequestMapping(params="method=list")
	public ModelAndView listUser(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获取页码和每页数
		int []pages=getPage();
		//设置页码和每页数
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("userName",getString("userName"));
		map.put("isdel", getByte("isdel"));
		String dept=getString("dept");
		if(dept!=null &&!"".equals(dept)){
			map.put("deptId", dept);
		}
		String deptOption=deptService.getDeptOption2(dept,true);
		PageInfo<User> page=userService.getUserPageInfo(map, pages[0], pages[1], "code desc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("deptOption", deptOption);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page", page);
		model.put("method", "list");		
		return new ModelAndView("system/user/listUser",model);	
	}
	
	/**
	 * 连接到到用户修改添加页面
	 * @return
	 */
	@RequestMapping(params="method=edit")
	//刷新局部
	public ModelAndView editUser(){
		Map<String,Object> model=new HashMap<String, Object>();
		String userId=getString("userId");
		User user=new User();
		if(!userId.equals("")&&userId!=null){
			user=userService.getUserById(userId);
			user.setPassword(Endecrypt.decrypt(user.getPassword()));
			String dname = user.getDeptName();
			model.put("dname", dname);
		}
		List<Dept> dList =  deptService.getDeptList(null);
		model.put("dList", dList);
		model.put("user", user);
		model.put("method", "save");
		model.put("tp", getString("tp"));
		String option = Const.getSexOption(user.getSex(),false);
		model.put("sexOption", option);
		return new ModelAndView("system/user/editUser",model);
	}
	
	/**
	 * 连接到到用户修改添加页面
	 * @return
	 */
	@RequestMapping(params="method=initializeedit")
	//刷新局部
	public ModelAndView User(){
		Map<String,Object> model=new HashMap<String, Object>();
		String userId=getString("userId");
		User user=new User();
		if(!userId.equals("")&&userId!=null){
			user=userService.getUserById(userId);
		}
		model.put("user", user);
		model.put("method", "initialize");
		return new ModelAndView("system/user/editUser",model);
	}
	/**
	 * 保存用户
	 * @return
	 */
	@RequestMapping(params="method=save")
	@SystemControllerLog(description = "保存用户信息")
	@ResponseBody
	public JSONObject saveUser(){
		JSONObject json=new JSONObject();
		try {
			User user=new User();
			String userId=getString("userId");
			String oldDeptId = null;
			if(userId!=null&&!userId.equals("")){
				user=userService.getUserById(userId);
				oldDeptId = user.getDeptId();
			}
			user.setCode(getString("code"));
			user.setUserName(getString("userName"));
			user.setName(getString("name"));
			user.setPhone(getString("phone"));
			user.setTel(getString("tel"));
			user.setSex(getByte("sex"));
			user.setDeptId(getString("deptId"));
			//修改
			if(userId!=null&&!userId.equals("")){
				user.setPassword(Endecrypt.encrypt(getString("password")));
				userService.updateUserAndNotice(oldDeptId,user);
				setJsonBySuccess(json, "操作成功", true);
			//新增
			}else{
				int codeCount = userService.getCodeCount(getString("code"));
				int userNameCount = userService.getUserNameCount(getString("userName"));
				if (codeCount > 0) {
					setJsonByFail(json, "此编号已存在");
					return json;
				} else if(userNameCount > 0) {
					setJsonByFail(json, "用户名已存在");
					return json;
				} else {
					user.setUserId(UuidUtil.get32UUID());
					user.setIsdel(Const.Isdel_No);
					user.setPassword(Endecrypt.encrypt("000000"));
					user.setCreateTime(DateTools.getThisDateTimestamp());
					userService.addUser(user);
					setJsonBySuccess(json, "操作成功", true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 密码初始化
	 * @return
	 */
	@RequestMapping(params="method=initialize")
	@SystemControllerLog(description = "密码初始化")
	@ResponseBody
	public JSONObject initializeUser(){
		JSONObject json=new JSONObject();
		User user=new User();
		String id = getString("userId");
		user = userService.getUserById(id);
		user.setPassword(Endecrypt.encrypt("000000"));
		try {
			userService.updateUser(user);
			setJsonBySuccess(json,"初始化成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "初始化失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除用户
	 * @return
	 */
	@RequestMapping(params="method=delete")
	@SystemControllerLog(description = "删除用户")
	@ResponseBody
	public JSONObject deleteUser(){
		JSONObject json=new JSONObject();
		try {
			String userId=getString("ids");
			userService.deleteUser(userId);
			setJsonBySuccess(json,"删除成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败,错误"+e.getMessage());
		}
		return json;
	}
	
	 /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改用户是否有效")
    @ResponseBody
    public JSONObject isdelRole(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	userService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
	 * 连接到用户选择职位页面
	 * @return
	 */
	@RequestMapping(params="method=listUserRole")
	//刷新局部
	public ModelAndView listRole(){
		Map<String,Object> model=new HashMap<String, Object>();
		String userId=getString("userId");
		List<Role> list = roleService.getRoleListByUser(userId, "r.dept_id");
		User user = userService.getUserById(userId);
		model.put("user", user);
		model.put("list", list);
		model.put("method", "saveUserRole");
		return new ModelAndView("system/user/listUserRole",model);
	}
	
	/**
	 * 保存用户职位
	 * @return
	 */
	@RequestMapping(params="method=saveUserRole")
	@SystemControllerLog(description = "保存用户职位")
	@ResponseBody
	public JSONObject saveUserRole(){
		JSONObject json=new JSONObject();
		try {
			roleService.saveUserRole(getArrayString("roleIds"), getString("userId"));
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
    
}
