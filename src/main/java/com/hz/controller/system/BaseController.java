package com.hz.controller.system;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.alibaba.fastjson.JSONObject;
import com.hz.entity.system.User;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;

public class BaseController {

	/**
	 * 得到cookie中的令牌
	 * 
	 * @param request
	 * @return
	 */
	public String getToken(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {
			return null;
		} else {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];

				if (cookie.getName().equals(Const.COOKIE_USER)) {
					return cookie.getValue();
				}
			}
			return null;
		}
	}

	/**
	 * 生成cookie
	 * 
	 * @param response
	 * @param userCode
	 * @return 返回cookie令牌
	 */
	public String addCookie(HttpServletResponse response, String userCode) {
		// 加密令牌
		Cookie cookie = new Cookie(Const.COOKIE_USER, userCode);
		// cookie.setDomain(DataTools.getSystemDomainName());
		cookie.setPath("/");
		cookie.setValue(userCode);
		cookie.setMaxAge(1);
		response.addCookie(cookie);
		return userCode;
	}

	/**
	 * 获取当前登录用户
	 * 
	 * @return
	 */
	public User getCurrentUser() {
		return (User) getSession().getAttribute(Const.SESSION_USER);
	}

	/**
	 * 删除cookie
	 * 
	 * @param request
	 * @param response
	 */
	public void delCookie(HttpServletRequest request, HttpServletResponse response) {
		Cookie cookie = new Cookie(Const.COOKIE_USER, null);
		cookie.setMaxAge(0);
		cookie.setPath("/");
		response.addCookie(cookie);
		// for(int i=0;i<request.getCookies().length;i++){
		//
		// }
	}

	/**
	 * 得到request对象
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		return request;
	}
	public HttpSession getSession() {
		HttpSession session = null;
		try {
			session = getRequest().getSession();
		} catch (Exception e) {
		}
		return session;
	}

	/**
	 * 获取当前页数和每页数 [0] 当前页数 [1] 每页数
	 * 
	 * @return
	 */
	public int[] getPage() {
		int[] pages = new int[2];
		String size = getRequest().getParameter("pageSize");
		String num = getRequest().getParameter("pageNum");
		if (num == null || num.equals("0")) {
			pages[0] = 1;
		} else {
			try {
				pages[0] = DataTools.stringToInt(num);
			} catch (Exception e) {
				pages[0] = 1;
			}
		}
		if (size == null || size.equals("0")) {
			pages[1] = 10;
		} else {
			try {
				pages[1] = DataTools.stringToInt(size);
			} catch (Exception e) {
				pages[1] = 10;
			}
		}
		return pages;
	}
	/**
	 * 测试分页的页面信息
	 * @param i
	 * @return
	 */
	public int[] getPage(String i) {
		int[] pages = new int[2];
		String size = getRequest().getParameter("pageSize"+i);
		String num = getRequest().getParameter("pageNum"+i);
		if (num == null || num.equals("0")) {
			pages[0] = 1;
		} else {
			try {
				pages[0] = DataTools.stringToInt(num);
			} catch (Exception e) {
				pages[0] = 1;
			}
		}
		if (size == null || size.equals("0")) {
			pages[1] = 10;
		} else {
			try {
				pages[1] = DataTools.stringToInt(size);
			} catch (Exception e) {
				pages[1] = 10;
			}
		}
		return pages;
	}

	/**
	 * 获取String参数
	 * 
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		String str = getRequest().getParameter(key);
		if (str == null) {
			str = "";
		}
		return str;
	}

	/**
	 * 获取int参数
	 * 
	 * @param key
	 * @return
	 */
	public int getInt(String key) {
		String str = getRequest().getParameter(key);
		int i = 0;
		try {
			if (str != null && !("".equals(str))) {
				i = Integer.parseInt(str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	/**
	 * 获取 byte参数
	 * 
	 * @param key
	 * @return
	 */
	public byte getByte(String key) {
		String str = getRequest().getParameter(key);
		byte i = 0;
		try {
			if (str != null && !("".equals(str))) {
				i = Byte.parseByte(str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	/**
	 * 获取double参数
	 * 
	 * @param key
	 * @return
	 */
	public double getDouble(String key) {
		String str = getRequest().getParameter(key);
		double i = 0;
		try {
			if (str != null && !("".equals(str))) {
				i = Double.parseDouble(str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	/**
	 * 获取Date参数
	 * 
	 * @param key
	 * @return
	 */
	public Date getDate(String key) {
		String str = getRequest().getParameter(key);
		Date i = null;
		try {
			if (str != null && !("".equals(str))) {
				i = DateTools.stringToSqlDate(str, "yyyy-MM-dd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	/**
	 * 获取Time参数精确到秒
	 * 
	 * @param key
	 * @return
	 * @throws ParseException 
	 * @throws Exception 
	 */
	public Timestamp getTime(String key) throws ParseException  {
		String str = getRequest().getParameter(key);
		Timestamp i = null;
			if (str != null && !("".equals(str))) {
				i = DateTools.stringToTimestamp(str);
			}
		return i;
	}
	/**
	 * 获取Time参数 精确到分
	 * @param key
	 * @return
	 */
	public Timestamp getTimeM(String key) {
		String str = getRequest().getParameter(key);
		Timestamp i = null;
			if (str != null && !("".equals(str))) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				try {
					 i = new Timestamp(sdf.parse(str).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		return i;
	}
	/**
	 * 获取Time参数 精确到日
	 * @param key
	 * @return
	 */
	public Timestamp getTimeD(String key) {
		String str = getRequest().getParameter(key);
		Timestamp i = null;
			if (str != null && !("".equals(str))) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					 i = new Timestamp(sdf.parse(str).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		return i;
	}
	/**
	 * 获取 String[]参数
	 * 
	 * @param key
	 * @return
	 */
	public String[] getArrayString(String key) {
		String[] str = getRequest().getParameterValues(key);
		return str;
	}

	/**
	 * 获取int[]参数
	 * 
	 * @param key
	 * @return
	 */
	public int[] getArrayInt(String key) {
		String[] str = getRequest().getParameterValues(key);
		int[] array = new int[str.length];
		for (int i = 0; i < str.length; i++) {
			try {
				array[i] = Integer.parseInt(str[i]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return array;
	}
	
	/**
	 * 获取Timestamp[]参数
	 * 
	 * @param key
	 * @return
	 */
	public Timestamp[] getArrayTimestamp(String key) {
		String[] str = getRequest().getParameterValues(key);
		if (str!=null&&!str.equals("")) {
			Timestamp[] array = new Timestamp[str.length];
			for (int i = 0; i < str.length; i++) {
				try {
					array[i] = Timestamp.valueOf(str[i]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return array;
		}
		return null;
	}

	/**
	 * 查找参数带回
	 * 
	 * @param model
	 * @return
	 */
	public Map<String, Object> getModel(Map<String, Object> model) {
		Enumeration<?> pNames = getRequest().getParameterNames();
		while (pNames.hasMoreElements()) {
			String name = (String) pNames.nextElement();
			String value = getRequest().getParameter(name);
			model.put(name, value);
		}
		return model;
	}

	/**
	 * 设置成功json参数，不刷新
	 * 
	 * @param json
	 * @param message
	 */
	public void setJsonBySuccess(JSONObject json, String message) {
		json.put("statusCode", 200);
		json.put("isRefresh", false);
		json.put("message", message);
	}

	/**
	 * 设置成功json参数
	 * 
	 * @param json
	 * @param message
	 * @param href
	 *            刷新地址
	 */
	public void setJsonBySuccess(JSONObject json, String message, String href) {
		json.put("statusCode", 200);
		json.put("isRefresh", true);
		json.put("message", message);
		json.put("href", href);
	}

	/**
	 * 设置成功json参数
	 * 
	 * @param json
	 * @param message
	 * @param flag
	 *            是否刷新
	 */
	public void setJsonBySuccess(JSONObject json, String message, boolean flag) {
		json.put("statusCode", 200);
		json.put("isRefresh", flag);
		json.put("message", message);
	}

	/**
	 * 设置失败json参数
	 * 
	 * @param json
	 * @param message
	 */
	public void setJsonByFail(JSONObject json, String message) {
		json.put("statusCode", 300);
		json.put("message", message);
	}
	/**
	 * 设置失败json参数
	 * 
	 * @param json
	 * @param message
	 */
	public void setJsonByWarning(JSONObject json, String message) {
		json.put("statusCode", 302);
		json.put("message", message);
	}
}
