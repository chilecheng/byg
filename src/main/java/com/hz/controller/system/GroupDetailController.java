package com.hz.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.GroupType;
import com.hz.entity.system.User;
import com.hz.service.system.GroupDetailService;
import com.hz.util.Const;

/**
 * 用户管理
 * @author rgy
 *
 */
@Controller
@RequestMapping("/groupDetail.do")
public class GroupDetailController extends BaseController{
	@Autowired
	private GroupDetailService groupDetail;
	
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url","groupDetail.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listGroupUser();
		
	}
	
	/**
	 * 用户列表
	 * @author rgy 
	 */
	@RequestMapping(params="method=list")
	public ModelAndView listGroupUser(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获取页码和每页数
		int []pages=getPage();
		//设置页码和每页数
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("userName",getString("userName"));
		map.put("type",getString("type"));
		map.put("isdel", getByte("isdel"));
		PageInfo<User> page=groupDetail.getGroupDetailPageInfo(map, pages[0], pages[1], "");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page", page);
		model.put("type", getString("type"));
		model.put("method", "list");		
		return new ModelAndView("system/groupUser/listGroupDetail",model);	
	}
	
	/**
	 * 从分组中删除
	 * @return
	 */
	@RequestMapping(params="method=delete")
	@SystemControllerLog(description = "从分组中删除用户")
	@ResponseBody
	public JSONObject deleteUser(){
		JSONObject json=new JSONObject();
		try {
			String ids=getString("ids");
			groupDetail.deleteUserFromGroup(ids);
			setJsonBySuccess(json,"删除成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 连接到到用户添加修改添加页面
	 * @return
	 */
	@RequestMapping(params="method=edit")
	//刷新局部
	public ModelAndView editUser(){
		Map<String,Object> model=new HashMap<String, Object>();
		/*String type = getString("type");
		GroupType group = groupDetail.getGroupByType(type);*/
		List<GroupType> list = groupDetail.getGroupTypeList();
		Map<String,String> map = new HashMap<String, String>();
		for (GroupType gt : list) {
			Byte b = gt.getType();
			map.put(b.toString(), gt.getName());
		}
		model.put("method", "save");
		model.put("map", map);
		model.put("type", getString("type"));
		model.put("userId", getString("userId"));
		return new ModelAndView("system/groupUser/editUserType",model);
	}
	
	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(params="method=save")
	@SystemControllerLog(description = "保存用户分组信息")
	@ResponseBody
	public JSONObject saveUser(){
		JSONObject json=new JSONObject();
		try {
			String userIds=getString("ids");
			String type=getString("gtype");
			groupDetail.updateUserType(userIds,type);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	
	@RequestMapping(params="method=add")
	//刷新局部
	public ModelAndView addUser(){
		Map<String,Object> model=new HashMap<String, Object>();
		List<User> list = groupDetail.getUserNoGroup();
		model.put("method", "save");
		model.put("type", getString("type"));
		model.put("list", list);
		return new ModelAndView("system/groupUser/addUser",model);
	}
}
