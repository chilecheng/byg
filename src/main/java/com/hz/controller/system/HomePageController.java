package com.hz.controller.system;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageHelper;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.ListFueralRecord;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.FuneralRecordService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
/**
 * 首页任务列表
 * @author jgj
 *
 */
@Controller
@RequestMapping("/homePage.do")
public class HomePageController extends BaseController{
	//火化委托单
	@Autowired
	private CommissionOrderService commissionService;
	//车辆派送
	@Autowired
	private CarSchedulRecordService carService;
	//礼仪出殡
	@Autowired
	private FuneralRecordService funeralService;
	//冰柜调度
	@Autowired
	private FreezerRecordService freezerService;
	@ModelAttribute  
    public void homeModel( Model model) {  
       model.addAttribute("url", "homePage.do");
    }
	
	@RequestMapping(params = "method=showList")
	public ModelAndView showHome() {
		Map<String,Object> model=new HashMap<String,Object>();
		
		String type=getString("type");
		getSession().setAttribute("refresh", "homePage.do?type=" + type);//首页列表操作之后进行refresh(),保持轮询
		String time=DateTools.getTimeFormatString("yyyy-MM-dd",DateTools.getThisDateTimestamp());
		if("btn2".equals(type)){//超期停尸
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("cremationTime", time);
			map.put("flag", Const.IsFlag_Yse);//入柜状态
			List<FreezerRecord> comList=freezerService.getFreezerRecordHome(map);
			
			model.put("comList", comList);
		}else if("btn1".equals(type)){//业务未收费
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("checkFlag", Const.Check_Yes);
			map.put("payFlag", Const.Pay_No);
			map.put("creatTime",time );		
			List<CommissionOrder> comList=commissionService.getUnPayList(map);
			
			model.put("comList", comList);
		}else if("btn3".equals(type)){//委托单列表
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("checkFlag", Const.Check_No);
			map.put("creatTime", time);
			List<CommissionOrder> comList=commissionService.getUnPayList(map);
			
			model.put("comList", comList);
		}else if("btn4".equals(type)){//礼仪出殡
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("checkFlag", Const.Check_Yes);
			map.put("payFlag", Const.Pay_Yes);
			map.put("cremaTionTime", time);			
			
			List<ListFueralRecord> comList=funeralService.getFuneralRecordList(map);
			model.put("comList", comList);
		}else if("btn5".equals(type)){//告别厅任务
			Map<String,Object> map=new HashMap<String, Object>();
			String tomorrow=DateTools.getTomorrowDate();
			map.put("tomorrow", tomorrow);
			map.put("farewellTime", time);
			map.put("checkFlag", Const.Check_Yes);
//			map.put("payFlag", Const.Pay_Yes);
			map.put("farewellFlag", Const.IsFlag_Yse);//未布置
			List<CommissionOrder> comList=commissionService.getUnPayList(map);
			
			model.put("comList", comList);
		}else if("btn6".equals(type)){//灵堂任务
			Map<String,Object> map=new HashMap<String, Object>();
			String tomorrow=DateTools.getTomorrowDate();
			map.put("tomorrow", tomorrow);
			map.put("mourningTime", time);
			map.put("checkFlag", Const.Check_Yes);
//			map.put("payFlag", Const.Pay_Yes);
			map.put("mourningFlag", Const.IsFlag_Yse);//未布置
			List<CommissionOrder> comList=commissionService.getUnPayList(map);
			
			model.put("comList", comList);
		}else if("btn7".equals(type)){//冰柜调度
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("arriveTime", time);
			map.put("checkFlag", Const.Check_Yes);
//			map.put("payFlag", Const.Pay_Yes);
			map.put("flag", Const.IsFlag_Lock);//预定状态
			List<FreezerRecord> comList=freezerService.getFreezerRecordHome(map);
			
			model.put("comList", comList);
		}else if("btn8".equals(type)){//车辆任务
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("sentState", Const.CarSFlag_NO);//未派车状态
			//今天和明天派车的都显示
			map.put("pickStartTime", DateTools.getThisDate());
			map.put("pickEndTime", DateTools.getDayEndTime(DateTools.getDayAfterDay(DateTools.getThisDate(), 1, Calendar.DATE)));
			List<CarSchedulRecord> comList=carService.getCarSchedulRecordList(map,"pick_time asc");
			model.put("comList", comList);
		}else if("btn9".equals(type)){//普通炉任务
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("cremationTime", time);
			map.put("checkFlag", Const.Check_Yes);
			map.put("payFlag", Const.Pay_Yes);
			map.put("furnaceFlag", Const.furnace_pt);
			PageHelper.orderBy("com.creat_time desc");
			List<CommissionOrder> comList=commissionService.getCrematorList(map);
			
			model.put("comList", comList);
		}else if("btn10".equals(type)){//特约炉任务
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("cremationTime", time);
			map.put("checkFlag", Const.Check_Yes);
			map.put("payFlag", Const.Pay_Yes);
			map.put("furnaceFlag", Const.furnace_ty);
			PageHelper.orderBy("com.creat_time desc");
			List<CommissionOrder> comList=commissionService.getCrematorList(map);
			model.put("comList", comList);
		}
		
			
		model.put("method", "post");
		model.put("type", type);
		model.put("url", "homePage.do");
		return new ModelAndView("taskList/"+type,model);
		
	}
}
