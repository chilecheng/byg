package com.hz.controller.system;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.GroupType;
import com.hz.service.system.GroupDetailService;
import com.hz.service.system.GroupUserService;
import com.hz.util.UuidUtil;

/**
 * 用户管理
 * @author rgy
 */
@Controller
@RequestMapping("/groupUser.do")
public class GroupUserController extends BaseController {

	@Autowired
	private GroupUserService groupService;
	/*@Autowired
	private RoleService roleService;*/
	@Autowired
	private GroupDetailService groupDetail;

	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "groupUser.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listUser();

	}

	/**
	 * 用户列表
	 * @author rgy
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listUser() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获取页码和每页数
		int[] pages = getPage();
		// 设置页码和每页数
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("name", getString("name"));
		PageInfo<GroupType> page = groupService.getGroupUserPageInfo(map, pages[0], pages[1], "type");
		model.put("page", page);
		model.put("method", "list");
		return new ModelAndView("system/groupUser/listGroup", model);
	}

	/**
	 * 连接到到用户分组修改添加页面
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	// 刷新局部
	public ModelAndView editUser() {
		Map<String, Object> model = new HashMap<String, Object>();
		String id = getString("id");
		GroupType type = new GroupType();
		if (!id.equals("") && id != null) {
			type = groupService.getTypeById(id);
		}
		model.put("type", type);
		model.put("method", "save");
		model.put("tp", getString("tp"));
		return new ModelAndView("system/groupUser/editType", model);
	}

	/**
	 * 保存分组
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存分组信息")
	@ResponseBody
	public JSONObject saveUser() {
		JSONObject json = new JSONObject();
		try {
			GroupType type = new GroupType();
			String id = getString("id");
			if (id != null && !id.equals("")) {
				type = groupService.getTypeById(id);
			}  
			type.setType(getByte("type"));;
			type.setName(getString("name"));
			// 修改
			if (id != null && !id.equals("")) {
				groupService.updateType(type);
				groupDetail.updateUT(type.getType(), getString("typeold"));
				setJsonBySuccess(json, "操作成功", true);
			} else {
				// 新增
				String typeString = getString("type");
				int i = Integer.valueOf(typeString);
				if (i < 0 || i >127) {
					setJsonByFail(json, "编号超出范围");
					return json;
				}
				int typeCount = groupService.getTypeCount((byte)i);
				int nameCount = groupService.getNameCount(getString("name"));
				if (typeCount > 0) {
					setJsonByFail(json, "此编号已存在");
					return json;
				} else if(nameCount > 0) {
					setJsonByFail(json, "组名已存在");
					return json;
				} else {
					type.setId(UuidUtil.get32UUID());
					groupService.addType(type);
					setJsonBySuccess(json, "操作成功", true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误" + e.getMessage());
		}
		return json;
	}

	/**
	 * 删除分组
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除用户分组信息")
	@ResponseBody
	public JSONObject deleteUser() {
		JSONObject json = new JSONObject();
		try {
			String ids = getString("ids");
			groupService.deleteUser(ids);
			setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败,错误" + e.getMessage());
		}
		return json;
	}

}
