package com.hz.controller.system;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.service.log.LogService;
import com.hz.util.DateTools;
/**
 * 用户操作日志  
 * @author jgj
 *
 */

@Controller
@RequestMapping("/operationLog.do")
public class OperationLogController extends BaseController{
	
	@Autowired
	private LogService logService;
	
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "operationLog.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() throws ParseException {
		return listFreeBasicServices();
    }
	
	
	/**
	 * 获取用户操作记录列表
	 * @return
	 * @throws ParseException 
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFreeBasicServices() throws ParseException {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String,Object>();
		String findBy=getString("find");
		maps.put("findBy", findBy);
		if("findByName".equals(findBy)){
			String findByName=getString("search");
			maps.put("findByName", findByName);
		}
		if("findByMethod".equals(findBy)){
			String findByCode=getString("search");
			maps.put("findByMethod", findByCode);
		}
		//开始时间
		String startStr=getString("startTime");
//		Timestamp startTime=null;
		if(!"".equals(startStr)){
//			startTime=DateTools.stringToTimestamp(startStr);	
			maps.put("startTime", startStr);
		}
		//结束时间
		String endStr=getString("endTime");
//		Timestamp endTime=null;
		if(!"".equals(endStr)){
//			endTime=DateTools.stringToTimestamp(endStr);
			maps.put("endTime", endStr+"23:59:59");
		}		
		
		maps.put("dName", "");
		PageInfo page=logService.getOperationLogPageInfo(maps,pages[0],pages[1],"create_time desc");
		model.put("page", page);
		model.put("bTime", startStr);
		model.put("eTime", endStr);
        model.put("method", "list");
		return new ModelAndView("system/operationLog/listOperationLog",model);		
    }
    
}

