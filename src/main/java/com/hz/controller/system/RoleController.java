package com.hz.controller.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.entity.system.Dept;
import com.hz.entity.system.Menu;
import com.hz.entity.system.Role;
import com.hz.service.system.DeptService;
import com.hz.service.system.RoleService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 职位管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/role.do")
public class RoleController extends BaseController{
	@Autowired
	private RoleService roleService;
	@Autowired
	private DeptService deptService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "role.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listRole();
    }

	/**
	 * 职位列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listRole(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		if (getString("clearPageNum") != null && getString("clearPageNum").equals("clear")) {
			pages[0] = 1;
		}
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<Role> page=roleService.getRolePageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("system/role/listRole",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editRole(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		String deptId=getString("deptId");
		Role role=new Role();
		Dept dept=new Dept();
		if(id!=null&&!id.equals("")){
			role=roleService.getRoleById(id);
			if (role.getDeptId()!=null&&!role.getDeptId().equals("")) {
				dept=deptService.getDeptById(deptId);
			}
		}
		if(deptId!=null&&!deptId.equals("")){
			dept=deptService.getDeptById(deptId);
		}
		String deptOption=deptService.getDeptOption(deptId);
		model.put("deptOption", deptOption);
        model.put("role", role);
        model.put("dept", dept);
        model.put("method", "save");
		return new ModelAndView("system/role/editRole",model);
    }

    /**
     * 保存职位
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存职位信息")
    @ResponseBody
    public JSONObject saveRole(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		Role role=new Role();
    		if (id!=null&&!id.equals("")) {
    			role=roleService.getRoleById(id);
    		}
    		role.setIndexFlag(getInt("indexFlag"));
    		role.setDeptId(getString("deptId"));
    		role.setName(getString("name"));
    		if (id==null||id.equals("")) {
    			role.setId(UuidUtil.get32UUID());
    			role.setIsdel(Const.Isdel_No);
    			roleService.addRole(role);
    		}else {
    			roleService.updateRole(role);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除职位
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除职位信息")
    @ResponseBody
    public JSONObject deleteRole(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    roleService.deleteRole(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改职位是否有效")
    @ResponseBody
    public JSONObject isdelRole(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	roleService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
    

	/**
	 * 链接到职位菜单设置页面
	 * @return
	 */
    @RequestMapping(params = "method=listRoleMenu")
    public ModelAndView listRoleMenu(){
		Map<String,Object> model=new HashMap<String,Object>();
		String roleId = getString("roleId");
		Role role = roleService.getRoleById(roleId);
		model.put("role", role);
		
		List<Menu> list = roleService.getMenuList(roleId, "m.father_id asc,m.index_flag asc,m.id asc");
//		StringBuilder content = new StringBuilder("");
		String content = "";
		int index = 0;
		for(int i=0;i<list.size();i++){
			Menu menu = list.get(i);
			if(menu.getMenuId().equals("")){
				if(index!=0){
					content+=",";
//					content.append(",");
				}
				content=getTree(content, menu, list);
//				content=getTree(content, menu, list);
				index++;
			}
		}
		model.put("content", content.toString());
        model.put("method", "saveRoleMenu");
		return new ModelAndView("system/role/editRoleMenu",model);
    }
    /**
     * 获得tree结构
     * @param content
     * @param menu
     * @param list
     * @return
     */
   /* public StringBuilder getTree(StringBuilder content,Menu menu,List<Menu> list){
    	boolean isFirst = false;
    	if(content.length() == 0){
    		isFirst = true;
    	}
    	content.append("{");
    	content.append("text:'"+menu.getName()+"'");
    	content.append(",tags:['"+menu.getId()+"']");
    	content.append(",selectable: false");
    	content.append(",state: {");
    	if(menu.getRoleMenuNum()>0){
        	content.append("checked: true,");
    	}
    	if(isFirst){
    		content.append("expanded: false}");
    	}else{
    		content.append("expanded: false}");
    	}
    	List<Menu> childs = new ArrayList<Menu>();
    	for(int i=0;i<list.size();i++){
    		Menu c = list.get(i);
    		if(c.getMenuId().equals(menu.getId())){
    			childs.add(c);
    		}
    	}
    	if(childs.size()>0){
    		content.append(",nodes: [");
    		for(int j=0;j<childs.size();j++){
    			if(j!=0){
    				content.append(",");
    			}
    			Menu m = childs.get(j);
    			content=getTree(content,m, list);
    		}
    		content.append("]");
    	}
    	content.append("}");
    	return content;
    }*/
    /**
     * 获得tree结构
     * @param content
     * @param menu
     * @param list
     * @return
     */
    public String getTree(String content,Menu menu,List<Menu> list){
    	boolean isFirst = false;
    	if(content.equals("")){
    		isFirst = true;
    	}
    	content+= "{";
    	content+="text:'"+menu.getName()+"'";
    	content+=",tags:['"+menu.getId()+"']";
    	content+=",selectable: false";
    	content+=",state: {";
    	if(menu.getRoleMenuNum()>0){
        	content+="checked: true,";
    	}
    	if(isFirst){
    		content+="expanded: true,}";
    	}else{
    		content+="expanded: false,}";
    	}
    	List<Menu> childs = new ArrayList<Menu>();
    	for(int i=0;i<list.size();i++){
    		Menu c = list.get(i);
    		if(c.getMenuId().equals(menu.getId())){
    			childs.add(c);
    		}
    	}
    	if(childs.size()>0){
    		content+=",nodes: [";
    		for(int j=0;j<childs.size();j++){
    			if(j!=0){
    				content+=",";
    			}
    			Menu m = childs.get(j);
    			content=getTree(content,m, list);
    		}
    		content+="]";
    	}
    	content+="}";
    	return content;
    }
    
    

    /**
     * 保存权限(暂时不使用，用ajax方法添加删除)
     * @return
     */
    @RequestMapping(params = "method=saveRoleMenu")
    @SystemControllerLog(description = "保存职位权限")
    @ResponseBody
    public JSONObject saveRoleMenu(){
		JSONObject json = new JSONObject();
    	try {
    		String ids =getString("ids");
    		String roleId =getString("roleId");
    		roleService.addOrEditRoleMenu(roleId, ids);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    

    /**
     * ajax保存职位菜单
     * @return
     */
    @RequestMapping(params = "method=addRoleMenu")
    @SystemControllerLog(description = "保存职位菜单")
    @ResponseBody
    public JSONObject addRoleMenu(){
		JSONObject json = new JSONObject();
    	try {
    		String menuId =getString("menuId");
    		String roleId =getString("roleId");
    		roleService.addRoleMenu(roleId, menuId);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    

    /**
     * ajax删除职位菜单
     * @return
     */
    @RequestMapping(params = "method=delRoleMenu")
    @SystemControllerLog(description = "删除职位菜单")
    @ResponseBody
    public JSONObject delRoleMenu(){
		JSONObject json = new JSONObject();
    	try {
    		String menuId =getString("menuId");
    		String roleId =getString("roleId");
    		roleService.delRoleMenu(roleId, menuId);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
}
