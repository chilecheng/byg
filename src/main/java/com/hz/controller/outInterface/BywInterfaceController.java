package com.hz.controller.outInterface;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.oldData.OldData;
import com.hz.entity.outInterface.FareInterface;
import com.hz.entity.outInterface.MourningInterface;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.ItemOrder;
import com.hz.service.oldData.OldDataService;
import com.hz.service.outInterface.OutInterfaceService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 开放给殡仪网的接口
 * @author jgj
 */
@Controller
@RequestMapping("/showData.do")
public class BywInterfaceController extends BaseController {
	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "showData.do");
	}
	@Autowired
	private OutInterfaceService outInterfaceService;

	/**
	 * 测试接口
	 * @return
	 */
	@RequestMapping(params = "method=returnData")
	@ResponseBody
	public JSONObject test(){
		JSONObject json = new JSONObject();
		String user=getString("user");
		String password=getString("password");
		String url=getString("url");
		//判断是否符合接口开放
		if("byw".equals(user) && "abc123".equals(password) &&"http://www.wzbyw.com.cn".equals(url)){
			try{
				Date now=DateTools.getThisDate();
				String beginTime=DateTools.getTimeFromatString("yyyy-MM-dd",now);
				List<FareInterface> fareList=outInterfaceService.getFarewellList(beginTime);
				List<MourningInterface> mourningList=outInterfaceService.getMourningList(beginTime);
				json.put("fareList", fareList);
				json.put("mourningList", mourningList);
			}catch(Exception e){
				e.printStackTrace();
			}
			return json;
			
		}else{
			return null;
		}
	}

}
