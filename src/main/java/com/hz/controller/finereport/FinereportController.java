package com.hz.controller.finereport;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.util.DateTools;

/**
 * ����
 * @author cjb
 *
 */
@Controller
@RequestMapping("/finereport.do")
public class FinereportController extends BaseController{
	@RequestMapping
    public ModelAndView unspecified() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		Date date = DateTools.getThisDateUtil();
		if(getDate("date")!=null){
			date = getDate("date");
		}
		model.put("date", date);
		return new ModelAndView("finereport/fineReport",model); 
    }
}	
