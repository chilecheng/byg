package com.hz.controller.warehouse;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.warehouse.Commodity;
import com.hz.entity.warehouse.Stock;
import com.hz.service.warehouse.CommodityService;
import com.hz.service.warehouse.StockService;
import com.hz.service.warehouse.WarehouseService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 库存信息管理
 * @author 金国军
 *
 */
@Controller
@RequestMapping("/stock.do")
public class StockController extends BaseController{
	@Autowired
	private StockService stockService;
	@Autowired
	private WarehouseService warehouseService;
	@Autowired
	private CommodityService commodityService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "stock.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listStock();
		
	}
	/**
	 * 库存信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listStock(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<Stock> page=stockService.getStockPageInfo(map, pages[0],pages[1], "");
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("warehouse/ye/listStock",model);
	}
	/**
	 * 连接到页面添加库存信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editStock(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		String warehouseOption="";
		String commodityOption="";
		int amount=0;
		Stock stock=new Stock();
		if(id!=null&&!id.equals("")){
			stock=stockService.getStockById(id);
			warehouseOption=warehouseService.getWarehouseOption(stock.getWarehouseId(), false);
			commodityOption=commodityService.getCommodityOption(stock.getCommodityId(), false);
			amount=stock.getAmount();
		}else{
			warehouseOption=warehouseService.getWarehouseOption("", false);
			commodityOption=commodityService.getCommodityOption("", false);
		}
		String intoWayOption=Const.getIntoWayOption(Const.IntoWay_Merchandise, false);
		model.put("intoWayOption", intoWayOption);
		model.put("warehouseOption", warehouseOption);
		model.put("commodityOption", commodityOption);
		model.put("amount",amount);
		model.put("stock", stock);
		model.put("method", "save");
		return new ModelAndView("warehouse/ye/editStock",model);
	}
	/**
	 * 保存库存信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存库存信息")
	@ResponseBody
	public JSONObject addStock(){
		JSONObject json=new JSONObject();
		try {
			Stock stock=new Stock();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				stock=stockService.getStockById(id);
			}
			String commodityId=getString("commodityId");
			stock.setCommodityId(commodityId);
			Commodity commodity=commodityService.getCommodityById(commodityId);
			String supplierId="";
			if(commodity!=null){
				supplierId=commodity.getSupplierId();
			}
			stock.setSupplierId(supplierId);
			stock.setWarehouseId(getString("warehouseId"));
			stock.setAmount(getInt("amount"));
			Timestamp recordTime=DateTools.getThisDateTimestamp();
			stock.setRecordTime(recordTime);
			stockService.addOrUpdate(id,stock);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除库存信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除库存信息")
	@ResponseBody
	public JSONObject delStock(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			stockService.delStock(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
