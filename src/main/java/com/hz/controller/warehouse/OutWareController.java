package com.hz.controller.warehouse;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.warehouse.Commodity;
import com.hz.entity.warehouse.OutWare;
import com.hz.entity.warehouse.Stock;
import com.hz.service.warehouse.CommodityService;
import com.hz.service.warehouse.OutWareService;
import com.hz.service.warehouse.StockService;
import com.hz.service.warehouse.WarehouseService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 出库信息管理
 * @author 金国军
 *
 */
@Controller
@RequestMapping("/outWare.do")
public class OutWareController extends BaseController{
	@Autowired
	private OutWareService outWareService;
	@Autowired
	private CommodityService commodityService;
	@Autowired
	private WarehouseService warehouseService;
	@Autowired
	private StockService stockService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "outWare.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listOutWare();
		
	}
	/**
	 * 出库信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listOutWare(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		PageInfo<OutWare> page=outWareService.getOutWarePageInfo(map, pages[0],pages[1], "");
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("warehouse/ye/listOutWare",model);
	}
	/**
	 * 连接到页面添加出库信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editOutWare(){
		Map<String,Object> model=new HashMap<String, Object>();
		OutWare outWare=new OutWare();
		String warehouseOption=warehouseService.getWarehouseOption("", false);
		String commodityOption=commodityService.getCommodityOption("", false);
		String outWayOption=Const.getOutWayOption(Const.IntoWay_Merchandise, false);
		model.put("outWayOption", outWayOption);
		model.put("warehouseOption", warehouseOption);
		model.put("commodityOption", commodityOption);
		model.put("outWare", outWare);
		model.put("method", "save");
		return new ModelAndView("warehouse/ye/editOutWare",model);
	}
	/**
	 * 保存出库信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存出库信息")
	@ResponseBody
	public JSONObject addOutWare(){
		JSONObject json=new JSONObject();
		try {
			OutWare outWare=new OutWare();
			String operator=getCurrentUser().getName();
			Timestamp now=DateTools.getThisDateTimestamp();
			String commodityId=getString("commodityId");
			String warehouseId=getString("warehouseId");
			int number=getInt("number");
			outWare.setId(UuidUtil.get32UUID());
			outWare.setSerialNumber(getString("serialNumber"));
			outWare.setNumber(number);
			outWare.setWarehouseId(warehouseId);
			outWare.setCommodityId(commodityId);
			outWare.setOutTime(now);
			outWare.setOperator(operator);
			outWare.setOutWay(getByte("outWay"));
			outWareService.saveOutWare(outWare);
			
			Commodity commodity=commodityService.getCommodityById(commodityId);
			String supplierId="";
			if(commodity!=null){
				supplierId=commodity.getSupplierId();
			}
			
			//以下更改库存记录
			stockService.linkedSave(commodityId, warehouseId, supplierId, now, number, "reduce");
			
			
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
