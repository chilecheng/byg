package com.hz.controller.warehouse;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.warehouse.Supplier;
import com.hz.service.warehouse.SupplierService;
import com.hz.util.Const;

/**
 * 供应商信息
 * @author 金国军
 *
 */
@Controller
@RequestMapping("/supplier.do")
public class SupplierController extends BaseController{
	@Autowired
	private SupplierService supplierService;
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "supplier.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listSupplier();
		
	}
	/**
	 * 供应商信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listSupplier(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isDel",getByte("isdel"));
		PageInfo<Supplier> page=supplierService.getSupplierPageInfo(map, pages[0],pages[1], "");
		model.put("Isdel_No", Const.Is_No);
		model.put("Isdel_Yes", Const.Is_Yes);
		model.put("page",page);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("method","list");
		return new ModelAndView("warehouse/base/listSupplier",model);
	}
	/**
	 * 连接到页面添加修改供应商信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editSupplier(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		Supplier supplier=new Supplier();
		String isDelOption="";
		if(id!=null&&!id.equals("")){
			supplier=supplierService.getSupplierById(id);
			isDelOption=Const.getIsdelOption(supplier.getIsDel(), false);
		}else{
			isDelOption=Const.getIsdelOption(Const.Is_Yes, false);
		}
		model.put("supplier", supplier);
		model.put("isDelOption", isDelOption);
		model.put("method", "save");
		return new ModelAndView("warehouse/base/editSupplier",model);
	}
	/**
	 * 保存供应商信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存供应商信息")
	@ResponseBody
	public JSONObject addSupplier(){
		JSONObject json=new JSONObject();
		try {
			Supplier supplier=new Supplier();
			String id=getString("id");
			if(id!=null && !"".equals(id)){
				supplier=supplierService.getSupplierById(id);
			}
			supplier.setSupplierId(getString("supplierId"));
			supplier.setName(getString("name"));
			supplier.setContact(getString("contact"));
			supplier.setPhoneNumber(getString("phoneNumber"));
			supplier.setBank(getString("bank"));
			supplier.setAccountNumber(getString("accountNumber"));
			supplier.setTaxId(getString("taxId"));
			supplier.setBalance(getDouble("balance"));
			supplier.setIsDel(getByte("isdel"));
			supplierService.addOrUpdate(id,supplier);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除供应商信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除供应商信息")
	@ResponseBody
	public JSONObject deleteSupplier(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			supplierService.delSupplier(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改供应商信息的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改供应商信息是否有效")
	@ResponseBody
	public JSONObject isdelSupplier(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			supplierService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
