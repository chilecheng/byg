package com.hz.controller.warehouse;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.warehouse.Commodity;
import com.hz.service.base.ItemService;
import com.hz.service.warehouse.CommodityService;
import com.hz.service.warehouse.SupplierService;
import com.hz.util.Const;

/**
 * 商品信息
 * @author 金国军
 *
 */
@Controller
@RequestMapping("/commodity.do")
public class CommodityController extends BaseController{
	@Autowired
	private CommodityService commodityService;
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private ItemService itemService;
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "commodity.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listCommodity();
		
	}
	/**
	 * 商品信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listCommodity(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isDel",getByte("isdel"));
		PageInfo<Commodity> page=commodityService.getCommodityPageInfo(map, pages[0],pages[1], "");
		model.put("Isdel_No", Const.Is_No);
		model.put("Isdel_Yes", Const.Is_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("warehouse/base/listCommodity",model);
	}
	/**   
	 * 连接到页面添加修改商品信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editCommodity(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		Commodity commodity=new Commodity();
		String supplierOption="";
		String itemOption="";
		String isDelOption="";
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("typeId", "29c8f72d873647199d48ed817ee64a0d");
		if(id!=null&&!id.equals("")){
			commodity=commodityService.getCommodityById(id);
			supplierOption=supplierService.getSupplierOption(commodity.getSupplierId(),false);
			itemOption=itemService.getItemOption(paramMap, commodity.getItemId(), false);
			isDelOption=Const.getIsdelOption(commodity.getIsDel(), false);
		}else{
			supplierOption=supplierService.getSupplierOption("",false);
			itemOption=itemService.getItemOption(paramMap, "", false);
			isDelOption=Const.getIsdelOption(Const.Is_Yes, false);
		}
		model.put("commodity", commodity);
		model.put("supplierOption", supplierOption);
		model.put("itemOption", itemOption);
		model.put("isDelOption", isDelOption);
		model.put("method", "save");
		return new ModelAndView("warehouse/base/editCommodity",model);
	}
	/**
	 * 保存商品信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存商品信息")
	@ResponseBody
	public JSONObject addCommodity(){
		JSONObject json=new JSONObject();
		try {
			Commodity commodity=new Commodity();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				commodity=commodityService.getCommodityById(id);
			}
			commodity.setCommodityId(getString("commodityId"));
			commodity.setName(getString("name"));
			commodity.setSupplierId(getString("supplierId"));
			commodity.setPrice(getDouble("price"));
			commodity.setItemId(getString("itemId"));
			commodity.setIsDel(getByte("isdel"));
			commodityService.addOrUpdate(id,commodity);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除商品信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除商品信息")
	@ResponseBody
	public JSONObject deleteCommodity(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			commodityService.delCommodity(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改商品信息的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改商品信息是否有效")
	@ResponseBody
	public JSONObject isdelCommodity(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			commodityService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
