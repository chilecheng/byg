package com.hz.controller.warehouse;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.warehouse.Commodity;
import com.hz.entity.warehouse.Stock;
import com.hz.entity.warehouse.Storage;
import com.hz.service.warehouse.CommodityService;
import com.hz.service.warehouse.StockService;
import com.hz.service.warehouse.StorageService;
import com.hz.service.warehouse.SupplierService;
import com.hz.service.warehouse.WarehouseService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 入库信息管理
 * @author 金国军
 *
 */
@Controller
@RequestMapping("/storage.do")
public class StorageController extends BaseController{
	@Autowired
	private StorageService storageService;
	@Autowired
	private StockService stockService;
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private CommodityService commodityService;
	@Autowired
	private WarehouseService warehouseService;
	
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "storage.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listStorage();
		
	}
	/**
	 * 入库信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listStorage(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		PageInfo<Storage> page=storageService.getStoragePageInfo(map, pages[0],pages[1], "");
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("warehouse/ye/listStorage",model);
	}
	/**
	 * 连接到页面添加入库信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editStorage(){
		Map<String,Object> model=new HashMap<String, Object>();
		Storage storage=new Storage();
		String warehouseOption=warehouseService.getWarehouseOption("", false);
		String commodityOption=commodityService.getCommodityOption("", false);
		String intoWayOption=Const.getIntoWayOption(Const.IntoWay_Merchandise, false);
		model.put("storage", storage);
		model.put("intoWayOption", intoWayOption);
		model.put("warehouseOption", warehouseOption);
		model.put("commodityOption", commodityOption);
		model.put("method", "save");
		return new ModelAndView("warehouse/ye/editStorage",model);
	}
	/**
	 * 保存入库信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存入库信息")
	@ResponseBody
	public JSONObject addStorage(){
		JSONObject json=new JSONObject();
		try {
			Storage storage=new Storage();
			String supplierId="";
			storage.setId(UuidUtil.get32UUID());
			storage.setSerialNumber(getString("serialNumber"));
			String commodityId=getString("commodityId");
			Commodity commodity=commodityService.getCommodityById(commodityId);
			if(commodity!=null){
				supplierId=commodity.getSupplierId();
			}
			storage.setCommodityId(getString("commodityId"));
			storage.setSupplierId(supplierId);
			String warehouseId=getString("warehouseId");
			storage.setWarehouseId(warehouseId);
			int number=getInt("number");
			storage.setNumber(number);
			Timestamp now=DateTools.getThisDateTimestamp();
			storage.setIntoTime(now);
			storage.setIntoWay(getByte("intoWay"));
			String operator=getCurrentUser().getName();
			storage.setOperator(operator);
			storageService.saveStorage(storage);
			
			//以下在库存单中加库存
			stockService.linkedSave(commodityId, warehouseId, supplierId, now, number, "add");
			
			
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
