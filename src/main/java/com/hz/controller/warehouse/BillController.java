package com.hz.controller.warehouse;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.warehouse.Bill;
import com.hz.entity.warehouse.Commodity;
import com.hz.service.warehouse.BillService;
import com.hz.service.warehouse.CommodityService;
import com.hz.service.warehouse.SupplierService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 结算信息管理
 * @author 金国军
 *
 */
@Controller
@RequestMapping("/bill.do")
public class BillController extends BaseController{
	@Autowired
	private BillService	billService;
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private CommodityService commodityService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "bill.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listBill();
		
	}
	/**
	 * 结算信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listBill(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<Bill> page=billService.getBillPageInfo(map, pages[0],pages[1], "");
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("warehouse/ye/listBill",model);
	}
	/**
	 * 连接到页面添加结算信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editBill(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		String commodityOption="";
		Bill bill=new Bill();
		if(id!=null&&!id.equals("")){
			bill=billService.getBillById(id);
			commodityOption=commodityService.getCommodityOption(bill.getCommodityId(), false);
		}else{
			commodityOption=commodityService.getCommodityOption("", false);
		}
		model.put("bill", bill);
		model.put("commodityOption", commodityOption);
		model.put("method", "save");
		return new ModelAndView("warehouse/ye/editBill",model);
	}
	/**
	 * 保存出库信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存结算信息")
	@ResponseBody
	public JSONObject addBill(){
		JSONObject json=new JSONObject();
		try {
			Bill bill=new Bill();
			String supplierId="";
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				bill=billService.getBillById(id);
			}
			bill.setSerialNumber(getString("serialNumber"));
			String commodityId=getString("commodityId");
			Commodity commodity=commodityService.getCommodityById(commodityId);
			if(commodity !=null){
				supplierId=commodity.getSupplierId();
			}
			bill.setCommodityId(commodityId);
			bill.setSupplierId(supplierId);
			int sell=getInt("sell");
			double price=getDouble("price");
			bill.setSell(sell);
			bill.setRecover(getInt("recover"));
			bill.setLoss(getInt("loss"));
			bill.setPrice(price);
			bill.setTotal(sell*price);
			String operator=getCurrentUser().getName();
			bill.setOperator(operator);
			Timestamp now=DateTools.getThisDateTimestamp();
			bill.setSettlementTime(now);
			billService.addOrUpdate(id,bill);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除结算信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除结算信息")
	@ResponseBody
	public JSONObject deleteBill(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			billService.delBill(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
