package com.hz.controller.warehouse;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.warehouse.Warehouse;
import com.hz.service.warehouse.WarehouseService;
import com.hz.util.Const;

/**
 * 仓库信息
 * @author 金国军
 *
 */
@Controller
@RequestMapping("/warehouse.do")
public class WarehouseController extends BaseController{
	@Autowired
	private WarehouseService warehouseService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "warehouse.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listWarehouse();
		
	}
	/**
	 * 供应商信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listWarehouse(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isDel",getByte("isdel"));
		PageInfo<Warehouse> page=warehouseService.getWarehousePageInfo(map, pages[0],pages[1], "");
		model.put("Isdel_No",Const.Is_No);
		model.put("Isdel_Yes",Const.Is_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("warehouse/base/listWarehouse",model);
	}
	/**
	 * 连接到页面添加修改仓库信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editWarehouse(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		Warehouse warehouse=new Warehouse();
		String wareOption="";
		if(id!=null&&!id.equals("")){
			warehouse=warehouseService.getWarehouseById(id);
			wareOption=Const.getIsdelOption(warehouse.getIsDel(), false);
		}else{
			wareOption=Const.getIsdelOption(Const.Is_Yes, false);
		}
		model.put("warehouse", warehouse);
		model.put("wareOption", wareOption);
		model.put("method", "save");
		return new ModelAndView("warehouse/base/editWarehouse",model);
	}
	/**
	 * 保存仓库信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存仓库信息")
	@ResponseBody
	public JSONObject addWarehouse(){
		JSONObject json=new JSONObject();
		try {
			Warehouse warehouse=new Warehouse();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				warehouse=warehouseService.getWarehouseById(id);
			}
			warehouse.setName(getString("name"));
			warehouse.setComment(getString("comment"));
			warehouse.setIsDel(getByte("isdel"));
			warehouseService.addOrUpdate(id,warehouse);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除仓库信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除仓库信息")
	@ResponseBody
	public JSONObject delWarehouse(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			warehouseService.delWarehouse(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改仓库信息的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改仓库信息是否有效")
	@ResponseBody
	public JSONObject isdelWarehouse(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			warehouseService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
