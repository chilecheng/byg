package com.hz.controller.cemetery.index;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.AppointmentRecord;
import com.hz.entity.cemetery.BuyRecord;
import com.hz.entity.cemetery.Cemetery;
import com.hz.service.cemetery.AppointmentRecordService;
import com.hz.service.cemetery.BuyRecordService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.util.Const;

/**
 * 陵园主界面--效果等同6.2系统界面
 *
 * @author jgj
 */
@Controller
@RequestMapping("/cemeteryInterface.do")
public class CemeteryInterfaceController extends BaseController {
    @Autowired
    private CemeteryService cemeteryService;
    @Autowired
    private BuyRecordService buyRecordService;
    @Autowired
    private AppointmentRecordService appointmentRecordService;

    @ModelAttribute
    public void CemeteryModel(Model model) {
        model.addAttribute("url", "cemeteryInterface.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listCemetery();
    }

    /**
     * 陵园信息查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listCemetery() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        // 状态为启用
        maps.put("isdel", Const.Isdel_No);
        List<Cemetery> list = cemeteryService.getCemeteryList(maps, "");
        model.put("method", "list");
        model.put("cemetery", list);
        return new ModelAndView("cemetery/cemeteryInterface/cemeteryInterface", model);
    }

    /**
     * 墓穴购买记录
     *
     * @return
     */
    @RequestMapping(params = "method=tombPayRecord")
    public ModelAndView tombPayRecord() {
        Map<String, Object> model = new HashMap<String, Object>();
        // 获得页面页码信息
        int[] pages = getPage();
        model = getModel(model);
        PageInfo<BuyRecord> page = buyRecordService.getBuyRecordPageInfo(null, pages[0], pages[1], null);
        model.put("page", page);
        model.put("method", "save");
        model.put("url", "cemeteryInterface.do");
        return new ModelAndView("cemetery/cemeteryInterface/listAppointmentRecord", model);
    }

    /**
     * 墓穴预约记录
     *
     * @return
     */
    @RequestMapping(params = "method=appointmentRecord")
    public ModelAndView appointmentRecord() {
        Map<String, Object> model = new HashMap<String, Object>();
        // 获得页面页码信息
        int[] pages = getPage();
        model = getModel(model);
        PageInfo<AppointmentRecord> page = appointmentRecordService.getAppointmentRecordPageInfo(null, pages[0], pages[1], null);
        model.put("page", page);
        model.put("method", "save");
        model.put("url", "cemeteryInterface.do");
        return new ModelAndView("cemetery/cemeteryInterface/listAppointmentRecord", model);
    }

    /**
     * 删除陵园信息
     *
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除陵园信息")
    @ResponseBody
    public JSONObject deleteCemetery() {
        JSONObject json = new JSONObject();
        try {
            String id = getString("ids");
            cemeteryService.deleteCemetery(id);
            setJsonBySuccess(json, "删除成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "删除失败，错误" + e.getMessage());
        }
        return json;
    }

}
