package com.hz.controller.cemetery.index;

import java.awt.Image;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.system.User;
import com.hz.service.cemetery.CemeteryService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 陵园信息增删改查控制层
 * @author jgj
 */
@Controller
@RequestMapping("/cemetery.do")
public class CemeteryController extends BaseController {
	@Autowired
	private CemeteryService cemeteryService;
	
	@ModelAttribute
	public void CemeteryModel(Model model) {
		model.addAttribute("url", "cemetery.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listCemetery();
	}

	/**
	 * 陵园信息查询列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listCemetery() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		// 填入查询内容
		Map<String, Object> maps = new HashMap<String, Object>();
		//死者姓名
		String name = getString("name");
		maps.put("name", name);
		String isDelOption=Const.getIsdelOption(Const.Isdel_No, true);
		PageInfo<Cemetery> page = cemeteryService.getCemeteryPageInfo(maps, pages[0], pages[1],"");
		model.put("page", page);
		model.put("method", "list");
		model.put("isDelOption", isDelOption);
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("name",name );
		return new ModelAndView("cemetery/cemetery/listCemetery", model);
	}
	
    /**
	 * 添加或修改陵园信息
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView editCemetery() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String id=getString("id");
		Cemetery cemetery=cemeteryService.getCemeteryById(id);
		String isDelOption="";
		if(cemetery !=null){
			isDelOption=Const.getIsdelOption(cemetery.getIsdel(), false);
		}else{
			isDelOption=Const.getIsdelOption(Const.Isdel_No, false);
		}
		model.put("cemetery", cemetery);
		model.put("isDelOption", isDelOption);
		model.put("method", "save");
		model.put("url", "cemetery.do");
		return new ModelAndView("cemetery/cemetery/editCemetery", model);
	}
	
	
	/**
	 * 图片上传
	 * @param file
	 * @return
	 * @throws Exception
	 * 
	 */
	@RequestMapping(params = "method=fileupload")  
	@SystemControllerLog(description = "公墓陵园图片上传")
	@ResponseBody
    public String fildUpload(@RequestParam(value="file",required=false) MultipartFile file)throws Exception{  
		CommonsMultipartFile cf= (CommonsMultipartFile)file; 
	    DiskFileItem fi = (DiskFileItem)cf.getFileItem(); 
	    File f = fi.getStoreLocation();
		Image img = null; 
		try {  
	        img = ImageIO.read(f);  
	        if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
	        	System.out.println("img:"+img);
	        	System.out.println("imgW:"+img.getWidth(null));
	        	System.out.println("imgH:"+img.getHeight(null));
	            return "";  
	        } else {
	        	//文件存储地址
//	    		Calendar cal = Calendar.getInstance();
//	    		cal.setTime(new Date());
//	    		int m = cal.get(Calendar.MONTH)+1;
	    		String fp = "upload"+File.separator + "cemeteryPhoto"+File.separator+"cemetery"; 
	    		//获得物理路径webapp所在路径 
	    		ServletContext context =getRequest().getSession().getServletContext();
	    		String filePath = context.getRealPath("");
	    		int i = 0;
	    		while (i < 3) {
	    			int lastFitst = filePath.lastIndexOf(File.separator);
	    			filePath = filePath.substring(0, lastFitst);
	    			i++;
	    		}
	    		filePath += File.separator+fp;
	    		File saveDirFile = new File(filePath);
	    		//检查文件夹是否存在，存在返回false，不存在返回true
	    		if (!saveDirFile.exists()) {
	    		saveDirFile.mkdirs();
	    		}
	    		//改名后的文件名
	            String nne="";
	            //相对路径
	            String xpath="";
	            //绝对路径
	            String rpath="";
	            if(!file.isEmpty()){  
	                //生成uuid作为文件名称  
	                String uuid = UUID.randomUUID().toString().replaceAll("-","");
//	                String name=getString("index");
	                //获得文件类型（可以判断如果不是图片，禁止上传）  
	                String contentType=file.getContentType();  
	                //获得文件后缀名称  
	                String imageName=contentType.substring(contentType.indexOf("/")+1);  
	                nne=""+uuid+"."+imageName;  
	                rpath=filePath+File.separator+nne;
	                xpath= fp+File.separator+nne;
	                file.transferTo(new File(rpath));  
	                System.out.println(rpath);
	                System.out.println(xpath);
	            }  
	            return xpath;
	        }
	    } catch (Exception e) { 
	    	e.printStackTrace();
	        return "";  
	    } finally {  
	        img = null;  
	    }  
    }
    /**
     * 保存陵园信息
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存陵园信息")
    @ResponseBody
    public JSONObject saveCemetery(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		Cemetery cemetery=new Cemetery();
    		String name=getString("lyname");
    		if (id!=null&&!id.equals("")) {
    			cemetery=cemeteryService.getCemeteryById(id);
    		}
    		cemetery.setName(name);
			cemetery.setAddress(getString("address"));
			cemetery.setIndex(getInt("index"));
			cemetery.setIsdel(getByte("isDel"));
			String photo=getString("filename");
			cemetery.setPhoto(photo);
    		User user=getCurrentUser();
    		if (id==null||id.equals("")) {
    			cemetery.setId(UuidUtil.get32UUID());
    			cemetery.setCreatePeople(user.getUserId());
    			cemetery.setCreateTime(DateTools.getThisDateTimestamp());
//    			if("".equals(photo) || photo==null){
//    				cemetery.setPhoto("cemeteryPhoto"+File.separator+"cemetery"+File.separator+name+".jpeg");
//    			}else{
//    			}
    			cemeteryService.saveCemetery(cemetery);
    		}else {
    			cemeteryService.updateCemetery(cemetery);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除陵园信息
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除陵园信息")
    @ResponseBody
    public JSONObject deleteCemetery(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    cemeteryService.deleteCemetery(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
	
}
