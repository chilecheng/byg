package com.hz.controller.cemetery.manage;

import java.awt.Image;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryArea;
import com.hz.entity.system.User;
import com.hz.service.cemetery.CemeteryAreaService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 陵园区域信息增删改查控制层
 * @author jgj
 */
@Controller
@RequestMapping("/cemeteryArea.do")
public class CemeteryAreaController extends BaseController {
	//老系统数据查询方法
	@Autowired
	private CemeteryAreaService cemeteryAreaService;
	@Autowired
	private CemeteryService cemeteryService;
	
	@ModelAttribute
	public void CemeteryAreaModel(Model model) {
		model.addAttribute("url", "cemeteryArea.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listCemeteryArea();
	}

	/**
	 * 陵园区域信息查询列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listCemeteryArea() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String type=getString("type");
		String show=getString("show");//页面展示 ，是否有陵园图纸页面1有 2没有
		String cemeteryId="";
		String cid=getString("cid");
		if(cid!=null && !"".equals(cid)){
			cemeteryId=cid;
		}else{
			cemeteryId=getString("cemeteryId");
		}
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("cemeteryId", cemeteryId);
		Cemetery cemetery=cemeteryService.getCemeteryById(cemeteryId);
		String isDelOption=Const.getIsdelOption(Const.Isdel_No, true);
		model.put("isDelOption", isDelOption);
		model.put("cemeteryId", cemeteryId);
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("method", "list");
		model.put("cemetery", cemetery);
		
		if("show".equals(type)){
			// 填入查询内容
			List<CemeteryArea> cemeteryAreaList=cemeteryAreaService.getCemeteryAreaList(maps, "");
			model.put("cemeteryAreaList", cemeteryAreaList);
			model.put("show",show );
			return new ModelAndView("cemetery/cemeteryArea/listCemeteryAreaSale", model);
		}else{
			// 获得页面页码信息
			int[] pages = getPage();
			PageInfo<CemeteryArea> page = cemeteryAreaService.getCemeteryAreaPageInfo(maps, pages[0], pages[1],"");
			model.put("page", page);
			return new ModelAndView("cemetery/cemeteryArea/listCemeteryArea", model);
		}
	}
	
    /**
	 * 添加或修改陵园信息
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView editCemeteryArea() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String id=getString("id");
		String cemeteryId=getString("cemeteryId");
		CemeteryArea cemeteryArea=cemeteryAreaService.getCemeteryAreaById(id);
		String isDelOption="";
		String sortOption="";
		if(cemeteryArea !=null){
			isDelOption=Const.getIsdelOption(cemeteryArea.getIsdel(), false);
			sortOption=Const.getSortOption(cemeteryArea.getSort(), false);
		}else{
			isDelOption=Const.getIsdelOption(Const.Isdel_No, false);
			sortOption=Const.getSortOption(Const.Isdel_No, false);
		}
		model.put("cemeteryArea", cemeteryArea);
		model.put("isDelOption", isDelOption);
		model.put("sortOption", sortOption);
		model.put("cemeteryId", cemeteryId);
		model.put("method", "save");
		model.put("url", "cemeteryArea.do");
		return new ModelAndView("cemetery/cemeteryArea/editCemeteryArea", model);
	}
	
	
	/**
	 * 图片上传
	 * @param file
	 * @return
	 * @throws Exception
	 * 
	 */
	@RequestMapping(params = "method=fileupload")  
	@SystemControllerLog(description = "公墓陵园区域图片上传")
	@ResponseBody
    public String fildUpload(@RequestParam(value="file",required=false) MultipartFile file)throws Exception{  
		CommonsMultipartFile cf= (CommonsMultipartFile)file; 
	    DiskFileItem fi = (DiskFileItem)cf.getFileItem(); 
	    File f = fi.getStoreLocation();
		Image img = null; 
		try {  
	        img = ImageIO.read(f);  
	        if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
	        	System.out.println("img:"+img);
	        	System.out.println("imgW:"+img.getWidth(null));
	        	System.out.println("imgH:"+img.getHeight(null));
	            return "";  
	        } else {
	        	//文件存储地址
//	    		Calendar cal = Calendar.getInstance();
//	    		cal.setTime(new Date());
//	    		int m = cal.get(Calendar.MONTH)+1;
	    		String fp = "upload"+File.separator + "cemeteryPhoto"+File.separator+"cemetery"+File.separator+"area"; 
	    		//获得物理路径webapp所在路径 
	    		ServletContext context =getRequest().getSession().getServletContext();
	    		String filePath = context.getRealPath("");
	    		int i = 0;
	    		while (i < 3) {
	    			int lastFitst = filePath.lastIndexOf(File.separator);
	    			filePath = filePath.substring(0, lastFitst);
	    			i++;
	    		}
	    		filePath += File.separator+fp;
	    		File saveDirFile = new File(filePath);
	    		//检查文件夹是否存在，存在返回false，不存在返回true
	    		if (!saveDirFile.exists()) {
	    		saveDirFile.mkdirs();
	    		}
	    		//改名后的文件名
	            String nne="";
	            //相对路径
	            String xpath="";
	            //绝对路径
	            String rpath="";
	            if(!file.isEmpty()){  
	                //生成uuid作为文件名称  
	                String uuid = UUID.randomUUID().toString().replaceAll("-","");
//	                String name=getString("index");
	                //获得文件类型（可以判断如果不是图片，禁止上传）  
	                String contentType=file.getContentType();  
	                //获得文件后缀名称  
	                String imageName=contentType.substring(contentType.indexOf("/")+1);  
	                nne=""+uuid+"."+imageName;  
	                rpath=filePath+File.separator+nne;
	                xpath= fp+File.separator+nne;
	                file.transferTo(new File(rpath));  
	                System.out.println(rpath);
	                System.out.println(xpath);
	            }  
	            return xpath;
	        }
	    } catch (Exception e) { 
	    	e.printStackTrace();
	        return "";  
	    } finally {  
	        img = null;  
	    }  
    }
    /**
     * 保存陵园信息
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存陵园区域信息")
    @ResponseBody
    public JSONObject saveCemeteryArea(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		CemeteryArea cemeteryArea=new CemeteryArea();
    		String name=getString("lyname");
    		String cemeteryId=getString("cemeteryId");
    		if (id!=null&&!id.equals("")) {
    			cemeteryArea=cemeteryAreaService.getCemeteryAreaById(id);
    		}
    		cemeteryArea.setCemeteryId(cemeteryId);
    		cemeteryArea.setName(name);
			cemeteryArea.setIndex(getInt("index"));
			cemeteryArea.setIsdel(getByte("isDel"));
			cemeteryArea.setSort(getByte("sort"));
			cemeteryArea.setComment(getString("comment"));
			String photo=getString("filename");
			cemeteryArea.setPhoto(photo);
    		User user=getCurrentUser();
    		if (id==null||id.equals("")) {
    			cemeteryArea.setId(UuidUtil.get32UUID());
    			cemeteryArea.setCreatePeople(user.getUserId());
    			cemeteryArea.setCreateTime(DateTools.getThisDateTimestamp());
//    			if("".equals(photo) || photo==null){
//    				cemetery.setPhoto("cemeteryPhoto"+File.separator+"cemetery"+File.separator+name+".jpeg");
//    			}else{
//    			}
    			cemeteryAreaService.saveCemeteryArea(cemeteryArea);
    		}else {
    			cemeteryAreaService.updateCemeteryArea(cemeteryArea);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除陵园信息
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除陵园信息")
    @ResponseBody
    public JSONObject deleteCemeteryArea(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    cemeteryAreaService.deleteCemeteryArea(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
	
}
