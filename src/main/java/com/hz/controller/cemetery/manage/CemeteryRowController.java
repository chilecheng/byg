package com.hz.controller.cemetery.manage;

import java.awt.Image;
import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryArea;
import com.hz.entity.cemetery.CemeteryRow;
import com.hz.entity.cemetery.CemeteryRowAndTomb;
import com.hz.entity.cemetery.Tomb;
import com.hz.service.cemetery.CemeteryAreaService;
import com.hz.service.cemetery.CemeteryRowService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 陵园排信息增删改查控制层
 *
 * @author jgj
 */
@Controller
@RequestMapping("/cemeteryRow.do")
public class CemeteryRowController extends BaseController {
    //老系统数据查询方法
    @Autowired
    private CemeteryRowService cemeteryRowService;
    @Autowired
    private CemeteryAreaService cemeteryAreaService;
    @Autowired
    private CemeteryService cemeteryService;

    @ModelAttribute
    public void CemeteryModel(Model model) {
        model.addAttribute("url", "cemeteryRow.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listCemeteryRow();
    }

    /**
     * 陵园排信息查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listCemeteryRow() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        String areaId = "";
        String aid = getString("aid");
        if (aid != null && !"".equals(aid)) {
            areaId = aid;
        } else {
            areaId = getString("areaId");
        }
        maps.put("areaId", areaId);
        PageInfo<CemeteryRow> page = cemeteryRowService.getCemeteryRowPageInfo(maps, pages[0], pages[1], "index_flag");
        String isDelOption = Const.getIsdelOption(Const.Isdel_No, true);
        model.put("method", "list");
        model.put("areaId", areaId);
        model.put("isDelOption", isDelOption);
        model.put("Isdel_No", Const.Isdel_No);
        model.put("Isdel_Yes", Const.Isdel_Yes);
        model.put("page", page);

        //地址链接使用
        String cemeteryId = getString("cemeteryId");
        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
        model.put("cemeteryArea", cemeteryArea);
        model.put("cemetery", cemetery);
        return new ModelAndView("cemetery/cemeteryRow/listCemeteryRow", model);
    }

    /**
     * 排、墓穴信息显示列表
     *
     * @return
     */
    @RequestMapping(params = "method=tomb")
    public ModelAndView listCemeteryTomb() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        String areaId = "";
        String aid = getString("aid");
        if (aid != null && !"".equals(aid)) {
            areaId = aid;
        } else {
            areaId = getString("areaId");
        }
        String cemeteryId = getString("cemeteryId");
        maps.put("areaId", areaId);
        maps.put("cid", cemeteryId);
        maps.put("isdel", Const.Isdel_No);
        List<CemeteryRowAndTomb> cemeteryRowList = cemeteryRowService.getRowAndTombList(maps, "index_flag desc");
        String isDelOption = Const.getIsdelOption(Const.Isdel_No, true);
        model.put("method", "list");
        model.put("areaId", areaId);
        model.put("isDelOption", isDelOption);
        model.put("Isdel_No", Const.Isdel_No);
        model.put("Isdel_Yes", Const.Isdel_Yes);
        model.put("cemeteryRowList", cemeteryRowList);
        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
        byte sort = 1;
        if (cemeteryArea != null) {
            sort = cemeteryArea.getSort();
            if (sort != 1) {//公墓从右往左排序
                int maxNum = 0;
                for (int i = 0; i < cemeteryRowList.size(); i++) {//循环拿排信息的最多墓穴数
                    int num = cemeteryRowList.get(i).getTomb().size();
                    if (num > maxNum) {
                        maxNum = num;
                    }
                }
                model.put("maxNum", maxNum);
//				List<CemeteryRowAndTomb> newList=new ArrayList<CemeteryRowAndTomb>();
                for (int i = 0; i < cemeteryRowList.size(); i++) {
                    Tomb tomb = new Tomb();
                    CemeteryRowAndTomb ce = cemeteryRowList.get(i);
                    List<Tomb> to = new ArrayList<Tomb>();
                    for (int j = maxNum - 1; j >= 0; j--) {
                        if (ce.getTomb().size() <= j) {
                            to.add(tomb);
                        } else {
                            to.add(ce.getTomb().get(j));
                        }
                    }
                    ce.setTomb(to);
                }
                model.put("cemeteryRowList2", cemeteryRowList);
            }
        }
        model.put("sort", sort);
        model.put("cemetery", cemetery);
        model.put("cemeteryArea", cemeteryArea);

        return new ModelAndView("cemetery/cemeteryRow/listCemeteryRowSale", model);
    }

    /**
     * 添加或修改陵园排信息
     *
     * @return
     */
    @RequestMapping(params = "method=edit")
    public ModelAndView editCemeteryRow() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String id = getString("id");
        CemeteryRow cemeteryRow = cemeteryRowService.getCemeteryRowById(id);
        String isDelOption = "";
        if (cemeteryRow != null) {
            isDelOption = Const.getIsdelOption(cemeteryRow.getIsdel(), false);
        } else {
            isDelOption = Const.getIsdelOption(Const.Isdel_No, false);
        }
        model.put("cemeteryRow", cemeteryRow);
        model.put("isDelOption", isDelOption);
        model.put("method", "save");
        model.put("url", "cemeteryRow.do");
        return new ModelAndView("cemetery/cemeteryRow/editCemeteryRow", model);
    }

    /**
     * 批量添加排信息
     *
     * @return
     */
    @RequestMapping(params = "method=batchAdd")
    public ModelAndView batchAddCemeteryRow() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String isDelOption = Const.getIsdelOption(Const.Isdel_No, false);

        model.put("isDelOption", isDelOption);
        model.put("method", "batchSave");
        model.put("url", "cemeteryRow.do");
        return new ModelAndView("cemetery/cemeteryRow/batchAddCemeteryRow", model);
    }

    /**
     * 批量保存陵园排信息
     *
     * @return
     */
    @SystemControllerLog(description = "批量保存陵园排信息")
    @ResponseBody
    @RequestMapping(params = "method=batchSave")
    public JSONObject batchSaveCemeteryRow() {
        JSONObject json = new JSONObject();
        try {
            int indexBegin = getInt("indexBegin");
            int indexEnd = getInt("indexEnd");
            String areaId = getString("areaId");
            String fileName = getString("filename");
            String comment = getString("comment");
            byte isdel = getByte("isDel");
            for (int i = indexBegin; i <= indexEnd; i++) {
                CemeteryRow cemeteryRow = new CemeteryRow();
                cemeteryRow.setAreaId(areaId);
                cemeteryRow.setId(UuidUtil.get32UUID());
                cemeteryRow.setName("第" + i + "排");
                cemeteryRow.setIndex(i);
                cemeteryRow.setPhoto(fileName);
                cemeteryRow.setRow(i + "");
                cemeteryRow.setComment(comment);
                cemeteryRow.setIsdel(isdel);
                cemeteryRowService.saveCemeteryRow(cemeteryRow);
            }
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 图片上传
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(params = "method=fileupload")
    @SystemControllerLog(description = "公墓陵园排图片上传")
    @ResponseBody
    public String fildUpload(@RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
        CommonsMultipartFile cf = (CommonsMultipartFile) file;
        DiskFileItem fi = (DiskFileItem) cf.getFileItem();
        File f = fi.getStoreLocation();
        Image img = null;
        try {
            img = ImageIO.read(f);
            if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
                System.out.println("img:" + img);
                System.out.println("imgW:" + img.getWidth(null));
                System.out.println("imgH:" + img.getHeight(null));
                return "";
            } else {
                //文件存储地址
//	    		Calendar cal = Calendar.getInstance();
//	    		cal.setTime(new Date());
//	    		int m = cal.get(Calendar.MONTH)+1;
                String fp = "upload" + File.separator + "cemeteryPhoto" + File.separator + "cemetery" + File.separator + "area" + File.separator + "row";
                //获得物理路径webapp所在路径
                ServletContext context = getRequest().getSession().getServletContext();
                String filePath = context.getRealPath("");
                int i = 0;
                while (i < 3) {
                    int lastFitst = filePath.lastIndexOf(File.separator);
                    filePath = filePath.substring(0, lastFitst);
                    i++;
                }
                filePath += File.separator + fp;
                File saveDirFile = new File(filePath);
                //检查文件夹是否存在，存在返回false，不存在返回true
                if (!saveDirFile.exists()) {
                    saveDirFile.mkdirs();
                }
                //改名后的文件名
                String nne = "";
                //相对路径
                String xpath = "";
                //绝对路径
                String rpath = "";
                if (!file.isEmpty()) {
                    //生成uuid作为文件名称
                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
//	                String name=getString("index");
                    //获得文件类型（可以判断如果不是图片，禁止上传）
                    String contentType = file.getContentType();
                    //获得文件后缀名称
                    String imageName = contentType.substring(contentType.indexOf("/") + 1);
                    nne = "" + uuid + "." + imageName;
                    rpath = filePath + File.separator + nne;
                    xpath = fp + File.separator + nne;
                    file.transferTo(new File(rpath));
                    System.out.println(rpath);
                    System.out.println(xpath);
                }
                return xpath;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            img = null;
        }
    }

    /**
     * 保存陵园信息
     *
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存陵园排信息")
    @ResponseBody
    public JSONObject saveCemeteryRow() {
        JSONObject json = new JSONObject();
        try {
            String id = getString("id");
            CemeteryRow cemeteryRow = new CemeteryRow();
            String name = getString("lyname");
            if (id != null && !id.equals("")) {
                cemeteryRow = cemeteryRowService.getCemeteryRowById(id);
            }
            cemeteryRow.setAreaId(getString("areaId"));
            cemeteryRow.setName(name);
            cemeteryRow.setComment(getString("comment"));
            cemeteryRow.setIndex(getInt("index"));
            cemeteryRow.setIsdel(getByte("isDel"));
            cemeteryRow.setRow(getString("index"));
            String photo = getString("filename");
            cemeteryRow.setPhoto(photo);
            if (id == null || id.equals("")) {
                cemeteryRow.setId(UuidUtil.get32UUID());
//    			if("".equals(photo) || photo==null){
//    				cemetery.setPhoto("cemeteryPhoto"+File.separator+"cemetery"+File.separator+name+".jpeg");
//    			}else{
//    			}
                cemeteryRowService.saveCemeteryRow(cemeteryRow);
            } else {
                cemeteryRowService.updateCemeteryRow(cemeteryRow);
            }
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 删除陵园排信息
     *
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除陵园排信息")
    @ResponseBody
    public JSONObject deleteCemeteryRow() {
        JSONObject json = new JSONObject();
        try {
            String id = getString("ids");
            cemeteryRowService.deleteCemeteryRow(id);
            setJsonBySuccess(json, "删除成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "删除失败，错误" + e.getMessage());
        }
        return json;
    }

}
