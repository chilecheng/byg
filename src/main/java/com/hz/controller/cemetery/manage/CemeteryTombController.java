package com.hz.controller.cemetery.manage;

import java.awt.Image;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import com.hz.util.DateTools;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryArea;
import com.hz.entity.cemetery.CemeteryRow;
import com.hz.entity.cemetery.Tomb;
import com.hz.service.cemetery.CemeteryAreaService;
import com.hz.service.cemetery.CemeteryRowService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.service.cemetery.CemeteryTombService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 陵园墓穴信息增删改查控制层
 *
 * @author jgj
 */
@Controller
@RequestMapping("/cemeteryTomb.do")
public class CemeteryTombController extends BaseController {
    //老系统数据查询方法
    @Autowired
    private CemeteryTombService cemeteryTombService;
    @Autowired
    private CemeteryService cemeteryService;
    @Autowired
    private CemeteryAreaService cemeteryAreaService;
    @Autowired
    private CemeteryRowService cemeteryRowService;

    @ModelAttribute
    public void CemeteryModel(Model model) {
        model.addAttribute("url", "cemeteryTomb.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listCemeteryTomb();
    }

    /**
     * 陵园墓穴信息查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listCemeteryTomb() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        String rowId = "";
        String rid = getString("rid");
        if (rid != null && !"".equals(rid)) {
            rowId = rid;
        } else {
            rowId = getString("rowId");
        }
        maps.put("rowId", rowId);
        String isDelOption = Const.getIsdelOption(Const.Isdel_No, true);
        PageInfo<Tomb> page = cemeteryTombService.getCemeteryTombPageInfo(maps, pages[0], pages[1], "index_flag");
        model.put("page", page);
        model.put("method", "list");
        model.put("rowId", rowId);
        model.put("isDelOption", isDelOption);
        model.put("Isdel_No", Const.Isdel_No);
        model.put("Isdel_Yes", Const.Isdel_Yes);
        //地址返回链接使用
        String cemeteryId = getString("cemeteryId");
        String areaId = getString("areaId");
        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
        CemeteryRow cemeteryRow = cemeteryRowService.getCemeteryRowById(rowId);
        model.put("cemetery", cemetery);
        model.put("cemeteryArea", cemeteryArea);
        model.put("cemeteryRow", cemeteryRow);
        return new ModelAndView("cemetery/cemeteryTomb/listCemeteryTomb", model);
    }

    /**
     * 添加或修改陵园墓穴信息
     *
     * @return
     */
    @RequestMapping(params = "method=edit")
    public ModelAndView editCemeteryTomb() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String id = getString("id");
        Tomb cemeteryTomb = cemeteryTombService.getCemeteryTombById(id);
        String isDelOption = "";
        if (cemeteryTomb != null) {
            isDelOption = Const.getIsdelOption(cemeteryTomb.getIsdel(), false);
        } else {
            isDelOption = Const.getIsdelOption(Const.Isdel_No, false);
        }
        model.put("cemeteryTomb", cemeteryTomb);
        model.put("isDelOption", isDelOption);
        model.put("method", "save");
        model.put("url", "cemeteryTomb.do");
        model.put("cemeteryId", getString("cemeteryId"));
        model.put("areaId", getString("areaId"));
        model.put("rowId", getString("rowId"));
        return new ModelAndView("cemetery/cemeteryTomb/editCemeteryTomb", model);
    }

    /**
     * 批量添加墓穴信息
     *
     * @return
     */
    @RequestMapping(params = "method=batchAdd")
    public ModelAndView batchAddCemeteryTomb() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String isDelOption = Const.getIsdelOption(Const.Isdel_No, false);
        model.put("cemeteryId", getString("cemeteryId"));
        model.put("areaId", getString("areaId"));
        model.put("rowId", getString("rowId"));
        model.put("isDelOption", isDelOption);
        model.put("method", "batchSave");
        model.put("url", "cemeteryTomb.do");
        return new ModelAndView("cemetery/cemeteryTomb/batchAddCemeteryTomb", model);
    }

    /**
     * 批量保存 墓穴信息
     *
     * @return
     */
    @SystemControllerLog(description = "批量保存墓穴信息")
    @ResponseBody
    @RequestMapping(params = "method=batchSave")
    public JSONObject batchSaveCemeteryTomb() {
        JSONObject json = new JSONObject();
        try {
            String rowId = getString("rowId");
            String areaId = getString("areaId");
            String cemeteryId = getString("cemeteryId");
            CemeteryRow cemeteryRow = cemeteryRowService.getCemeteryRowById(rowId);
            CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
            Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
            String comment = getString("comment");
            String fileName = getString("filename");
            Double maintenance = getDouble("maintenance");
            String price = getString("price");
            Byte isdel = getByte("isDel");
            int indexBegin = getInt("indexBegin");
            int indexEnd = getInt("indexEnd");
            if (indexBegin < indexEnd) {
                for (int i = indexBegin; i <= indexEnd; i++) {
                    Tomb tomb = new Tomb();
                    tomb.setId(UuidUtil.get32UUID());
                    tomb.setName(cemetery.getName() + cemeteryArea.getName() + cemeteryRow.getName() + i + "号");
                    tomb.setMaintenance(maintenance);
                    tomb.setPrice(DataTools.stringToDouble(price));
                    tomb.setRowId(rowId);
                    tomb.setPhoto(fileName);
                    tomb.setIndex(i);
                    tomb.setIsdel(isdel);
                    tomb.setComment(comment);
                    tomb.setState(Const.GM_KONG);
                    tomb.setRowName(cemeteryRow.getName());
                    tomb.setOpertor(getCurrentUser().getName());
                    tomb.setAreaId(areaId);
                    tomb.setcId(cemeteryId);
                    tomb.setCreatime(DateTools.getThisDate());
                    cemeteryTombService.saveCemeteryTomb(tomb);
                }
            } else {
                for (int i = indexBegin; i >= indexEnd; i--) {
                    Tomb tomb = new Tomb();
                    tomb.setId(UuidUtil.get32UUID());
                    tomb.setName(cemetery.getName() + cemeteryArea.getName() + cemeteryRow.getName() + i + "号");
                    tomb.setMaintenance(maintenance);
                    tomb.setPrice(DataTools.stringToDouble(price));
                    tomb.setRowId(rowId);
                    tomb.setPhoto(fileName);
                    tomb.setIndex(i);
                    tomb.setIsdel(isdel);
                    tomb.setComment(comment);
                    tomb.setState(Const.GM_KONG);
                    tomb.setRowName(cemeteryRow.getName());
                    tomb.setOpertor(getCurrentUser().getName());
                    tomb.setAreaId(areaId);
                    tomb.setcId(cemeteryId);
                    tomb.setCreatime(DateTools.getThisDate());
                    cemeteryTombService.saveCemeteryTomb(tomb);
                }
            }
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 图片上传
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(params = "method=fileupload")
    @SystemControllerLog(description = "公墓陵园墓穴图片上传")
    @ResponseBody
    public String fildUpload(@RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
        CommonsMultipartFile cf = (CommonsMultipartFile) file;
        DiskFileItem fi = (DiskFileItem) cf.getFileItem();
        File f = fi.getStoreLocation();
        Image img = null;
        try {
            img = ImageIO.read(f);
            if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
                System.out.println("img:" + img);
                System.out.println("imgW:" + img.getWidth(null));
                System.out.println("imgH:" + img.getHeight(null));
                return "";
            } else {
                //文件存储地址
//	    		Calendar cal = Calendar.getInstance();
//	    		cal.setTime(new Date());
//	    		int m = cal.get(Calendar.MONTH)+1;
                String fp = "upload" + File.separator + "cemeteryPhoto" + File.separator + "cemetery" + File.separator + "area" + File.separator + "row" + File.separator + "tomb";
                //获得物理路径webapp所在路径
                ServletContext context = getRequest().getSession().getServletContext();
                String filePath = context.getRealPath("");
                int i = 0;
                while (i < 3) {
                    int lastFitst = filePath.lastIndexOf(File.separator);
                    filePath = filePath.substring(0, lastFitst);
                    i++;
                }
                filePath += File.separator + fp;
                File saveDirFile = new File(filePath);
                //检查文件夹是否存在，存在返回false，不存在返回true
                if (!saveDirFile.exists()) {
                    saveDirFile.mkdirs();
                }
                //改名后的文件名
                String nne = "";
                //相对路径
                String xpath = "";
                //绝对路径
                String rpath = "";
                if (!file.isEmpty()) {
                    //生成uuid作为文件名称
                    String uuid = UUID.randomUUID().toString().replaceAll("-", "");
//	                String name=getString("index");
                    //获得文件类型（可以判断如果不是图片，禁止上传）
                    String contentType = file.getContentType();
                    //获得文件后缀名称
                    String imageName = contentType.substring(contentType.indexOf("/") + 1);
                    nne = "" + uuid + "." + imageName;
                    rpath = filePath + File.separator + nne;
                    xpath = fp + File.separator + nne;
                    file.transferTo(new File(rpath));
                    System.out.println(rpath);
                    System.out.println(xpath);
                }
                return xpath;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            img = null;
        }
    }

    /**
     * 保存陵园信息
     *
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存陵园墓穴信息")
    @ResponseBody
    public JSONObject saveCemeteryTomb() {
        JSONObject json = new JSONObject();
        try {
            String id = getString("id");
            Tomb cemeteryTomb = new Tomb();
            String name = getString("lyname");
            if (id != null && !id.equals("")) {
                cemeteryTomb = cemeteryTombService.getCemeteryTombById(id);
            }
            String rowId = getString("rowId");
            String areaId = getString("areaId");
            String cemeteryId = getString("cemeteryId");
            CemeteryRow cemeteryRow = cemeteryRowService.getCemeteryRowById(rowId);
            CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
            Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
            cemeteryTomb.setRowId(rowId);
            cemeteryTomb.setName(name);
            cemeteryTomb.setPrice(getDouble("price"));
            cemeteryTomb.setIndex(getInt("index"));
            cemeteryTomb.setIsdel(getByte("isDel"));
            cemeteryTomb.setComment(getString("comment"));
            cemeteryTomb.setMaintenance(getDouble("maintenance"));
            cemeteryTomb.setRowName(cemeteryRow.getName());
            cemeteryTomb.setOpertor(getCurrentUser().getName());
            cemeteryTomb.setAreaId(areaId);
            cemeteryTomb.setcId(cemeteryId);
            cemeteryTomb.setCreatime(DateTools.getThisDate());
            String photo = getString("filename");
            cemeteryTomb.setPhoto(photo);
            if (id == null || id.equals("")) {
                cemeteryTomb.setId(UuidUtil.get32UUID());
                cemeteryTomb.setState(Const.GM_KONG);
                cemeteryTomb.setName(cemetery.getName() + cemeteryArea.getName() + cemeteryRow.getName() + name);
//    			if("".equals(photo) || photo==null){
//    				cemetery.setPhoto("cemeteryPhoto"+File.separator+"cemetery"+File.separator+name+".jpeg");
//    			}else{
//    			}
                cemeteryTombService.saveCemeteryTomb(cemeteryTomb);
            } else {
                cemeteryTombService.updateCemeteryTomb(cemeteryTomb);
            }
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 删除陵园墓穴信息
     *
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除陵园墓穴信息")
    @ResponseBody
    public JSONObject deleteCemetery墓穴() {
        JSONObject json = new JSONObject();
        try {
            String id = getString("ids");
            cemeteryTombService.deleteCemeteryTomb(id);
            setJsonBySuccess(json, "删除成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "删除失败，错误" + e.getMessage());
        }
        return json;
    }

}
