package com.hz.controller.cemetery.record;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.MaintenancePaymentedRecord;
import com.hz.entity.cemetery.PayRecord;
import com.hz.entity.cemetery.PaymentedRecord;
import com.hz.service.cemetery.record.MaintenancePaymentRecordService;
import com.hz.service.cemetery.record.PayRecordService;
import com.hz.service.cemetery.record.PaymentedRecordService;
import com.hz.util.Const;

/**
 * 缴费记录 :墓穴购买缴费记录/墓穴维护缴费记录/墓穴雕刻安放费
 *
 * @author ethan
 */
@Controller
@RequestMapping("/paymentedRecord.do")
public class PaymentedRecordController extends BaseController {

    @Autowired
    private PaymentedRecordService paymentedRecordService;
    @Autowired
    private MaintenancePaymentRecordService maintenancePaymentRecordService;
    @Autowired
    private PayRecordService payRecordService;

    @ModelAttribute
    public void CemeteryModel(Model model) {
        model.addAttribute("url", "paymentedRecord.do");
    }

//	@RequestMapping
//	public ModelAndView unspecified() {
//		return listCemetery();
//	}

    /**
     * 查询 墓穴缴费记录
     *
     * @return
     */
    @RequestMapping(params = "method=cemeteryBuyedRecord")
    public ModelAndView Cemeteryrecord() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        String searchType = getString("searchType");
        String searchVal = getString("searchVal");
        maps.put("searchType", searchType);
        maps.put("searchVal", searchVal);
        String searchOption = Const.getSearchTypeOptionByString(searchType, new byte[]{Const.Search_Type_Name, Const.Search_Type_InvoiceNumber});
        PageInfo<PaymentedRecord> page = paymentedRecordService.getCemeteryrecord(maps, pages[0], pages[1], "name asc");
        model.put("method", "list");
        model.put("page", page);
        model.put("searchOption", searchOption);
        return new ModelAndView("cemetery/paymentRecord/cemeterypaymentRecord", model);
    }

    /**
     * 查询 墓穴维护缴费记录
     *
     * @return
     */
    @RequestMapping(params = "method=maintenancePaymentRecord")
    public ModelAndView MaintenancePayment() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        String searchType = getString("searchType");
        String searchVal = getString("searchVal");
        maps.put("searchType", searchType);
        maps.put("searchVal", searchVal);
        String searchOption = Const.getSearchTypeOptionByString(searchType, new byte[]{Const.Search_Type_Name, Const.Search_Type_InvoiceNumber});
        PageInfo<MaintenancePaymentedRecord> page = maintenancePaymentRecordService.getmaintenancePaymentRecord(maps, pages[0], pages[1], "id asc");
        model.put("method", "list");
        model.put("page", page);
        model.put("searchOption", searchOption);
        return new ModelAndView("cemetery/paymentRecord/maintenancePaymentRecord", model);
    }

    /**
     * 查询 墓穴雕刻安放费/雕刻费/迁移费
     *
     * @return
     */
    @RequestMapping(params = "method=payRecord")
    public ModelAndView Carveandplace() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        Byte type = getByte("type");
        switch (type) {
            case 1:
                maps.put("type", Const.GMCHARGE_DKAFF);
                break;
            case 4:
                maps.put("type", Const.GMCHARGE_QYF);
                break;
            case 5:
                maps.put("type", Const.GMCHARGE_DKF);
                break;

        }
        String searchType = getString("searchType");
        String searchVal = getString("searchVal");
        maps.put("type", type);
        maps.put("searchType", searchType);
        maps.put("searchVal", searchVal);
        String searchOption = Const.getSearchTypeOptionByString(searchType, new byte[]{Const.Search_Type_Name, Const.Search_Type_InvoiceNumber});
		/*maps.put("payerName", payerName);
		maps.put("invoiceNmuber", invoiceNmuber);*/
		PageInfo<PayRecord> page=payRecordService.getPayRecordPageInfo(maps, pages[0], pages[1],"id asc");
		model.put("method", "list");
		model.put("page", page);
		model.put("type", type);
		model.put("searchOption", searchOption);
		return new ModelAndView("cemetery/paymentRecord/payRecord", model);
		}
	/**
	 * 查询 墓穴雕刻安放费/雕刻费/迁移费
	 * @return list
	 *//*

	@RequestMapping(params = "method=list")
	public ModelAndView listCemetery() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		List<PayRecord> list=payRecordService.getCemeteryList(null, "");
		model.put("method", "list");
		model.put("cemetery", list);
		return new ModelAndView("cemetery/cemeteryInterface/cemeteryInterface", model);
	}*/
}
