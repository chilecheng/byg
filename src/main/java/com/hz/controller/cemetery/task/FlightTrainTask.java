package com.hz.controller.cemetery.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Classname FlightTrainTask
 * @Description TODO
 * @Date 2019/7/23 13:59
 * @Created by ZhixiangWang
 */
@Component
public class FlightTrainTask {
    @Scheduled(cron = "0 30 9 ? * *") // 每天9:30执行一次。
    public void taskCycle() {
        System.out.println("使用SpringMVC框架配置定时任务");
    }
}
