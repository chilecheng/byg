package com.hz.controller.cemetery.sale;

import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryArea;
import com.hz.service.cemetery.CemeteryAreaService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 未售墓穴信息查询控制层
 *
 * @ClassName NotSaleTombController
 * @Description TODO
 * @Author ZhixiangWang
 * @Date 2019/6/27 14:11
 * @Version 1.0
 */
@Controller
@RequestMapping("/notSaleTomb.do")
public class NotSaleTombController extends BaseController {

    @Autowired
    private CemeteryService cemeteryService;

    @Autowired
    private CemeteryAreaService cemeteryAreaService;

    @ModelAttribute
    public void notSaleTombModel(Model model) {
        model.addAttribute("url", "notSaleTomb.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listNotSaleTomb();
    }

    /**
     * 未售墓穴信息查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listNotSaleTomb() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 填入陵园查询内容
        Map<String, Object> cemeteryMaps = new HashMap<String, Object>(16);
        // 状态为启用
        cemeteryMaps.put("isdel", Const.Isdel_No);
        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
        model.put("cemeteryList", cemeteryList);
        //获取选中陵园ID
        String cemeteryId = getString("selectCemetery");
        if (cemeteryId == null || "".equals(cemeteryId)) {
            cemeteryId = cemeteryList.get(0).getId();
        }
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        maps.put("cemeteryId", cemeteryId);
        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "index_flag asc");
        model.put("method", "list");
        model.put("cemeteryAreaList", cemeteryAreaList);
        model.put("selectCemetery", cemeteryId);
        return new ModelAndView("cemetery/cemetery/sale/notSaleTomb", model);
    }


}
