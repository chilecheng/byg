package com.hz.controller.cemetery.sale;

import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.*;
import com.hz.service.cemetery.CemeteryAreaService;
import com.hz.service.cemetery.CemeteryRowService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * 墓区查看控制层
 *
 * @Classname TombAreaViewController
 * @Description TODO
 * @Date 2019/7/2 16:01
 * @Created by ZhixiangWang
 */
@Controller
@RequestMapping("/tombAreaView.do")
public class TombAreaViewController extends BaseController {

    @Autowired
    private CemeteryService cemeteryService;

    @Autowired
    private CemeteryAreaService cemeteryAreaService;

    @Autowired
    private CemeteryRowService cemeteryRowService;

    @ModelAttribute
    public void TombAreaViewModel(Model model) {
        model.addAttribute("url", "tombAreaView.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listTombAreaView();
    }

    /**
     * 墓穴查看视图
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listTombAreaView() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 填入陵园查询内容
        Map<String, Object> cemeteryMaps = new HashMap<String, Object>();
        // 状态为启用
        cemeteryMaps.put("isdel", Const.Isdel_No);
        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
        //获取选中陵园ID
        String cemeteryId = getString("selectCemetery");
        List<Object[]> list = new ArrayList<Object[]>();
        for (Cemetery cemetery : cemeteryList) {
            list.add(new Object[]{cemetery.getId(), cemetery.getName()});
        }
        String selectCemeteryOption = "";
        if (cemeteryId == null || "".equals(cemeteryId)) {
            cemeteryId = cemeteryList.get(0).getId();
        }
        // 填入墓区查询内容
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("cemeteryId", cemeteryId);
        selectCemeteryOption = DataTools.getOptionByList(list, cemeteryId, false);
        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "index_flag asc");
        //获取选中区域ID
        String areaId = getString("selectArea");
        List<Object[]> areaList = new ArrayList<Object[]>();
        for (CemeteryArea cemeteryArea : cemeteryAreaList) {
            areaList.add(new Object[]{cemeteryArea.getId(), cemeteryArea.getName()});
        }
        String selectAreaOption = "";
        //获取选中陵园(selectCemeteryChange)或陵区(selectAreaChange,有排选择时用)是否改变
        String type = getString("type");
        if (cemeteryAreaList.size() < 1) {
            areaId = "selectCemeteryChange";
        } else {
            if ((areaId == null || "".equals(areaId)) || type.equals("selectCemeteryChange")) {
                areaId = cemeteryAreaList.get(0).getId();
            }
        }
        selectAreaOption = DataTools.getOptionByList(areaList, areaId, false);
        model.put("method", "list");
        model.put("cemeteryId", cemeteryId);
        model.put("areaId", areaId);
        model.put("selectCemeteryOption", selectCemeteryOption);
        model.put("selectAreaOption", selectAreaOption);
        String entries = getString("entries");
        List<CemeteryRowAndTomb> cemeteryRowList = new ArrayList<CemeteryRowAndTomb>();
        if ("".equals(entries) || entries == null) {
            // 填入排查询内容
            Map<String, Object> rowMap = new HashMap<String, Object>(16);
            rowMap.put("areaId", areaId);
            rowMap.put("cid", cemeteryId);
            rowMap.put("isdel", Const.Isdel_No);
            cemeteryRowList = cemeteryRowService.getRowAndTombList(rowMap, "index_flag desc");
            model.put("cemeteryRowList", cemeteryRowList);
        }
        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
        //雪山公墓华侨墓区（有通道）
        if (cemeteryArea != null && cemeteryArea.getId().equals("CAID0038")) {
            byte aisle = 1;
            model.put("aisle", aisle);
        }
        //雪山公墓莲花芯墓区和凤凰山墓区（有排选择）
        if (cemeteryArea != null && (cemeteryArea.getId().equals("CAID0037") || cemeteryArea.getId().equals("CAID0039"))) {
            //获取选中排ID
            String rowId = getString("selectRow");
            if (cemeteryRowList.size() < 1) {
                rowId = "selectAreaChange";
            } else {
                if ((rowId == null || "".equals(rowId)) || type.equals("selectAreaChange")) {
                    rowId = cemeteryRowList.get(cemeteryRowList.size() - 1).getId();
                }
            }
            // 填入墓穴查询内容
            Map<String, Object> tombMap = new HashMap<String, Object>();
            tombMap.put("rowId", rowId);
            List<Object[]> rowList = new ArrayList<Object[]>();
            String rowName = "";
            List<Tomb> tomb = new ArrayList<Tomb>();
            for (CemeteryRowAndTomb cemeteryRowAndTomb : cemeteryRowList) {
                rowList.add(new Object[]{cemeteryRowAndTomb.getId(), cemeteryRowAndTomb.getName()});
                if (cemeteryRowAndTomb.getId().equals(rowId)) {
                    rowName = cemeteryRowAndTomb.getName();
                    tomb = cemeteryRowAndTomb.getTomb();
                }
            }
            //凤凰山墓区下拉选择南排合并（南1-5排，南6-9排，南11-14排，南15-18排，南10排不用合并）
            if (areaId.equals("CAID0039")) {
                for (int i = rowList.size() - 1; i >= 0; i--) {
                    if (i > 0 && i < 19 && i != 9) {
                        rowList.remove(i);
                    }
                }
                rowList.add(1, new Object[]{446, "南15-18排"});
                rowList.add(2, new Object[]{442, "南11-14排"});
                rowList.add(4, new Object[]{437, "南6-9排"});
                rowList.add(5, new Object[]{432, "南1-5排"});
            }
            Map<String, String> specialRowMap = new HashMap<String, String>();
            specialRowMap.put("701", "19排");
            specialRowMap.put("422", "20排");
            specialRowMap.put("423", "21排");
            specialRowMap.put("424", "22排");
            specialRowMap.put("425", "23排");
            specialRowMap.put("426", "24排");
            specialRowMap.put("427", "25排");
            specialRowMap.put("428", "26排");
            specialRowMap.put("429", "27排");
            specialRowMap.put("430", "28排");
            specialRowMap.put("431", "29排");
            //凤凰山墓区19-29排（没有通道）
            for (String key : specialRowMap.keySet()) {
                if (areaId.equals("CAID0039") && key.equals(rowId)) {
                    model.put("specialRow", "NineteenToTwenty-nineRows");
                }
            }
            //凤凰山墓区30排（有通道，但通道两边不是按左偶右奇布局）
            if (areaId.equals("CAID0039") && rowId.equals("791")) {
                model.put("specialRow", "ThirtyRows");
            } else if (areaId.equals("CAID0039") && rowId.equals("692")) {
                //凤凰山墓区追思亭（特殊布局）
                model.put("specialRow", "ChasingPavilion");
            } else if (areaId.equals("CAID0039") && rowId.equals("432")) {
                //凤凰山墓区南1-5排
                for (int i = cemeteryRowList.size() - 1; i >= 0; i--) {
                    if (i < 14 || i > 18) {
                        cemeteryRowList.remove(i);
                    }
                }
                //倒序
                Collections.reverse(cemeteryRowList);
                model.put("cemeteryRowList1", cemeteryRowList);
                model.put("specialRow", "SouthOneToFiveRows");
            } else if (areaId.equals("CAID0039") && rowId.equals("437")) {
                //凤凰山墓区南6-9排
                for (int i = cemeteryRowList.size() - 1; i >= 0; i--) {
                    if (i < 10 || i > 13) {
                        cemeteryRowList.remove(i);
                    }
                }
                //倒序
                Collections.reverse(cemeteryRowList);
                model.put("cemeteryRowList1", cemeteryRowList);
                model.put("specialRow", "SouthSixToNineRows");
            } else if (areaId.equals("CAID0039") && rowId.equals("442")) {
                //凤凰山墓区南11-14排
                for (int i = cemeteryRowList.size() - 1; i >= 0; i--) {
                    if (i < 5 || i > 8) {
                        cemeteryRowList.remove(i);
                    }
                }
                //倒序
                Collections.reverse(cemeteryRowList);
                model.put("cemeteryRowList1", cemeteryRowList);
                model.put("specialRow", "SouthElevenToFourteenRows");
            } else if (areaId.equals("CAID0039") && rowId.equals("446")) {
                //凤凰山墓区南15-18排
                for (int i = cemeteryRowList.size() - 1; i >= 0; i--) {
                    if (i < 1 || i > 4) {
                        cemeteryRowList.remove(i);
                    }
                }
                //倒序
                Collections.reverse(cemeteryRowList);
                model.put("cemeteryRowList1", cemeteryRowList);
                model.put("specialRow", "SouthFifteenToEighteen");
            } else if (areaId.equals("CAID0037") && rowId.equals("786")) {
                //莲花芯墓区思远亭（特殊布局）
                model.put("specialRow", "SiyuanPavilion");
            } else if (areaId.equals("CAID0037") && (rowId.equals("782") || rowId.equals("783") || rowId.equals("784") || rowId.equals("785"))) {
                //莲花芯墓区的排中排
                Map<Integer, List<Tomb>> map = new HashMap<>();
                //将数据库row2字段相同的数据进行合并
                for (Tomb tomb1 : tomb) {
                    Integer key = tomb1.getRow2();
                    if (map.containsKey(key)) {
                        map.get(key).add(tomb1);
                    } else {
                        List<Tomb> tomb1List = new ArrayList<>();
                        tomb1List.add(tomb1);
                        map.put(key, tomb1List);
                    }
                }
                //莲花芯墓区东围墙朝西公墓和西围墙朝东公墓（没通道）
                if (rowId.equals("782") || rowId.equals("783")) {
                    model.put("specialRow", "EastAndWestWallCemetery");
                    //倒序
                    Map<Integer, List<Tomb>> tMapRever = new TreeMap<>(Collections.reverseOrder());
                    tMapRever.putAll(map);
                    model.put("tombMap", tMapRever);
                } else if (rowId.equals("784")) {
                    //莲花芯墓区中路（有通道，但通道两边按东西分）
                    model.put("specialRow", "MiddleRoad");
                    //倒序
                    Map<Integer, List<Tomb>> tMapRever = new TreeMap<>();
                    tMapRever.putAll(map);
                    model.put("tombMap", tMapRever);
                } else {
                    //莲花芯墓区中排（有通道）
                    model.put("specialRow", "MiddleRow");
                    model.put("tombMap", map);
                }
            }
            //下拉排选择倒序
            Collections.reverse(rowList);
            String selectRowOption = DataTools.getOptionByList(rowList, rowId, false);
            model.put("selectRowOption", selectRowOption);
            model.put("rowName", rowName);
            model.put("tomb", tomb);
        }
        byte sort = 1;
        if (cemeteryArea != null) {
            sort = cemeteryArea.getSort();
            int maxNum = 0;
            int total = 0;
            for (int i = 0; i < cemeteryRowList.size(); i++) {//循环拿排信息的最多墓穴数
                int num = cemeteryRowList.get(i).getTomb().size();
                total += num;
                if (num > maxNum) {
                    maxNum = num;
                }
            }
            if (!cemeteryArea.getId().equals("CAID0037") || !cemeteryArea.getId().equals("CAID0039")) {
                model.put("total", total);
            }
            model.put("maxNum", maxNum);
            if (sort != 1) {//公墓从右往左排序
                for (int i = 0; i < cemeteryRowList.size(); i++) {
                    Tomb tomb = new Tomb();
                    CemeteryRowAndTomb ce = cemeteryRowList.get(i);
                    List<Tomb> to = new ArrayList<Tomb>();
                    for (int j = maxNum - 1; j >= 0; j--) {
                        if (ce.getTomb().size() <= j) {
                            to.add(tomb);
                        } else {
                            to.add(ce.getTomb().get(j));
                        }
                    }
                    ce.setTomb(to);
                }
                model.put("cemeteryRowList2", cemeteryRowList);
            }
        }
        model.put("sort", sort);
        model.put("cemetery", cemetery);
        model.put("cemeteryArea", cemeteryArea);
        return new ModelAndView("cemetery/cemetery/sale/tombAreaView", model);
    }
}
