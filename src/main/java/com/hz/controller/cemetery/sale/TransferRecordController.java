package com.hz.controller.cemetery.sale;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.AppointmentRecord;
import com.hz.entity.cemetery.TransferRecord;
import com.hz.service.cemetery.TransferRecordService;
import com.hz.util.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 墓穴迁出记录增删改查控制层
 *
 * @Classname TransferRecordController
 * @Description TODO
 * @Date 2019/7/17 17:21
 * @Created by ZhixiangWang
 */
@Controller
@RequestMapping("/transferRecord.do")
public class TransferRecordController extends BaseController {
    @Autowired
    private TransferRecordService transferRecordService;

    @ModelAttribute
    public void TransferRecordModel(Model model) {
        model.addAttribute("url", "transferRecord.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listTransferRecord();
    }

    /**
     * 墓穴迁出记录查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listTransferRecord() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        String searchType = getString("searchType");
        String searchVal = getString("searchVal");
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        maps.put("searchType", searchType);
        maps.put("searchVal", searchVal);
        PageInfo<TransferRecord> page = transferRecordService.getTransferRecordPageInfo(maps, pages[0], pages[1], "");
        model.put("IsHave_Yes", Const.IsHave_Yes);
        model.put("IsHave_No", Const.IsHave_No);
        model.put("page", page);
        model.put("method", "list");
        model.put("searchType", searchType);
        return new ModelAndView("cemetery/cemetery/sale/transferRecord", model);
    }

}
