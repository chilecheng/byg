package com.hz.controller.cemetery.sale;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.AppointmentRecord;
import com.hz.service.cemetery.AppointmentRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 墓穴预约记录增删改查控制层
 *
 * @Classname AppointmentRecordController
 * @Description TODO
 * @Date 2019/7/17 16:26
 * @Created by ZhixiangWang
 */
@Controller
@RequestMapping("/appointmentRecord.do")
public class AppointmentRecordController extends BaseController {
    @Autowired
    private AppointmentRecordService appointmentRecordService;

    @ModelAttribute
    public void AppointmentRecordModel(Model model) {
        model.addAttribute("url", "appointmentRecord.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listAppointmentRecord();
    }

    /**
     * 墓穴预约记录查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listAppointmentRecord() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        //首次进入标志
        String entries = getString("entries");
        String searchType = getString("searchType");
        String searchCondition = getString("searchCondition");
        String searchVal = getString("searchVal");
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        maps.put("searchType", searchType);
        maps.put("searchCondition", searchCondition);
        maps.put("searchVal", searchVal);
        PageInfo<AppointmentRecord> page = appointmentRecordService.getAllAppointmentRecordPageInfo(maps, pages[0], pages[1], "");
        model.put("page", page);
        model.put("method", "list");
        model.put("entries", entries);
        model.put("searchType", searchType);
        model.put("searchCondition", searchCondition);
        return new ModelAndView("cemetery/cemetery/sale/appointmentRecord", model);
    }

}
