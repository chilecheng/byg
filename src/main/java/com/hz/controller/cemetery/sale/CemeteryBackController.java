package com.hz.controller.cemetery.sale;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.CemeteryBack;
import com.hz.service.cemetery.CemeteryBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 已退墓穴记录增删改查控制层
 *
 * @Classname CemeteryBackController
 * @Description TODO
 * @Date 2019/6/27 19:47
 * @Created by ZhixiangWang
 */
@Controller
@RequestMapping("/cemeteryBack.do")
public class CemeteryBackController extends BaseController {
    @Autowired
    private CemeteryBackService cemeteryBackService;

    @ModelAttribute
    public void CemeteryBackModel(Model model) {
        model.addAttribute("url", "cemeteryBack.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listCemeteryBack();
    }

    /**
     * 已退墓穴记录查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listCemeteryBack() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        String searchType = getString("searchType");
        String searchVal = getString("searchVal");
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        maps.put("searchType", searchType);
        maps.put("searchVal", searchVal);
        PageInfo<CemeteryBack> page = cemeteryBackService.getCemeteryBackPageInfo(maps, pages[0], pages[1], "");
        model.put("page", page);
        model.put("method", "list");
        model.put("searchType", searchType);
        return new ModelAndView("cemetery/cemetery/sale/cemeteryBack", model);
    }
}
