package com.hz.controller.cemetery.sale;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.BuyRecord;
import com.hz.entity.cemetery.Cemetery;
import com.hz.service.cemetery.BuyRecordService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.util.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 墓穴购买记录增删改查控制层
 *
 * @ClassName BuyRecordController
 * @Description TODO
 * @Author ZhixiangWang
 * @Date 2019/7/17 16:59
 * @Version 1.0
 */
@Controller
@RequestMapping("buyRecord.do")
public class BuyRecordController extends BaseController {
    @Autowired
    private BuyRecordService buyRecordService;
    @Autowired
    private CemeteryService cemeteryService;

    @ModelAttribute
    public void buyRecordModel(Model model) {
        model.addAttribute("url", "buyRecord.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listBuyRecord();
    }

    /**
     * 墓穴购买记录查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listBuyRecord() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        String searchType = getString("searchType");
        String searchVal = getString("searchVal");
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        maps.put("searchType", searchType);
        maps.put("searchVal", searchVal);
        PageInfo<BuyRecord> page = buyRecordService.getBuyRecordPageInfo(maps, pages[0], pages[1], "");
        model.put("page", page);
        model.put("method", "list");
        model.put("searchType", searchType);
        return new ModelAndView("cemetery/cemetery/sale/buyRecord", model);
    }

    /**
     * @param
     * @return org.springframework.web.servlet.ModelAndView
     * @Auther: ZhixiangWang on 2019/7/24 16:45
     * @Description: TODO
     */
    @RequestMapping(params = "method=data")
    public ModelAndView dataInquiry() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        // 填入陵园查询内容
        Map<String, Object> cemeteryMaps = new HashMap<String, Object>(16);
        // 状态为启用
        cemeteryMaps.put("isdel", Const.Isdel_No);
        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
        model.put("cemeteryList", cemeteryList);
        //获取选中陵园ID
        String cemeteryId = getString("selectCemetery");
        model.put("selectCemetery", cemeteryId);
        //首次进入标志
        String entries = getString("entries");
        String radioType = getString("radioType");
        String searchVal = getString("searchVal");
        PageInfo<BuyRecord> page = new PageInfo<BuyRecord>();
        if ("".equals(entries) || entries == null) {
            // 填入查询内容
            Map<String, Object> maps = new HashMap<String, Object>(16);
            maps.put("cId", cemeteryId);
            maps.put("radioType", radioType);
            maps.put("searchVal", searchVal);
            page = buyRecordService.getBuyAndUserRecordPageInfo(maps, pages[0], pages[1], "");
        } else {
            PageHelper.startPage(pages[0], pages[1]);
        }
        model.put("page", page);
        if ("".equals(radioType) || radioType == null) {
            radioType = "3";
        }
        model.put("method", "data");
        model.put("entries", entries);
        model.put("radioType", radioType);
        return new ModelAndView("cemetery/cemetery/sale/dataInquiry", model);
    }
}
