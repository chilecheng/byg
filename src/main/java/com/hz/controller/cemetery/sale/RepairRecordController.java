package com.hz.controller.cemetery.sale;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.*;
import com.hz.service.cemetery.BuyRecordService;
import com.hz.service.cemetery.CemeteryTombService;
import com.hz.service.cemetery.RepairRecordService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 墓穴维修记录增删改查控制层
 *
 * @ClassName RepairRecordController
 * @Description TODO
 * @Author ZhixiangWang
 * @Date 2019/7/18 9:13
 * @Version 1.0
 */
@Controller
@RequestMapping("gmRepairRecord.do")
public class RepairRecordController extends BaseController {
    @Autowired
    private RepairRecordService repairRecordService;
    @Autowired
    private BuyRecordService buyRecordService;
    @Autowired
    private CemeteryTombService cemeteryTombService;

    @ModelAttribute
    public void repairRecordModel(Model model) {
        model.addAttribute("url", "gmRepairRecord.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listRepairRecord();
    }

    /**
     * 墓穴维修记录查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listRepairRecord() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        //填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        //首次进入标志
        String entries = getString("entries");
        String searchType = getString("searchType");
        String searchCondition = getString("searchCondition");
        String searchVal = getString("searchVal");
        byte repairType = getByte("repairType");
        maps.put("searchType", searchType);
        maps.put("searchCondition", searchCondition);
        maps.put("searchVal", searchVal);
        maps.put("repairType", repairType);
        PageInfo<RepairRecord> page = repairRecordService.getRepairRecordPageInfo(maps, pages[0], pages[1], "");
        model.put("page", page);
        model.put("method", "list");
        model.put("entries", entries);
        model.put("searchType", searchType);
        model.put("searchCondition", searchCondition);
        model.put("repairType", repairType);
        model.put("GMTYPE_YES", Const.GMTYPE_YES);
        model.put("GMTYPE_NO", Const.GMTYPE_NO);
        return new ModelAndView("cemetery/cemetery/sale/repairRecord", model);
    }

    /**
     * 墓穴完成维修
     *
     * @return
     */
    @RequestMapping(params = "method=complete")
    public ModelAndView saveRepairRecord() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        String repairId = getString("id");
        RepairRecord repairRecord = repairRecordService.getRepairRecordById(repairId);
        model.put("repairRecord", repairRecord);
        model.put("url", "gmRepairRecord.do");
        model.put("method", "saveComplete");
        return new ModelAndView("cemetery/cemetery/sale/completeRepair", model);
    }

    /**
     * 保存墓穴完成维修记录
     *
     * @return
     */
    @RequestMapping(params = "method=saveComplete")
    @SystemControllerLog(description = "保存墓穴完成维修记录")
    @ResponseBody
    public JSONObject saveComplete() {
        JSONObject json = new JSONObject();
        try {
            String repairRecordId = getString("repairRecordId");
            RepairRecord repairRecord = repairRecordService.getRepairRecordById(repairRecordId);
            String name = getString("name");
            repairRecord.setName(name);
            String phoneNumber = getString("phoneNumber");
            repairRecord.setPhoneNumber(phoneNumber);
            String comment = getString("comment");
            repairRecord.setComment(comment);
            repairRecord.setRepairType(Const.GMTYPE_YES);
            String completeTime = getString("completeTime");
            repairRecord.setCompleteTime(DataTools.stringToSqlDate(completeTime));
            repairRecordService.updateRepairRecord(repairRecord);
            BuyRecord buyRecord = buyRecordService.getBuyRecordByTombId(repairRecord.getTombId());
            Tomb tomb = cemeteryTombService.getCemeteryTombById(repairRecord.getTombId());
            if (buyRecord != null) {
                tomb.setState(Const.GM_SOU);
            }else {
                tomb.setState(Const.GM_KONG);
            }
            cemeteryTombService.updateCemeteryTomb(tomb);
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

}
