package com.hz.controller.cemetery.sale;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.*;
import com.hz.service.cemetery.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 选定墓穴总界面：购买、预定、修改等功能
 *
 * @author jgj
 */
@Controller
@RequestMapping("/tombType.do")
public class TombTypeListController extends BaseController {
    @Autowired
    private CemeteryTombService tombService;
    @Autowired
    private CemeteryService cemeteryService;
    @Autowired
    private CemeteryAreaService cemeteryAreaService;
    @Autowired
    private TombTypeListService tombTypeListService;
    @Autowired
    private UserRecordService userRecordService;
    @Autowired
    private BuyRecordService buyRecordService;
    @Autowired
    private MaintainRecordService maintainRecordService;
    @Autowired
    private PaymentRecordService paymentRecordService;
    @Autowired
    private RepairRecordService repairRecordService;
    @Autowired
    private GmPayRecordService gmPayRecordService;

    @ModelAttribute
    public void CemeteryAreaModel(Model model) {
        model.addAttribute("url", "tombType.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return TombType();
    }

    /**
     * 墓穴状态信息总界面
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView TombType() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tid = getString("tid");
        Tomb tomb = tombService.getCemeteryTombById(tid);
        BuyRecord buyRecord = buyRecordService.getBuyRecordByTombId(tid);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("buyRecordId", buyRecord.getId());
        List<UserRecord> userList = userRecordService.getUserRecordList(paramMap, "");
        List<MaintainRecord> mainList = maintainRecordService.getMaintainRecordList(paramMap, "");
        List<PaymentRecord> payList = paymentRecordService.getPaymentRecordList(paramMap, "");
        model.put("tomb", tomb);
        model.put("method", "list");
        model.put("buyRecord", buyRecord);
        model.put("userList", userList);
        model.put("mainList", mainList);
        model.put("payList", payList);
        model.put("GMDEATH_SHO", Const.GMDEATH_SHO);
        model.put("GMDEATH_GU", Const.GMDEATH_GU);
        String areaId = getString("areaId");
        String cemeteryId = getString("cemeteryId");
        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
        model.put("cemetery", cemetery);
        model.put("cemeteryArea", cemeteryArea);
        model.put("areaId", areaId);
        model.put("cemeteryId", cemeteryId);
        return new ModelAndView("cemetery/sale/tombType", model);
    }

    /**
     * 墓穴销售界面
     *
     * @return
     */
    @RequestMapping(params = "method=sale")
    public ModelAndView saleCemetery() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);

        String sexOption = Const.getGMSexOption(Const.Sex_Fale, false);
        String deadOption = Const.getGMDeadOption(Const.GMDEATH_SHO, false);
        String longTimeOption = Const.getGMLongTimeOption(1, false);
        Date nowTime = DateTools.getThisDate();
        String tid = getString("tid");
        Tomb tomb = tombService.getCemeteryTombById(tid);
        model.put("nowTime", nowTime);
        model.put("deadOption", deadOption);
        model.put("sexOption", sexOption);
        model.put("longTimeOption", longTimeOption);
        model.put("tid", tid);
        model.put("tomb", tomb);
        model.put("method", "save");
        return new ModelAndView("cemetery/sale/editTombSale", model);
    }

    /**
     * 查看信息
     *
     * @return
     */
    @RequestMapping(params = "method=read")
    public ModelAndView readCemetery() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tombId = getString("tid");
        BuyRecord buyRecord = buyRecordService.getBuyRecordByTombId(tombId);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("buyRecordId", buyRecord.getId());
        List<UserRecord> userList = userRecordService.getUserRecordList(paramMap, "");
        List<MaintainRecord> mainList = maintainRecordService.getMaintainRecordList(paramMap, "");
        List<PaymentRecord> payList = paymentRecordService.getPaymentRecordList(paramMap, "");
        model.put("buyRecord", buyRecord);
        model.put("userList", userList);
        model.put("mainList", mainList);
        model.put("payList", payList);
        model.put("GMDEATH_SHO", Const.GMDEATH_SHO);
        model.put("GMDEATH_GU", Const.GMDEATH_GU);
        return new ModelAndView("cemetery/sale/readTomb", model);
    }

    /**
     * 公墓各项目操作修改
     *
     * @return
     */
    @RequestMapping(params = "method=items")
    public ModelAndView cemeteryItemsModify() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        //获取操作的公墓id
        String tombId = getString("tid");
        String type = getString("type");
        String url = "cemetery/sale";

        //缴纳墓穴费用
        if ("mxf".equals(type)) {
            //查询对应墓碑的缴费记录
            PaymentRecord payRecord = buyRecordService.getPaymentRecordByTombId(tombId);
            model.put("tombId", tombId);
            model.put("payRecord", payRecord);
            url = url + "/mxf";

            //普通收费
        } else if ("ptsf".equals(type)) {
            // 5:雕刻费 4:迁移费 6:维修费
            //封龙门=1:雕刻安放费?
            String selectTollOption = Const.getGmChargeTypeOption(new byte[]{Const.GMCHARGE_DKF, Const.GMCHARGE_QYF, Const.GMCHARGE_WXF, Const.GMCHARGE_DKAFF});
            Tomb tomb = tombService.getCemeteryTombById(tombId);
            model.put("user", getCurrentUser().getName());
            model.put("tomb", tomb);
            model.put("selectTollOption", selectTollOption);
            url = url + "/ptsf";

            //1:雕刻安放封龙门费
        } else if ("dkafflm".equals(type)) {
            Tomb tomb = tombService.getCemeteryTombById(tombId);
            model.put("user", getCurrentUser().getName());
            model.put("tomb", tomb);
            url = url + "/dkafflm";

            //6:维修登记
        } else if ("wxdj".equals(type)) {
            String repairTypeOption = Const.getRepairTypeOption(Const.GMTYPE_NO, false);
            BuyRecord buyRecord = buyRecordService.getBuyRecordByTombId(tombId);
            model.put("user", getCurrentUser().getName());
            model.put("buyRecord", buyRecord);
            model.put("repairTypeOption", repairTypeOption);
            url = url + "/wxdj";

            //安葬事项联系单
        } else if ("lxd".equals(type)) {
            Tomb tomb = tombService.getCemeteryTombById(tombId);
            model.put("tomb", tomb);
            url = url + "/lxd";
        }
        model.put("type", type);
        model.put("method", "saveItems");
        return new ModelAndView(url, model);
    }

    /**
     * 已购墓穴相关操作集合
     *
     * @return
     */
    @RequestMapping(params = "method=saveItems")
    @SystemControllerLog(description = "已购墓穴操作")
    @ResponseBody
    public JSONObject saveCemeteryItems() {
        JSONObject json = new JSONObject();
        try {
            String type = getString("type");
            String tombId = getString("tombId");
            BuyRecord buyRecord = buyRecordService.getBuyRecordByTombId(tombId);
            String buyRecordId = buyRecord.getId();
            String name = getString("name");
            String invoiceNumber = getString("invoiceNumber");
            String payTimeName = getString("payTime");
            Date payTime = DataTools.stringToSqlDate(payTimeName);
            String remark = getString("remark");
            String operator = getCurrentUser().getName();
            String createTime = getString("createTime");
            String id = UuidUtil.get32UUID();
            String payerName = getString("payerName");
            double total = getDouble("total");
            String phone = getString("phone");
            if ("mxf".equals(type)) {
                PaymentRecord paymentRecord = new PaymentRecord();
                paymentRecord.setRecordId(buyRecordId);
                paymentRecord.setName(name);
                paymentRecord.setInvoiceNumber(invoiceNumber);
                paymentRecord.setPayTime(payTime);
                paymentRecord.setTotalAmount(getDouble("totalAmount"));
                paymentRecord.setRemark(remark);
                paymentRecord.setOperator(getString("operator"));
                paymentRecord.setCreaTime(DateTools.getThisDate());
                paymentRecordService.saveTombPaymentRecord(paymentRecord, buyRecordId);
            } else if ("ptsf".equals(type)) {
                PayRecord payRecord = new PayRecord();
                byte selectToll = getByte("selectToll");
                payRecord.setId(id);
                payRecord.setBuyRecordId(buyRecordId);
                payRecord.setCreateTime(DateTools.stringToTimestamp(createTime));
                payRecord.setOperator(operator);
                payRecord.setType(selectToll);
                payRecord.setCostName(Const.getGmChargeType(selectToll));
                payRecord.setPayerName(payerName);
                payRecord.setInvoiceNumber(invoiceNumber);
                payRecord.setTotal(total);
                payRecord.setPayTime(payTime);
                payRecord.setPhone(phone);
                payRecord.setRemark(remark);
                payRecord.setUserId("");
                gmPayRecordService.saveGmPayRecord(payRecord);
            } else if ("dkafflm".equals(type)) {
                PayRecord payRecord = new PayRecord();
                payRecord.setId(id);
                payRecord.setBuyRecordId(buyRecordId);
                payRecord.setCreateTime(DateTools.getThisDateTimestamp());
                payRecord.setType(Const.GMCHARGE_DKAFF);
                payRecord.setCostName(Const.getGmChargeType(Const.GMCHARGE_DKAFF));
                payRecord.setPayerName(payerName);
                payRecord.setInvoiceNumber(invoiceNumber);
                payRecord.setTotal(total);
                payRecord.setPayTime(payTime);
                payRecord.setOperator(operator);
                payRecord.setPhone(phone);
                payRecord.setRemark(remark);
                payRecord.setUserId("");
                gmPayRecordService.saveGmPayRecord(payRecord);
            } else if ("wxdj".equals(type)) {
                RepairRecord repairRecord = new RepairRecord();
                repairRecord.setId(id);
                repairRecord.setName(name);
                repairRecord.setPhoneNumber(getString("phoneNumber"));
                repairRecord.setComment(getString("comment"));
                repairRecord.setBeginTime(DataTools.stringToSqlDate(getString("beginTime")));
                repairRecord.setEndTime(DataTools.stringToSqlDate(getString("endTime")));
                repairRecord.setTombId(tombId);
                repairRecord.setRepairType(getByte("repairType"));
                tombTypeListService.saveRepairRecord(repairRecord, tombId);
//                if ((payerName != null || !"".equals(payerName)) || (invoiceNumber != null || !"".equals(invoiceNumber)) || (total != null || !"".equals(total))) {
                PayRecord payRecord = new PayRecord();
                payRecord.setId(id);
                payRecord.setBuyRecordId(buyRecordId);
                payRecord.setCreateTime(DateTools.stringToTimestamp(createTime));
                payRecord.setType(Const.GMCHARGE_WXF);
                payRecord.setCostName(Const.getGmChargeType(Const.GMCHARGE_WXF));
                payRecord.setPayerName(payerName);
                payRecord.setInvoiceNumber(invoiceNumber);
                payRecord.setTotal(total);
//                payRecord.setPayTime(payTime);
                payRecord.setPayTime(DateTools.getThisDate());
                payRecord.setOperator(operator);
//                payRecord.setPhone(phone);
//                payRecord.setRemark(remark);
                payRecord.setUserId("");
                gmPayRecordService.saveGmPayRecord(payRecord);
//                }
            }
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 维护续费
     *
     * @return
     */
    @RequestMapping(params = "method=maintain")
    public ModelAndView maintainTomb() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tombId = getString("tid");
        BuyRecord buyRecord = buyRecordService.getBuyRecordByTombId(tombId);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("buyRecordId", buyRecord.getId());
        List<MaintainRecord> mainList = maintainRecordService.getMaintainRecordList(paramMap, "");
        String longTimeOption = Const.getGMLongTimeOption(1, false);
        model.put("longTimeOption", longTimeOption);
        model.put("mainList", mainList);
        model.put("method", "saveMaintain");
        return new ModelAndView("cemetery/sale/maintainTomb", model);
    }


    /**
     * 保存墓穴维护续费
     *
     * @return
     */
    @RequestMapping(params = "method=saveMaintain")
    @SystemControllerLog(description = "保存墓穴维护续费记录")
    @ResponseBody
    public JSONObject saveMaintain() {
        JSONObject json = new JSONObject();
        try {
            String tombId = getString("id");
            BuyRecord buyRecord = buyRecordService.getBuyRecordByTombId(tombId);
            MaintainRecord maintainRecord = new MaintainRecord();
            String beginTime = getString("beginTime");
            maintainRecord.setBeginTime(DataTools.stringToSqlDate(beginTime));
            String endTime = getString("endTime");
            maintainRecord.setEndTime(DataTools.stringToSqlDate(endTime));
            maintainRecord.setId(UuidUtil.get32UUID());
            maintainRecord.setInvoiceNumber(getString("invoiceNumber"));
            maintainRecord.setLongTime(getInt("longTime"));
            String payMoney = getString("payMoney");
            maintainRecord.setPayMoney(DataTools.stringToDouble(payMoney));
            maintainRecord.setPayName(getString("payName"));
            maintainRecord.setPayTime(DateTools.getThisDate());
            maintainRecord.setTombId(tombId);
            maintainRecord.setRecordId(buyRecord.getId());
            maintainRecordService.saveMaintainRecord(maintainRecord);
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 换证、补证界面
     *
     * @return
     */
    @RequestMapping(params = "method=change")
    public ModelAndView changeCertificates() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tid = getString("tid");
        BuyRecord buyrecord = buyRecordService.getBuyRecordByTombId(tid);
        model.put("buyrecord", buyrecord);
        model.put("method", "saveChange");
        return new ModelAndView("cemetery/sale/changeCertificates", model);
    }

    /**
     * 保存墓穴补证换证
     *
     * @return
     */
    @RequestMapping(params = "method=saveChange")
    @SystemControllerLog(description = "保存墓穴补证换证")
    @ResponseBody
    public JSONObject saveChange() {
        JSONObject json = new JSONObject();
        try {
            String id = getString("");
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 预定墓穴
     *
     * @return
     */
    @RequestMapping(params = "method=yuding")
    public ModelAndView yudingCemetery() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tid = getString("tid");
        model.put("tid", tid);
        model.put("method", "saveYuding");
        return new ModelAndView("cemetery/sale/yudingTomb", model);
    }

    /**
     * 取消公墓预约记录
     *
     * @return
     */
    @RequestMapping(params = "method=escYuding")
    @SystemControllerLog(description = "取消墓穴预约记录")
    @ResponseBody
    public JSONObject escYuding() {
        JSONObject json = new JSONObject();
        try {
            String tid = getString("tid");
            tombTypeListService.escYuding(tid);
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 保存公墓预约记录
     *
     * @return
     */
    @RequestMapping(params = "method=saveYuding")
    @SystemControllerLog(description = "保存墓穴预约记录")
    @ResponseBody
    public JSONObject saveYuding() {
        JSONObject json = new JSONObject();
        try {
            String tombId = getString("id");
            AppointmentRecord appointment = new AppointmentRecord();
            appointment.setId(UuidUtil.get32UUID());
            appointment.setName(getString("name"));
            appointment.setIdCard(getString("IDCard"));
            appointment.setPhoneNumber(getString("phoneNumber"));
            appointment.setAddress(getString("address"));
            appointment.setTombId(tombId);
            String money = getString("earnestMoney");
            appointment.setEarnestMoney(DataTools.stringToDouble(money));
            String endTime = getString("endTime");
            appointment.setEndTime(DataTools.stringToSqlDate(endTime));
            appointment.setComment(getString("comment"));
            appointment.setCreatePeople(getCurrentUser().getName());
            appointment.setCreateTime(DateTools.getThisDate());
            tombTypeListService.saveYudingRecord(appointment, tombId);
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 墓穴迁出界面
     *
     * @return
     */
    @RequestMapping(params = "method=transfer")
    public ModelAndView transferTomb() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tid = getString("tid");
        String isRecoveryOption = Const.getIsOption(Const.Is_Yes, false);
        String userNameOption = userRecordService.getUserNameOption("", tid, Const.Is_No);
        model.put("tid", tid);
        model.put("isRecoveryOption", isRecoveryOption);
        model.put("userNameOption", userNameOption);
        model.put("method", "saveTransfer");
        return new ModelAndView("cemetery/sale/transferTomb", model);
    }

    /**
     * 保存墓穴迁出记录
     *
     * @return
     */
    @RequestMapping(params = "method=saveTransfer")
    @SystemControllerLog(description = "保存墓穴迁出记录")
    @ResponseBody
    public JSONObject saveTransfer() {
        JSONObject json = new JSONObject();
        try {
            TransferRecord transferRecord = new TransferRecord();
            String tombId = getString("id");
//			String userName=getString("userName");
            String[] userNames = getArrayString("userName");
            for (int i = 0; i < userNames.length; i++) {
                String userId = userNames[i];
                transferRecord.setId(UuidUtil.get32UUID());
                transferRecord.setApplicant(getString("applicant"));
                transferRecord.setPhoneNumber(getString("phoneNumber"));
                transferRecord.setTombId(tombId);
                transferRecord.setIsRecovery(getByte("isRecovery"));
                String transferTime = getString("transferTime");
                transferRecord.setTransferTime(DataTools.stringToSqlDate(transferTime));
                transferRecord.setComment(getString("comment"));
                transferRecord.setCreatePeople(getCurrentUser().getName());
                transferRecord.setCreateTime(DateTools.getThisDate());
                tombTypeListService.saveTransferRecord(userId, transferRecord, tombId);
            }
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 修缮墓穴
     *
     * @return
     */
    @RequestMapping(params = "method=repair")
    public ModelAndView repairCemetery() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tid = getString("tid");
        model.put("tid", tid);
        model.put("method", "saveRepair");
        return new ModelAndView("cemetery/sale/repairTomb", model);
    }

    /**
     * 保存公墓维修记录
     *
     * @return
     */
    @RequestMapping(params = "method=saveRepair")
    @SystemControllerLog(description = "保存墓穴维修记录")
    @ResponseBody
    public JSONObject saveRepair() {
        JSONObject json = new JSONObject();
        try {
            String tombId = getString("id");
            RepairRecord repairRecord = new RepairRecord();
            repairRecord.setId(UuidUtil.get32UUID());
            repairRecord.setName(getString("name"));
            repairRecord.setPhoneNumber(getString("phoneNumber"));
            repairRecord.setTombId(tombId);
            String beginTime = getString("beginTime");
            repairRecord.setBeginTime(DataTools.stringToSqlDate(beginTime));
            String endTime = getString("endTime");
            repairRecord.setEndTime(DataTools.stringToSqlDate(endTime));
            repairRecord.setComment(getString("comment"));
            repairRecord.setRepairType(Const.GMTYPE_NO);
            tombTypeListService.saveRepairRecord(repairRecord, tombId);
            setJsonBySuccess(json, "保存成功", true);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 墓穴修缮完成界面
     *
     * @return
     */
    @RequestMapping(params = "method=completeRepair")
    public ModelAndView completeRepair() {
        Map<String, Object> model = new HashMap<String, Object>();
        model = getModel(model);
        String tid = getString("tid");
        model.put("tid", tid);
        model.put("method", "saveCompleteRepair");
        return new ModelAndView("cemetery/sale/completeRepair", model);
    }

    /**
     * 保存墓穴修缮完成信息
     *
     * @return
     */
    @RequestMapping(params = "method=saveCompleteRepair")
    @SystemControllerLog(description = "保存修缮完成信息")
    @ResponseBody
    public JSONObject saveCompleteRepair() {
        JSONObject json = new JSONObject();
        try {
            String tombId = getString("id");
            RepairRecord repairRecord = repairRecordService.getRepairRecordByTombIdAndType(tombId, Const.Is_No);
            String completeTime = getString("completeTime");
            repairRecord.setCompleteTime(DataTools.stringToSqlDate(completeTime));
            repairRecord.setRepairType(Const.Is_Yes);
            tombTypeListService.saveCompleteRepair(repairRecord, tombId);
            setJsonBySuccess(json, "修缮完成", true);
        } catch (Exception e) {
            setJsonByFail(json, "修缮失败，错误" + e.getMessage());
        }
        return json;
    }

    /**
     * 保存墓穴购买信息
     *
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存墓穴购买信息")
    @ResponseBody
    public JSONObject saveTombSale() {
        JSONObject json = new JSONObject();
        try {
            String tombId = getString("tombId");
            Tomb tomb = tombService.getCemeteryTombById(tombId);
            //使用者信息
            String userName[] = getArrayString("deadName");
            String sex[] = getArrayString("sex");
            String deadType[] = getArrayString("deadType");
            String borthTime[] = getArrayString("borthTime");
            String deadTime[] = getArrayString("deadTime");
            String releaseTime[] = getArrayString("releaseTime");
            String longMenTime[] = getArrayString("longMenTime");
            //墓穴购买记录基本信息
            BuyRecord buyRecord = new BuyRecord();
            String buyId = UuidUtil.get32UUID();
            buyRecord.setId(buyId);
            buyRecord.setCardHolder(getString("cardHolder"));
            buyRecord.setTombsCard(getString("tombsCard"));
            buyRecord.setIDNumber(getString("IDNumber"));
            buyRecord.setAddress(getString("address"));
            buyRecord.setComment(getString("comment"));
            buyRecord.setContactNumber(getString("contactNumber"));
            buyRecord.setPhoneNumber(getString("phoneNumber"));
//    		buyRecord.setCemeteryId(getString("cemeteryId"));
//    		buyRecord.setAreaId(getString("areaId"));
//    		buyRecord.setRowId(getString("rowId"));
            buyRecord.setTombId(tombId);
            buyRecord.setGraveInfo(tomb.getName());
            buyRecord.setCreatePeople(getCurrentUser().getUserId());
            buyRecord.setCreateTime(DateTools.getThisDate());
            //墓穴购买记录支付信息
            PaymentRecord paymentRecord = new PaymentRecord();
            paymentRecord.setId(UuidUtil.get32UUID());
            paymentRecord.setInvoiceNumber(getString("payNumber"));
            paymentRecord.setName(getString("payName"));
            String tombPayTimeStr = getString("tombPayTime");
            Date tombPayTime = null;
            if (tombPayTimeStr != null && tombPayTimeStr != "") {
                tombPayTime = DateTools.stringToSqlDate(tombPayTimeStr, "yyyy-MM-dd");
                paymentRecord.setPayTime(tombPayTime);
            }
            paymentRecord.setRecordId(buyId);
            paymentRecord.setTotalAmount(getDouble("payPrice"));
            //维护信息
            String[] beginTime = getArrayString("beginTime");
            String[] endTime = getArrayString("endTime");
            String[] longTime = getArrayString("longTime");
            String[] payName = getArrayString("maipayName");
            String[] payTime = getArrayString("maiPayTime");
            String[] payMoney = getArrayString("payMoney");
            String[] invoiceNumber = getArrayString("invoiceNumber");
            tombTypeListService.saveAllSaleRecord(userName, sex, deadType,
                    borthTime, deadTime, releaseTime, longMenTime, buyId,
                    buyRecord, paymentRecord, beginTime, endTime, longTime, payName,
                    payTime, payMoney, invoiceNumber, tombId);
            setJsonBySuccess(json, "保存成功", "tombType.do?method=list&tid=" + tombId);
        } catch (Exception e) {
            setJsonByFail(json, "保存失败，错误" + e.getMessage());
        }
        return json;
    }


}
