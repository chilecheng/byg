package com.hz.controller.cemetery.sale;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryArea;
import com.hz.entity.cemetery.CemeteryRow;
import com.hz.entity.cemetery.Tomb;
import com.hz.service.cemetery.CemeteryTombService;
import com.hz.util.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 墓穴查询控制层
 *
 * @ClassName TombSearchController
 * @Description TODO
 * @Author ZhixiangWang
 * @Date 2019/7/1 16:38
 * @Version 1.0
 */
@Controller
@RequestMapping("/tombSearch.do")
public class TombSearchController extends BaseController {
    @Autowired
    private CemeteryTombService cemeteryTombService;

    @ModelAttribute
    public void tombSearchModel(Model model) {
        model.addAttribute("url", "tombSearch.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return tombSearch();
    }

    /**
     * 陵园墓穴信息查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView tombSearch() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        // 填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        //首次进入标志
        String entries = getString("entries");
        String searchType = getString("searchType");
        String searchCondition = getString("searchCondition");
        String searchVal = getString("searchVal");
        maps.put("searchType", searchType);
        maps.put("searchCondition", searchCondition);
        maps.put("searchVal", searchVal);
        PageInfo<Tomb> page = cemeteryTombService.getCemeteryTombPageInfo(maps, pages[0], pages[1], "index_flag");
        model.put("page", page);
        model.put("method", "list");
        model.put("GM_KONG", Const.GM_KONG);
        model.put("GM_SOU", Const.GM_SOU);
        model.put("GM_WEI", Const.GM_WEI);
        model.put("GM_QIAN", Const.GM_QIAN);
        model.put("GM_GUO", Const.GM_GUO);
        model.put("GM_YUDING", Const.GM_YUDING);
        model.put("searchType", searchType);
        model.put("entries", entries);
        model.put("searchCondition", searchCondition);
        return new ModelAndView("cemetery/cemetery/sale/tombSearch", model);
    }

}
