package com.hz.controller.cemetery.sale;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.UserRecord;
import com.hz.service.cemetery.UserRecordService;
import com.hz.util.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 墓穴使用者记录增删改查控制层
 *
 * @ClassName UserRecordController
 * @Description TODO
 * @Author ZhixiangWang
 * @Date 2019/6/27 17:37
 * @Version 1.0
 **/
@Controller
@RequestMapping("/userRecord.do")
public class UserRecordController extends BaseController {
    @Autowired
    private UserRecordService userRecordService;

    @ModelAttribute
    public void userRecordModel(Model model) {
        model.addAttribute("url", "userRecord.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listUserRecord();
    }

    /**
     * 墓穴使用者记录查询列表
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listUserRecord() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        // 获得页面页码信息
        int[] pages = getPage();
        //首次进入标志
        String entries = getString("entries");
        String searchType = getString("searchType");
        String searchCondition = getString("searchCondition");
        String searchVal = getString("searchVal");
        int type = getInt("type");
        //填入查询内容
        Map<String, Object> maps = new HashMap<String, Object>(16);
        maps.put("searchType", searchType);
        maps.put("searchCondition", searchCondition);
        maps.put("searchVal", searchVal);
        PageInfo<UserRecord> page = userRecordService.getUserRecordPageInfo(maps, pages[0], pages[1], "release_time desc");
        model.put("page", page);
        model.put("method", "list");
        model.put("entries", entries);
        model.put("Sex_Fale", Const.Sex_Fale);
        model.put("Sex_Female", Const.Sex_Female);
        model.put("Sex_Unknown", Const.Sex_Unknown);
        model.put("GMDEATH_SHO", Const.GMDEATH_SHO);
        model.put("GMDEATH_GU", Const.GMDEATH_GU);
        model.put("searchType", searchType);
        model.put("searchCondition", searchCondition);
        if (type == 1) {
            return new ModelAndView("cemetery/cemetery/sale/cemeteryCarving", model);
        } else {
            return new ModelAndView("cemetery/cemetery/sale/userRecord", model);
        }
    }


}
