package com.hz.controller.cemetery.sale;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.CemeteryBack;
import com.hz.service.cemetery.CemeteryBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * �����������Ʋ�
 *
 * @ClassName ReductionApprovalController
 * @Description TODO
 * @Author ZhixiangWang
 * @Date 2019/7/29 17:31
 * @Version 1.0
 **/
@Controller
@RequestMapping("/reductionApproval.do")
public class ReductionApprovalController extends BaseController {

    @ModelAttribute
    public void reductionApprovalModel(Model model) {
        model.addAttribute("url", "reductionApproval.do");
    }

    @RequestMapping
    public ModelAndView unspecified() {
        return listReductionApproval();
    }

    /**
     * ����������ѯ�б�
     *
     * @return
     */
    @RequestMapping(params = "method=list")
    public ModelAndView listReductionApproval() {
        Map<String, Object> model = new HashMap<String, Object>(16);
        model = getModel(model);
        model.put("method", "list");
        return new ModelAndView("cemetery/cemetery/sale/reductionApproval", model);
    }
}
