package com.hz.controller.cemetery.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.cemetery.BuyRecord;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryArea;
import com.hz.entity.cemetery.CemeteryRowAndTomb;
import com.hz.entity.cemetery.MaintenancePaymentedRecord;
import com.hz.entity.cemetery.PayRecord;
import com.hz.entity.cemetery.PaymentedRecord;
import com.hz.entity.cemetery.UserRecord;
import com.hz.service.cemetery.CemeteryAreaService;
import com.hz.service.cemetery.CemeteryRowService;
import com.hz.service.cemetery.CemeteryService;
import com.hz.service.cemetery.ReportService;
import com.hz.service.cemetery.record.PaymentedRecordService;
import com.hz.util.Const;
import com.hz.util.DataTools;

import net.sf.jsqlparser.expression.StringValue;

/**
-----------------------------------
*Describtion:报表统计
*-----------------------------------
*Creatime:2019年6月25日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
/**
 * 报表统计:墓穴费统计/维护费统计/雕刻安放费统计/安放量统计/墓穴销售统计/墓区汇总表
 *
 * @author wujiayang
 */
@Controller
@RequestMapping("/reportStatistics.do")
public class ReportController extends BaseController {
	@Autowired
    private ReportService reportService;
	@Autowired
    private CemeteryService cemeteryService;
	@Autowired
    private CemeteryAreaService cemeteryAreaService;
	@Autowired
    private CemeteryRowService cemeteryRowService;
	@ModelAttribute
	 public void CemeteryModel(Model model) {
	       model.addAttribute("url", "reportStatistics.do");
	    }
/**
* 墓穴费统计
*
* @return
* */
	 @RequestMapping(params="method=paymentStatistics")
	 public ModelAndView paymentRecord(){
		 Map<String, Object> model = new HashMap<String, Object>();
	        model = getModel(model);
	        // 获得页面页码信息
	        int[] pages = getPage();
	        // 填入查询内容
	        Map<String, Object> cemeteryMaps = new HashMap<String, Object>();
	        cemeteryMaps.put("isdel", Const.Isdel_No);
	        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
	        //获取选中陵园ID
	        String cemeteryId = getString("selectCemetery");
	        List<Object[]> list = new ArrayList<Object[]>();
	        for (Cemetery cemetery : cemeteryList) {
	            list.add(new Object[]{cemetery.getId(), cemetery.getName()});
	        }
	        String selectCemeteryOption = "";
	        if (cemeteryId == null || "".equals(cemeteryId)) {
	            cemeteryId = cemeteryList.get(0).getId();
	        }
	        // 填入陵区查询内容
	        Map<String, Object> maps = new HashMap<String, Object>();
	        maps.put("cemeteryId", cemeteryId);
	        selectCemeteryOption = DataTools.getOptionByList(list, cemeteryId, false);
	        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "");
	        //获取选中区域ID
	        String areaId = getString("selectArea");
	        List<Object[]> areaList = new ArrayList<Object[]>();
	        for (CemeteryArea cemeteryArea : cemeteryAreaList) {
	            areaList.add(new Object[]{cemeteryArea.getId(), cemeteryArea.getName()});
	        }
	        String selectAreaOption = "";
	        //获取选中陵园是否改变
	        String type = getString("type");
	        if (cemeteryAreaList.size() < 1) {
	            areaId = "1";
	        } else {
	            if ((areaId == null || "".equals(areaId)) || type.equals("1")) {
	                areaId = cemeteryAreaList.get(0).getId();
	            }
	        }
	        selectAreaOption = DataTools.getOptionByList(areaList, areaId, false);
	     // 填入排查询内容
	        Map<String, Object> rowMap = new HashMap<String, Object>();
	        rowMap.put("areaId", areaId);
	        rowMap.put("cid", cemeteryId);
	        rowMap.put("isdel", Const.Isdel_No);
	        List<CemeteryRowAndTomb> cemeteryRowList = cemeteryRowService.getRowAndTombList(rowMap, "index_flag desc");
	        model.put("cemeteryRowList", cemeteryRowList);
	        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
	        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
	        String startTime = getString("startTime");
	        String endTime = getString("endTime");
	        Map<String, Object> tombMap = new HashMap<String, Object>();
	        if ("".equals(type)||type==null) {
	        	tombMap.put("cemeteryId", cemeteryId);
	        	tombMap.put("areaId", areaId);
	        	tombMap.put("startTime", startTime);
	        	tombMap.put("endTime", endTime);
	        	int result=reportService.getCemeteryrecord(tombMap);
	        	model.put("result", result);
			}
	        PageInfo<PaymentedRecord> page = reportService.getCemeteryrecord(tombMap, pages[0], pages[1], "record_id asc");
	        model.put("method", "paymentStatistics");
	        model.put("startTime", startTime);
	        model.put("endTime", endTime);
	        model.put("cemetery", cemetery);
	        model.put("cemeteryArea", cemeteryArea);
	        model.put("cemeteryId", cemeteryId);
	        model.put("areaId", areaId);
	        model.put("page", page);
	        model.put("selectCemeteryOption", selectCemeteryOption);
	        model.put("selectAreaOption", selectAreaOption);
        return new ModelAndView("cemetery/reportStatistics/paymentStatistics", model);

	 }
/**
* 维护费统计
*
* @return
* */
	 	@RequestMapping(params="method=maintenanceStatistics")
	 	public ModelAndView maintenanceRecord(){
	 		Map<String, Object> model = new HashMap<String, Object>();
	        model = getModel(model);
	        // 获得页面页码信息
	        int[] pages = getPage();
	        // 填入陵区查询内容
	        Map<String, Object> cemeteryMaps = new HashMap<String, Object>();
	        cemeteryMaps.put("isdel", Const.Isdel_No);
	        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
	        //获取选中陵园ID
	        String cemeteryId = getString("selectCemetery");
	        List<Object[]> list = new ArrayList<Object[]>();
	        for (Cemetery cemetery : cemeteryList) {
	            list.add(new Object[]{cemetery.getId(), cemetery.getName()});
	        }
	        String selectCemeteryOption = "";
	        if (cemeteryId == null || "".equals(cemeteryId)) {
	            cemeteryId = cemeteryList.get(0).getId();
	        }
	        Map<String, Object> maps = new HashMap<String, Object>();
	        maps.put("cemeteryId", cemeteryId);
	        selectCemeteryOption = DataTools.getOptionByList(list, cemeteryId, false);
	        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "");
	        //获取选中区域ID
	        String areaId = getString("selectArea");
	        List<Object[]> areaList = new ArrayList<Object[]>();
	        for (CemeteryArea cemeteryArea : cemeteryAreaList) {
	            areaList.add(new Object[]{cemeteryArea.getId(), cemeteryArea.getName()});
	        }
	        String selectAreaOption = "";
	        //获取选中陵园是否改变
	        String type = getString("type");
	        if (cemeteryAreaList.size() < 1) {
	            areaId = "1";
	        } else {
	            if ((areaId == null || "".equals(areaId)) || type.equals("1")) {
	                areaId = cemeteryAreaList.get(0).getId();
	            }
	        }
	        selectAreaOption = DataTools.getOptionByList(areaList, areaId, false);
	     // 填入排查询内容
	        Map<String, Object> rowMap = new HashMap<String, Object>();
	        rowMap.put("areaId", areaId);
	        rowMap.put("cid", cemeteryId);
	        rowMap.put("isdel", Const.Isdel_No);
	        List<CemeteryRowAndTomb> cemeteryRowList = cemeteryRowService.getRowAndTombList(rowMap, "index_flag desc");
	        model.put("cemeteryRowList", cemeteryRowList);
	        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
	        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
	        String startTime = getString("startTime");
	        String endTime = getString("endTime");
	        maps.put("areaId", areaId);
	        maps.put("startTime", startTime);
	        maps.put("endTime", endTime);
	        Map<String, Object> tombMap = new HashMap<String, Object>();
	        if ("".equals(type)||type==null) {
	        	tombMap.put("cemeteryId", cemeteryId);
	        	tombMap.put("areaId", areaId);
	        	tombMap.put("startTime", startTime);
	        	tombMap.put("endTime", endTime);
	        	int result=reportService.getMaintenancerecord(tombMap);
	        	model.put("result", result);
			}
	        PageInfo<MaintenancePaymentedRecord> page = reportService.getMaintenancerecord(tombMap, pages[0], pages[1], "record_id asc");
	        model.put("method", "maintenanceStatistics");
	        model.put("page", page);
	        model.put("startTime", startTime);
	        model.put("endTime", endTime);
	        model.put("cemetery", cemetery);
	        model.put("cemeteryArea", cemeteryArea);
	        model.put("cemeteryId", cemeteryId);
	        model.put("areaId", areaId);
	        model.put("selectCemeteryOption", selectCemeteryOption);
	        model.put("selectAreaOption", selectAreaOption);
	 	return new ModelAndView("cemetery/reportStatistics/maintenanceStatistics",model);

	 	 }
/**
* 雕刻安放费统计
*
* @return
* */
	 	@RequestMapping(params="method=carvingFeeStatistics")
	    public ModelAndView carvingFeeRecord(){
	 		Map<String, Object> model = new HashMap<String, Object>();
	        model = getModel(model);
	        // 获得页面页码信息
	        int[] pages = getPage();
	        // 填入陵区查询内容
	        Map<String, Object> cemeteryMaps = new HashMap<String, Object>();
	        cemeteryMaps.put("isdel", Const.Isdel_No);
	        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
	        //获取选中陵园ID
	        String cemeteryId = getString("selectCemetery");
	        List<Object[]> list = new ArrayList<Object[]>();
	        for (Cemetery cemetery : cemeteryList) {
	            list.add(new Object[]{cemetery.getId(), cemetery.getName()});
	        }
	        String selectCemeteryOption = "";
	        if (cemeteryId == null || "".equals(cemeteryId)) {
	            cemeteryId = cemeteryList.get(0).getId();
	        }
	        Map<String, Object> maps = new HashMap<String, Object>();
	        maps.put("cemeteryId", cemeteryId);
	        selectCemeteryOption = DataTools.getOptionByList(list, cemeteryId, false);
	        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "");
	        //获取选中区域ID
	        String areaId = getString("selectArea");
	        List<Object[]> areaList = new ArrayList<Object[]>();
	        for (CemeteryArea cemeteryArea : cemeteryAreaList) {
	            areaList.add(new Object[]{cemeteryArea.getId(), cemeteryArea.getName()});
	        }
	        String selectAreaOption = "";
	        //获取选中陵园是否改变
	        String type = getString("type");
	        if (cemeteryAreaList.size() < 1) {
	            areaId = "1";
	        } else {
	            if ((areaId == null || "".equals(areaId)) || type.equals("1")) {
	                areaId = cemeteryAreaList.get(0).getId();
	            }
	        }
	        selectAreaOption = DataTools.getOptionByList(areaList, areaId, false);
	     // 填入排查询内容
	        Map<String, Object> rowMap = new HashMap<String, Object>();
	        rowMap.put("areaId", areaId);
	        rowMap.put("cid", cemeteryId);
	        rowMap.put("isdel", Const.Isdel_No);
	        List<CemeteryRowAndTomb> cemeteryRowList = cemeteryRowService.getRowAndTombList(rowMap, "index_flag desc");
	        model.put("cemeteryRowList", cemeteryRowList);
	        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
	        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
	        String startTime = getString("startTime");
	        String endTime = getString("endTime");
	        maps.put("areaId", areaId);
	        maps.put("startTime", startTime);
	        maps.put("endTime", endTime);
	        Map<String, Object> tombMap = new HashMap<String, Object>();
	        if ("".equals(type)||type==null) {
	        	tombMap.put("cemeteryId", cemeteryId);
	        	tombMap.put("areaId", areaId);
	        	tombMap.put("startTime", startTime);
	        	tombMap.put("endTime", endTime);
	        	int result=reportService.getPaycerecord(tombMap);
	        	model.put("result", result);
			}
	        PageInfo<PayRecord> page = reportService.getPaycerecord(tombMap, pages[0], pages[1], "buy_record_id asc");
	        model.put("method", "carvingFeeStatistics");
	        model.put("page", page);
	        model.put("startTime", startTime);
	        model.put("endTime", endTime);
	        model.put("cemetery", cemetery);
	        model.put("cemeteryArea", cemeteryArea);
	        model.put("cemeteryId", cemeteryId);
	        model.put("areaId", areaId);
	        model.put("selectCemeteryOption", selectCemeteryOption);
	        model.put("selectAreaOption", selectAreaOption);
	 	return new ModelAndView("cemetery/reportStatistics/carvingFeeStatistics",model);

	 }
/**
* 安放量统计
*
* @return
* */
		@RequestMapping(params="method=placementStatistics")
		public ModelAndView placementRecord(){
			Map<String, Object> model = new HashMap<String, Object>();
	        model = getModel(model);
	        // 获得页面页码信息
	        int[] pages = getPage();
	        // 填入陵区查询内容
	        Map<String, Object> cemeteryMaps = new HashMap<String, Object>();
	        cemeteryMaps.put("isdel", Const.Isdel_No);
	        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
	        //获取选中陵园ID
	        String cemeteryId = getString("selectCemetery");
	        List<Object[]> list = new ArrayList<Object[]>();
	        for (Cemetery cemetery : cemeteryList) {
	            list.add(new Object[]{cemetery.getId(), cemetery.getName()});
	        }
	        String selectCemeteryOption = "";
	        if (cemeteryId == null || "".equals(cemeteryId)) {
	            cemeteryId = cemeteryList.get(0).getId();
	        }
	        Map<String, Object> maps = new HashMap<String, Object>();
	        maps.put("cemeteryId", cemeteryId);
	        selectCemeteryOption = DataTools.getOptionByList(list, cemeteryId, false);
	        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "");
	        //获取选中区域ID
	        String areaId = getString("selectArea");
	        List<Object[]> areaList = new ArrayList<Object[]>();
	        for (CemeteryArea cemeteryArea : cemeteryAreaList) {
	            areaList.add(new Object[]{cemeteryArea.getId(), cemeteryArea.getName()});
	        }
	        String selectAreaOption = "";
	        //获取选中陵园是否改变
	        String type = getString("type");
	        if (cemeteryAreaList.size() < 1) {
	            areaId = "1";
	        } else {
	            if ((areaId == null || "".equals(areaId)) || type.equals("1")) {
	                areaId = cemeteryAreaList.get(0).getId();
	            }
	        }
	        selectAreaOption = DataTools.getOptionByList(areaList, areaId, false);
	     // 填入排查询内容
	        Map<String, Object> rowMap = new HashMap<String, Object>();
	        rowMap.put("areaId", areaId);
	        rowMap.put("cid", cemeteryId);
	        rowMap.put("isdel", Const.Isdel_No);
	        List<CemeteryRowAndTomb> cemeteryRowList = cemeteryRowService.getRowAndTombList(rowMap, "index_flag desc");
	        model.put("cemeteryRowList", cemeteryRowList);
	        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
	        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
	        String startTime = getString("startTime");
	        String endTime = getString("endTime");
	        Map<String, Object> tombMap = new HashMap<String, Object>();
	        if ("".equals(type)||type==null) {
	        	tombMap.put("cemeteryId", cemeteryId);
	        	tombMap.put("areaId", areaId);
	        	tombMap.put("startTime", startTime);
	        	tombMap.put("endTime", endTime);
	        	int result=reportService.getplacementrecord(tombMap);
	        	model.put("result", result);
			}
	        PageInfo<UserRecord> page = reportService.getplacementrecord(tombMap, pages[0], pages[1], "buy_record_id asc");
	        model.put("method", "placementStatistics");
	        model.put("page", page);
	        model.put("startTime", startTime);
	        model.put("endTime", endTime);
	        model.put("cemetery", cemetery);
	        model.put("cemeteryArea", cemeteryArea);
	        model.put("cemeteryId", cemeteryId);
	        model.put("areaId", areaId);
	        model.put("selectCemeteryOption", selectCemeteryOption);
	        model.put("selectAreaOption", selectAreaOption);
			return new ModelAndView("cemetery/reportStatistics/placementStatistics",model);

		}
/**
* 墓穴销售统计
*
* @return
* */
		@RequestMapping(params="method=salesStatistics")
		public ModelAndView salesRecord(){
				Map<String, Object> model = new HashMap<String, Object>();
				model = getModel(model);
				 // 获得页面页码信息
		        int[] pages = getPage();
		        // 填入陵区查询内容
		        Map<String, Object> cemeteryMaps = new HashMap<String, Object>();
		        cemeteryMaps.put("isdel", Const.Isdel_No);
		        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
		        //获取选中陵园ID
		        String cemeteryId = getString("selectCemetery");
		        List<Object[]> list = new ArrayList<Object[]>();
		        for (Cemetery cemetery : cemeteryList) {
		            list.add(new Object[]{cemetery.getId(), cemetery.getName()});
		        }
		        String selectCemeteryOption = "";
		        if (cemeteryId == null || "".equals(cemeteryId)) {
		            cemeteryId = cemeteryList.get(0).getId();
		        }
		     // 填入陵区查询内容
		        Map<String, Object> maps = new HashMap<String, Object>();
		        maps.put("cemeteryId", cemeteryId);
		        selectCemeteryOption = DataTools.getOptionByList(list, cemeteryId, false);
		        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "");
		        //获取选中区域ID
		        String areaId = getString("selectArea");
		        List<Object[]> areaList = new ArrayList<Object[]>();
		        for (CemeteryArea cemeteryArea : cemeteryAreaList) {
		            areaList.add(new Object[]{cemeteryArea.getId(), cemeteryArea.getName()});
		        }
		        String selectAreaOption = "";
		        //获取选中陵园是否改变
		        String type = getString("type");
		        if (cemeteryAreaList.size() < 1) {
		            areaId = "1";
		        } else {
		            if ((areaId == null || "".equals(areaId)) || type.equals("1")) {
		                areaId = cemeteryAreaList.get(0).getId();
		            }
		        }
		        selectAreaOption = DataTools.getOptionByList(areaList, areaId, false);
		     // 填入排查询内容
		        Map<String, Object> rowMap = new HashMap<String, Object>();
		        rowMap.put("areaId", areaId);
		        rowMap.put("cid", cemeteryId);
		        rowMap.put("isdel", Const.Isdel_No);
		        List<CemeteryRowAndTomb> cemeteryRowList = cemeteryRowService.getRowAndTombList(rowMap, "index_flag desc");
		        model.put("cemeteryRowList", cemeteryRowList);
		        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
		        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
		        String startTime = getString("startTime");
		        String endTime = getString("endTime");
		        Map<String, Object> tombMap = new HashMap<String, Object>();
		        if ("".equals(type)||type==null) {
		        	tombMap.put("cemeteryId", cemeteryId);
		        	tombMap.put("areaId", areaId);
		        	tombMap.put("startTime", startTime);
		        	tombMap.put("endTime", endTime);
		        	/*--销售墓穴数量--*/
					int paymentedTimeresult=reportService.getPaymentedTime(tombMap);
					/*--墓穴费总计--*/
					int paymentedCemeteryresult=reportService.getCemeteryrecord(tombMap);
					/*--维护费总计--*/
					int maintenanceCemeteryresult=reportService.getMaintenancerecord(tombMap);
					/*--雕刻安放费总计--*/
					int payCemeteryresult=reportService.getPaycerecord(tombMap);
					/*--迁移费--*/
					int moveCemeteryresult=reportService.getMoverecord(tombMap);
					/*--封龙门次数--*/
					int closeCemeteryresult=reportService.getCloserecord(tombMap);
					/*--安放量--*/
					int placementCemeteryresult=reportService.getplacementrecord(tombMap);
					model.put("paymentedTimeresult", paymentedTimeresult);
					model.put("paymentedCemeteryresult", paymentedCemeteryresult);
					model.put("maintenanceCemeteryresult", maintenanceCemeteryresult);
					model.put("payCemeteryresult", payCemeteryresult);
					model.put("moveCemeteryresult", moveCemeteryresult);
					model.put("closeCemeteryresult", closeCemeteryresult);
					model.put("placementCemeteryresult", placementCemeteryresult);
				}
				PageInfo<BuyRecord> page = reportService.getsaleRecord(tombMap, pages[0], pages[1], "grave_info asc");
				model.put("startTime", startTime);
		        model.put("endTime", endTime);
		        model.put("cemetery", cemetery);
		        model.put("cemeteryArea", cemeteryArea);
		        model.put("cemeteryId", cemeteryId);
		        model.put("areaId", areaId);
		        model.put("page", page);
		        model.put("selectCemeteryOption", selectCemeteryOption);
		        model.put("selectAreaOption", selectAreaOption);
				
				return new ModelAndView("cemetery/reportStatistics/salesStatistics",model);

		}

/** 墓区汇总
*
* @return
* */
		@RequestMapping(params="method=summaryOfCemeteries")
		public ModelAndView summaryOfCemeteriesRecord(){
			Map<String, Object> model = new HashMap<String, Object>();
			model = getModel(model);
			 // 获得页面页码信息
	        int[] pages = getPage();
	        // 填入陵区查询内容
	        Map<String, Object> cemeteryMaps = new HashMap<String, Object>();
	        cemeteryMaps.put("isdel", Const.Isdel_No);
	        List<Cemetery> cemeteryList = cemeteryService.getCemeteryList(cemeteryMaps, "");
	        String cemeteryId = getString("selectCemetery");
	        List<Object[]> list = new ArrayList<Object[]>();
	        for (Cemetery cemetery : cemeteryList) {
	            list.add(new Object[]{cemetery.getId(), cemetery.getName()});
	        }
	        String selectCemeteryOption = "";
	        if (cemeteryId == null || "".equals(cemeteryId)) {
	            cemeteryId = cemeteryList.get(0).getId();
	        }
	        Map<String, Object> maps = new HashMap<String, Object>();
	        maps.put("cemeteryId", cemeteryId);
	        selectCemeteryOption = DataTools.getOptionByList(list, cemeteryId, false);
	        List<CemeteryArea> cemeteryAreaList = cemeteryAreaService.getCemeteryAreaList(maps, "");
	        //获取选中区域ID
	        String areaId = getString("selectArea");
	        List<Object[]> areaList = new ArrayList<Object[]>();
	        for (CemeteryArea cemeteryArea : cemeteryAreaList) {
	            areaList.add(new Object[]{cemeteryArea.getId(), cemeteryArea.getName()});
	        }
	        String selectAreaOption = "";
	        //获取选中陵园是否改变
	        String type = getString("type");
	        if (cemeteryAreaList.size() < 1) {
	            areaId = "1";
	        } else {
	            if ((areaId == null || "".equals(areaId)) || type.equals("1")) {
	                areaId = cemeteryAreaList.get(0).getId();
	            }
	        }
	        selectAreaOption = DataTools.getOptionByList(areaList, areaId, false);
	     // 填入排查询内容
	        Map<String, Object> rowMap = new HashMap<String, Object>();
	        rowMap.put("areaId", areaId);
	        rowMap.put("cid", cemeteryId);
	        rowMap.put("isdel", Const.Isdel_No);
	        List<CemeteryRowAndTomb> cemeteryRowList = cemeteryRowService.getRowAndTombList(rowMap, "index_flag desc");
	        model.put("cemeteryRowList", cemeteryRowList);
	        Cemetery cemetery = cemeteryService.getCemeteryById(cemeteryId);
	        CemeteryArea cemeteryArea = cemeteryAreaService.getCemeteryAreaById(areaId);
	        String startTime = getString("startTime");
	        String endTime = getString("endTime");
	        Map<String, Object> tombMap = new HashMap<String, Object>();
	        if (("".equals(type)||type==null)&&!startTime.equals("")&&startTime!=null) {
	        	tombMap.put("cemeteryId", cemeteryId);
	        	tombMap.put("areaId", areaId);
	        	tombMap.put("startTime", startTime);
	        	tombMap.put("endTime", endTime);
				/*--已建成墓穴数量--*/
				int tombresult=reportService.getTomb(tombMap);
				/*--历史所有已售出墓穴数量--*/
				int paymentedNumber=reportService.PaymentedNumber(tombMap);
				/*--历史所有已售出墓穴金额总计--*/
				int paymentedMoney=reportService.PaymentedMoney(tombMap);
				/*--历史售出墓穴最小金额--*/
				int paymentedMinMoney=reportService.PaymentedMinMoney(tombMap);
				/*--历史售出墓穴最大金额--*/
				int paymentedMaxMoney=reportService.PaymentedMaxMoney(tombMap);
				/*--所有安放量--*/
				int placement=reportService.getplacementrecords(tombMap);
				//未售出量
				int unsaleNumber=tombresult-paymentedNumber;
				model.put("unsaleNumber", unsaleNumber);
				model.put("tombresult", tombresult);
				model.put("paymentedNumber", paymentedNumber);
				model.put("paymentedMoney", paymentedMoney);
				model.put("paymentedMinMoney", paymentedMinMoney);
				model.put("paymentedMaxMoney", paymentedMaxMoney);
				model.put("placement", placement);
	        }
			PageInfo<BuyRecord> page = reportService.getsalemoneyRecord(tombMap, pages[0], pages[1], "grave_info asc");
			model.put("startTime", startTime);
	        model.put("endTime", endTime);
	        model.put("cemetery", cemetery);
	        model.put("cemeteryArea", cemeteryArea);
	        model.put("cemeteryId", cemeteryId);
	        model.put("areaId", areaId);
	        model.put("page", page);
	        model.put("selectCemeteryOption", selectCemeteryOption);
	        model.put("selectAreaOption", selectAreaOption);
		return new ModelAndView("cemetery/reportStatistics/summaryOfCemeteries",model);

}
}
