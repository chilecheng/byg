package com.hz.controller.payDivision;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.payDivision.BatchLosses;
import com.hz.entity.ye.ItemOrder;
import com.hz.service.base.BillUnitService;
import com.hz.service.payDivision.BatchLossesService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;


/**
 * 收费科批量挂账
 * @author jgj
 *
 */
@Controller
@RequestMapping("/batchLosses.do")
public class BatchLossesController extends BaseController{
	@Autowired
	private BatchLossesService batchLossesService;
	@Autowired
	private BillUnitService billUnitService;
	
	
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "batchLosses.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listBatchLosses();
    }

	/**
	 * 获取挂账业务列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listBatchLosses() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String,Object>();		
		
		maps.put("tickFlag", Const.Is_Yes);//是挂账
		maps.put("billPayFlag", Const.IsHave_No);//收费状态
		maps.put("checkFlag", Const.Check_Yes);//已审核
		
		PageInfo<BatchLosses> page=batchLossesService.getBatchLossesPageInfo(maps,pages[0],pages[1],"");
		
		model.put("page", page);
		model.put("method","list");
		return new ModelAndView("ye/charge_department/entrust/entrust",model);
    }
    
    /**
     * 链接到收费详情页
     */
    @RequestMapping(params = "method=edit")
    public ModelAndView editBatchLosses(){
    	
		Map<String,Object> model=new HashMap<String,Object>();			
		//挂账单位ID
		String id = getString("id");
		//未收费
		byte billPayFlag=Const.IsHave_No;
		//查询条件
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("billPayFlag", billPayFlag);
		maps.put("billUnitId", id);
		maps.put("tickFlag", Const.Is_Yes);
		maps.put("checkFlag", Const.Check_Yes);//已审核
		List<ItemOrder> orderList=batchLossesService.getBatchLossesListByMap(maps);
		int size=orderList.size();
		double allTotal=0.0;
		for(int i=0;i<size;i++){
			allTotal+=orderList.get(i).getTotal();
		}
		//结算时间
		Timestamp nowTime=new Timestamp(System.currentTimeMillis());
		String billName=billUnitService.getBillUnitId(id).getName();
//		PageInfo page=commissionOrderService.getCommissionOrderPageInfo(maps, pages[0], pages[1], "");
		

		//将选中的挂账基本信息返回并显示
		model.put("billList", orderList);	
		model.put("size", size);
		model.put("allTotal", allTotal);
		model.put("id",id);
		model.put("nowTime", nowTime);
		model.put("billName", billName);
		model.put("method", "edit");
		return new ModelAndView("ye/charge_department/entrust/payment",model);
    }
    /**
     * 保存批量挂账收费
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存批量挂账收费")
    @ResponseBody
    public JSONObject BatchLosses(){
		JSONObject json = new JSONObject();
    	try {
//    		JSONObject json2 = JSONObject.parseObject(getString("saveData"));
//    		
    	String billUnitId=getString("billUnitId");
    	String [] itemIds=getArrayString("saveInner");
    	double total=getDouble("total");
    	Timestamp payTime=getTimeM("payTime");	
    	String id=UuidUtil.get32UUID();
    	String[] commissionId=getArrayString("commissionId");
    	//保存
    	batchLossesService.saveBatchLosses(billUnitId,itemIds,total,payTime,id,commissionId);
    	
    	setJsonBySuccess(json, "保存成功","batchLosses.do");    	
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
			
		}
		return json;
    }
}