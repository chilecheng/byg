package com.hz.controller.payDivision;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Item;
import com.hz.entity.payDivision.NonCommissioned;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BuyWreathRecord;
import com.hz.entity.ye.ListFuneralOrderRecord;
import com.hz.service.base.ItemService;
import com.hz.service.payDivision.NonCommissionedService;
import com.hz.service.system.MenuService;
import com.hz.service.ye.AshesRecordDService;
import com.hz.service.ye.AshesRecordService;
import com.hz.service.ye.BuyWreathRecordDService;
import com.hz.service.ye.BuyWreathRecordService;
import com.hz.service.ye.FuneralBuyService;
import com.hz.util.Const;


/**
 * 收费科非委托业务收费
 * @author jgj
 *
 */
@Controller
@RequestMapping("/nonCommissioned.do")
public class NonCommissionedController extends BaseController{
	@Autowired
	private MenuService menuService;
 	//非委托业务
	@Autowired
	private NonCommissionedService nonCommissionedService;
	//骨灰寄存详情
	@Autowired
	private AshesRecordDService ashesRecordDService;
	//骨灰寄存
	@Autowired
	private AshesRecordService ashesRecordService;
	
	//花圈花篮具体记录
	@Autowired
	private BuyWreathRecordDService buyService;
	//花圈花篮购买
	@Autowired
	private BuyWreathRecordService buyWreathService;
	//丧葬用品购买
	@Autowired
	private FuneralBuyService funeralBuyService;
	@Autowired
	private ItemService itemService;
	
	
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "nonCommissioned.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listNonCommissioned();
    }

	/**
	 * 获取非委托业务列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listNonCommissioned() {
		Map<String,Object> model=new HashMap<String,Object>();
		String searchVal=getString("searchVal");
		byte type=getByte("findBy");
		String startTime=getString("startTime");
		String endTime=getString("endTime");
		String checkType=getString("checkType");
		Map<String,Object> paramMap=new HashMap<String, Object>();
		if(checkType!=null && "payNo".equals(checkType)){			
			paramMap.put("payFlag", Const.Pay_No);
		}
		if(checkType!=null && "payYes".equals(checkType)){
			paramMap.put("payFlag", Const.Pay_Yes);
		}
		//搜索值非空
		String searchType=getString("searchType");
		if(searchVal!=null && !"".equals(searchVal)){
			//String searchType="";
			if(type==Const.Search_Type_Name){
				 searchType="findByName";
			}else if(type==Const.Search_Type_Number){
				 searchType="findByNumber";
			}else if(type==Const.Search_Type_BuyNumber){
				searchType="findByBuyNumber";
			}else if(type==Const.Search_Type_PayType){
				searchType="findByPayType";
				if(searchVal.equals("代收")){
					searchVal="4";
				}else if(searchVal.equals("挂账")){
					searchVal="5";
				}else if(searchVal.equals("支付宝")){
					searchVal="3";
				}else if(searchVal.equals("刷卡")){
					searchVal="2";
				}else if(searchVal.equals("现金")){
					searchVal="1";
				}
			}
			paramMap.put(searchType, searchVal);
			paramMap.put("searchVal", searchVal);
		}
		//开始时间非空
		if(startTime!=null && !"".equals(startTime)){
			paramMap.put("startTime", startTime);
		}
		//结束时间非空
		if(endTime!=null && !"".equals(endTime)){
			paramMap.put("endTime", endTime+"23:59:59");
		}
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容	
		String searchOption=Const.getSearchTypeOptionByByte(type,new byte[]{Const.Search_Type_BuyNumber,Const.Search_Type_Number,Const.Search_Type_Name,Const.Search_Type_PayType});
		PageInfo<NonCommissioned> page=nonCommissionedService.getNonCommissionedPageInfo(paramMap, pages[0], pages[1], "non_serial_number desc");
		model.put("page", page);
		model.put("startTime", startTime);
		model.put("endTime", endTime);
		model.put("searchOption",searchOption);
		model.put("method","list");
		model.put("searchVal", searchVal);
		model.put("checkType", checkType);
		return new ModelAndView("ye/charge_department/undelegate/undelegate",model);
    }
    
    /**
     * 链接到收费详情页
     */
    @RequestMapping(params = "method=edit")
    public ModelAndView editNonCommissioned(){
		Map<String,Object> model=new HashMap<String,Object>();
		String userName=getCurrentUser().getName();
		String id=getString("id");
		String fid=getString("fid");
		int dept=2;
		if(fid!=null && !"".equals(fid)){
			fid=fid.substring(fid.indexOf("ul_")+3);	
			String deptName=menuService.getMenuById(fid).getName();
			if("业务一科".equals(deptName)){
				dept=1;
			}
		}
		String name=getString("code4");
		String serialNumber=getString("code2");
		System.out.println(serialNumber);
		System.out.println(dept);
		System.out.println(fid);
		String total=getString("code8");
		//截取流水号后4位,作为查询对应数据表的判断
		String type="";
		if(serialNumber.length()>=4){
			type=serialNumber.substring(serialNumber.length()-4, serialNumber.length());			
		}
		List<?> list=new ArrayList();
		Item item=itemService.getItemByHelpCode("423");//获取骨灰寄存费,寄存费可在页面中自行修改
		double pice=0;
		String ghName="骨灰寄存费";
		if(item!=null){
			pice=item.getPice();
			ghName=item.getName();
		}
		//以下 根据流水号来查不同的数据表
		if("SZYP".equals(type)){//丧葬用品
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("buyRecordId", id);
			list=funeralBuyService.getBuyGoodsRecordFee(paramMap);
			
			ListFuneralOrderRecord funeralOrder=funeralBuyService.getBuyGoodsRecordFeeById(id);
			model.put("buyName", funeralOrder.getTakeName());//默认购买人为付款人
			if(funeralOrder.getActuallyPaid()!=0.0){
				double realMoney=funeralOrder.getActuallyPaid();
				model.put("realMoney", realMoney);
			}
				
		}else if("HQHL".equals(type)){//花圈花篮
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("buyWreathRecordId", id);
			BuyWreathRecord wreathRecord=buyWreathService.getBuyWreathRecordId(id);
			model.put("buyName", wreathRecord.getOrderUser());//默认购买人为付款人
			if(wreathRecord.getActuallyPaid()!=0.0){
				double realMoney=wreathRecord.getActuallyPaid();
				model.put("realMoney", realMoney);
			}
			list=buyService.getBuyWreathRecordDFee(paramMap);
		}else if("GHJC".equals(type)){//骨灰寄存
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("recordId", id);
			AshesRecord ashes=ashesRecordService.getAshesRecordById(id);
			model.put("buyName", ashes.getsName());//默认寄存人为付款人
			list=ashesRecordDService.getAshesRecordDFee(paramMap);
			double realMoney=0.0;
			for(int i=0;i<list.size();i++){
				AshesRecordD ad=(AshesRecordD)list.get(i);
				ad.setName(ghName);//造个假数据,统一格式
				ad.setPice(pice);//同上
				realMoney+=ad.getActuallyPaid();
			}
			if(realMoney!=0.0){
				model.put("realMoney", realMoney);
			}
		}
		String payTypeOption=Const.getPayTypeOptionByByte(Const.PayType_Cash, new byte[]{Const.PayType_Cash,Const.PayType_Card,Const.PayType_Alipay,Const.PayType_ZhuanZhi});
		String payTypeOnly=Const.getAliPayOption(Const.PayType_Alipay, false);
		Timestamp nowTime=new Timestamp(System.currentTimeMillis());

		//将选中的挂账基本信息返回并显示
		model.put("list", list);
		model.put("method", "edit");
		model.put("name", name);
		model.put("now", nowTime);
		model.put("total", total);
		model.put("id", id);
		model.put("payType", payTypeOption);
		model.put("aliPay", payTypeOnly);
		model.put("serialNumber", serialNumber);
		model.put("userName", userName);
		model.put("dept", dept);
		return new ModelAndView("ye/charge_department/undelegate/undelegateDetail",model);
    }
    
    /**
     * 点击姓名链接到详情页 查看
     */
    @RequestMapping(params = "method=show")
    public ModelAndView showNonCommissioned(){
		Map<String,Object> model=new HashMap<String,Object>();	
		String userName=getCurrentUser().getName();
		String id=getString("id");
		String name=getString("name");
		String serialNumber=getString("serialNumber");
		double total=getDouble("total");
		String payName="";
		String payType="";
		double giveChange=0.0;
		double actualAmount=0.0;
		Timestamp payTime=null;
		String comment="";
		//截取流水号后4位,作为查询对应数据表的判断
		String type=serialNumber.substring(serialNumber.length()-4, serialNumber.length());
		Item item=itemService.getItemByHelpCode("423");//获取骨灰寄存费,寄存费可在页面中自行修改
		double pice=0;
		String ghName="骨灰寄存费";
		if(item!=null){
			pice=item.getPice();
			ghName=item.getName();
		}
		
		List list=new ArrayList();		
		//以下 根据流水号来查不同的数据表
		if("SZYP".equals(type)){//丧葬用品
			//基本信息
			ListFuneralOrderRecord funeralOrder= funeralBuyService.getBuyGoodsRecordFeeById(id);
			payName=funeralOrder.getPayName();
			payTime=funeralOrder.getPayTime();
			payType=funeralOrder.getPayTypeName();
			giveChange=funeralOrder.getGiveChange();
			actualAmount=funeralOrder.getActualAmount();
			comment=funeralOrder.getRemarktext();
			//记录信息
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("buyRecordId", id);
			list=funeralBuyService.getBuyGoodsRecordFee(paramMap);
		}else if("HQHL".equals(type)){//花圈花篮
			BuyWreathRecord wreathRecord= buyWreathService.getBuyWreathRecordId(id);
			payName=wreathRecord.getPayName();
			payTime=wreathRecord.getPayTime();
			payType=wreathRecord.getPayTypeName();
			giveChange=wreathRecord.getGiveChange();
			actualAmount=wreathRecord.getActualAmount();
			comment=wreathRecord.getComment();
			
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("buyWreathRecordId", id);
			list=buyService.getBuyWreathRecordDFee(paramMap);
		}else if("GHJC".equals(type)){//骨灰寄存
			AshesRecord ashesRecord=ashesRecordService.getAshesRecordById(id);
			comment =ashesRecord.getComment();
			
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("recordId", id);
			list=ashesRecordDService.getAshesRecordDFee(paramMap);
			for(int i=0;i<list.size();i++){
				if(i==0){
					AshesRecordD asd=(AshesRecordD)list.get(i);
					payName=asd.getPayName();
					payTime=asd.getPayTime();
					payType=asd.getPayTypeName();
					giveChange=asd.getGiveChange();
					actualAmount=asd.getActualAmount();
				}
				AshesRecordD ad=(AshesRecordD)list.get(i);
				ad.setName(ghName);//造个假数据,统一格式
				ad.setPice(pice);//同上
			}
		}

		//将选中的挂账基本信息返回并显示
		
		model.put("payName", payName);
		model.put("payTime", payTime);
		model.put("comment", comment);
		model.put("payType", payType);
		model.put("giveChange", giveChange);
		model.put("actualAmount", actualAmount);
		
		model.put("list", list);
		model.put("method", "show");
		model.put("name", name);
		model.put("total", total);
		model.put("id", id);
		model.put("serialNumber", serialNumber);
		model.put("userName", userName);
		return new ModelAndView("ye/charge_department/undelegate/showUndelegate",model);
    }
    
    /**
     * 撤消收费
     */
    @RequestMapping(params = "method=revoke")
    @SystemControllerLog(description = "撤消非委托业务收费")
    @ResponseBody
    public JSONObject revokeNonCommissioned(){
		JSONObject json = new JSONObject();
		try{
		String id=getString("id");
		String serialNumber=getString("code2");
		//截取流水号后4位,作为查询对应数据表的判断
		String type=serialNumber.substring(serialNumber.length()-4, serialNumber.length());
		nonCommissionedService.updataRevoke(type,id);
		setJsonBySuccess(json, "操作成功",true);    	
	} catch (Exception e) {
		e.printStackTrace();
		setJsonByFail(json, "操作失败，错误"+e.getMessage());
		
	}
	return json;
    }
    
    /**
     * 保存数据
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存非委托业务收费")
    @ResponseBody
    public JSONObject SaveNonCommissioned(){
		JSONObject json = new JSONObject();
    	try {
    	String id=getString("id");
    	String payName=getString("payName");
    	Timestamp payTime=getTimeM("payTime");
    	String comment=getString("comment");
    	double actualAmount=getDouble("realMoney");
    	double giveChange=getDouble("changeMoney");
    	String serialNumber=getString("serialNumber");
    	byte payType=getByte("payType");
    	//截取流水号后4位,作为对应数据表的判断
		String type=serialNumber.substring(serialNumber.length()-4, serialNumber.length());
		nonCommissionedService.saveNonCommissioned(id,payName,payTime,comment,actualAmount,giveChange,payType,type);		
    
    	   	
    	setJsonBySuccess(json, "保存成功","nonCommissioned.do");    	
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
			
		}
		return json;
    }
}