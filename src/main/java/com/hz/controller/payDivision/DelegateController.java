package com.hz.controller.payDivision;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Jeneration;
import com.hz.entity.payDivision.BusinessFees;
import com.hz.entity.socket.Notice;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.DelegateRecord;
import com.hz.entity.ye.DelegateRecordNew;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.PrintRecord;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.service.base.JenerationService;
import com.hz.service.payDivision.BusinessFeesService;
import com.hz.service.payDivision.DelegateService;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.ye.BaseReductionDService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FireAttestPrintService;
import com.hz.service.ye.HardReductionDService;
import com.hz.service.ye.HardReductionService;
import com.hz.service.ye.ItemOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;


/**
 * 委托业务收费
 * @author gpf
 *
 */
@Controller
@RequestMapping("/delegate.do")
public class DelegateController extends BaseController {
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	private DelegateService delegateService;
	@Autowired
	private FireAttestPrintService firePrintService;
	@Autowired
	private BusinessFeesService businessFeesService;
	@Autowired
	private JenerationService jenerationService;
	@Autowired
	private HardReductionService hardReductionService;
	/**
	 * websocket 收费完成后 给各用户推送消息
	 */
	/*@Autowired
	private SystemWebSocketHandler socketHandler;*/
	@Autowired
	private HardReductionDService hardReductionDService;
	@Autowired
	private BaseReductionDService baseReductionDService;
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "delegate.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listDelegate();
    }
	/**
	 * 获取非挂账业务列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listDelegate() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		String searchType=getString("select");
		//填入查询内容
		Map<String,Object> maps=new HashMap<String,Object>();
		String selectValue=getString("selectValue");
		//String name = getString("name");
		String startTime=getString("startTime");
		maps.put("selectValue", selectValue);
		maps.put("select", searchType);
		maps.put("startTime", startTime);
		String endTime=getString("endTime");
		
		if(!"".equals(endTime)){
			maps.put("endTime", endTime+"23:59:59");
		}
		String beginTime=getString("beginTime");
		maps.put("beginTime", beginTime);
		String overTime=getString("overTime");
		if(!"".equals(overTime)){
			maps.put("overTime", overTime+"23:59:59");
		}
		maps.put("tickFlag",Const.TickFlag_Delegate);
		maps.put("checkFlag", Const.Check_Yes);
		
		String type=getString("checkType");
    	maps =  checkType(maps,type);	
    	model.put("checkType", type);
    	
    	if("".equals(selectValue) && "".equals(overTime) && "".equals(endTime)
    			&& "".equals(startTime) && "".equals(beginTime)){
    		maps.put("nullData", 8);//为改善委托业务收费速度，默认无，当有搜索值时才正常显示
    	}
    		// 时间 以审核时间进行搜索(原有方法，太卡，也不符合客户要求)
//		PageInfo<DelegateRecord> page=delegateService.getdelegateRecordPageInfo(maps,pages[0],pages[1],"co.creat_time desc");
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_Code});
		model.put("searchOption", searchOption);
		//新加(收费测试：最新分组功能)
		PageInfo<DelegateRecordNew> page1=delegateService.getItemOrderPageInfo(maps,pages[0],pages[1],"newcom.creat_time desc");
		model.put("page", page1);
		
		
		
		
		return new ModelAndView("ye/charge_department/delegate/delegate",model);
    }
    /**
     * 收费页面
     * @return
     */
    @RequestMapping(params = "method=detail")
    public ModelAndView showDelegateDetail() {
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String time=getString("payTime");
		String orderNumber=getString("orderNumber");
		System.out.println(time);
		String total1=getString("total");
		String  total=total1.substring(0,total1.length()-1);
		String id = getString("id");
//		byte checkFlag = Const.Check_Yes;
//		byte tickFlag  = Const.TickFlag_Delegate;
//		DelegateRecord delegrate = new DelegateRecord();
//		delegrate = delegateService.getorderById(id,checkFlag,tickFlag);
		
		
//		Double stotal = getDouble("stotal");
		
		//更新查看次数
		/*CommissionOrder com=commissionOrderService.getCommissionOrderById(id);
		if(com.getViewsUnpay()==0){
			com.setViewsUnpay(Const.Pay_Yes);
			commissionOrderService.updateNumber(com);
		}*/
		commissionOrderService.updateNumber3(id,Const.Pay_Yes);
		CommissionOrder order=commissionOrderService.getCommissionOrderById(id);
		String payTypeOption=Const.getPayTypeOptionByByte(Const.PayType_Cash, new byte[]{Const.PayType_Cash,Const.PayType_Card,Const.PayType_Alipay,Const.PayType_ZhuanZhi});
		//委托业务 基本收费项   丧葬用品
		Map<String,Object> paramMap=new HashMap<String, Object>();
		paramMap.put("commissionOrderId", id);
		paramMap.put("typeFlag", 1);
		paramMap.put("sort", "payTime");//排序
		paramMap.put("tickFlag", Const.TickFlag_Delegate);
		List<ItemOrder> list=itemOrderService.getDelegrateItemOrderList(paramMap);	
		//服务项目
		Map<String,Object> sMap=new HashMap<String, Object>();
		sMap.put("commissionOrderId", id);
		sMap.put("typeFlag", 2);
		sMap.put("sort", "payTime");//排序
		paramMap.put("tickFlag", Const.TickFlag_Delegate);
		List<ItemOrder> list4=itemOrderService.getDelegrateItemOrderList(sMap);	
		//困难减免项	
		Map<String,Object> param=new HashMap<String, Object>();
		param.put("commissionOrderId", id);
		List<HardReductionD> list2 = hardReductionDService.getHardReductionDFeeList(param);
		//基本减免项
		List<BaseReductionD> list3=	baseReductionDService.getBaseReductionDFList(param);
	
		// 声明 当下 时间  作为默认时间 
		Timestamp nowTime=new Timestamp(System.currentTimeMillis());
		//model.put("currentTime",DateTools.getThisDate());
		model.put("fName", order.getfName());
		model.put("currentTime", nowTime);
		model.put("name", order.getName());
		model.put("stotal",total);
		model.put("orderId", order.getId());
		model.put("code",order.getCode());
		//获取办理人姓名
      	model.put("payeeId", getCurrentUser().getName());
      	//业务单号
      	model.put("serviceCode", order.getCode());
      	
      	model.put("picrDiffer", total);
      	model.put("list", list);
      	model.put("list2", list2);
      	model.put("list3", list3);	
      	model.put("list4", list4);	
      	model.put("payTypeOption", payTypeOption);
      	model.put("orderNumber", orderNumber);
      	model.put("method", "save");
//      	if(delegateService.getBoolPayed(id)!=0){
//      		return new ModelAndView("ye/charge_department/delegate/delegateDetail",model);	
//      	}else{
      	return new ModelAndView("ye/charge_department/delegate/firstDelegateDetail",model);	
//      	}
      	
    	
    }
    
    
    /**
     *   委托信息详细单 页面
     */
    @RequestMapping(params = "method=showchar")
    public ModelAndView showAllDelegateDetail() {
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String payNumber=getString("payNumber");//收费流水号
		String orderNumber=getString("orderNumber");//业务订单流水号
		
		String id=getString("id");
		CommissionOrder com=commissionOrderService.getCommissionOrderById(id);
		model.put("name", com.getName());
		
		//基本减免
//		Map<String,Object> baseMap=new HashMap<String,Object>();
//		baseMap.put("orderNumber", orderNumber);
//		List<BaseReductionD> baseList=baseReductionDService.getBaseReductionDFList(baseMap);
		double baseTotal=baseReductionDService.getbaseTotalByOrderNumber(orderNumber);
		//困难减免
//		Map<String,Object> hardMap=new HashMap<String,Object>();
//		hardMap.put("orderNumber", orderNumber);
//		List<HardReductionD> hardList=hardReductionDService.getHardReductionDFeeList(hardMap);
		double hardTotal=hardReductionDService.gethardTotalByOrderNumber(orderNumber);
		//服务项目
		Map<String,Object> sMap=new HashMap<String,Object>();
		sMap.put("orderNumber", orderNumber);
		sMap.put("itemType", Const.Type_Service);//服务项目
		sMap.put("commissionOrderId", id);
		sMap.put("sort","payTime");//排序
		sMap.put("tickFlag", Const.TickFlag_Delegate);
		List<ItemOrder> serviceList=itemOrderService.getItemOrderList(sMap);
		double serviceTotal=itemOrderService.getTotalByOrderNumber(sMap);
		//丧葬用品
		Map<String,Object> aMap=new HashMap<String,Object>();
		aMap.put("orderNumber", orderNumber);
		aMap.put("itemType", Const.Type_Articles);//丧葬用品
		aMap.put("commissionOrderId", id);
		aMap.put("sort","payTime");//排序
		aMap.put("tickFlag", Const.TickFlag_Delegate);
		List<ItemOrder> articlesList=itemOrderService.getItemOrderList(aMap);
		double articlesTotal=itemOrderService.getTotalByOrderNumber(aMap);
		//收费记录
		BusinessFees businessFees= businessFeesService.getBusinessFeesByPayNumber(payNumber);
		
      	model.put("baseTotal", baseTotal);
      	model.put("hardTotal", hardTotal);
      	model.put("serviceTotal", serviceTotal);
      	model.put("articlesTotal", articlesTotal);
//      	model.put("baseList", baseList);
//      	model.put("hardList", hardList);
      	model.put("serviceList", serviceList);
      	model.put("articlesList", articlesList);
      	model.put("businessFees", businessFees);
      	//判断是否是分第一次付费
      	
      	return new ModelAndView("ye/charge_department/delegate/charDelegate",model);		
      	
    }
    
    
    /**
     * 保存 相应的 收费记录
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存业务订单相应的收费记录")
    @ResponseBody
    public JSONObject savedelegateRecord(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("orderId");//火化委托单id
    		String serviceCode=getString("serviceCode");
    		Timestamp payTime=getTimeM("payTime");
    		String payeeId=getCurrentUser().getName();
    		String payName=getString("payName");
    		double payAmount=getDouble("realMoney");
    		byte payType=getByte("payWay");
    		String comment=getString("comment");
    		String orderNumber=getString("orderNumber");
    		String payNumber=jenerationService.getJenerationCode(Jeneration.SFJL_TYPE);
    		//考虑收费时页面没有获取到订单流水号，在此做限制
    		if("".equals(orderNumber)||orderNumber ==null){
    			setJsonByFail(json, "订单流水号获取失败，请重新收费！");
    			return json;
    		}
    		//收费前判断一下是否是最新的记录，如果不是需要重新收费
    		Map<String,Object> paramMap=new HashMap<String,Object>();
    		paramMap.put("orderNumber", orderNumber);
    		List<ItemOrder> listn=itemOrderService.getItemOrderList(paramMap);
    		if(listn.size()<=0){
    			setJsonByFail(json, "该收费项目已发生变更，请返回重新收费！");
    			return json;
    		}else{
    			//收费表
    			BusinessFees businessFees=new BusinessFees();
    			businessFees.setId(UuidUtil.get32UUID());
    			businessFees.setOrderId(id);
    			businessFees.setPayNumber(payNumber);
    			businessFees.setServiceCode(serviceCode);
    			businessFees.setPayAmount(payAmount);
    			businessFees.setPayName(payName);
    			businessFees.setPayTime(payTime);
    			businessFees.setPayType(payType);
    			businessFees.setComment(comment);
    			businessFees.setPayeeId(payeeId);
    			businessFees.setOrderNumber(orderNumber);
    			
    			//基本减免表
    			Map<String,Object> baseMap=new HashMap<String,Object>();
    			baseMap.put("payTime", null);
    			baseMap.put("commissionOrderId", id);
    			List<BaseReductionD> baseList=baseReductionDService.getBaseReductionDFList(baseMap);
    			//困难减免表
    			Map<String,Object> hardMap=new HashMap<String,Object>();
    			hardMap.put("payTime", null);
    			hardMap.put("commissionOrderId", id);
    			List<HardReductionD> hardList=hardReductionDService.getHardReductionDFeeList(hardMap);
    			
    			String hardCheck=hardReductionService.getHardCheckFlagByComId(id);
    			if(hardCheck==null ||"1".equals(hardCheck)){//没有困难减免或已审核则收费
    				//项目记录表
    				//获取 itemOrder的信息 将 item 的相关信息进行更新
    				List<ItemOrder> list = new ArrayList<ItemOrder>();
    				Map<String,Object> maps=new HashMap<String,Object>();
    				maps.put("commissionOrderId", id);
    				//此处新加条件，只选未收费的，否则所有记录都变成同一天收费了
    				maps.put("payFlag", Const.Pay_No);
    				//再加个条件，符合的订单流水号，否则会把其他该用户的订单也收费了
    				maps.put("orderNumber", orderNumber);
    				list = itemOrderService.getItemOrderList(maps);
    				//火化委托单收费记录表
    				//获取火化委托单信息将火化委托单  pay_flag  变为已付  
    				//有个特殊情况，如果还有未收费的，不能变成已付状态
    				maps.remove("orderNumber");
    				maps.put("notOrderNumber", orderNumber);
    				List<ItemOrder> noPayItem=itemOrderService.getItemOrderList(maps);
    				CommissionOrder commissionOrder = new CommissionOrder();
					commissionOrder=commissionOrderService.getCommissionOrderById(id);
    				if(noPayItem.size()<=0){//没有未收费的记录，才改状态
    					commissionOrder.setPayFlag(Const.Pay_Yes);
    					Map<String,Object> map=new HashMap<String,Object>();
    					map.put("cid", id);
    					List<PrintRecord> printList=firePrintService.getPrintRecordByMap(map);
    					if(commissionOrder.getCremationFlag()==Const.furnace_flag_Yes){//已火化
    						if(printList.size()>0){//有火化证明记录
    							commissionOrder.setScheduleFlag(Const.schedule_flag_4);
    						}else{
    							commissionOrder.setScheduleFlag(Const.schedule_flag_3);
    						}
    						
    					}else if(commissionOrder.getCremationFlag()==Const.furnace_flag_No){//未火化
    						commissionOrder.setScheduleFlag(Const.schedule_flag_2);
    					}else{//如有其他情况，统一视为未火化处理（应该没有这种情况）
    						commissionOrder.setScheduleFlag(Const.schedule_flag_2);
    					}
    				}
    				//事务控制收费及相关修改
    				businessFeesService.saveBusinessFees(businessFees, commissionOrder, list, payTime, baseList,hardList,orderNumber);
    				
    				setJsonBySuccess(json, "收费成功");
    			}else{
    				setJsonByFail(json, "收费失败，困难减免金额过大需经领导审核通过！");
    			}
    		}
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "收费失败，错误"+e.getMessage());
		}
		return json;
    }
    
    
    /**
     * 撤销收费并做相应更改
     * @return
     */
    @RequestMapping(params = "method=cancle")
    @SystemControllerLog(description = "撤销委托 业务订单的收费")
    @ResponseBody
    public JSONObject CanceldelegateRecord(){
    	JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");//委托单id
    		String payNumber=getString("payNumber");//收费流水号
    		String orderNumber=getString("orderNumber");//业务订单流水号
    		if(orderNumber!=null && !"".equals(orderNumber) && payNumber!=null && !"".equals(payNumber)){
    			//基本减免表
//    			Map<String,Object> baseMap=new HashMap<String,Object>();
//    			baseMap.put("orderNumber", orderNumber);
//    			List<BaseReductionD> baseList=baseReductionDService.getBaseReductionDFList(baseMap);
    			//困难减免表
//    			Map<String,Object> hardMap=new HashMap<String,Object>();
//    			hardMap.put("orderNumber", orderNumber);
//    			List<HardReductionD> hardList=hardReductionDService.getHardReductionDFeeList(hardMap);
    			//火化委托单
    			//将火化委托单的支付状态变为未支付
    			CommissionOrder commissionOrder = new CommissionOrder();
    			commissionOrder=commissionOrderService.getCommissionOrderById(id);
    			commissionOrder.setPayFlag(Const.Pay_No);
    			commissionOrder.setScheduleFlag(Const.schedule_flag_1);
    			//项目订单表
    			List<ItemOrder> list = new ArrayList<ItemOrder>();
    			Map<String,Object> maps=new HashMap<String,Object>();
    			maps.put("orderNumber", orderNumber);
    			list = itemOrderService.getItemOrderList(maps);	
    			Map<String,Object> paramMap=new HashMap<String,Object>();
    			paramMap.put("orderNumber", orderNumber);
    			paramMap.put("payTime", null);
    			businessFeesService.cancleBusinessFees(payNumber, commissionOrder,list,paramMap);
    			setJsonBySuccess(json, "撤销成功", "delegate.do?method=list");
    		}else{
    			setJsonByFail(json, "撤销失败，订单或收费流水号为空，请联系技术人员");
    		}
	    } catch (Exception e) {
	    	e.printStackTrace();
			setJsonByFail(json, "撤销失败，错误"+e.getMessage());
		}
	return json;
    	  	
    }
    

    
    
    /**
     * 封装查询条件 
     * @param type
     * @param checkType
     * @param map
     * @param clickId
     * @return
     */
    public Map<String, Object> checkType(Map<String, Object> map,String checkType){
    	
    	if("stationNo".equals(checkType)){
    		map.put("payFlag", Const.Pay_No);
    	}else if("stationYes".equals(checkType)){
    		map.put("payFlag", Const.Pay_Yes);
    	}else{
    		map.put("payFlag", null);
    	}
    		
    	return map;
    }
	

	
	
	
	

}
