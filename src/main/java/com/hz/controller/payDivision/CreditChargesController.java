package com.hz.controller.payDivision;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.BillUnit;
import com.hz.entity.payDivision.CreditCharges;
import com.hz.entity.payDivision.CreditChargesView;
import com.hz.entity.payDivision.NonCommissioned;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BuyWreathRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.ListFuneralOrderRecord;
import com.hz.service.base.BillUnitService;
import com.hz.service.payDivision.CreditChargesViewService;
import com.hz.service.payDivision.NonCommissionedService;
import com.hz.service.system.MenuService;
import com.hz.service.ye.AshesRecordDService;
import com.hz.service.ye.AshesRecordService;
import com.hz.service.ye.BuyWreathRecordDService;
import com.hz.service.ye.BuyWreathRecordService;
import com.hz.service.ye.ChargeAccountService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FuneralBuyService;
import com.hz.service.ye.ItemOrderService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;


/**
 * 收费科挂账收费业务
 * @author jgj
 *
 */
@Controller
@RequestMapping("/creditCharges.do")
public class CreditChargesController extends BaseController{
	@Autowired
	private CreditChargesViewService creditChargesViewService;
	@Autowired
	private ChargeAccountService chargeAccountService;
 	//非委托业务
	@Autowired
	private NonCommissionedService nonCommissionedService;
	//服务和丧葬服品 项目
	@Autowired
	private ItemOrderService itemOrderService;
	//挂账单位
	@Autowired
	private BillUnitService billUnitService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "creditCharges.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listCreditCharges();
    }

	/**
	 * 获取非委托业务列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listCreditCharges() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		
		//挂账收费条件
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("tickFlag", Const.Is_Yes);//是挂账
//		maps.put("billPayFlag", Const.IsHave_No);//收费状态
		maps.put("checkFlag", Const.Check_Yes);//已审核
		String startTime=getString("startTime");
		if(!"".equals(startTime)){
			maps.put("startTime", startTime);
		}
		String endTime=getString("endTime");
		if(!"".equals(endTime)){
			maps.put("endTime", endTime+"23:59:59");
		}
		byte type=getByte("select");
		if("1".equals(type)){//死者姓名查询
			maps.put("dName", getString("selectValue"));
		}else if("20".equals(type)){//挂账单位
			maps.put("billUnit", getString("selectValue"));
		}
		String checkType=getString("checkType");
		if("stationYes".equals(checkType)){//已收费
			maps.put("payFlag", Const.IsHave_Yes);
		}else if("stationNo".equals(checkType)){//未收费
			maps.put("payFlag", Const.IsHave_No);
		}
		PageInfo<CreditChargesView> page=creditChargesViewService.getgetCreditChargesViewPageInfo(maps,pages[0],pages[1],"");
		
		//填入查询内容	
		String searchOption=Const.getSearchTypeOptionByByte(type,new byte[]{Const.Search_Type_Name,Const.Search_Type_BillUnit});
		model.put("checkType", checkType);
		model.put("page", page);
		model.put("searchOption",searchOption);
		model.put("method","list");
		return new ModelAndView("ye/charge_department/creditCharges/creditCharges",model);
    }
    
    /**
     * 链接到收费详情页
     */
    @RequestMapping(params = "method=edit")
    public ModelAndView editCreditCharges(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id=getString("id");//此id为 commissionOrderId
		//基本信息
		Timestamp nowTime=new Timestamp(System.currentTimeMillis());
		String payTypeOption=Const.getPayTypeOptionByByte(Const.PayType_Cash, new byte[]{Const.PayType_Cash,Const.PayType_Card,Const.PayType_Alipay,Const.PayType_ZhuanZhi});
		CommissionOrder com=commissionOrderService.getCommissionOrderById(id);
		if(com !=null){
			String dName=com.getName();
			String billId=com.getBillUnitId();
			BillUnit bill=billUnitService.getBillUnitId(billId);
			if(bill!=null){
				String billName=bill.getName();
				model.put("billName", billName);
			}
			model.put("billUnitId",billId);
			model.put("dName", dName);
		}
		
		double total=0.0;
		//挂账记录--服务项目
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("commissionOrderId", id);
		maps.put("tickFlag", Const.Is_Yes);
		maps.put("itemType", Const.Type_Service);
		List<ItemOrder> listOrderService=itemOrderService.getItemOrderList(maps);
		double totalService=0.0;//合计：应收款
		for(int i=0;i<listOrderService.size();i++){
			totalService+=listOrderService.get(i).getTotal();
		}
		//挂账记录--丧葬用品
		Map<String,Object> mapa=new HashMap<String,Object>();
		mapa.put("commissionOrderId", id);
		mapa.put("tickFlag", Const.Is_Yes);
		mapa.put("itemType", Const.Type_Articles);
		List<ItemOrder> listOrderArticles=itemOrderService.getItemOrderList(mapa);
		double totalArticles=0.0;//合计：应收款
		for(int i=0;i<listOrderArticles.size();i++){
			totalArticles+=listOrderArticles.get(i).getTotal();
		}
		total=totalArticles+totalService;
		
		model.put("now", nowTime);
		model.put("totalService", totalService);
		model.put("totalArticles", totalArticles);
		model.put("listOrderService", listOrderService);
		model.put("listOrderArticles", listOrderArticles);
		model.put("total", total);
		model.put("method", "edit");
		model.put("id", id);
		model.put("payTypeOption", payTypeOption);
		return new ModelAndView("ye/charge_department/creditCharges/editCreditCharges",model);
    }
    
    /**
     * 点击姓名链接到详情页 查看
     */
    @RequestMapping(params = "method=show")
    public ModelAndView showCreditCharges(){
    	Map<String,Object> model=new HashMap<String,Object>();
		String id=getString("id");//此id为 commissionOrderId
		//基本信息
		CreditCharges credit=new CreditCharges();
		CreditCharges creditById=creditChargesViewService.getCreditChargesByOrderId(id);
		if(creditById !=null){//判断非空
			credit=creditById;
		}
		Timestamp nowTime=credit.getPayTime();
		String payTypeOption=Const.getPayTypeOptionByByte(credit.getPayMethod(), new byte[]{Const.PayType_Cash,Const.PayType_Card,Const.PayType_Alipay,Const.PayType_ZhuanZhi});
		CommissionOrder com=commissionOrderService.getCommissionOrderById(id);
		if(com !=null){
			String dName=com.getName();
			String billId=com.getBillUnitId();
			BillUnit bill=billUnitService.getBillUnitId(billId);
			if(bill!=null){
				String billName=billUnitService.getBillUnitId(billId).getName();
				model.put("billName", billName);
			}
			model.put("billUnitId",billId);
			model.put("dName", dName);
		}
		double total=0.0;
		//挂账记录--服务项目
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("commissionOrderId", id);
		maps.put("tickFlag", Const.Is_Yes);
		maps.put("itemType", Const.Type_Service);
		List<ItemOrder> listOrderService=itemOrderService.getItemOrderList(maps);
		double totalService=0.0;//合计：应收款
		for(int i=0;i<listOrderService.size();i++){
			totalService+=listOrderService.get(i).getTotal();
		}
		//挂账记录--丧葬用品
		Map<String,Object> mapa=new HashMap<String,Object>();
		mapa.put("commissionOrderId", id);
		mapa.put("tickFlag", Const.Is_Yes);
		mapa.put("itemType", Const.Type_Articles);
		List<ItemOrder> listOrderArticles=itemOrderService.getItemOrderList(mapa);
		double totalArticles=0.0;//合计：应收款
		for(int i=0;i<listOrderArticles.size();i++){
			totalArticles+=listOrderArticles.get(i).getTotal();
		}
		total=totalArticles+totalService;
		
		model.put("now", nowTime);
		model.put("totalService", totalService);
		model.put("totalArticles", totalArticles);
		model.put("listOrderService", listOrderService);
		model.put("listOrderArticles", listOrderArticles);
		
		
		model.put("total", total);
		model.put("credit", credit);
		model.put("method", "show");
		model.put("id", id);
		model.put("payTypeOption", payTypeOption);
		return new ModelAndView("ye/charge_department/creditCharges/showCreditCharges",model);
    }
    
    
    /**
     * 保存数据
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存挂账业务收费")
    @ResponseBody
    public JSONObject SaveCreditCharges(){
		JSONObject json = new JSONObject();
    	try {
    	String id=getString("id");
    	String payName=getString("payName");
    	Timestamp payTime=getTimeM("payTime");
    	String comment=getString("comment");
    	String billUnitId=getString("billUnitId");
    	double payMoney=getDouble("payMoney");
    	double changeMoney=getDouble("changeMoney");
    	byte payType=getByte("payType");
    	
    	CreditCharges creditCharges=new CreditCharges();
    	creditCharges.setId(UuidUtil.get32UUID());
    	creditCharges.setBillUnitId(billUnitId);
    	creditCharges.setCommissionOrderId(id);
    	creditCharges.setPayName(payName);
    	creditCharges.setPayMethod(payType);
    	creditCharges.setPayMoney(payMoney);
    	creditCharges.setChangeMoney(changeMoney);
    	creditCharges.setPayTime(payTime);
    	creditCharges.setComment(comment);
    	creditCharges.setUserId(getCurrentUser().getUserId());
    	//事务控制更新
    	creditChargesViewService.saveAllPayFlag(creditCharges,id,payTime);
    	setJsonBySuccess(json, "保存成功","creditCharges.do");    	
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
			
		}
		return json;
    }
}