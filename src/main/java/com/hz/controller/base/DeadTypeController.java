package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.DeadType;
import com.hz.service.base.DeadTypeService;
import com.hz.util.Const;

/**
 * 死亡类型
 * @author rgy
 *
 */
@Controller
@RequestMapping("/deadType.do")
public class DeadTypeController extends BaseController{
	@Autowired
	private DeadTypeService dts;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "deadType.do");
		
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listDeadType();
		
	}
	/**
	 * 死亡类型列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listDeadType(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel", getByte("isdel"));
		PageInfo<DeadType> page=dts.getDeadTypePageInfo(map, pages[0], pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
        model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/deadType/listDeadType",model);
		
	}
	/**
	 * 修改死亡类型
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView editDeadType(){
		Map<String,Object> model=new HashMap<String, Object>();
		DeadType deadType=new DeadType();
		String id=getString("id");
		if(id!=null&&!id.equals("")){
			deadType=dts.getDeadTypeId(id);
		}
		model.put("deadType", deadType);
		model.put("method", "save");
		return new ModelAndView("base/deadType/editDeadType",model);
	}
	/**
	 * 保存死亡类型
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存死亡类型")
	@ResponseBody
	public JSONObject addTransport(){
		JSONObject json=new JSONObject();
		try {
			DeadType deadType=new DeadType();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				deadType=dts.getDeadTypeId(id);
			}
			deadType.setName(getString("name"));
			deadType.setIndexFlag(getInt("indexFlag"));
			dts.addOrUpdate(id,deadType);
			setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除死亡类型
	 * @return
	 */
	@RequestMapping(params ="method=delete")
	@SystemControllerLog(description = "删除死亡类型")
	@ResponseBody
	public JSONObject deleteTransport(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			dts.deleteDeadType(id);;
			setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 是否启用
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改死亡类型是否有效")
    @ResponseBody
	public JSONObject updateTranport(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			dts.updateDeadTypeIsdel(id, isdel);
			setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
}
