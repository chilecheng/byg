package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Freezer;
import com.hz.service.base.FreezerService;
import com.hz.service.base.FreezerTypeService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * ��ع���Ϣ����
 * @author tsp
 *
 */
@Controller
@RequestMapping("/freezer.do")
public class FreezerController extends BaseController{
	@Autowired
	private FreezerService freezerService;
	@Autowired
	private FreezerTypeService freezerTypeService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "freezer.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listFreezer();
    }

	/**
	 * ��ع���Ϣ�б�
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFreezer(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//���ҳ��ҳ����Ϣ
		int[] pages = getPage();
		//�����ѯ����
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<Freezer> page=freezerService.getFreezerPageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/freezer/listFreezer",model);
    }
	 
	/**
	 * ���ӵ��޸�����ҳ��
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFreezer(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		Freezer freezer=new Freezer();
		if(id!=null&&!id.equals("")){
			freezer=freezerService.getFreezerById(id);
		}
		String outOption=Const.getIsOption(freezer.getOutFlag(),false);
		String splitOption=Const.getIsOption(freezer.getSplitFlag(),false);
		String typeOption=freezerTypeService.getFreezerTypeOption(freezer.getTypeId(),false);
		model.put("outOption", outOption);
		model.put("splitOption", splitOption);
		model.put("typeOption", typeOption);
        model.put("freezer", freezer);
        model.put("method", "save");
		return new ModelAndView("base/freezer/editFreezer",model);
    }

    /**
     * ������ع���Ϣ
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "������ع���Ϣ")
    @ResponseBody
    public JSONObject saveFreezer(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		Freezer freezer=new Freezer();
    		if (id!=null&&!id.equals("")) {
    			freezer=freezerService.getFreezerById(id);
    		}
    		freezer.setIndexFlag(getInt("indexFlag"));
    		freezer.setName(getString("name"));
    		freezer.setBuyTime(getDate("buyTime"));
    		freezer.setCode(getString("code"));
    		freezer.setComment(getString("comment"));
    		freezer.setOutFlag(getByte("outFlag"));
    		freezer.setPice(getDouble("pice"));
    		freezer.setSplitFlag(getByte("splitFlag"));
    		freezer.setTypeId(getString("typeId"));
    		if (id==null||id.equals("")) {
    			freezer.setId(UuidUtil.get32UUID());
    			freezer.setIsdel(Const.Isdel_No);
    			freezer.setFlag(Const.IsFlag_No);
    			freezerService.addFreezer(freezer);
    		}else {
    			freezerService.updateFreezer(freezer);
    		}
    		setJsonBySuccess(json, "����ɹ�", true);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			setJsonByFail(json, "����ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
    
    /**
     * ɾ����ع���Ϣ
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "ɾ����ع�")
    @ResponseBody
    public JSONObject deleteFreezer(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    freezerService.deleteFreezer(id);
    		setJsonBySuccess(json, "ɾ���ɹ�", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "ɾ��ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
    /**
     * �Ƿ�����
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "�޸���ع��Ƿ���Ч")
    @ResponseBody
    public JSONObject isdelFreezer(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	freezerService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "�����ɹ�", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "����ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
}
