package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.ProveUnit;
import com.hz.service.base.ProveUnitService;
import com.hz.util.Const;

/**
 * 证明单位信息
 * @author rgy
 *
 */
@Controller
@RequestMapping("/proveUnit.do")
public class ProveUnitCotroller extends BaseController{
	@Autowired
	private ProveUnitService ps;
	
	@ModelAttribute
	public void poplateModel(Model model){
		model.addAttribute("url", "proveUnit.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listProveUnit();
		
	}
	/**
	 * 证明单位信息
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listProveUnit(){
		Map<String, Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel", getByte("isdel"));
		PageInfo<ProveUnit> page=ps.getProveUnitPageInfo(map, pages[0], pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page", page);
		model.put("method", "list");
		return new ModelAndView("base/proveUnit/listProveUnit",model);
	}
	/**
	 * 连接到添加修改页面
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editProveUnit(){
		Map<String,Object> model=new HashMap<String, Object>();
		String proveUnitId=getString("proveUnitId");
		ProveUnit proveUnit=new ProveUnit();
		if(proveUnitId!=null&&!proveUnitId.equals("")){
			proveUnit=ps.getProveUnitId(proveUnitId);
		}
		String proveTypeOption=Const.getProveTypeOption(proveUnit.getProveType(), false);
		model.put("proveTypeOption", proveTypeOption);
		model.put("proveUnit", proveUnit);
		model.put("method", "save");
		return new ModelAndView("base/proveUnit/editProveUnit",model);
	} 
	/**
	 * 保存证明单位
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存证明单位")
	@ResponseBody
	public JSONObject addProveUnit(){
		JSONObject json=new JSONObject();
		try {
			String proveUnitId=getString("proveUnitId");
			ProveUnit proveUnit=new ProveUnit();
			if(proveUnitId!=null&&!proveUnitId.equals("")){
				proveUnit=ps.getProveUnitId(proveUnitId);
			}
			proveUnit.setName(getString("name"));
			proveUnit.setProveType(getByte("proveType"));
			proveUnit.setIndexFlag(getInt("indexFlag"));
			ps.addOrUpdate(proveUnitId,proveUnit);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除单位证明
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除证明单位")
	@ResponseBody
	public JSONObject deleteProveUnit(){
		JSONObject json=new JSONObject();
		try {
			String proveUnitId=getString("ids");
			ps.deleteProveUnit(proveUnitId);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
		
	}
	/**
	 * 修改是否启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改证明单位是否有效")
	@ResponseBody
	public JSONObject updateIsdelProve(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			ps.updateisdelProve(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	
}
