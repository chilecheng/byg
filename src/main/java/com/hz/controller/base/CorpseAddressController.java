package com.hz.controller.base;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.CorpseAddress;
import com.hz.service.base.CorpseAddressService;
import com.hz.util.Const;

/**
 * 接尸地址
 * @author	jgj
 *
 */
@Controller
@RequestMapping("/corpseAddress.do")
public class CorpseAddressController extends BaseController{
	@Autowired
	private CorpseAddressService cs;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "corpseAddress.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listCorpseAddress();
		
	}
	/**
	 * 接尸地址列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listCorpseAddress(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<CorpseAddress> page=cs.getCorpseAddressPageInfo(map, pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page", page);
		model.put("method","list");
		return new ModelAndView("base/corpseAddress/listCorpseAddress",model);	
	}
	/**
	 * 连接到页面添加修改接尸地址
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editCorpseAddress(){
		Map<String,Object> model=new HashMap<String, Object>();
		CorpseAddress corpseAddress=new CorpseAddress();
		String id=getString("id");
		if(id!=null&&!id.equals("")){
			corpseAddress=cs.getCorpseAddressById(id);
		}
		model.put("corpseAddress",corpseAddress);
		model.put("method","save");
		return new ModelAndView("base/corpseAddress/editCorpseAddress",model);	
	}
	/**
	 * 添加接尸地址
	 * @return
	 */
	@RequestMapping(params ="method=save")
	@SystemControllerLog(description = "保存接尸地址")
	@ResponseBody
	public JSONObject addCorpseAddress(){
		JSONObject json=new JSONObject();
		try {
			CorpseAddress corpseAddress=new CorpseAddress();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				corpseAddress=cs.getCorpseAddressById(id);
			}
			corpseAddress.setName(getString("name"));
			corpseAddress.setIndexFlag(getInt("indexFlag"));
			cs.addOrUpdate(id,corpseAddress);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除接尸地址
	 * @return
	 */
	@RequestMapping(params ="method=delete")
	@SystemControllerLog(description = "删除接尸地址")
	@ResponseBody
	public JSONObject deleteConrpseUnit(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			cs.deleteCorpseAddress(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改接尸地址的是否启动
	 * @return
	 */
	@RequestMapping(params ="method=isdel")
	@SystemControllerLog(description = "修改接尸地址是否有效")
	@ResponseBody
	public JSONObject updateCospseIsdel(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			cs.updateConrpseIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
}
