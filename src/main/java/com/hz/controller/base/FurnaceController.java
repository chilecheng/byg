package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Furnace;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.FurnaceTypeService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 火化炉信息管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/furnace.do")
public class FurnaceController extends BaseController{
	@Autowired
	private FurnaceService furnaceService;
	@Autowired
	private FurnaceTypeService furnaceTypeService;
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "furnace.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified(Model model) {
		return listFurnace();
    }

	/**
	 * 火化炉信息列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFurnace(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		maps.put("flag", getByte("flag"));
		PageInfo<Furnace> page=furnaceService.getFurnacePageInfo(maps,pages[0],pages[1],"");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("IsFlag_No", Const.IsFlag_No);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("isFlagOption", Const.getIsFlagOption(getByte("flag"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/furnace/listFurnace",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFurnace(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		Furnace furnace=new Furnace();
		if(id!=null&&!id.equals("")){
			furnace=furnaceService.getFurnaceById(id);
		}
		String typeOption=furnaceTypeService.getFurnaceTypeOption(furnace.getTypeId(),false);
		String halfOption=Const.getIsOption(furnace.getIsHalf(), false);
        model.put("furnace", furnace);
        model.put("typeOption", typeOption);
        model.put("halfOption", halfOption);
        model.put("method", "save");
		return new ModelAndView("base/furnace/editFurnace",model);
    }

    /**
     * 保存火化炉信息
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存火化炉信息")
    @ResponseBody
    public JSONObject saveFurnace(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		Furnace furnace=new Furnace();
    		if (id!=null&&!id.equals("")) {
    			furnace=furnaceService.getFurnaceById(id);
    		}
    		furnace.setIndexFlag(getInt("indexFlag"));
    		furnace.setName(getString("name"));
    		furnace.setCode(getString("code"));
    		furnace.setComment(getString("comment"));
    		furnace.setDes(getString("des"));
    		furnace.setFuel(getDouble("fuel"));
    		furnace.setTimeline(getString("timeline"));
    		furnace.setTypeId(getString("typeId"));
    		furnace.setIsHalf(getByte("halfType"));
    		if (id==null||id.equals("")) {
    			furnace.setId(UuidUtil.get32UUID());
    			furnace.setIsdel(Const.Isdel_No);
    			furnace.setFlag(Const.IsFlag_No);
    			furnaceService.addFurnace(furnace);
    		}else {
    			furnaceService.updateFurnace(furnace);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除火化炉信息
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除火化炉信息")
    @ResponseBody
    public JSONObject deleteFurnace(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    furnaceService.deleteFurnace(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改火化炉是否有效")
    @ResponseBody
    public JSONObject isdelFurnace(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	furnaceService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
