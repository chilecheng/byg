package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.BillUnit;
import com.hz.service.base.BillUnitService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;


/**
 * 挂账单位信息
 * @author jgj
 *
 */
@Controller
@RequestMapping("/billUnit.do")
public class BillUnitController extends BaseController{
	@Autowired
	private BillUnitService billUnitService;
	
	@ModelAttribute
	public void poplateModel(Model model){
		model.addAttribute("url", "billUnit.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listBillUnit();
		
	}
	/**
	 * 挂账单位信息
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listBillUnit(){
		Map<String, Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("name",getString("name"));
		map.put("isdel", getByte("isdel"));
		PageInfo<BillUnit> page=billUnitService.getBillUnitPageInfo(map, pages[0], pages[1], "index_number asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page", page);
		model.put("method", "list");
		return new ModelAndView("base/billUnit/listBillUnit",model);
	}
	/**
	 * 连接到添加修改页面
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editBillUnit(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		BillUnit billUnit=new BillUnit();
		if(id!=null&&!id.equals("")){
			billUnit=billUnitService.getBillUnitId(id);
		}
		model.put("billUnit", billUnit);
		model.put("method", "save");
		return new ModelAndView("base/billUnit/editBillUnit",model);
	} 
	/**
	 * 保存挂账单位
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存挂账单位")
	@ResponseBody
	public JSONObject addBillUnit(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			BillUnit billUnit=new BillUnit();
			if(id!=null&&!id.equals("")){
				billUnit=billUnitService.getBillUnitId(id);
			}
			billUnit.setName(getString("name"));
			billUnit.setIndexNumber(getInt("index"));
			if(id!=null&&!id.equals("")){
				billUnitService.updateBillUnit(billUnit);
			}else{
				billUnit.setId(UuidUtil.get32UUID());
				billUnit.setIsdel(Const.Isdel_No);
				
				billUnitService.addBillUnit(billUnit);
			}
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除挂账单位
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除挂账单位")
	@ResponseBody
	public JSONObject deleteProveUnit(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			billUnitService.deleteBillUnit(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;		
	}
	/**
	 * 修改挂账单位是否启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改挂账单位是否有效")
	@ResponseBody
	public JSONObject updateIsdelBill(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			billUnitService.updateisdelBill(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
	}
	
}