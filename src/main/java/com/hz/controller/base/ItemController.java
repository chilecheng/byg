package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Item;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.TypeFeesService;
import com.hz.util.Const;
import com.hz.util.ItemConst;
import com.hz.util.UuidUtil;

/**
 * 收费项目管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/item.do")
public class ItemController extends BaseController{
	@Autowired
	private ItemService itemService;
	@Autowired
	private ItemTypeService itemTypeService;
	//收费项目类型
	@Autowired
	private TypeFeesService typeFeesService;
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "item.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listItem();
    }

	/**
	 * 收费项目列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listItem(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String searchOption = Const.getSearchTypeOption(new byte[]{Const.Search_Type_ItemName,Const.Search_Type_HelpCode});
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
//		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
		maps.put("typeId", getString("typeId"));
		maps.put("sort", getByte("sort"));
		maps.put("helpCode", getString("helpCode"));
		maps.put("itemName", Const.Search_Type_ItemName);
		maps.put("hcode", Const.Search_Type_HelpCode);
		PageInfo<Item> page=itemService.getItemPageInfo(maps,pages[0],pages[1],"index_flag asc,code asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("typeIdOption", itemTypeService.getItemTypeOption(null,getString("typeId"), true));
		model.put("sortOption", ItemConst.getSortOption(getByte("sort"), true));
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
        model.put("searchVal", searchVal);
		model.put("searchOption", searchOption);
		return new ModelAndView("base/item/listItem",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editItem(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		Item item=new Item();
		if(id!=null&&!id.equals("")){
			item=itemService.getItemById(id);
		}
		String itemTypeOption= itemTypeService.getItemTypeOption(null,item.getTypeId(),false);
		String baseFlagOption= Const.getIsOption(item.getBaseFlag(), false);
		String hardFlagOption= Const.getIsOption(item.getHardFlag(), false);
		String defaultFlagOption= Const.getIsOption(item.getDefaultFlag(), false);
		String sortOption= ItemConst.getSortOption(item.getSort(), false);
		String typeFeesOption=typeFeesService.getTypeFeesOption(item.getTypeFeesId(),false);
		model.put("sortOption", sortOption);
		model.put("defaultFlagOption", defaultFlagOption);
		model.put("baseFlagOption", baseFlagOption);
		model.put("hardFlagOption", hardFlagOption);
		model.put("itemTypeOption", itemTypeOption);
		model.put("typeFeesOption", typeFeesOption);
        model.put("item", item);
        model.put("method", "save");
		return new ModelAndView("base/item/editItem",model);
    }

    /**
     * 保存收费项目
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存收费项目")
    @ResponseBody
    public JSONObject saveItem(){
		JSONObject json = new JSONObject();
		try {
			String id = getString("id");
			Item item = new Item();
			if (id != null && !id.equals("")) {
				item = itemService.getItemById(id);
			}
			item.setIndexFlag(getInt("indexFlag"));
			item.setName(getString("name"));
			item.setCode(getString("code"));
			item.setComment(getString("comment"));
			item.setBaseFlag(getByte("baseFlag"));
			item.setHardFlag(getByte("hardFlag"));
			item.setDefaultFlag(getByte("defaultFlag"));
			item.setMeasure(getString("measure"));
			item.setNameJp(getString("nameJp"));
			item.setPice(getInt("pice"));
			item.setTypeId(getString("typeId"));
			item.setSort(getByte("sort"));
			if (id != null && !id.equals("")) {
				if (!getString("helpCode").equals(item.getHelpCode())) {
					int count = itemService.ifHelpCodeExit(getString("helpCode"));
					if (count >= 1) {
						setJsonByFail(json, "该助记码已经存在");
						return json;
					}
				}
			} else {
				int count = itemService.ifHelpCodeExit(getString("helpCode"));
				if (count >= 1) {
					setJsonByFail(json, "该助记码已经存在");
					return json;
				}
			}
			item.setHelpCode(getString("helpCode"));
			item.setTypeFeesId(getString("typeFees"));
			if (id == null || id.equals("")) {
				item.setId(UuidUtil.get32UUID());
				item.setIsdel(Const.Isdel_No);
				int count = itemService.ifExit(item.getName(), item.getTypeId());
				if (count > 0) {
					setJsonByFail(json, "该类别中已存在该项目");
					return json;
				}
				itemService.addItem(item);
			} else {
				itemService.updateItem(item);
			}
			setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误");
		}
		return json;
    }
    
    /**
     * 删除收费项目
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除收费项目")
    @ResponseBody
    public JSONObject deleteItem(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    itemService.deleteItem(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改收费项目是否有效")
    @ResponseBody
    public JSONObject isdelItem(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	itemService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
