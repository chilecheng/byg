package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.SpecialBusinessType;
import com.hz.service.base.SpecialBusinessTypeService;
import com.hz.util.Const;

/**
 * 特殊业务类型
 * @author jgj
 *
 */
@Controller
@RequestMapping("/specialBusinessType.do")
public class SpecialBusinessTypeController extends BaseController{
	@Autowired
	private SpecialBusinessTypeService specialBusinessTypeService;
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "specialBusinessType.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listSpecialBusinessType();
		
	}
	/**
	 * 特殊业务列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listSpecialBusinessType(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<SpecialBusinessType> page=specialBusinessTypeService.getSpecialBusinessTypePageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/specialBusiness/listSpecialBusiness",model);
	}
	/**
	 * 连接到页面添加修改特殊业务类型
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editSpecialBusiness(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		SpecialBusinessType specialBusiness=new SpecialBusinessType();
		if(id!=null&&!id.equals("")){
			specialBusiness=specialBusinessTypeService.getSpecialBusinessTypeById(id);
		}
		model.put("specialBusiness", specialBusiness);
		model.put("method", "save");
		return new ModelAndView("base/specialBusiness/editSpecialBusiness",model);
	}
	/**
	 * 保存特殊业务类型
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存特殊业务类型")
	@ResponseBody
	public JSONObject saveSpecialBusiness(){
		JSONObject json=new JSONObject();
		try {
			SpecialBusinessType specialBusiness=new SpecialBusinessType();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				specialBusiness=specialBusinessTypeService.getSpecialBusinessTypeById(id);
			}
			specialBusiness.setName(getString("name"));
			specialBusiness.setIndexFlag(getInt("indexFlag"));
			specialBusinessTypeService.addOrUpdate(id,specialBusiness);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除特殊业务类型
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除特殊业务类型")
	@ResponseBody
	public JSONObject deleteSpecialBusiness(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			specialBusinessTypeService.deleteSpecialBusinessType(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改特殊业务类型的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改特殊业务类型是否有效")
	@ResponseBody
	public JSONObject isdelNation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			specialBusinessTypeService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
