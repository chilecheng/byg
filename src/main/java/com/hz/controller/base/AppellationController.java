package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Appellation;
import com.hz.service.base.AppellationService;
import com.hz.util.Const;
/**
 * 称谓关系
 * @author rgy
 *
 */
@Controller
@RequestMapping("/appellation.do")
public class AppellationController extends BaseController{
	@Autowired
	private AppellationService as;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "appellation.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listAppellaion();
		
	}
	/**
	 * 称谓关系列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listAppellaion(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<Appellation> page=as.getAppellationPageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/appellation/listAppellation",model);
	}
	/**
	 * 连接到页面添加修改称谓关系
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editAppellation(){
		Map<String,Object> model=new HashMap<String, Object>();
		String appellationId=getString("appellationId");
		Appellation appellation=new Appellation();
		if(appellationId!=null&&!appellationId.equals("")){
			appellation=as.getAppellationById(appellationId);
		} 
		model.put("appellation", appellation);
		model.put("method", "save");
		return new ModelAndView("base/appellation/editAppellation",model);
	}
	/**
	 * 保存称谓关系
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存称谓关系")
	@ResponseBody
	public JSONObject addAppellation(){
		JSONObject json=new JSONObject();
		try {
			Appellation appellation=new Appellation();
			String appellationId=getString("appellationId");
			if(appellationId!=null&&!appellationId.equals("")){
				appellation=as.getAppellationById(appellationId);
			}
			appellation.setName(getString("name"));
			appellation.setIndexFlag(getInt("indexFlag"));
			as.addOrUpdate(appellationId,appellation);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除称谓关系
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除称谓关系")
	@ResponseBody
	public JSONObject deleteAppellation(){
		JSONObject json=new JSONObject();
		try {
			String appellationId=getString("ids");
			as.deleteAppellation(appellationId);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改称谓关系的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改称谓关系是否有效")
	@ResponseBody
	public JSONObject isdelAppellation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			as.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
