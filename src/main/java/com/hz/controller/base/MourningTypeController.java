package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.MourningType;
import com.hz.service.base.MourningTypeService;
import com.hz.util.Const;

/**
 * 灵堂类型
 * @author rgy
 *
 */
@Controller
@RequestMapping("/mourningType.do")
public class MourningTypeController extends BaseController{
	@Autowired
	private MourningTypeService mts;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "mourningType.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listMourningType();
		
	}
	/**
	 * 灵堂类型列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listMourningType(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<MourningType> page=mts.getMourningType(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/mourningType/listMourningType",model);
	}
	/**
	 * 连接到页面添加修改灵堂类型
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editMourningType(){
		Map<String,Object> model=new HashMap<String, Object>();
		String mourningId=getString("id");
		MourningType mourningType=new MourningType();
		if(mourningId!=null&&!mourningId.equals("")){
			mourningType=mts.getMourningTypeId(mourningId);
		}
		model.put("mourningType", mourningType);
		model.put("method", "save");
		return new ModelAndView("base/mourningType/editMourningType",model);
	}
	/**
	 * 保存灵堂类型
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存灵堂类型")
	@ResponseBody
	public JSONObject addMourningType(){
		JSONObject json=new JSONObject();
		try {
			MourningType mourningType=new MourningType();
			String mourningId=getString("id");
			if(mourningId!=null&&!mourningId.equals("")){
				mourningType=mts.getMourningTypeId(mourningId);
			}
			mourningType.setName(getString("name"));
			mourningType.setIndexFlag(getInt("indexFlag"));
			mts.addOrUpdate(mourningId,mourningType);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除灵堂类型
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除灵堂类型")
	@ResponseBody
	public JSONObject deleteMourningType(){
		JSONObject json=new JSONObject();
		try {
			String mourningId=getString("ids");
			mts.deleteMourningType(mourningId);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改灵堂类型的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改灵堂类型是否有效")
	@ResponseBody
	public JSONObject isdelMourning(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			mts.updateMourningIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
