package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Hospital;
import com.hz.service.base.HospitalService;
import com.hz.util.Const;

/**
 * 医院管理
 * @author rgy
 *
 */
@Controller
@RequestMapping("/hospital.do")
public class HospitalController extends BaseController{
	@Autowired
	private HospitalService hospitalService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "hospital.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listHospital();
    }

	/**
	 * 医院列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listHospital(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<Hospital> page=hospitalService.getHospitalPageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
        model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/hospital/listHospital",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editHospital(){
		Map<String,Object> model=new HashMap<String,Object>();
		String hospitalId = getString("hospitalId");
		Hospital hospital=new Hospital();
		if(hospitalId!=null&&!hospitalId.equals("")){
			hospital=hospitalService.getHospitalId(hospitalId);
		}
        model.put("hospital", hospital);
        model.put("method", "save");
		return new ModelAndView("base/hospital/editHospital",model);
    }

    /**
     * 保存医院
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存医院信息")
    @ResponseBody
    public JSONObject saveHospital(){
		JSONObject json = new JSONObject();
    	try {
    		Hospital hospital=new Hospital();
    		String hospitalId =getString("hospitalId");
    		if (hospitalId!=null&&!hospitalId.equals("")) {
    			hospital=hospitalService.getHospitalId(hospitalId);
    		}
    		hospital.setName(getString("name"));
    		hospital.setIndexFlag(getInt("indexFlag"));
    		hospitalService.addOrUpdate(hospitalId,hospital);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除医院
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除医院信息")
    @ResponseBody
    public JSONObject deleteDept(){
		JSONObject json = new JSONObject();
    	try {
	    	String hospitalId = getString("ids");
		    hospitalService.DoDeleteHospital(hospitalId);;
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 修改是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改医院信息是否有效")
    @ResponseBody
    public JSONObject isdelRole(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	hospitalService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
