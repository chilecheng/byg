package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Register;
import com.hz.service.base.RegisterService;
import com.hz.util.Const;

/**
 * 户籍信息
 * @author rgy
 *
 */
@Controller
@RequestMapping("/register.do")
public class RegisterController extends BaseController{
	@Autowired
	private RegisterService rs;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "register.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listRegister();
		
	}
	/**
	 * 户籍信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listRegister(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<Register> page=rs.getRegisterPageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/register/listRegister",model);
	}
	/**
	 * 连接到页面添加修改户籍信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editAppellation(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("registerId");
		Register register=new Register();
		if(id!=null&&!id.equals("")){
			register=rs.getRegisterId(id);
		}
		model.put("register", register);
		model.put("method", "save");
		return new ModelAndView("base/register/editRegister",model);
	}
	/**
	 * 保存户籍信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存户籍信息")
	@ResponseBody
	public JSONObject addAppellation(){
		JSONObject json=new JSONObject();
		try {
			Register register=new Register();
			String id=getString("registerId");
			if(id!=null&&!id.equals("")){
				register=rs.getRegisterId(id);
			}
			register.setName(getString("name"));
			register.setType(getByte("bype"));
			register.setFatherId(getString("fatherId"));
			register.setIndexFlag(getInt("indexFlag"));
			rs.addOrUpdate(id,register);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除户籍信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除户籍信息")
	@ResponseBody
	public JSONObject deleteAppellation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			rs.deleteRegister(id);;
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改户籍信息的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改户籍信息是否有效")
	@ResponseBody
	public JSONObject isdelAppellation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			rs.updateisdelRegister(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
