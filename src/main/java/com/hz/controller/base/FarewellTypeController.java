package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.FarewellType;
import com.hz.service.base.FarewellTypeService;
import com.hz.util.Const;

/**
 * 告别厅类型
 * @author rgy
 *
 */
@Controller
@RequestMapping("/farewellType.do")
public class FarewellTypeController extends BaseController{
	@Autowired
	private FarewellTypeService fts;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "farewellType.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listFarewellType();
		
	}
	/**
	 * 告别厅类型列表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	public ModelAndView listFarewellType(){
		Map<String, Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<FarewellType> page=fts.getFarewellTypepageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/farewellType/listFarewellType",model);
	}
	/**
	 * 连接到页面添加修改告别厅类型
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editFarewellType(){
		Map<String,Object> model=new HashMap<String, Object>();
		String farewellId=getString("id");
		FarewellType farewellType=new FarewellType();
		if(farewellId!=null&&!farewellId.equals("")){
			farewellType=fts.getFarewellTypeById(farewellId);
		}
		model.put("farewellType", farewellType);
		model.put("method", "save");
		return new ModelAndView("base/farewellType/editFarewellType",model);
	}
	/**
	 * 保存告别厅类型
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存告别厅类型")
	@ResponseBody
	public JSONObject addFarewellType(){
		JSONObject json=new JSONObject();
		try {
			FarewellType farewellType=new FarewellType();
			String farewellId=getString("id");
			if(farewellId!=null&&!farewellId.equals("")){
				farewellType=fts.getFarewellTypeById(farewellId);
			}
			farewellType.setName(getString("name"));
			farewellType.setIndexFlag(getInt("indexFlag"));
			fts.addOrUpdate(farewellId,farewellType);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除告别厅类型
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除告别厅类型")
	@ResponseBody
	public JSONObject deleteFarewellType(){
		JSONObject json=new JSONObject();
		try {
			String farewellId=getString("ids");
			fts.deleteFarewellType(farewellId);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改告别厅类型的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改告别厅类型是否有效")
	@ResponseBody
	public JSONObject isdelAppellation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			fts.updateFarewellIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
