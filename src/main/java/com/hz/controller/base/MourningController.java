package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Mourning;
import com.hz.service.base.MourningService;
import com.hz.service.base.MourningSpecService;
import com.hz.service.base.MourningTypeService;
import com.hz.util.Const;

/**
 * 灵堂信息
 * @author rgy
 *
 */
@Controller
@RequestMapping("/mourning.do")
public class MourningController extends BaseController{
	@Autowired
	private MourningService ms;
	@Autowired
	private MourningTypeService mts;
	@Autowired
	private MourningSpecService mss;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "mourning.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listMourningType();
		
	}
	/**
	 * 灵堂信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listMourningType(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<Mourning> page=ms.getMourningPageInfo(map, pages[0], pages[1], "");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/mourning/listMourning",model);
	}
	/**
	 * 连接到页面添加修改灵堂信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editMourning(){
		Map<String,Object> model=new HashMap<String, Object>();
		String mourningId=getString("id");
		Mourning mourning=new Mourning();
		if(mourningId!=null&&!mourningId.equals("")){
			mourning=ms.getMourningById(mourningId);
		}
		String optionMourningType=mts.getMourningTypeOption(getString("typeId"), false);
		String optionMourningSpec=mss.getMourningSpecOption(getString("specId"), false);
		model.put("optionMourningType", optionMourningType);
		model.put("optionMourningSpec", optionMourningSpec);
		model.put("mourning", mourning);
		model.put("method", "save");
		return new ModelAndView("base/mourning/editMourning",model);
	}
	/**
	 * 保存灵堂信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存灵堂信息")
	@ResponseBody
	public JSONObject addMourning(){
		JSONObject json=new JSONObject();
		try {
			Mourning mourning=new Mourning();
			String mourningId=getString("id");
			if(mourningId!=null&&!mourningId.equals("")){
				mourning=ms.getMourningById(mourningId);
			}
			mourning.setCode(getString("code"));
			mourning.setName(getString("name"));
			mourning.setTypeId(getString("typeId"));
			mourning.setSpecId(getString("specId"));
			mourning.setPicPath(getString("picPath"));
			mourning.setIndexFlag(getInt("indexFlag"));
			ms.addOrUpdate(mourningId,mourning);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除灵堂信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除灵堂信息")
	@ResponseBody
	public JSONObject deleteMourning(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			ms.deleteMourning(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改灵堂的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改灵堂是否有效")
	@ResponseBody
	public JSONObject isdelMourning(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			ms.updateMourningIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
