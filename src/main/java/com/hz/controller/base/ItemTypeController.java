package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.ItemType;
import com.hz.service.base.ItemTypeService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 收费项目类别管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/itemType.do")
public class ItemTypeController extends BaseController{
	@Autowired
	private ItemTypeService itemTypeService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "itemType.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listItemType();
    }

	/**
	 * 收费项目类别列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listItemType(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		maps.put("type", getByte("itemType"));
		PageInfo<ItemType> page=itemTypeService.getItemTypePageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("itemTypeOption", Const.getItemTypeOption(getByte("itemType"), true));
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/item/listItemType",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editItemType(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		ItemType itemType=new ItemType();
		if(id!=null&&!id.equals("")){
			itemType=itemTypeService.getItemTypeById(id);
		}
		String itemTypeOption= Const.getItemTypeOption(itemType.getType(), false);
		String countFlagOption= Const.getIsOption(itemType.getCountFlag(), false);
		model.put("itemTypeOption", itemTypeOption);
		model.put("countFlagOption", countFlagOption);
        model.put("itemType", itemType);
        model.put("method", "save");
		return new ModelAndView("base/item/editItemType",model);
    }

    /**
     * 保存收费项目类别
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存收费项目类别")
    @ResponseBody
    public JSONObject saveItemType(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		ItemType itemType=new ItemType();
    		if (id!=null&&!id.equals("")) {
    			itemType=itemTypeService.getItemTypeById(id);
    		}
    		itemType.setIndexFlag(getInt("indexFlag"));
    		itemType.setName(getString("name"));
    		itemType.setCode(getString("code"));
    		itemType.setComment(getString("comment"));
    		itemType.setType(getByte("type"));
    		itemType.setCountFlag(getByte("countFlag"));
    		if (id==null||id.equals("")) {
    			itemType.setId(UuidUtil.get32UUID());
    			itemType.setIsdel(Const.Isdel_No);
    			itemTypeService.addItemType(itemType);
    		}else {
    			itemTypeService.updateItemType(itemType);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除收费项目类别
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除收费项目类别")
    @ResponseBody
    public JSONObject deleteItemType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    itemTypeService.deleteItemType(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改收费项目类别是否有效")
    @ResponseBody
    public JSONObject isdelItemType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	itemTypeService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
