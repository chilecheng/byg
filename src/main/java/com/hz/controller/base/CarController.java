package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Car;
import com.hz.service.base.CarService;
import com.hz.service.base.CarTypeService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 车辆信息管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/car.do")
public class CarController extends BaseController{
	@Autowired
	private CarService carService;
	@Autowired
	private CarTypeService carTypeService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "car.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listCar();
    }

	/**
	 * 车辆信息列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listCar(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("driverName", getString("driverName"));
		maps.put("isdel", getByte("isdel"));
		maps.put("flag", getByte("flag"));
		PageInfo<Car> page=carService.getCarPageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("IsFlag_No", Const.IsFlag_No);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("isFlagOption", Const.getIsFlagOption(getByte("flag"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/car/listCar",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editCar(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		Car car=new Car();
		if(id!=null&&!id.equals("")){
			car=carService.getCarById(id);
		}
		String typeOption=carTypeService.getCarTypeOption(car.getTypeId(),false);
        model.put("car", car);
        model.put("method", "save");
        model.put("typeOption", typeOption);
		return new ModelAndView("base/car/editCar",model);
    }

    /**
     * 保存车辆信息
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存车辆信息")
    @ResponseBody
    public JSONObject saveCar(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		Car car=new Car();
    		if (id!=null&&!id.equals("")) {
    			car=carService.getCarById(id);
    		}
    		car.setIndexFlag(getInt("indexFlag"));
    		car.setName(getString("name"));
    		car.setAdditional(getString("additional"));
    		car.setBuyDate(getDate("buyDate"));
    		car.setBuyPice(getDouble("buyPice"));
    		car.setCarNumber(getString("carNumber"));
    		car.setDriverAddr(getString("driverAddr"));
    		car.setDriverName(getString("driverName"));
    		car.setDriverPhone(getString("driverPhone"));
    		car.setFirstInspect(getDate("firstInspect"));
    		car.setInsurerCode(getString("insurerCode"));
    		car.setInsurerCompany(getString("insurerCompany"));
    		car.setInsurerContent(getString("insurerContent"));
    		car.setInsurerTime(getDate("insurerTime"));
    		car.setRetestInspect(getDate("retestInspect"));
    		car.setSellerName(getString("sellerName"));
    		car.setSellerPhone(getString("sellerPhone"));
    		car.setServicePice(getDouble("servicePice"));
    		car.setTypeId(getString("typeId"));
    		if (id==null||id.equals("")) {
    			car.setId(UuidUtil.get32UUID());
    			car.setIsdel(Const.Isdel_No);
    			car.setFlag(Const.IsFlag_No);
    			carService.addCar(car);
    		}else {
    			carService.updateCar(car);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除车辆信息
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除车辆信息")
    @ResponseBody
    public JSONObject deleteCar(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    carService.deleteCar(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改车辆信息是否有效")
    @ResponseBody
    public JSONObject isdelCar(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	carService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
	 * 链接到查看页面
	 * @return
	 */
    @RequestMapping(params = "method=see")
    public ModelAndView seeCar(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		Car	car=carService.getCarById(id);
        model.put("car", car);
		return new ModelAndView("base/car/seeCar",model);
    }
}
