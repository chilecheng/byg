package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Farewell;
import com.hz.service.base.FarewellService;
import com.hz.service.base.FarewellTypeService;
import com.hz.util.Const;

/**
 * 告别厅信息
 * @author rgy
 *
 */
@Controller
@RequestMapping("/farewell.do")
public class FarewellController extends BaseController{
	@Autowired
	private FarewellService fs;
	
	@Autowired
	private FarewellTypeService fts;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "farewell.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listFarewell();
		
	}
	/**
	 * 告别厅信息列表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	public ModelAndView listFarewell(){
		Map<String, Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<Farewell> page=fs.getFarewellpageInfo(map, pages[0], pages[1], "");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/farewell/listFarewell",model);
	}
	/**
	 * 连接到页面添加修改告别厅
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editFarewell(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		Farewell farewell=new Farewell();
		if(id!=null&&!id.equals("")){
			farewell=fs.getFarewellById(id);
		}
		String optionFarewellType=fts.getFarewellTypeOption(getString("typeId"), false);
		model.put("optionFarewellType", optionFarewellType);
		model.put("farewell", farewell);
		model.put("method", "save");
		return new ModelAndView("base/farewell/editFarewell",model);
	}
	/**
	 * 保存告别厅
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存告别厅")
	@ResponseBody
	public JSONObject addFarewell(){
		JSONObject json=new JSONObject();
		try {
			Farewell farewell=new Farewell();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				farewell=fs.getFarewellById(id);
			}
			farewell.setName(getString("name"));
			farewell.setCode(getString("code"));
			farewell.setPice(getDouble("pice"));
			farewell.setTypeId(getString("typeId"));
			farewell.setPicPath(getString("picPath"));
			farewell.setIndexFlag(getInt("indexFlag"));
			fs.addOrUpdate(id,farewell);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除告别厅
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除告别厅")
	@ResponseBody
	public JSONObject deleteFarewellType(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			fs.deleteFarewell(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改告别厅的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改告别厅是否有效")
	@ResponseBody
	public JSONObject isdelFarewell(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			fs.updateFarewellIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
