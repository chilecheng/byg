package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.FreezerType;
import com.hz.service.base.FreezerTypeService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * ��ع����͹���
 * @author tsp
 *
 */
@Controller
@RequestMapping("/freezerType.do")
public class FreezerTypeController extends BaseController{
	@Autowired
	private FreezerTypeService freezerTypeService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "freezerType.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listFreezerType();
    }

	/**
	 * ��ع������б�
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFreezerType(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//���ҳ��ҳ����Ϣ
		int[] pages = getPage();
		//�����ѯ����
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<FreezerType> page=freezerTypeService.getFreezerTypePageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/freezer/listFreezerType",model);
    }
	 
	/**
	 * ���ӵ��޸�����ҳ��
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFreezerType(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		FreezerType freezerType=new FreezerType();
		if(id!=null&&!id.equals("")){
			freezerType=freezerTypeService.getFreezerTypeById(id);
		}
        model.put("freezerType", freezerType);
        model.put("method", "save");
		return new ModelAndView("base/freezer/editFreezerType",model);
    }

    /**
     * ������ع�����
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "������ع�����")
    @ResponseBody
    public JSONObject saveFreezerType(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		FreezerType freezerType=new FreezerType();
    		if (id!=null&&!id.equals("")) {
    			freezerType=freezerTypeService.getFreezerTypeById(id);
    		}
    		freezerType.setIndexFlag(getInt("indexFlag"));
    		freezerType.setName(getString("name"));
    		if (id==null||id.equals("")) {
    			freezerType.setId(UuidUtil.get32UUID());
    			freezerType.setIsdel(Const.Isdel_No);
    			freezerTypeService.addFreezerType(freezerType);
    		}else {
    			freezerTypeService.updateFreezerType(freezerType);
    		}
    		setJsonBySuccess(json, "����ɹ�", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "����ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
    
    /**
     * ɾ����ع�����
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "ɾ����ع�����")
    @ResponseBody
    public JSONObject deleteFreezerType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    freezerTypeService.deleteFreezerType(id);
    		setJsonBySuccess(json, "ɾ���ɹ�", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "ɾ��ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
    /**
     * �Ƿ�����
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "�޸���ع��Ƿ���Ч")
    @ResponseBody
    public JSONObject isdelFreezerType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	freezerTypeService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "�����ɹ�", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "����ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
}
