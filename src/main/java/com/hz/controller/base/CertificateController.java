package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Certificate;
import com.hz.service.base.CertificateService;
import com.hz.util.Const;

/**
 * 证件类型
 * @author rgy
 *
 */
@Controller
@RequestMapping("/certificate.do")
public class CertificateController extends BaseController{
	@Autowired
	private CertificateService cs;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "certificate.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listCertificate();
		
	}
	/**
	 * 证件类型列表
	 * @return
	 */
	@RequestMapping(params="method=list")
	public ModelAndView listCertificate(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<Certificate> page=cs.getCertificatePageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
        model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/certificate/listCertificate",model);
		
	}
	/**
	 * 连接到添加修改证件
	 * @return
	 */
	@RequestMapping(params="method=edit")
	 public ModelAndView editCertificate(){
		Map<String,Object> model=new HashMap<String,Object>();
		String certificateId = getString("certificateId");
		Certificate certificate=new Certificate();
		if(certificateId!=null&&!certificateId.equals("")){
			certificate=cs.getCertificateId(certificateId);
		}
        model.put("certificate", certificate);
        model.put("method", "save");
		return new ModelAndView("base/certificate/editCertificate",model);
    }
	/**
	 * 保存证件
	 * @return
	 */
	 @RequestMapping(params = "method=save")
	 @SystemControllerLog(description = "保存证件")
	    @ResponseBody
	    public JSONObject saveCertificate(){
		 JSONObject json = new JSONObject();
	    	try {
	    		Certificate certificate=new Certificate();
	    		String certificateId =getString("certificateId");
	    		if (certificateId!=null&&!certificateId.equals("")) {
	    			certificate=cs.getCertificateId(certificateId);
	    		}
	    		certificate.setName(getString("name"));
	    		certificate.setIndexFlag(getInt("indexFlag"));
	    		cs.addOrUpdate(certificateId,certificate);
	    		setJsonBySuccess(json, "保存成功", true);
			} catch (Exception e) {
				setJsonByFail(json, "保存失败，错误"+e.getMessage());
			}
			return json;
	    }
	
	/**
	 * 删除证件
	 * @return
	 */
	@RequestMapping(params="method=delete")
	@SystemControllerLog(description = "删除证件")
	@ResponseBody
	public JSONObject deleteCertificate(){
		JSONObject json=new JSONObject();
		try {
			String certificateId=getString("ids");
			cs.deleteCertificate(certificateId);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败");
		}
		return json;
		
	}
	/**
	 * 是否启动
	 * @return
	 */
	  @RequestMapping(params = "method=isdel")
	  @SystemControllerLog(description = "修改证件是否有效")
	    @ResponseBody
	    public JSONObject isdelCarType(){
			JSONObject json = new JSONObject();
	    	try {
		    	String id = getString("id");
		    	byte isdel=getByte("isdel");
		    	cs.updateIsdel(id, isdel);
	    		setJsonBySuccess(json, "操作成功", true);
			} catch (Exception e) {
				// TODO: handle exception
				setJsonByFail(json, "操作失败，错误"+e.getMessage());
			}
			return json;
	    }
}
