package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.HardPerson;
import com.hz.service.base.HardPersonService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 重点救助对象类别管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/hardPerson.do")
public class HardPersonController extends BaseController{
	@Autowired
	private HardPersonService hardPersonService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "hardPerson.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listHardPerson();
    }

	/**
	 * 重点救助对象类别列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listHardPerson(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<HardPerson> page=hardPersonService.getHardPersonPageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/hardPerson/listHardPerson",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editHardPerson(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		HardPerson hardPerson=new HardPerson();
		if(id!=null&&!id.equals("")){
			hardPerson=hardPersonService.getHardPersonById(id);
		}
        model.put("hardPerson", hardPerson);
        model.put("method", "save");
		return new ModelAndView("base/hardPerson/editHardPerson",model);
    }

    /**
     * 保存重点救助对象类别
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存重点求助对象类别")
    @ResponseBody
    public JSONObject saveHardPerson(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		HardPerson hardPerson=new HardPerson();
    		if (id!=null&&!id.equals("")) {
    			hardPerson=hardPersonService.getHardPersonById(id);
    		}
    		hardPerson.setIndexFlag(getInt("indexFlag"));
    		hardPerson.setName(getString("name"));
    		if (id==null||id.equals("")) {
    			hardPerson.setId(UuidUtil.get32UUID());
    			hardPerson.setIsdel(Const.Isdel_No);
    			hardPersonService.addHardPerson(hardPerson);
    		}else {
    			hardPersonService.updateHardPerson(hardPerson);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除重点救助对象类别
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除重点求助对象类别")
    @ResponseBody
    public JSONObject deleteHardPerson(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    hardPersonService.deleteHardPerson(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改重点求助对象是否有效")
    @ResponseBody
    public JSONObject isdelHardPerson(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	hardPersonService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
