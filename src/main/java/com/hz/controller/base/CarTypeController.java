package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.CarType;
import com.hz.service.base.CarTypeService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 车辆类型管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/carType.do")
public class CarTypeController extends BaseController{
	@Autowired
	private CarTypeService carTypeService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "carType.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listCarType();
    }

	/**
	 * 车辆类型列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listCarType(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<CarType> page=carTypeService.getCarTypePageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/car/listCarType",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editCarType(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		CarType carType=new CarType();
		if(id!=null&&!id.equals("")){
			carType=carTypeService.getCarTypeById(id);
		}
        model.put("carType", carType);
        model.put("method", "save");
		return new ModelAndView("base/car/editCarType",model);
    }

    /**
     * 保存车辆类型
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存车辆类型")
    @ResponseBody
    public JSONObject saveCarType(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		CarType carType=new CarType();
    		if (id!=null&&!id.equals("")) {
    			carType=carTypeService.getCarTypeById(id);
    		}
    		carType.setIndexFlag(getInt("indexFlag"));
    		carType.setName(getString("name"));
    		if (id==null||id.equals("")) {
    			carType.setId(UuidUtil.get32UUID());
    			carType.setIsdel(Const.Isdel_No);
    			carTypeService.addCarType(carType);
    		}else {
    			carTypeService.updateCarType(carType);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除车辆类型
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除车辆类型")
    @ResponseBody
    public JSONObject deleteCarType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    carTypeService.deleteCarType(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改车辆类型是否有效")
    @ResponseBody
    public JSONObject isdelCarType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	carTypeService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
