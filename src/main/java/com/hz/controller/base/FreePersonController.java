package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.FreePerson;
import com.hz.service.base.FreePersonService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 免费对象类别管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/freePerson.do")
public class FreePersonController extends BaseController{
	@Autowired
	private FreePersonService freePersonService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "freePerson.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listFreePerson();
    }

	/**
	 * 免费对象类别列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFreePerson(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<FreePerson> page=freePersonService.getFreePersonPageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/freePerson/listFreePerson",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFreePerson(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		FreePerson freePerson=new FreePerson();
		if(id!=null&&!id.equals("")){
			freePerson=freePersonService.getFreePersonById(id);
		}
        model.put("freePerson", freePerson);
        model.put("method", "save");
		return new ModelAndView("base/freePerson/editFreePerson",model);
    }

    /**
     * 保存免费对象类别
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存免费对象类别")
    @ResponseBody
    public JSONObject saveFreePerson(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		FreePerson freePerson=new FreePerson();
    		if (id!=null&&!id.equals("")) {
    			freePerson=freePersonService.getFreePersonById(id);
    		}
    		freePerson.setIndexFlag(getInt("indexFlag"));
    		freePerson.setName(getString("name"));
    		if (id==null||id.equals("")) {
    			freePerson.setId(UuidUtil.get32UUID());
    			freePerson.setIsdel(Const.Isdel_No);
    			freePersonService.addFreePerson(freePerson);
    		}else {
    			freePersonService.updateFreePerson(freePerson);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除免费对象类别
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除免费对象类别")
    @ResponseBody
    public JSONObject deleteFreePerson(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    freePersonService.deleteFreePerson(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改免费对象类别")
    @ResponseBody
    public JSONObject isdelFreePerson(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	freePersonService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
