package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.TransportType;
import com.hz.service.base.TransportTypeService;
import com.hz.util.Const;

/**
 * 运输类型
 * @author rgy
 *
 */
@Controller
@RequestMapping("/transportType.do")
public class TransportTypeController extends BaseController{
	@Autowired
	private TransportTypeService tts;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "transportType.do");
		
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listTransportType();
		
	}
	/**
	 * 运输类型列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listTransportType(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel", getByte("isdel"));
		PageInfo<TransportType> page=tts.getTransportPageInfo(map, pages[0], pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
        model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/transportType/listTransportType",model);
		
	}
	/**
	 * 修改运输类型
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView editTransportType(){
		Map<String,Object> model=new HashMap<String, Object>();
		TransportType transportType=new TransportType();
		String transportId=getString("transportId");
		if(transportId!=null&&!transportId.equals("")){
			transportType=tts.getTransportTypeById(transportId);
		}
		model.put("transportType", transportType);
		model.put("method", "save");
		return new ModelAndView("base/transportType/editTransportType",model);
	}
	/**
	 * 保存运输类型
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存运输类型")
	@ResponseBody
	public JSONObject addTransport(){
		JSONObject json=new JSONObject();
		try {
			TransportType transportType=new TransportType();
			String transportId=getString("transportId");
			if(transportId!=null&&!transportId.equals("")){
				transportType=tts.getTransportTypeById(transportId);
			}
			transportType.setName(getString("name"));
			transportType.setIndexFlag(getInt("indexFlag"));
			tts.addUpdate(transportId,transportType);
			setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除运输类型
	 * @return
	 */
	@RequestMapping(params ="method=delete")
	@SystemControllerLog(description = "删除运输类型")
	@ResponseBody
	public JSONObject deleteTransport(){
		JSONObject json=new JSONObject();
		try {
			String transportId=getString("ids");
			tts.deleteTransportType(transportId);
			setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 是否启用
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改运费类型是否有效")
    @ResponseBody
	public JSONObject updateTranport(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			tts.updateTransportIsdel(id, isdel);
			setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
}
