package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.DeadReason;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.util.Const;

/**
 * 死亡原因
 * @author rgy
 *
 */
@Controller
@RequestMapping("/deadReason.do")
public class DeadReasonController extends BaseController{
	@Autowired
	private DeadReasonService drs;
	@Autowired
	private DeadTypeService dts;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "deadReason.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listDeadReason();
		
	}
	/**
	 * 死亡原因列表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	public ModelAndView listDeadReason(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<DeadReason> page=drs.getDeadReasonPageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/deadReason/listDeadReason",model);
		
	}
	/**
	 * 连接到页面添加修改死亡原因
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editAppellation(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		DeadReason deadReason=new DeadReason();
		if(id!=null&&!id.equals("")){
			deadReason=drs.getDeadReasonById(id);
		}
		String typeOption=dts.getDeadTypeOption(getString("typeId"), false);
		model.put("deadReason", deadReason);
		model.put("typeOption", typeOption);
		model.put("method", "save");
		return new ModelAndView("base/deadReason/editDeadReason",model);
	}
	/**
	 * 保存死亡原因
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存死亡原因")
	@ResponseBody
	public JSONObject addDeadReason(){
		JSONObject json=new JSONObject();
		try {
			DeadReason deadReason=new DeadReason();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				deadReason=drs.getDeadReasonById(id);
			}
			deadReason.setName(getString("name"));
			deadReason.setIndexFlag(getInt("indexFlag"));
			deadReason.setTypeId(getString("typeId"));
			drs.addOrUpdate(id,deadReason);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除死亡原因
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除死亡原因")
	@ResponseBody
	public JSONObject deleteDeadReason(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			drs.deleteDeadReason(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改死亡原因的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改死亡原因是否有效")
	@ResponseBody
	public JSONObject isdelAppellation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			drs.updateDeadIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
