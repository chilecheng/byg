package com.hz.controller.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.AshesPosition;
import com.hz.service.base.AshesPositionService;
import com.hz.util.UuidUtil;

/**
 * �ǻҺ�λ�ù���
 * @author hw
 *
 */
@Controller
@RequestMapping("/ashesPosition.do")
public class AshesPositionController extends BaseController{
	@Autowired
	private AshesPositionService ashesPositionService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "ashesPosition.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listAshesPosition();
    }

	/**
	 * �ǻҺ�λ���б�
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listAshesPosition(){
		Map<String,Object> model=new HashMap<String,Object>();
	
		List<AshesPosition> list = ashesPositionService.getAshesPositionList();
		if(!list.isEmpty()){
			model.put("number", (list.get(0)).getNumber());
		}
        model.put("method", "save");
		return new ModelAndView("base/ashesPosition/listAshesPosition",model);
    }
	 


    /**
     * ����ǻҺ�λ��
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "����ǻҺ�λ��")
    @ResponseBody
    public JSONObject saveAshesPosition(){
		JSONObject json = new JSONObject();
    	try {
    		List<AshesPosition> list = ashesPositionService.getAshesPositionList();
    		AshesPosition ashesPosition=new AshesPosition();
    		ashesPosition.setNumber(getInt("number"));
    		if(list.isEmpty()){
    			
    			ashesPosition.setId(UuidUtil.get32UUID());
    			ashesPositionService.addAshesPosition(ashesPosition);
    		}else {
    			ashesPosition.setId((list.get(0)).getId());
    			ashesPositionService.updateAshesPosition(ashesPosition);
    		}
    		setJsonBySuccess(json, "����ɹ�", "ashesPosition.do?method=list");
		} catch (Exception e) {
			setJsonByFail(json, "����ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
    
   
   
}
