package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Nation;
import com.hz.service.base.NationService;
import com.hz.util.Const;

/**
 * 民族信息
 * @author rgy
 *
 */
@Controller
@RequestMapping("/nation.do")
public class NationController extends BaseController{
	@Autowired
	private NationService nationService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "nation.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listNation();
		
	}
	/**
	 * 民族信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listNation(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<Nation> page=nationService.getNationPageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/nation/listNation",model);
	}
	/**
	 * 连接到页面添加修改民族信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editNation(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		Nation nation=new Nation();
		if(id!=null&&!id.equals("")){
			nation=nationService.getNationById(id);
		}
		model.put("nation", nation);
		model.put("method", "save");
		return new ModelAndView("base/nation/editNation",model);
	}
	/**
	 * 保存民族信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存民族信息")
	@ResponseBody
	public JSONObject addNation(){
		JSONObject json=new JSONObject();
		try {
			Nation nation=new Nation();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				nation=nationService.getNationById(id);
			}
			nation.setName(getString("name"));
			nation.setIndexFlag(getInt("indexFlag"));
			nationService.addOrUpdate(id,nation);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除民族信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除民族信息")
	@ResponseBody
	public JSONObject deleteNation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			nationService.deleteNation(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改民族信息的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改民族信息是否有效")
	@ResponseBody
	public JSONObject isdelNation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			nationService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
