package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.MourningSpec;
import com.hz.service.base.MourningSpecService;
import com.hz.util.Const;

/**
 * 灵堂规格
 * @author rgy
 *
 */
@Controller
@RequestMapping("/mourningSpec.do")
public class MourningSpecController extends BaseController{
	@Autowired
	private MourningSpecService mss;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "mourningSpec.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listMourningSpec();
		
	}
	/**
	 * 灵堂规格列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listMourningSpec(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<MourningSpec> page=mss.getMourningPageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/mourningSpec/listMourningSpec",model);
	}
	/**
	 * 连接到页面添加修改灵堂规格
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editMourningSpec(){
		Map<String,Object> model=new HashMap<String, Object>();
		String mourningId=getString("id");
		MourningSpec mourningSpec=new MourningSpec();
		if(mourningId!=null&&!mourningId.equals("")){
			mourningSpec=mss.getMourningSpecById(mourningId);
		}
		model.put("mourningSpec", mourningSpec);
		model.put("method", "save");
		return new ModelAndView("base/mourningSpec/editMourningSpec",model);
	}
	/**
	 * 保存灵堂规格
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存灵堂规格")
	@ResponseBody
	public JSONObject addMourningSpec(){
		JSONObject json=new JSONObject();
		try {
			MourningSpec mourningSpec=new MourningSpec();
			String mourningId=getString("id");
			if(mourningId!=null&&!mourningId.equals("")){
				mourningSpec=mss.getMourningSpecById(mourningId);
			}
			mourningSpec.setName(getString("name"));
			mourningSpec.setIndexFlag(getInt("indexFlag"));
			mss.addOrUpdate(mourningId,mourningSpec);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除灵堂规格
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除灵堂规格")
	@ResponseBody
	public JSONObject deleteMourningSpec(){
		JSONObject json=new JSONObject();
		try {
			String mourningId=getString("ids");
			mss.deleteMourningSpec(mourningId);;
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改灵堂规格的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改灵堂规格是否有效")
	@ResponseBody
	public JSONObject isdelAppellation(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			mss.updateMourningIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
