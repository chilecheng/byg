package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.TypeFees;
import com.hz.service.base.TypeFeesService;
import com.hz.util.Const;

/**
 * 收费项目类型
 * @author jgj
 *
 */
@Controller
@RequestMapping("/typeFees.do")
public class TypeFeesController extends BaseController{
	@Autowired
	private TypeFeesService typeFeesService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "typeFees.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listTypeFees();
		
	}
	/**
	 * 收费项目信息列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listTypeFees(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("name",getString("name"));
		map.put("isdel",getByte("isdel"));
		PageInfo<TypeFees> page=typeFeesService.getTypeFeesPageInfo(map, pages[0],pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"), true));
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("base/typeFees/listTypeFees",model);
	}
	/**
	 * 连接到页面添加修改收费项目类型信息
	 * @return
	 */
	@RequestMapping(params ="method=edit")
	public ModelAndView editTypeFrees(){
		Map<String,Object> model=new HashMap<String, Object>();
		String id=getString("id");
		TypeFees typeFees=new TypeFees();
		if(id!=null&&!id.equals("")){
			typeFees=typeFeesService.getTypeFeesById(id);
		}
		model.put("typeFees", typeFees);
		model.put("method", "save");
		return new ModelAndView("base/typeFees/editTypeFees",model);
	}
	/**
	 * 保存收费项目类型信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存收费项目类型信息")
	@ResponseBody
	public JSONObject addTypeFrees(){
		JSONObject json=new JSONObject();
		try {
			TypeFees typeFees=new TypeFees();
			String id=getString("id");
			if(id!=null&&!id.equals("")){
				typeFees=typeFeesService.getTypeFeesById(id);
			}
			typeFees.setName(getString("name"));
			typeFees.setIndexFlag(getInt("indexFlag"));
			typeFeesService.addOrUpdate(id,typeFees);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 删除收费项目类型信息
	 * @return
	 */
	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除收费项目类型信息")
	@ResponseBody
	public JSONObject deleteTypeFees(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("ids");
			typeFeesService.deleteTypeFees(id);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 修改收费项目类型信息的启动
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "修改收费项目类型信息是否有效")
	@ResponseBody
	public JSONObject isdelTypeFees(){
		JSONObject json=new JSONObject();
		try {
			String id=getString("id");
			byte isdel=getByte("isdel");
			typeFeesService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误"+e.getMessage());
		}
		return json;
	}
}
