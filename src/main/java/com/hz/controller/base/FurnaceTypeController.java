package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.FurnaceType;
import com.hz.service.base.FurnaceTypeService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 火化炉类型管理
 * @author tsp
 *
 */
@Controller
@RequestMapping("/furnaceType.do")
public class FurnaceTypeController extends BaseController{
	@Autowired
	private FurnaceTypeService furnaceTypeService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "furnaceType.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listFurnaceType();
    }

	/**
	 * 火化炉类型列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFurnaceType(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getByte("isdel"));
		PageInfo<FurnaceType> page=furnaceTypeService.getFurnaceTypePageInfo(maps,pages[0],pages[1],"index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/furnace/listFurnaceType",model);
    }
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFurnaceType(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		FurnaceType furnaceType=new FurnaceType();
		if(id!=null&&!id.equals("")){
			furnaceType=furnaceTypeService.getFurnaceTypeById(id);
		}
        model.put("furnaceType", furnaceType);
        model.put("method", "save");
		return new ModelAndView("base/furnace/editFurnaceType",model);
    }

    /**
     * 保存火化炉类型
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存火化炉类型")
    @ResponseBody
    public JSONObject saveFurnaceType(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		FurnaceType furnaceType=new FurnaceType();
    		if (id!=null&&!id.equals("")) {
    			furnaceType=furnaceTypeService.getFurnaceTypeById(id);
    		}
    		furnaceType.setIndexFlag(getInt("indexFlag"));
    		furnaceType.setName(getString("name"));
    		if (id==null||id.equals("")) {
    			furnaceType.setId(UuidUtil.get32UUID());
    			furnaceType.setIsdel(Const.Isdel_No);
    			furnaceTypeService.addFurnaceType(furnaceType);
    		}else {
    			furnaceTypeService.updateFurnaceType(furnaceType);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除火化炉类型
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除火化炉类型")
    @ResponseBody
    public JSONObject deleteFurnaceType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
		    furnaceTypeService.deleteFurnaceType(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改火化炉类型是否有效")
    @ResponseBody
    public JSONObject isdelFurnaceType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	furnaceTypeService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
