package com.hz.controller.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Toponym;
import com.hz.service.base.RegisterService;
import com.hz.service.base.ToponymService;
import com.hz.util.Const;

/**
 * 地名
 * @author rgy
 *
 */

@Controller
@RequestMapping("/toponym.do")
public class ToponymCotroller extends BaseController{
	@Autowired
	private ToponymService toponymService;
	@Autowired
	private RegisterService registerService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "toponym.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listToponym();
		
	}
	/**
	 * 地名列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listToponym(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		//获取页码
		int pages[]=getPage();
		//查询
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		map.put("isdel", getByte("isdel"));		
		PageInfo<Toponym> page=toponymService.getToponymPageInfo(map, pages[0], pages[1], "index_flag asc");
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
        model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("base/toponym/listToponym",model);
	}
	/**
	 * 连接到页面的添加修改
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView editToponym(){
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		Toponym toponym=new Toponym();
		String toponymId=getString("toponymId");
		if(toponymId!=null&&!toponymId.equals("")){
			toponym=toponymService.getToponymById(toponymId);
		}
		String registerOption=registerService.getRegisterOption(null, false);
		model.put("registerOption", registerOption);
		model.put("isOption",Const.getIsOption((byte)2, false));
		model.put("toponym", toponym);
		model.put("method", "save");
		return new ModelAndView("base/toponym/editToponym",model);	
	}
	/**
	 * 保存地名
	 * @return
	 */
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存地名")
    @ResponseBody
    public JSONObject saveToponym(){
		JSONObject json = new JSONObject();
    	try {
    		Toponym toponym=new Toponym();
    		String toponymId =getString("toponymId");
    		if (toponymId!=null&&!toponymId.equals("")) {
    			toponym=toponymService.getToponymById(toponymId);
    		}
    		toponym.setFatherId(getString("fatherId"));
    		toponym.setName(getString("name"));
    		toponym.setIndexFlag(getInt("indexFlag"));
//    		toponym.setIsBase(getByte("isBase"));
    		//修改
    		toponymService.addOrUpdate(toponymId,toponym);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
	 /**
     * 删除地名
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除地名")
    @ResponseBody
    public JSONObject deleteDept(){
		JSONObject json = new JSONObject();
    	try {
	    	String topponymId = getString("ids");
		    toponymService.deleteToponym(topponymId);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 是否启用
     * @return
     */
    @RequestMapping(params = "method=isdel")
    @SystemControllerLog(description = "修改地名是否有效")
    @ResponseBody
    public JSONObject isdelToponym(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	byte isdel=getByte("isdel");
	    	toponymService.updateIsdel(id, isdel);
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
		}
		return json;
    }
}
