package com.hz.controller.ye;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.FreezerType;
import com.hz.entity.system.User;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FreezerRecord;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;
import com.hz.service.base.FreezerService;
import com.hz.service.base.FreezerTypeService;
import com.hz.service.system.UserService;

/**
 * 冷藏柜记录信息
 *
 *	 @author gpf
 */
@Controller
@RequestMapping("/freezerRecord.do")
public class FreezerRecordController extends BaseController{
	@Autowired
	private FreezerRecordService freezerRecordService;
	@Autowired
	private FreezerService freezerService;
	@Autowired
	private UserService userService; 
	@Autowired
	private FreezerTypeService freezerTypeService;
	@Autowired
	private CommissionOrderService OrderService;
	@Autowired
	private MourningRecordService mourningRecordService;

	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "freezerRecord.do");
    }
	

	@RequestMapping
    public ModelAndView unspecified() {
		return editFreezer();
    }

	/**
	 * 冷藏柜记录列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFreezerRecord(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
				int[] pages = getPage();
			
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("name", getString("name"));
		maps.put("isdel", getString("isdel"));
		PageInfo<FreezerRecord> page=freezerRecordService.getFreezerRecordPageInfo(maps,pages[0],pages[1], null);
		
		model.put("Isdel_No", Const.Isdel_No);
		model.put("Isdel_Yes", Const.Isdel_Yes);
		model.put("isdelOption", Const.getIsdelOption(getByte("isdel"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("ye/freezerRecord/listFreezerRecord",model);
    }
    
    /**
	 * 冷藏柜 调度  页面 
	 * 冷藏柜记录列表
	 * @return
	 */
	@RequestMapping(params = "method=list2")
    public ModelAndView listFreezer(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		String searchType=getString("select");
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		//String name = getString("name");
		String startTime=getString("startTime");
		String endTime=getString("endTime");
		maps.put("selectValue", getString("selectValue"));
		maps.put("select", searchType);
		maps.put("startTime",startTime);
		maps.put("endTime", endTime+"23:59:59");
		
		
    	String checkType=getString("checkType");
    	maps =  checkType(maps,checkType);	
    	if(checkType.equals("stationWait")){
    		Date beginDate = getDate("beginDate");
    		Date endDate = getDate("endDate");
    		beginDate = DateTools.getThisDate();
    		endDate = DateTools.getDayAfterDay(new java.sql.Date(beginDate.getTime()), 1, Calendar.DATE);
    		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(beginDate.getTime())));
    		maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(endDate.getTime())));
    		PageInfo<FreezerRecord> page=freezerRecordService.getFreezerRecordPageInfo2(maps,pages[0],pages[1],"");	
    		model.put("page", page);
    	}else if("stationOut".equals(checkType)){//需出柜
    		if("".equals(startTime)){
//    			maps.put("outTime", DateTools.getThisDate());
    			maps.put("outTime", DateTools.getThisDateTimestamp());
    		}else{
    			maps.put("outTime", startTime);
    		}
    		maps.put("iceFlag", Const.Is_Yes);//已入库
    		PageInfo<FreezerRecord> page=freezerRecordService.getFreezerRecordPageInfo2(maps,pages[0],pages[1],"");	
    		model.put("page", page);
    	}
    	else{
    		PageInfo<FreezerRecord> page=freezerRecordService.getFreezerRecordPageInfo2(maps,pages[0],pages[1],"");	
			model.put("page", page);
    	}
    	String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_IceNumber});
    	model.put("searchOption", searchOption);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
   		model.put("IsFlag_No", Const.IsFlag_No);
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("checkType", checkType);
		
		return new ModelAndView("ye/second_department/freezer/listFreezer",model);
    }
    
	
	
    
    /**
     * 冷藏柜调度页面    视图
     * @return
     */

	@RequestMapping(params = "method=editFreezer")
    public ModelAndView editFreezer(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);	
		List<Object> typeList=new ArrayList<Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		List<Object> number=new ArrayList<Object>();
		Map<String, Object> freezermaps=new HashMap<String, Object>();
		
		List<FreezerRecord> freezerRecordList = freezerRecordService.getNotOutFreezerRecordList(null);
		List<FreezerType> freezerTypeList = freezerTypeService.getFreezerTypeList(null,"index_flag asc,name asc");//冷藏柜类型信息
		for (int i = 0; i < freezerTypeList.size(); i++) {
			FreezerType freezerType=(FreezerType) freezerTypeList.get(i);
			typeList.add(freezerType.getName());
			
			Map<String,Object> map=new HashMap<String,Object>();
	        map.put("typeId",freezerType.getId());
	        map.put("flag", Const.IsFlag_No);
	        int x=freezerService.getFreezerNumber(map);
	        map.remove("flag");
	        map.put("flagNo", Const.IsFlag_No);
	        int y=freezerService.getFreezerNumber(map);
	        Map<String,Integer> numberMap=new HashMap<String,Integer>();
	        numberMap.put("idleNumber", x);
	        numberMap.put("usingNumber", y);
			number.add(numberMap);
			
			freezermaps.put("typeId",freezerType.getId());
			freezermaps.put("isdel",Const.Isdel_No);
			List<Freezer> freezerList = freezerService.getFreezerList(freezermaps,"index_flag asc,name asc");
			Object[] ob=new Object[freezerList.size()];
			for (int j = 0; j < freezerList.size(); j++) {
				ob[j]=freezerList.get(j);
				for (int j2 = 0; j2 < freezerRecordList.size(); j2++) {
					
					if (freezerList.get(j).getId()==freezerRecordList.get(j2).getFreezerId()||freezerList.get(j).getId().equals(freezerRecordList.get(j2).getFreezerId()))
					{
						ob[j]=freezerRecordList.get(j2);
					}
				}
			
			}
			list.add(ob);
		}	
		model.put("number", number);
		model.put("list", list);
		model.put("typeList", typeList);
        model.put("method", "editFreezer");
      //  model.put("IsFlag_No",Const.IsFlag_No);
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);    
        
        
        //通过 在转柜操作中     href 链接 中加入type  从而使代码公用  
        String type=getString("type");
        if(type.equals("transfer")){
        	return new ModelAndView("ye/second_department/freezer/choseFreezer",model);
        }
        else{
        	return new ModelAndView("ye/second_department/freezer/freezer",model);
        }
        
        
        
    }
    
    

    /**
     * 冷藏柜单击
     * @return
     */
    @RequestMapping(params = "method=freezer_click")
    @ResponseBody
    public JSONObject freezer_click(){
    	JSONObject json = new JSONObject();
    	int type=getInt("type");
    	int freezer=getInt("freezer");
		List<List<Freezer>> list=new ArrayList<List<Freezer>>();
		Map<String, Object>freezermaps=new HashMap<String, Object>();
		List<FreezerType> freezerTypeList = freezerTypeService.getFreezerTypeList(null,"index_flag asc,name asc");//冷藏柜类型信息
		for (int i = 0; i < freezerTypeList.size(); i++) {
			FreezerType freezerType=freezerTypeList.get(i);
			freezermaps.put("typeId",freezerType.getId());
			freezermaps.put("isdel",Const.Isdel_No);
			List<Freezer> freezerList = freezerService.getFreezerList(freezermaps,"index_flag asc,name asc");
			list.add(freezerList);
		}
		List<Freezer> tList=list.get(type);
		Freezer freezer1=(Freezer) tList.get(freezer);
		json.put("name", freezer1.getName());
		json.put("id", freezer1.getId());
		return json;
    	/*JSONObject json = new JSONObject();
    	int type=getInt("type");
    	int freezer=getInt("freezer");
		List<List<Freezer>> list=new ArrayList<List<Freezer>>();
		Map<String, Object>freezermaps=new HashMap<String, Object>();
		List<FreezerType> freezerTypeList = freezerTypeService.getFreezerTypeList(null,"index_flag asc,name asc");//冷藏柜类型信息
		for (int i = 0; i < freezerTypeList.size(); i++) {
			FreezerType freezerType=freezerTypeList.get(i);
			freezermaps.put("typeId",freezerType.getId());
			freezermaps.put("outFlag",Const.Is_No);
			freezermaps.put("splitFlag",Const.Is_No);
			freezermaps.put("isdel",Const.Isdel_No);
			List<Freezer> freezerList = freezerService.getFreezerList(freezermaps,"index_flag asc,name asc");
			list.add(freezerList);
		}
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("isdel",Const.Isdel_No );
		maps.put("outFlag",Const.Is_Yes );
		maps.put("isdel",Const.Isdel_No );
		maps.put("splitFlag",Const.Is_No );
		List<Freezer> freezerDList = freezerService.getFreezerList(maps,"index_flag asc,name asc");//外送单体
		list.add(freezerDList);
		maps.put("splitFlag",Const.Is_Yes );
		List<Freezer> freezerFList = freezerService.getFreezerList(maps,"index_flag asc,name asc");//外送分体
		list.add(freezerFList);
		List<Freezer> tList=list.get(type);
		Freezer freezer1=(Freezer) tList.get(freezer);
		json.put("name", freezer1.getName());
		json.put("id", freezer1.getId());
		return json;*/
    }
    
    
    
    /**
     * 解锁
     * @return
     */
   
    @RequestMapping(params = "method=unlock")
    @SystemControllerLog(description = "冷藏柜解锁")
    @ResponseBody
    public JSONObject unlock(){
    	JSONObject json = new JSONObject();
		List<Freezer> freezerList = freezerService.getFreezerList(null,"index_flag asc,name asc");
		for (int i = 0; i < freezerList.size(); i++) {
			Freezer freezer2=freezerList.get(i);
			if (freezer2.getFlag()==Const.IsFlag_Lock) {
				if (freezer2.getUserName().equals(getCurrentUser().getName())||getCurrentUser().getName()==freezer2.getUserName()) {
					freezer2.setFlag(Const.IsFlag_No);
					freezerService.updateFreezer(freezer2);
				}
			}
		}
		return json;
    }
   
    
    

    /**
	 * 链接到入柜操作
	 * @return
	 */
    @RequestMapping(params = "method=into")
    public ModelAndView intoFreezerRecord(){
    	 String id = getString("id");
		 Map<String,Object> model =  modelput(id,1);
//		String homeType=getString("homeType");
		 
		//首页列表查看状态
		FreezerRecord freezerRecord=freezerRecordService.getFreezerRecordId(id);
 		if(freezerRecord.getViewsDispatch()==0){
 			freezerRecord.setViewsDispatch(Const.One);
 			freezerRecordService.updateFreezerRecord(freezerRecord);
 		}
//		 model.put("homeType", homeType);
 		model.put("goWhere",getString("goWhere"));
 		model.put("noRef", getString("noRef"));
		return new ModelAndView("ye/second_department/freezer/intoFreezer",model);
    }
    
    /**
     * 保存冰柜 入柜相关信息
     * @return
     */
    @RequestMapping(params = "method=saveinto")
    @SystemControllerLog(description = "保存冰柜入柜相关信息")
    @ResponseBody
    public JSONObject saveintAshesRecordD(){
		JSONObject json = new JSONObject();
    	try {
    		String id = getString("id");
    		FreezerRecord freezerRecord=new FreezerRecord();
    		freezerRecord=freezerRecordService.getFreezerRecordId(id);
    	 	//model.put("agentUser", getCurrentUser().getName());
    		freezerRecord.setIntoStaff(getString("assiginUser"));
    		freezerRecord.setFlag(Const.IsFlag_Yse);
    		freezerRecord.setIntoOperator(getCurrentUser().getName());
    		//关联 冰柜的状态的更新
    		//	model.put("agentUser", );
    	     Freezer freezer = new Freezer();
    	     freezer = freezerService.getFreezerById(freezerRecord.getFreezerId()); 
    	    // freezer = freezerRecord.getFreezer();
    	     freezer.setId(freezerRecord.getFreezerId());
    	     freezer.setFlag(Const.IsFlag_Yse);
 
    		if (id==null||id.equals("")) {
    			freezerRecord.setId(UuidUtil.get32UUID());
    			freezerRecord.setFlag(Const.IsFlag_No);
    			freezerRecordService.addFreezerRecord(freezerRecord);
    		}else {
    			freezerRecordService.intoOutFreezerRecord(freezerRecord,freezer);
    			
    		}
    		if ("inner".equals(getString("goWhere"))) {
    			setJsonBySuccess(json, "入柜成功", "freezerRecord.do?method=list2");
    			String noRefresh = getString("noRef");
    			if (noRefresh!=null && !noRefresh.equals("")) {
    				json.put("noRefresh", true);
    			}
			} else {
				setJsonBySuccess(json, "入柜成功", true);
				json.put("refresh", getSession().getAttribute("refresh"));
			}
		} catch (Exception e) {
			setJsonByFail(json, "入柜失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
   	 * 链接到出柜操作
   	 * @return
   	 */
       @RequestMapping(params = "method=out")
       public ModelAndView outFreezerRecord(){
    	  String homeType=getString("homeType");
    		String id = getString("id");
    		Map<String,Object> model =  modelput(id,2);
//    		 //获取当前时间
//    Timestamp nowTime=new Timestamp(System.currentTimeMillis());
//    		model.put("outTime",nowTime);
    		FreezerRecord freezerRecord=freezerRecordService.getFreezerRecordId(id);
    		if(freezerRecord.getViewsOut()==0){
    			freezerRecord.setViewsOut(Const.One);
    			freezerRecordService.updateFreezerRecord(freezerRecord);
    		}
    		model.put("homeType", homeType);
    		
   		return new ModelAndView("ye/second_department/freezer/outFreezer", model);
       }
       
       /**
        * 保存冰柜 入柜相关信息
        * @return
        */
      
       @RequestMapping(params = "method=saveout")
       @SystemControllerLog(description = "保存冰柜出柜相关信息")
       @ResponseBody
       public JSONObject saveOutAshesRecordD(){
   		JSONObject json = new JSONObject();
       	try {
       		String id = getString("id");
       		FreezerRecord freezerRecord=new FreezerRecord();
       		freezerRecord=freezerRecordService.getFreezerRecordId(id);
       		
       	//	byte flag =freezerRecord.getFlag();
       	//  出柜前需要进行判断  其状态再进行出柜

       		freezerRecord.setOutStaff(getString("outer"));
       		freezerRecord.setOutRemark(getString("outRemark"));
       		freezerRecord.setOutOperator(getCurrentUser().getName());
       
       		freezerRecord.setOutTime(getTimeM("outTime"));
       		freezerRecord.setFlag(Const.IsFlag_No);
       		//关联 冰柜的状态的更新
       	    Freezer freezer = new Freezer();
       	    freezer = freezerService.getFreezerById(freezerRecord.getFreezerId()); 
       	    freezer.setId(freezerRecord.getFreezerId());
       	    freezer.setFlag(Const.IsFlag_No);
       	     
       	    
       	    freezerRecordService.saveOutAndUpdateMourning(id,freezerRecord,freezer);
       	    
       	//	freezerRecordService.updateFreezerRecord(freezerRecord);
       		setJsonBySuccess(json, "出柜成功", "freezerRecord.do?method=list2");	
       		
       
       		
   		} catch (Exception e) {
   			setJsonByFail(json, "出柜失败，错误"+e.getMessage());
   		}
   		return json;
       }
       
       
       /**
      	 * 链接到转柜操作
      	 * @return
      	 */
          @RequestMapping(params = "method=transfer")
         public ModelAndView transferFreezerRecord(){
        	 
              String id = getString("id");
             //  String flag =getString("flag");
             
     		 Map<String,Object> model =  modelput(id,3);
      		return new ModelAndView("ye/second_department/freezer/transferFreezer",model);
          } 
          /**
        	 * 弹出显示  记录详细信息
        	 * @return
        	 */
            @RequestMapping(params = "method=showFreeRecord")
           public ModelAndView showFreezerRecord(){	
          	    String id = getString("id");
        		 Map<String,Object> model =  modelput(id,4);
          		
       		return new ModelAndView("ye/second_department/freezer/charFreezer",model);
       		
           }
          /**
           * 用于判断保存  转柜信息
           */
          @RequestMapping(params = "method=savetransfer")
          @SystemControllerLog(description = "保存转柜信息")
          @ResponseBody
          public JSONObject saveTransferRecordD(){
        	  JSONObject json = new JSONObject();
             	try {
             		String id = getString("id");
             		FreezerRecord freezerRecord=new FreezerRecord();
             		freezerRecord=freezerRecordService.getFreezerRecordId(id);
             		
             		//原   冰柜的状态的更新 为 空闲
             	     Freezer freezer = new Freezer();
             	    freezer = freezerService.getFreezerById(freezerRecord.getFreezerId()); 
             	     freezer.setId(freezerRecord.getFreezer().getId());
             	     freezer.setFlag(Const.IsFlag_No); 
           
             	     
             	     
             	     //转柜后    新 冰柜状态的改为 
             	 Freezer newFreezer = new Freezer();
             	newFreezer = freezerService.getFreezerById(getString("freezerId"));
             	 newFreezer.setId(getString("freezerId"));
             	 newFreezer.setFlag(Const.IsFlag_Yse);
             	 
             	freezerRecord.setTransferRemark(getString("remark"));
         		freezerRecord.setFreezerId(getString("freezerId"));
             	 
          	   CommissionOrder order = new CommissionOrder();
             	order=  OrderService.getCommissionOrderById(freezerRecord.getCommissionOrderId());
            	  order.setFreezerId(getString("freezerId"));
            	  order.setId(freezerRecord.getCommissionOrderId());
             	 
          
             		if (id==null||id.equals("")) {
             			freezerRecord.setId(UuidUtil.get32UUID());
             			freezerRecord.setFlag(Const.IsFlag_No);
             			freezerRecordService.addFreezerRecord(freezerRecord);
             		}else {
             			freezerRecordService.updateTransferFreezerRecord(freezerRecord,freezer,newFreezer,order);
             		
             			setJsonBySuccess(json, "转柜成功", "freezerRecord.do?method=list2");	
             		}
         		} catch (Exception e) {
         			setJsonByFail(json, "转柜失败，错误"+e.getMessage());
         		}
         		return json;      		
             }
     
	 
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFreezerRecord(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		FreezerRecord freezerRecord=new FreezerRecord();
		if(id!=null&&!id.equals("")){
			freezerRecord=freezerRecordService.getFreezerRecordId(id);
		}
        model.put("freezerRecord", freezerRecord);
        model.put("method", "save");
		return new ModelAndView("ye/freezerRecord/editFreezerRecord",model);
    }


	/**
     * 删除冷藏柜记录
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除冷藏柜记录")
    @ResponseBody
    public JSONObject deleteFreezerRecord(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
	    	freezerRecordService.deleteFreezerRecord(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 封装查询条件 
     * @param type
     * @param checkType
     * @param map
     * @param clickId
     * @return
     */
    public Map<String, Object> checkType(Map<String, Object> map,String checkType){
    	
    	if("stationNo".equals(checkType)){
    		map.put("checkFlag", Const.IsFlag_Lock);
    	}
    	if("stationIng".equals(checkType)){
    		map.put("checkFlag", Const.IsFlag_Yse);
    	}
    	if("stationEnd".equals(checkType)){
    		map.put("checkFlag", Const.IsFlag_No);
    	}

    	if("stationAll".equals(checkType)){
    		map.put("checkFlag", null);
    	}
    	if("stationWait".equals(checkType)){
    		map.put("last","last");
    	}
    		
    	return map;
    }
    
    

    
    
    //  用于 通过  选中 某条 数据而获取 相关的详细属性信息
    public  Map<String, Object>  modelput(String id, int i){
    //	String freezerRecordId = new String(id);
    	int index=i; 	
    	Map<String,Object> model=new HashMap<String,Object>();
  		FreezerRecord freezerRecord=new FreezerRecord();
  		freezerRecord=freezerRecordService.getFreezerRecordId(id);
  		model.put("currentTime",DateTools.getTimeString("yyyy-MM-dd HH:mm"));
  		//获取办理人姓名
      	model.put("agentUser", getCurrentUser().getName());
  	 	model.put("id", freezerRecord.getId());
  		model.put("name", freezerRecord.getOrder().getName());
  		model.put("fName",freezerRecord.getOrder().getfName());
  		model.put("age", freezerRecord.getOrder().getAge());
  		model.put("fPhone",freezerRecord.getOrder().getfPhone());
  		model.put("code", freezerRecord.getOrder().getCode());
  		model.put("sex", freezerRecord.getOrder().getSexName());
  		model.put("beginDate", freezerRecord.getBeginDate());
  		model.put("endDate",freezerRecord.getEndDate());
  		model.put("outRemark",freezerRecord.getOutRemark() );
  		if(freezerRecord.getMourning()!=null){
  			model.put("mourningName", freezerRecord.getMourning().getName() );
  		}else{
  			model.put("mourningName", null);
  		}
  		if(freezerRecord.getFarewell()!=null){
  			model.put("farewellName", freezerRecord.getFarewell().getName() );
  		}else{
  			model.put("farewellName", null);
  		}
  		if(freezerRecord.getMourningRecord()!=null){
  			model.put("mourningRecordBD", freezerRecord.getMourningRecord().getBeginTime()  );
  		}else{
  			model.put("mourningRecordBD", null);
  		}
  		if(freezerRecord.getFarewellRecord()!=null){
  			model.put("farewellRecordBD", freezerRecord.getFarewellRecord().getBeginDate()  );
  		}else{
  			model.put("farewellRecordBD", null);
  		}
  		if(freezerRecord.getFreezer()!=null){
  			model.put("freezerName", freezerRecord.getFreezer().getName());
  		}else{
  			model.put("freezerName", null);
  		}
  	  		model.put("repairFee",freezerRecord.getRepairFee() );
  	  	model.put("intoStaff", freezerRecord.getIntoStaff());
  	  //	model.put("outStaff", freezerRecord.getOutStaff());
  		model.put("outTime", freezerRecord.getOutTime());
  		model.put("cremationTime",freezerRecord.getCreatTime());
  		model.put("intoOperator", freezerRecord.getIntoOperator());
		model.put("outOperator", freezerRecord.getOutOperator());
	  	//intoOperator
	
  		
   		//通过 冰柜管理员的id  来获取冰柜管理员的姓名信息   以及录入人员想信息
   		String intostaffOption= userService.getNameById(freezerRecord.getIntoStaff());
   		model.put("intoStaffOption", intostaffOption);
   		String outStaffId = freezerRecord.getOutStaff();
   		String outStaff  =   userService.getNameById(outStaffId);
   		model.put("outStaff", outStaff);
 	   /* String staffOption = userService.getuserOption(freezerRecord.getUserOptionId(), false);
	    model.put("staffOption", staffOption);*/
	    List<User> intoFreezer = userService.getUserListByRoleName(Const.FUNERAL1);
	    intoFreezer.addAll(userService.getUserListByRoleName(Const.FUNERAL2));
	    List<Object[]> intoFreezerList = new ArrayList<Object[]>();
	    for (User user : intoFreezer) {
	    	intoFreezerList.add(new Object[]{user.getUserId(),user.getName()});
		}
	    model.put("staffOption", DataTools.getOptionByList(intoFreezerList,null,false));
   		String freezerOption =freezerService.getBlankFreezerOption(freezerRecord.getFreezerOption(),false);
   		model.put("freezerOption", freezerOption);
   		
  
   		model.put("Sex_Fale", Const.Sex_Fale);
   		model.put("Sex_Female", Const.Sex_Female);
   		
   	
  		if(index==1){
  			model.put("method", "saveinto");
  		}
  		if(index==2){
  		  Timestamp nowTime=new Timestamp(System.currentTimeMillis());
  		model.put("outTime", nowTime  );
  			model.put("method", "saveout");
  		}
  		if(index==3){
  			model.put("method", "savetransfer");
  		}
  		if(index==4){
  			model.put("outTime", freezerRecord.getOutTime());
  			model.put("method", null);
  		}
	
    	return model;
    }
    
    /**
     * 人员选择
     * @return
     */
    @RequestMapping(params = "method=choose")
    public ModelAndView chooseUser(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	List<Object[]> list = new ArrayList<Object[]>();
    	List<User> users = userService.getUserListByRoleName(Const.FUNERAL1);
    	users.addAll(userService.getUserListByRoleName(Const.FUNERAL2));
    	for(int i=0;i<users.size();i++){
    		list.add(new Object[]{users.get(i).getUserId(),users.get(i).getName()});
    	}
    	model.put("groupName", Const.INTER);
    	model.put("list",list);
    	return new ModelAndView("../../common/worker",model);
    }
    
    
}