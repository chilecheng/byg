package com.hz.controller.ye.staticalForm;


import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.base.Item;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.staticalForm.BaseReductionStaticalForm;
import com.hz.service.base.RegisterService;
import com.hz.service.ye.staticalForm.BaseReductionStaticalFormService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;
import com.hz.util.ItemConst;
import com.hz.util.ItemTypeConst;
/**
 * 基本殡葬减免统计
 * @author mbz
 *
 */
@Controller			
@Scope("session")
@RequestMapping("/baseReductionStaticalForm.do")
public class BaseReductionStaticalFormController extends BaseController {
	@Autowired
	private BaseReductionStaticalFormService brsfs;
	@Autowired
	private RegisterService registerService;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "baseReductionStaticalForm.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified() {
		return listBaseReductionStatistical();
	}
	
	/**
	 * 基本减免统计报表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	private ModelAndView listBaseReductionStatistical() {
		Map<String,Object> model = new HashMap<String,Object>();
		model.get(model);
		list.removeAll(list);
		// 查询
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isdel", Const.Is_Yes);
		Date start = getDate("startTime");
		Date end = getDate("endTime");
		if (start == null) {
			start = DateTools.getThisDate();
		} else {
			start = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(start));
		}
		if (end == null) {
			end = DateTools.getDayEndTime(DateTools.getThisDate());
		} else {
			end = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(end));
		}
		String index = "";
		String name = getString("name");
		String area = getString("area");
		map.put("name", name);
		map.put("area", area);
		//根据时间查询
		map.put("start", start);
		map.put("end", end);
		map.put("checkFlag", Const.Check_Yes);
		List<BaseReductionStaticalForm> baseReSaFo = brsfs.findBaseReductionSF(map);
		//List<Object> list = new ArrayList<Object>();
		for(int i = 0; i<baseReSaFo.size(); i++){
			Object[] o = new Object[13];
			String dName = baseReSaFo.get(i).getCommissionOrder().getName();
			int dSex = baseReSaFo.get(i).getCommissionOrder().getSex();
			String cerCode = baseReSaFo.get(i).getCommissionOrder().getCertificateCode();
			String reName = baseReSaFo.get(i).getToponym().getName();
			Date dTime = baseReSaFo.get(i).getCommissionOrder().getdTime();
			Timestamp payTime = baseReSaFo.get(i).getPayTime();
			Date creTime = baseReSaFo.get(i).getCommissionOrder().getCremationTime();
			List<BaseReductionD> item = baseReSaFo.get(i).getBaseReductionD();
			int jieShiFei = 0;
			int taiShiFei = 0;
			int lengCangFei = 0;
			int huoHuaFei = 0;
			for (BaseReductionD it : item) {
				if (ItemTypeConst.PTCJSF.equals(it.getHelpCode())) {
					jieShiFei += it.getTotal();
				} else if (ItemTypeConst.TSF.equals(it.getHelpCode())) {
					taiShiFei += it.getTotal();
				} else if (ItemConst.LengCang_Sort==it.getSort()) {
					lengCangFei += it.getTotal();
				} else if (ItemConst.HuoHua_Sort==it.getSort()) {
					huoHuaFei += it.getTotal();
				}
			}
			/*if(item.getName() != null){
				itemName = item.getName();
				if(itemName.contains("休息室")){
					xiuFees = item.getPice();
				}
			}
			if("".equals(item.getSort())){
				sort = item.getSort();
				if(sort == ItemConst.GuHuiHe_Sort){
					guFees = item.getPice();
				}
			}
			if("".equals(item.getSort())){
				sort = item.getSort();
				if(sort == ItemConst.QiTa_Sort){
					otherFees = item.getPice();
				}
			}*/
			String total = baseReSaFo.get(i).getTotal();
			o[0] = i + 1;
			o[1] = dName;
			o[2] = dSex;
			o[3] = cerCode;
			o[4] = reName;
			o[5] = dTime;
			o[6] = payTime;
			o[7] = creTime;
			o[8] = jieShiFei;
			o[9] = taiShiFei;
			o[10] = lengCangFei;
			o[11] = huoHuaFei;
			o[12] = total;
			list.add(o);
		}
		String registerOption=registerService.getRegisterOption(null, false);
		model.put("list", list);
		model.put("registerOption", registerOption);
		model.put("method", "list");
		model.put("index", index);
		model.put("start", start);
		model.put("end", end);
		return new ModelAndView("ye/statisticalForm/baseFuneralReduction/baseFuneralReduction", model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "基本殡葬减免统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "基本殡葬减免统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "基本殡葬减免统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "基本殡葬减免统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
