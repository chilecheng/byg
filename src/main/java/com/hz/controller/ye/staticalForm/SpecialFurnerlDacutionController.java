package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.base.FurnaceService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 特约炉特约时间
 * @author rgy
 *
 */
@Controller
@Scope("session")
@RequestMapping("/specialFurnerlDacution.do")
public class SpecialFurnerlDacutionController extends BaseController {
	@Autowired
	private FurnaceService furnaceService;  
	@Autowired
	private CommissionOrderService cs;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "specialFurnerlDacution.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return showStaticalForm();
	}

	@RequestMapping(params ="method=list")
	public ModelAndView showStaticalForm() {
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		Map<String, Object> map=new HashMap<String, Object>();
		Date beginDate =DateTools.getThisDate();
		Date endDate =DateTools.getThisDate();
	    if(getDate("beginDate")!=null && getDate("endDate")!=null){
	    	beginDate=getDate("beginDate");
	    	endDate=getDate("endDate");
	    	beginDate = DateTools.getDayFirstTime(beginDate);
	    	map.put("cremationBeginDate", DateTools.getDayFirstTime(getDate("beginDate")));
	    	System.out.println(DateTools.getDayFirstTime(getDate("beginDate"))+"-----------------");
	    	map.put("cremationEndDate", DateTools.getDayEndTime(DateTools.utilDateToSqlDate(getDate("endDate"))));
	    	System.out.println(DateTools.getDayEndTime(DateTools.utilDateToSqlDate(getDate("endDate")))+"-----------------");
	    	//map.put("cremationBeginDate", DateTools.getDayFirstTime(getDate("beginDate")));
	    	//map.put("cremationEndDate", DateTools.getDayLastTime(getDate("endDate")));
	    }else{
	    	map.put("cremationBeginDate", DateTools.getDayFirstTime(beginDate));
	    	map.put("cremationEndDate", DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate)));
//	    	map.put("cremationBeginDate", DateTools.getDayFirstTime(DateTools.getThisDate()));
//			map.put("cremationEndDate", DateTools.getDayLastTime(DateTools.getThisDate()));
	    }
        map.put("checkFlag", Const.Check_Yes);																														
		String type=getString("type");
		System.out.println("---------type="+type);
		String name= null;
		if(type!=null && type!=""){
			beginDate=null;
			endDate=null;
		    if(type.equals("1")){
			    name="7号";
		    }
			if(type.equals("2")){
				name="8号";
			}
			if(type.equals("3")){
				name="9号";
			}
			if(type.equals("4")){
				name="10号";
			}
			if(type.equals("5")){
			}
		}
		map.put("xname", name);

		List<CommissionOrder> co = cs.getCommissionOrderList(map);
		for(int i=0; i<co.size(); i++){
			Object[] o = new  Object[6];
			Date cremationTime = co.get(i).getCremationTime();
			String name1 = co.get(i).getName();
			int age = co.get(i).getAge();
			String sexName = co.get(i).getSexName();
			String fName = "";
			if(co.get(i).getFurnace()!=null&&!co.get(i).getFurnace().equals("")){
				fName = co.get(i).getFurnace().getName();
			}
			String comment = co.get(i).getComment();
			o[0] = cremationTime;
			o[1] = name1;
			o[2] = age;
			o[3] = sexName;
			o[4] = fName;
			o[5] = comment;
			list.add(o);
		}
		model.put("method", "list");
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		System.out.println("--------begin="+beginDate);
		System.out.println("--------end="+endDate);
		System.out.println("--------------name="+name);
		System.out.println("---------type="+type);
		return new ModelAndView("ye/statisticalForm/specialFurnaceOrder/specialFurnaceOrder",model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "特约炉特约时间统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "特约炉特约时间统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //System.out.println("excel导出成功！");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "特约炉特约时间统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "特约炉特约时间统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //System.out.println("excel导出成功！");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
