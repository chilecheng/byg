package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.ItemOrder;
import com.hz.service.ye.CommissionOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;
import com.hz.util.ItemConst;

/**
 * 遗体处理安排表
 * @author rgy
 *
 */
@Controller
@Scope("session")
@RequestMapping("/corpseDealTime.do")
public class CorpseDealTimeController extends BaseController{
	@Autowired
	private CommissionOrderService commissionOrderService;
	
	List<Object[]> list=new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "corpseDealTime.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listFarewell();
		
	}
	/**
	 * 遗体处理安排列表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	public ModelAndView listFarewell(){
		Map<String, Object> model=new HashMap<String, Object>();
		model=getModel(model);
		list.removeAll(list);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		Date beginDate = getDate("beginDate");
		Date endDate = getDate("endDate");
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("isdel", Const.Is_Yes);
		if (beginDate == null || ("").equals(beginDate)) {
			beginDate = DateTools.getDayFirstTime(new Date());
		}else{
			beginDate = DateTools.getDayFirstTime(beginDate);
		}
		if (endDate == null || ("").equals(endDate)) {
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		}else{
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}

		map.put("mourningbeginDate", beginDate);
		map.put("mourningEndDate", endDate);
		map.put("checkFlag", Const.Check_Yes);
		//List<Object> list2=new ArrayList<Object>();
		List<CommissionOrder> listOrder=commissionOrderService.getCommissionOrderList(map);
		for (int i = 0; i < listOrder.size(); i++) {
			//获取服务项目收费list
			Object[] o =new Object[10];
			List<ItemOrder> listItemOrder=listOrder.get(i).getListItemOrder();
			/**
			 * 获取火化委托单信息
			 */
			Date date=new Date();//火化预约时间
			if(listOrder.get(i).getMourningTime()!=null&&!listOrder.get(i).getMourningTime().equals("")){
				date=listOrder.get(i).getMourningTime();
			}
			if(listOrder.get(i).getFarewellTime()!=null&&!listOrder.get(i).getFarewellTime().equals("")){
				date=listOrder.get(i).getFarewellTime();
			}
			if(listOrder.get(i).getCremationTime()!=null&&!listOrder.get(i).getCremationTime().equals("")){
				date=listOrder.get(i).getCremationTime();
			}
			String deadName=listOrder.get(i).getName();//死者名字
			int age=listOrder.get(i).getAge();//死者年龄
			String sexName=listOrder.get(i).getSexName();//死者性别
			
			String farewellName=null;//告别厅
			if(listOrder.get(i).getFarewell()!=null&&!listOrder.get(i).getFarewell().equals("")){
				farewellName=listOrder.get(i).getFarewell().getName();//告别厅	
			}
			String mourningName=null;
			if(listOrder.get(i).getMourning()!=null&&!listOrder.get(i).getMourning().equals("")){
				mourningName=listOrder.get(i).getMourning().getName();		
			}
			String freezerName=null;//冰柜
			if(listOrder.get(i).getFreezer()!=null&&!listOrder.get(i).getFreezer().equals("")){
				freezerName=listOrder.get(i).getFreezer().getName();//冰柜			
			}
			/**
			 * 收费项目
			 */
			String chuangyihuazhuang="";//穿衣化妆
			int chuanyihuazhuangCount=0;
			StringBuffer zhiGuan=new StringBuffer();
			double zhiguan=0;////纸棺
			int zhiguanCount=0;
			StringBuffer zhengRong=new StringBuffer();
			String zhengrong="";//整容
			int zhengrongCount=0;
			byte sort=-1;
			int count=0;
			String itemName="";
			for (int j = 0; j < listItemOrder.size(); j++) {
				ItemOrder it=listItemOrder.get(j);
				if(it.getItem()!=null){
					count=listItemOrder.get(j).getNumber();
					itemName=listItemOrder.get(j).getItem().getName();
					sort=listItemOrder.get(j).getItem().getSort();
					if(sort == ItemConst.ChuanYiHuaZhuang_Sort){
						chuangyihuazhuang=listItemOrder.get(j).getItem().getName();
					}if(sort == ItemConst.ZhiGuan_Sort){
						zhiguan=listItemOrder.get(j).getItem().getPice();
						zhiGuan.append(zhiguan).append(",");
					}if(sort == ItemConst.ZhengRong_Sort){
						zhengrong=listItemOrder.get(j).getItem().getName();
						zhengRong.append(zhengrong).append(",");
					}
				}
			}
			o[0] = date;
			o[1] = deadName;
			o[2] = age;
			o[3] = sexName;
			o[4] = farewellName;
			o[5] = mourningName;
			o[6] = freezerName;
			o[7] = chuangyihuazhuang;
			o[8] = zhiGuan;
			o[9] = zhengRong;
			list.add(o);
		}
		PageHelper.startPage(pages[0], pages[1]);
		PageInfo<Object[]> page=new PageInfo<Object[]>(list);
		model.put("list", list);
		model.put("page",page);
		model.put("method","list");
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		return new ModelAndView("ye/statisticalForm/corpseDealTime/corpseDealTime",model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "遗体处理安排统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "遗体处理安排统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "遗体处理安排统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "遗体处理安排统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

