package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.ye.CommissionOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 纳骨送灵预约时间报表
 * @author rgy
 *
 */
@Controller
@Scope("session")
@RequestMapping("/boneSoulOrder.do")
public class BoneSoulOrderController extends BaseController {
	@Autowired
	private CommissionOrderService cs;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "boneSoulOrder.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return showStaticalForm();
	}
	
	@RequestMapping(params ="method=list")
	public ModelAndView showStaticalForm() {
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		list.removeAll(list);
		//int[] pages=getPage();
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("isdel", Const.Is_Yes);
		Map<String, Object> map=new HashMap<String, Object>();
		Date beginDate=getDate("beginDate");
		Date endDate=getDate("endDate");
		if(beginDate==null || ("").equals(beginDate)){
			beginDate=DateTools.getDayFirstTime(new Date());
		}else{
			beginDate=DateTools.getDayFirstTime(beginDate);
		}
		if(endDate==null || ("").equals(endDate)){
			endDate=DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		}else{
			endDate=DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}
		map.put("cremationBeginDate", beginDate);
		map.put("cremationEndDate", endDate);
		map.put("checkFlag", Const.Check_Yes);
		//PageInfo page=cs.getCommissionOrderPageInfo(map, pages[0],pages[1], null);
		List<CommissionOrder> co = cs.getCommissionOrderList(map);
		for(int i=0; i<co.size(); i++){
			Object[] o = new  Object[6];
			Date cremationTime = co.get(i).getCremationTime();
			String name = co.get(i).getName();
			int age = co.get(i).getAge();
			String sexName = co.get(i).getSexName();
			String fName = "";
			if(co.get(i).getFurnace()!=null&&!co.get(i).getFurnace().equals("")){
				fName = co.get(i).getFurnace().getName();
			}
			String comment = co.get(i).getComment();
			o[0] = cremationTime;
			o[1] = name;
			o[2] = age;
			o[3] = sexName;
			o[4] = fName;
			o[5] = comment;
			list.add(o);
		}
		//model.put("page", page);
		model.put("method", "list");
		model.put("list", list);
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		return new ModelAndView("ye/statisticalForm/boneSoulOrder/boneSoulOrder",model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "纳骨送灵预约时间统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "纳骨送灵预约时间统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "纳骨送灵预约时间统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "纳骨送灵预约时间统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
