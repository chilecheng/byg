package com.hz.controller.ye.staticalForm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.PincardRecord;
import com.hz.service.ye.staticalForm.CloseCardRecordService;
import com.hz.util.DateTools;

/**
 * 销卡记录
 * @author rgy
 *
 */
@Controller
@Scope("session")
@RequestMapping("/closeCardRecord.do")
public class CloseCardRecordController extends BaseController {
	@Autowired
	private CloseCardRecordService ccs;
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "closeCardRecord.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return showStaticalForm();
	}
	
	@RequestMapping(params ="method=list")
	public ModelAndView showStaticalForm() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		int pages[]=getPage();
		Map<String, Object> map = new HashMap<String, Object>();
		Date beginDate = getDate("beginDate");
		Date endDate = getDate("endDate");
		if (beginDate == null || ("").equals(beginDate)) {
			beginDate = DateTools.getDayFirstTime(new Date());
		}else{
			beginDate = DateTools.getDayFirstTime(beginDate);
		}
		if (endDate == null || ("").equals(endDate)) {
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		}else{
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}
		String oldCard = getString("oldCard");
		map.put("oldCard", oldCard);
		//根据火化时间查询
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		PageInfo<PincardRecord> page=ccs.getCloseCardRecordPageInfo(map, pages[0], pages[1]);
		model.put("method", "list");
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		model.put("page", page);
		return new ModelAndView("ye/statisticalForm/pincardQuery/pincardQuery",model);
	}
}
