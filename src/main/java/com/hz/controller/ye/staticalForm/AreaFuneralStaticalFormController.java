package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 火化量统计
 * @author mbz
 *
 */
@Controller
@Scope("session")
@RequestMapping("/areaFuneralStaticalForm.do")
public class AreaFuneralStaticalFormController extends BaseController {
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "areaFuneralStaticalForm.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified() {
		return listAreaFuneralStatical();
	}
	
	/**
	 * 火化量统计报表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	private ModelAndView listAreaFuneralStatical() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		list.removeAll(list);
//		Date startTime = getDate("startTime");
//		Date endTime = getDate("endTime");
//		if (startTime == null) {
//			startTime = DateTools.getThisDate();
//		} else {
//			startTime = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(startTime));
//		}
//		if (endTime == null) {
//			endTime = DateTools.getDayEndTime(DateTools.getThisDate());
//		} else {
//			endTime = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endTime));
//		}
//		//map.put("start", start);
//		//map.put("end", end);
//		//List<Object[]> list = new ArrayList<Object[]>();
//		Map<String, Object> paramap = new HashMap<String, Object>();
//		paramap.put("isdel", Const.Is_Yes);
//		List<Toponym> tList = afsfs.getToponymList(paramap,"name");
//		Object[] first = new Object[tList.size()+2];
//		for(int i = 0; i<first.length; i++ ){
//			if(i == 0){
//				first[i] = "日期";
//			} else if(i == first.length-1){
//				first[i] = "合计";
//			} else {
//				first[i] = tList.get(i-1).getName();
//			}
//		}
//		list.add(first);
//		Object[] other = null;
//		
//		List<Date> dayList = DateTools.getEveryDay(startTime,endTime);
//		for (Date date : dayList) {
//			other = new Object[tList.size()+2];
//			other[0] = date;
//			list.add(other);
//		}
//		Date startDate = null;
//		Date endDate = null;
//		Map<String,Object> map = null;
//		Object[] last = new Object[tList.size()+2];
//		last[0] = "合计";
//		for (int i = 1; i < last.length; i++) {
//			last[i] = 0;
//		}
//		for(int i = 1; i<list.size(); i++){
//			Object[] o = list.get(i);
//			for(int j = 1; j < o.length; j++){
//				map = new HashMap<String, Object>();
//				startDate = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate((Date)o[0]));
//				endDate = DateTools.getDayEndTime(new java.sql.Date(((Date)o[0]).getTime()));
//				map.put("startDate", startDate);
//				map.put("endDate", endDate);
//				map.put("checkFlag", Const.Check_Yes);
//				if(j == o.length-1){
//					o[j] = afsfs.getToponymIdCount(map);
//					last[j] = (Integer)last[j] + afsfs.getToponymIdCount(map);
//				}else{
//					map.put("toponymId",tList.get(j-1).getToponymId());
//					o[j] = afsfs.getToponymIdCount(map);
//					last[j] = (Integer)last[j] + afsfs.getToponymIdCount(map);
//				}
//			}
//		}
//		list.add(last);
//		model.put("list", list);
//		model.put("startTime", startTime);
//		model.put("endTime", endTime);

		return new ModelAndView("ye/statisticalForm/areaFuneralStatic/areaFuneralStatic", model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "火化量统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "火化量统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "火化量统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "火化量统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
