package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.service.ye.staticalForm.CarSchedualService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 车辆调度统计
 * @author cjb
 *
 */
@Controller
@Scope("session")
@RequestMapping("/receiptStatical.do")
public class ReceiptStaticalConller extends BaseController {
	@Autowired
	private CarSchedualService carSchedualService;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "receiptStatical.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified() {
		return listCarList();
	}
	
	@RequestMapping(params="method=list")
	private ModelAndView listCarList() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		list.removeAll(list);
		// 查询
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("isdel", Const.Is_Yes);
		Map<String, Object> map = new HashMap<String, Object>();
		Date start = getDate("startTime");
		Date end = getDate("endTime");
		if (start == null) {
			start = DateTools.getDayFirstTime(DateTools.getThisDate());
		} else {
			start = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(start));
		}
		if (end == null) {
			end = DateTools.getDayEndSqlDate(DateTools.getThisDate());
		} else {
			end = DateTools.getDayEndSqlDate(DateTools.utilDateToSqlDate(end));
		}
		map.put("start", start);
		map.put("end", end);
		map.put("checkFlag", Const.Check_Yes);
		List<CarSchedulRecord> cs = carSchedualService.getCarList(map);
		for(int i=0; i<cs.size(); i++){
			Object[] o = new Object[7];
			Date pickTime = cs.get(i).getPickTime();
			String dAddr = cs.get(i).getCommissionOrder().getdAddr();
			String fPhone = cs.get(i).getCommissionOrder().getfPhone();
			String oName = cs.get(i).getCommissionOrder().getName();
			String tName = cs.get(i).getTransportType().getName();
			String cName = cs.get(i).getCarType().getName();
			String comment = cs.get(i).getComment();
			o[0] = pickTime;
			o[1] = dAddr;
			o[2] = fPhone;
			o[3] = oName;
			o[4] = tName;
			o[5] = cName;
			o[6] = comment;
			list.add(o);
		}
		model.put("method", "list");
		model.put("list", list);
		model.put("start", start);
		model.put("end", end);
//		return new ModelAndView("ye/statisticalForm/dispatchListCar/dispatchListCar", model);
		return null;
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "车辆调度统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "车辆调度统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "车辆调度统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "车辆调度统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
