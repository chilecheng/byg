package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.base.ProveUnit;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.staticalForm.DeadProveUnitService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 非正常死亡单位证明报表
 * @author mbz
 *
 */
@Controller
@Scope("session")
@RequestMapping("/deadUnitProve.do")
public class DeadProveUnitController extends BaseController {
	@Autowired
	private CommissionOrderService cos;
	@Autowired
	private DeadProveUnitService dpus;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "deadUnitProve.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return showStaticalForm();
	}
	
	@RequestMapping(params ="method=list")
	public ModelAndView showStaticalForm() {
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		list.removeAll(list);
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("isdel", Const.Is_Yes);
		Map<String, Object> map=new HashMap<String, Object>();
		//得到开始日期
		Date beginDate=getDate("beginDate");
		//得到结束日期
		Date endDate=getDate("endDate");
		if(beginDate==null || beginDate.equals("")){
			//得到第一天的开始时间
			beginDate=DateTools.getDayFirstTime(new Date());
		} else {
			beginDate=DateTools.getDayFirstTime(beginDate);
		}
		if(endDate==null || endDate.equals("")){
			endDate=DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		} else {
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		map.put("checkFlag", Const.Check_Yes);
		List<ProveUnit> pu = dpus.getDeadProveUnitList(map);
//		for(int i=0; i<pu.size(); i++){
//			Object[] o = new Object[2];
//			String name = pu.get(i).getName();
//			String num = pu.get(i).getProveUnitId();
//			o[0] = name;
//			o[1] = num;
//			list.add(o);
//		}
		Object[] first = new Object[2];
		first[0] = "证明单位";
		first[1] = "数量";
		list.add(first);
		Object[] other = new Object[2];
		for (int i=0; i<pu.size(); i++) {
			other[0] = pu.get(i).getName();
			other[1] = dpus.getDeadProveUnitIdCount(map);
			list.add(other);
		}
		Object[] last = new Object[2];
		last[0] = "合计";
//		Map<String,Object> maps = new HashMap<String, Object>();
//		for(int i = 1; i<list.size(); i++){
//			Object[] o = list.get(i);
//			for(int j = 1; j < o.length; j++){
//				maps = new HashMap<String, Object>();
//				maps.put("checkFlag", Const.Check_Yes);
//				if(j == o.length-1){
//					o[j] = dpus.getDeadProveUnitIdCount(map);
//					last[j] = (Integer)last[j] + dpus.getDeadProveUnitIdCount(map);
//				}else{
//					maps.put("proveUnitId",pu.get(j-1).getProveUnitId());
//					o[j] = dpus.getDeadProveUnitIdCount(map);
//					last[j] = (Integer)last[j] + dpus.getDeadProveUnitIdCount(map);
//				}
//			}
//		}
		
//		List<Map<String,Integer>> listMap = new ArrayList<Map<String,Integer>>();
//		Map<String,Integer> maps = new HashMap<String, Integer>();
//		for (CommissionOrder co : list) {
//			int count = 0;
//			String name = co.getProveUnit().getName();
//			if (maps.containsKey(name)) {
//				list.remove(maps);
//				count = maps.get(name) + 1;
//				maps.put(name, count);
//				list.add(maps);
//			} else {
//				list.remove(maps);
//				count += 1;
//				maps.put(name, count);
//				list.add(maps);
//			}
//		}
//		Map<String,Integer> lastMap=new HashMap<String, Integer>();
//		lastMap.put("合计", list.size());
//		list.add(lastMap);
		list.add(last);
		model.put("list", list);
		model.put("method", "list");
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		return new ModelAndView("ye/statisticalForm/unNormalDieStatic/deadUnitProve",model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "非正常死亡单位证明统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "非正常死亡单位证明统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //System.out.println("excel导出成功！");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "非正常死亡单位证明统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "非正常死亡单位证明统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //System.out.println("excel导出成功！");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
