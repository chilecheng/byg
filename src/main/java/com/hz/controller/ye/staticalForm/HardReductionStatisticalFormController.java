package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.staticalForm.HardReductionStaticalForm;
import com.hz.service.ye.staticalForm.HardReductionStaticalFormService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;
import com.hz.util.ItemConst;
import com.hz.util.ItemTypeConst;
/**
 * 困难殡葬减免统计
 * @author mbz
 *
 */
@Controller
@Scope("session")
@RequestMapping("/hardReductionStatisticalForm.do")
public class HardReductionStatisticalFormController extends BaseController {
	@Autowired
	private HardReductionStaticalFormService hrsfs;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "hardReductionStatisticalForm.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified() {
		return listHardReductionStatistical();
	}
	
	/**
	 * 困难减免统计报表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	private ModelAndView listHardReductionStatistical() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		list.removeAll(list);
//		list.add(new Object[]{"序号","姓名","性别","身份证号","户口所在地","死亡时间","结算时间","火化时间","骨灰盒","休息室","其他减免","免费金额"});
		// 查询
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isdel", Const.Is_Yes);
		Date start = getDate("startTime");
		Date end = getDate("endTime");
//		if (start == null || ("").equals(start)) {
//			start = DateTools.getDayFirstTime(new Date());
//		}
//		if (end == null || ("").equals(end)) {
//			end = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(start));
//		}
//		String type = getString("type");
//		if ("1".equals(type)) {
//			start = DateTools.getDayFirstTime(DateTools.getThisDate());
//			end = DateTools.getDayEndTime(DateTools.getThisDate());
//		}
//		if ("2".equals(type)) {
//			Date tomorrow = DateTools.getDayAfterDay(DateTools.getThisDate(), 1, Calendar.DAY_OF_YEAR);
//			start = DateTools.getDayFirstTime(tomorrow);
//			end = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(tomorrow));
//		}
		
		if (start == null) {
			start = DateTools.getThisDate();
		} else {
			start = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(start));
		}
		if (end == null) {
			end = DateTools.getDayEndTime(DateTools.getThisDate());
		} else {
			end = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(end));
		}
		
		map.put("start", start);
		map.put("end", end);
		String index = "";
		String name = getString("name");
		String area = getString("area");
		map.put("name", name);
		map.put("area", area);
		map.put("checkFlag", Const.Check_Yes);
		List<HardReductionStaticalForm> hardReSaFo = hrsfs.findHardReductionSF(map);
		
		//List<Object> list1 = new ArrayList<Object>();
		for(int i = 0; i<hardReSaFo.size(); i++){
			Object[] o = new Object[12];
			String dName = hardReSaFo.get(i).getCommissionOrder().getName();
			int dSex = hardReSaFo.get(i).getCommissionOrder().getSex();
			String cerCode = hardReSaFo.get(i).getCommissionOrder().getCertificateCode();
			String reName = hardReSaFo.get(i).getToponym().getName();
			Date dTime = hardReSaFo.get(i).getCommissionOrder().getdTime();
			Timestamp payTime = hardReSaFo.get(i).getPayTime();
			List<HardReductionD> item = hardReSaFo.get(i).getHardReductionD();
			Date creTime = hardReSaFo.get(i).getCommissionOrder().getCremationTime();
			double guHuiHe = 0;
			double xiuXiShi = 0;
			double qiTa = 0;
			for (HardReductionD it : item) {
				if (ItemConst.GuHuiHe_Sort==it.getSort()) {
					guHuiHe += it.getTotal();
				} else if (ItemTypeConst.XXS.equals(it.getHelpCode())) {
					xiuXiShi += it.getTotal();
				} else {
					qiTa += it.getTotal();
				}
			}
			String total = hardReSaFo.get(i).getTotal();
			o[0] = i + 1;
			o[1] = dName;
			o[2] = dSex;
			o[3] = cerCode;
			o[4] = reName;
			o[5] = dTime;
			o[6] = payTime;
			o[7] = creTime;
			o[8] = guHuiHe;
			o[9] = xiuXiShi;
			o[10] = qiTa;
			o[11] = total;
			list.add(o);
		}
//		String registerOption=registerService.getRegisterOption(null, false);
		model.put("list", list);
//		model.put("registerOption", registerOption);
		model.put("method", "list");
		model.put("index", index);
		model.put("start", start);
		model.put("end", end);
		return new ModelAndView("ye/statisticalForm/hardFuneralReduction/hardFuneralReduction", model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "困难殡葬减免统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "困难殡葬减免统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "困难殡葬减免统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "困难殡葬减免统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
