package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.DelegateRecord;
import com.hz.entity.ye.staticalForm.ChargeStaticForm;
import com.hz.service.ye.staticalForm.ChargeStaticService;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.mysql.fabric.xmlrpc.base.Data;

/**
 * 收费统计 报表 
 * @author gaopengfei
 *
 */
@Controller
@Scope("session")
@RequestMapping("/chargeStatic.do")
public class ChargeStaticController  extends BaseController {
	
	@Autowired
	private ChargeStaticService chargeStaticService;
	List<Object[]> list = new ArrayList<Object[]>();
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "chargeStatic.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listCharge();
    }

	/**
	 * 获取  收费统计报表list
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listCharge() {
		  Map<String,Object> model=new HashMap<String,Object>();
			model=getModel(model);
			//获得页面页码信息
			int[] pages = getPage();
			//填入查询内容
			Map<String,Object> maps=new HashMap<String,Object>();	
			list.removeAll(list);
			Date startTime = getDate("startTime");
			Date endTime = getDate("endTime");
			if (startTime == null) {
				startTime = DateTools.getThisDate();
			} else {
				startTime = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(startTime));
			}
			if (endTime == null) {
			//	endTime = DateTools.getDayEndTime(DateTools.getThisDate());
				endTime = DateTools.getThisDate();
			} else {
				endTime = DateTools.getThisDate();
				// endTime = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endTime));
			}
			
//			maps.put("startTime", getDate("startTime"));
			maps.put("startTime", startTime);
			maps.put("endTime", endTime);
		  
			
			ChargeStaticForm charge	=  chargeStaticService.SumChargeStatic(maps);
		  
		PageInfo<ChargeStaticForm> page=chargeStaticService.getdelegateRecordPageInfo(maps,pages[0],pages[1],"");	
	//	PageInfo<ChargeStaticForm> page2=chargeStaticService.getdelegateRecordPageInfo2(maps,pages[0],pages[1],"");
		
		List<ChargeStaticForm> ch = page.getList();
	//	List<ChargeStaticForm> ch = chargeStaticService.getchargeStaticList(maps);
		for(int i=0; i<ch.size(); i++){
			Object[] o = new  Object[5];
			String name = ch.get(i).getName();
			int number = ch.get(i).getNumber();
			Double yingshou = ch.get(i).getYingshou();
			Double yishou = ch.get(i).getYishou();
			Double weishou = ch.get(i).getWeishou();
			o[0] = name;
			o[1] = number;
			o[2] = yingshou;
			o[3] = yishou;
			o[4] = weishou;
			list.add(o);
		}
		
		
		model.put("charge",charge);
/*		model.put("number",charge.getNumber());
		model.put("yishou", charge.getYishou());
		model.put("weishou", charge.getWeishou());
		model.put("yingshou", charge.getYingshou());*/	
		model.put("startTime", startTime);
		model.put("endTime", endTime);
		
		model.put("page", page);

		model.put("list", list);
		model.put("method", "list");
		  return new ModelAndView("ye/statisticalForm/chargeStatic/chargeStatic",model);
	  }
	
	/**
     * 导出Excel测试(Gpf)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "收费统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
     
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "收费统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
	
	
	
	

}
