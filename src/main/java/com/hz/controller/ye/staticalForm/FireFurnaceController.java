package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.base.Furnace;
import com.hz.service.base.FurnaceService;
import com.hz.service.ye.FurnaceRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 火化统计
 * @author rgy
 *
 */

@Controller
@Scope("session")
@RequestMapping("/fireFurnaceRecord.do")
public class FireFurnaceController extends BaseController{
	@Autowired
	private FurnaceService furnaceService;
	List<Object[]> list=new ArrayList<Object[]>();
	@Autowired
	private FurnaceRecordService frs;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url","fireFurnaceRecord.do");
	}
	
	
	@RequestMapping
	public ModelAndView unspecified(){
		return fireFurnaceController();
		
	}
	
	@RequestMapping(params ="method=list")
	public ModelAndView fireFurnaceController(){
		Map<String,Object> model=new HashMap<String, Object>();
		list.removeAll(list);
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("isdel", Const.Is_Yes);
		//获取时间
		Map<String,Object> maps=new HashMap<String, Object>();
		Date beginDate = getDate("beginDate");
		Date endDate=getDate("endDate");
		if(beginDate==null){
			beginDate = DateTools.getThisDate();
		}
		if(endDate == null){
			endDate = DateTools.getThisDate();
		}
		maps.put("beginDate", beginDate);
		maps.put("endDate", endDate);
		maps.put("isdel", Const.Is_Yes);
		//获取火化炉的list
		List<Furnace> listFurnace=furnaceService.getFurnaceList(maps, "");
		//火化炉加两个长度
		int objectLength=listFurnace.size()+2;
		//定义第一行数组
		Object []firstLength=new Object[objectLength];
		//遍历新的火化炉
		for (int i = 0; i < firstLength.length; i++) {
			if(i == 0){
				firstLength[i]="时间";
			}else if(i == firstLength.length-1){
				firstLength[i]="合计";
			}else{
				firstLength[i]=listFurnace.get(i-1).getName();
			}
		}
		list.add(firstLength);
		Object[] obj=null;
		//获取一段时间的list
		List<Date> listDay=DateTools.getEveryDay(beginDate, endDate);
		for (Date date : listDay) {
			obj=new Object[objectLength];
			obj[0]=date;
			list.add(obj);
		}
		Date startTime=null;
		Date endTime=null;
		Map<String,Object> map=null;
		//定义最后一行数组
		Object []last=new Object[objectLength];
		last[0]="合计";
		for (int i =1; i < last.length; i++) {
			last[i]=0;
		}
		//遍历list
		for (int i = 1; i < list.size(); i++) {
			Object []o=list.get(i);
			for (int j = 1; j < o.length; j++) {
				map=new HashMap<String, Object>();
				startTime=DateTools.getDayFirstTime((Date)o[0]);
				endTime=DateTools.getDayEndTime(new java.sql.Date(((Date)o[0]).getTime()));
				map.put("startTime", startTime);
				map.put("endTime",endTime);
				map.put("checkFlag", Const.Check_Yes);
				if(j==o.length-1){
					//最后一列总计
					o[j]=frs.getFurnaceRecordCount(map);
					//最后一行总计
					last[j]=(Integer)last[j]+frs.getFurnaceRecordCount(map);
				}else{
					//获取炉号根据id查找
					map.put("id", listFurnace.get(j-1).getId());
					o[j]=frs.getFurnaceRecordCount(map);
					last[j]=(Integer)last[j]+frs.getFurnaceRecordCount(map);
				}
			}
		}
		list.add(last);
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		model.put("method", "list");
		model.put("list", list);
		return new ModelAndView("ye/statisticalForm/funeralStatistic/funeralStatistic",model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "火化统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "火化统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "火化统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "火化统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
