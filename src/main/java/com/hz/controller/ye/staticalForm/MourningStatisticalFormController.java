package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.base.Mourning;
import com.hz.service.base.MourningService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 灵堂统计
 * @author hujun
 *
 */
@Controller
@Scope("session")
@RequestMapping("/mouringStatisticalForm.do")
public class MourningStatisticalFormController extends BaseController {
	
	@Autowired
	private MourningRecordService mourningRecordService;
	@Autowired
	private MourningService mourningService;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "mouringStatisticalForm.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return showStaticalForm();
	}
	
	@RequestMapping(params ="method=list")
	public ModelAndView showStaticalForm() {
		list.removeAll(list);
		Map<String, Object> model=new HashMap<String, Object>();
		Date startDate = getDate("startTime");
		Date endDate = getDate("endTime");
		if (startDate == null) {
			startDate = DateTools.getThisDate();
		}
		if (endDate == null) {
			endDate = DateTools.getThisDate();
		}
		Date startTime = null;
		Date endTime = null;
		//List<Object[]> list = new ArrayList<Object[]>();
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("isdel", Const.Is_Yes);
		List<Mourning> mourninglist = mourningService.getMourningList(paramMap, "name");
		int objectLength = mourninglist.size() + 2;//？？？？
		Object[] firstLine = new Object[objectLength];
		for (int i = 0; i < firstLine.length; i++) {
			if (i == 0) {
				firstLine[i] = "日期";
			} else if (i == firstLine.length - 1) {
				firstLine[i] = "合计";
			} else {
					firstLine[i] = mourninglist.get(i-1).getName();
			}
		}
		list.add(firstLine);
		Object[] o = null;
		List<Date> dayList = DateTools.getEveryDay(startDate,endDate);
		Map<String,Object> maps = null;
		for (Date date : dayList) {
			startTime = DateTools.getDayFirstTime(date);
			endTime = DateTools.getDayEndTime(new java.sql.Date(date.getTime()));
			maps = new HashMap<String, Object>();
			maps.put("startTime", startTime);
			maps.put("endTime", endTime);
			maps.put("checkFlag", Const.Check_Yes);
			if (mourningRecordService.getMourningRecordCount(maps) > 0) {
				o = new Object[objectLength];
				o[0] = date;
				list.add(o);
			}
		}
		Map<String,Object> map = null;
		Object[] last = new Object[objectLength];
		last[0] = "合计";
		for (int i = 1; i < last.length; i++) {
			last[i] = 0;
		}
		for (int i = 1; i < list.size(); i++) {
			Object[] o1 = list.get(i);
			for (int j = 1; j < o1.length; j++) {
				map = new HashMap<String, Object>();
				startTime = DateTools.getDayFirstTime((Date)o1[0]);
				endTime = DateTools.getDayEndTime(new java.sql.Date(((Date)o1[0]).getTime()));
				map.put("startTime", startTime);
				map.put("endTime", endTime);
				map.put("checkFlag", Const.Check_Yes);
				if (j == o1.length -1) {
					o1[j] = mourningRecordService.getMourningRecordCount(map);
					last[j] = (Integer)last[j] + mourningRecordService.getMourningRecordCount(map);
				} else {
					map.put("id",mourninglist.get(j-1).getId());
					o1[j] = mourningRecordService.getMourningRecordCount(map);
					last[j] = (Integer)last[j] + mourningRecordService.getMourningRecordCount(map);
				}
			}
		}
		list.add(last);
		model.put("startTime", startDate);
		model.put("endTime", endDate);
		model.put("list", list);
		return new ModelAndView("ye/statisticalForm/mourningStatic/mourningStatic",model);
	}
	

	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "灵堂统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "灵堂统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "灵堂统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "灵堂统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
