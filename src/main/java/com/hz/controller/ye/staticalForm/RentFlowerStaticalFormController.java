package com.hz.controller.ye.staticalForm;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.base.Item;
import com.hz.entity.ye.staticalForm.RentFlowerStaticalForm;
import com.hz.service.ye.staticalForm.RentFlowerStaticalFormService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;
/**
 * 租售花圈花篮统计
 * @author mbz
 *
 */
@Controller
@Scope("session")
@RequestMapping("/rentFlowerStaticalForm.do")
public class RentFlowerStaticalFormController extends BaseController {
	@Autowired
	private RentFlowerStaticalFormService rfsfs;
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "rentFlowerStaticalForm.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified() {
		return listRentFlowerStaticalForm();
	}
	
	/**
	 * 租售花圈花篮统计
	 * @return
	 */
	@RequestMapping(params ="method=list")
	private ModelAndView listRentFlowerStaticalForm() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		list.removeAll(list);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isdel", Const.Is_Yes);
		// 查询
		Date startTime = getDate("startTime");
		Date endTime = getDate("endTime");
		if (startTime == null) {
			startTime = DateTools.getThisDate();
		} else {
			startTime = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(startTime));
		}
		if (endTime == null) {
			endTime = DateTools.getDayEndTime(DateTools.getThisDate());
		} else {
			endTime = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endTime));
		}
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		map.put("checkFlag", Const.Check_Yes); 
		
		List<RentFlowerStaticalForm> rfsf = rfsfs.findRentFlowerSF(map);
//		for(int i=0; i<rfsf.size(); i++){
//			Object[] o = new Object[18]; 
//			Date creatTiem = rfsf.get(i).getCreatTime();
//			String itemName = "";
//			int num = 0;
//			List<Item> itemList = rfsfs.findItemNameList(map);
//			for(int j=0; j<itemList.size(); j++){
//				Item it = itemList.get(j);
//				if(it.getName().contains("绢花圈")){
//					num += rfsf.get(j).getNumber();
//				}
//			}
//			
//		}
		
		
		
		model.put("list", list);
		model.put("method", "list");
		model.put("startTime", startTime);
		model.put("endTime", endTime);
		return new ModelAndView("ye/statisticalForm/rentFlowerStatic/rentFlowerStatic", model);	
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "租售花圈花篮统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "租售花圈花篮统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "租售花圈花篮统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "租售花圈花篮统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
