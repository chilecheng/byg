package com.hz.controller.ye;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.BaseReduction;
import com.hz.service.ye.BaseReductionService;

/**
 * ������������
 * @author rgy
 *
 */
@Controller
@RequestMapping("/baseReduction.do")
public class BaseReductionController extends BaseController{
	@Autowired
	private BaseReductionService baseReductionService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "baseReduction.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listBaseReduction();
    }

	/**
	 * �������������б�
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listBaseReduction(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//���ҳ��ҳ����Ϣ
		int[] pages = getPage();
		//�����ѯ����
		Map maps=new HashMap();
		PageInfo page=baseReductionService.getBaseReductionPageInfo(maps,pages[0],pages[1],null);
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("ye/baseReduction/listBaseReduction",model);
    }
	/**
	 * ���ӵ��޸�����ҳ��
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editBaseReduction(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		BaseReduction baseReduction=new BaseReduction();
		if(id!=null&&!id.equals("")){
			baseReduction=baseReductionService.getBaseReductionId(id);
		}
        model.put("baseReduction", baseReduction);
        model.put("method", "save");
		return new ModelAndView("ye/baseReduction/listBaseReduction",model);
    }
    /**
     * ���������������
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "�����������������Ϣ")
    @ResponseBody
    public JSONObject saveBaseReduction(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		BaseReduction baseReduction=new BaseReduction();
    		if (id!=null&&!id.equals("")) {
    			baseReduction=baseReductionService.getBaseReductionId(id);
    		}
    		baseReductionService.addOrUpdate(id,baseReduction);
    		setJsonBySuccess(json, "����ɹ�", true);
		} catch (Exception e) {
			setJsonByFail(json, "����ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
    
    /**
     * ɾ��������������
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "ɾ����������������Ϣ")
    @ResponseBody
    public JSONObject deleteBaseReduction(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
	    	baseReductionService.deleteBaseReduction(id);
    		setJsonBySuccess(json, "ɾ���ɹ�", true);
		} catch (Exception e) {
			setJsonByFail(json, "ɾ��ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
}
