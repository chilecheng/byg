package com.hz.controller.ye.firstDepart;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.system.User;
import com.hz.entity.ye.BuyWreathRecord;
import com.hz.entity.ye.BuyWreathRecordD;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.base.FarewellService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.MourningService;
import com.hz.service.system.UserService;
import com.hz.service.ye.BuyWreathRecordDService;
import com.hz.service.ye.BuyWreathRecordService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 花圈花篮订购
 * @author hw
 *
 */
@Controller
@RequestMapping("/buyWreathRecord.do")
public class BuyWreathRecordController extends BaseController {
	@Autowired
	private BuyWreathRecordService buyWreathRecordService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private MourningRecordService mourningRecordService;
	@Autowired
	private ItemTypeService itemTypeService;
	@Autowired
	private ItemService itemService;
	@Autowired
	private BuyWreathRecordDService buyWreathRecordDService;
	@Autowired
	private UserService userService;
	@Autowired
	private MourningService mourningService;
	@Autowired
	private FarewellService farewellService;
	@Autowired
	private FarewellRecordService farewellRecordService;
	 @ModelAttribute  
	    public void populateModel( Model model) {  
	       model.addAttribute("url", "buyWreathRecord.do");
	    }
		
		@RequestMapping
	    public ModelAndView unspecified() {
			return listbuyWreathRecord();
	    }
		/**
		 *花圈花篮订购
		 * @return
		 */
	    @RequestMapping(params = "method=list")
	    public ModelAndView listbuyWreathRecord(){
	    	Map<String,Object> model=new HashMap<String,Object>();
	    	model=getModel(model);
	    	//获得页面页码信息
			int[] pages = getPage();
			//插入查询条件
			Map<String, Object> maps = new HashMap<String, Object>();
			String input = getString("search");
			String startTime = getString("startTime");
			String endTime = getString("endTime");
			
			String checkType=getString("checkType");
			if(checkType!=null && "payNo".equals(checkType)){
				maps.put("pay_flag", Const.Pay_No);
			}
			if(checkType!=null && "payYes".equals(checkType)){
				maps.put("pay_flag", Const.Pay_Yes);
			}
			if(startTime!=null && !"".equals(startTime)){
				maps.put("begin", startTime);
			}
			if(endTime!=null && !"".equals(endTime)){
				maps.put("end", endTime+"23:59:59");
			}
			String type = getString("type");
			if(type.equals("deadName")){
				maps.put("deadName", input);
			}
			if(type.equals("writeUser")){
				maps.put("writeUser", input);
			}
			if(type.equals("orderUser")){
				maps.put("orderUser", input);
			}
			if(type.equals("buyNumber")){
				maps.put("buyNumber", input);
			}
			maps.put("search",input);
			maps.put("checkFlag", Const.Check_Yes);
			PageInfo<BuyWreathRecord> page=buyWreathRecordService.getBuyWreathRecordPageInfo(maps,pages[0],pages[1],"creat_time desc");
			model.put("page", page);
			model.put("method", "list");
			model.put("checkType", checkType);
			return new ModelAndView("ye/first_department/flower/buyFlower",model);
	    }
		/**
		 *花圈花篮订购
		 * @return
		 */
	    @RequestMapping(params = "method=edit")
	    public ModelAndView editbuyWreathRecord(){
	    	Map<String,Object> model=new HashMap<String,Object>();
	    	//获取添加人姓名
	    	model.put("agentUser", getCurrentUser().getName());
	    	List<Object[]> list1 = new ArrayList<Object[]>();
	    	List<User> users = userService.getUserListByRoleName(Const.WRITER);
	    	for(int i=0;i<users.size();i++){
	    		list1.add(new Object[]{users.get(i).getUserId(),users.get(i).getName()});
	    	}
	    	String writer=DataTools.getOptionByList(list1, null, false);
	    	model.put("writer",writer);
	    	model.put("method", "save");
	    	return new ModelAndView("ye/first_department/flower/toBuyFlower",model);
	    }
	    /**
	     * 死者姓名寻找
	     * @return
	     */
	    @RequestMapping(params = "method=dNameSearch")
	    @ResponseBody
	    public JSONObject dNameSearch(){
			JSONObject json = new JSONObject();
	    	String dName = getString("dName");
	    	Map<String, Object> maps = new HashMap<String, Object>();
	    	maps.put("name", dName);
	    	maps.put("checkFlag", Const.Check_Yes);
	    	maps.put("goto", Const.GOTO3);//用于区分查询火化委托单的接口(只查所需数据，提高性能)
	    	List<CommissionOrder> list = commissionOrderService.getCommissionOrderList(maps);
	    	CommissionOrder co = null;
	    	String dSex;
	    	String mourningPlace=null;
	    	int dAge;
	    	Timestamp beginDate=null;
	    	Timestamp endDate=null;
	    	String farewellPlace=null;
	    	Timestamp farewellBeginDate=null;
	    	String farewellDate="";
	    	String mourningBeginDate="";
	    	String mourningEndDate="";
	    	if(list.size()==1){
	    		co = list.get(0);
	    		dSex = co.getSexName();
		    	dAge = co.getAge();
		    	if(co.getMourningId() !=null && !"".equals(co.getMourningId())){
		    		//灵堂号
		    		mourningPlace = co.getMourning().getName();
		    		//守灵时间
		    		beginDate = co.getMourningTime();
		    		if (beginDate != null) {
		    			mourningBeginDate=DateTools.getTimeFormatString("yyyy-MM-dd hh:mm", beginDate);
					}
		    		endDate = co.getMourningEndTime();
		    		if (endDate != null) {
		    			mourningEndDate=DateTools.getTimeFormatString("yyyy-MM-dd hh:mm", endDate);
					}
		    	}
		    	if(co.getFarewellId() !=null && !"".equals(co.getFarewellId())){
		    		//告别厅号
		    		farewellPlace = co.getFarewell().getName();
		    		//告别时间
		    		farewellBeginDate = co.getFarewellTime();
		    		farewellDate=DateTools.getTimeFormatString("yyyy-MM-dd hh:mm", farewellBeginDate);
		    	}
		    	String name = co.getName();
		    	String certificateCode = co.getCertificateCode();
		    	Timestamp cremationTime0 = co.getCremationTime();
		    	String cremationTime="";
		    	if(cremationTime0!=null){
		    		cremationTime = DateTools.getTimeFormatString("yyyy-MM-dd hh:mm", cremationTime0);
		    	}
		    	json.put("name", name);
		    	json.put("dNameId",co.getId());
		    	json.put("dSex", dSex);
		    	json.put("dAge", dAge);
		    	json.put("mourningPlace", mourningPlace);
		    	json.put("beginDate", mourningBeginDate);
		    	json.put("endDate", mourningEndDate);
		    	json.put("farewellPlace", farewellPlace);
		    	json.put("farewellDate", farewellDate);
		    	json.put("cremationTime", cremationTime);
		    	json.put("certificateCode", certificateCode);
		    	json.put("nowDate", DateTools.getTimeString("yyyy-MM-dd "));
		    	json.put("dName", dName);
	    	}
	    	json.put("size", list.size());
			return json;
	    }
	    /**
		 *花圈花篮订购添加
		 * @return
		 */
	    @RequestMapping(params = "method=dNameList")
	    public ModelAndView dNameListbuyWreathRecord(){
	    	Map<String,Object> model=new HashMap<String,Object>();
	    	model=getModel(model);
	    	//获得页面页码信息
//			int[] pages = getPage();
	    	String dName = getString("dName");
	    	Map<String, Object> maps = new HashMap<String, Object>();
	    	maps.put("name", dName);
	    	maps.put("checkFlag", Const.Check_Yes);
	    	maps.put("goto", Const.GOTO3);//用于区分查询火化委托单的接口(只查所需数据，提高性能) 花圈花篮购买页面 点姓名搜索的弹出框
//	    	PageHelper.startPage(pages[0], pages[1]);
//			PageHelper.orderBy("creat_time");
	    	List<CommissionOrder> list = commissionOrderService.getCommissionOrderList(maps);
//	    	PageInfo<CommissionOrder> page1 = new PageInfo<CommissionOrder>(list);
	    	List<Object[]> list0 = new ArrayList<Object[]>();
	    	for(int i=0;i<list.size();i++){
	    		String mourningPlace=null;
	    		String farewellPlace=null;
		    	Timestamp farewellDate=null;
		    	//灵堂号
		    	Timestamp beginDate=null;
		    	Timestamp endDate=null;
		    	if(list.get(i).getMourningId()!=null && !"".equals(list.get(i).getMourningId())){
		    		mourningPlace = list.get(i).getMourning().getName();
		    		beginDate = list.get(i).getMourningTime();
		    		endDate = list.get(i).getMourningEndTime();
		    	}
		    	if(list.get(i).getFarewellId()!=null && !"".equals(list.get(i).getFarewellId())){
		    		farewellPlace = list.get(i).getFarewell().getName();
		    		farewellDate = list.get(i).getFarewellTime();
		    	}
		    	String certificateCode = list.get(i).getCertificateCode();
		    	Timestamp cremationTime0 = list.get(i).getCremationTime();
		    	String cremationTime="";
		    	if(cremationTime0!=null){
		    		cremationTime = DateTools.getTimeFormatString("yyyy-MM-dd hh:mm:ss", cremationTime0);
		    	}
		    	
		    	list0.add(new Object[]{list.get(i).getId(),list.get(i).getName(),list.get(i).getArriveTime(),list.get(i).getCertificateCode(),list.get(i).getdAddr(),list.get(i).getAge(),
		    			list.get(i).getSexName(),mourningPlace,beginDate,endDate,farewellPlace,farewellDate,certificateCode,cremationTime});
	    	}
//	    	PageInfo<Object[]> page = new PageInfo<Object[]>(list0);
//	    	page.setPageNum(page1.getPageNum());
//			page.setSize(page1.getSize());
//			page.setPageSize(page1.getPageSize());
//			page.setStartRow(page1.getStartRow());
//			page.setEndRow(page1.getEndRow());
//			page.setTotal(page1.getTotal());
//			page.setPages(page1.getPages());
			model.put("pageSearch", list0);
			model.put("dName", dName);
			model.put("nowDate", DateTools.getTimeString("yyyy-MM-dd"));
	    	return new ModelAndView("../../common/loser",model);
	    }
	  	    /**
	  	     * 花圈花篮订购添加
	  	     * @return
	  	     */
	  	    @RequestMapping(params = "method=insert")
	  	    public ModelAndView insertbuyWreathRecord(){
	  	    	Map<String,Object> model=new HashMap<String,Object>();
	  		
	  			//添加服务项目
	  			List<Object[]> itemList=new ArrayList<Object[]>();
	  			Map<String, Object> maps=new HashMap<String, Object>();
	  			maps.put("name","花圈花篮");
	  			maps.put("isdel", Const.Isdel_No);
	  			List<ItemType> itemTypeList1=itemTypeService.getItemTypeList(maps);
	  			for (int i = 0; i < itemTypeList1.size(); i++) {
	  				ItemType itemType=itemTypeList1.get(i);
	  				Map<String, Object> itemMaps=new HashMap<String, Object>();
	  				itemMaps.put("typeId", itemType.getId());
	  				itemMaps.put("isdel", Const.Isdel_No);
	  				List<Item> itemList1=itemService.getItemList(itemMaps);
	  				itemList.add(new Object[]{itemType,itemList1});
	  			}
	  			model.put("itemTree", getTree(itemList));
	  	    	return new ModelAndView("ye/first_department/flower/selectItems",model);
	  	    }
	    /**
	     * 添加花圈花篮
	     * @return
	     */
	    @RequestMapping(params = "method=addItems")
	    @ResponseBody
	    public List<Object[]> saveItemOrder(){
			List<Object[]> list = new ArrayList<Object[]>();
			String id =getString("ids");
			String[] ids = id.split(",");
			
			Map<String, Object> maps=new HashMap<String, Object>();
		    for(int i=0;i<ids.length;i++){
		    	if(!ids[i].equals("")){
		    		Item item=itemService.getItemById(ids[i]);
			    	maps.put("typeId", item.getTypeId());
			    	maps.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(maps,ids[i],false);
			    	list.add(new Object[]{itemOption,item.getPice(),1,item.getPice()});
		    	}
		    }
			return list;
	    }
	    /**
	     * 保存花圈花篮订购
	     * @return
	     */
	    @RequestMapping(params = "method=save")
	    @SystemControllerLog(description = "保存花圈花篮订购")
	    @ResponseBody
	    public JSONObject savebuyWreathRecord(){
			JSONObject json = new JSONObject();
	    	try {
	    		String bwrId = getString("bwrId");
	    		String dNameId = getString("dNameId");
	    		String name =getString("dName");
	    		
	    		String[] tbody = getArrayString("table");
	    		if (tbody == null) {
	    			json.put("statusCode", 302);
	    			json.put("message", "请选择购买项目");
	    			return json;
				}
	    		String id = UuidUtil.get32UUID();
	    		String ordernum = getString("ordernum");
	    		String contact = getString("contact");
	    		String createUserId = getCurrentUser().getUserId();
	    		String buyer = getString("buyer");
	    		Timestamp buyTime = getTimeM("buyTime");
	    		String writer = getString("writer");
	    		String assiginUserId = getString("assiginUserId");
	    		String bname = getString("bname");
	    		String comment = getString("comment");
	    		buyWreathRecordService.savebuyWreathRecord(bwrId,dNameId,tbody,id,ordernum,contact,
	    				createUserId,buyer,buyTime,writer,assiginUserId,bname,comment,name);
	    		setJsonBySuccess(json, "保存成功", "buyWreathRecord.do");
			} catch (Exception e) {
				setJsonByFail(json, "保存失败，错误"+e.getMessage());
			}
			return json;
	    }
	    
	    /**
		 *花圈花篮订购显示
		 * @return
		 */
	    @RequestMapping(params = "method=show")
	    public ModelAndView showbuyWreathRecord(){
	    	Map<String,Object> model=new HashMap<String,Object>();
	    	String id = getString("id");
	    	BuyWreathRecord buyWreathRecord = buyWreathRecordService.getBuyWreathRecordId(id);
	    	CommissionOrder com = new CommissionOrder();
	    	com = buyWreathRecord.getCommissionOrder();
	    	String deadName = null;
	    	Integer dAge = null;
	    	String dSex = null;
	    	if (com != null) {
	    		deadName = com.getName();
	    		dAge = com.getAge();
	    		dSex = com.getSexName();
			}else{
				deadName=buyWreathRecord.getdName();
			}
		    String orderUser = buyWreathRecord.getOrderUser();
		    String contact = buyWreathRecord.getContact();
		    Timestamp ts = buyWreathRecord.getCreatTime();
		    Date buyTime = DateTools.getDate(ts);
		    String mourningPlace = null;
		    Date mourStart = null;
		    Date mourEnd = null;
		    String mourningTime = null;
		    if (com != null && com.getMourningId() != null) {
		    	mourStart = DateTools.getDate(mourningRecordService.getMourningRecordByOId(buyWreathRecord.getCommissionOrderId()).getBeginTime());
		    	mourEnd = DateTools.getDate(mourningRecordService.getMourningRecordByOId(buyWreathRecord.getCommissionOrderId()).getEndTime());
		    	mourningPlace = mourningService.getMourningById(buyWreathRecord.getCommissionOrder().getMourningId()).getName();
		    	mourningTime = mourStart+"--"+mourEnd;
			}
		    //Timestamp mt = buyWreathRecord.getCommissionOrder().getMourningTime();
		    //
		    //Date mourningTime = DateTools.getDate(mt);
		    String writeUser ="";
		    if(buyWreathRecord.getWriteUser()!=null){
		    	writeUser = buyWreathRecord.getWriteUser().getName();		    	
		    }
		    String farewellPlace = null;
		    if (com != null && com.getFarewellId() != null) {
		    	farewellPlace = farewellService.getFarewellById(com.getFarewellId()).getName();
		    }
		    String assignUser = "";
		    String assignUserId = buyWreathRecord.getAssignUserId();
		    if (assignUserId != null) {
				String[] assignUserIds = assignUserId.split(",");
				for (int i = 0; i < assignUserIds.length; i++) {
					if (i > 0) {
						assignUser += ",";
					}
					User user = userService.getUserById(assignUserIds[i]);
					if (user != null) {
						assignUser += user.getName();
					}
				} 
			}
			Timestamp farewellBeginDate = null;
		    if (farewellRecordService.getFarewellRecordByOrderId(buyWreathRecord.getCommissionOrderId()) != null) {
		    	farewellBeginDate = farewellRecordService.getFarewellRecordByOrderId(buyWreathRecord.getCommissionOrderId()).getBeginDate();
		    }
		    String serialNumber = buyWreathRecord.getSerialNumber();
		    String orderNumber = buyWreathRecord.getOrderNumber();
		    model.put("deadName",deadName);
		    model.put("orderUser",orderUser);
		    model.put("dAge",dAge);
		    model.put("contact",contact);
		    model.put("dSex",dSex);
		    model.put("buyTime",buyTime);
		    model.put("mourningPlace",mourningPlace);
//		    CommissionOrder coo = buyWreathRecord.getCommissionOrder();
		    model.put("mourningTime",mourningTime);
		    //model.put("unitBuyFlag", buyWreathRecord.getUnitBuyFlagName());
		    model.put("writeUser",writeUser);
		    model.put("farewellPlace",farewellPlace);
		    model.put("assignUser",assignUser);
		    model.put("farewellTime",farewellBeginDate);
		    model.put("serialNumber",serialNumber);
		    model.put("orderNumber",orderNumber);
		    model.put("comment", buyWreathRecord.getComment());
		    model.put("creatUser", userService.getUserById(buyWreathRecord.getCreatUserId()).getName());
		    model.put("agentUser", buyWreathRecord.getAgentUser());
		    Map<String, Object> maps = new HashMap<String, Object>();
		    maps.put("buyWreathRecordId", id);
		    List<BuyWreathRecordD> list = buyWreathRecordDService.getBuyWreathRecordDList(maps);
		    model.put("list", list);
	    	return new ModelAndView("ye/first_department/flower/charFlower",model);
	    }
		/**
		 *花圈花篮订购修改
		 * @return
		 */
	@RequestMapping(params = "method=reedit")
	public ModelAndView reeditBuyWreathRecord() {
		Map<String, Object> model = new HashMap<String, Object>();
		String id = getString("id");
		BuyWreathRecord buyWreathRecord = buyWreathRecordService.getBuyWreathRecordId(id);
		String deadName = null;
		Integer dAge = null;
		String dSex = null;
		String mourningPlace = null;
		String farewellPlace = null; 
		Timestamp farewellBeginDate = null;
		Date mourStart = null;
		Date mourEnd = null;
		String mourningTime  = null;
		if (buyWreathRecord.getCommissionOrder() != null) {
			dAge = buyWreathRecord.getCommissionOrder().getAge();
			dSex = buyWreathRecord.getCommissionOrder().getSexName();
			String mourningid=buyWreathRecord.getCommissionOrder().getMourningId();
			if(mourningid !=null && "".equals(mourningid)){
				mourningPlace = mourningService.getMourningById(mourningid).getName();
				mourStart = DateTools.getDate(mourningRecordService.getMourningRecordByOId(buyWreathRecord.getCommissionOrderId()).getBeginTime());
				mourEnd = DateTools.getDate(mourningRecordService.getMourningRecordByOId(buyWreathRecord.getCommissionOrderId()).getEndTime());
				mourningTime = mourStart + "--" + mourEnd;
			}
			String farewellId=buyWreathRecord.getCommissionOrder().getFarewellId();
			if(farewellId!=null &&!"".equals(farewellId)){
				farewellPlace = farewellService.getFarewellById(farewellId).getName();
				farewellBeginDate = farewellRecordService.getFarewellRecordByOrderId(buyWreathRecord.getCommissionOrderId()).getBeginDate();
			}
		}
		deadName=buyWreathRecord.getdName();
		
		String orderUser = buyWreathRecord.getOrderUser();
		String contact = buyWreathRecord.getContact();
		Timestamp ts = buyWreathRecord.getCreatTime();
		// Timestamp mt =
		// buyWreathRecord.getCommissionOrder().getMourningTime();
		//
		// Date mourningTime = DateTools.getDate(mt);
		List<Object[]> list1 = new ArrayList<Object[]>();
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("groupType", (byte) 3);
		List<User> users = userService.getUserListByRoleName(Const.WRITER);
		for (int i = 0; i < users.size(); i++) {
			list1.add(new Object[] { users.get(i).getUserId(), users.get(i).getName() });
		}
		String writer = DataTools.getOptionByList(list1, buyWreathRecord.getWriteUserId(), false);
		String assignUser = "";
		String assignUserId = buyWreathRecord.getAssignUserId();
		model.put("assignUserId", assignUserId);
		if (assignUserId != null && !assignUserId.equals("")) {
			String[] assignUserIds = assignUserId.split(",");
			for (int i = 0; i < assignUserIds.length; i++) {
				if (i > 0) {
					assignUser += ",";
				}
				assignUser += userService.getUserById(assignUserIds[i]).getName();
			}
		}
		String orderNumber = buyWreathRecord.getOrderNumber();
		model.put("deadName", deadName);
		model.put("orderUser", orderUser);
		model.put("dAge", dAge);
		model.put("contact", contact);
		model.put("dSex", dSex);
		model.put("buyTime", ts);
		model.put("mourningPlace", mourningPlace);
		// CommissionOrder coo = buyWreathRecord.getCommissionOrder();
		String serialNumber = buyWreathRecord.getSerialNumber();
		model.put("mourningTime", mourningTime);
		// model.put("unitBuyFlag", buyWreathRecord.getUnitBuyFlagName());
		model.put("writer", writer);
		model.put("farewellPlace", farewellPlace);
		model.put("assignUser", assignUser);
		model.put("farewellTime", farewellBeginDate);
		model.put("serialNumber", serialNumber);
		model.put("orderNumber", orderNumber);
		model.put("comment", buyWreathRecord.getComment());
		model.put("creatUser", userService.getUserById(buyWreathRecord.getCreatUserId()).getName());
		model.put("agentUser", buyWreathRecord.getAgentUser());
		Map<String, Object> maps1 = new HashMap<String, Object>();
		maps1.put("buyWreathRecordId", id);
		List<BuyWreathRecordD> list = buyWreathRecordDService.getBuyWreathRecordDList(maps1);
		List<Object[]> list0 = new ArrayList<Object[]>();
		double total = 0;
		for (int i = 0; i < list.size(); i++) {
			total = total + list.get(i).getTotal();
			Item item = itemService.getItemById(list.get(i).getItemId());
			maps.put("typeId", item.getTypeId());

			String itemOption = itemService.getItemOption(maps, list.get(i).getItemId(), false);
			list0.add(new Object[] { itemOption, item.getPice(), list.get(i).getNumber(), list.get(i).getTotal(),
					list.get(i).getSelfCall(), list.get(i).getDeadCall(), list.get(i).getComment() });

		}
		model.put("bwrId", id);
		model.put("total", total);
		model.put("list", list0);
		model.put("sum", list.size());
		model.put("method", "save");
		return new ModelAndView("ye/first_department/flower/reBuyFlower", model);
	}
	    /**
	     * 删除花圈花篮订购
	     * @return
	     */
	    @RequestMapping(params = "method=delete")
	    @SystemControllerLog(description = "删除花圈花篮订购信息")
	    @ResponseBody
	    public JSONObject deleteBuyWreathRecord(){
			JSONObject json = new JSONObject();
	    	try {
		    	String id = getString("ids");
		    	buyWreathRecordService.deleteBuyWreathRecord(id);
		    	String []ids=id.split(",");
				for (int i = 0; i < ids.length; i++) {
					Map<String, Object> maps = new HashMap<String, Object>();
				    maps.put("buyWreathRecordId", ids[i]);
				    List<BuyWreathRecordD> list = buyWreathRecordDService.getBuyWreathRecordDList(maps);
				    for(int j=0;j<list.size();j++){
				    	buyWreathRecordDService.deleteBuyWreathRecordD(list.get(j).getId());
				    }
				}
	    		setJsonBySuccess(json, "删除成功", true);
			} catch (Exception e) {
				e.printStackTrace();
				setJsonByFail(json, "删除失败，错误"+e.getMessage());
			}
			return json;
	    }
	    
	    /**
	     * 人员选择
	     * @return
	     */
	    @RequestMapping(params = "method=choose")
	    public ModelAndView chooseUser(){
	    	Map<String,Object> model=new HashMap<String,Object>();
	    	List<Object[]> list = new ArrayList<Object[]>();
	    	List<User> users = userService.getUserListByRoleName(Const.ARRANGER);
	    	for(int i=0;i<users.size();i++){
	    		list.add(new Object[]{users.get(i).getUserId(),users.get(i).getName()});
	    	}
	    	model.put("groupName", Const.ARRANGER);
	    	model.put("list",list);
	    	return new ModelAndView("../../common/worker",model);
	    }
	    
	    /**
	     * 获得tree结构
	     * @param list
	     * @return
	     */
	    @SuppressWarnings("rawtypes")
		public String getTree(List<Object[]> list){
	    	int sum=0;
	    	String content="";
	    	for (int i = 0; i < list.size(); i++) {
	    		if (i>0) {
	    			content+=",";
				}
	    		Object[] objects=list.get(i);
	    		ItemType itemType=(ItemType)objects[0];
		    	content+= "{";
		    	content+="text:'"+itemType.getName()+"'";
//		    	content+=",tags:['"+itemType.getId()+"']";
		    	content+=",selectable: false";
		    	List itemList=(List)objects[1];
		    	content+=",nodes: [";
		    	for (int j = 0; j < itemList.size(); j++) {
		    		if (j>0) {
		    			content+=",";
					}
		    		Item item=(Item)itemList.get(j);
		    		content+="{";
		    		content+="text:'"+item.getName()+"'";
		    		content+=",tags:['"+item.getId()+"_"+sum+"']";
		    		sum++;
		    		content+="}";
		    	}
		    	content+="]";
		    	content+="}";
	    	}
	    	return content;
	    }
}
