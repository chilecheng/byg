package com.hz.controller.ye.firstDepart;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.ItemOrder;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;
import com.hz.util.ItemConst;
import com.hz.util.ItemTypeConst;

/**
 * 灵堂安排任务单
 * @author rgy
 *
 */
@Controller
@Scope("session")
@RequestMapping("/mourningWordOrder.do")
public class MourningWordOrderController extends BaseController{
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private MourningRecordService mourningRecordService;

	@Autowired
	private UserNoticeService userNoticeService;
	List<Object[]> list=new ArrayList<Object[]>();//定义一个总list
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "mourningWordOrder.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		return listFarewell();
		
	}
	/**
	 * 灵堂安排任务单列表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	public ModelAndView listFarewell(){
		Map<String, Object> model=new HashMap<String, Object>();
		model=getModel(model);
		list.removeAll(list);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String, Object> map=new HashMap<String, Object>();
		Date beginDate=getDate("beginDate");
		Date endDate=getDate("endDate");
		//设定当前日期
		if (beginDate == null || ("").equals(beginDate)) {
			beginDate = DateTools.getDayFirstTime(new Date());
		}else{
			beginDate = DateTools.getDayFirstTime(beginDate);
		}
		if (endDate == null || ("").equals(endDate)) {
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		}else{
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Mour});
		if(searchVal !=null && !"".equals(searchVal)){
			map.put("mourningName", searchVal);
		}
		map.put("mourningbeginDate", beginDate);
		map.put("mourningEndDate", endDate);
		map.put("checkFlag", Const.Check_Yes);
		PageHelper.startPage(pages[0], pages[1]);
		PageHelper.orderBy("creat_time");
		List<CommissionOrder> list1=commissionOrderService.getCommissionOrderList(map);
		
		PageInfo<CommissionOrder> page1 = new PageInfo<CommissionOrder>(list1);
		for (int i = 0; i < list1.size(); i++) {
			//得到commission的集合
			Object[] o=new Object[16];
			List<ItemOrder> listOrder=list1.get(i).getListItemOrder();
			/**
			 * 火化委托单
			 */
			String id=list1.get(i).getId();
			String mourningName=null;//灵堂
			if(list1.get(i).getMourning()!=null&&!list1.get(i).getMourning().equals("")){
				  mourningName=list1.get(i).getMourning().getName();//灵堂
				}
			String farewellName=null;//告别厅
			if(list1.get(i).getFarewell()!=null&&!list1.get(i).getFarewell().equals("")){
				farewellName=list1.get(i).getFarewell().getName();//告别厅号
				}
			Date mourningTime=list1.get(i).getMourningTime();//设灵时间
			Date mourningEndTime=list1.get(i).getMourningEndTime();//设灵结束时间
			//告别时间    （如果有告别时间  取告别时间  如果告别时间为空区预约火化时间   两者都没有用 的话   设置值为空）
			Date gbTime = null;
			Timestamp farewellTime = list1.get(i).getFarewellTime();
			Timestamp cremationTime = list1.get(i).getCremationTime();
			if(farewellTime != null){
				gbTime = farewellTime;
			}
			if( farewellTime == null &&  cremationTime != null ){
				gbTime = cremationTime;
			}
			
			String Deadname=list1.get(i).getName();//死者姓名
			String sex=list1.get(i).getSexName();//性别
			int age=list1.get(i).getAge();//年龄
			byte dealIsdel0 = mourningRecordService.getMourningRecordByOId(list1.get(i).getId()).getIsdel();
			//= list1.get(i).getDealIsdel();
			String dealIsdel1 = "";
			if (dealIsdel0 == (byte) 1) {
				dealIsdel1 = "1";
			}
			if (dealIsdel0 == (byte) 2) {
				dealIsdel1 = "2";
			}
//			if (dealIsdel0 == (byte) 3) {
//				dealIsdel1 = "3";
//			}
			String dealIsdel = commissionOrderService.getDealIselOptionTwo(dealIsdel1, false);// 处理状态
			/**
			 * 服务项目
			 */
			int duilianCount=0;
			int weihuaCount=0;
			int heishaCount=0;
			int huamenCount=0;
			int beijingCount=0;
			int baishaCount=0;//新加白纱
			StringBuilder DuiLian=new StringBuilder();//这些为小分类的总数，暂时没用到，到时候需要统计每个小类时用到
			String duiLian="";//对联
			StringBuilder WeiHua=new StringBuilder();
			String weiHua="";//围花
			StringBuilder HeiSha=new StringBuilder();
			String heiSha="";//黑纱
			StringBuilder HuaMen=new StringBuilder();
			String huaMen="";//花门
			StringBuilder BeiJing=new StringBuilder();
			String beiJing="";//灵堂背景    新加的
			StringBuilder BaiSha=new StringBuilder();
			String baiSha="";//新加 白纱
			int count=0;
			int sort=-1;
//			String itemName="";
			String helpCode = null;
			//遍历itemOrder
			for (int j = 0; j < listOrder.size(); j++) {
				ItemOrder it=listOrder.get(j);
				if(it.getItem()!=null){
					count=listOrder.get(j).getNumber();
					helpCode = listOrder.get(j).getItem().getHelpCode();
					sort=listOrder.get(j).getItem().getSort();
//					itemName=it.getItem().getName();
					if(ItemTypeConst.LITANGWL.equals(helpCode)){
						duilianCount += count;
						duiLian=listOrder.get(j).getComment();
						if (duiLian !=null && !duiLian.equals("")) {
							DuiLian.append(duiLian).append(",");
						}
					}if(sort == ItemConst.LingTangWeiHua_Sort){
						weihuaCount+=count;
						weiHua=listOrder.get(j).getItem().getPice()+"*"+count;
						WeiHua.append(weiHua).append(",");
					}if(sort == ItemConst.LingTangHeiSha_Sort){
						heiSha=listOrder.get(j).getItem().getName()+"*"+count;
						HeiSha.append(heiSha).append(",");
					}if(sort == ItemConst.LingTangHuaMen_Sort){
						huamenCount+=count;
						huaMen=listOrder.get(j).getItem().getPice()+"*"+count;
						HuaMen.append(huaMen).append(",");
					}if(sort == ItemConst.LingTangBeiJing){
						beijingCount+=count;
						beiJing=listOrder.get(j).getItem().getPice()+"*"+count;
						BeiJing.append(beiJing).append(",");
					}if(sort == ItemConst.LingTangBaiSha){
						baishaCount+=count;
						baiSha=listOrder.get(j).getItem().getName()+"*"+count;
						BaiSha.append(baiSha).append(",");
					}
				}
				
			}
			o[0] = id;
			o[1] = mourningName;
			o[2] = mourningTime;
			o[3] = mourningEndTime;
			o[4] = gbTime; 
			o[5] = Deadname;
			o[6] = sex;
			o[7] = age;
			o[8] = DuiLian;
			o[9] = WeiHua;
			o[10] = HeiSha;
			o[11] = BaiSha;
			o[12] = HuaMen;
			o[13] = BeiJing;
			o[14] = farewellName;
			o[15] = dealIsdel;
			list.add(o);
		}
		PageInfo<Object[]> page = new PageInfo<Object[]>(list);
		page.setPageNum(page1.getPageNum());
		page.setSize(page1.getSize());
		page.setPageSize(page1.getPageSize());
		page.setStartRow(page1.getStartRow());
		page.setEndRow(page1.getEndRow());
		page.setTotal(page1.getTotal());
		page.setPages(page1.getPages());
		userNoticeService.updateUserNoticeByUser(getCurrentUser().getUserId(), Const.NoticeType_MourningTask, DateTools.getThisDate());
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		model.put("Isdel_Yes", Const.Yes_treated);
		model.put("Isdel_No", Const.No_treated);
		model.put("searchOption", searchOption);
		model.put("searchVal", searchVal);
		model.put("page",page);
		model.put("method","list");
		return new ModelAndView("ye/mourningWordOrder/listMourningWordOrder",model);
	}
	
	/**
	  * 改变处理状态
	  */
	 @RequestMapping(params = "method=isdel")
	 @ResponseBody
		public JSONObject updateDealIsdel() {
			JSONObject json = new JSONObject();
			try {
				String id = getString("id");
				byte isdel = getByte("isdel");
				mourningRecordService.updateMourningRecordIsDeal(id, isdel);
				setJsonBySuccess(json, "操作成功", true);
			} catch (Exception e) {
				e.printStackTrace();
				setJsonByFail(json, "操作失败,错误" + e.getMessage());
			}
			return json;
		}
	
	/**
	 * 修改灵堂安排任务单状态
	 * 
	 * @return
	 */
	/*@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "灵堂安排任务单修改状态")
	@ResponseBody
	public JSONObject updateDealIsdel() {
		JSONObject json = new JSONObject();
		try {
			String id = getString("id");
			byte isdel = getByte("isdel");
			commissionOrderService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误" + e.getMessage());
		}
		return json;
	}*/
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "灵堂安排任务单统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "灵堂安排任务单统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "灵堂安排任务单统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "灵堂安排任务单统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
