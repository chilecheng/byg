package com.hz.controller.ye.firstDepart;

import java.util.ArrayList;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Appellation;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.system.User;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.ListFuneralOrderRecord;
import com.hz.entity.ye.ListOrderRecordDetail;
import com.hz.service.base.AppellationService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FuneralBuyService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 丧葬用品购买
 * @author lipengpeng
 */
@Controller
@RequestMapping("/funeralBuy.do")
public class FuneralBuyController extends BaseController {

	@Autowired
	private FuneralBuyService funeralbuyservice;

	@Autowired
	private CommissionOrderService commissionOrderService;

	@Autowired
	private AppellationService appellationService;

	@Autowired
	private ItemTypeService itemTypeService;

	@Autowired
	private ItemService itemService;

	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "funeralBuy.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listAllOrderCount();
		// return insertOrderCount();
	}

	@RequestMapping(params = "method=list")
	public ModelAndView listAllOrderCount() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		Map<String, Object> maps = new HashMap<String, Object>();
		String checkType=getString("checkType");
		if(checkType!=null && "payNo".equals(checkType)){
			maps.put("payFlag", Const.Pay_No);
		}
		if(checkType!=null && "payYes".equals(checkType)){
			maps.put("payFlag", Const.Pay_Yes);
		}
		String searchType=getString("select_kind");
//		maps.put("payFlag", Const.Pay_No);
		maps.put("select_kind", searchType);
		maps.put("search_val", getString("search_val"));
		maps.put("startTime", getString("startTime"));
		maps.put("endTime", getString("endTime")+"23:59:59");
//		maps.put("checkFlag", Const.Check_Yes);
		PageInfo<ListFuneralOrderRecord> page = funeralbuyservice.getAllRecordPageInfo(maps, pages[0], pages[1], "buy_time desc");
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_BuyNumber,Const.Search_Type_BuyName,Const.Search_Type_Card,Const.Search_Type_Name});
		model.put("checkType", checkType);
		model.put("page", page);
		model.put("searchOption", searchOption);
		return new ModelAndView("ye/first_department/funeral/buFuneral", model);
	}

	@RequestMapping(params = "method=showOrderDialog")
	public ModelAndView showOrderDialog() {
		String id = getString("id");
		Map<String, Object> model = new HashMap<String, Object>();
		User u = getCurrentUser();
		model.put("u", u);
		List<ListFuneralOrderRecord> lr = funeralbuyservice.findOrderRecordById(id);
		if (lr != null && lr.size() != 0) {
			if (lr.get(0) != null) {
				model.put("lr", lr.get(0));
			}
		}
		if (lr != null && lr.size() != 0 && lr.get(0).getCommissionorderid() != null && lr.get(0).getCommissionorderid() != "") {
			model.put("dSex",commissionOrderService.getCommissionOrderById(lr.get(0).getCommissionorderid()).getSexName());
		}
		model.put("buygoodsid", id);// !!!
		if (lr != null && lr.size() != 0 && lr.get(0).getRelationshipid() != null && lr.get(0).getRelationshipid() != "") {
			String fOption = appellationService.getAppellationOption(lr.get(0).getRelationshipid(), false);// 朋友？
			model.put("fOption", fOption);
		}
		if (lr != null && lr.size() != 0) {
			model.put("buyTime", lr.get(0).getBuyTime());
			model.put("takeawayTime", lr.get(0).getTakeawayTime());
		}
		List<ListOrderRecordDetail> ld = funeralbuyservice.findOrderDetailById(id);// 查找订单明细
		model.put("ld", ld);
		return new ModelAndView("ye/first_department/funeral/charFuneral", model);
	}

	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存丧葬用品购买信息")
	@ResponseBody
	public JSONObject insertOrderCount() {
		JSONObject json = new JSONObject();
		Map<String, Object> model = new HashMap<String, Object>();
		ListFuneralOrderRecord listfuneralorderrecord = new ListFuneralOrderRecord();
		try {
			String[] tbody = getArrayString("table");
			if (tbody == null) {
				json.put("statusCode", 302);
				json.put("message", "请选择购买丧葬用品");
				return json;
			}
			String name =getString("dName");
			String cid=getString("dNameId");//两者都是委托单的id
			String ordernum=getString("ordernum");//购买单号
			String bname=getString("bname");//代办人
			String remarktext=getString("remarktext");//备注
			Timestamp buyTime=getTimeM("buyTime");//!!!
			Date recTime=getDate("recTime");//领用时间
			String buyer=getString("buyer");
			String relactive=getString("relactive");
			String s=UuidUtil.get32UUID();
			listfuneralorderrecord.setId(s);
			listfuneralorderrecord.setdName(name);
			listfuneralorderrecord.setCommissionorderid(cid);
			listfuneralorderrecord.setBuyTime(DateTools.dateToTimestamp(buyTime));//!!!
			listfuneralorderrecord.setTakeName(buyer);
			listfuneralorderrecord.setRemarktext(remarktext);
			listfuneralorderrecord.setTakeawayTime(DateTools.dateToTimestamp(recTime));//!!!
			listfuneralorderrecord.setReplaceName(bname);
			listfuneralorderrecord.setBuyOrderId(ordernum);
//			Appellation app=appellationService.getAppellationById(relactive);//关系值：如朋友
//			listfuneralorderrecord.setRelationship(app.getName());
			listfuneralorderrecord.setRelationshipid(relactive);// 关系id
			User user = getCurrentUser();
			model.put("user", user);
			listfuneralorderrecord.setCreateUserId(user.getUserId());
			listfuneralorderrecord.setPayFlag(Const.Pay_No);
			listfuneralorderrecord.setDealIsdel(Const.No_treated);
			funeralbuyservice.insertOrderCount(listfuneralorderrecord,tbody,s);
			setJsonBySuccess(json, "保存成功", "funeralBuy.do");
		} catch (Exception e) {
			e.printStackTrace();//
			setJsonByFail(json, "保存失败，错误" + e.getMessage());
		}
		return json;
	}

	@RequestMapping(params = "method=update")
	@SystemControllerLog(description = "修改丧葬用品购买信息")
	@ResponseBody
	public JSONObject updateOrderCount() {

		JSONObject json = new JSONObject();
		try {
			String buygoodsid = getString("buygoodsid");
			String ordernum = getString("ordernum");
			String bname = getString("bname");
			String remarktext = getString("remarktext");
			String[] tbody = getArrayString("table");
			Date buyTime = getDate("buyTime");
			// String buyTime=getString("buyTime");//!!!!!!这里也可以用
			Date recTime = getDate("recTime");
			// String recTime=getString("recTime");////!!!
			String buyer = getString("buyer");
			String relactive = getString("relactive");
			ListFuneralOrderRecord listfuneralorderrecord = funeralbuyservice.findOrderRecordById(buygoodsid).get(0);
			listfuneralorderrecord.setBuyTime(DateTools.dateToTimestamp(buyTime));// !!!new
			listfuneralorderrecord.setTakeName(buyer);
			listfuneralorderrecord.setRemarktext(remarktext);
			listfuneralorderrecord.setTakeawayTime(DateTools.dateToTimestamp(recTime));// !!!
			listfuneralorderrecord.setReplaceName(bname);
			listfuneralorderrecord.setBuyOrderId(ordernum);
			listfuneralorderrecord.setRelationshipid(relactive);
			Appellation app = appellationService.getAppellationById(relactive);// 关系值：如朋友
			listfuneralorderrecord.setRelationship(app.getName());
			listfuneralorderrecord.setPayFlag(Const.Pay_No);
			listfuneralorderrecord.setDealIsdel(Const.No_treated);
			funeralbuyservice.updateOrderCount(listfuneralorderrecord,buygoodsid,tbody,ordernum);
			setJsonBySuccess(json, "修改成功", "funeralBuy.do");
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "修改失败" + e.getMessage());
		}
		return json;
	}

	@RequestMapping(params = "method=buy")
	public ModelAndView buy() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		User u = getCurrentUser();
		// 与死者关系
		String fAppellationOption = appellationService.getAppellationOption(null, false);
		model.put("u", u);
		model.put("method", "save");
		model.put("recTime", DateTools.getThisDate());
		model.put("fAppellationOption", fAppellationOption);
		return new ModelAndView("ye/first_department/funeral/toBuyFuneral", model);
	}

	@RequestMapping(params = "method=edit")
	public ModelAndView modify() {
		String id = getString("id");// y_buy_goods_record的id
		Map<String, Object> model = new HashMap<String, Object>();
		User u = getCurrentUser();
		model.put("u", u);
		List<ListFuneralOrderRecord> listrec = funeralbuyservice.findOrderRecordById(id);
		if (listrec != null && listrec.size() != 0) {
			if (listrec.get(0) != null) {
				model.put("listrec", listrec.get(0));
			}

		}
		if (listrec != null && listrec.size() != 0 && listrec.get(0).getCommissionorderid() != null
				&& listrec.get(0).getCommissionorderid() != "") {
			// 得到性别的汉字
			model.put("dSex",
					commissionOrderService.getCommissionOrderById(listrec.get(0).getCommissionorderid()).getSexName());
		}
		model.put("method", "update");
		model.put("buygoodsid", id);// !!!
		String fOption = appellationService.getAppellationOption(listrec.get(0).getRelationshipid(), false);// 朋友？
		model.put("fOption", fOption);
		model.put("takeawayTime", listrec.get(0).getTakeawayTime());
		model.put("buyTime", listrec.get(0).getBuyTime());
		List<ListOrderRecordDetail> listdetail = funeralbuyservice.findOrderDetailById(id);// 查找订单明细
		model.put("listdetail", listdetail);
		List<String> lid = new ArrayList<String>();
		for (int k = 0; k < listdetail.size(); k++) {
			lid.add(listdetail.get(k).getItemId());//
		}
		List<String> lsOption = funeralbuyservice.getSadGoodsOption(lid, false);// ******
		model.put("lsOption", lsOption);// !!!^^^
		return new ModelAndView("ye/first_department/funeral/reBuyFuneral", model);
	}

	@RequestMapping(params = "method=delete")
	@SystemControllerLog(description = "删除丧葬用品购买信息")
	@ResponseBody
	public JSONObject delete() {
		JSONObject json = new JSONObject();
		String id[] = getString("id").split(",");// y_buy_goods_record的id
		try {
			for (int i = 0; i < id.length; i++) {
				funeralbuyservice.deleteFirstStepById(id[i]);// 删除这个订单的所有订单明细
				funeralbuyservice.deleteOrderRecordById(id[i]);
			}
			setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败" + e.getMessage());
		}
		return json;
	}
	
	@RequestMapping(params = "method=addItems")
	@ResponseBody
	public List<Object[]> addItems() {
		List<Object[]> list = new ArrayList<Object[]>();
		String id = getString("ids");
		String[] ids = id.split(",");
		Map<String ,Object> maps = new HashMap<String, Object>();
		for (int i = 0; i < ids.length; i++) {
			if (!ids[i].equals("")) {
				Item item = itemService.getItemById(ids[i]);
				maps.put("typeId", item.getTypeId());
				maps.put("isdel", Const.Isdel_No);
				String itemOption = itemService.getItemOption(maps, ids[i], false);
				list.add(new Object[] { itemOption, item.getPice(), 1, item.getPice() });
			}

		}
		return list;
	}

	@RequestMapping(params = "method=selectItems")
	public ModelAndView editItemOrders() {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Object[]> itemList = new ArrayList<Object[]>();
		Map<String ,Object> maps = new HashMap<String, Object>();
		maps.put("isdel", Const.Isdel_No);
		// maps.put("name",new String());
		List<ItemType> itemTypeList1 = itemTypeService.getMoreItemTypeList(maps);
		for (int i = 0; i < itemTypeList1.size(); i++) {
			ItemType itemType = (ItemType) itemTypeList1.get(i);
			Map<String, Object> itemMaps = new HashMap<String, Object>();
			itemMaps.put("typeId", itemType.getId());
			itemMaps.put("isdel", Const.Isdel_No);
			List<Item> itemList1 = itemService.getItemList(itemMaps);
			itemList.add(new Object[] { itemType, itemList1 });
		}
		model.put("itemTree", getTree(itemList));
		return new ModelAndView("ye/first_department/funeral/selectItems_h", model);
	}

	/**
	 * 获得tree结构
	 * 
	 * @param list
	 * @return
	 */
	public String getTree(List<Object[]> list) {
		int sum = 0;
//		String content = "";
		StringBuilder content = new StringBuilder("");
		for (int i = 0; i < list.size(); i++) {
			if (i > 0) {
//				content += ",";
				content.append(",");
			}
			Object[] objects =  list.get(i);
			ItemType itemType = (ItemType) objects[0];
//			content += "{";
			content.append("{");
//			content += "text:'" + itemType.getName() + "'";
			content.append("text:'").append(itemType.getName()).append("'");
			// content+=",tags:['"+itemType.getId()+"']";
			content.append(",tags:['").append(itemType.getId()).append("']");
//			content += ",selectable: false";
			content.append(",selectable: false");
			List<?> itemList = (List<?>) objects[1];
//			content += ",nodes: [";
			content.append(",nodes: [");
			for (int j = 0; j < itemList.size(); j++) {
				if (j > 0) {
//					content += ",";
					content.append(",");
				}
				Item item = (Item) itemList.get(j);
//				content += "{";
				content.append("{");
//				content += "text:'" + item.getName() + "'";
				content.append("text:'").append(item.getName()).append("'");
//				content += ",tags:['" + item.getId() + "_" + sum + "']";
				content.append(",tags:['").append(item.getId()).append("_").append(sum).append("']");
				sum++;
//				content += "}";
				content.append("}");
			}
//			content += "]";
			content.append("]");
//			content += "}";
			content.append("}");
		}
		return content.toString();
	}

	/**
	 * 死者姓名寻找
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=dNameSearch")
	@ResponseBody
	public JSONObject dNameSearch() {
		JSONObject json = new JSONObject();
		try {
			String dName = getString("dName");
			Map<String, Object> maps = new HashMap<String, Object>();
			maps.put("name", dName);
			maps.put("checkFlag", Const.Check_Yes);
			maps.put("goto", Const.GOTO3);//用于区分查询火化委托单的接口(只查所需数据，提高性能)
			List<CommissionOrder> list = commissionOrderService.getCommissionOrderList(maps);
			if (list != null && list.size() == 1) {
				CommissionOrder co = list.get(0);
				String dSex = co.getSexName();
				int dAge = co.getAge();
				Timestamp hallTime = co.getMourningTime();
				Timestamp fareTime = co.getFarewellTime();
				json.put("dNameId", co.getId());
				json.put("dName", co.getName());
				json.put("dSex", dSex);
				json.put("dAge", dAge);
				if (hallTime != null) {
					String hallTime0=DateTools.getTimeFormatString("yyyy-MM-dd hh:mm", hallTime);
					json.put("hallTime", hallTime0);
					json.put("hallName", co.getMourning().getName());
				}
				if (fareTime != null) {
					String fareTime0=DateTools.getTimeFormatString("yyyy-MM-dd hh:mm", fareTime);
					json.put("fareTime", fareTime0);
					json.put("fareName", co.getFarewell().getName());
				}
//				List<ListFuneralOrderRecord> listfm = funeralbuyservice.findFareWellAndMourningById(co.getId());
			}
			json.put("statusCode", 200);
			json.put("hideName", dName);
			json.put("size", list.size());
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "系统出错");
		}

		return json;
	}

	/**
	 * from hw
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=dNameList")
	public ModelAndView dNameListbuyWreathRecord() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
//		int[] pages = getPage();
		String dName = getString("dName");
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("name", dName);
		maps.put("checkFlag", Const.Check_Yes);
		maps.put("goto", Const.GOTO3);//用于区分查询火化委托单的接口(只查所需数据，提高性能) 丧葬用品购买页面 点姓名搜索的弹出框
//		PageHelper.startPage(pages[0], pages[1]);
//		PageHelper.orderBy("creat_time");
		List<CommissionOrder> list = commissionOrderService.getCommissionOrderList(maps);
//		PageInfo<CommissionOrder> page1 = new PageInfo<CommissionOrder>(list);
		List<Object[]> list0 = new ArrayList<Object[]>();
		for (int i = 0; i < list.size(); i++) {
			Timestamp hallTime = list.get(i).getMourningTime();
			Timestamp fareTime = list.get(i).getFarewellTime();
			String hallName = null;
			String fareName = null;
			if (hallTime != null) {
				hallName = list.get(i).getMourning().getName();
			}
			if (fareTime != null) {
				fareName = list.get(i).getFarewell().getName();
			}
			list0.add(new Object[] { list.get(i).getId(), list.get(i).getName(), list.get(i).getArriveTime(),
				list.get(i).getCertificateCode(), list.get(i).getdAddr(), list.get(i).getAge(),
				list.get(i).getSex(), hallName, hallTime, fareName, fareTime});
		}
//		PageInfo<Object[]> page = new PageInfo<Object[]>(list0);
//		page.setPageNum(page1.getPageNum());
//		page.setSize(page1.getSize());
//		page.setPageSize(page1.getPageSize());
//		page.setStartRow(page1.getStartRow());
//		page.setEndRow(page1.getEndRow());
//		page.setTotal(page1.getTotal());
//		page.setPages(page1.getPages());
		model.put("page", list0);
		return new ModelAndView("../../js/selectOne", model);
	}
	/*
	 * @RequestMapping(params = "method=cardNumChange")
	 * 
	 * @ResponseBody public JSONObject cardNumChange() { JSONObject json = new
	 * JSONObject(); String id = getString("dNameId"); CommissionOrder co =
	 * commissionOrderService.getCommissionOrderById(id); String dSex =
	 * co.getSexName(); String deadName=co.getName(); int dAge = co.getAge();
	 * //String hallName=co.getMourningId(); Timestamp
	 * hallTime=co.getMourningTime(); //String fareName=co.getFarewellId();
	 * Timestamp fareTime=co.getFarewellTime(); json.put("dSex", dSex);
	 * json.put("dAge", dAge); json.put("deadName", deadName);
	 * //json.put("hallName", hallName); json.put("hallTime", new
	 * Date(hallTime.getTime())); //json.put("fareName", fareName);
	 * json.put("fareTime",new Date(fareTime.getTime()));//!!!
	 * 
	 * List<ListFuneralOrderRecord>
	 * listfm=funeralbuyservice.findFareWellAndMourningById(id);
	 * if(listfm.get(0)!=null) { json.put("hallName",
	 * listfm.get(0).getMourningName()); json.put("fareName",
	 * listfm.get(0).getFarewellName()); }
	 * 
	 * return json; //return new
	 * ModelAndView("ye/first_department/funeral/reBuyFuneral",null); }
	 */

	/*
	 * @RequestMapping(params = "method=deadNameChange")
	 * 
	 * @ResponseBody public JSONObject deadNameChange() {
	 * 
	 * JSONObject json = new JSONObject(); String id = getString("dNameId");
	 * CommissionOrder co = commissionOrderService.getCommissionOrderById(id);
	 * String dSex = co.getSexName(); String cardNum=co.getCardCode(); int dAge
	 * = co.getAge(); Timestamp hallTime=co.getMourningTime(); Timestamp
	 * fareTime=co.getFarewellTime(); json.put("dSex", dSex); json.put("dAge",
	 * dAge); json.put("cardNum", cardNum); json.put("hallTime", new
	 * Date(hallTime.getTime())); json.put("fareTime", new
	 * Date(fareTime.getTime()));//DateTools.getTimeFormatString("yyyy-mm-dd",
	 * fareTime) List<ListFuneralOrderRecord>
	 * listfm=funeralbuyservice.findFareWellAndMourningById(id);
	 * if(listfm.get(0)!=null) { json.put("hallName",
	 * listfm.get(0).getMourningName()); json.put("fareName",
	 * listfm.get(0).getFarewellName()); }
	 * 
	 * return json; }
	 */

}
