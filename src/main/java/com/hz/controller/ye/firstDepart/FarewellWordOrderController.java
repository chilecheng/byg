package com.hz.controller.ye.firstDepart;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.FarewellTaskList;
import com.hz.entity.ye.ItemOrder;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.FarewellTaskService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;
import com.hz.util.ItemConst;
import com.hz.util.ItemTypeConst;

/**
 * 告别厅安排任务单
 * @author rgy
 *
 */
@Controller
@Scope("session")
@RequestMapping("/farewellWordOrder.do")
public class FarewellWordOrderController extends BaseController{
	@Autowired
	private FarewellTaskService farewellTaskService;
	@Autowired
	private FarewellRecordService farewellRecordService;
	@Autowired
	private UserNoticeService userNoticeService;
	List<Object[]> list=new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "farewellWordOrder.do");
	}
	@RequestMapping
	public ModelAndView unspecified(){
		userNoticeService.updateUserNoticeByUser(getCurrentUser().getUserId(), Const.NoticeType_FarewellTask, DateTools.getThisDate());
		return listFarewell();
		
	}
	/**
	 * 告别厅安排任务单列表
	 * @return
	 */
	@RequestMapping(params ="method=list")
	public ModelAndView listFarewell(){
		Map<String, Object> model=new HashMap<String, Object>();
		model=getModel(model);
		list.removeAll(list);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("name",getString("name"));
		//获取时间
		Date beginDate = getDate("beginDate");
		Date endDate = getDate("endDate");
		if (beginDate == null || ("").equals(beginDate)) {
			beginDate = DateTools.getDayFirstTime(new Date());
		}else{
			beginDate = DateTools.getDayFirstTime(beginDate);
		}
		if (endDate == null || ("").equals(endDate)) {
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		}else{
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_FareWell});
		if(searchVal !=null && !"".equals(searchVal)){
			map.put("fareWellName", searchVal);
		}
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		map.put("itemType", ItemTypeConst.GBTSF);
		map.put("checkFlag", Const.Check_Yes);
		PageHelper.startPage(pages[0], pages[1]);
		PageHelper.orderBy("bf.name,co.farewell_time");
		List<FarewellTaskList> list1 = farewellTaskService.getFarewellTaskList(map);
		PageInfo<FarewellTaskList> page1 = new PageInfo<FarewellTaskList>(list1);
		for (int i = 0; i < list1.size(); i++) {
			Object[] o=new Object[20];
			List<ItemOrder> listOrder=list1.get(i).getListItemOrder();
			//火化委托单的数据
			String id=list1.get(i).getId();
			String moruningName=list1.get(i).getMname();
			String farewellName=list1.get(i).getFname();
			Timestamp farewellTimes=list1.get(i).getBeginDate();//时间
			String name=list1.get(i).getCname();//名字
			String sexName=list1.get(i).getSexName();//性别
			int age=list1.get(i).getAge();//年龄
			byte dealIsdel0 = farewellRecordService.getFarewellRecordId(list1.get(i).getId()).getIsdel();
			//list1.get(i).getDealIsdel();
			String dealIsdel1 = "";
			if (dealIsdel0 == (byte) 1) {
				dealIsdel1 = "1";
			}
			if (dealIsdel0 == (byte) 2) {
				dealIsdel1 = "2";
			}
//			if (dealIsdel0 == (byte) 3) {
//				dealIsdel1 = "3";
//			}
			String dealIsdel = farewellTaskService.getDealIselOptionTwo(dealIsdel1, false);// 处理状态
			//服务项目的数据
			int weihuaCont=0;
			int heishaCount=0;
			int baishaCount=0;
			int xiaoqiuCount=0;
			int huamenCount=0;//这些为小分类的总数，暂时没用到，到时候需要统计每个小类时用到
			StringBuilder duiLian = new StringBuilder("");
			String duilian="";//对联
			String weihua="";//围花
			StringBuilder weiHua = new StringBuilder("");
			String heisha="";//黑纱
			String baisha="";//白纱
			StringBuilder xiaoQiu=new StringBuilder();
			String xiaoqiu="";//孝球
			StringBuilder huaMens=new StringBuilder();
			String huamen="";//花门
			String liyi="";//礼仪
			String siyi="";//司仪
			String ditan="";//地毯
			String beiJing="";//绢花台  改为告别厅背景
			StringBuilder beiJings=new StringBuilder();
			String dianziyixiang="";//电子遗像
			String shexiang="";//摄像
			byte sort=-1;
			int count=0;
			String helpCode = null;
			
			//因单价在项目中可变，所以将  listOrder.get(j).getItem().getPice()
			//改为 listOrder.get(j).getPice() ，取最终的单价
			for (int j = 0; j < listOrder.size(); j++) {
				ItemOrder it=listOrder.get(j);
				if(it.getItem() !=null){
					sort=it.getItem().getSort();
					helpCode = it.getItem().getHelpCode();
					sort= it.getItem().getSort();
					count=listOrder.get(j).getNumber();
					if(ItemTypeConst.LITINGWL.equals(helpCode)){
						duilian=listOrder.get(j).getComment();
						if (duilian != null) {
							duiLian.append(duilian).append(",");
						}
					}if(sort== ItemConst.GaoBieTingWeiHua_Sort){
						weihuaCont+=count;
						
//						weihua=listOrder.get(j).getItem().getPice()+"*"+count;
						weihua=listOrder.get(j).getPice()+"*"+count;
						weiHua.append(weihua).append(",");
					}if(ItemTypeConst.GBTHSBZ.equals(helpCode)){
						heisha="黑纱";
					}if(ItemTypeConst.GBTBSBZ.equals(helpCode)){
						baisha="白纱";
					}if(sort == ItemConst.GaoBieTingXiaoQiu_Sort){
						xiaoqiuCount+=count;
						if (ItemTypeConst.GBTHQBZ.equals(helpCode)) {
							xiaoqiu="黑球"+"*"+count;
						} else if(ItemTypeConst.GBTBQBZ.equals(helpCode)) {
							xiaoqiu="白球"+"*"+count;
						}
						xiaoQiu.append(xiaoqiu).append(",");
					}if(sort == ItemConst.GaoBieTingHuaMen_Sort){
						huamenCount+=count;
//						huamen=listOrder.get(j).getItem().getPice()+"*"+count;
						huamen=listOrder.get(j).getPice()+"*"+count;
						huaMens.append(huamen).append(",");
					}if(ItemTypeConst.LYCB.equals(helpCode)){
						liyi=listOrder.get(j).getItem().getName();
					}if(ItemTypeConst.LTSY.equals(helpCode)){
						siyi=listOrder.get(j).getItem().getName();
					}if(ItemTypeConst.GBTDTBZ.equals(helpCode)){
						ditan=listOrder.get(j).getItem().getName();
					}if(sort== ItemConst.BeiJing_Sort){
//						beiJing=listOrder.get(j).getItem().getPice()+"*"+count;
						beiJing=listOrder.get(j).getPice()+"*"+count;
						beiJings.append(beiJing).append(",");
					}
//					if(ItemTypeConst.BJJHT.equals(helpCode)){
//						juanhuatai=listOrder.get(j).getItem().getName();
//					}
					if(ItemTypeConst.DZYX.equals(helpCode)){
						dianziyixiang=listOrder.get(j).getItem().getName();
					}if(ItemTypeConst.BZLYSX.equals(helpCode)){
						shexiang=listOrder.get(j).getItem().getName();
					}	
				}
				
			}
			o[0] = id;
			o[1] = farewellName;
			o[2] = farewellTimes;
			o[3] = name;
			o[4] = sexName;
			o[5] = age;
			
			o[6] = duiLian;
			o[7] = weiHua;
			o[8] = heisha;
			o[9] = baisha;
			
			o[10] = xiaoQiu;
			o[11] = huamen;
			o[12] = liyi;
			o[13] = siyi;
			
			o[14] = ditan;
			o[15] = beiJings;
			o[16] = dianziyixiang;
			o[17] = shexiang;
			o[18] = moruningName;
			o[19] = dealIsdel;
			list.add(o);
		}
		
		
		PageInfo<Object[]> page = new PageInfo<Object[]>(list);
		page.setPageNum(page1.getPageNum());
		page.setSize(page1.getSize());
		page.setPageSize(page1.getPageSize());
		page.setStartRow(page1.getStartRow());
		page.setEndRow(page1.getEndRow());
		page.setTotal(page1.getTotal());
		page.setPages(page1.getPages());
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		model.put("Isdel_Yes", Const.Yes_treated);
		model.put("Isdel_No", Const.No_treated);
		model.put("searchOption", searchOption);
		model.put("searchVal", searchVal);
		model.put("page",page);
		model.put("method","list");
		model.put("list",list);
		return new ModelAndView("ye/farewellWordOrder/listFarewellWordOrder",model);
	}
	
	 /**
	  * 改变处理状态
	  */
	 @RequestMapping(params = "method=isdel")
	 @ResponseBody
		public JSONObject updateDealIsdel() {
			JSONObject json = new JSONObject();
			try {
				String id = getString("id");
				byte isdel = getByte("isdel");
				farewellRecordService.updateFarewellRecordIsDeal(id, isdel);
				setJsonBySuccess(json, "操作成功", true);
			} catch (Exception e) {
				e.printStackTrace();
				setJsonByFail(json, "操作失败,错误" + e.getMessage());
			}
			return json;
		}
	
	/**
	 * 修改告别厅安排任务单状态
	 * 
	 * @return
	 *//*
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "告别厅安排任务单修改状态")
	@ResponseBody
	public JSONObject updateDealIsdel() {
		JSONObject json = new JSONObject();
		try {
			String id = getString("id");
			byte isdel = getByte("isdel");
			farewellTaskService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误" + e.getMessage());
		}
		return json;
	}*/
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "告别厅安排任务单统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream output = new FileOutputStream("E://staticalform/告别厅安排任务单统计.xls");  
            String  title = "告别厅安排任务单统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "告别厅安排任务单统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+ new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "告别厅安排任务单统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
