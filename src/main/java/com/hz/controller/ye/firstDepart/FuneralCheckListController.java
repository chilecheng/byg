package com.hz.controller.ye.firstDepart;


import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.BuyGoodD;
import com.hz.entity.ye.Buygood;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.DelegateRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.ItemChange;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.ListFuneralOrderRecord;
import com.hz.service.payDivision.DelegateService;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FuneralBuyService;
import com.hz.service.ye.ItemBeforeService;
import com.hz.service.ye.ItemChangeService;
import com.hz.service.ye.ItemOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;
import com.hz.util.ItemConst;

/**
 * 丧葬用品核对表
 * @author gpf
 */
@Controller
@Scope("session")
@RequestMapping("/taskFuneral.do")
public class FuneralCheckListController  extends BaseController  {
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private DelegateService delegateService;
	@Autowired
	private FuneralBuyService funeralbuyservice;
	@Autowired
	private UserNoticeService userNoticeService;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	private ItemBeforeService itemBeforeService;
	@Autowired
	private ItemChangeService itemChangeService;
	List<Object[]> list = new ArrayList<Object[]>();
	//用于保存非委托业务丧葬用品
	List<Object[]> list2 = new ArrayList<Object[]>();
	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "taskFuneral.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		if (getString("tongji")!=null && getString("tongji").equals("yes")) {
			Map<String, Object> model = new HashMap<String, Object>();
			model = getModel(model);
			return new ModelAndView("ye/taskFuneral/listTaskFuneral", model);
		} else {
			userNoticeService.updateUserNoticeByUser(getCurrentUser().getUserId(), Const.NoticeType_FuneralTask, DateTools.getThisDate());
			return listFarewell();
		}
	}

	/**
	 * 丧葬用品核对列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listFarewell() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		list.removeAll(list);
		list2.removeAll(list2);
		Map<String, Object> map = new HashMap<String, Object>();
		Date calculateTime = DateTools.getThisDateTimestamp();
		Date beginDate = getDate("beginDate");
		Date endDate = getDate("endDate");
		if (beginDate == null || ("").equals(beginDate)) {
			beginDate = DateTools.getDayFirstTime(new Date());
		}else{
			beginDate = DateTools.getDayFirstTime(beginDate);
		}
		if (endDate == null || ("").equals(endDate)) {
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(beginDate));
		}else{
			endDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(endDate));
		}
		String type = getString("checkType");
		//j今日领用
		if ("1".equals(type)) {
			Date jbeginDate = DateTools.getDayFirstTime(DateTools.getThisDate());
			Date jendDate = DateTools.getDayEndTime(DateTools.getThisDate());
			map.put("takeawayTimeBegin", jbeginDate);
			map.put("takeawayTimeEnd", jendDate);
			model.put("beginDate", jbeginDate);
			model.put("endDate", jendDate);
		}
		//明日领用
		if ("2".equals(type)) {
			Date tomorrow = DateTools.getDayAfterDay(DateTools.getThisDate(), 1, Calendar.DAY_OF_YEAR);
			Date mbeginDate = DateTools.getDayFirstTime(tomorrow);
			Date mendDate = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(tomorrow));
			map.put("takeawayTimeBegin", mbeginDate);
			map.put("takeawayTimeEnd", mendDate);
			model.put("beginDate", mbeginDate);
			model.put("endDate", mendDate);
		}
		if("3".equals(type)||"".equals(type)){
			map.put("cremationBeginDate", beginDate);
			map.put("cremationEndDate", endDate);
			model.put("beginDate", beginDate);
			model.put("endDate", endDate);
		}
		String name = getString("name");
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		model.put("checkType", type);
		map.put("name", name);
		map.put("searchType", searchType);
		map.put("searchVal", searchVal);
		//根据火化时间查询
		
		map.put("checkFlag", Const.Check_Yes);

		// x项目修改为 付费与否都在列表里面显示
		 // map.put("payFlag", Const.Pay_Yes);	
		List<DelegateRecord> listorder = delegateService.getdelegateListByPayTime(map);
	//	List<CommissionOrder> listorder2 = commissionOrderService.getCommissionOrderList(map);
		Map<String, String> namePrice = new HashMap<String, String>();
		//查询非委托业务项 丧葬用品
		
		List<Buygood> listbuyGood = funeralbuyservice.findItemOrder(map);
		Map<String, String> namePrice2 = new HashMap<String, String>();
		
		
		List<Map<Long,String>> changeFlagFuneral=new ArrayList<Map<Long,String>>();//记录类别，以便在前端作判断
		for (int i = 0; i < listbuyGood.size(); i++) {//非委托业务（丧葬用品购买）
			Object[] o = new Object[20];
			String 	takeawayTimeStr="";
			String deadName = null;
			Integer age = null;
			String sex = null;
			Date farewellTime = null;
			String comment = null;
			Buygood buygood = listbuyGood.get(i);
			String id=buygood.getId();
			String orderId= buygood.getCommissionOrderId();
			String farewellName = null;// 告别厅
			Timestamp takeawayTime = buygood.getTakeawayTime();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");//定义日期类型格式			
			if(takeawayTime != null){
				takeawayTimeStr = format.format(takeawayTime);
			}
			String commissionOrderId = buygood.getCommissionOrderId();
			String buyOrderId =buygood.getBuyOrderId();
			
			//以下开始判断是否有更改 丧葬用品记录
			Map<String,Object> maps=new HashMap<String,Object>();
			maps.put("buyOrderId", buyOrderId);
			List<ItemChange> itemChangeList=itemChangeService.getItemChangeList(maps);
			if(itemChangeList.size()>0){//有更改记录，即需要在页面中显示出来
				Map<Long,String> sortmap=new HashMap<>();
				for(int j=0;j<itemChangeList.size();j++){
					int x=Integer.parseInt(itemChangeList.get(j).getSort());
					if(x==ItemConst.BaoBuHe_Sort){
						sortmap.put(12L, "yes");
					}else if(x==ItemConst.XiaoHuaQuan_Sort){
						sortmap.put(10L, "yes");
					}else if(x==ItemConst.BaoHuJi_Sort){
						sortmap.put(11L, "yes");
					}else if(x==ItemConst.GuHuiHe_Sort){
						sortmap.put(9L, "yes");
					}else if(x==ItemConst.HongXiaoDai_Sort){
						sortmap.put(13L, "yes");
					}else if(x==ItemConst.ShouBei_Sort){
						sortmap.put(14L, "yes");
					}else if(x==ItemConst.ShouYi_Sort){
						sortmap.put(15L, "yes");
					}else if(x==ItemConst.YuSan_Sort){
						sortmap.put(16L, "yes");
					}else{
						sortmap.put(17L, "no");
					}
				}
				changeFlagFuneral.add(sortmap);
			}
			
			
			if (commissionOrderId != null && !commissionOrderId.equals("")) {
				CommissionOrder com = commissionOrderService.getCommissionOrderById(commissionOrderId);
				if(com != null && com.equals("")){
					deadName = com.getName();// 死者名字
					age = com.getAge();// 年龄
					sex = com.getSexName();// 性别
					if (com.getFarewell() != null && !com.getFarewell().equals("")) {
						farewellName = com.getFarewell().getName();
					}
					farewellTime = com.getFarewellTime();// 告别时间
				}
			}
			comment = buygood.getRemarks();// 备注
			String payName = buygood.getTakeName();//购买人姓名
			byte dealIsdel0 = buygood.getDealIsdel();
			String dealIsdel1 = "";
			if (dealIsdel0 == (byte) 1) {
				dealIsdel1 = "1";
			}
			if (dealIsdel0 == (byte) 2) {
				dealIsdel1 = "2";
			}
			if (dealIsdel0 == (byte) 3) {
				dealIsdel1 = "3";
			}
			String dealIsdel = commissionOrderService.getDealIselOption0(dealIsdel1, false);// 处理状态
			StringBuilder baoBuHe = new StringBuilder();
			String baobuhe = "";// 包布盒
			StringBuilder xiaoHuaQuan = new StringBuilder();
			String xiaohuaquan = "";// 小花圈
			StringBuilder baoHuJi = new StringBuilder();
			String baohuji = "";// 保护剂
			StringBuilder guHuiHe = new StringBuilder();
			String guhuihe = "";// 骨灰盒
			StringBuilder hongXiaoDai = new StringBuilder();
			String hongxiaodai = "";// 红孝带
			StringBuilder shouBei = new StringBuilder();
			String shoubei = "";// 寿被
			StringBuilder shouYi = new StringBuilder(); 
			String shouyi = "";// 寿衣
			StringBuilder yuYi = new StringBuilder();
			String yuyi = "";// 雨s 伞[
			byte sort = -1;
			String itemName = "";
			int count = 0;
	
			
			
			for(int j=0;j<buygood.getListbuyGoodD().size();j++){
				
				if (buygood.getListbuyGoodD() != null) {
					BuyGoodD buy = buygood.getListbuyGoodD().get(j);
					sort = buy.getItem().getSort();
					count = buy.getNumber();
					itemName = buy.getItem().getName();
					int pice = buy.getItem().getPice();
					if (sort == ItemConst.BaoBuHe_Sort) {
						baobuhe = pice + "*" + count;
						baoBuHe.append(baobuhe).append(",");
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
						
						baoBuHe.append(baobuhe).append(",");
					}
					if (sort == ItemConst.XiaoHuaQuan_Sort) {
						xiaohuaquan = pice + "*" + count;
						xiaoHuaQuan.append(xiaohuaquan).append(",");
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.BaoHuJi_Sort) {
						baohuji = pice + "*" + count;
						baoHuJi.append(baohuji).append(",");
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.GuHuiHe_Sort) {
						guhuihe = pice + "*" + count;
						guHuiHe.append(guhuihe).append(",");
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.HongXiaoDai_Sort) {
						hongxiaodai = pice + "*" + count;
						hongXiaoDai.append(hongxiaodai).append(",");
						
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.ShouBei_Sort) {
						shoubei = pice + "*" + count;
						shouBei.append(shoubei).append(",");
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.ShouYi_Sort) {
						shouyi = pice + "*" + count;
						shouYi.append(shouyi).append(",");
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.YuSan_Sort) {
						yuyi = pice + "*" + count;
						yuYi.append(yuyi).append(",");
						if (namePrice2.containsKey(itemName)) {
							String strs = namePrice2.get(itemName);
							count += Integer.valueOf(strs);
							namePrice2.put(itemName, ""+count);
						} else {
							namePrice2.put(itemName, ""+count);
						}
					}
				}
			}
			//全部空白项  不予以显示
			if(baoBuHe.length() == 0 &&  xiaoHuaQuan.length() ==0 && baoHuJi.length()==0 && guHuiHe.length() == 0 && hongXiaoDai.length()==0 && shouYi.length()==0 && shouBei.length()==0 && yuYi.length()== 0){		
				continue;
		}
		else{
			o[0] = id;
			o[1] = i+1;
			o[2] = orderId;
			o[3] = deadName;
			o[4] = age;
			o[5] = sex;
			o[6] = farewellName;
			o[7] = farewellTime;
			o[8] = payName;
			/*o[8] = comment;
			o[9] = dealIsdel;*/
//			o[9] = baoBuHe;
			o[10] = xiaoHuaQuan;
			o[11] = baoHuJi;
//			o[12] = guHuiHe;
			o[9]=guHuiHe;
			o[12]=baoBuHe;
			o[13] = hongXiaoDai;
			o[14] = shouBei;
			o[15] = shouYi;
			o[16] = yuYi;
			
			o[17] = comment;
			o[18] = dealIsdel;
			o[19] = "";
			list2.add(o);
			}
		}
//		List<ItemChange> itemChangeList=itemChangeService.getItemChangeList(null);
		List<Map<Long,String>> changeSortCommission=new ArrayList<Map<Long,String>>();//记录类别，以便在前端作判断
		for (int i = 0; i < listorder.size(); i++) {//委托业务（火化委托单）
			Object[] o = new Object[18];
			String id=listorder.get(i).getId();
			CommissionOrder  commissoinOrder = commissionOrderService.getCommissionOrderById(id);
			//以下开始判断是否有更改 丧葬用品记录
			Map<String,Object> maps=new HashMap<String,Object>();
			maps.put("commissionOrderId", id);
			List<ItemChange> itemChangeList=itemChangeService.getItemChangeList(maps);
			if(itemChangeList.size()>0){//有更改记录，即需要在页面中显示出来
				Map<Long,String> sortmap=new HashMap<>();
				for(int j=0;j<itemChangeList.size();j++){
					int x=Integer.parseInt(itemChangeList.get(j).getSort());
					if(x==ItemConst.BaoBuHe_Sort){
						sortmap.put(10L, "yes");
					}else if(x==ItemConst.XiaoHuaQuan_Sort){
						sortmap.put(8L, "yes");
					}else if(x==ItemConst.BaoHuJi_Sort){
						sortmap.put(9L, "yes");
					}else if(x==ItemConst.GuHuiHe_Sort){
						sortmap.put(7L, "yes");
					}else if(x==ItemConst.HongXiaoDai_Sort){
						sortmap.put(11L, "yes");
					}else if(x==ItemConst.ShouBei_Sort){
						sortmap.put(12L, "yes");
					}else if(x==ItemConst.ShouYi_Sort){
						sortmap.put(13L, "yes");
					}else if(x==ItemConst.YuSan_Sort){
						sortmap.put(14L, "yes");
					}else{
						sortmap.put(15L, "no");
					}
				}
				changeSortCommission.add(sortmap);
			}
			
			/*Timestamp takeTime = listorder.get(i).getPayTime();*/
			//p判别领用时间
			Timestamp takeawayTime = getTakeawayTime(commissoinOrder);
			Timestamp  farewellTime = commissoinOrder.getFarewellTime();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");//定义日期类型格式
			String takeTimeStr = "";
			takeTimeStr = format.format(takeawayTime);
			String deadName = listorder.get(i).getName();// 死者名字
			int age = listorder.get(i).getAge();// 年龄
			String sex = listorder.get(i).getSexName();// 性别
			String farewellName = null;// 告别厅
			if (listorder.get(i).getFarewell() != null && !listorder.get(i).getFarewell().equals("")) {
				farewellName = listorder.get(i).getFarewell().getName();
			}
		
			String comment = listorder.get(i).getComment();// 备注
			byte dealIsdel0 = listorder.get(i).getDealIsdel();
			String dealIsdel1 = "";
			if (dealIsdel0 == (byte) 1) {
				dealIsdel1 = "1";
			}
			if (dealIsdel0 == (byte) 2) {
				dealIsdel1 = "2";
			}
			if (dealIsdel0 == (byte) 3) {
				dealIsdel1 = "3";
			}
			String dealIsdel = commissionOrderService.getDealIselOption0(dealIsdel1, false);// 处理状态
			StringBuilder baoBuHe = new StringBuilder();
			String baobuhe = "";// 包布盒
			StringBuilder xiaoHuaQuan = new StringBuilder();
			String xiaohuaquan = "";// 小花圈
			StringBuilder baoHuJi = new StringBuilder();
			String baohuji = "";// 保护剂
			StringBuilder guHuiHe = new StringBuilder();
			String guhuihe = "";// 骨灰盒
			StringBuilder hongXiaoDai = new StringBuilder();
			String hongxiaodai = "";// 红孝带
			StringBuilder shouBei = new StringBuilder();
			String shoubei = "";// 寿被
			StringBuilder shouYi = new StringBuilder(); 
			String shouyi = "";// 寿衣
			StringBuilder yuYi = new StringBuilder();
			String yuyi = "";// 雨s 伞[
			byte sort = -1;
			String itemName = "";
			int count = 0;
			
			List<ItemOrder> itemOrder = listorder.get(i).getListItemOrder();
			for (int j = 0; j < itemOrder.size(); j++) {
				ItemOrder it = itemOrder.get(j);
				if (it.getItem() != null) {
					sort = it.getItem().getSort();
					count = itemOrder.get(j).getNumber();
					itemName = it.getItem().getName();
//这是之前的处理方式，下面的改成 只有数量，以防出错，备份					
//					if (sort == ItemConst.BaoBuHe_Sort) {
//						baobuhe = itemOrder.get(j).getItem().getPice() + "*" + count;
//						if (namePrice.containsKey(itemName)) {
//							String[] strs = namePrice.get(itemName).split("\\*");
//							count += Integer.valueOf(strs[1]);
//							namePrice.put(itemName, strs[0] + "*" + count);
//						} else {
//							namePrice.put(itemName, itemOrder.get(j).getItem().getPice() + "*" + count);
//						}
//						
//						baoBuHe.append(baobuhe).append(",");
//					}
					if (sort == ItemConst.BaoBuHe_Sort) {
						baobuhe = itemOrder.get(j).getItem().getPice() + "*" + count;
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
						
						baoBuHe.append(baobuhe).append(",");
					}
					if (sort == ItemConst.XiaoHuaQuan_Sort) {
						xiaohuaquan = itemOrder.get(j).getItem().getPice() + "*" + count;
						xiaoHuaQuan.append(xiaohuaquan).append(",");
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.BaoHuJi_Sort) {
						baohuji = itemOrder.get(j).getItem().getPice() + "*" + count;
						baoHuJi.append(baohuji).append(",");
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.GuHuiHe_Sort) {
						guhuihe = itemOrder.get(j).getItem().getPice() + "*" + count;
						guHuiHe.append(guhuihe).append(",");
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.HongXiaoDai_Sort) {
						hongxiaodai = itemOrder.get(j).getItem().getPice() + "*" + count;
						hongXiaoDai.append(hongxiaodai).append(",");
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.ShouBei_Sort) {
						shoubei = itemOrder.get(j).getItem().getPice() + "*" + count;
						shouBei.append(shoubei).append(",");
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.ShouYi_Sort) {
						shouyi = itemOrder.get(j).getItem().getPice() + "*" + count;
						shouYi.append(shouyi).append(",");
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
					}
					if (sort == ItemConst.YuSan_Sort) {
						yuyi = itemOrder.get(j).getItem().getPice() + "*" + count;
						yuYi.append(yuyi).append(",");
						if (namePrice.containsKey(itemName)) {
							String strs = namePrice.get(itemName);
							count += Integer.valueOf(strs);
							namePrice.put(itemName, ""+count);
						} else {
							namePrice.put(itemName, ""+count);
						}
					}
				}
			}
	/*		if(baoBuHe.toString() != "" ||  xiaoHuaQuan.toString() !="" || baoHuJi.toString()!="" || guHuiHe.toString() != "" || hongXiaoDai.toString() !="" || shouYi.toString()!="" || shouBei.toString()!="" || yuYi.toString()!= ""){	*/
				
			if(baoBuHe.length() == 0 &&  xiaoHuaQuan.length() ==0 && baoHuJi.length()==0 && guHuiHe.length() == 0 && hongXiaoDai.length()==0 && shouYi.length()==0 && shouBei.length()==0 && yuYi.length()== 0){		
				continue;
			}
			else{
				o[0] = id;
				o[1] = i+1;
				o[2] = deadName;
				o[3] = age;
				o[4] = sex;
				o[5] = farewellName;
				o[6] = farewellTime;
//				o[7] = baoBuHe;
				o[8] = xiaoHuaQuan;
				o[9] = baoHuJi;
//				o[10] = guHuiHe;
				o[7] = guHuiHe;
				o[10] = baoBuHe;
				o[11] = hongXiaoDai;
				o[12] = shouBei;
				o[13] = shouYi;
				o[14] = yuYi;
				
				o[15] = comment;
				o[16] = dealIsdel;
				o[17] = "";
				list.add(o);
			}
		}
		//客户要求排序：按  是否有告别厅排序，再按骨灰盒价格排序     委托业务领用
		List<Object[]> listYes = new ArrayList<Object[]>();
		List<Object[]> listNot = new ArrayList<Object[]>();
		
		for(int i=0;i<list.size();i++){//先将有告别厅与没有告别厅的区别开
			String gaobie=(String)list.get(i)[5];
			if(gaobie!=null && !"".equals(gaobie)){
				listYes.add(list.get(i));
			}else{
				listNot.add(list.get(i));
			}
		}
		list.removeAll(list);//清空之前的list,重新添加
		
		int[] intNum=new int[listYes.size()];//骨灰盒价格
		int[] intIndex=new int[listYes.size()];//骨灰盒定位index
		for(int i=0;i<listYes.size();i++){
			String guhuihe="";
			if(listYes.get(i)[7]!=null){
				guhuihe=listYes.get(i)[7].toString();
			}
//			String guhuihe=listYes.get(i)[7].toString();
			String strm[]=guhuihe.split("\\*");
			if(guhuihe==null || "".equals(guhuihe)){
				strm[0]="0";
			}
			int x=Integer.parseInt(strm[0]);
			intNum[i]=x;
			intIndex[i]=i;
		}
		
		for(int i=0;i<listYes.size();i++){
			for(int j=i+1;j<listYes.size();j++){
				if(intNum[i]<intNum[j]){
					int temp=intNum[i];
					intNum[i]=intNum[j];
					intNum[j]=temp;
					
					int x=intIndex[i];
					intIndex[i]=intIndex[j];
					intIndex[j]=x;
				}
			}
		}
		int index=1;
		for(int i=0;i<intIndex.length;i++){
			listYes.get(intIndex[i])[1]=index;
			list.add(listYes.get(intIndex[i]));
			index++;
		}
		//排序没有告别厅的
		int[] intNum2=new int[listNot.size()];
		int[] intIndex2=new int[listNot.size()];
		for(int i=0;i<listNot.size();i++){
			String guhuihe="";
			if(listNot.get(i)[7]!=null){
				guhuihe=listNot.get(i)[7].toString();
			}
//			String guhuihe=listNot.get(i)[7].toString();
			String strm[]=guhuihe.split("\\*");
			if(guhuihe==null || "".equals(guhuihe)){
				strm[0]="0";
			}
			int x=Integer.parseInt(strm[0]);
			intNum2[i]=x;
			intIndex2[i]=i;
		}
		
		for(int i=0;i<listNot.size();i++){
			for(int j=i+1;j<listNot.size();j++){
				if(intNum2[i]<intNum2[j]){
					int temp=intNum2[i];
					intNum2[i]=intNum2[j];
					intNum2[j]=temp;
					
					int x=intIndex2[i];
					intIndex2[i]=intIndex2[j];
					intIndex2[j]=x;
				}
			}
		}
		for(int i=0;i<intIndex2.length;i++){
			listNot.get(intIndex2[i])[1]=index;
			list.add(listNot.get(intIndex2[i]));
			index++;
		}
		
		
		
		
		//客户要求排序：按  是否有告别厅排序，再按骨灰盒价格排序    非委托业务领用
		List<Object[]> listYes2 = new ArrayList<Object[]>();
		List<Object[]> listNot2 = new ArrayList<Object[]>();
		
		for(int i=0;i<list2.size();i++){//先将有告别厅与没有告别厅的区别开
			String gaobie=(String)list2.get(i)[6];
			if(gaobie!=null && !"".equals(gaobie)){
				listYes2.add(list2.get(i));
			}else{
				listNot2.add(list2.get(i));
			}
		}
		list2.removeAll(list2);//清空之前的list,重新添加
		
		int[] intNum3=new int[listYes2.size()];//骨灰盒价格
		int[] intIndex3=new int[listYes2.size()];//骨灰盒定位index
		for(int i=0;i<listYes2.size();i++){
			String guhuihe="";
			if(listYes2.get(i)[9]!=null){
				guhuihe=listYes2.get(i)[9].toString();
			}
			String strm[]=guhuihe.split("\\*");
			if(guhuihe==null || "".equals(guhuihe)){
				strm[0]="0";
			}
			int x=Integer.parseInt(strm[0]);
			intNum3[i]=x;
			intIndex3[i]=i;
		}
		
		for(int i=0;i<listYes2.size();i++){
			for(int j=i+1;j<listYes2.size();j++){
				if(intNum3[i]<intNum3[j]){
					int temp=intNum3[i];
					intNum3[i]=intNum3[j];
					intNum3[j]=temp;
					
					int x=intIndex3[i];
					intIndex3[i]=intIndex3[j];
					intIndex3[j]=x;
				}
			}
		}
		int index2=1;
		for(int i=0;i<intIndex3.length;i++){
			listYes2.get(intIndex3[i])[1]=index2;
			list2.add(listYes2.get(intIndex3[i]));
			index2++;
		}
		//排序没有告别厅的
		int[] intNum4=new int[listNot2.size()];
		int[] intIndex4=new int[listNot2.size()];
		for(int i=0;i<listNot2.size();i++){
			String guhuihe="";
			if(listNot2.get(i)[9]!=null){
				guhuihe=listNot2.get(i)[9].toString();
			}
			String strm[]=guhuihe.split("\\*");
			if(guhuihe==null || "".equals(guhuihe)){
				strm[0]="0";
			}
			int x=Integer.parseInt(strm[0]);
			intNum4[i]=x;
			intIndex4[i]=i;
		}
		
		for(int i=0;i<listNot2.size();i++){
			for(int j=i+1;j<listNot2.size();j++){
				if(intNum4[i]<intNum4[j]){
					int temp=intNum4[i];
					intNum4[i]=intNum4[j];
					intNum4[j]=temp;
					
					int x=intIndex4[i];
					intIndex4[i]=intIndex4[j];
					intIndex4[j]=x;
				}
			}
		}
		for(int i=0;i<intIndex4.length;i++){
			listNot2.get(intIndex4[i])[1]=index2;
			list2.add(listNot2.get(intIndex4[i]));
			index2++;
		}
		
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Name,Const.Search_Type_Card});
		model.put("searchOption", searchOption);
		model.put("list", list);
		model.put("list2", list2);
		model.put("namePrice", namePrice);
		model.put("namePrice2", namePrice2);
		model.put("method", "list");
		model.put("Yes_treated", Const.Yes_treated);
		model.put("No_treated", Const.No_treated);
		model.put("Yes_usered", Const.Yes_usered);
		model.put("dealIsdel", Const.getDealIsdelOption(getByte("id"), false));
		model.put("calculateTime", calculateTime);
		
		model.put("changeFlagFuneral", changeFlagFuneral);
		model.put("changeFlagCommission", changeSortCommission);
		return new ModelAndView("ye/taskFuneral/listTaskFuneral", model);
	}

	/**
	 * 委托业务
	 * 修改丧葬用品核对列表状态
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=isdel")
	@SystemControllerLog(description = "委托业务修改丧葬用品核对列表信息")
	@ResponseBody
	public JSONObject updateDealIsdel() {
		JSONObject json = new JSONObject();
		try {
			String id = getString("id");
			byte isdel = getByte("isdel");
			commissionOrderService.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误" + e.getMessage());
		}
		return json;
	}
	
	/**
	 * 非委托业务
	 * 修改丧葬用品核对列表状态
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=isdelNodelegate")
	@SystemControllerLog(description = "非委托业务修改丧葬用品核对列表信息")
	@ResponseBody
	public JSONObject updateNodelegateDealIsdel() {
		JSONObject json = new JSONObject();
		try {
			String id = getString("id");
			byte isdel = getByte("isdel");
			funeralbuyservice.updateIsdel(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误" + e.getMessage());
		}
		return json;
	}
	/**
	 * 备注 编辑页面
	 * @return
	 */
	@RequestMapping(params = "method=editComment")
    public ModelAndView editcomment(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		String src = getString("src");
		String comment="";
		if(src.equals("delegat")){
			CommissionOrder commissionOrder = commissionOrderService.getCommissionOrderById(id);
			comment = commissionOrder.getComment();
		}else{
			ListFuneralOrderRecord   buygood = funeralbuyservice.getBuyGoodsRecordFeeById(id);
			comment = buygood.getRemarktext();
		}
        model.put("src", src);
        model.put("id", id);
        model.put("comment", comment);
        model.put("method", "saveComment");
		return new ModelAndView("ye/taskFuneral/listTaskComment",model);
    }
	
	/**
	 * 备注编辑页面的保存
	 * @return
	 */
	@RequestMapping(params = "method=saveComment")
	@SystemControllerLog(description = "丧葬用品备注信息修改")
	@ResponseBody
	public JSONObject editComment() {
		JSONObject json = new JSONObject();
		try {
			String src = getString("src");
			String id = getString("id");
			String comment = getString("comment");
			//委托业务备注信息修改
			if(src.equals("delegat")){
				CommissionOrder commissionOrder = commissionOrderService.getCommissionOrderById(id);
				commissionOrder.setComment(comment);
				commissionOrderService.updateCommissionOrder(commissionOrder);
			}else{
				//非委托业务备注信息修改
				ListFuneralOrderRecord   buygood = funeralbuyservice.getBuyGoodsRecordFeeById(id);
				buygood.setRemarktext(comment);
				funeralbuyservice.updateOrderRecord(buygood);
			}
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误" + e.getMessage());
		}
		return json;
	}
	
	
	
	
	
	
	public  Timestamp getTakeawayTime(CommissionOrder  commissionOrder){
		Timestamp  farewellTime = commissionOrder.getFarewellTime();
		Timestamp mourningTime = commissionOrder.getMourningTime();
		Timestamp checkTime = commissionOrder.getCheckTime();
		Timestamp  creamtionTime = commissionOrder.getCremationTime();
		Timestamp takeawayTime = null;
		if(farewellTime!= null){
			takeawayTime =farewellTime; 
		}
		else if	(creamtionTime!=null){
				takeawayTime=   creamtionTime;}
		 else if(mourningTime!=null){
					takeawayTime = mourningTime;}
		 else{
			 	takeawayTime = checkTime;
				}
		
		return takeawayTime;	
	}
    /**
	 * 丧葬用品核对列表
	 * 
	 * @return
	 */
	/*@RequestMapping(params = "method=listInfo")
	public ModelAndView listFarewells() {
		Map<String, Object> model = new HashMap<String, Object>();
		Map map = new HashMap();
		String id = getString("id");
		map.put("isdel", getByte("isdel"));
		map.put("commissionOrderId", id);
		CommissionOrder cs = new CommissionOrder();
		ItemOrder itemOrder = new ItemOrder();
		Item item = new Item();
		List list = itemOrderService.getItemOrderList(map);
		for (int i = 0; i < list.size(); i++) {
			itemOrder = (ItemOrder) list.get(i);
		}
		if (id != null && !id.equals("")) {
			cs = commissionOrderService.getCommissionOrderById(id);

		}
		model.put("cs", cs);
		model.put("id", id);
		model.put("itemOrder", itemOrder);
		return new ModelAndView("ye/taskFuneral/charFuneral", model);
	}*/

	/**
     * 导出Excel测试(gpf)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "丧葬用品核对表统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[17];  //表格的标题栏
        /*int size =list.get(0).length;
        for (int i= 0;i<size;i++){
        	//headers[i] = list.get(0)[i].toString();
        	headers[i] = list.get(0)[i].toString();
        }*/
        headers[0]="编号";
        headers[1]="日期";
        headers[2]="死者姓名";
        headers[3]="年龄";
        headers[4]="性别";
        headers[5]="告别厅";
        headers[6]="告别时间";
        headers[7]="备注";
        headers[8]="状态";
        headers[9]="包布盒";
        headers[10]="小花圈";
        headers[11]="保护剂";
        headers[12]="骨灰盒";
        headers[13]="红孝带";
        headers[14]="寿被";
        headers[15]="寿衣";
        headers[16]="雨伞";
       /* headers[0]="日期";
        headers[1]="死者姓名";
        headers[2]="年龄";
        headers[3]="性别";
        headers[4]="告别厅";
        headers[5]="告别时间";
        headers[6]="备注";
        headers[7]="状态";
        headers[8]="包布盒";
        headers[9]="小花圈";
        headers[10]="保护剂";
        headers[11]="骨灰盒";
        headers[12]="红孝带";
        headers[13]="寿被";
        headers[14]="寿衣";
        headers[15]="雨伞";*/
        
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "丧葬用品核对表统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Excel测试(gpf)
     * @return
     */
    @RequestMapping(params = "method=exportExcelNoDel")
    public void exportExcelNoDel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "丧葬用品核对表统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[18];  //表格的标题栏
       /* for (int i= 0;i<list2.size();i++){
        	headers[i] = list2.get(0)[i].toString();
        }*/
        headers[0]="编号";
        headers[1]="日期";
        headers[2]="死者姓名";
        headers[3]="年龄";
        headers[4]="性别";
        headers[5]="告别厅";
        headers[6]="告别时间";
        headers[7]="购买人";
        headers[8]="备注";
        headers[9]="状态";
        headers[10]="包布盒";
        headers[11]="小花圈";
        headers[12]="保护剂";
        headers[13]="骨灰盒";
        headers[14]="红孝带";
        headers[15]="寿被";
        headers[16]="寿衣";
        headers[17]="雨伞";
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "丧葬用品核对表统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list2, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "丧葬用品核对表统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}        
    	response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[17];  //表格的标题栏
        /*    for (int i= 0;i<list.size();i++){
    	headers[i] = list.get(0)[i].toString();
    }*/
        headers[0]="编号";
        headers[1]="日期";
        headers[2]="死者姓名";
        headers[3]="年龄";
        headers[4]="性别";
        headers[5]="告别厅";
        headers[6]="告别时间";
        headers[7]="备注";
        headers[8]="状态";
        headers[9]="包布盒";
        headers[10]="小花圈";
        headers[11]="保护剂";
        headers[12]="骨灰盒";
        headers[13]="红孝带";
        headers[14]="寿被";
        headers[15]="寿衣";
        headers[16]="雨伞";
 
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "丧葬用品核对表统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
/*			String commissionOrderId = listorder.get(i).getId();
Map<String, Object> maps = new HashMap<String, Object>();
maps.put("commissionOrderId", commissionOrderId);
List<Buygood> buyGoods = funeralbuyservice.findItemOrder(maps);

int itemOrderSize = itemOrder.size();
//生成一个  在结果集  满足 查询条件的 item的id的集合   用于  对 buyGood满足条件的 itemID进行  比对
List<String>  set = new ArrayList<String>();
for(int n=0; n<itemOrderSize;n++){
	 int sortS = itemOrder.get(n).getItem().getSort();
	if (sortS==ItemConst.BaoBuHe_Sort || sortS==ItemConst.XiaoHuaQuan_Sort || sortS==ItemConst.BaoHuJi_Sort || sortS==ItemConst.GuHuiHe_Sort || sortS==ItemConst.HongXiaoDai_Sort||
			sortS==ItemConst.ShouBei_Sort || sortS==ItemConst.ShouYi_Sort || sortS==ItemConst.YuSan_Sort){
		String itemid=itemOrder.get(n).getItem().getId();
		set.add(itemid);
	}
}

List<ItemOrder> listA = new ArrayList<ItemOrder>();
 HashMap<String,Object> hashMapA = new HashMap<String,Object>();
List<Buygood> listB = new ArrayList<Buygood>();
 HashMap<String,Object> hashMapB = new HashMap<String,Object>();
//求取 火化委托单的 丧葬用品项 （去重复 ）并将整合后的项 转化为 hash 表
for(int n=0; n<itemOrderSize;n++){
	 int sortS = itemOrder.get(n).getItem().getSort();
	if (sortS==ItemConst.BaoBuHe_Sort || sortS==ItemConst.XiaoHuaQuan_Sort || sortS==ItemConst.BaoHuJi_Sort || sortS==ItemConst.GuHuiHe_Sort || sortS==ItemConst.HongXiaoDai_Sort||
			sortS==ItemConst.ShouBei_Sort || sortS==ItemConst.ShouYi_Sort || sortS==ItemConst.YuSan_Sort){
		//bool  判断hashMap中是否有 重复的Key
		Object bool=  hashMapA.get(itemOrder.get(n).getItem().getId());
		if( bool != null){
			for(int i1=0;i1<listA.size();i1++){
				if(itemOrder.get(n).getItem().getId().equals(listA.get(i1).getItem().getId())){
					int s=itemOrder.get(n).getNumber()+listA.get(i1).getNumber();
					listA.get(i1).setNumber(s);
				}
			}			
		}
		else{
			hashMapA.put(itemOrder.get(n).getItem().getId(),(object) itemOrder.get(n)); 
			listA.add(itemOrder.get(n));
		}
	}
	  
	}*/

/*//求取  丧葬用品购买记录 （将重复项整合） 并生成hash 表
for (int x1 = 0; x1 < buyGoods.size(); x1++) {
	Buygood buy2= buyGoods.get(x1);
		int sort22 = buy2.getSort();
		if (sort22==ItemConst.BaoBuHe_Sort || sort22==ItemConst.XiaoHuaQuan_Sort || sort22==ItemConst.BaoHuJi_Sort || sort22==ItemConst.GuHuiHe_Sort || sort22==ItemConst.HongXiaoDai_Sort||
				sort22==ItemConst.ShouBei_Sort || sort22==ItemConst.ShouYi_Sort || sort22==ItemConst.YuSan_Sort){
			Object bool=  hashMapB.get(buy2.getItemId());
			
			//	Boolean bool= (Boolean) hashMapA.put(itemOrder.get(n).getItem().getId(),(object) itemOrder.get(n));
				if( bool != null){
					for(int i1=0;i1<listB.size();i1++){
						if(buy2.getItemId().equals(listB.get(i1).getItemId())){
							int s = buy2.getNumber()+listB.get(i1).getNumber();
							listB.get(i1).setNumber(s);
						}
					}			
				}
				else{
					hashMapB.put(buy2.getItemId(),buy2); 
					listB.add(buy2);
				}
		}
}

//通过 循环 判断 求取 两者之间的交集	
List<ItemOrder> tmpitemOrder = new ArrayList<ItemOrder>();
for(int s = 0;s<listB.size();s++){
	String 	key=listB.get(s).getItemId();
	Object bool=hashMapA.get(key);
	
    if (bool != null){
    	for(int i1=0;i1<listA.size();i1++){
    		if(listA.get(i1).getItem().getId().equals(listB.get(s).getItemId())){
    			int s1= listA.get(i1).getNumber() + listB.get(s).getNumber();
    			listA.get(i1).setNumber(s1);
    		}
    	}
    	
    }
    else{
    	Item item = new Item();
		item.setId(listB.get(s).getItemId());
		item.setName(listB.get(s).getName());
		item.setSort(listB.get(s).getSort());
		ItemOrder newitemOrder = new ItemOrder();
		newitemOrder.setNumber(listB.get(s).getNumber());
		newitemOrder.setItem(item);
		tmpitemOrder.add(newitemOrder);
    }		
}


for(int i1=0;i1<tmpitemOrder.size();i1++){	
	listA.add(tmpitemOrder.get(i1));
}

itemOrder.clear();

for(int j1 = 0;j1<listA.size();j1++){
	ItemOrder itemOrderNew = listA.get(j1);
	itemOrder.add(itemOrderNew);	
}*/
