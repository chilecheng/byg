package com.hz.controller.ye.firstDepart;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.BuyWreathRecord;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.ye.BuyWreathRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
/**
 * 花圈花篮统计单
 * @author mbz
 *
 */
@Controller
@RequestMapping("/buyFlowerWordOrder.do")
public class BuyFlowerWordOrderController extends BaseController {
	@Autowired
	private BuyWreathRecordService buyWreathRecordService;
	@Autowired
	private UserNoticeService userNoticeService;
	
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "buyWreathRecord.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		userNoticeService.updateUserNoticeByUser(getCurrentUser().getUserId(), Const.NoticeType_BuyFlower, DateTools.getThisDate());
		return listbuyFlowerWordOrder();
    }
	/**
	 * 花圈花篮统计
	 * @return
	 */
	@RequestMapping(params = "method=list")
	private ModelAndView listbuyFlowerWordOrder() {
		Map<String,Object> model=new HashMap<String,Object>();
    	model=getModel(model);
    	//获得页面页码信息
		int[] pages = getPage();
		//插入查询条件
		Map<String, Object> maps = new HashMap<String, Object>();
		List<Object[]> list = new ArrayList<Object[]>();
		String input = getString("search");
		Date start = getDate("startTime");
		Timestamp startTime = null;
		Timestamp endTime = null;
		if(start!=null){
			startTime = new Timestamp(start.getTime());
		}
		Date end = getDate("endTime");
		if(end!=null){
			endTime = new Timestamp(end.getTime());
		}
		maps.put("begin", startTime);
		maps.put("end", endTime);
		String type = getString("type");
		if(type.equals("deadName")){
			maps.put("deadName", input);
		}
		if(type.equals("writeUser")){
			maps.put("writeUser", input);
		}
		if(type.equals("orderUser")){
			maps.put("orderUser", input);
		}
		if(type.equals("buyNumber")){
			maps.put("buyNumber", input);
		}
		maps.put("checkFlag", Const.Check_Yes);
		PageHelper.startPage(pages[0], pages[1]);
		PageHelper.orderBy("");
		List<BuyWreathRecord> listBuyWreathRecord = new ArrayList<BuyWreathRecord>();
		listBuyWreathRecord = buyWreathRecordService.getBuyWreathRecordList(maps);
		PageInfo<BuyWreathRecord> page1 = new PageInfo<BuyWreathRecord>(listBuyWreathRecord);
		for(int i=0; i<listBuyWreathRecord.size(); i++){
			Object[] o=new Object[8];
			BuyWreathRecord bwr = listBuyWreathRecord.get(i);
			String cardCode = bwr.getCommissionOrder().getCardCode();
			String name = bwr.getCommissionOrder().getName();
			String fName = bwr.getCommissionOrder().getfName();
			String fareName = bwr.getCommissionOrder().getFarewell().getName();
			Timestamp fTime = bwr.getCommissionOrder().getFarewellTime();
			String mourName = bwr.getCommissionOrder().getMourning().getName();
			Timestamp mTime = bwr.getCommissionOrder().getMourningTime();
			//Integer number = bwr.getBuyWreathRecordD().getNumber();
			o[0] = cardCode;
			o[1] = name;
			o[2] = fName;
			o[3] = fareName;
			o[4] = fTime;
			o[5] = mourName;
			o[6] = mTime;
			//o[7] = number;
			list.add(o);
		}
		PageInfo<Object[]> page = new PageInfo<Object[]>(list);
		page.setPageNum(page1.getPageNum());
		page.setSize(page1.getSize());
		page.setPageSize(page1.getPageSize());
		page.setStartRow(page1.getStartRow());
		page.setEndRow(page1.getEndRow());
		page.setTotal(page1.getTotal());
		page.setPages(page1.getPages());
		
		model.put("page", page);
		model.put("list", list);
		//PageInfo<BuyWreathRecord> page=buyWreathRecordService.getBuyWreathRecordPageInfo(maps,pages[0],pages[1],"");
		//model.put("page", page);
		model.put("method", "list");
		return new ModelAndView("ye/first_department/flowerStatic/listFlowerStatic",model);
	}
	/**
	 * 信息显示
	 * @return
	 */
	@RequestMapping(params = "method=readOnly")
	public ModelAndView readOnly(){
		Map<String, Object> model=new HashMap<String, Object>();
		Map<String, Object> maps = new HashMap<String, Object>();
		String id = getString("id");
		List<BuyWreathRecord> bwr = buyWreathRecordService.getBuyWrethRecord(maps);
		model.put("bwr", bwr);
		return new ModelAndView("ye/first_department/flowerStatic/readFlowerStatic",model);
	}
}
