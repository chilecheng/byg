package com.hz.controller.ye;

//import java.sql.Date;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.system.User;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FillCardRecord;
import com.hz.entity.ye.ListCardRecord;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.RecardService;
import com.hz.util.UuidUtil;
/**
 * 补卡
 * @author lipengpeng
 *
 */
@Controller
@RequestMapping("/buka.do")
public class RecardController extends BaseController{
	@Autowired
	private RecardService recardervice;
	@Autowired
	private CommissionOrderService commissionOrderService;
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "buka.do");
    }
	@RequestMapping
    public ModelAndView unspecified() {
		return listCardCount();
    }
	/**
	 * 补卡列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listCardCount() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//...
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("deadName", getString("deadName"));
		maps.put("toTime", getString("toTime"));
		maps.put("dieID", getString("dieID"));
		maps.put("linkName", getString("linkName"));
		PageInfo<ListCardRecord> page=recardervice.getAllRecordPageInfo(maps,pages[0],pages[1],"arrive_time desc");//null or ''
		model.put("page", page);
		return new ModelAndView("ye/buCard/buyFuneral",model);

	}
	/**
	 * 链接到修改添加页面
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView insertCount()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		FillCardRecord fillCardRecord=new FillCardRecord();
		model.put("fillCardRecord", fillCardRecord);
		//recardervice.addFillCard(fillCardRecord);
		String id =getString("id");
		CommissionOrder commissionOrder=new CommissionOrder();
		if (id!=null&&!id.equals("")) {
			commissionOrder=commissionOrderService.getCommissionOrderById(id);
		}
		model.put("commissionOrder", commissionOrder);
		User u = getCurrentUser();
		model.put("u", u);
		return new ModelAndView("ye/buCard/recard",model);
	}
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存补卡信息")
	@ResponseBody
	public JSONObject saveCardInfo(){
		JSONObject json = new JSONObject();
		try {
			FillCardRecord fillCardRecord=new FillCardRecord();
			String cid =getString("cid");//
			fillCardRecord.setCommissionorderid(cid);//commission_order_id
			fillCardRecord.setId(UuidUtil.get32UUID());// id
			String contact=getString("tscPhone");
			fillCardRecord.setContact(contact);	//contact
			fillCardRecord.setCreateTime(new Date());//create_time
			String contactName=getString("transactor");
			fillCardRecord.setContactname(contactName);//contact_name
			fillCardRecord.setOldcard(commissionOrderService.getCommissionOrderById(cid).getCardCode());//old_card
			fillCardRecord.setNewcard(getString("newCard"));//new_card
			User u = getCurrentUser();
			fillCardRecord.setUserid(u.getUserId());//外键！！！	
			//判断卡号是否重复
			int  count =recardervice.getCountCardCode(getString("newCard"));
			if(count == 0){
				recardervice.addFillCardAndUpdateCommissionCard(fillCardRecord);
				setJsonBySuccess(json, "保存成功", true);
			}else{
				setJsonByFail(json, "此卡号已经存在");
			}
			
			
		}
		catch(Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		//unspecified();
		return json;
	}
	
}



