package com.hz.controller.ye;

import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.service.base.AppellationService;
import com.hz.service.ye.AshesRecordDService;
import com.hz.service.ye.AshesRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 骨灰寄存记录缴费，续费记录
 * @author hw
 *
 */
@Controller
@RequestMapping("/ashesRecordD.do")
public class AshesRecordDController extends BaseController{
	@Autowired
	private AshesRecordDService ashesRecordDService;
	@Autowired
	private AshesRecordService ashesRecordService;
	//称谓关系
	@Autowired
	private AppellationService appellationService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "ashesRecordD.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listashesRecordD();
    }

	/**
	 *骨灰寄存缴费，续费记录列表  到期提醒?
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listashesRecordD(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获取当前时间
		Date currentDate  = DateTools.getThisDate();
		model.put("currentDate", currentDate);
		//获取一个星期后的日期
		Date maxDate = DateTools.getDayAfterDay(currentDate,7,Calendar.DATE);
		//获取分页
		int pages[]=getPage();
		//获取记录最小值
//		int min = pages[1]*(pages[0]-1);
		//插入查询条件
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("statusFlag", Const.IsFlag_Yse);
		maps.put("maxDate", maxDate);
//		Map<String,Object> maps0=new HashMap<String, Object>();
		/*maps0.put("statusFlag", Const.IsFlag_Yse);
		maps0.put("maxDate", maxDate);
		maps0.put("min", min);
		maps0.put("num", pages[1]);*/
		PageInfo<AshesRecord> page=ashesRecordService.getAshesRecordPageInfo(maps, pages[0],pages[1], "");
		model.put("page", page);
		return new ModelAndView("ye/ashesRecordD/listAshesRecordD",model);
    }
    /**
	 * 列表形式链接到添加页面
	 * @return
	 */
    @RequestMapping(params = "method=renew")
    public ModelAndView renewashesRecordD(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	//获取办理人姓名
    	model.put("agentUser", getCurrentUser().getName());
    	
    	//获取系统时间
    	//DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	model.put("currentTime",DateTools.getTimeString("yyyy-MM-dd HH:mm:ss"));
    	//获取死者姓名 
    	String ashesRecordId = getString("ashesRecordId");
    	AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
    	String dName = ar.getdName();
    	String code = ar.getCode();
    	String serialNumber=ar.getSerialNumber();
    	Date endDate = ar.getEndDate();
    	model.put("code", code);
    	model.put("endDate", endDate);
    	model.put("dName", dName);
    	model.put("serialNumber", serialNumber);
    	model.put("method", "saveRenew");
    	model.put("ashesRecordId", ashesRecordId);
    	return new ModelAndView("ye/ashesRecordD/renewAshesRecordD",model);
    }
    
    
    /**
     * 骨灰寄存续费
     * @return
     */
    @RequestMapping(params = "method=saveRenew")
    @SystemControllerLog(description = "保存骨灰寄存续费")
    @ResponseBody
    public JSONObject saveRenewAshesRecordD(){
    	JSONObject json = new JSONObject();
    	try {
    		String ashesRecordId = getString("ashesRecordId");
    		AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
        	String code = ar.getCode();
        	Date endDate = ar.getEndDate();
        	
        	Map<String,Object> paramMap =new HashMap<String, Object>();
        	paramMap.put("recordId", ashesRecordId);
        	paramMap.put("pay_flag", Const.Pay_No);
        	List<AshesRecordD> list=ashesRecordDService.getAshesRecordDList(paramMap);
        	/*
        	 * 此处有两种情况:1.之前都是已缴费
        	 * 				 2.之前有续期,但未缴费的
        	 */
        	if(list.size()>0){//第二种情况 
        		AshesRecordD ashesRecordD =list.get(0);  
        		ashesRecordD.setEndDate(getDate("newEndDate"));
        		ashesRecordD.setTotal(ashesRecordD.getTotal()+getDouble("total"));
        		ashesRecordD.setLongTime(ashesRecordD.getLongTime()+getInt("longTime"));
        		ashesRecordDService.updateAshesRecordD(ashesRecordD);
        	}else if(list.size()==0){//第一种 情况 
	        	AshesRecordD ashesRecordD=new AshesRecordD();
	        	ashesRecordD.setId(UuidUtil.get32UUID());
	        	ashesRecordD.setRecordId(ashesRecordId);
	        	ashesRecordD.setCode(code);
        		ashesRecordD.setBeginDate(endDate);
        		ashesRecordD.setTotal(getDouble("total"));
        		ashesRecordD.setEndDate(getDate("newEndDate"));
        		ashesRecordD.setPayFlag(Const.Pay_No);
        		ashesRecordDService.addAshesRecordD(ashesRecordD);
        	}
        	//骨灰寄存时间随着续费更改
        	AshesRecord ashesRecord=ashesRecordService.getAshesRecordById(ashesRecordId);
    		ashesRecord.setEndDate(getDate("newEndDate"));
    		ashesRecord.setDepositLong(ashesRecord.getDepositLong()+getInt("longTime"));
    		ashesRecordService.updateAshesRecord(ashesRecord);
    		
    		setJsonBySuccess(json, "续费成功", "ashesRecord.do");
		} catch (Exception e) {
			setJsonByFail(json, "续费失败，错误"+e.getMessage());
		}
		return json;
    	
    }
    
    
    
    /**
	 * 骨灰寄存领出
	 * @return
	 */
    @RequestMapping(params = "method=out")
    public ModelAndView outashesRecordD(){
    	Map<String,Object> model=new HashMap<String,Object>();
      	//获取办理人姓名
    	model.put("agentUser", getCurrentUser().getName());
    	
    	//获取系统时间
    	//DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	model.put("currentTime",DateTools.getTimeString("yyyy-MM-dd HH:mm:ss"));
    	
    	model.put("takeTime", DateTools.getThisDate());
    	
    	//获取死者姓名
 
    	String ashesRecordId = getString("ashesRecordId");
    	AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
    	model.put("dName", ar.getdName());
		model.put("dSex", ar.getSexName());
		model.put("dAge", ar.getdAge());
		model.put("cremationTime", ar.getCremationTime());
		model.put("placeCode", ar.getAshesPositionId());
		model.put("beginDate", ar.getBeginDate());
		model.put("endDate", ar.getEndDate());
	
		model.put("sName", ar.getsName());
		model.put("code", ar.getCode());
		model.put("sPhone", ar.getsPhone());
		model.put("sAddr", ar.getsAddr());
		String appellation = (appellationService.getAppellationById(ar.getfAppellationId())).getName();
		model.put("appellation", appellation);
    	model.put("method", "saveOut");
    	model.put("ashesRecordId", ashesRecordId);
    	return new ModelAndView("ye/ashesRecordD/outAshesRecordD",model);
    }
    
    /**
     * 保存骨灰寄存领出信息
     * @return
     */
    @RequestMapping(params = "method=saveOut")
    @SystemControllerLog(description = "保存骨灰寄存领出信息")
    @ResponseBody
    public JSONObject saveOutAshesRecordD(){
		JSONObject json = new JSONObject();
    	try {
    		String ashesRecordId = getString("ashesRecordId");
    		AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
    		ar.setTakeTime(getDate("takeTime"));
    		ar.setTakeName(getString("takeName"));
    		ar.setTakeUserId(getCurrentUser().getUserId());
    		ar.setComment(getString("comment"));
    		ar.setStatusFlag(Const.IsFlag_No);
    		ashesRecordService.updateAshesRecord(ar);
    		setJsonBySuccess(json, "领出成功", "ashesRecordD.do");
		} catch (Exception e) {
			setJsonByFail(json, "领出失败，错误"+e.getMessage());
		}
		return json;
    }

    
   
}
