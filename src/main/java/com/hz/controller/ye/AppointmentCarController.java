package com.hz.controller.ye;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Jeneration;
import com.hz.entity.ye.Appointment;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.base.CarTypeService;
import com.hz.service.base.CorpseAddressService;
import com.hz.service.base.JenerationService;
import com.hz.service.base.TransportTypeService;
import com.hz.service.ye.AppointmentService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 出车登记
 * @author jgj
 *
 */
@Controller
@RequestMapping("/appointmentCar.do")
public class AppointmentCarController extends BaseController{
	//预约登记
	@Autowired
	private AppointmentService appointmentService;
	
//	
	//流水号
	@Autowired
	private JenerationService jenerationService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	//运输类型
	@Autowired
	private TransportTypeService transportTypeService;
	//车辆类型
	@Autowired
	private CarTypeService carTypeService;
	//接尸地址
	@Autowired
	private CorpseAddressService corpseAddService;

	
    @ModelAttribute  
    public void populateModel(Model model) {  
       model.addAttribute("url", "appointmentCar.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listOrder();
    }

	
	 /**
     * 记录列表(模块首页)
     * @return
     */
    @RequestMapping(params = "method=listOrder")
    public ModelAndView listOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int[] pages = getPage();
		String search=getString("search");
		String beginDate=getString("startTime");
		String endDate=getString("endTime");
		Map<String,Object> maps =new HashMap<>();
		if(!"".equals(search) && search!=null){
//			maps.put("findBy",getString("find"));	
			maps.put("name", search);
		}
		if(!"".equals(beginDate) && beginDate !=null){
			maps.put("carTimeS",beginDate);
		}
		if(!"".equals(endDate) && endDate !=null){
			maps.put("carTimeE", endDate+"23:59:59");
		}
		maps.put("isCar", Const.Is_Yes);
		PageInfo page=appointmentService.getAppointmentListPageInfo(maps,pages[0],pages[1],"");
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		model.put("page", page);
		model.put("method","listOrder");
		return new ModelAndView("ye/appointmentCar/listOrderCar",model);
    }
    
	/**
	 * 链接到出车登记页面
	 * @return
	 */
	@RequestMapping(params = "method=carOrder")
    public ModelAndView orderAppointment(){
		Map<String,Object> model=new HashMap<String,Object>();
//		String id=getString("id");
//		Appointment appointment=appointmentService.getAppointmentById(id);
		//接尸地址
		String corpseAddOption=corpseAddService.getCorpseAddressOption(null, false);
		//车辆类型
		String carTypeOption=carTypeService.getCarTypeOption(null,false);
		//运输类型
		String transportOption=transportTypeService.getTransportTypeOption(null, false);
		model.put("method", "save");
		model.put("carTypeOption",carTypeOption );
		model.put("transportOption",transportOption );
		model.put("corpseAddOption", corpseAddOption);
		return new ModelAndView("ye/appointmentCar/carOrder",model);
	}
	/**
	 * 修改出车登记 
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	public ModelAndView editCarOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id =getString("id");
		Appointment appointment=appointmentService.getAppointmentById(id);		
		Timestamp time=appointment.getDepartureTime();
		String pickAdd=appointment.getPickAddress();
		//接尸地址
		String corpseAddOption=corpseAddService.getCorpseAddressOption(null, false);
		//车辆类型
		String carTypeOption=carTypeService.getCarTypeOption(appointment.getCarType(),false);
		//运输类型
		String transportOption=transportTypeService.getTransportTypeOption(appointment.getTransportType(), false);
		model.put("name", appointment.getName());
		model.put("phone", appointment.getPhone());
		model.put("departureTime", time);
		model.put("pickAdd", pickAdd);
		model.put("corpseAddOption", corpseAddOption);
		model.put("carTypeOption", carTypeOption);
		model.put("transportOption", transportOption);	
		model.put("method", "editSave");
		model.put("id", id);
		return new ModelAndView("ye/appointmentCar/editCarOrder",model);
	}
	/**
     * 修改出车预约登记信息
     * @return
     */
    @RequestMapping(params = "method=editSave")
    @SystemControllerLog(description = "修改出车预约登记信息")
    @ResponseBody
    public JSONObject editSave(){
    	JSONObject json = new JSONObject();
    	try{
    		String id=getString("id");
    		String name=getString("dName");
    		String phone=getString("fphone");
    		String carType=getString("carType");
    		String transportType=getString("transport");
    		String pickAddress=getString("pickAddr");
    		Timestamp departureTime=getTimeM("departureTime");
    		
    		Appointment appointment=appointmentService.getAppointmentById(id);
    		appointment.setName(name);
    		appointment.setPhone(phone);
    		appointment.setCarType(carType);
			appointment.setDepartureTime(departureTime);
			appointment.setPickAddress(pickAddress);
			appointment.setTransportType(transportType);
			appointment.setIsCar(Const.Is_Yes);
			appointmentService.updateAppointment(appointment);
    		setJsonBySuccess(json, "修改成功", "appointmentCar.do?method=listOrder");
    	} catch (Exception e) {    		
			e.printStackTrace();
			setJsonByFail(json, "修改失败，错误"+e.getMessage());
    	}
    	return json;
    }
	/**
	 * 查看页面
	 * @return
	 */
	@RequestMapping(params = "method=show")
    public ModelAndView showAppointment(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id=getString("id");
		//接尸地址
		String corpseAddOption=corpseAddService.getCorpseAddressOption(null, false);
		Appointment appointment=appointmentService.getAppointmentById(id);
		//车辆类型
		String carTypeOption=carTypeService.getCarTypeOption(appointment.getCarType(),false);
		//运输类型
		String transportOption=transportTypeService.getTransportTypeOption(appointment.getTransportType(), false);
		model.put("name", appointment.getName());
		model.put("phone", appointment.getPhone());
		model.put("departureTime",appointment.getDepartureTime());
		model.put("pickAdd", appointment.getPickAddress());
		model.put("carTypeOption", carTypeOption);
		model.put("transportOption", transportOption);
		model.put("corpseAddOption", corpseAddOption);
		return new ModelAndView("ye/appointmentCar/showCarOrder",model);
	}
	/**
	 * 选择界面(生成委托单OR数据转移卡号)
	 * @return
	 */
	@RequestMapping(params = "method=choose")
	public ModelAndView chooseAppointment(){
		Map<String,Object> model=new HashMap<String,Object>();
		model.put("id", getString("id"));
		model.put("method", "jumpPage");
		return new ModelAndView("ye/appointmentCar/chooseOrder",model);
	}
	
	/**
	 * 保存出车预约登记信息
	 * @return
	 */
	@RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存出车预约登记信息")
    @ResponseBody
	public JSONObject save(){
		JSONObject json=new JSONObject();
		try{
			String id=getString("id");
			String userName=getCurrentUser().getName();
			String name=getString("dName");
			String phone=getString("fphone");
			Timestamp departureTime=getTimeM("departureTime");
			String pickAddr=getString("pickAddr");
			String carType=getString("carType");
			String transport=getString("transport");
			
			if(id != null && !"".equals(id)){//有ID证明是原有记录,做修改
				Appointment appointment=appointmentService.getAppointmentById(id);
				appointment.setCarType(carType);
				appointment.setPickAddress(pickAddr);
				appointment.setTransportType(transport);
				appointment.setDepartureTime(departureTime);
				appointment.setName(name);
				appointment.setPhone(phone);
				appointment.setIsCar(Const.Is_Yes);
				appointmentService.updateAppointment(appointment);
			}else{//无ID,新加记录
				Appointment appointment=new Appointment();
				Timestamp nowTime=DateTools.getThisDateTimestamp();
				String serialNumber=jenerationService.getJenerationCode(Jeneration.YYDJ_TYPE);
				appointment.setSerialNumber(serialNumber);
				appointment.setId(UuidUtil.get32UUID());
				appointment.setCreateName(userName);
				appointment.setCarType(carType);
				appointment.setCreateTime(nowTime);
				appointment.setPickAddress(pickAddr);
				appointment.setTransportType(transport);
				appointment.setDepartureTime(departureTime);
				appointment.setName(name);
				appointment.setPhone(phone);
				appointment.setIsCar(Const.Is_Yes);
				appointmentService.saveAppointment(appointment);
			}
			setJsonBySuccess(json, "保存成功", "appointmentCar.do?method=listOrder");  
		} catch (Exception e) {    		
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
    	}
		return json;
	}
	
	
    /**
     * 根据登记姓名查找信息 单结果
     * @return
     */
    @RequestMapping(params = "method=dNameSearch")
    @ResponseBody
    public JSONObject dNameSearch(){
		JSONObject json = new JSONObject();
    	String dName = getString("dName");
    	Map<String, Object> maps = new HashMap<String, Object>();
    	maps.put("name", dName);
    	List<Appointment> list = appointmentService.getAppointmentList(maps);
    	Appointment appointment=null;
    	if(list.size()==1){
    		appointment = list.get(0);
    		String serialNumber=appointment.getSerialNumber();
    		String name = appointment.getName();
    		String phone = appointment.getPhone();	
    		String id = appointment.getId();
	    	json.put("name", name);
	    	json.put("phone",phone);
	    	json.put("serialNumber", serialNumber);
	    	json.put("id", id);
    	}
    	json.put("size", list.size());
		return json;
    }
    
    /**
	 *登记姓名查找  多结果 
	 * @return
	 */
    @RequestMapping(params = "method=NameList")
    public ModelAndView dNameListbuyWreathRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	model=getModel(model);
    	//获得页面页码信息
		int[] pages = getPage();
    	String dName = getString("name");
    	Map<String, Object> maps = new HashMap<String, Object>();
    	maps.put("name", dName);
    	PageHelper.startPage(pages[0], pages[1]);
    	List<Appointment> list = appointmentService.getAppointmentList(maps);
    	PageInfo<Appointment> page1 = new PageInfo<Appointment>(list);
    	List<Object[]> list0 = new ArrayList<Object[]>();
    	for(int i=0;i<list.size();i++){
	    	list0.add(new Object[]{list.get(i).getId(),list.get(i).getSerialNumber(),list.get(i).getName(),
	    			list.get(i).getPhone()});
    	}
    	PageInfo<Object[]> page = new PageInfo<Object[]>(list0);
    	page.setPageNum(page1.getPageNum());
		page.setSize(page1.getSize());
		page.setPageSize(page1.getPageSize());
		page.setStartRow(page1.getStartRow());
		page.setEndRow(page1.getEndRow());
		page.setTotal(page1.getTotal());
		page.setPages(page1.getPages());
		model.put("page", page);
    	return new ModelAndView("ye/appointmentCar/seachResult",model);
    }
	
	/**
	 * 删除预约登记 信息
	 * @return
	 */
	@RequestMapping(params = "method=deleteMessage")
    @SystemControllerLog(description = "删除预约登记信息")
    @ResponseBody
    public JSONObject delAppointment(){
    	JSONObject json = new JSONObject();
    	try{
    		String id=getString("id");
    		Appointment appointment=appointmentService.getAppointmentById(id);
    		if(appointment.getIsAppointment()!=Const.Is_Yes){//仅有车辆预约登记,删除整条记录
    			appointmentService.delAppointment(id);
    		}else{//否则仅更改车辆登记 相关信息
    			appointment.setCarType(null);
    			appointment.setDepartureTime(null);
    			appointment.setPickAddress(null);
    			appointment.setTransportType(null);
    			appointment.setIsCar(Const.Zero);
    			appointmentService.updateAppointment(appointment);    			
    		}
    		
    		setJsonBySuccess(json, "删除成功", "appointmentCar.do?method=listOrder");
    	} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
    	}
    	return json;
	}
	
	@RequestMapping(params = "method=isExist")
	@ResponseBody
	public JSONObject isExist(){
		JSONObject json= new JSONObject();
		try{
			String cardCode=getString("code");
			CommissionOrder commissionOrder=commissionOrderService.getCommissionOrderByCardCode(cardCode);
			if(commissionOrder!=null){
				setJsonBySuccess(json, "卡号正确");
			}else{
				setJsonByWarning(json, "该卡号没有数据");
			}
		}catch (Exception e){
			e.printStackTrace();
			setJsonByFail(json, "查询失败，错误"+e.getMessage());
		}
		return json;
	}
	
    
   
   
   
}
