package com.hz.controller.ye;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.BillUnit;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.ye.ChargeAccount;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.ItemOrder;
import com.hz.service.base.BillUnitService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.ye.ChargeAccountService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.ItemOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;
/**
 * 挂账业务   控制层
 * @author jgj
 *
 */
@Controller
@RequestMapping("/chargeAccount.do")
public class ChargeAccountController extends BaseController{
	@Autowired
	private ChargeAccountService chargeAccountService;
	
	@Autowired
	private CommissionOrderService commissionOrderService;
	//挂账单位
	@Autowired
	private BillUnitService billUnitService;
	@Autowired
	private ItemService itemService;
	//收费项目类别
	@Autowired
	private ItemTypeService itemTypeService;
	//用户收费记录
	@Autowired
	private ItemOrderService itemOrderService;
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "chargeAccount.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listChargeAccount();
    }

	/**
	 * 获取挂账业务列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listChargeAccount() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//收费状态(初始为全部)
		String payFlag=getString("checkType");
		//填入查询内容
		Map<String,Object> maps=new HashMap<String,Object>();
		if("checkNo".equals(payFlag)){
			maps.put("billPayFlag", Const.IsHave_No);
		}
		if("checkYes".equals(payFlag)){
			maps.put("billPayFlag", Const.IsHave_Yes);
		}
		String findBy=getString("find");
		maps.put("findBy", findBy);
		//查找值为姓名
		if("1".equals(findBy)){
			String findByName=getString("search");
			maps.put("findByName", findByName);
		}
		//查找值为卡号
		if("2".equals(findBy)){
			String findByCode=getString("search");
			maps.put("findByCode", findByCode);
		}
		//开始时间
		String startTime=getString("startTime");
		if(!"".equals(startTime)){
			maps.put("startTime", startTime);
		}
		//结束时间
		String endTime=getString("endTime");
		if(!"".equals(endTime)){
			maps.put("endTime", endTime+"23:59:59");
		}
		String searchOption=Const.getSearchTypeOptionByString(findBy,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name});
		maps.put("tick_flag", Const.Is_Yes);//是挂账
		maps.put("checkFlag", Const.Check_Yes);//已审核
		PageInfo<CommissionOrder> page=chargeAccountService.getChargeAccountPageInfo(maps,pages[0],pages[1],"");
		model.put("page", page);
		model.put("checkType", payFlag);
		model.put("Card_Certificate", Const.Card_Certificate);
		model.put("Card_Military", Const.Card_Military);
		model.put("Card_Passport", Const.Card_Passport);
        model.put("method", "list");
        model.put("searchOption", searchOption);
		return new ModelAndView("ye/chargeAccount/listChargeAccount",model);
		
    }
    /**
     * 删除选中挂账信息
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除挂账信息")
    @ResponseBody
    public JSONObject delChargeAccount(){
    	JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
	    	String[] ids=id.split(",");
	    	if(ids.length>=1){
	    		for(int i=0;i<ids.length;i++){
	    			Map<String,Object> paramMap = new HashMap<String, Object>();
	    			paramMap.put("commissionOrderId", ids[i]);
	    			paramMap.put("tickFlag", Const.Is_Yes);//加条件,只删除挂账的记录条
	    			chargeAccountService.delChargeAccont(paramMap);
	    		}
	    	}
//		    commissionOrderService.deleteCommissionOrder(id);
	    	
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			// TODO: handle exception
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
	 * 链接到挂帐详情页面(只显示)
	*/
	@RequestMapping(params = "method=show")
    public ModelAndView showChargeAccount(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		
//		ChargeAccount chargeAccount=new ChargeAccount();
		CommissionOrder commission=new CommissionOrder();
		if(id!=null&&!id.equals("")){
			commission=commissionOrderService.getCommissionOrderById(id);
		}
//		else {
//			List list=deadTypeService.getDeadTypeList(null);
//			if (list.size()>0) {
//				DeadType deadType=(DeadType)list.get(0);
//				deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId());
//			}
//		}	
		
		//服务项目
		List<Object> serviceList=new ArrayList<Object>();
		
		//丧葬用品
		List<Object> articlesList=new ArrayList<Object>();
		
		//根据业务编号查找
		Map<String,Object> conditionMaps=new HashMap<String,Object>();
		conditionMaps.put("commissionOrderId",id);
		conditionMaps.put("tickFlag", Const.Is_Yes);
		//根据火化委托单号找到相应的收费项目记录
		List<ItemOrder> list=chargeAccountService.getProjectFees(conditionMaps);
		double serviceSum=0,articlesSum=0;
		String itemId=null;
		for(int i=0;i<list.size();i++){
			Item item= itemService.getItemById(list.get(i).getItemId());
			if(item !=null){			
				double total=list.get(i).getTotal();
				int number=list.get(i).getNumber();
				String comment=list.get(i).getComment();
				//是否挂账   (此处无挂账标志时  显示为错误，若需要可以做个判断将空或其他值定为 否）
				String tickFlag=Const.getIs(list.get(i).getTickFlag());
				String typeId=item.getTypeId();
				ItemType type=itemTypeService.getItemTypeById(typeId);
				String typeName=type.getName();
				String name=item.getName();
				double pice=item.getPice();
			
			//如果是服务项目item.getBaseFlag()
				
			if(type.getType()==Const.Type_Service){
				Map<String,Object> serviceMaps=new HashMap<String,Object>();
				serviceMaps.put("itemId",itemId);
				serviceMaps.put("total",total);
				serviceMaps.put("number",number);
				serviceMaps.put("tickFlag",tickFlag);
				serviceMaps.put("comment", comment);
				serviceMaps.put("typeId",typeId);
				serviceMaps.put("name",name);
				serviceMaps.put("pice",pice);
				serviceMaps.put("typeName",typeName);
				serviceSum+=list.get(i).getTotal();
				serviceList.add(serviceMaps);
				//如果是丧葬用品
			}else if(type.getType()==Const.Type_Articles){
				Map<String,Object> articlesMaps=new HashMap<String,Object>();
				articlesMaps.put("itemId",itemId);
				articlesMaps.put("total",total);
				articlesMaps.put("number",number);
				articlesMaps.put("tickFlag",tickFlag);
				articlesMaps.put("comment",comment);
				articlesMaps.put("typeId",typeId);
				articlesMaps.put("name",name);
				articlesMaps.put("pice",pice);
				articlesMaps.put("typeName",typeName);
				articlesSum+=list.get(i).getTotal();
				articlesList.add(articlesMaps);
				
			}
			}
		}
		//服务项目列表、合计及丧葬用品列表、合计
		model.put("serviceSum",serviceSum);
		model.put("articlesSum",articlesSum);
		model.put("serviceList", serviceList);
		model.put("articlesList", articlesList);
		//挂账单位
		BillUnit billUnit=billUnitService.getBillUnitId(commission.getBillUnitId());
		//将选中的挂账基本信息返回并显示
		model.put("id",id);
		model.put("commission",commission);
		model.put("code",commission.getCode());
		model.put("serialNumber",commission.getCode()+"GZYW");
		model.put("name",commission.getName());
		if(billUnit!=null){
			model.put("billUnitName",billUnit.getName());			
		}
		model.put("certificateId",commission.getCertificateId());
		model.put("certificateCode",commission.getCertificateCode());
		return new ModelAndView("ye/chargeAccount/showChargeAccount",model);
    }
	
	/**
	 * 链接到挂帐详情页面(修改)
	*/
	@RequestMapping(params = "method=edit")
    public ModelAndView editChargeAccount(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		CommissionOrder commission=new CommissionOrder();
		String billUnitOption=billUnitService.getBillUnitOption(commission.getBillUnitId(), false);
		
		if(id!=null&&!id.equals("")){
			commission=commissionOrderService.getCommissionOrderById(id);
		}else {
//			List list=deadTypeService.getDeadTypeList(null);
//			if (list.size()>0) {
//				DeadType deadType=(DeadType)list.get(0);
//				deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId());
//			}
		}
		//服务项目
		List<Object[]> serviceList=new ArrayList<Object[]>();
		//丧葬用品
		List<Object[]> articlesList=new ArrayList<Object[]>();
		
		//根据业务编号查找
		Map<String,Object> conditionMaps=new HashMap<String,Object>();
		conditionMaps.put("commissionOrderId",id);
		//根据火化委托单号找到相应的收费项目记录
		List<ItemOrder> list=chargeAccountService.getProjectFees(conditionMaps);
		Map<String,Object> maps1=new HashMap<String,Object>();
		//收费类别条件
		Map<String,Object> typeMaps=new HashMap<String,Object>();
		
//		serviceMaps.put("defaultFlag", Const.Is_Yes);//是否默认服务
		
		for(int i=0;i<list.size();i++){
			String itemOrderId=list.get(i).getId();
			String itemId=itemOrderService.findItemOrderById(itemOrderId).getItemId();
			Item item= itemService.getItemById(itemId);
			if(item!=null){
				String typeId=item.getTypeId();
				ItemType type=itemTypeService.getItemTypeById(typeId);
				//服务项目
				if(item!=null && type.getType()==Const.Type_Service){
					typeMaps.put("type", Const.Type_Service);				
					//查询条件，类别
					maps1.put("typeId", item.getTypeId());
					//小计（单价*数量）
					int total=(int)list.get(i).getTotal();			
					//数量
					int number=list.get(i).getNumber();			
					//备注
					String comment=list.get(i).getComment();
					//是否挂账
					String tickFlagOption=Const.getIsOption(list.get(i).getTickFlag(),true);
					//名称（收费类别下的具体项目）
			    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
			    	//收费类别（冷藏柜，告别厅）
			    	String itemTypeOption=itemTypeService.getItemTypeOption(typeMaps,item.getTypeId(), false);
			    	//单价
			    	/*double pice=item.getPice();
					String name=item.getName();*/
					serviceList.add(new Object[]{itemOrderId,"service",itemOption,item.getPice(),itemTypeOption,number,tickFlagOption,total,comment});
				//丧葬用品
				}else if(item !=null && type.getType()==Const.Type_Articles){
					typeMaps.put("type", Const.Type_Articles);
					//查询条件，类别
					maps1.put("typeId", item.getTypeId());
					//小计（单价*数量）
					int total=(int)list.get(i).getTotal();			
					//数量
					int number=list.get(i).getNumber();			
					//备注
					String comment=list.get(i).getComment();
					//是否挂账
					String tickFlagOption=Const.getIsOption(list.get(i).getTickFlag(),true);
					//名称（收费类别下的具体项目）
			    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
			    	//收费类别（冷藏柜，告别厅）
			    	String itemTypeOption=itemTypeService.getItemTypeOption(typeMaps,item.getTypeId(), false);
			    	//单价
			    	/*double pice=item.getPice();
					String name=item.getName();*/
					articlesList.add(new Object[]{itemOrderId,"articles",itemOption,item.getPice(),itemTypeOption,number,tickFlagOption,total,comment});
				}
			}		
			
		}
		//作为增加收费的默认字段值(服务项目）
		String newId=UuidUtil.get32UUID();
		Map<String,Object> defaultMap=new HashMap<String,Object>();
		defaultMap.put("limit","1");
		defaultMap.put("baseFlag", Const.Type_Service);
		defaultMap.put("type", Const.Type_Service);
		List<Item> item= itemService.getItemList(defaultMap);
		typeMaps.put("type", Const.Type_Service);
		maps1.put("typeId", item.get(0).getTypeId());		
		
		String tickFlagOption=Const.getIsOption(Const.Is_Yes,true);
		String defaultNameOption=itemService.getItemOption(maps1,item.get(0).getId(),false);		
		String itemTypeOption=itemTypeService.getItemTypeOption(typeMaps,item.get(0).getTypeId(),false);
		double defaultPice=item.get(0).getPice();
		
		List<Object[]> defaultServiceList=new ArrayList<Object[]>();
		defaultServiceList.add(new Object[]{"service",defaultNameOption,defaultPice,itemTypeOption,1,tickFlagOption,defaultPice,null,newId});
		
		//作为增加收费的默认字段值(丧葬用品）		
		defaultMap.put("limit","1");
		defaultMap.put("baseFlag", Const.Type_Articles);
		defaultMap.put("type", Const.Type_Articles);
		item= itemService.getItemList(defaultMap);
		
		typeMaps.put("type", Const.Type_Articles);
		maps1.put("typeId", item.get(0).getTypeId());		
		
		tickFlagOption=Const.getIsOption(Const.Is_Yes,true);
		defaultNameOption=itemService.getItemOption(maps1,item.get(0).getId(),false);		
		itemTypeOption=itemTypeService.getItemTypeOption(typeMaps,item.get(0).getTypeId(),false);
		defaultPice=item.get(0).getPice();
		List<Object[]> defaultArticlesList=new ArrayList<Object[]>();
		defaultArticlesList.add(new Object[]{"articles",defaultNameOption,defaultPice,itemTypeOption,1,tickFlagOption,defaultPice,null,newId});
		
		
		model.put("serviceList", serviceList);
		model.put("articlesList", articlesList);
		model.put("defaultServiceList", defaultServiceList);
		model.put("defaultArticlesList", defaultArticlesList);
		if(commission.getCertificate()!=null){
			model.put("certificateId",commission.getCertificate().getName());			
		}
		
		
		//将选中的挂账基本信息返回并显示
		model.put("id",id);
		model.put("code",commission.getCode());
		model.put("serialNumber",commission.getCode()+"GZYW");
		model.put("name",commission.getName());
		model.put("billUnitOption",billUnitOption);
		
		model.put("certificateCode",commission.getCertificateCode());
		model.put("commission",commission);
		model.put("method", "edit");
		return new ModelAndView("ye/chargeAccount/editChargeAccount",model);
    }
	
	 /**
     * 项目类别改变
     * @return
     */
    @RequestMapping(params = "method=itemTypeChange")
    @ResponseBody
    public JSONObject itemTypeChange(){
		JSONObject json = new JSONObject();
    	Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("typeId", getString("id"));
		List<Item> list=itemService.getItemList(maps);
		Item item=new Item();
		if (list.size()>0) {
			item=list.get(0);
		}
    	String str=itemService.getItemOption(maps, "", false);
		json.put("pice", item.getPice());
		json.put("str", str);
		return json;
    }
    
    
    /**
     * 项目名称改变
     * @return
     */
    @RequestMapping(params = "method=itemNameChange")
    @ResponseBody
    public JSONObject itemNameChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
		json.put("pice", item.getPice());
		return json;
    }
	
	
	/**
     * 保存挂账信息
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存挂账信息")
    @ResponseBody
    public JSONObject saveCommissionOrder(){
		JSONObject json = new JSONObject();
    	try {
//    		JSONObject json2 = JSONObject.parseObject(getString("saveData"));
//    		
    		//需要删除的ID
    		String[] delNumber=getRequest().getParameterValues("delNumber");
    		//挂账单位名称
    		String commissionId=getString("commissionId");
    		//保存服务项目 
    		String[] serviceData=getRequest().getParameterValues("saveService");
    		//保存丧葬用品列表记录
    		String[] articlesData=getRequest().getParameterValues("saveArticles");
    		String billUnitId=getString("billUnitId");
    		
    		chargeAccountService.saveChargeAccount(delNumber,commissionId,serviceData,articlesData,billUnitId);
    		
    		
    	setJsonBySuccess(json, "保存成功","chargeAccount.do");
    	
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
			
		}
		return json;
    }
    
    
    /**
   	 * 添加服务项目（弹窗）
   	 * @return
   	 */
       @RequestMapping(params = "method=insertService")
       public ModelAndView insertService(){
       	Map<String,Object> model=new HashMap<String,Object>();
   	
   		//添加服务项目
   		List<Object[]> itemListAll=new ArrayList<Object[]>();
   		Map<String,Object> maps=new HashMap<String,Object>();
   		maps.put("type", Const.Type_Service);
   		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
   		for (int i = 0; i < itemTypeList.size(); i++) {
   			ItemType itemType=(ItemType)itemTypeList.get(i);
   			Map<String,Object> itemMaps=new HashMap<String,Object>();
   			itemMaps.put("typeId", itemType.getId());
   			List<Item> itemList=itemService.getItemList(itemMaps);
   			itemListAll.add(new Object[]{itemType,itemList});
   		}
   		model.put("itemTree", getTree(itemListAll)); 
   		model.put("type", "service");
       	return new ModelAndView("ye/chargeAccount/selectItems",model);
       }
    /**
  	 * 添加丧葬用品（弹窗）
  	 * @return
  	 */
      @RequestMapping(params = "method=insertArticles")
      public ModelAndView insertArticles(){
      	Map<String,Object> model=new HashMap<String,Object>();
  	
  		//添加丧葬用品
  		ArrayList<Object[]> itemListAll=new ArrayList<Object[]>();
  		Map<String,Object> maps=new HashMap<String,Object>();
  		maps.put("type", Const.Type_Articles);
  		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
  		for (int i = 0; i < itemTypeList.size(); i++) {
  			ItemType itemType=(ItemType)itemTypeList.get(i);
  			Map<String,Object> itemMaps=new HashMap<String,Object>();
  			itemMaps.put("typeId", itemType.getId());
  			List<Item> itemList=itemService.getItemList(itemMaps);
  			itemListAll.add(new Object[]{itemType,itemList});
  		}
  		model.put("itemTree", getTree(itemListAll)); 
  		model.put("type", "articles");
      	return new ModelAndView("ye/chargeAccount/selectItems",model);
      }
       
       /**
        * 弹窗中显示添加效果
        * @return
        */
       @RequestMapping(params = "method=addItems")
       @ResponseBody
       public List saveItemOrders(){
   		List list = new ArrayList();
   		String id =getString("ids");
   		String[] ids = id.split(",");
   		Map<String,Object> maps=new HashMap<String,Object>();
   		Map<String,Object> paramMap=new HashMap<String,Object>();
   	    for(int i=0;i<ids.length;i++){
   	    	if(!ids[i].equals("")){
   	    		Item item=itemService.getItemById(ids[i]);
   		    	maps.put("typeId", item.getTypeId());
   		    	String itemOption=itemService.getItemOption(maps,ids[i],false);
   		    	paramMap.put("type", Const.Type_Service);
   		    	String typeOption=itemTypeService.getItemTypeOption(paramMap, item.getTypeId(), false);
   		    	String isOption=Const.getIsOption(Const.Is_Yes, false);
   		    	list.add(new Object[]{itemOption,item.getPice(),typeOption,1,item.getPice(),isOption,"service"});
   	    	}	    	
   	    }
   		return list;
       }
       /**
        * 弹窗中显示添加效果(用品收费)
        * @return
        */
       @RequestMapping(params = "method=addItema")
       @ResponseBody
       public List saveItemOrdera(){
   		List list = new ArrayList();
   		String id =getString("ids");
   		String[] ids = id.split(",");
   		Map<String,Object> maps=new HashMap<String,Object>();
   		Map<String,Object> paramMap=new HashMap<String,Object>();
   	    for(int i=0;i<ids.length;i++){
   	    	if(!ids[i].equals("")){
   	    		Item item=itemService.getItemById(ids[i]);
   		    	maps.put("typeId", item.getTypeId());
   		    	String itemOption=itemService.getItemOption(maps,ids[i],false);
   		    	paramMap.put("type", Const.Type_Articles);
   		    	String typeOption=itemTypeService.getItemTypeOption(paramMap, item.getTypeId(), false);
   		    	String isOption=Const.getIsOption(Const.Is_Yes, false);
   		    	list.add(new Object[]{itemOption,item.getPice(),typeOption,1,item.getPice(),isOption,"articles"});
   	    	}	    	
   	    }
   		return list;
       }
       
       /**
        * 获得tree结构
        * @param list
        * @return
        */
       public String getTree(List list){
       	int sum=0;
       	String content="";
       	for (int i = 0; i < list.size(); i++) {
       		if (i>0) {
       			content+=",";
   			}
       		Object[] objects=(Object[]) list.get(i);
       		ItemType itemType=(ItemType)objects[0];
   	    	content+= "{";
   	    	content+="text:'"+itemType.getName()+"'";
//   	    	content+=",tags:['"+itemType.getId()+"']";
   	    	content+=",selectable: false";
   	    	List itemList=(List)objects[1];
   	    	content+=",nodes: [";
   	    	for (int j = 0; j < itemList.size(); j++) {
   	    		if (j>0) {
   	    			content+=",";
   				}
   	    		Item item=(Item)itemList.get(j);
   	    		content+="{";
   	    		content+="text:'"+item.getName()+"'";
   	    		content+=",tags:['"+item.getId()+"_"+sum+"']";
   	    		sum++;
   	    		content+="}";
   	    	}
   	    	content+="]";
   	    	content+="}";
       	}
       	return content;
       }

     
}
