package com.hz.controller.ye;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Appellation;
import com.hz.entity.base.Certificate;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.system.User;
import com.hz.entity.system.UserRole;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.service.base.AppellationService;
import com.hz.service.base.CertificateService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.ProveUnitService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.HardReductionDService;
import com.hz.service.ye.HardReductionService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 困难减免申请
 * @author jgj
 *
 */
@Controller
@RequestMapping("/hardReduction.do")
public class HardReductionController extends BaseController{
	//困难减免基本信息
	@Autowired
	private HardReductionService hardReductionService;
	//困难减免项目
	@Autowired
	private HardReductionDService hardReductionDService;
	//项目信息
	@Autowired
	private ItemService itemService;
	//火化委托单
	@Autowired
	private CommissionOrderService commissionOrderService;
	//与死者关系
	@Autowired
	private AppellationService appellationService;
	//证明单位
	@Autowired
	private ProveUnitService proveUnitService;
	//证件类型
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ItemTypeService itemTypeService;
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "hardReduction.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listHardReduction();
    }

	/**
	 * 困难减免申请列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listHardReduction(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);		
		//获得页面页码信息
		int[] pages1=getPage("1");
		int[] pages2=getPage("2");
		int[] pages3=getPage("3");
		int[] pages4=getPage("4");
		//获得当前登录者的ID
		String agentUserId=getCurrentUser().getUserId();
		//从页面获取查询数据
		String clickId=getString("clickId");
		String checkType=getString("checkType");
		String search=getString("search");
		String find=getString("find");
		String startTime=getString("startTime");
		String endTime=getString("endTime");
		//我的起草查询
		Map<String,Object> maps=new HashMap<String,Object>();
		String type="qicao";
		maps=checkType(type, checkType, maps, clickId,find,search,startTime,endTime);		
		maps.put("agentUserId", agentUserId);
		PageInfo<HardReduction> page=hardReductionService.getHardReductionPageInfo(maps,pages1[0],pages1[1],"create_time desc");
		model.put("page", page);
		model.put("page1", page);
		//待办事宜查询
		Map<String,Object> UpcomingMap=new HashMap<String,Object>();
		type="wait";
		UpcomingMap=checkType(type, checkType, UpcomingMap, clickId,find,search,startTime,endTime);		
		UpcomingMap.put("checkFlag", Const.Check_No);
		PageInfo<HardReduction> upcoming=hardReductionService.getHardReductionPageInfo(UpcomingMap,pages2[0],pages2[1],"create_time desc");
		model.put("upcoming",upcoming);
		model.put("page2", upcoming);
		//我的审批查询
		Map<String,Object> myMap=new HashMap<String,Object>();
		type="shenpi";
		myMap.put("checkUserId",agentUserId );
		myMap=checkType(type, checkType, myMap, clickId,find,search,startTime,endTime);
		PageInfo<HardReduction> myList=hardReductionService.getHardReductionPageInfo(myMap,pages3[0],pages3[1],"create_time desc");
		model.put("myList", myList);
		model.put("page3", myList);
		//查阅事宜查询
		Map<String,Object> lookMap=new HashMap<String,Object>();
		type="lookUp";
		lookMap=checkType(type, checkType, lookMap, clickId,find,search,startTime,endTime);
		String searchOption=Const.getSearchTypeOptionByString(find,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name});
		model.put("searchOption", searchOption);
		
		PageInfo<HardReduction> allList=hardReductionService.getHardReductionPageInfo(lookMap,pages4[0],pages4[1],"create_time desc");			
		model.put("allList", allList);
		model.put("page4", allList);
        model.put("method", "list");
        model.put("checkType", checkType);
        User user = getCurrentUser();
        for (UserRole ur : user.getRoles()) {
			if (("减免审批员").equals(ur.getRole().getName())) {
				model.put("isShenP", "yes");
			}
		}
		return new ModelAndView("ye/hardReduction/listHardReduction",model);
    }
    /**
     * 封装查询条件 
     * @param type
     * @param checkType
     * @param map
     * @param clickId
     * @return
     */
    public Map<String,Object> checkType(String type,String checkType,Map<String,Object> map,String clickId,String find,String search,
    		String startTime,String endTime){
    	
    	if(type.equals(clickId) && "checkNo".equals(checkType)){
    		map.put("checkFlag", Const.Check_No);
		}else if(type.equals(clickId) && "checkYes".equals(checkType)){
			map.put("checkFlag", Const.Check_Yes);
		}else if(type.equals(clickId) && "checkEsc".equals(checkType)){
			map.put("checkFlag", Const.Check_Esc);
		}else if(type.equals(clickId) && "checkAll".equals(checkType)){
			
		}else if(type.equals(clickId) && "checkSpecial".equals(checkType)){
			map.put("special", Const.Is_Yes);
		}
    	if(type.equals(clickId) && "2".equals(find) ){
    		map.put("findByCode",search);
    	}
    	if(type.equals(clickId) && "1".equals(find)){
    		map.put("findByName",search);
    	}
    	if(type.equals(clickId) && startTime!=null && endTime!=""){
    		map.put("startTime", startTime);
    	}
    	if(type.equals(clickId) && endTime!=null && endTime!=""){
    		map.put("endTime", endTime+"23:59:59");
    	}
    	return map;
    }
    
   
    /**
     * 点击姓名弹出困难减免详情(只显示)
     * @return
     */
    @RequestMapping(params ="method=show")
    public ModelAndView showHardReduction(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	//困难减免ID
    	String id=getString("id");
    	//困难减免信息
    	HardReduction hardReduction=hardReductionService.getHardReductionId(id);
    	String proveId=hardReduction.getProveComment();//证明单位ID
    	String proveUnitName="";
    	if(proveUnitService.getProveUnitId(proveId)!=null){
    		proveUnitName=proveUnitService.getProveUnitId(proveId).getName();//证明单位名称
    	}
    	String proveIds=hardReduction.getProveIds();
    	String[] prove=proveIds.split(",");
    	List<String> proveName=new ArrayList<String>();
    	for(int i=0;i<prove.length;i++){
    		Certificate certificate=certificateService.getCertificateId(prove[i]);
    		if(certificate !=null){
    			proveName.add(certificate.getName());
    		}
    	}
    	//困难减免项目详情
    	Map<String,Object> paramMap=new HashMap<String,Object>();
    	paramMap.put("hardReductionId", id);
    	List<HardReductionD> hardReductionDList=hardReductionDService.getHardReductionDList(paramMap);
    	double allTotal=0;//合计
    	//循环该困难减免的所有项目记录
    	List<Object> ReductionDList=new ArrayList<Object>();
    	for(HardReductionD hardReductionD:hardReductionDList){
    		String itemId=hardReductionD.getItemId();
    		Item item=itemService.getItemById(itemId);
    		if(item!=null){
	    		Map<String,Object> ReductionDMaps=new HashMap<String,Object>();
	    		ReductionDMaps.put("name", item.getName());
	    		ReductionDMaps.put("pice", item.getPice());
	    		ReductionDMaps.put("number",hardReductionD.getNumber());    		
	    		ReductionDMaps.put("comment",hardReductionD.getComment());
	    		double total=hardReductionD.getTotal();
	    		allTotal+=total;
	    		ReductionDMaps.put("total",total);
	    		ReductionDList.add(ReductionDMaps);
    		}
    	}
    	model.put("hardReduction", hardReduction);
    	model.put("ReductionDList", ReductionDList);
    	model.put("proveUnitName", proveUnitName);
    	model.put("allTotal", allTotal);
    	model.put("proveName", proveName);
        model.put("method", "show");
		return new ModelAndView("ye/hardReduction/showHardReduction",model);
    }
    /**
     * 将审核状态改变已审核
     * @return
     */
    @RequestMapping(params="method=pose")
    @SystemControllerLog(description = "修改困难减免审核状态为已审核")
    @ResponseBody
    public JSONObject pose(){
    	JSONObject json = new JSONObject();
    	try{
    		String userId=getCurrentUser().getUserId();
    		String poseIds=getString("allPoseIds");
    		String[] allPoseIds=poseIds.split(",");
    		long time=System.currentTimeMillis();
    		Timestamp checkTime=new Timestamp(time);
    		hardReductionService.changeCheckFlag(Const.Check_Yes,userId,checkTime,allPoseIds);
    		
    		setJsonBySuccess(json, "审批成功", "hardReduction.do");    			
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "审批失败，错误"+e.getMessage());
		}    	
    	return json;
    }
    
    /**
     * 将审核状态改变为被取消
     * @return
     */
    @RequestMapping(params="method=desc")
    @SystemControllerLog(description = "修改困难减免审核状态为被取消")
    @ResponseBody
    public JSONObject desc(){
    	JSONObject json = new JSONObject();
    	try{
    		String userId=getCurrentUser().getUserId();
    		String allDescIds=getString("allDescIds");
    		String[] descIds=allDescIds.split(",");
    		long time=System.currentTimeMillis();
    		Timestamp checkTime=new Timestamp(time);
    		hardReductionService.changeCheckFlag(Const.Check_Esc,userId,checkTime,descIds);
        	setJsonBySuccess(json, "取消成功", "hardReduction.do");
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "取消失败，错误"+e.getMessage());
		}    	
    	return json;
    }
    
	/**
	 * 链接到申请页面
	 * @return
	 */
    @RequestMapping(params = "method=add")
    public ModelAndView editHardReduction(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		HardReduction hardReduction=new HardReduction();
		if(id!=null&&!id.equals("")){
			hardReduction=hardReductionService.getHardReductionId(id);
		}
		//得个当前时间
		Timestamp creatTime=new Timestamp(System.currentTimeMillis());
		//得到所有火化委托单的姓名（数据大的时候，效率应该很差，待优化）
//		List<CommissionOrder> commissionList=commissionOrderService.getCommissionOrderList(null);
//		List dNames=new ArrayList();
//		List codes=new ArrayList();
//		for(int i=0;i<commissionList.size();i++){			
//			dNames.add(new Object[]{commissionList.get(i).getId(),commissionList.get(i).getName()});
//			codes.add(new Object[]{commissionList.get(i).getId(),commissionList.get(i).getCode()});
//		}
//		String nameOption=DataTools.getOptionByList(dNames, null, false);
//		String codeOption=DataTools.getOptionByList(codes, null, false);	
		
		
//		String proId=hardReduction.getProveComment();
//		String proveOption=null;
//		if(proId!=null&&!"".equals(proId)){
//			proveOption=proveUnitService.getProveUnitOption(proId, false);
//		}else{
		String proveOption=proveUnitService.getProveUnitOption(null, false);
			
//		}
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("type", "2");
		List<Certificate> certificateList=certificateService.getCertificateListBy(paramMap);
		//作为添加记录的默认数据
		Map<String,Object> defaultMap=new HashMap<String,Object>();
		defaultMap.put("hardFlag", Const.Is_Yes);
		defaultMap.put("limit", "1");
		List<Item> item= itemService.getItemList(defaultMap);
		double defaultPice=item.get(0).getPice();
		Map<String,Object> hardMap=new HashMap<String, Object>();
		hardMap.put("hardFlag", Const.Is_Yes); 
		List hardlist=itemService.getItemList(hardMap);
		String defaultNameOption=itemService.getItemOption(null,item.get(0).getId(),false);
		
		Map<String,Object> defaultAdd=new HashMap<String,Object>();
		defaultAdd.put("defaultPice", defaultPice);
		defaultAdd.put("defaultName", defaultNameOption);
		List hlist=new ArrayList();
		for (int i = 0; i < hardlist.size(); i++) {
			Item item2=(Item) hardlist.get(i);
			String hard_itemOption=itemService.getItemOption(null, item2.getId(), false);
			hlist.add(new Object[]{item2.getId(),hard_itemOption,1,item2.getPice(),""});
		}
		model.put("hardlist", hlist);		
//		model.put("nameOption", nameOption);
//		model.put("codeOption", codeOption);
        model.put("hardReduction", hardReduction);
        model.put("proveOption", proveOption);
        model.put("method", "add");
        model.put("creatTime", creatTime);
		model.put("defaultAdd", defaultAdd);
		model.put("certificateList", certificateList);
        return new ModelAndView("ye/hardReduction/addHardReduction",model);
    }
    
    /**
     * 根据死者姓名弹出相应信息
     * @return
     */
    @RequestMapping(params = "method=dNameSearch")
    @ResponseBody
    public JSONObject dNameChange(){
    	JSONObject json = new JSONObject();
    	//根据死者姓名，找对应的火化委托单
    	String dName=getString("dName");
    	Map<String,Object> paramMap=new HashMap<String, Object>();
    	paramMap.put("name", dName);
    	paramMap.put("checkFlag", Const.Check_Yes);//已审核
    	List<CommissionOrder> list=commissionOrderService.getCommissionOrderList(paramMap);
    	if(list.size()==1){
    		CommissionOrder commission=list.get(0);
    		//基本信息
    		String fPhone=commission.getfPhone();
    		String fName=commission.getfName();
    		String code=commission.getCode();
    		String cardCode=commission.getCardCode();
    		String sex=commission.getSexName();
    		int age=commission.getAge();
    		String name=commission.getName();
    		String dNameId=commission.getId();
    		String dieId=commission.getCertificateCode();
    		String proveUnitId=commission.getProveUnitId();
    		//与死者关系
    		String appellationId=commission.getfAppellationId();
    		List<Appellation> appellationList=appellationService.getAppellationList(null);
    		List list2 = new ArrayList();
    		for(int i=0;i<appellationList.size();i++){
    			list2.add(new Object[]{appellationList.get(i).getAppellationId(),appellationList.get(i).getName()});
    		}
    		String fAppellationOption=DataTools.getOptionByList(list2, appellationId, false);
    		//传值给前端json
    		json.put("dName", name);
    		json.put("dNameId", dNameId);
    		json.put("fAppellationOption", fAppellationOption);
    		json.put("code",code );
    		json.put("sex",sex );
    		json.put("cardCode",cardCode );
    		json.put("fName",fName );
    		json.put("fPhone",fPhone );
    		json.put("age", age);
    		json.put("dieId", dieId);
    		json.put("proveUnitId", proveUnitId);
    	}
    	json.put("size", list.size());   
		return json;
    }
    
    /**
	 *死者姓名筛选
	 * @return
	 */
    @RequestMapping(params = "method=dNameList")
    public ModelAndView dNameListbuyWreathRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	model=getModel(model);
    	//获得页面页码信息
//		int[] pages = getPage();
    	String dName = getString("dName");
    	Map<String,Object> maps = new HashMap<String,Object>();
    	maps.put("name", dName);
    	maps.put("checkFlag", Const.Check_Yes);//已审核
    	List<CommissionOrder> list = commissionOrderService.getCommissionOrderList(maps);
    	List list0 = new ArrayList();
    	for(int i=0;i<list.size();i++){
    		CommissionOrder commission=list.get(i);
    		//基本信息
    		String fPhone=commission.getfPhone();
    		String fName=commission.getfName();
    		String code=commission.getCode();
    		String cardCode=commission.getCardCode();
    		String sex=commission.getSexName();
    		int age=commission.getAge();
    		String name=commission.getName();
    		String dNameId=commission.getId();
    		String dieId=commission.getCertificateCode();
    		String proveUnitId=commission.getProveUnitId();
    		//与死者关系
    		String appellationId=commission.getfAppellationId();
    		List<Appellation> appellationList=appellationService.getAppellationList(null);
    		List list2 = new ArrayList();
    		for(int j=0;j<appellationList.size();j++){
    			list2.add(new Object[]{appellationList.get(j).getAppellationId(),appellationList.get(j).getName()});
    		}
    		String fAppellationOption=DataTools.getOptionByList(list2, appellationId, false);
    		
    		list0.add(new Object[]{
    				dNameId,name,age,sex,dieId,commission.getdAddr(),cardCode,fName,fPhone,fAppellationOption,proveUnitId
    		});
//    		String mourningPlace=null;
//    		String farewellPlace=null;
//	    	Date farewellDate=null;	    	
//	    	Date beginDate=null;
//	    	Date endDate=null;
	    	
//	    	list0.add(new Object[]{list.get(i).getId(),list.get(i).getName(),list.get(i).getArriveTime(),list.get(i).getCertificateCode(),list.get(i).getdAddr(),list.get(i).getAge(),
//	    			list.get(i).getSex(),mourningPlace,beginDate,endDate,farewellPlace,farewellDate});
    	}
//    	PageInfo page = new PageInfo(list0);
		model.put("page", list0);
  
    	return new ModelAndView("../../common/hardResult",model);
    }
    
    /**
     * 保存困难减免申请
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存困难减免申请信息")
    @ResponseBody
    public JSONObject saveHardReduction(){
		JSONObject json = new JSONObject();
    	try {    		
    		//获得当前登录者的ID
    		String userId=getCurrentUser().getUserId();
    		String dNameId=getString("dNameId");
    		String fName=getString("fName");
    		String phone=getString("phone");
    		String connect=getString("connect");
    		String other=getString("other");
    		byte spscial=getByte("spscial");
    		String certify=getString("certify");
    		String reason=getString("reason");
    		String remark=getString("remark");
    		String identifyType=getString("identifyType");
    		double sTotal=getDouble("sTotal");
    		Timestamp creatTime=getTimeM("creatTime");
    		String proveUnitContent=getString("proveUnitContent");
    		
    		long time=System.currentTimeMillis();
    		Timestamp checkTime=new Timestamp(time);
    		
    		HardReduction hardReduction=new HardReduction();
    		String hardId=UuidUtil.get32UUID();
			hardReduction.setId(hardId);
			hardReduction.setCommissionOrderId(dNameId);
			hardReduction.setProveIds(identifyType);
			hardReduction.setProveComment(certify);
			hardReduction.setReason(reason);
			hardReduction.setComment(other);
			//小于1200元，直接审核通过，大于则未审核
			if(sTotal>=1200){
				hardReduction.setCheckFlag(Const.Check_No);
			}else{
				hardReduction.setCheckFlag(Const.Check_Yes);
				hardReduction.setCheckTime(checkTime);
				hardReduction.setCheckUserId(userId);
			}
			hardReduction.setAgentUserId(userId);
			hardReduction.setCreateTime(creatTime);
			hardReduction.setApplicant(fName);
			hardReduction.setSpecial(spscial);
			hardReduction.setPhone(phone);
			hardReduction.setAppellationId(connect);
			hardReduction.setRemark(remark);
			
//			CommissionOrder comm=commissionOrderService.getCommissionOrderById(dNameId);
//			comm.setProveUnitId(certify);
//			comm.setProveUnitContent(proveUnitContent);
			
			String[] saveData=getRequest().getParameterValues("saveData");
			
			hardReductionService.saveHardReduction(hardReduction,saveData,hardId);
			
			
    		setJsonBySuccess(json, "保存成功", "hardReduction.do");
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
	 * 添加困难减免项目（弹窗）
	 * @return
	 */
    @RequestMapping(params = "method=insert")
    public ModelAndView insertbuyWreathRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
	
		//添加服务项目
		List itemListAll=new ArrayList();
//		Map maps=new HashMap();
//		maps.put("name","花圈花篮");
		List itemTypeList=itemTypeService.getItemTypeList(null);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			Map itemMaps=new HashMap();
			itemMaps.put("typeId", itemType.getId());
			List itemList=itemService.getItemList(itemMaps);
			itemListAll.add(new Object[]{itemType,itemList});
		}
		model.put("itemTree", getTree(itemListAll));  
    	return new ModelAndView("ye/hardReduction/selectItems",model);
    }
    
    /**
     * 添加花圈花篮
     * @return
     */
    @RequestMapping(params = "method=addItems")
    @ResponseBody
    public List saveItemOrder(){
		List list = new ArrayList();
		String id =getString("ids");
		String[] ids = id.split(",");		
//		Map maps=new HashMap();
	    for(int i=0;i<ids.length;i++){
	    	if(!ids[i].equals("")){
	    		Item item=itemService.getItemById(ids[i]);
//		    	maps.put("typeId", item.getTypeId());
		    	String itemOption=itemService.getItemOption(null,ids[i],false);
		    	list.add(new Object[]{itemOption,item.getPice(),1,item.getPice()});
	    	}	    	
	    }
		return list;
    }
    
    /**
     * 获得tree结构
     * @param list
     * @return
     */
    public String getTree(List list){
    	int sum=0;
    	String content="";
    	for (int i = 0; i < list.size(); i++) {
    		if (i>0) {
    			content+=",";
			}
    		Object[] objects=(Object[]) list.get(i);
    		ItemType itemType=(ItemType)objects[0];
	    	content+= "{";
	    	content+="text:'"+itemType.getName()+"'";
//	    	content+=",tags:['"+itemType.getId()+"']";
	    	content+=",selectable: false";
	    	List itemList=(List)objects[1];
	    	content+=",nodes: [";
	    	for (int j = 0; j < itemList.size(); j++) {
	    		if (j>0) {
	    			content+=",";
				}
	    		Item item=(Item)itemList.get(j);
	    		content+="{";
	    		content+="text:'"+item.getName()+"'";
	    		content+=",tags:['"+item.getId()+"_"+sum+"']";
	    		sum++;
	    		content+="}";
	    	}
	    	content+="]";
	    	content+="}";
    	}
    	return content;
    }
    
}
