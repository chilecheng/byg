package com.hz.controller.ye;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.system.User;
import com.hz.entity.ye.FireAttestPrintInfo;
import com.hz.entity.ye.PincardRecord;
import com.hz.entity.ye.PrintRecord;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FireAttestPrintService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 三科火化证明打印
 * @author lpp
 *
 */
@Controller
@RequestMapping("/fireAttestPrint.do")
public class FireAttestPrintController extends BaseController{
	
	@Autowired
	private FireAttestPrintService fireAttestPrintService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "fireAttestPrint.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return new ModelAndView("ye/third_department/cremationAttest/searchPage",null);	
	}
	
	@RequestMapping(params = "method=judge")
	@ResponseBody
	public JSONObject  judge()
	{
		JSONObject json = new JSONObject();
		String cardCode=getString("readCard");
		
		FireAttestPrintInfo finfo=fireAttestPrintService.getBasicInfoByCard(cardCode);
		
		if(finfo==null)
		{
			setJsonByFail(json,"没有对应的卡号");		
		}
		else if(finfo.getPincardFlag()==Const.pincard_Yes)
		{
			setJsonByFail(json,"已销卡");
		}
		else if(finfo.getCheckFlag()==Const.Check_No)
		{
			setJsonByFail(json,"该卡尚未审核");
		}
		else if(finfo.getCremationFlag()==Const.Is_No
				||fireAttestPrintService.getActualFireTimeByCId(finfo.getCid())==null)//"未火化"
		{
			setJsonByFail(json,"该死者未火化");		
		}
		else
		{
			setJsonBySuccess(json,"已火化,可以打印","fireAttestPrint.do?method=list&cardCode="+cardCode);
		}
		
		return json;
		
	}
	
	@RequestMapping(params = "method=list")
	public ModelAndView showCheckCard()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		String cardCode=getString("cardCode");
		
		FireAttestPrintInfo finfo=fireAttestPrintService.getBasicInfoByCard(cardCode);
		if(finfo!=null)
		{
			model.put("id", finfo.getCid());
			model.put("age", finfo.getAge());
			model.put("dname", finfo.getDname());
			model.put("sex", finfo.getSexName());
			//已火化，一定有值
			String begin_time=fireAttestPrintService.getActualFireTimeByCId(finfo.getCid());
			model.put("begin_time", Timestamp.valueOf(begin_time));
			model.put("certificateCode", finfo.getCertificateCode());
			model.put("business_state", finfo.getBusinessState());
			model.put("schedulFlag", finfo.getScheduleFlag());
			model.put("cardCode", cardCode);
		}
		
		return new ModelAndView("ye/third_department/cremationAttest/printCremation",model);
	}
	/**
	 * 领取人录入
	 * @return
	 */
	@RequestMapping(params = "method=receiveLogin")
	public ModelAndView receiveLogin()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		String id=getString("id");		
		String sex=getString("sex");		
		String dname=getString("dname");		
		String certificateCode=getString("certificateCode");		
		String cardCode=getString("cardCode");
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("cid", id);
		PrintRecord printRecord=new PrintRecord();
		List<PrintRecord> list=fireAttestPrintService.getPrintRecordByMap(map);
		if(list.size()>0){
			printRecord=list.get(0);			
		}
		
		model.put("id", id);
		model.put("sex", sex);
		model.put("dname",dname);
		model.put("certificateCode",certificateCode);
		model.put("cardCode", cardCode);
		model.put("printRecord", printRecord);
		String pageFlag=getString("pageFlag");//1代表printCremation页面，2代表printReceiveCremation页面
		
		model.put("pageFlag", pageFlag);
		return new ModelAndView("ye/third_department/cremationAttest/receiptor",model);
	}
	
	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "三科火化证明保存")
	public ModelAndView save()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		PrintRecord printRecord=new PrintRecord();
		String printId=getString("printId");
		String id=getString("id");
		String receiptor=getString("receiptor");
		String receiptorId=getString("receiptorId");
		
		if(printId!=null && !"".equals(printId)){
			printRecord=fireAttestPrintService.getPrintRecordById(printId);
		}else{
			printRecord.setId(UuidUtil.get32UUID());
		}
		
		printRecord.setCid(id);
		printRecord.setTakeName(receiptor);
		printRecord.setTakeCode(receiptorId);
		User u = getCurrentUser();
		printRecord.setUserId(u.getUserId());
		printRecord.setPrintTime(DateTools.dateToTimestamp(new java.util.Date()));
		
		if(printId!=null && !"".equals(printId)){
			fireAttestPrintService.updatePrintRecord(printRecord);
		}else{
			fireAttestPrintService.addPrintRecord(printRecord);
		}
		
		String cardCode=getString("cardCode");
		
		FireAttestPrintInfo finfo=fireAttestPrintService.getBasicInfoByCard(cardCode);
		Date now=DateTools.getThisDate();
		String userName=getCurrentUser().getName();//签发人
		
		
		
		if(finfo!=null)
		{
			model.put("id", finfo.getCid());
			model.put("age", finfo.getAge());
			model.put("dname", finfo.getDname());
			model.put("sex", finfo.getSexName());
			//已火化，一定有值
			String begin_time=fireAttestPrintService.getActualFireTimeByCId(finfo.getCid());
			model.put("begin_time", Timestamp.valueOf(begin_time));
			model.put("certificateCode", finfo.getCertificateCode());
			model.put("business_state", finfo.getBusinessState());
			model.put("cardCode", cardCode);
			model.put("receiptor", receiptor);
			model.put("receiptorId", receiptorId);
			model.put("userName", userName);
			model.put("now", now);
			model.put("schedulFlag", finfo.getScheduleFlag());
		
		}
		
		return new ModelAndView("ye/third_department/cremationAttest/printReceiveCremation",model);
	}
	
	@RequestMapping(params = "method=back")
	public ModelAndView back()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		String pageFlag=getString("pageFlag");//1代表printCremation页面，2代表printReceiveCremation页面
		String cardCode=getString("cardCode");
		
		FireAttestPrintInfo finfo=fireAttestPrintService.getBasicInfoByCard(cardCode);
		String receiptor=getString("receiptor");
		String receiptorId=getString("receiptorId");
		
		if(finfo!=null)
		{
			model.put("id", finfo.getCid());
			model.put("age", finfo.getAge());
			model.put("dname", finfo.getDname());
			model.put("sex", finfo.getSexName());
			Date now=DateTools.getThisDate();
			String userName=getCurrentUser().getName();//签发人
			//已火化，一定有值
			String begin_time=fireAttestPrintService.getActualFireTimeByCId(finfo.getCid());
			model.put("begin_time", Timestamp.valueOf(begin_time));
			model.put("certificateCode", finfo.getCertificateCode());
			model.put("business_state", finfo.getBusinessState());
			model.put("cardCode", cardCode);
			model.put("receiptor", receiptor);
			model.put("receiptorId", receiptorId);
			model.put("userName", userName);
			model.put("now", now);
			model.put("schedulFlag", finfo.getScheduleFlag());
		}
		if(pageFlag.equals("1")){
			return new ModelAndView("ye/third_department/cremationAttest/printCremation",model);
		}else{//pageFlag=2
			return new ModelAndView("ye/third_department/cremationAttest/printReceiveCremation",model);
		}
	}
	
	
	@RequestMapping(params = "method=destoryCard")
	@SystemControllerLog(description = "三科火化证明销卡")
	@ResponseBody
	public JSONObject destoryCard()
	{
		JSONObject json = new JSONObject();
		String id = getString("id");
		
		try
		{
			PincardRecord pinCard=new PincardRecord();
			pinCard.setId(UuidUtil.get32UUID());
			pinCard.setCommissionOrderId(id);
			pinCard.setCreatTime(DateTools.getThisDate());
			pinCard.setCreatUserId(getCurrentUser().getUserId());
			String cardId=commissionOrderService.getCardCodeById(id);
			pinCard.setOldCard(cardId);
			
			fireAttestPrintService.delCard(pinCard,id);
			json.put("info", "销卡成功");
		}
		catch(Exception e)
		{
			json.put("info", "销卡异常");
			e.printStackTrace();
		}
		return json;
	}
	
	@RequestMapping(params = "method=showInfo")
	public ModelAndView showInfo()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		String id=getString("id");
		String sex= getString("sex");
		String dname= getString("dname");
		String certificateCode= getString("certificateCode");
		
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("cid", id);
		PrintRecord printRecord=new PrintRecord();
		List<PrintRecord> list=fireAttestPrintService.getPrintRecordByMap(map);
		if(list.size()>0){
			printRecord=list.get(0);			
		}
		
		model.put("printRecord", printRecord);
		model.put("sex", sex);
		model.put("dname", dname);
		model.put("certificateCode", certificateCode);
		
		return new ModelAndView("ye/third_department/cremationAttest/charReceiptor",model);
	}
	/**
	 *   ajax  更新打印状态
	 */
		@RequestMapping(params="method=upSchedual")
		@SystemControllerLog(description = "三科火化证明更新打印状态")
		@ResponseBody
		public JSONObject upSchedual(){
			JSONObject json = new JSONObject();
			String id = getString("id");
			commissionOrderService.updateCommissionOrderScheduleFlag(id, Const.schedule_flag_4);
			/*json.put("isRefresh", true);*/
			return json;
		}
	
	
}
