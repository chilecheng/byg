package com.hz.controller.ye;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Farewell;
import com.hz.entity.system.User;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.ListFueralRecord;
import com.hz.entity.ye.MourningRecord;
import com.hz.service.base.FarewellService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.system.UserService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.FuneralRecordService;
import com.hz.service.ye.ItemOrderService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 告别厅记录
 * @author cjb
 */
@Controller
@RequestMapping("/farewellRecord.do")
public class FarewellRecordController extends BaseController{
	@Autowired
	private FarewellRecordService farewellRecordService;
	@Autowired
	private MourningRecordService mourningRecordService;
	@Autowired
	private FarewellService farewellService;
	@Autowired
	private CommissionOrderService cs;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	private ItemTypeService itemTypeService;
	@Autowired
	private UserService userService;
	@Autowired
	private FuneralRecordService funeralRecordService;
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "farewellRecord.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listFarewellRecord();
    }

	/**
	 * 告别厅记录列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFarewellRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//预设当前日期
		Date date = DateTools.getThisDateUtil();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date = getDate("date");
		}
		model.put("date", date);
		List<Farewell> fList = farewellService.getFarewellList(null,"");//获得所有的告别厅
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		List<FarewellRecord> fRList=farewellRecordService.getFarewellRecordList(maps,"begin_date asc");//获得所有的告别厅记录
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[14];
		dOb[0]="告别厅";
		//将时间分成需要的 时间段  并装入数组 
		for (int i = 0; i < 13; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
		}
		list.add(dOb);
		//设置默认列表数组             告别厅个数    每个告别厅放入一个Object数组
		for(int i=0;i<fList.size();i++) {
			Farewell farewell = fList.get(i);
			list.add(new Object[]{farewell,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历 所有的   告别厅记录条数
		for (FarewellRecord fr : fRList) {
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;
			//对比告别厅，确认行数
			for (int i = 1; i < list.size(); i++) {
				Object[] ob = list.get(i);
				Farewell farewell = (Farewell) ob[0];
				if(farewell.getId().equals(fr.getFarewellId())) {
					row = i;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if(DateTools.isSameHour(da, new Date(fr.getBeginDate().getTime()))) {
					col = 2*(d-1)+ifHalft(fr.getBeginDate());
				}
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					if(DateTools.isSameHour(da, new Date(fr.getEndDate().getTime()))){
						endCol = (d-1)*2+ifHalft(fr.getEndDate());
					}
				}
			}
			if (row != 0) {
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					Object[] o = (Object[])list.get(row);
					if(endCol==0){
						endCol = 28;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						o[x] = fr;
					}
				} else {
					if(row!=0){
						Object[] o = (Object[])list.get(row);
						if (col != 0) {
							o[col] = fr;
						}
					}
				}
			}
		}
		
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Bzwc", Const.IsFlag_Bzwc);
		model.put("method", list);
		model.put("list", list);
		return new ModelAndView("ye/farewellRecord/listFarewellRecord",model);
    }
    
    /**
	 * 告别厅记录
	 * @return
	 */
    @RequestMapping(params = "method=listInfo")
    public ModelAndView editFarewellRecord1(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		String type = getString("type");
		String searchType = getString("searchType");
		Map<String, Object> maps=new HashMap<String, Object>();
		Date beginDate = getDate("beginDate");
		Date endDate = getDate("endDate");
		if (beginDate != null && endDate!= null) {
			maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(beginDate.getTime())));
			maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(endDate.getTime())));
		}
		if (("5".equals(type))) {
			beginDate = DateTools.getThisDate();
			endDate = DateTools.getDayAfterDay(new java.sql.Date(beginDate.getTime()), 1, Calendar.DATE);
			maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(beginDate.getTime())));
			maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(endDate.getTime())));
		} else if ("2".equals(type)) {
			int half = ifHalft(DateTools.dateToTimestamp(new Date()));
			beginDate = getBeginDate(half);
			endDate = getEndDate(half);
			maps.put("beginDate", DateTools.dateToTimestamp(beginDate));
			maps.put("endDate", DateTools.dateToTimestamp(endDate));
		} else if ("3".equals(type)) {
			int half = ifHalft(DateTools.dateToTimestamp(new Date()));
			Date untilDate = getUntilDate(half);
			maps.put("untilDate", DateTools.dateToTimestamp(untilDate));
		} else if("1".equals(type)) {
			maps.put("isArrange", Const.IsFlag_Yse);
			maps.put("NoArrange", Const.IsFlag_Yse);
		}
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Name,Const.Search_Type_Card});
		maps.put("searchVal", getString("searchVal"));
		maps.put("searchType", searchType);
		maps.put("comm", "yes");
		maps.put("checkFlag", Const.Check_Yes);
		PageInfo<FarewellRecord> page=farewellRecordService.getFarewellRecordPageInfo(maps, pages[0],pages[1], "creat_time desc");
		model.put("searchOption", searchOption);
		model.remove("method");
		model.put("method","listInfo");
		model.put("page", page);
		model.put("checkType", type);
		model.put("IsFlag_Bzwc",Const.IsFlag_Bzwc);
		model.put("IsFlag_Yse",Const.IsFlag_Yse);
		return new ModelAndView("ye/farewellRecord/listFarewell",model);
    }
	
    
	/**
     * 死者告别厅使用详情
     * @return
     */
    @RequestMapping(params = "method=detail")
    public ModelAndView userDetail(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String id = getString("id");
		CommissionOrder co = cs.getCommissionOrderById(id);
		MourningRecord mr = mourningRecordService.getMourningRecord(id);
		Map<String,Object> paramMap = new HashMap<String, Object>();
		String type = Const.getItemType(getString("type"));
		String typeId = itemTypeService.getIdByTypeName(type);
		paramMap.put("typeId", typeId);
		paramMap.put("cid", id);
		List<ItemOrder> list = itemOrderService.getItemOrderListByType(paramMap);
		String enterId = getString("enterId");
		if (enterId != null && !(enterId.equals(""))) {
			String enterUser = userService.getNameById(enterId);
			model.put("enterUser",enterUser);
		}
		model.put("co",co);
		model.put("mr",mr);
		model.put("list",list);
		try {
			model.put("gbTime",getTime("gbTime"));
			model.put("arrTime",getTime("arrTime"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("ye/farewellRecord/userFarewellDetail",model);
    }

	/**
	 * 链接到布置页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFarewellRecord(){
		Map<String,Object> model=new HashMap<String,Object>();
		model = getModel(model);
		String id = getString("id");
//		String homeType=getString("homeType");
		FarewellRecord farewellRecord = new FarewellRecord();
		CommissionOrder commissionOrder = new CommissionOrder();
		Integer flag = null;//是否布置完成标志
		if(id!=null&&!id.equals("")){
 			farewellRecord=farewellRecordService.getFarewellRecordByOrderId(id);
			//flag 为7 表示布置完成，其他则为 未布置
			if (farewellRecord.getFlag() == 7) {
				flag = 1;
			} else {
				flag = 0;
			}
			commissionOrder=cs.getCommissionOrderById(id);
			
			//首页列表记录查看状态
			if(commissionOrder.getViewsFarewell()==0){
				commissionOrder.setViewsFarewell(Const.One);
				cs.updateNumber(commissionOrder);
			}
			
		}
		Map<String,Object> paramMap = new HashMap<String, Object>();
		String type = Const.getItemType(getString("type"));
		String typeId = itemTypeService.getIdByTypeName(type);
		paramMap.put("typeId", typeId);
		paramMap.put("cid", id);
		List<ItemOrder> list = itemOrderService.getItemOrderListByType(paramMap);
		List<User> groupUserList = userService.getUserListByRoleName(Const.ARRANGER);
		List<User> emceeGroupList = userService.getUserListByRoleName(Const.EMCEE);
		model.put("farewellRecord", farewellRecord);
        model.put("commissionOrder", commissionOrder);
        model.put("groupUserList", groupUserList);
        model.put("method", "arrange");
        model.put("list",list);
//        model.put("homeType", homeType);
        model.put("flag",flag);
        model.put("user",getSession().getAttribute(Const.SESSION_USER));
        model.put("id",id);
        model.put("emceeGroupList", emceeGroupList);
		return new ModelAndView("ye/farewellRecord/editFarewellRecord",model);
    }
    /**
     * 布置告别厅信息
     * @return
     */
    @RequestMapping(params = "method=arrange")
    @SystemControllerLog(description = "布置告别厅信息")
    @ResponseBody
    public JSONObject arrangeFarewell(){
		JSONObject json = new JSONObject();
    	try {
    		String[] handlerIds = getArrayString("handler");
    		String[] comments = getArrayString("comment");
    		String[] orderIds = getArrayString("oderId");
    		String id = getString("id");
    		FarewellRecord farewellRecord = farewellRecordService.getFarewellRecordByOrderId(id);
    		farewellRecord.setEmceeId(getString("emceeId"));
    		farewellRecord.setChargeFee(getDouble("chargeFee"));
    		farewellRecord.setEnterId(getString("enterId"));
    		farewellRecord.setArrangeTime(getTimeM("arrangeTime"));
    		farewellRecord.setComment(getString("fcomment"));
    		farewellRecord.setFlag(Const.IsFlag_Bzwc);
    		farewellRecordService.arrangeFarewell(handlerIds,comments,orderIds,farewellRecord);
    		setJsonBySuccess(json, "布置完成", true);
    		json.put("refresh", getSession().getAttribute("refresh"));
    		String noRefresh = getString("noRef");
			if (noRefresh!=null && !noRefresh.equals("")) {
				json.put("noRefresh", true);
			}
		} catch (Exception e) {
			setJsonByFail(json, "布置失败，错误"+e.getMessage());
			e.printStackTrace();
		}
		return json;
    }
    /**
     * 保存告别厅记录
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存告别厅记录")
    @ResponseBody
    public JSONObject saveFarewellRecord(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		FarewellRecord farewellRecord=new FarewellRecord();
    		if (id!=null&&!id.equals("")) {
    			farewellRecord=farewellRecordService.getFarewellRecordId(id);
    		}
    		farewellRecord.setFarewellId(getString("farewellId"));
			farewellRecord.setCreateUserId(getString("createUserId"));
			farewellRecord.setCreateTime(DateTools.dateToTimestamp(getTime("createTime")));
			farewellRecord.setCommissionOrderId(getString("commissionOrderId"));
    		if (id==null||id.equals("")) {
    			farewellRecord.setId(UuidUtil.get32UUID());
    			farewellRecord.setFlag(Const.IsFlag_No);
    			farewellRecordService.addFarewellRecord(farewellRecord);
    		}else {
    			farewellRecordService.updateFarewellRecord(farewellRecord);
    		}
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
     * 判断是过半还是没有过半
     * @param time
     * @return 1：上半时 2：下半时
     */
    public byte ifHalft(Timestamp time) {
    	//时间转换对象
    	Calendar cal=Calendar.getInstance();
    	//设置时间
    	cal.setTime(new Date(time.getTime()));
    	int min=cal.get(Calendar.MINUTE);
    	if(min>=0 && min<30){
    		return (byte)1;
    	}else{
    		return (byte)2;
    	}
    }
    /**
     * 获取当前时间点的开始时间
     * @param i
     * @return
     */
    private Date getBeginDate(int i) {
    	Date beginDate = new Date();
    	Calendar ca = Calendar.getInstance();
    	ca.setTime(beginDate);
    	if (i == 1) {
    		ca.set(Calendar.MINUTE, 0);
    		ca.set(Calendar.SECOND, 0);
		} else if (i == 2) {
			ca.set(Calendar.MINUTE, 30);
			ca.set(Calendar.SECOND, 0);
		}
    	return ca.getTime();
    }
    /**
     * 获取当前时间点的结束时间
     * @param i
     * @return
     */
    private Date getEndDate(int i) {
    	Date beginDate = new Date();
    	Calendar ca = Calendar.getInstance();
    	ca.setTime(beginDate);
    	if (i == 1) {
    		ca.set(Calendar.MINUTE, 30);
    		ca.set(Calendar.SECOND, 0);
    		ca.set(Calendar.MILLISECOND, 000);
		} else if (i == 2) {
			ca.add(Calendar.HOUR_OF_DAY, 1);
			ca.set(Calendar.MINUTE, 0);
			ca.set(Calendar.SECOND, 0);
			ca.set(Calendar.MILLISECOND, 000);
		}
    	return ca.getTime();
    }
    /**
     * 获取当前时间之前的最晚时间
     * @param half
     * @return
     */
    private Date getUntilDate(int half) {
    	Date beginDate = new Date();
    	Calendar ca = Calendar.getInstance();
    	ca.setTime(beginDate);
    	if (half == 1) {
    		ca.set(Calendar.MINUTE, 0);
    		ca.set(Calendar.SECOND, 0);
    		ca.set(Calendar.MILLISECOND, 000);
		} else if (half == 2) {
			ca.set(Calendar.MINUTE, 30);
			ca.set(Calendar.SECOND, 0);
			ca.set(Calendar.MILLISECOND, 000);
		}
    	return ca.getTime();
	}

}
