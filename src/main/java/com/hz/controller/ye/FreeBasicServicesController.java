package com.hz.controller.ye;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Appellation;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.base.AppellationService;
import com.hz.service.base.FreePersonService;
import com.hz.service.base.HardPersonService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.ye.BaseReductionDService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FreeBasicServicesService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;
/**
 * 基本服务减免申请
 * @author jgj
 *
 */
@Controller
@RequestMapping("/freeBasicServices.do")
public class FreeBasicServicesController extends BaseController{
	@Autowired
	private ItemTypeService itemTypeService;
	@Autowired
	private FreeBasicServicesService freeBasicService;
	//火化委托单
	@Autowired
	private CommissionOrderService  commissionOrderService;
	//基本减免申请项目
	@Autowired
	private BaseReductionDService baseReduuctionDService;	
	//收费项目
	@Autowired
	private ItemService itemService;
	//免费对象类别
	@Autowired
	private FreePersonService freePersonService;
	//重点救助对象类别
	@Autowired
	private HardPersonService hardPersonService;
	//与死者关系
	@Autowired
	private AppellationService appellationService;
	
	
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "freeBasicServices.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listFreeBasicServices();
    }
	
	
	/**
	 * 获取基本服务减免申请列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFreeBasicServices() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String,Object>();
		String findBy=getString("find");
		maps.put("findBy", findBy);
		if("1".equals(findBy)){//姓名
			String findByName=getString("search");
			maps.put("findByName", findByName);
		}
		if("2".equals(findBy)){//卡号
			String findByCode=getString("search");
			maps.put("findByCode", findByCode);
		}
		if("17".equals(findBy)){//地区
			String findByArea=getString("search");
			maps.put("findByArea", findByArea);
		}
		//开始时间
		String startTime=getString("startTime");
		if(!"".equals(startTime)){
			maps.put("startTime", startTime);
		}
		//结束时间
		String endTime=getString("endTime");
		if(!"".equals(endTime)){
			maps.put("endTime", endTime);
		}		
		String searchOption=Const.getSearchTypeOptionByString(findBy,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_Area});
		
		maps.put("dName", "");
		PageInfo<BaseReduction> page=freeBasicService.getFreeBasicServicesPageInfo(maps,pages[0],pages[1],"pay_time desc");
		model.put("page", page);
        model.put("method", "list");
        model.put("searchOption", searchOption);
		return new ModelAndView("ye/freeBasicServices/listFreeBasicServices",model);		
    }
    
    /**
     * 链接到减免申请详情（只显示）
     * @return
     */
    @RequestMapping(params = "method=show")
    public ModelAndView showFreeBasicServices() {
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//申请记录ID
		String id = getString("id");
		BaseReduction baseReduction=freeBasicService.getFreeBasicById(id);
		//免费对象类别
		String freePersonAll=baseReduction.getFreePersonIds();
		String[] free=freePersonAll.split(",");
		String freePerson="";
		for(int i=0;i<free.length;i++){
			if(!"".equals(free[i])){
				String name=freePersonService.getFreePersonById(free[i]).getName();
				freePerson+=name+" &nbsp; ";
			}
		}
		//重点救助对象类别
		String hardPersonAll=baseReduction.getHardPersonIds();
		String[] hard=hardPersonAll.split(",");
		String hardPerson="";
		for(String hardId :hard){
			String name="";
			if(hardPersonService.getHardPersonById(hardId)!=null){
				name=hardPersonService.getHardPersonById(hardId).getName();
				hardPerson+=name+" &nbsp; ";
			}
		}
		//获取火化委托单实体类
		CommissionOrder commission=commissionOrderService.getCommissionOrderById(baseReduction.getCommissionOrderId());
		String appellationId="";
		String appellationName="";
		if(commission !=null){
			//与死者关系
			appellationId=commission.getfAppellationId();
		}
		if(appellationId!=null && appellationId!=""){
			appellationName=appellationService.getAppellationById(appellationId).getName();			
		}
		//减免详情
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("baseReductionId", id);
		//根据申请id在减免申请详情表中找到所有对应的记录，返回list
		List list=baseReduuctionDService.getBaseReductionDList(maps);
		double allTotal=0;//合计总价
		List ReductionDList=new ArrayList();
		if(list.size()>0){			
			for(int i=0;i<list.size();i++){
				//循环这些记录，并按记录的Item_id找ba_item表的相关记录
				BaseReductionD baseReductionD=(BaseReductionD)list.get(i);
				int number=baseReductionD.getNumber();
				double total=baseReductionD.getTotal();
				String comment=baseReductionD.getComment();
				//ba_item表中的数据 
				String itemId=baseReductionD.getItemId();
				String name=itemService.getItemById(itemId).getName();
				double pice=itemService.getItemById(itemId).getPice();
				allTotal+=total;
				Map<String,Object> ReductionDMaps=new HashMap<String,Object>();
				ReductionDMaps.put("number", number);
				ReductionDMaps.put("total", total);
				ReductionDMaps.put("comment", comment);
				ReductionDMaps.put("name", name);
				ReductionDMaps.put("pice", pice);
				ReductionDList.add(ReductionDMaps);
			}			
		}
		model.put("ReductionDList",ReductionDList);
		model.put("allTotal",allTotal);
		model.put("freePerson", freePerson);
		model.put("hardPerson",hardPerson);
		model.put("commission", commission);
		model.put("appellationName", appellationName);
    	return new ModelAndView("ye/freeBasicServices/showFreeBasicServices",model);
    }
    
    /**
	 * 链接到添加页面
	 * @return
	 */
    @RequestMapping(params = "method=add")
    public ModelAndView addFreeBasicServices() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		
		AshesRecord ashesRecord = new AshesRecord();
		
//		List<CommissionOrder> commissionList=commissionOrderService.getCommissionOrderList(null);
//		List dNames=new ArrayList();
//		for(int i=0;i<commissionList.size();i++){
//			
//			dNames.add(new Object[]{commissionList.get(i).getId(),commissionList.get(i).getName()});
//		}
//		String nameOption=DataTools.getOptionByList(dNames, null, false);
		
		Map<String,Object> baseMap=new HashMap<String, Object>();
		baseMap.put("baseFlag", Const.Is_Yes); 
		List<Item> baseList=itemService.getItemList(baseMap);
		List<Object> list=new ArrayList<Object>();
		for (int i = 0; i < baseList.size(); i++) {
			Item item2=(Item) baseList.get(i);
			String hard_itemOption=itemService.getItemOption(null, item2.getId(), false);
			list.add(new Object[]{item2.getId(),hard_itemOption,1,item2.getPice(),""});
		}
		
		
		String fAppellationOption=appellationService.getAppellationOption(ashesRecord.getfAppellationId(),false);
		//免费对象类别
		List freePersonList=freePersonService.getFreePersonList(null);
		//重点救助对象类别
		List hardPersonList=hardPersonService.getHardPersonList(null);
		//作为添加记录的默认数据
		Map defaultMap=new HashMap();
		defaultMap.put("limit", "1");
		List<Item> item= itemService.getItemList(defaultMap);
		double defaultPice=item.get(0).getPice();
		String defaultNameOption=itemService.getItemOption(new HashMap(),item.get(0).getId(),false);
		Map<String,Object> defaultAdd=new HashMap<String,Object>();
		defaultAdd.put("defaultPice", defaultPice);
		defaultAdd.put("defaultName", defaultNameOption);
		
		model.put("fAppellationOption", fAppellationOption);
		model.put("baseList", list);
        model.put("method", "add");
//        model.put("nameOption",nameOption);
        model.put("freePersonList", freePersonList);
        model.put("hardPersonList", hardPersonList);
        model.put("defaultAdd", defaultAdd);
		return new ModelAndView("ye/freeBasicServices/addFreeBasicServices",model);		
    }
    
    /**
     * 根据死者姓名弹出相应信息
     * @return
     */
    @RequestMapping(params = "method=dNameSearch")
    @ResponseBody
    public JSONObject dNameChange(){
    	JSONObject json = new JSONObject();
    	//根据死者姓名，找对应的火化委托单
    	String dName=getString("dName");
    	Map<String,Object> paramMap=new HashMap<String, Object>();
    	paramMap.put("name", dName);
    	paramMap.put("checkFlag", Const.Check_Yes);//条件添加已审核
    	List<CommissionOrder> list=commissionOrderService.getCommissionOrderList(paramMap);
    	if(list.size()==1){
    		CommissionOrder commission=list.get(0);    		
    		//基本信息
    		Timestamp dieTime=commission.getdTime();
    		Timestamp cremaTime=commission.getCremationTime();     		
    		String addr=commission.getdAddr();
    		String dieId=commission.getCertificateCode();
    		
    		String cardCode=commission.getCardCode();
    		String sex=commission.getSexName();
    		int age=commission.getAge();
    		String name=commission.getName();
    		String dNameId=commission.getId();    		
    		
    		//与死者关系
    		String fCardCode=commission.getfCardCode(); 
    		String fPhone=commission.getfPhone();
    		String fName=commission.getfName();
    		String appellationId=commission.getfAppellationId();
    		List<Appellation> appellationList=appellationService.getAppellationList(null);
    		List list2 = new ArrayList();
    		for(int i=0;i<appellationList.size();i++){
    			list2.add(new Object[]{appellationList.get(i).getAppellationId(),appellationList.get(i).getName()});
    		}
    		String fAppellationOption=DataTools.getOptionByList(list2, appellationId, false); 
    		
    		//传值给前端json
    		json.put("addr", addr);
    		json.put("dieTime", dieTime);
    		json.put("cremaTime", cremaTime);
    		json.put("fCardCode", fCardCode);
    		json.put("dName", name);
    		json.put("dNameId", dNameId);
    		json.put("fAppellationOption", fAppellationOption);
    		json.put("sex",sex );
    		json.put("cardCode",cardCode );
    		json.put("fName",fName );
    		json.put("fPhone",fPhone );
    		json.put("dAge", age);
    		json.put("dieId", dieId);
    	}
    	//传值给前端json
		json.put("size",list.size());
		return json;
    }
    
    /**
	 *死者姓名筛选
	 * @return
	 */
    @RequestMapping(params = "method=dNameList")
    public ModelAndView dNameListbuyWreathRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	model=getModel(model);
    	//获得页面页码信息
//		int[] pages = getPage();
    	String dName = getString("dName");
    	Map<String,Object> maps = new HashMap<String,Object>();
    	maps.put("name", dName);
    	maps.put("checkFlag", Const.Check_Yes);//已审核
    	List<CommissionOrder> list = commissionOrderService.getCommissionOrderList(maps);
    	List list0 = new ArrayList();
    	for(int i=0;i<list.size();i++){
    		CommissionOrder commission=list.get(i);
    		//基本信息
    		Timestamp dieTime=commission.getdTime();
    		Timestamp cremaTime=commission.getCremationTime();     		
    		String addr=commission.getdAddr();
    		String dieId=commission.getCertificateCode();
    		
    		String cardCode=commission.getCardCode();
    		String sex=commission.getSexName();
    		int age=commission.getAge();
    		String name=commission.getName();
    		String dNameId=commission.getId();    		
    		
    		//与死者关系
    		String fCardCode=commission.getfCardCode(); 
    		String fPhone=commission.getfPhone();
    		String fName=commission.getfName();
    		String appellationId=commission.getfAppellationId();
    		List<Appellation> appellationList=appellationService.getAppellationList(null);
    		List list2 = new ArrayList();
    		for(int j=0;j<appellationList.size();j++){
    			list2.add(new Object[]{appellationList.get(j).getAppellationId(),appellationList.get(j).getName()});
    		}
    		String fAppellationOption=DataTools.getOptionByList(list2, appellationId, false); 
    		
    		list0.add(new Object[]{
    				dNameId,name,age,sex,dieId,addr,cardCode,fName,fPhone,fAppellationOption,dieTime,cremaTime,fCardCode
    		});
    	}
//    	PageInfo page = new PageInfo(list0);
		model.put("page", list0);
  
    	return new ModelAndView("../../common/basicResult",model);
    }
    /**
     * 链接到减免申请详情页（修改）
     */
    @RequestMapping(params = "method=edit")
    public ModelAndView editFreeBasicServices(){
		Map<String,Object> model=new HashMap<String,Object>();			
		//申请记录ID
		String id = getString("id");
		BaseReduction baseReduction=freeBasicService.getFreeBasicById(id);
		//免费对象类别
		String freePerson=baseReduction.getFreePersonIds();
		//重点救助对象类别
		String hardPerson=baseReduction.getHardPersonIds();
		//获取火化委托单实体类
		CommissionOrder commission=commissionOrderService.getCommissionOrderById(baseReduction.getCommissionOrderId());
		//与死者关系
    	String relationShip="";
    	String appellationId=commission.getfAppellationId();
    	if(appellationId!=null){
    		Appellation appellation=appellationService.getAppellationById(appellationId);
    		relationShip=appellation.getName();    		
    	}
		
		//减免详情
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("baseReductionId", id);
		//根据申请id在减免申请详情表中找到所有对应的记录，返回list
		List list=baseReduuctionDService.getBaseReductionDList(maps);
		double allTotal=0;//合计总价
		List ReductionDList=new ArrayList();
		if(list.size()>0){			
			for(int i=0;i<list.size();i++){
				//循环这些记录，并按记录的Item_id找ba_item表的相关记录
				BaseReductionD baseReductionD=(BaseReductionD)list.get(i);
				int number=baseReductionD.getNumber();
				//减免项目ID
				String baseId=baseReductionD.getId();
				double total=baseReductionD.getTotal();
				String comment=baseReductionD.getComment();
				//ba_item表中的数据 
				String itemId=baseReductionD.getItemId();
				//String name=itemService.getItemById(itemId).getName();
				//名称（收费类别下的具体项目）
		    	String name=itemService.getItemOption(new HashMap(),itemService.getItemById(itemId).getId(),false);
				double pice=itemService.getItemById(itemId).getPice();
				allTotal+=total;
				Map<String,Object> ReductionDMaps=new HashMap<String,Object>();
				ReductionDMaps.put("number", number);
				ReductionDMaps.put("total", total);
				ReductionDMaps.put("comment", comment);
				ReductionDMaps.put("name", name);
				ReductionDMaps.put("pice", pice);
				ReductionDMaps.put("baseId", baseId);
				ReductionDList.add(ReductionDMaps);
			}
		}
		//作为添加记录的默认数据
		Map<String,Object> defaultMap=new HashMap<String,Object>();
		defaultMap.put("limit", "1");
		List<Item> item= itemService.getItemList(defaultMap);
		double defaultPice=item.get(0).getPice();
		String defaultNameOption=itemService.getItemOption(new HashMap<String,Object>(),item.get(0).getId(),false);
		Map<String,Object> defaultAdd=new HashMap<String,Object>();
		defaultAdd.put("defaultPice", defaultPice);
		defaultAdd.put("defaultName", defaultNameOption);
		
		//免费对象类别
		List freePersonList=freePersonService.getFreePersonList(null);
		//重点救助对象类别
		List hardPersonList=hardPersonService.getHardPersonList(null);
		model.put("ReductionDList",ReductionDList);
		model.put("allTotal",allTotal);
		model.put("freePerson", freePerson);
		model.put("hardPerson",hardPerson);
		model.put("commission", commission);
		model.put("defaultAdd", defaultAdd);
		model.put("freePersonList", freePersonList);
		model.put("hardPersonList", hardPersonList);
		model.put("id", id);
		model.put("relationShip", relationShip);
		
		
		if(id!=null&&!id.equals("")){

		}else {
//			List list=deadTypeService.getDeadTypeList(null);
//			if (list.size()>0) {
//				DeadType deadType=(DeadType)list.get(0);
//				deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId());
//			}
		}
		

		//将选中的挂账基本信息返回并显示
		model.put("id",id);
		model.put("method", "edit");
		return new ModelAndView("ye/freeBasicServices/editFreeBasicServices",model);
    }
    
    
    /**
     * 减免名称改变
     * @return
     */
    @RequestMapping(params = "method=itemNameChange")
    @ResponseBody
    public JSONObject itemNameChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
		json.put("pice", item.getPice());
		return json;
    }
	
    
    /**
     * 保存基本减免信息
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存基本减免信息")
    @ResponseBody
    public JSONObject saveBaseReduction(){
		JSONObject json = new JSONObject();
    	try {
    		String reductionId=getString("baseReductionId");
    		//保存火化委托单（备注）
    		BaseReduction redction=freeBasicService.getFreeBasicById(reductionId);
    		String commissionId=redction.getCommissionOrderId();
    		CommissionOrder commission=commissionOrderService.getCommissionOrderById(commissionId);
    		commission.setComment(getString("com_comment"));
    		
    		String baseReductionId=getString("baseReductionId");
    		//重点救助对象类别
    		String hardId=getString("hardIdString");
    		//免费对象类别
    		String freeId=getString("freeIdString");
    		
    		BaseReduction baseReduction=freeBasicService.getFreeBasicById(baseReductionId);
    		baseReduction.setHardPersonIds(hardId);
    		baseReduction.setFreePersonIds(freeId);	
    		
    		//需要删除的ID
    		String delNumber[]=getRequest().getParameterValues("delNumber");
    		
    		String[] saveData=getRequest().getParameterValues("saveData");
    		
    		freeBasicService.saveFreeBasic(baseReduction,commission,saveData,delNumber);
    			
    		
		
		
		
		
    	setJsonBySuccess(json, "保存成功","freeBasicServices.do");    	
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
			
		}
		return json;
    }
    
    /**
     * 保存添加的基本减免信息
     * @return
     */
    @RequestMapping(params = "method=addSave")
    @SystemControllerLog(description = "添加基本减免信息")
    @ResponseBody
    public JSONObject addBaseReduction(){
		JSONObject json = new JSONObject();
    	try {
    		String reductionId=getString("id");
    		if(reductionId!=null&&reductionId!=""){
    			
    		}else{
    			reductionId=UuidUtil.get32UUID();
    		}
    		//重点救助对象类别
    		String hardId=getString("hardIdString");
    		//免费对象类别
    		String freeId=getString("freeIdString");
    		//经办人身份证号
    		String fidcard=getString("fCardCode");
    		//减免申请表数据
    		BaseReduction baseReduction=new BaseReduction();
    		baseReduction.setHardPersonIds(hardId);
    		baseReduction.setFreePersonIds(freeId);
    		baseReduction.setId(reductionId);
    		Date date=DateTools.getThisDate();
    		baseReduction.setCreateTime(date);
    		baseReduction.setFidcard(fidcard);
    		baseReduction.setCommissionOrderId(getString("dNameId"));
    		//保存减免项目 
    		String[] saveData=getRequest().getParameterValues("saveData");
    		
    		freeBasicService.addSaveFreeBasic(baseReduction,saveData,reductionId);
    		
		
		setJsonBySuccess(json, "保存成功", "freeBasicServices.do");
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
        
    /**
	 * 添加基本减免项目（弹窗）
	 * @return
	 */
    @RequestMapping(params = "method=insert")
    public ModelAndView insertFreeBasicItem(){
    	Map<String,Object> model=new HashMap<String,Object>();
	
		//添加服务项目
		List itemListAll=new ArrayList();
//		Map maps=new HashMap();
//		maps.put("name","花圈花篮");
		List itemTypeList=itemTypeService.getItemTypeList(null);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			Map<String,Object> itemMaps=new HashMap<String,Object>();
			itemMaps.put("typeId", itemType.getId());
			List itemList=itemService.getItemList(itemMaps);
			itemListAll.add(new Object[]{itemType,itemList});
		}
		model.put("itemTree", getTree(itemListAll));  
    	return new ModelAndView("ye/freeBasicServices/selectItems",model);
    }
    
    /**
     * 添加花圈花篮
     * @return
     */
    @RequestMapping(params = "method=addItems")
    @ResponseBody
    public List saveItemOrder(){
		List list = new ArrayList();
		String id =getString("ids");
		String[] ids = id.split(",");		
//		Map maps=new HashMap();
	    for(int i=0;i<ids.length;i++){
	    	if(!ids[i].equals("")){
	    		Item item=itemService.getItemById(ids[i]);
//		    	maps.put("typeId", item.getTypeId());
		    	String itemOption=itemService.getItemOption(null,ids[i],false);
		    	list.add(new Object[]{itemOption,item.getPice(),1,item.getPice()});
	    	}	    	
	    }
		return list;
    }
    
    /**
     * 获得tree结构
     * @param list
     * @return
     */
    public String getTree(List list){
    	int sum=0;
    	String content="";
    	for (int i = 0; i < list.size(); i++) {
    		if (i>0) {
    			content+=",";
			}
    		Object[] objects=(Object[]) list.get(i);
    		ItemType itemType=(ItemType)objects[0];
	    	content+= "{";
	    	content+="text:'"+itemType.getName()+"'";
//	    	content+=",tags:['"+itemType.getId()+"']";
	    	content+=",selectable: false";
	    	List itemList=(List)objects[1];
	    	content+=",nodes: [";
	    	for (int j = 0; j < itemList.size(); j++) {
	    		if (j>0) {
	    			content+=",";
				}
	    		Item item=(Item)itemList.get(j);
	    		content+="{";
	    		content+="text:'"+item.getName()+"'";
	    		content+=",tags:['"+item.getId()+"_"+sum+"']";
	    		sum++;
	    		content+="}";
	    	}
	    	content+="]";
	    	content+="}";
    	}
    	return content;
    }
}
