package com.hz.controller.ye;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.MourningRecord;
import com.hz.entity.ye.BaseReductionD;
import com.hz.service.base.MourningService;
import com.hz.service.ye.AshesRecordService;
import com.hz.service.ye.BaseReductionDService;
import com.hz.service.ye.BaseReductionService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 基本减免申请项目
 * @author rgy
 *
 */
@Controller
@RequestMapping("/baseReductionD.do")
public class BaseReductionDController extends BaseController{
	@Autowired
	private BaseReductionDService baseReductionDService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "baseReductionD.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listBaseReductionD();
    }

	/**
	 * 基本减免申请项目列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listBaseReductionD(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map maps=new HashMap();
		PageInfo page=baseReductionDService.getBaseReductionDPageInfo(maps,pages[0],pages[1],null);
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("ye/baseReductionD/listBaseReductionD",model);
    }
	/**
	 * 链接到修改添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editBaseReductionD(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		BaseReductionD baseReductionD=new BaseReductionD();
		if(id!=null&&!id.equals("")){
			baseReductionD=baseReductionDService.getBaseReductionDId(id);
		}
        model.put("baseReductionD", baseReductionD);
        model.put("method", "save");
		return new ModelAndView("ye/baseReductionD/listBaseReductionD",model);
    }
    /**
     * 保存基本减免申请项目
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存基本减免申请项目")
    @ResponseBody
    public JSONObject saveBaseReductionD(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		BaseReductionD baseReductionD=new BaseReductionD();
    		if (id!=null&&!id.equals("")) {
    			baseReductionD=baseReductionDService.getBaseReductionDId(id);
    		}
    		baseReductionDService.addOrUpdate(id,baseReductionD);
    		setJsonBySuccess(json, "保存成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除基本减免申请项目
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除基本减免申请项目信息")
    @ResponseBody
    public JSONObject deleteBaseReductionD(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
	    	baseReductionDService.deleteBaseReductionD(id);
    		setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
		}
		return json;
    }
}
