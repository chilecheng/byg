package com.hz.controller.ye;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hz.util.Const;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.system.User;
import com.hz.entity.ye.AutoFreezerDetail;
import com.hz.entity.ye.AutopsyRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.base.ProveUnitService;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.service.ye.AutopsyRecordService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**法政验尸
 * @author gpf
 */
@Controller
@RequestMapping("/autoFreezer.do")
public class AutoFreezerController extends BaseController {
	@Autowired
	private AutopsyRecordService autopsyRecordService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private ProveUnitService proveUnitService;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	DeptService deptService;

	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "autoFreezer.do");
	}

	@RequestMapping(params = "method=add")
	public ModelAndView addAutoFreezer() {
		String id = getString("id");
		Map<String, Object> model = new HashMap<String, Object>();
		User u = getCurrentUser();
		model.put("u", u);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		AutoFreezerDetail detail = autopsyRecordService.getAutoFD(map);
		model.put("id", detail.getId());
		model.put("name", detail.getName());
		model.put("code", detail.getCode());
		model.put("comment", detail.getComment());
		model.put("age", detail.getAge());
		model.put("sex", detail.getSexName());
		model.put("freezerName", detail.getFreezerName());
		model.put("proveName", detail.getProveName());
		if (detail.getRegisterTime() == null) {
			model.put("registerTime", DateTools.getThisDateTimestamp());
		} else {
			model.put("registerTime", detail.getRegisterTime());
		}
		model.put("yTime", detail.getyTime());

		model.put("method", "save");
		String pUption = proveUnitService.getProveUnitOption(detail.getProveUnitId(), false);//
		model.put("pUption", pUption);
		return new ModelAndView("ye/second_department/autopsy/toApplyAutopsy", model);
	}

	@RequestMapping(params = "method=save")
	@SystemControllerLog(description = "保存验尸信息")
	@ResponseBody
	public JSONObject saveAutoFreezer() {
		JSONObject json = new JSONObject();
		try {
			AutopsyRecord apr = new AutopsyRecord();
			String cid = getString("id");
			String id = UuidUtil.get32UUID();
			String code = getString("fNumber");
			Timestamp yTime = getTimeM("yTime");
			Map<String, Object> maps = new HashMap<String, Object>();
			CommissionOrder com = commissionOrderService.getCommissionOrderById(cid);
			Timestamp arriveTime = com.getArriveTime();
			Timestamp t = DateTools.dateToTimestamp(yTime);
			Timestamp autoFreezerTime = getAutoFreezerTime(t, arriveTime);
			java.sql.Date date = DateTools.getThisDate();
			User u = getCurrentUser();

			List<AutopsyRecord> checkcid = autopsyRecordService.checkcid(cid);
			if (checkcid != null && checkcid.size() != 0) {
				// 验尸申请更改
				apr.setProveUnitId(getString("checkpsy"));
				apr.setComment(getString("comment"));
				apr.setyTime(DateTools.dateToTimestamp(yTime));
				apr.setCommissionOrderId(cid);
				apr.setAutoFreezerTime(autoFreezerTime);
				autopsyRecordService.toRevise(apr);
				// 变更日子 满足消息提醒条件 返回1，否则返回0
				int n = autopsyRecordService.getTypeSession(apr);
				int n2 = userNoticeService.getCount(u.getUserId(), Const.NoticeType_YsjdTask, date);
				int nowNumber = n + n2;
				getSession().setAttribute("autoFreezerNumber", nowNumber);
			} else {
				// 验尸申请新增
				maps.put("commissionOrderId", cid);
				maps.put("id", id);
				// List<AutopsyRecord>
				// list=autoFreezerService.findAllAutoFreezer(maps);
				apr.setId(id);
				apr.setCommissionOrderId(cid);
				apr.setCode(code);
				apr.setProveUnitId(getString("checkpsy"));
				apr.setyTime(DateTools.dateToTimestamp(yTime));
				apr.setComment(getString("comment"));
				apr.setRegisterTime(getTime("registerTime"));
				apr.setFlag((byte) 2);
				User user = getCurrentUser();
				apr.setRegisterUserId(user.getUserId());
				apr.setAutoFreezerTime(autoFreezerTime);
				autopsyRecordService.addAutopsyRecord(apr);
				// 查询是否属于当天的信息 是返回1 否 返回0
				int n = autopsyRecordService.getTypeIsertSession(apr);
				int n2 = userNoticeService.getCount(u.getUserId(), Const.NoticeType_YsjdTask, date);
				int nowNumber = n + n2;
				getSession().setAttribute("autoFreezerNumber", nowNumber);

			}
			setJsonBySuccess(json, "保存成功", "autoFreezer.do?method=list");
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败" + e.getMessage());
		}
		return json;
	}

	public String getDateBefore(Timestamp t, int day) {
		Calendar dates = Calendar.getInstance();
		dates.setTime(t);
		dates.set(Calendar.DATE, dates.get(Calendar.DATE) - day);
		java.util.Date dateTimestamp = dates.getTime();
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String LgTime = sdformat.format(dateTimestamp);
		return LgTime;
	}

	public Timestamp getAutoFreezerTime(Timestamp yTime, Timestamp b) {
		String jdTimeDate = new String();
		Timestamp yTimeDate = yTime;
		Timestamp beginDate = b;
		long between = yTimeDate.getTime() - beginDate.getTime();
		double days = Math.floor(between / (24 * 3600 * 1000));
		if (days < 7) {
			jdTimeDate = getDateBefore(yTimeDate, 1);
		} else {
			jdTimeDate = getDateBefore(yTimeDate, 2);
		}
		Timestamp autoFreezerTime = Timestamp.valueOf(jdTimeDate);
		return autoFreezerTime;
	}

}
