package com.hz.controller.ye.thirdDepart;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.system.User;
import com.hz.entity.ye.ComOrSpeFuneralRecord;
import com.hz.entity.ye.FireCheckInInfo;
import com.hz.entity.ye.PaperInfo;
import com.hz.entity.ye.PolitenessInfo;
import com.hz.service.system.UserService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FireCheckInService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ItemConst;
/**
 * 三科火化登记
 * @author lipengpeng
 *
 */
@Controller
@RequestMapping("/fireCheckIn.do")
public class FireCheckInController extends BaseController{
	@Autowired
	private FireCheckInService fireCheckInService;
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private CommissionOrderService commissionOrderService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "fireCheckIn.do");
	}

	@RequestMapping
	public ModelAndView unspecified(){
		return new ModelAndView("ye/third_department/registration/searchPage",null);	
	}
	
	@RequestMapping(params = "method=judge")
	@ResponseBody
	public JSONObject judge()
	{
		JSONObject json = new JSONObject();
		String cardCode=getString("readCard");
		cardCode=cardCode.trim();//新加去除空格
		int count = commissionOrderService.getCommissionOrderByCard(cardCode);
		if (count > 1) {
			setJsonByFail(json, "存在多个此卡号的用户，请再次确认卡号！");
			return json;
		}
		FireCheckInInfo listfc = null;
		try {
			listfc = fireCheckInService.getBasicInfoByCard(cardCode);
			if (listfc == null) {
				setJsonByFail(json, "没有对应的卡号");
			} else if (listfc.getPincardFlag() == Const.pincard_Yes) {
				setJsonByFail(json, "已销卡");
			} else if (listfc.getCheckFlag() == Const.Check_No) {
				setJsonByFail(json, "该卡尚未审核");
			} else if (listfc.getPayFlag() == Const.Pay_No) {
				setJsonByFail(json, "还未收费");
			} else if (listfc.getFurnaceFlag() == 3 && listfc.getCremationTime() ==null ) {
				setJsonByFail(json, "该遗体尚不需要火化");
			} else if (listfc.getCremationFlag() ==Const.furnace_flag_Yes){
				setJsonByFail(json, "该遗体已火化");
			}
			else {
				setJsonBySuccess(json, "存在卡号", "fireCheckIn.do?method=list&cardCode=" + cardCode);
			}
		} catch (Exception e) {
			setJsonByFail(json, "卡号重复");
			e.printStackTrace();
		}
		return json;
	}
	
	@RequestMapping(params = "method=list")
	public ModelAndView showCheckCard()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String cardCode=getString("cardCode");
		if (getString("id") != null && !getString("id").equals("")) {
			cardCode = commissionOrderService.getCardCodeById(getString("id"));
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cardCode", cardCode);
		map.put("changeFlag", Const.ChangeFlag_No);
		model.put("recordId", fireCheckInService.getRecordIdByCardcode(map));//取出y_furnace_record里面的id
		FireCheckInInfo listfc=fireCheckInService.getBasicInfoByCard(cardCode);
		
		if(listfc!=null)
		{
			model.put("listfc", listfc);
			model.put("listfc_sex", listfc.getSexName());//!!!
			model.put("cremation_flag", listfc.getCremationOrNot());
			model.put("fireType", listfc.getFurnaceFlag());
			model.put("furnaceName", listfc.getFurnaceName());
		}
		PaperInfo pinfo=fireCheckInService.getPaperInfoByCard(cardCode, "纸棺");
		if(pinfo!=null&&pinfo.getPaperlist()!=null&&pinfo.getPaperlist().size()>0)//>0
		{
			model.put("pinfo", pinfo.getPaperlist());//.get(0).getPaperName()
		}
		PolitenessInfo poinfo=fireCheckInService.getPolitenessInfoByCard(cardCode, ItemConst.LiyiChubin_Sort);
		if(poinfo!=null&&poinfo.getPolitenesslist()!=null&&poinfo.getPolitenesslist().size()>0)//>0
		{
			model.put("poinfo", "有");
		}
		else
		{
			model.put("poinfo", "无");
		}
		return new ModelAndView("ye/third_department/registration/showFireInfo",model);
	}
	
	@RequestMapping(params = "method=arrange")
	public ModelAndView arrange()
	{
		Map<String,Object> model=new HashMap<String,Object>();
/*		String age=getString("age");
		model.put("age", age);*/
		String fphone=getString("fphone");
		model.put("fphone", fphone);
		String dname=getString("dname");
		model.put("dname", dname);
		String fname=getString("fname");
		model.put("fname", fname);
		String code=getString("code");
		model.put("code", code);
		String sex=getString("sex");
		model.put("sex", sex);
		String age=getString("age");
		model.put("age", age);
		Timestamp cremationtime = null;
		try {
			cremationtime = getTime("cremationtime");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		model.put("cremationtime", DateTools.getTimeFormatString("yyyy-MM-dd HH:mm:ss",cremationtime));
	/*	String str =  URLDecoder.decode(title, "UTF-8");*/
		String cremationFlag=getString("cremationFlag");
		model.put("cremationFlag", cremationFlag);
		String cardCode=getString("cardCode");
		String cid=getString("cid");
		FireCheckInInfo lf=fireCheckInService.getBasicInfoByCard(cardCode);
		if(cremationFlag.equals("未火化"))
		{
			String fireworkerOption=fireCheckInService.getFireWorkerOption(null, false);
			model.put("fireworkerOption", fireworkerOption);
			if(lf!=null)
			{
				if(lf.getFurnaceFlag() == Const.furnace_ty)
				{
					String fireid=fireCheckInService.findFurnaceIdBycommissionId(cid);
					if(fireid!=null)
					{
						String specialOption=fireCheckInService.getAllSpecialOrCommonFurnaceOption(fireid, false, "特约炉");//
						model.put("FurnaceOption", specialOption);
						model.put("fireid", fireid);//特约炉id
						model.put("spe_or_com_flag", true);//true代表特约炉
					}
				}
				else if(lf.getFurnaceFlag() == Const.furnace_pt)
				{
					String commonOption=fireCheckInService.getAllSpecialOrCommonFurnaceOption(null, false, "普通炉");
					model.put("FurnaceOption", commonOption);
					model.put("spe_or_com_flag", false);//false代表普通炉
				}
				model.put("startTime",DateTools.getThisDateUtil());
			}
			User u = getCurrentUser();
			model.put("uname", u.getName());
		}
		else//已火化
		{
			ComOrSpeFuneralRecord cr=fireCheckInService.findFurnaceInfoBycommissionId(cid);
			if(cr!=null)
			{
				String fireworkerOption=fireCheckInService.getFireWorkerOption(cr.getWorkerId(), false);
				model.put("fireworkerOption", fireworkerOption);
				
				if(lf!=null)
				{
					String fireid=cr.getFurnaceId();
					if(lf.getFurnaceFlag()==2)
					{				
						if(fireid!=null)
						{
							String specialOption=fireCheckInService.getAllSpecialOrCommonFurnaceOption(fireid, false, "特约炉");//
							model.put("FurnaceOption", specialOption);
							model.put("fireid", fireid);//特约炉id
							model.put("spe_or_com_flag", true);//true代表特约炉
						}
						
					}
					else if(lf.getFurnaceFlag()==1)
					{
						String commonOption=fireCheckInService.getAllSpecialOrCommonFurnaceOption(fireid, false, "普通炉");
						model.put("FurnaceOption", commonOption);
						model.put("spe_or_com_flag", false);//false代表普通炉
					}
				}
				model.put("startTime",cr.getBeginTime());
				model.put("comment", cr.getComment());
				model.put("uname",userservice.getNameById(cr.getCreateUserId()));//
			}
			
		}
		model.put("cid", cid);
		model.put("cardCode", cardCode);
		model.put("recordId", getString("recordId"));
		return new ModelAndView("ye/third_department/registration/arrangeRegration",model);
	}
	
	@RequestMapping(params = "method=insertOrUpdate")
	@SystemControllerLog(description = "三科火化登记更改")
	public void insertOrUpdate(HttpServletResponse response) {
		/**
		 * 对于普通炉是insert，因为没有记录存在，而且普通炉在这里不存在转炉；
		 * 对于特约炉分两种情况，一种是未更改炉号，即没有转炉，此时是update，因为已经有记录存在；另一种是更改了炉号，即转炉，此时既有insert又有update，
		 * 插入新记录（change_flag为2），更改已存在记录（change_flag改为1）。
		 * 其中以上的insert可以共用，两个update不能共用
		 */
		String cremationFlag = getString("cremationFlag");
		try {
			if (cremationFlag.equals("已火化")) {
				response.sendRedirect("furnaceRecord.do?method=listInfo&type=three");//重定向到列表页，并在后面加return，不然后面的代码也会执行下去
				return;
			}
			String spe_or_com_flag = getString("spe_or_com_flag");
			String cid = getString("cid");
			Timestamp cremationtime = getTimeM("orderTime");
			Timestamp FireStartTime = getTimeM("FireStartTime");
			String funeralNum = getString("funeralNum");
			String fireworker = getString("fireworker");
			String remark = getString("remark");
			String recordId = getString("recordId");//旧记录ID
			User u = getCurrentUser();
			String fireid = getString("fireid");
			ComOrSpeFuneralRecord cr = new ComOrSpeFuneralRecord();
			fireCheckInService.insertOrUpdate(spe_or_com_flag,cr,cid,fireid,funeralNum,remark,fireworker,cremationtime,FireStartTime,u,recordId);
			response.sendRedirect("fireAttestPrint.do?method=list&cardCode=" + getString("cardCode"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return;
	}
}
