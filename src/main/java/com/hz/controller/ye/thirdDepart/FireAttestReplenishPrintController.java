package com.hz.controller.ye.thirdDepart;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.controller.ye.CommissionOrderController;
import com.hz.entity.system.User;
import com.hz.entity.ye.ComOrSpeFuneralRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FireAttestPrintInfo;
import com.hz.entity.ye.FireCheckInInfo;
import com.hz.entity.ye.PaperInfo;
import com.hz.entity.ye.PolitenessInfo;
import com.hz.entity.ye.PrintAgain;
import com.hz.entity.ye.PrintRecord;
import com.hz.service.system.UserService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FireAttestPrintService;
import com.hz.service.ye.FireCheckInService;
import com.hz.service.ye.FirePrintAgainService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.ItemConst;
import com.hz.util.UuidUtil;
/**
 * 三化证明补打
 * @author jgj
 *
 */
@Controller
@RequestMapping("/fireAttestReplenishPrint.do")
public class FireAttestReplenishPrintController extends BaseController{
	private static Logger logger = LoggerFactory.getLogger(CommissionOrderController.class); 
	@Autowired
	private FireAttestPrintService fireAttestPrintService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private FirePrintAgainService firePrintAgainService;
	
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "fireAttestReplenishPrint.do");
	}

	@RequestMapping
	public ModelAndView unspecified(){
		return fireAttestPrintList();	
	}
	
	/**
	 * 已火化  列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView fireAttestPrintList(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int[] pages = getPage();
		String findBy=getString("findBy");
		String searchVal=getString("searchVal");
		String searchOption=Const.getSearchTypeOptionByString(findBy,new byte[]{Const.Search_Type_Name,Const.Search_Type_IDCard});
		Map<String,Object> map=new HashMap<String,Object>();
		if("1".equals(findBy)){//姓名
			map.put("findByName", searchVal);
		}
		if("18".equals(findBy)){//身份证号
			map.put("findByIDCard", searchVal);
		}
		//开始时间
		String startStr=getString("startTime");
		if(!"".equals(startStr)){
//			Date startTime=DateTools.stringToSqlDate(startStr, "yyyy-MM-dd");	
			map.put("startTime", startStr);
		}
		//结束时间
		String endStr=getString("endTime");
		if(!"".equals(endStr)){
//			Date endTime=DateTools.stringToSqlDate(endStr, "yyyy-MM-dd");	
			map.put("endTime", endStr+"23:59:59");
		}
		map.put("cremationFlag", Const.furnace_flag_Yes);
		PageInfo<FireAttestPrintInfo> page=fireAttestPrintService.getBasicInfoListPageInfo(map,pages[0],pages[1],"print_time desc");
		model.put("searchOption", searchOption);
		model.put("page", page);
		model.put("method", "list");
		return new ModelAndView("ye/third_department/fireAttestReplenishPrint/fireAttestPrintList",model);
	}
	/**
	 * 火化证明打印信息显示
	 * @return
	 */
	@RequestMapping(params = "method=show")
	public ModelAndView fireView(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String printId=getString("printId");
		String orderId=getString("orderId");		
		CommissionOrder order=commissionOrderService.getCommissionOrderById(orderId);
		PrintRecord printRecord= fireAttestPrintService.getPrintRecordById(printId);
		List<PrintAgain> listAgain=firePrintAgainService.getPrintAgainList(printId);
		model.put("commissionOrder", order);
		model.put("listAgain", listAgain);
		model.put("printRecord", printRecord);
		model.put("method", "list");
		return new ModelAndView("ye/third_department/fireAttestReplenishPrint/fireAttestPrintReadOnly",model);
	}
	
	/**
	 * 补打领取人录入
	 * @return
	 */
	@RequestMapping(params="method=insert")
	public ModelAndView PickNameInsert(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String id=getString("id");
		String cid=getString("cid");
		PrintRecord printRecord= fireAttestPrintService.getPrintRecordById(id);
//		String takeName=getString("takeName");
//		String takeCode=getString("takeCode");
		String dName=getString("dName");
		String dCode=getString("dCode");
		String dSex=getString("dSex");
//		
//		model.put("takeName", takeName);
//		model.put("takeCode", takeCode);
		model.put("dName", dName);
		model.put("dCode", dCode);
		model.put("dSex", dSex);
		model.put("printRecord", printRecord);
		model.put("printId", id);
		model.put("commissionOrderId", cid);
		model.put("method", "save");
		return new ModelAndView("ye/third_department/fireAttestReplenishPrint/order",model);
	}
	
	/**
	 * 保存火化证明补打领取人录入
	 * @return
	 */
	@SystemControllerLog(description = "保存火化证明补打领取人录入")
	@ResponseBody
	@RequestMapping(params="method=save")
	public JSONObject saveCommissionOrder(){
		JSONObject json = new JSONObject();
		try{
			String commissionOrderId=getString("commissionOrderId");
			String printId=getString("id");
			String takeNameAgain=getString("takeName");
			String takeCodeAgain=getString("takeCode");
			Timestamp printTimeAgain=DateTools.getThisDateTimestamp();
			String userId=getCurrentUser().getUserId();
			fireAttestPrintService.addOrUpdate(commissionOrderId,printId,takeNameAgain,takeCodeAgain,printTimeAgain,userId);
			
			setJsonBySuccess(json, "保存成功", "ye/third_department/fireAttestReplenishPrint/fireAttestPrintList");
		}catch(Exception e){
			e.printStackTrace();
			logger.error(DataTools.getStackTrace(e));
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return  json;
	}
	/**
	 * 火化证明补打
	 * @return
	 */
	@RequestMapping(params="method=print")
	public ModelAndView printAgain(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String id=getString("id");
		String cid=getString("cid");
		PrintRecord printRecord= fireAttestPrintService.getPrintRecordById(id);
		String beginTime=fireAttestPrintService.getActualFireTimeByCId(cid);
		String dName=getString("dName");
		String dCode=getString("dCode");
		String dSex=getString("dSex");
		String userName=getCurrentUser().getName();
		Date now=DateTools.getThisDate();
		model.put("dName", dName);
		model.put("dCode", dCode);
		model.put("dSex", dSex);
		model.put("userName", userName);
		model.put("now", now);
		model.put("beginTime", Timestamp.valueOf(beginTime));
		model.put("printRecord", printRecord);
		model.put("printId", id);
		model.put("commissionOrderId", cid);
		model.put("method", "save");
		return new ModelAndView("ye/third_department/fireAttestReplenishPrint/orderPrint",model);
	}
	/**
	 * 三科火化证明补打 更新打印
	 * @return
	 */
	@RequestMapping(params="method=upSchedual")
	@SystemControllerLog(description = "三科火化证明补打更新状态")
	@ResponseBody
	public JSONObject upSchedual(){
		JSONObject json = new JSONObject();
		String id = getString("id");
		CommissionOrder com=null;
		PrintRecord printRecord=null;
		String cid="";
		PrintAgain printAgain=new PrintAgain();
		if(id!=null && !"".equals(id)){
			printRecord= fireAttestPrintService.getPrintRecordById(id);
			cid=printRecord.getCid();
			printRecord.setIsPrintAgain(Const.Is_Yes);
			printRecord.setPrintTimeAgain(DateTools.getThisDateTimestamp());
			com=commissionOrderService.getCommissionOrderById(cid);
			
			printAgain.setId(UuidUtil.get32UUID());
			printAgain.setPrintId(id);
			printAgain.setPrintTimeAgain(printRecord.getPrintTimeAgain());
			printAgain.setTakeCodeAgain(printRecord.getTakeCodeAgain());
			printAgain.setTakeNameAgain(printRecord.getTakeNameAgain());
			printAgain.setUserId(getCurrentUser().getUserId());
		}else{
			cid=getString("commissionOrderId");
			printRecord=new PrintRecord();
			printRecord.setId(UuidUtil.get32UUID());
			printRecord.setCid(cid);
			printRecord.setPrintTime(DateTools.getThisDateTimestamp());
			printRecord.setTakeCode(getString("takeCode"));
			printRecord.setTakeName(getString("takeName"));
			printRecord.setUserId(getCurrentUser().getUserId());
			printRecord.setIsPrintAgain(Const.Is_No);
		}
		fireAttestPrintService.addAndUpdate(printRecord,cid,printAgain,id);
		json.put("commissionOrder", com);
//		commissionOrderService.updateCommissionOrderScheduleFlag(cid, Const.schedule_flag_4);
//		fireAttestPrintService.updatePrintRecord(printRecord);
//		firePrintAgainService.addPrintAgain(printAgain);
//		fireAttestPrintService.addPrintRecord(printRecord);
		return json;
		
		
		
		
		
	}
	
}
