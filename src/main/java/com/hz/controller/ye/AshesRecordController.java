package com.hz.controller.ye;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Appellation;
import com.hz.entity.base.AshesPosition;
import com.hz.entity.base.Item;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.base.AppellationService;
import com.hz.service.base.AshesPositionService;
import com.hz.service.base.ItemService;
import com.hz.service.system.UserService;
import com.hz.service.ye.AshesRecordDService;
import com.hz.service.ye.AshesRecordService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 骨灰寄存记录信息
 * @author hw
 *
 */
@Controller
@RequestMapping("/ashesRecord.do")
public class AshesRecordController extends BaseController{
	@Autowired
	private AshesRecordService ashesRecordService;
	//续费
	@Autowired
	private AshesRecordDService ashesRecordDService;
	//骨灰位置寄存
	@Autowired
	private AshesPositionService ashesPositionService;
	//火化委托单
	@Autowired
	private CommissionOrderService commissionOrderService;
	//称谓关系
	@Autowired
	private AppellationService appellationService;
	@Autowired
	private UserService	 userService;
	@Autowired
	private ItemService itemService;
	
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "ashesRecord.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listashesRecord();
    }

	/**
	 * 骨灰寄存记录信息列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listashesRecord(){
		Map<String,Object> model=new HashMap<String,Object>();
        model.put("method", "list");
        model.put("type", getString("type"));
        //这个地方，要做数据的筛选，因为你这样子的话等于把数据库里所有有过保存的记录都会查询出来，这样日积月累，会非常多的，影响性能，这边应该
        //做筛选只读取有用的记录，比如当前的最小位置和最大位置有范围，那么你查询的时候就可以带上这两个参数去查询
        //这样查出来的就是保存在特定位置范围内的数据了，你还要进行筛选，筛选哪些目前正寄存的记录，之前已经完成了的记录就不需要查出来了
        //页面获取页码信息
		String num = getString("pageNum");
		int pageNum;
		
		if(num==null||num.equals("0")||num.isEmpty()){
			pageNum=1;
		}else{
			try {
				pageNum = DataTools.stringToInt(num);
			} catch (Exception e) {
				pageNum=1;
			}
		}
		//行数
		int pageSize = 20;
		//每行
		int celSize = 10;
        //数据库只有一个数据干嘛要查询list，在service里面写一个方法，直接得到唯一的一条数据就可以了
		AshesPosition ap = ashesPositionService.getAshesPosition();
		//这个number用来页面上显示总共有多少位置
		int number = ap.getNumber();
		//显示页码数
		int pages = number/200+1;
		model.put("number", ap.getNumber());
		List<String[]> cell=new ArrayList<String[]>();
		 
		//遍历生成所传到页面的数据形式，自己写
		//判断现在是第几页，比如是第二页
		//那么你存进去的编号应该是从2（页面）*20（行数）*10（每行个数）+1开始
		//再生成的同事，要查询出记录填入数据
		
		int init = (pageNum-1)*pageSize*celSize;
		   //插入查询条件
      	Map<String,Object> maps=new HashMap<String,Object>();
      	maps.put("statusFlag", Const.IsFlag_Yse);
      	int min = init+1;
        int max = init+pageSize*celSize;
        maps.put("minAshesPositionId", min);
        maps.put("maxAshesPositionId", max);
        List<AshesRecord> list = ashesRecordService.getAshesRecordList(maps);
		if (pages != pageNum) {
			for (int i = 0; i < pageSize; i++) {
				String[] strs = new String[celSize];
				for (int j = 0; j < celSize; j++) {
					strs[j] = (i * celSize + (j + 1) + init) + "号";
					for (int k = 0; k < list.size(); k++) {
						String placeCode = (list.get(k)).getAshesPositionId();
						Byte flag = (list.get(k)).getStatusFlag();
						if ((strs[j]).equals(placeCode) && flag == 1) {
							String dName = (list.get(k)).getdName();
							strs[j] = strs[j].concat(":" + dName);
						}
					}
				}
				cell.add(strs);
			}
		}else{
			int lastNumber = number-init;
			int rows = lastNumber/celSize+1;
			for(int i=0;i<rows;i++){
				if(i!=rows-1){
					String[] strs = new String[celSize];
					for(int j=0;j<celSize;j++){
						strs[j] = (i*10+(j+1)+init)+"号";
						for(int k=0;k<list.size();k++){
							String placeCode = (list.get(k)).getAshesPositionId();
							Byte flag = (list.get(k)).getStatusFlag();
							if(strs[j].equals(placeCode)&&flag==1){
								String dName =  (list.get(k)).getdName();
								strs[j] = strs[j].concat(":" + dName);
							}
						}
					}
					cell.add(strs);
				}else{
					int lastcelSize = lastNumber-(rows-1)*celSize;
					String[] strs = new String[lastcelSize];
					
					for(int j=0;j<lastcelSize;j++){
						
						strs[j] = (i*10+(j+1)+init)+"号";
						
						for(int k=0;k<list.size();k++){
							String placeCode = (list.get(k)).getAshesPositionId();
							Byte flag = (list.get(k)).getStatusFlag();
							if(strs[j].equals(placeCode)&&flag==1){
								String dName =  (list.get(k)).getdName();
								strs[j] = strs[j].concat(":" + dName);
//								strs[j].concat(":"+dName);
							}
						}
					}
					cell.add(strs);
				}
			}
		}
		//分页信息
		PageInfo<String[]> page = new PageInfo<String[]>(cell);
		page.setPages(pages);
		page.setPageNum(pageNum);
		model.put("page", page);
		model.put("pageNum", pageNum);
		return new ModelAndView("ye/ashesRecord/listAshesRecord",model);
    }
	/**
	 * 链接到添加页面
	 * @return
	 */
    @RequestMapping(params = "method=edit")
	public ModelAndView editashesRecord() {
		Map<String, Object> model = new HashMap<String, Object>();
		AshesRecord ashesRecord = new AshesRecord();
		String num = getString("pageNum");
		// 页面获取页码信息
		int pageNum;
		if (num == null || num.equals("0") || num.isEmpty()) {
			pageNum = 1;
		} else {
			try {
				pageNum = DataTools.stringToInt(num);
			} catch (Exception e) {
				pageNum = 1;
			}
		}
    		model.put("pageNum", pageNum);
    	//获取添加人姓名
    	model.put("agentUser", getCurrentUser().getName());
    	//获取系统时间
    	model.put("currentTime",DateTools.getTimeString("yyyy-MM-dd HH:mm:ss"));
    	//获取火化寄存位置
    	String placeCode = getString("placeCode");
    	int flag = 2;
    	if(!placeCode.equals(null)&&placeCode!=""){
    		flag = 1;
    	}
    	model.put("flag", flag);
    	model.put("placeCode", placeCode);
    	//获取火化时间
    	//预设当前日期
		Date cremationTime = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("cremationTime")!=null){
			cremationTime = getDate("cremationTime");
		}
		model.put("cremationTime", cremationTime);
		//获取存入时间
		Date beginDate = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("beginDate")!=null){
			beginDate = getDate("beginDate");
		}
		model.put("beginDate", beginDate);
		//获取到期时间
		Date endDate = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("endDate")!=null){
			endDate = getDate("endDate");
		}
		model.put("endDate", endDate);
		//与死者关系
		String fAppellationOption=appellationService.getAppellationOption(ashesRecord.getfAppellationId(),false);
		model.put("fAppellationOption", fAppellationOption);
    	/*//获取死者姓名
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("goto", Const.GOTO2);
    	List<CommissionOrder> list =  commissionOrderService.getCommissionOrderList(map);
    	List<Object[]> dNames = new ArrayList<Object[]>();
    	for (int i=0;i<list.size();i++){
    		dNames.add(new Object[]{(list.get(i)).getId(),(list.get(i)).getName()});
    	}
    	String dName=DataTools.getOptionByList(dNames, null, true);
    	model.put("dName", dName);*/
		Item item=itemService.getItemByHelpCode("423");//获取骨灰寄存费
		double pice=0;
		if(item!=null){
			pice=item.getPice();
		}
    	model.put("method", "save");
    	model.put("pice", pice);
    	return new ModelAndView("ye/ashesRecord/editAshesRecord",model);
    }
    /**
     * 死者姓名改变
     * @return
     */
    @RequestMapping(params = "method=dNameChange")
    @ResponseBody
    public JSONObject dNameChange(){
		JSONObject json = new JSONObject();
    	String id = getString("dNameId");
    	CommissionOrder co = commissionOrderService.getCommissionOrderById(id);
    	String dSex = co.getSexName();
    	int dAge = co.getAge();
    	String certificateCode = co.getCertificateCode();
    	Timestamp cremationTime = co.getCremationTime();
    	json.put("dSex", dSex);
    	json.put("dAge", dAge);
    	json.put("certificateCode", certificateCode);
    	json.put("cremationTime", cremationTime);
		return json;
    }
    /**
     * 保存骨灰寄存
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存骨灰寄存信息")
    @ResponseBody
    public JSONObject saveashesRecord(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("dNameId");
    		int count = ashesRecordService.getIfexit(id);//判断是否已经寄存
    		if (!"".equals(id) && count >= 1) {
    			setJsonByWarning(json, "该死者已寄存");
    			return json;
			}
    		String dName;
    		if(id!=null&&!id.equals("")){
    			Map<String, Object> map = new HashMap<String, Object>();
    			map.put("id", id);
    			map.put("goto", 2);
    			CommissionOrder co = commissionOrderService.getCommissionOrderByIdChoose(map);
        		dName = co.getName();
    		}else{
    			dName = getString("dName");
    		}
    		AshesRecord ashesRecord = new AshesRecord();
    		String uuid=UuidUtil.get32UUID();
    		ashesRecord.setId(uuid);
    		ashesRecord.setdName(dName);
    		String dSexName = getString("dSex");
    		Byte dSex;
    		if(dSexName.equals("男")){
    			dSex = 1;
    			ashesRecord.setdSex(dSex);
    		}
    		if(dSexName.equals("女")){
    			dSex = 2;
    			ashesRecord.setdSex(dSex);
    		}
    		ashesRecord.setCommissionId(id);
    		ashesRecord.setdAge(getInt("dAge"));
    		ashesRecord.setCremationTime(getDate("cremationTime"));
    		ashesRecord.setBeginDate(getDate("beginDate"));
    		ashesRecord.setAshesPositionId(getString("placeCode"));
    		ashesRecord.setEndDate(getDate("endDate"));
    		ashesRecord.setDepositLong(getInt("depositLong"));
    		ashesRecord.setsName(getString("sName"));
    		ashesRecord.setCode(getString("sCode"));
    		ashesRecord.setfAppellationId(getString("fAppellationId"));
    		ashesRecord.setsPhone(getString("sPhone"));
    		ashesRecord.setsAddr(getString("sAddr"));
    		ashesRecord.setComment(getString("comment"));
    		ashesRecord.setCreateUserId(getCurrentUser().getUserId());
    		ashesRecord.setCreateTime(DateTools.stringToTimestamp(getString("currentTime")));
//    		String serialNumber=jenerationService.getJenerationCode(Jeneration.GHJC_TYPE);//骨灰寄存流水号
//    		ashesRecord.setSerialNumber(serialNumber);
    		ashesRecord.setStatusFlag(Const.IsFlag_Yse);
    		//添加到续费记录表保存 
    		AshesRecordD asd=new AshesRecordD();
    		asd.setId(UuidUtil.get32UUID());
    		asd.setRecordId(uuid);
    		asd.setCode(getString("sCode"));
    		asd.setBeginDate(getDate("beginDate"));
    		asd.setEndDate(getDate("endDate"));
    		asd.setLongTime(getInt("depositLong"));
    		asd.setPayFlag(Const.Pay_No);
    		asd.setTotal(getDouble("total"));
    		ashesRecordService.addAshesRecord(ashesRecord,asd);
    	    //页面获取页码信息
    		String num = getString("pageNum");
    		int pageNum;
    		if(num==null||num.equals("0")||num.isEmpty()){
    			pageNum=1;
    		}else{
    			try {
    				pageNum = DataTools.stringToInt(num);
    			} catch (Exception e) {
    				pageNum=1;
    			}
    		}
    		setJsonBySuccess(json, "保存成功", "ashesRecord.do?method=list&pageNum="+pageNum);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
	/**
	 * 链接到显示页面
	 * @return
	 */
    @RequestMapping(params = "method=show")
    public ModelAndView showashesRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	AshesRecord ashesRecord = new AshesRecord();
    	//查询条件
    	Map<String,Object> maps=new HashMap<String, Object>();
    	String placeName = "";
		try {
			placeName = DataTools.getDecoder(getString("placeName"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String[] context = placeName.split(":");
    	//骨灰柜号
		maps.put("ashesPositionIdOnly", context[0]);
		maps.put("statusFlag", Const.IsFlag_Yse);
		List<AshesRecord> list=ashesRecordService.getAshesRecordList(maps);
		if(list.size()>0){
			ashesRecord =(AshesRecord)ashesRecordService.getAshesRecordList(maps).get(0);
			model.put("dName", ashesRecord.getdName());
			model.put("dSex", ashesRecord.getSexName());
			model.put("dAge", ashesRecord.getdAge());
			model.put("cremationTime", ashesRecord.getCremationTime());
			model.put("placeCode", ashesRecord.getAshesPositionId());
			model.put("beginDate", ashesRecord.getBeginDate());
			model.put("endDate", ashesRecord.getEndDate());
			model.put("depositLong", ashesRecord.getDepositLong());
			model.put("sName", ashesRecord.getsName());
			model.put("code", ashesRecord.getCode());
			model.put("sPhone", ashesRecord.getsPhone());
			model.put("sAddr", ashesRecord.getsAddr());
			model.put("comment", ashesRecord.getComment());	
			Appellation a=appellationService.getAppellationById(ashesRecord.getfAppellationId());
			String appellation ="";
			if(a!=null){
				appellation= a.getName();
			}
			model.put("appellation", appellation);			
		}
    
    	return new ModelAndView("ye/ashesRecord/showAshesRecord",model);
    }
    
    /**
     * 点击死者姓名查看详细信息
     * @return
     */
    @RequestMapping(params = "method=showMessage")
    public ModelAndView showMessage(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	String id =getString("id");
    	AshesRecord ashesRecord=new AshesRecord();
    	if(id!=null && !"".equals(id)){
    		ashesRecord=ashesRecordService.getAshesRecordById(id);
    	}
    	String appellation = (appellationService.getAppellationById(ashesRecord.getfAppellationId())).getName();
    	String name=userService.getNameById(ashesRecord.getTakeUserId());
    	model.put("userName", name);
    	model.put("ashesRecord",ashesRecord);
    	model.put("appellation", appellation);
    	return new ModelAndView("ye/ashesRecord/showMessage",model);
    }
    
    
    /**
	 * 进入列表页面
	 * @return
	 */
    @RequestMapping(params="method=ashesRecordList")
    public ModelAndView ashesRecordList(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//查询条件
		Map<String,Object> maps0=new HashMap<String, Object>();
		Map<String,Object> maps=new HashMap<String, Object>();
		String type = getString("type");
		String input = getString("name");
		Date beginDate = getDate("beginDate");
		String searchOption=Const.getSearchTypeOptionByString(type,new byte[]{
				Const.Search_Type_RegisterNumber,Const.Search_Type_DieName,
				Const.Search_Type_RegisterName,Const.Search_Type_RegisterAddr});
		
		Date endDate = getDate("endDate");
		maps0.put("begin", beginDate);
		maps0.put("end", endDate);
		maps.put("end", endDate);
		maps.put("begin", beginDate);
		if(type.equals("11")){
			maps0.put("code", input);
			maps.put("code", input);
		}
		if(type.equals("12")){
			maps0.put("dName", input);
			maps.put("dName", input);
		}
		if(type.equals("13")){
			maps0.put("sName", input);
			maps.put("sName", input);
		}
		if(type.equals("16")){
			maps0.put("ashesPositionId", input);
			maps.put("ashesPositionId", input);
		}
		//获取分页
		int pages[]=getPage();
    	
    	//获取记录最小值
		int min = pages[1]*(pages[0]-1);
    	maps0.put("statusFlag", Const.IsFlag_Yse);
    	maps0.put("min", min);
		maps0.put("num", pages[1]);
/*    	List<AshesRecord> list = ashesRecordService.getAshesRecordList(maps0);
    	String[] serialCode = new String[list.size()];
    	model.put("serialCode", serialCode);*/
		//插入查询条件
		
		//以下控制骨灰寄存查询状态：1为寄存中，2为已领出，默认为寄存中
		String checkType=getString("checkType");
		if("flagOut".equals(checkType)){
			maps.put("statusFlag", Const.IsFlag_No);
		}else if("flagIn".equals(checkType) || "".equals(checkType)){
			maps.put("statusFlag", Const.IsFlag_Yse);
		}
//		else if("flagAll".equals(checkType)){
//			
//		}
		PageInfo<AshesRecord> page=ashesRecordService.getAshesRecordPageInfo(maps, pages[0],pages[1], "");
		model.put("checkType", checkType);
		model.put("page", page);
		model.put("searchOption", searchOption);
		return new ModelAndView("ye/ashesRecord/ashesRecordList",model);
    }
    /**
	 * 列表形式链接到添加页面
	 * @return
	 */
    @RequestMapping(params = "method=listEdit")
    public ModelAndView listEditashesRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	AshesRecord ashesRecord = new AshesRecord();
        //页面获取页码信息
    		String num = getString("pageNum");
    		int pageNum;
    		if(num==null||num.equals("0")||num.isEmpty()){
    			pageNum=1;
    		}else{
    			try {
    				pageNum = DataTools.stringToInt(num);
    			} catch (Exception e) {
    				pageNum=1;
    			}
    		}
    		model.put("pageNum", pageNum);
    	//获取添加人姓名
    	model.put("agentUser", getCurrentUser().getName());
    	//获取系统时间
    	//DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	model.put("currentTime",DateTools.getTimeString("yyyy-MM-dd HH:mm:ss"));
    	//获取火化寄存位置
    	String placeCode = getString("placeCode");
    	model.put("placeCode", placeCode);
    	//获取火化时间
    	//预设当前日期
		Date cremationTime = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("cremationTime")!=null){
			cremationTime = getDate("cremationTime");
		}
		model.put("cremationTime", cremationTime);
		//获取存入时间
		Date beginDate = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("beginDate")!=null){
			beginDate = getDate("beginDate");
		}
		model.put("beginDate", beginDate);
		//获取到期时间
		Date endDate = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("endDate")!=null){
			endDate = getDate("endDate");
		}
		model.put("endDate", endDate);
		//与死者关系
		String fAppellationOption=appellationService.getAppellationOption(ashesRecord.getfAppellationId(),false);
		model.put("fAppellationOption", fAppellationOption);
    	
    	//获取死者姓名
    	Map<String,Object> comissionMaps = new HashMap<String, Object>();
    	String name = getString("name");
    	comissionMaps.put("name",name);
    	List<CommissionOrder> list =  commissionOrderService.getCommissionOrderList(null);
    	List<Object[]> dNames = new ArrayList<Object[]>();
    	for (int i=0;i<list.size();i++){
    		dNames.add(new Object[]{(list.get(i)).getId(),(list.get(i)).getName()});
    	}
    	String dName=DataTools.getOptionByList(dNames, null, true);
    	model.put("dName", dName);
    	model.put("method", "save");
    	return new ModelAndView("ye/ashesRecord/listEditAshesRecord",model);
    }
    /**
	 * 列表形式链接到添加页面
	 * @return
	 */
    @RequestMapping(params = "method=renew")
    public ModelAndView renewashesRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	//获取办理人姓名
    	model.put("agentUser", getCurrentUser().getName());
    	//获取系统时间
    	//DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	model.put("currentTime",DateTools.getTimeString("yyyy-MM-dd HH:mm:ss"));
    	//获取死者姓名 
    	String ashesRecordId = getString("ashesRecordId");
    	AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
    	String dName = ar.getdName();    	
    	String serialNumber=ar.getSerialNumber();
    	String dcode = ar.getDieCode();
    	Date endDate = ar.getEndDate();
    	
    	Item item=itemService.getItemByHelpCode("423");//获取骨灰寄存费,寄存费可在页面中自行修改
		double pice=0;
		if(item!=null){
			pice=item.getPice();
		}
		
    	model.put("pice", pice);
    	model.put("dcode", dcode);
    	model.put("endDate", endDate);
    	model.put("serialNumber", serialNumber);
    	model.put("dName", dName);
    	model.put("method", "saveRenew");
    	model.put("ashesRecordId", ashesRecordId);
    	return new ModelAndView("ye/ashesRecord/renewAshesRecord",model);
    }
    /**
     * 骨灰寄存续费
     * @return
     */
    @RequestMapping(params = "method=saveRenew")
    @SystemControllerLog(description = "骨灰寄存续费")
    @ResponseBody
    public JSONObject saveRenewAshesRecord(){
		JSONObject json = new JSONObject();
    	try {
    		String ashesRecordId = getString("ashesRecordId");
    		AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
        	String code = ar.getCode();
        	Date endDate = ar.getEndDate();
        	Map<String,Object> paramMap =new HashMap<String, Object>();
        	paramMap.put("recordId", ashesRecordId);
        	List<AshesRecordD> list=ashesRecordDService.getAshesRecordDList(paramMap);
        	/*
        	 * 此处有两种情况:1.之前都是已缴费
        	 * 				 2.之前有续期,但未缴费的
        	 * 
        	 * 更改成只有一条记录   2016.9.29
        	 */
//        	if(list.size()>0){//第二种情况 
        		AshesRecordD ashesRecordD =list.get(0);  
        		ashesRecordD.setEndDate(getDate("newEndDate"));
        		ashesRecordD.setTotal(ashesRecordD.getTotal()+getDouble("total"));
        		ashesRecordD.setLongTime(ashesRecordD.getLongTime()+getInt("longTime"));
        		ashesRecordD.setPayFlag(Const.Pay_No);
        		ashesRecordDService.updateAshesRecordD(ashesRecordD);
//        	}else if(list.size()==0){//第一种 情况 
//	        	AshesRecordD ashesRecordD=new AshesRecordD();
//	        	ashesRecordD.setId(UuidUtil.get32UUID());
//	        	ashesRecordD.setRecordId(ashesRecordId);
//	        	ashesRecordD.setCode(code);
//        		ashesRecordD.setBeginDate(endDate);
//        		ashesRecordD.setTotal(getDouble("total"));
//        		ashesRecordD.setEndDate(getDate("newEndDate"));
//        		ashesRecordD.setLongTime(getInt("longTime"));
//        		ashesRecordD.setPayFlag(Const.Pay_No);
//        		ashesRecordDService.addAshesRecordD(ashesRecordD);
//        	}
        	//骨灰寄存时间随着续费更改
        	AshesRecord ashesRecord=ashesRecordService.getAshesRecordById(ashesRecordId);
    		ashesRecord.setEndDate(getDate("newEndDate"));
    		ashesRecord.setDepositLong(ashesRecord.getDepositLong()+getInt("longTime"));
    		ashesRecordService.updateAshesRecord(ashesRecord);
    		
    		setJsonBySuccess(json, "续费成功", "ashesRecord.do");
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "续费失败，错误"+e.getMessage());
		}
		return json;
    }
    /**
	 * 骨灰寄存领出
	 * @return
	 */
    @RequestMapping(params = "method=out")
    public ModelAndView outashesRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
      	//获取办理人姓名
    	model.put("agentUser", getCurrentUser().getName());
    	//获取系统时间
    	//DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	model.put("currentTime",DateTools.getTimeString("yyyy-MM-dd HH:mm:ss"));
    	model.put("takeTime", DateTools.getThisDate());
    	//获取死者姓名
    	String ashesRecordId = getString("ashesRecordId");
    	AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
    	model.put("dName", ar.getdName());
		model.put("dSex", ar.getSexName());
		model.put("dAge", ar.getdAge());
		model.put("cremationTime", ar.getCremationTime());
		model.put("placeCode", ar.getAshesPositionId());
		model.put("beginDate", ar.getBeginDate());
		model.put("endDate", ar.getEndDate());
		model.put("sName", ar.getsName());
		model.put("code", ar.getCode());
		model.put("sPhone", ar.getsPhone());
		model.put("sAddr", ar.getsAddr());
		model.put("serialNumber", ar.getSerialNumber());//流水号
		model.put("comment", ar.getComment());
		String appellation = (appellationService.getAppellationById(ar.getfAppellationId())).getName();
		model.put("appellation", appellation);
    	model.put("method", "saveOut");
    	model.put("ashesRecordId", ashesRecordId);
    	return new ModelAndView("ye/ashesRecord/outAshesRecord",model);
    }
    /**
     * 保存骨灰寄存领出信息
     * @return
     */
    @RequestMapping(params = "method=saveOut")
    @SystemControllerLog(description = "保存骨灰寄存领出信息")
    @ResponseBody
    public JSONObject saveOutAshesRecord(){
		JSONObject json = new JSONObject();
    	try {
    		String ashesRecordId = getString("ashesRecordId");
    		AshesRecord ar = ashesRecordService.getAshesRecordById(ashesRecordId);
    		ar.setTakeTime(getDate("takeTime"));
    		ar.setTakeName(getString("takeName"));
    		ar.setTakeUserId(getCurrentUser().getUserId());
    		ar.setComment(getString("comment"));
    		ar.setTakePhone(getString("takePhone"));
    		ar.setStatusFlag(Const.IsFlag_No);
    		ashesRecordService.updateAshesRecord(ar);
    		setJsonBySuccess(json, "领出成功", "ashesRecord.do");
		} catch (Exception e) {
			setJsonByFail(json, "领出失败，错误"+e.getMessage());
		}
		return json;
    }
}
