package com.hz.controller.ye;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Freezer;
import com.hz.entity.system.User;
import com.hz.entity.ye.RepairInfo;
import com.hz.service.base.CarService;
import com.hz.service.base.FarewellService;
import com.hz.service.base.FreezerService;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.MourningService;
import com.hz.service.system.UserService;
import com.hz.service.ye.RepairService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 维修接口
 * @author cjb
 *
 */
@Controller
@RequestMapping("/repairRecord.do")
public class RepairController extends BaseController {
	@Autowired
	CarService cs;
	@Autowired
	private FarewellService farewellService;
	@Autowired
	private MourningService mourningService; 
	@Autowired
	private FurnaceService furnaceService;
	@Autowired
	private RepairService repairService;
	@Autowired
	private UserService userService;
	@Autowired 
	private FreezerService freezerService;

	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "repairRecord.do");
       model.addAttribute("user", getSession().getAttribute(Const.SESSION_USER));
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return editRepair();
    }
	
	@RequestMapping(params = "method=repair")
	private ModelAndView editRepair() {
		Map<String,Object> model=new HashMap<String,Object>();
		model = getModel(model);
		model.put("method", "save");
		String type = getString("type");
		if ("car".equals(type)) {//车辆维修
			String itemOption = cs.getCarOption(null);
			model.put("itemOption", itemOption);
			return new ModelAndView("ye/second_department/car/repairCar",model); 
		}
		String itemOption = null;
		if ("fare".equals(type)) {//告别厅维修
			itemOption = farewellService.getFarewellOption(null, false);
		} else if ("mour".equals(type)) {//灵堂维修
			itemOption = mourningService.getMourningOption(null, false);
		} else if ("furnace".equals(type)) {//火化炉维修
			itemOption = furnaceService.getFurnaceOption(null, false);
		}  else if("freezer".equals(type)){	//冰柜维修
			itemOption = freezerService.getFreezerOption(null, true);	
		}
		
		model.put("itemOption", itemOption);
		return new ModelAndView("ye/repairRecord/repairRecord",model);
	}
	
	@RequestMapping(params = "method=save")
	@ResponseBody
	public JSONObject arrangeFarewell(){
		JSONObject json = new JSONObject();
		try {
			RepairInfo repairInfo = new RepairInfo(); 
			Map<String,Object> map = new HashMap<String, Object>();
			String type = getString("type");
			repairInfo.setRepairFee(DataTools.stringToDouble(getString("fee"))==-1 ? 0 : DataTools.stringToDouble(getString("fee")));
			repairInfo.setComment(getString("reason"));
			if ("fare".equals(type)) {
				Calendar cal = Calendar.getInstance();
				Date startDate = getTimeM("startTime");
				cal.setTime(startDate);
				if (cal.get(Calendar.HOUR_OF_DAY) < 4) {
					cal.set(Calendar.HOUR_OF_DAY, 4);
				}
				if (cal.get(Calendar.HOUR_OF_DAY) >= 17) {
					cal.set(Calendar.HOUR_OF_DAY, 17);
				}
				repairInfo.setBeginTime(DateTools.dateToTimestamp(cal.getTime()));
				Date endDate = getTimeM("endTime");
				cal.setTime(endDate);
				if (cal.get(Calendar.HOUR_OF_DAY) >= 17) {
					cal.set(Calendar.HOUR_OF_DAY, 17);
				}
				if (cal.get(Calendar.HOUR_OF_DAY) < 4) {
					cal.set(Calendar.HOUR_OF_DAY, 4);
				}
				repairInfo.setEndTime(DateTools.dateToTimestamp(cal.getTime()));
			} else if ("furnace".equals(type)) {
				/*Calendar cal = Calendar.getInstance();
				Date startDate = new Date(getTimeM("startTime").getTime());
				cal.setTime(startDate);*/
//				if (cal.get(Calendar.HOUR_OF_DAY) < 3) {
//					cal.set(Calendar.HOUR_OF_DAY, 3);
//				}
//				if (cal.get(Calendar.HOUR_OF_DAY) > 2) {
//					cal.set(Calendar.HOUR_OF_DAY, 2);
//				}
//				repairInfo.setBeginTime(DateTools.dateToTimestamp(cal.getTime()));
				repairInfo.setBeginTime(getTimeM("startTime"));
				/*Date endDate = new Date(getTimeM("endTime").getTime());
				cal.setTime(endDate);*/
//				if (cal.get(Calendar.HOUR_OF_DAY) > 2) {
//					cal.set(Calendar.HOUR_OF_DAY, 2);
//				}
//				if (cal.get(Calendar.HOUR_OF_DAY) < 3) {
//					cal.set(Calendar.HOUR_OF_DAY, 3);
//				}
//				repairInfo.setEndTime(DateTools.dateToTimestamp(cal.getTime()));
				repairInfo.setEndTime(getTimeM("endTime"));
			} else {
				repairInfo.setBeginTime(getTimeM("startTime"));
				repairInfo.setEndTime(getTimeM("endTime")); 
			}
			repairInfo.setCreateTime(DateTools.getThisDateTimestamp());
			repairInfo.setCreateUserId(getString("reportId"));
			repairInfo.setId(UuidUtil.get32UUID());
			repairInfo.setItemId(getString("itemId"));
			repairInfo.setFlag(Const.IsFlag_Decrate);
			if ("car".equals(type)) {
				repairInfo.setRepairName(getString("repairName"));
			}
			if("freezer".equals(type)){
				Freezer freezer2 = new  Freezer();
				freezer2 = freezerService.getFreezerById(getString("itemId"));
				freezer2.setFlag(Const.IsFlag_Decrate);
				freezerService.updateFreezer(freezer2);
			}
			map.put("type", type);
			map.put("repairInfo", repairInfo);
			repairService.repair(map);
			if("freezer".equals(type)){
				setJsonBySuccess(json, "填写成功", true);
			} else{
			setJsonBySuccess(json, "填写成功", true);
			}
		} catch (Exception e) {
			setJsonByFail(json, "填写出错"+e.getMessage());
			e.printStackTrace();
		}
		return json;
	}
	
	/**
	 * 查看维修信息
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(params = "method=info")
	public ModelAndView showInfo() throws UnsupportedEncodingException {
		Map<String,Object> model=new HashMap<String,Object>();
		model = getModel(model);
		RepairInfo repairInfo = null;
		String itemOption = null;
		String itemId = getString("itemId");
		String id = getString("id");
		String type = getString("type");
		String userId = getString("createUserId");
		String userName = userService.getNameById(userId);
		model.put("userName", userName);
		if ("carInfo".equals(type)) {
			itemOption = cs.getCarOption(itemId);
			repairInfo = repairService.showCarInfo(id);
			model.put("repairInfo", repairInfo);
			model.put("itemOption", itemOption);
			model.put("method", "change");
			return new ModelAndView("ye/second_department/car/repairCar",model);
		}
		if ("mour".equals(type)) {
			itemOption = mourningService.getMourningOption(itemId, false);
			repairInfo = repairService.showMourInfo(id);
		} else if ("fare".equals(type)) {
			repairInfo = repairService.showFareInfo(id);
			itemOption = farewellService.getFarewellOption(itemId, false);
		} else if ("furnace".equals(type)) {
			repairInfo = repairService.showFurnaceInfo(id);
			itemOption = furnaceService.getFurnaceOption(itemId, false);
		}
		else if ("freezer".equals(type)) {
			repairInfo = repairService.showFreezerInfo(id);
			itemOption = freezerService.getFreezerOption(itemId, false);
		}
		model.put("currentUser", ((User)getSession().getAttribute(Const.SESSION_USER)));
		model.put("repairInfo", repairInfo);
		model.put("itemOption", itemOption);
//		model.put("itemName", new String(getString("iname").getBytes("iso8859-1"),"utf-8"));
		return new ModelAndView("ye/repairRecord/repairInfo",model);
	}
	/*@RequestMapping(params = "method=complete")
	@ResponseBody
	public JSONObject completeRepair() {
		JSONObject json = new JSONObject();
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			String reportId = getString("reportId");
			String type = getString("type");
			Timestamp now = DateTools.getThisDateTimestamp();
			map.put("type", type);
			map.put("endTime", now);
			map.put("id", reportId);
			repairService.completeRepair(map);
			setJsonBySuccess(json, "维修完成", true);
		} catch (Exception e) {
			setJsonByFail(json, "上报错误"+e.getMessage());
			e.printStackTrace();
		}
		return json;
	}*/
	@RequestMapping(params = "method=change")
	@ResponseBody
	private JSONObject changeRepair() {
		JSONObject json = new JSONObject();
		Map<String,Object> map = new HashMap<String, Object>();
		try {
			String id = getString("id");
			String type = getString("type");
			RepairInfo repairInfo = null;
			if ("mour".equals(type)) {
				repairInfo = repairService.showMourInfo(id);
			} else if ("fare".equals(type)) {
				repairInfo = repairService.showFareInfo(id);
			} else if ("furnace".equals(type)) {
				repairInfo = repairService.showFurnaceInfo(id);

			}	else if ("freezer".equals(type)) {
				repairInfo = repairService.showFreezerInfo(id);
				String currentTime =DateTools.getTimeString("yyyy-MM-dd HH:mm");
				String endTime = getString("endTime");
//		    	long endTime=repairInfo.getEndTime().getTime();
				//获取当前时间和结束时间时间差 ，来判断维修是否结束
//			long	currentTime = DateTools.getThisDate().getTime();
//				doublecha= currentTime -endTime;
				if("freezer".equals(type)){		
					Freezer freezer2 = new  Freezer();
					freezer2 = freezerService.getFreezerById(repairInfo.getItemId());
//					if (cha >=0 ){
					if(currentTime.compareTo(endTime)>0){
						freezer2.setFlag(Const.IsFlag_No);
						freezer2.setId(repairInfo.getItemId());
						repairInfo.setFlag(Const.IsFlag_No);
						freezerService.updateFreezer(freezer2);
					}
				}
				
			} else if ("carInfo".equals(type)) {
				repairInfo = repairService.showCarInfo(id);
				repairInfo.setRepairName(getString("repairName"));

			}
			repairInfo.setRepairFee(DataTools.stringToDouble(getString("fee")));
			repairInfo.setBeginTime(getTimeM("startTime"));
			repairInfo.setEndTime(getTimeM("endTime"));
			repairInfo.setItemId(getString("itemId"));
			repairInfo.setCreateUserId(getString("userId"));
			repairInfo.setCreateUserId(getString("reportId"));
			map.put("type", type);
			map.put("repairInfo", repairInfo);
			repairService.changeInfo(map);
			setJsonBySuccess(json, "更改成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "更改错误"+e.getMessage());
			e.printStackTrace();
		}
		return json;
	}
}
