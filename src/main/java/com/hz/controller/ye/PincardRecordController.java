package com.hz.controller.ye;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.PincardRecord;
import com.hz.service.ye.PincardRecordService;
import com.hz.util.Const;
/**
 * 销卡记录
 * @author hw
 *
 */
@Controller
@RequestMapping("/pincardRecord.do")
public class PincardRecordController extends BaseController{
	@Autowired
	private PincardRecordService pincardRecordService;
	 @ModelAttribute  
	    public void populateModel( Model model) {  
	       model.addAttribute("url", "pincardRecord.do");
	    }
		
		@RequestMapping
	    public ModelAndView unspecified() {
			return listpincardRecord();
	    }

		/**
		 * 火化委托单列表
		 * @return
		 */
	    @RequestMapping(params = "method=list")
	    public ModelAndView listpincardRecord(){
			Map<String,Object> model=new HashMap<String,Object>();
			model=getModel(model);
			//插入查询条件
			Map<String,Object> maps = new HashMap<String,Object>();
			String search = getString("search");
			Date start = getDate("startTime");
			Date end = getDate("endTime");
			maps.put("name", search);
			maps.put("start", start);
			maps.put("end", end);
			//获得页面页码信息
			int[] pages = getPage();
			PageInfo<PincardRecord> page=pincardRecordService.getPincardRecordPageInfo(maps,pages[0],pages[1],"creat_time asc");
			model.put("page", page);
			return new ModelAndView("ye/pincardRecord/listPincardRecord",model);
	    }
		 

}
