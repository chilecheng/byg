package com.hz.controller.ye;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Mourning;
import com.hz.entity.system.User;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.MourningRecord;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.MourningService;
import com.hz.service.system.UserService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.ItemOrderService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 灵堂记录信息
 * @author rgy
 *
 */
@Controller
@RequestMapping("/mourningRecord.do")
public class MourningRecordController extends BaseController{
	@Autowired
	private MourningRecordService mourningRecordService;
	@Autowired
	private FarewellRecordService farewellRecordService;
	
	@Autowired
	private MourningService mourningService;
	
	@Autowired
	private CommissionOrderService cs;
	@Autowired
	private ItemTypeService itemTypeService;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	private UserService userService;
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "mourningRecord.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listMourningRecord();
    }

	/**
	 * 灵堂记录列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listMourningRecord(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//预设当前日期
		Date date = DateTools.getThisDate();
		Map<String, Object> maps=new HashMap<String, Object>();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date = getDate("date");
		}
		model.put("date", date);
		List<Mourning> mList = mourningService.getMourningList(null,"");
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		
		maps.put("endDate", DateTools.getDayEndTime(DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), 6, Calendar.DATE)));
		List<MourningRecord> rList = mourningRecordService.getMourningRecordList(maps,"begin_time");
		//获取搜索日期的之后7天的所有日期List
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[8];
		dOb[0]="灵堂";
		//设置第一行（日期）
		for(int i=0;i<7;i++){
			dOb[i+1]=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), i, Calendar.DATE);
		}
		list.add(dOb);
		//设置默认列表数组
		for(int i=0;i<mList.size();i++){
			Mourning mouring = (Mourning)mList.get(i);
			list.add(new Object[]{mouring,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历所有记录，放在相应的位置
		for(int i=0;i<rList.size();i++){
			MourningRecord mr = rList.get(i);
			int row = 0;//行
			int col = 0;//列
			int endCol=0;//最后一列
			//对比灵堂，确认行数
			for(int j=1;j<list.size();j++){
				//把数组放在list中
				Object[] ob = list.get(j);
				Mourning m = (Mourning)ob[0];
				if(m.getId().equals(mr.getMourningId())){
					row = j;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++){
				Date da = (Date)dOb[d];
				if(isSomeDay(da, new Date(mr.getBeginTime().getTime()))){
					col = (d-1)*2+isPmOrAm(mr.getBeginTime());
				}
				if(isSomeDay(da, new Date(mr.getEndTime().getTime()))){
					endCol = (d-1)*2+isPmOrAm(mr.getEndTime());
				}
			}
			if(row!=0){
				Object[] o = (Object[])list.get(row);
				if(endCol==0){
					endCol = 14;
				}
				if(col==0){
					col=1;
				}
				for(int x=col;x<=endCol;x++){
					o[x] = mr;
				}
			}
			
		}
		model.put("list", list);
        model.put("method", "list");
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
        model.put("IsFlag_Bzwc",Const.IsFlag_Bzwc);
        model.put("IsFlag_QC", Const.IsFlag_QC);
        model.put("IsFlag_YY", Const.IsFlag_YY);
		return new ModelAndView("ye/mourningRecord/listMourningRecord",model);
    }
    
    /**
     * 进入列表页
     * @return
     */
    @RequestMapping(params="method=mourningList")
    public ModelAndView listMourning() {
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		String beginDate = getString("beginDate");
		String endDate = getString("endDate");
		if (beginDate != null && !"".equals(beginDate)) {
			maps.put("mourningbeginTime", beginDate);
		}
		if(endDate !=null && !"".equals(endDate)){
			maps.put("mourningendTime", endDate+"23:59:59");
		}
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		String type = getString("type");
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
		maps.put("comm", "yes");//排除维修的记录
		maps.put("checkFlag", Const.Check_Yes);//排除未审核 
		if ("now".equals(type)) {//近两天设灵
			if("".equals(beginDate)){
				maps.put("startTime", DateTools.getDayString(-2));
			}else{
				maps.put("startTime", DateTools.getAfterDays(beginDate,-2));
			}
			if("".equals(endDate)){
				maps.put("endTime", DateTools.getDayString(2));
			}else{
				maps.put("endTime", DateTools.getAfterDays(beginDate,2));
			}
			maps.put("flag", "now");
		} else if ("doing".equals(type)) {
			if("".equals(endDate)){
				maps.put("endTime", DateTools.getThisDateTimestamp());
			}else{
				maps.put("endTime", endDate+"23:59:59");
			}
			if("".equals(beginDate)){
				maps.put("startTime", DateTools.getThisDateTimestamp());
			}else{
				maps.put("startTime", beginDate);
			}
			maps.put("flag", "doing");//进行中
		} else if ("yes".equals(type)) {
			if("".equals(endDate)){
				maps.put("startTime", DateTools.getThisDateTimestamp());
			}else{
				maps.put("startTime", beginDate);
			}
			maps.put("flag", "yes");//已完成
		} else if ("no".equals(type)) {
			if("".equals(beginDate)){
				maps.put("startTime", DateTools.getThisDateTimestamp());
			}else{
				maps.put("startTime", beginDate);
			}
			maps.put("flag", "not");//未完成
		} 
		PageInfo<MourningRecord> page=mourningRecordService.getMourningRecordPageInfo(maps, pages[0],pages[1], "c.creat_time desc");
//		String searchOption = Const.getSearchTypeOption(new byte[]{1,3});
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Name,Const.Search_Type_Mour});
		model.put("name", getString("searchVal"));
		model.put("searchOption", searchOption);
		model.remove("method");
		model.put("method","mourningList");
		model.put("page", page);
		model.put("checkType", type);
		model.put("IsFlag_Bzwc",Const.IsFlag_Bzwc);
		model.put("IsFlag_Yse",Const.IsFlag_Yse);
		return new ModelAndView("ye/mourningRecord/listMourning",model);
		
    }
    /**
     * 死者灵堂使用详情
     * @return
     * @throws UnsupportedEncodingException 
     */
    @RequestMapping(params = "method=detail")
    public ModelAndView userDetail() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String id = getString("id");
		CommissionOrder co = cs.getCommissionOrderById(id);
		FarewellRecord fr = farewellRecordService.getFarewellRecordByOrderId(id);
		Map<String,Object> paramMap = new HashMap<String, Object>();
		String type = Const.getItemType(getString("type"));
		String typeId = itemTypeService.getIdByTypeName(type);
		paramMap.put("typeId", typeId);
		paramMap.put("cid", id);
		List<ItemOrder> list = itemOrderService.getItemOrderListByType(paramMap);
		if (fr != null) {
			model.put("gbTime",fr.getBeginDate());
		}
		String enterId = getString("enterId");
		if (enterId != null && !(enterId.equals(""))) {
			String enterUser = userService.getNameById(enterId);
			model.put("enterUser",enterUser);
		}
		model.put("co",co);
		try {
			model.put("arrTime",getTime("arrTime"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.put("mr_time",getString("mr_time"));
		model.put("mname",getString("mname"));
		model.put("flag", getString("flag"));
		model.put("list",list);
		return new ModelAndView("ye/mourningRecord/userMourningDetail",model);
    }
	/**
	 * 链接布置页面
	 * @return
	 */
    @RequestMapping(params="method=edit")
    public ModelAndView editMourningRecord(){
		Map<String,Object> model=new HashMap<String,Object>();
		model = getModel(model);
		String id = getString("id");
//		String homeType=getString("homeType");
		MourningRecord mourningRecord=new MourningRecord();
		CommissionOrder commissionOrder=new CommissionOrder();
//		Integer flag = null;//是否布置完成标志
		if(id!=null&&!id.equals("")){
			mourningRecord=mourningRecordService.getMourningRecordByOId(id);
		/*	Timestamp arrange_time =  mourningRecord.getArrangeTime();
			Date now = new Date();
			if (null != arrange_time && (now.getTime() - arrange_time.getTime()) > 0) {
				flag = 1;
			} else {
				flag = 0;
			}*/
			commissionOrder=cs.getCommissionOrderById(id);
			//首页列表记录查看状态
			if(commissionOrder.getViewsMourning()==0){
				commissionOrder.setViewsMourning(Const.One);
				cs.updateNumber(commissionOrder);
			}
		}
		Map<String,Object> paramMap = new HashMap<String, Object>();
		String type = Const.getItemType(getString("type"));
		String typeId = itemTypeService.getIdByTypeName(type);
		paramMap.put("typeId", typeId);
		paramMap.put("cid", id);
		List<ItemOrder> list = itemOrderService.getItemOrderListByType(paramMap);
		List<User> groupUserList = userService.getUserListByRoleName(Const.ARRANGER);
        model.put("mourningRecord", mourningRecord);
        model.put("commissionOrder", commissionOrder);
        model.put("groupUserList", groupUserList);
        model.put("method", "arrange");
        model.put("list",list);
        model.put("url","mourningRecord.do");
//        model.put("flag",flag);
//        model.put("homeType", homeType);
        model.put("user",getSession().getAttribute(Const.SESSION_USER));
        model.put("id",id);
		return new ModelAndView("ye/mourningRecord/editMourningRecord",model);
    }
    /**
     * 布置灵堂信息
     * @return
     */
    @RequestMapping(params = "method=arrange")
    @SystemControllerLog(description = "保存灵堂布置信息")
    @ResponseBody
    public JSONObject arrangeFarewell(){
		JSONObject json = new JSONObject();
    	try {
    		String[] handlerIds = getArrayString("handler");
    		String[] comments = getArrayString("comment");
    		String[] orderIds = getArrayString("oderId");
    		String id = getString("id");
    		MourningRecord mourningRecord = mourningRecordService.getMourningRecordByOId(id);
    		mourningRecord.setEnterId(getString("enterId"));
    		mourningRecord.setArrangeTime(getTimeM("arrangeTime"));
    		mourningRecord.setFlag(Const.IsFlag_Bzwc);
    		mourningRecordService.arrangeMourning(handlerIds,comments,orderIds,mourningRecord);
    		setJsonBySuccess(json, "布置完成", true);
    		json.put("refresh", getSession().getAttribute("refresh"));
    		String noRefresh = getString("noRef");
			if (noRefresh!=null && !noRefresh.equals("")) {
				json.put("noRefresh", true);
			}
		} catch (Exception e) {
			setJsonByFail(json, "布置失败，错误"+e.getMessage());
			e.printStackTrace();
		}
		return json;
    }
    /**
     * 判断是否为同一天
     * @param day1
     * @param day2
     * @return
     */
    private boolean isSomeDay(Date day1,Date day2){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ds1 = sdf.format(day1);
        String ds2 = sdf.format(day2);
        if (ds1.equals(ds2)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 判断上午下午
     * @param time
     * @return 1：上午 2：下午
     */
    private byte isPmOrAm(Timestamp time){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(time.getTime()));
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if(hour>=0 && hour<12){
			return (byte)1;
		}else{
			return (byte)2;
		}
		
    }
    
    /**
	  * 改变处理状态
	  */
	 @RequestMapping(params = "method=isdel")
	 @ResponseBody
	public JSONObject updateDealIsdel() {
		JSONObject json = new JSONObject();
		try {
			String id = getString("id");
			byte isdel = getByte("isdel");
			mourningRecordService.updateMourningRecordIsDeal(id, isdel);
			setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "操作失败,错误" + e.getMessage());
		}
		return json;
	}
    
   /* private Date getEndDate(int half) {
    	Date endDate = new Date();
    	Calendar ca = Calendar.getInstance();
    	ca.setTime(endDate);
    	if (half == 1) {
    		ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH), 12, 0, 0);
		} else if (half == 2) {
			ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		}
    	
    	return ca.getTime();
	}

	private Date getBeginDate(int half) {
		Date beginDate = new Date();
    	Calendar ca = Calendar.getInstance();
    	ca.setTime(beginDate);
    	if (half == 1) {
    		ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		} else if (half == 2) {
			ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH), 12, 0, 0);
		}
    	
    	return ca.getTime();
	}
	*/

	/*private Date getUntilDate(int half) {
		Date beginDate = new Date();
    	Calendar ca = Calendar.getInstance();
    	ca.setTime(beginDate);
    	if (half == 1) {
    		ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		} else if (half == 2) {
			ca.set(ca.get(Calendar.YEAR), ca.get(Calendar.MONTH), ca.get(Calendar.DAY_OF_MONTH), 12, 0, 0);
		}
    	return ca.getTime();
	}*/
}
