package com.hz.controller.ye;

import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.oldData.OldData;
import com.hz.entity.ye.CommissionOrder;
import com.hz.service.base.CorpseUnitService;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.service.base.ProveUnitService;
import com.hz.service.base.ToponymService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 委托业务综合查询
 * @author gpf
 */
@Controller
@RequestMapping("/commissionOrderCheck.do")
public class CommissionOrderCheckController extends BaseController {
	//地区
	@Autowired
	private ToponymService toponymService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	// 死亡类型
	@Autowired
	private DeadTypeService deadTypeService;
	// 死亡原因
	@Autowired
	private DeadReasonService deadReasonService;
	// 证明单位
	/*
	 * @Autowired private ProveUnitService proveUnitService;
	 */
	// 接尸单位
	@Autowired
	private CorpseUnitService corpseUnitService;
	//证明单位
	@Autowired
	private ProveUnitService proveUnitService;
	// 灵堂号
	/*
	 * @Autowired private MourningService MourningService;
	 */
	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "commissionOrderCheck.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listCommissionOrder();
	}

	/**
	 * 火化委托单列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listCommissionOrder() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		// 填入查询内容

		Map<String, Object> maps = new HashMap<String, Object>();
		String name = getString("name");
//		maps.put("checkFlag", Const.Check_Yes);综合查询不需要加审核条件
		maps.put("name", name);
		String cardCode = getString("cardCode");
		maps.put("cardCode", cardCode);
		
		//死亡时间
//		Date dTime = getDate("dTime");//开始
//		if(dTime !=null){
//			Timestamp dTimeBeginDate =  new Timestamp (DateTools.getDayFirstTime(dTime).getTime());
//			Timestamp dTimeEndDate =DateTools.getDayEndTime(DateTools.utilDateToSqlDate(dTime));
//			maps.put("dTimeBeginDate", dTimeBeginDate);
//			maps.put("dTimeEndDate", dTimeEndDate);
//		}
//		maps.put("dTime", getDate("dTime"));
		
		
		//死亡时间
		String dTime=getString("dTime");
		String dTimeEnd=getString("dTimeEnd");//结束
		if(!"".equals(dTime) && dTime!=null){
			maps.put("dTime", dTime);
		}
		if(!"".equals(dTimeEnd) && dTimeEnd!=null){
			maps.put("dTimeEnd", dTimeEnd+"23:59:59");
		}
		
		String  pickAddr =getString("pickAddr");
		maps.put("pickAddr", pickAddr);
		//接尸单位
		String  corpseUnitId= getString("corpseUnitId");
		maps.put("corpseUnitId", corpseUnitId);
		
		
//		deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
		String deadReasonId = getString("deadReasonId");
		maps.put("deadReasonId", deadReasonId);
		
		String  deadTypeId =  getString("deadTypeId");
		maps.put("deadTypeId", deadTypeId);
		
		//死者户籍
		String toponymId = getString("toponymId");
		if(toponymId !=null && !"".equals(toponymId)){
			String toponymName=toponymService.getToponymById(toponymId).getName();
			if("外市".equals(toponymName)){
				maps.put("toponymNameOutCity", "温州市");
			}else if("外省".equals(toponymName)){
				maps.put("toponymNameOutP", "浙江省");
			}else{
				maps.put("toponymName", toponymName);
			}
		}
		String code = getString("code");
		maps.put("code", code);
		model.put("code", code);
		//身份证号
		String  certificateCode= getString("certificateCode");
		maps.put("certificateCode", certificateCode);
		model.put("certificateCode",certificateCode );
		
		byte sex= getByte("sex");
		maps.put("sex", getString("sex"));
		String fName = getString("fName");
		maps.put("fName", fName);
		model.put("fName", fName);
		
		String  fPhone= getString("fPhone");
		maps.put("fPhone", fPhone);
		model.put("fPhone",  fPhone);

		//火化预约时间
//		Date cremationTime = getDate("cremationTime");
//		if(cremationTime != null){
//			Date cremationBeginDate = DateTools.getDayFirstTime(cremationTime);
//			Date cremationEndDate =DateTools.getDayEndTime(DateTools.utilDateToSqlDate(cremationTime));
//			maps.put("cremationBeginDate", cremationBeginDate);
//			maps.put("cremationEndDate", cremationEndDate);
//		}
//		maps.put("cremationTime", getDate("cremationTime"));
//		model.put("cremationTime", cremationTime  );
		
		String cremationTime=getString("cremationTime");
		String cremationTimeEnd=getString("cremationTimeEnd");
		if(!"".equals(cremationTime) && cremationTime!=null){
			maps.put("cremationTime", cremationTime);
		}
		if(!"".equals(cremationTimeEnd) && cremationTimeEnd!=null){
			maps.put("cremationTimeEnd", cremationTimeEnd+"23:59:59");
		}
		
		
		//登记日期
//		Date creatTime = getDate("creatTime");
//		if(creatTime!=null){
//				Date creatTimeBeginDate = DateTools.getDayFirstTime(creatTime);
//				Date creatTimeEndDate =DateTools.getDayEndTime(DateTools.utilDateToSqlDate(creatTime));
//				maps.put("creatTimeBeginDate", creatTimeBeginDate);
//				maps.put("creatTimeEndDate", creatTimeEndDate);
//		}
//		maps.put("creatTime", getDate("creatTime"));
//		model.put("creatTime", creatTime);
		String creatTime=getString("creatTime");
		String creatTimeEnd=getString("creatTimeEnd");
		if(!"".equals(creatTime) && creatTime!=null){
			maps.put("creatTime", creatTime);
		}
		if(!"".equals(creatTimeEnd) && creatTimeEnd!=null){
			maps.put("creatTimeEnd", creatTimeEnd+"23:59:59");
		}
		
		//到馆 时间
//		Date arriveTime = getDate("arriveTime");
//		if(arriveTime != null){
//			Date arriveBeginDate = DateTools.getDayFirstTime(arriveTime);
//			Date arriveEndDate =DateTools.getDayEndTime(DateTools.utilDateToSqlDate(arriveTime));
//			maps.put("arriveBeginDate", arriveBeginDate);
//			maps.put("arriveEndDate", arriveEndDate);
//			}
//		maps.put("arriveTime", getDate("arriveTime"));
//		model.put("arriveTime", arriveTime);
		String arriveTime=getString("arriveTime");
		String arriveTimeEnd=getString("arriveTimeEnd");
		if(!"".equals(arriveTime) && arriveTime!=null){
			maps.put("arriveTime", arriveTime);
		}
		if(!"".equals(arriveTimeEnd) && arriveTimeEnd !=null){
			maps.put("arriveTimeEnd", arriveTimeEnd+"23:59:59");
		}
		
		/*maps.put("freezerId", getString("freezerId"));*/
		String freezerName = getString("freezerName");
		maps.put("freezerName",freezerName);
		model.put("freezerName", freezerName);
		String  mourningName =  getString("mourningName");
		maps.put("mourningName",mourningName);
		model.put("mourningName", mourningName );
		String  farewellName = getString("farewellName");
		maps.put("farewellName", farewellName);
		model.put("farewellName",farewellName );
		
		//火化炉类型
		byte furance_flag =getByte("furnaceTypeId");
		maps.put("furnaceTypeId", furance_flag);
		
		String  furnaceName = getString("furnaceName");
		maps.put("furnaceName", furnaceName);
		model.put("furnaceName", furnaceName);
		
		// 火化时间
//		Date furnaceTime = getDate("furnaceTime");
//		if(furnaceTime != null){
//			Date furnaceTimeBeginDate = DateTools.getDayFirstTime(furnaceTime);
//			Date furnaceTimeEndDate =DateTools.getDayEndTime(DateTools.utilDateToSqlDate(furnaceTime));
//			maps.put("furnaceTimeBeginDate", furnaceTimeBeginDate);
//			maps.put("furnaceTimeEndDate", furnaceTimeEndDate);
//		}
//		maps.put("furnaceTime", getString("furnaceTime"));
//		model.put("furnaceTime", furnaceTime);
		String furnaceTime=getString("furnaceTime");
		String furnaceTimeEnd=getString("furnaceTimeEnd");
		if(!"".equals(furnaceTime) && furnaceTime!=null){
			maps.put("furnaceTime", furnaceTime);
		}
		if(!"".equals(furnaceTimeEnd) && furnaceTimeEnd!=null){
			maps.put("furnaceTimeEnd", furnaceTimeEnd+"23:59:59");
		}
		//按证明单位查询
		String proveUnitId=getString("proveUnitId");
		if(!"".equals(proveUnitId)&&proveUnitId!=null){
			maps.put("proveUnitId", proveUnitId);
		}
		
		PageInfo<CommissionOrder> page = commissionOrderService.getCommissionOrderPageInfo(maps, pages[0], pages[1],"name asc");
		// PageInfo<CommissionOrder>
		// page=commissionOrderService.getWeiTuoPageInfo(maps,pages[0],pages[1],"name asc");
//		CommissionOrder co = new CommissionOrder();
		String corpseUnitOption = corpseUnitService.getCorpseUnitOption(
				corpseUnitId, false);
		String deadReasonOption = deadReasonService.getDeadReasonOption(
				deadReasonId, false);
		String deadTypeOption = deadTypeService.getDeadTypeOption(
				deadTypeId, false);
		/*String registerOption = registerService.getRegisterOption(
				co.getRegisterId(), false);*/
		String toponymOption=toponymService.getToponymOption(toponymId,false);
		/*String furnaceTypeOption = furnaceTypeService.getFurnaceTypeOption(
				co.getFurnaceTypeId(), false);*/
		String sexOption = Const.getSexOption(sex, false);
		String furnaceTypeOption  = Const.getFuranceTypeOption(furance_flag,false);
		String proveUnitOption=proveUnitService.getProveUnitOption(proveUnitId,false);
		model.put("sexOption", sexOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("deadReasonOption", deadReasonOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("toponymOption", toponymOption);
		model.put("furnaceTypeOption", furnaceTypeOption);
		model.put("proveUnitOption", proveUnitOption);
		model.put("page", page);
		model.put("method", "list");
		model.put("name",name );
		model.put("cardCode",cardCode);
		return new ModelAndView("ye/commissionOrderCheck/listCommissionOrderCheck", model);
	}
	/**
	 * 首页业务全视图查询通道
	 * @return
	 */
	@RequestMapping(params = "method=show")
	public ModelAndView show() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		//查询条件
		String findByName=getString("findName");
		String findByCode=getString("findCode");
		Map<String,Object> paramMap=new HashMap<String, Object>();
		paramMap.put("cardCode", findByCode);
		paramMap.put("name", findByName);
		PageInfo<CommissionOrder> page = commissionOrderService.getCommissionOrderPageInfo(paramMap, pages[0], pages[1],"");
		model.put("page", page);
		return new ModelAndView("ye/commissionOrderCheck/showCheckList", model);
	}
	/**
	 * 点击姓名查看信息（打印）
	 * @return
	 */
	@RequestMapping(params = "method=print")
	public ModelAndView printCommissionOrderCheck() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String id=getString("id");
		CommissionOrder com=commissionOrderService.getCommissionCheckPrintById(id);
//		OldData old=oldDataService.getOldDataByID(id);
		model.put("com", com);
		model.put("url", "commissionOrderCheck.do");
		return new ModelAndView("ye/commissionOrderCheck/printCommissionOrderCheck", model);
	}
}
