package com.hz.controller.ye;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.MourningRecord;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.service.base.MourningService;
import com.hz.service.ye.AshesRecordService;
import com.hz.service.ye.BaseReductionService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.HardReductionDService;
import com.hz.service.ye.HardReductionService;
import com.hz.service.ye.MourningRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * ���Ѽ���������Ŀ
 * @author rgy
 *
 */
@Controller
@RequestMapping("/hardReductionD.do")
public class HardReductionDController extends BaseController{
	@Autowired
	private HardReductionDService hardReductionDService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "hardReductionD.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listHardReductionD();
    }

	/**
	 * ���Ѽ���������Ŀ�б�
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listHardReductionD(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//���ҳ��ҳ����Ϣ
		int[] pages = getPage();
		//�����ѯ����
		Map maps=new HashMap();
		PageInfo page=hardReductionDService.getHardReductionDPageInfo(maps,pages[0],pages[1],null);
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("ye/hardReductionD/listHardReductionD",model);
    }
	/**
	 * ���ӵ��޸�����ҳ��
	 * @return
	 */
    @RequestMapping(params = "method=edit")
    public ModelAndView editHardReductionD(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		HardReductionD hardReductionD=new HardReductionD();
		if(id!=null&&!id.equals("")){
			hardReductionD=hardReductionDService.getHardReductionDId(id);
		}
        model.put("hardReductionD", hardReductionD);
        model.put("method", "save");
		return new ModelAndView("ye/hardReductionD/listHardReductionD",model);
    }
    /**
     * �������Ѽ���������Ŀ
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "�������Ѽ���������Ŀ��Ϣ")
    @ResponseBody
    public JSONObject saveHardReductionD(){
		JSONObject json = new JSONObject();
    	try {
    		String id =getString("id");
    		HardReductionD hardReductionD=new HardReductionD();
    		if (id!=null&&!id.equals("")) {
    			hardReductionD=hardReductionDService.getHardReductionDId(id);
    		}
    		hardReductionDService.addOrUpdate(id,hardReductionD);
    		setJsonBySuccess(json, "����ɹ�", true);
		} catch (Exception e) {
			setJsonByFail(json, "����ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
    
    /**
     * ɾ�����Ѽ���������Ŀ
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "ɾ�����Ѽ���������Ŀ��Ϣ")
    @ResponseBody
    public JSONObject deleteHardReductionD(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("ids");
	    	hardReductionDService.deleteHardReductionD(id);
    		setJsonBySuccess(json, "ɾ���ɹ�", true);
		} catch (Exception e) {
			setJsonByFail(json, "ɾ��ʧ�ܣ�����"+e.getMessage());
		}
		return json;
    }
}
