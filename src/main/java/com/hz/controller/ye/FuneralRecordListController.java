package com.hz.controller.ye;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Farewell;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.ListFueralRecord;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.FuneralRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 出殡礼仪的火化委托单信息
 * 
 * @author czl
 * 
 */ 
@Controller
@RequestMapping("funeralRecordList.do")
public class FuneralRecordListController extends BaseController {
	@Autowired
	private FuneralRecordService funeralRecordService;

	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "funeralRecordList.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listFuneral();
	}
	/**
	 * 出殡礼仪调度
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	private ModelAndView listFuneral() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 用于传给后台的数据 存储
		Map<String, Object> paramMap = new HashMap<String, Object>();
		// 预设当前日期
//		Date startTime = DateTools.getThisDateUtil();
		Date startTime=null;
		try {
			startTime = getToDayDate();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		
//		Date endTime = DateTools.getThisDateUtil();
		// 判断页面是否有传日期回来，是则赋值给date
		if (getDate("startTime") != null) {
			startTime = getDate("startTime");
		}
		model.put("startTime", startTime);
		paramMap.put("startTime", DateTools.getDayFirstTime(new java.sql.Date(startTime.getTime())));
		paramMap.put("endTime",DateTools.getDayEndTime(new java.sql.Date(startTime.getTime())));
		// 获取所有的出殡的人员名单
		List<ListFueralRecord> flist = funeralRecordService.getFuneralRecord(paramMap, "");
		
		List<Object[]> blist = new ArrayList<Object[]>();
		// 将时间分成需要的 时间段 并装入数组
		Object[] ob  = new Object[4];
		Object[] ob1 = new Object[4];
		Object[] ob2 = new Object[4];
		Object[] ob3 = new Object[4];
		Object[] ob4 = new Object[4];
		Object[] ob5 = new Object[4];
		Object[] ob6 = new Object[4];
		Object[] ob7 = new Object[4];
		Object[] ob8 = new Object[4];
		getHalfts(ob,  startTime, 2);
		getHalfts(ob1, startTime, 4);
		getHalfts(ob2, startTime, 6);
		getHalfts(ob3, startTime, 8);
		getHalfts(ob4, startTime, 10);
		getHalfts(ob5, startTime, 12);
		getHalfts(ob6, startTime, 14);
		getHalfts(ob7, startTime, 0);
		getHalfts(ob8, startTime, 16);
	    blist.add(ob);blist.add(ob1);blist.add(ob2);blist.add(ob3);blist.add(ob4);blist.add(ob5);blist.add(ob6);blist.add(ob7);blist.add(ob8);
        // 给一个时段的名单放在一个 Object数组
	    Object[] result=  new Object[16];
	    Object[] result1= new Object[16];
	    Object[] result2= new Object[16];
	    Object[] result3= new Object[16];
	    Object[] result4= new Object[16];
	    Object[] result5= new Object[16];
	    Object[] result6= new Object[16];
	    Object[] result7= new Object[16];
	    Object[] result8= new Object[16];
		// 遍历符合要求的 成员 放在对应的位置
		get(flist, ob,  result);
		get(flist, ob1, result1);
		get(flist, ob2, result2);
		get(flist, ob3, result3);
		get(flist, ob4, result4);
		get(flist, ob5, result5);
		get(flist, ob6, result6);
		get(flist, ob7, result7);
		get(flist, ob8, result8);
		//最终数据列表
		model.put("result",  result);
		model.put("result1", result1);
		model.put("result2", result2);
		model.put("result3", result3);
		model.put("result4", result4);
		model.put("result5", result5);
		model.put("result6", result6);
		model.put("result7", result7);
		model.put("result8", result8);
        //时刻 列表
		model.put("time",  ob);
		model.put("time1", ob1);
		model.put("time2", ob2);
		model.put("time3", ob3);
		model.put("time4", ob4);
		model.put("time5", ob5);
		model.put("time6", ob6);
		model.put("time7", ob7);
		model.put("time8", ob8);
		model.put("flist", flist);
		model.put("IsFlag_Yse",Const.IsFlag_No);
		model.put("IsFlag_Lock", Const.IsFlag_Yse);
		return new ModelAndView("ye/funeralProcession/funeralProcession", model);
	}

	//首次点击 进入列表
	@RequestMapping(params="method=begin")
	private ModelAndView listFuneralRecird(){
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		int[] pages = getPage();
		Map<String, Object> maps = new HashMap<String, Object>();
		//获取当前的时间
		Date startTime=DateTools.getThisDateUtil();
		Date endTime=DateTools.getThisDateUtil();
		if(getDate("startTime")!=null&&getDate("endTime")!=null){
			startTime=getDate("startTime");
			endTime=getDate("endTime");
			maps.put("startTime", DateTools.getDayFirstTime(new java.sql.Date(
				startTime.getTime())));
			maps.put("endTime", DateTools.getDayEndTime(new java.sql.Date(
				endTime.getTime())));
		}else{
			maps.put("startTime", DateTools.getDayFirstTime(new java.sql.Date(
					startTime.getTime())));
			maps.put("endTime", DateTools.getDayEndTime(new java.sql.Date(
					endTime.getTime())));
		}
		model.put("startTime", startTime);
		model.put("endTime", endTime);
		PageInfo<ListFueralRecord> page = funeralRecordService.getFarewellRecordPageInfo(maps, pages[0], pages[1], "");		
		List<ListFueralRecord> list = funeralRecordService.getFuneralRecord(maps, "");
		//下拉选数据
		String searchOption = Const.getSearchTypeOption(new byte[] { 1, 2 });
		model.put("list", list);
		model.put("searchOption", searchOption);// 选择条件
		model.put("method", "listInfo");
		model.put("page", page);
		return  new ModelAndView("ye/funeralProcession/listFuneralProcession",model);
	}
	
	/**
	 * 礼仪出殡的火化委托单记录列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=listInfo")
	private ModelAndView listFuneralRecords() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		// 获取查询类型 及 对面输入框内的值
		Map<String, Object> maps = new HashMap<String, Object>();
		// 获取 前后时间
		Date startTime = (Date)getDate("startTime");
		Date endTime = (Date)getDate("endTime");
		// 判断时间是否为空   为空则将今天的时间 赋值给 他们
		if (startTime != null && endTime != null) {
			maps.put("startTime", DateTools.getDayFirstTime(new java.sql.Date(
					startTime.getTime())));
			maps.put("endTime", DateTools.getDayEndTime(new java.sql.Date(
					endTime.getTime())));
		}
		String searchType = getString("searchType");
		String searchVal  = getString("searchVal");
		maps.put("searchVal", searchVal);
		maps.put("searchType", searchType);
		PageInfo<ListFueralRecord> page = funeralRecordService
				.getFarewellRecordPageInfo(maps, pages[0], pages[1], "");
		//下拉选数据
		String searchOption = Const.getSearchTypeOptionByString(searchType,new byte[] { 1, 2 });
		model.put("searchOption", searchOption);// 选择条件
		model.put("method", "listInfo");
		model.put("page", page);
		model.put("name", searchVal);
		model.put("startTime", startTime);
		model.put("endTime", endTime);
		return new ModelAndView("ye/funeralProcession/listFuneralProcession",model);
	}

	/**
	 * 判断 是否为对应时段的  数据
	 * @param flist 收索出来的所有死者名单
	 * @param b 时间片段
	 * @param result 最后赋值的结果
	 */
	private static void get(List<ListFueralRecord> flist,Object[] b,Object[] result){
		for(ListFueralRecord f:flist){
			for(int i=0;i<4;i++){
				Date  date = (Date)b[i];
				Timestamp newTime=DateTools.getThisDateTimestamp();
				if(f.getFarewellTime()!=null){  //如果有告别时间取告别时间，如果没有则取火化预约时间
					newTime=f.getFarewellTime();
				}else{
					newTime=f.getCremationTime();
				}
				if(isSameHalfHour(date, new Date(newTime.getTime() ))){
					if(i==0){
					    if(result[0]==null){
					    	result[0]=f;
					    }else{
					    	if(result[1]==null){
					    		result[1]=f;
					    	}else{
					    		if(result[2]==null){
					    			result[2]=f;
					    		}else{
					    			if(result[3]==null){
					    				result[3]=f;
					    			}
					    		}
					    	}
					    }
					}
                   if(i==1){
                	   if(result[4]==null){
					    	result[4]=f;
					    }else{
					    	if(result[5]==null){
					    		result[5]=f;
					    	}else{
					    		if(result[6]==null){
					    			result[6]=f;
					    		}else{
					    			if(result[7]==null){
					    				result[7]=f;
					    			}
					    		}
					    	}
					    }
					}
                  if(i==2){
                	  if(result[8]==null){
					    	result[8]=f;
					    }else{
					    	if(result[9]==null){
					    		result[9]=f;
					    	}else{
					    		if(result[10]==null){
					    			result[10]=f;
					    		}else{
					    			if(result[11]==null){
					    				result[11]=f;
					    			}
					    		}
					    	}
					    }
	
                    }
                  if(i==3){
                	  if(result[12]==null){
					    	result[12]=f;
					    }else{
					    	if(result[13]==null){
					    		result[13]=f;
					    	}else{
					    		if(result[14]==null){
					    			result[14]=f;
					    		}else{
					    			if(result[15]==null){
					    				result[15]=f;
					    			}
					    		}
					    	}
					    }
	
                    }
				}
			}
		}
	}
	/**
	 * 判断是过半还是没有过半
	 * 
	 * @param time
	 * @return 1：上半时 2：下半时
	 */
	public byte ifHalft(Timestamp time) {
		// 时间转换对象
		Calendar cal = Calendar.getInstance();
		// 设置时间
		cal.setTime(new Date(time.getTime()));
		int min = cal.get(Calendar.MINUTE);
		if (min >= 0 && min < 30) {
			return (byte) 1;
		} else {
			return (byte) 2;
		}
	}

	/**
	 * 获取当前时间点的开始时间
	 * 
	 * @param i
	 * @return
	 */
	private Date getBeginDate(int i) {
		Date beginDate = new Date();
		Calendar ca = Calendar.getInstance();
		ca.setTime(beginDate);
		if (i == 1) {
			ca.set(Calendar.MINUTE, 0);
			ca.set(Calendar.SECOND, 0);
		} else if (i == 2) {
			ca.set(Calendar.MINUTE, 30);
			ca.set(Calendar.SECOND, 0);
		}
		return ca.getTime();
	}

	/**
	 * 获取当前时间点的结束时间
	 * 
	 * @param i
	 * @return
	 */
	private Date getEndDate(int i) {
		Date beginDate = new Date();
		Calendar ca = Calendar.getInstance();
		ca.setTime(beginDate);
		if (i == 1) {
			ca.set(Calendar.MINUTE, 30);
			ca.set(Calendar.SECOND, 0);
			ca.set(Calendar.MILLISECOND, 000);
		} else if (i == 2) {
			ca.add(Calendar.HOUR_OF_DAY, 1);
			ca.set(Calendar.MINUTE, 0);
			ca.set(Calendar.SECOND, 0);
			ca.set(Calendar.MILLISECOND, 000);
		}
		return ca.getTime();
	}

	/**
	 * 获取当前时间之前的最晚时间
	 * 
	 * @param half
	 * @return
	 */
	private Date getUntilDate(int half) {
		Date beginDate = new Date();
		Calendar ca = Calendar.getInstance();
		ca.setTime(beginDate);
		if (half == 1) {
			ca.set(Calendar.MINUTE, 0);
			ca.set(Calendar.SECOND, 0);
			ca.set(Calendar.MILLISECOND, 000);
		} else if (half == 2) {
			ca.set(Calendar.MINUTE, 30);
			ca.set(Calendar.SECOND, 0);
			ca.set(Calendar.MILLISECOND, 000);
		}
		return ca.getTime();
	}

	/**
	 * 获取一个时间的后半小时 仅连续4段
	 * 只能得出半小时之后的时间
	 * @param ob 数组对象
	 * @param startTime 开始时间
	 * @param index  
	 */
	private static void getHalfts(Object[] ob, Date startTime, int index) {
		ob[0] = DateTools.getHalfHourAfterHalfHour(
				new Date(startTime.getTime()), index, 0, Calendar.MINUTE);
		ob[1] = DateTools.getHalfHourAfterHalfHour(
				new Date(startTime.getTime()), index, 30, Calendar.MINUTE);
		ob[2] = DateTools.getHalfHourAfterHalfHour(
				new Date(startTime.getTime()), index + 1, 0, Calendar.MINUTE);
		ob[3] = DateTools.getHalfHourAfterHalfHour(
				new Date(startTime.getTime()), index + 1, 30, Calendar.MINUTE);

	}

	/**
	 * 判断是否为同半小时
	 * 
	 * @param date1    表中时间
	 * @param date2   查出的火化委托单的时间
	 * @return
	 */
	public static boolean isSameHalfHour(java.util.Date da1, java.util.Date da2) {
		long timeBegin = da1.getTime();
		long timeEnd = da1.getTime() + 30 * 60 * 1000;
		long time2 = da2.getTime();
		if (time2 >= timeBegin && time2 < timeEnd) {
			return true;
		} else {
			return false;
			
		}
		
	}
	/**
	 * 获取当前的日期并转化为00：00：00 时刻
	 * @return 
	 * @throws ParseException
	 */
	public  java.util.Date  getToDayDate() throws ParseException{
		java.util.Date xx=new java.util.Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	java.util.Date date= new java.util.Date(xx.getTime());
	String now=sdf.format(date);
	java.util.Date date1 = (java.util.Date)sdf.parse(now);
	return date1;
	}


}
