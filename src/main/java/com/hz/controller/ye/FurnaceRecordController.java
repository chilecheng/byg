package com.hz.controller.ye;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Furnace;
import com.hz.entity.system.User;
import com.hz.entity.ye.ComOrSpeFuneralRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FireCheckInInfo;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.entity.ye.PaperInfo;
import com.hz.entity.ye.PolitenessInfo;
import com.hz.service.base.FurnaceService;
import com.hz.service.system.UserService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FireCheckInService;
import com.hz.service.ye.FurnaceRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 火化炉调度
 * @author chenjianbin
 */
@Controller
@RequestMapping("/furnaceRecord.do")
public class FurnaceRecordController extends BaseController{
	@Autowired
	private FurnaceService furnaceService;
	@Autowired
	private FurnaceRecordService furService;
	@Autowired
	private CommissionOrderService cs;
	@Autowired
	private FireCheckInService fireCheckInService;
	@Autowired
	private UserService userService;
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "furnaceRecord.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified(Model model) {
		return listFurnace();
    }

	/**
	 * 火化炉调度信息表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listFurnace(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//预设当前日期
		Date date = DateTools.getThisDateUtil();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date = getDate("date");
		}
		model.put("date", date);
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("furnaceBeginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("furnaceEndDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		List<FurnaceRecord> frList = furnaceService.getFurnaceRecordList(maps);
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[25];		
		dOb[0]="炉号";
		for (int i = 0; i < dOb.length-1; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 3, i, Calendar.HOUR);
		}
		list.add(dOb);
		Object[] dObTen = new Object[25];		
		dObTen[0]="炉号";
		for (int i = 0; i < dObTen.length-1; i++) {
			dObTen[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,20,Calendar.HOUR);
		}
		Object[] dObNine = new Object[25];		
		dObNine[0]="炉号";
		for (int i = 0; i < dObNine.length-1; i++) {
			dObNine[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,30,Calendar.HOUR);
		}
		Object[] dObEight = new Object[25];		
		dObEight[0]="炉号";
		for (int i = 0; i < dObEight.length-1; i++) {
			dObEight[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,40, Calendar.HOUR);
		}
		
		for (Furnace furnace : furnaceList) {
			Object[] o = new Object[dOb.length];
			o[0] = furnace;
			list.add(o);
		}
		for (FurnaceRecord furnaceRecord : frList) {
			Timestamp endTime = furnaceRecord.getEndTime();
			Timestamp orderTime = furnaceRecord.getOrderTime();
			byte flag=furnaceRecord.getFlag();
			if (endTime != null && !(endTime.equals(""))) {
				String endTimeStr = DateTools.getTimeFormatString("mm", endTime);
				if (("30".equals(endTimeStr)||"20".equals(endTimeStr)||"40".equals(endTimeStr))) {
					endTime = DateTools.quzheng(endTime);
				}
			}
			if (orderTime != null && !(orderTime.equals(""))) {
				String orderTimeStr = DateTools.getTimeFormatString("mm", orderTime);
				if (("30".equals(orderTimeStr)||"20".equals(orderTimeStr)||"40".equals(orderTimeStr))) {
					orderTime = DateTools.quzheng(orderTime);
				}
			}
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;//列
			for (int i = 1; i < list.size(); i++) {
				Object[] o = list.get(i);
				Furnace furnace = (Furnace) o[0];
				if (furnace.getId().equals(furnaceRecord.getFurnaceId())) {
					row = i;
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if (endTime != null && !(endTime.equals(""))) {
					if(DateTools.isSameHour(da, new Date(furnaceRecord.getBeginTime().getTime()))) {
						col = d;
					}
					if(DateTools.isSameHour(da, new Date(endTime.getTime()))){
						endCol = d;
					}
				} else {
					if (orderTime != null && !(orderTime.equals(""))) {
						if(DateTools.isSameHour(da, new Date(orderTime.getTime()))) {
							col = d;
						}
					}
				}
			}
			if(row!=0){
				Object[] o = (Object[])list.get(row);
				if (furnaceRecord.getEndTime() != null && !(furnaceRecord.getEndTime().equals(""))) {
					if(endCol==0){
						endCol = dOb.length-1;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						//业务需求：如果维修与业务同时存在，优先显示业务
						if(flag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
							o[x] = furnaceRecord;
						}
						if(flag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
							o[x] = furnaceRecord;
						}
					}
				} else {
					if (col != 0) {
						o[col] = furnaceRecord;
					}
				}
				
			}
		}
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Wc", Const.IsFlag_Wc);
		model.put("IsFlag_Jx", Const.IsFlag_Jx);
		model.put("method", "list");
		model.put("list", list);
		model.put("dOb", dOb);
		model.put("dObTen",dObTen);
		model.put("dObNine",dObNine);
		model.put("dObEight",dObEight);
		model.put("list_size", list.size());
		return new ModelAndView("ye/furnaceRecord/furnaceRecord",model);
    }
    /**
	 * 火化炉记录列表
	 * @return
	 */
    @RequestMapping(params="method=listInfo")
    public ModelAndView editFarewellRecord1() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		String type = getString("type");
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		Date beginDate = getDate("beginDate");
		Date endDate = getDate("endDate");
		if (beginDate != null && !"".equals(beginDate)) {
			maps.put("furnaceBeginDate", DateTools.getDayFirstTime(new java.sql.Date(beginDate.getTime())));
		}
		if (endDate != null && !"".equals(endDate)) {
			maps.put("furnaceEndDate", DateTools.getDayEndTime(new java.sql.Date(endDate.getTime())));
		}
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		maps.put("name", getString("name"));
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
		maps.put("comm", "yes");
		maps.put("checkFlag", Const.Check_Yes);
		maps.put("changeFlag", Const.ChangeFlag_No);
//		maps.put("itemSort", ItemConst.HuoHua_Sort);
		PageInfo<FurnaceRecord> page=furnaceService.getFurnaceRecordPageInfo(maps, pages[0],pages[1], "c.creat_time desc ");
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Name,Const.Search_Type_Card});
		model.put("searchOption", searchOption);
		
		model.put("method","listInfo");
		model.put("page", page);
		model.put("type", type);
		return new ModelAndView("ye/furnaceRecord/listFurnace",model);
    }
    /**
     * 死者火化炉使用详情
     * @return
     * @throws UnsupportedEncodingException 
     */
	@RequestMapping(params = "method=detail")
    public ModelAndView userDetail() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String id = getString("id");
		CommissionOrder co = cs.getCommissionOrderById(id);
		model.put("co",co);
		String wkId = getString("wkId");
		String crId = getString("crId");
		model.put("worker",userService.getNameById(wkId));
		model.put("creater",userService.getNameById(crId));
		return new ModelAndView("ye/furnaceRecord/userFurnaceDetail",model);
    }
    /**
	 * 业务三科 火化确认
	 * @return
	 */
	@RequestMapping(params = "method=confirm")
	public ModelAndView confirm() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String cardCode = cs.getCardCodeById(getString("id"));
		FireCheckInInfo listfc = fireCheckInService.getBasicInfoByCard(cardCode);
		if (listfc != null) {
			model.put("listfc", listfc);
			model.put("listfc_sex", listfc.getSexName());// !!!
			model.put("cremation_flag", listfc.getCremationOrNot());
		}
		PaperInfo pinfo = fireCheckInService.getPaperInfoByCard(cardCode, "纸棺");
		if (pinfo != null && pinfo.getPaperlist() != null && pinfo.getPaperlist().size() > 0)// >0
		{
			model.put("pinfo", pinfo.getPaperlist());// .get(0).getPaperName()
		}
		PolitenessInfo poinfo = fireCheckInService.getPolitenessInfoByCard(cardCode, 3);
		if (poinfo != null && poinfo.getPolitenesslist() != null && poinfo.getPolitenesslist().size() > 0)// >0
		{
			model.put("poinfo", "有");
		} else {
			model.put("poinfo", "无");
		}
		return new ModelAndView("ye/third_department/registration/showFireInfoList", model);
	}

	/**
	 * 火化入炉(业务三科) 转炉(调度科)
	 */
	@RequestMapping(params = "method=arrange")
	public ModelAndView arrange(){
		Map<String, Object> model = new HashMap<String, Object>();
		model=getModel(model);
		String cremationFlag = getString("code9");
		String changeto = getString("changeto");
//		String type = getString("type");
		String cardCode = cs.getCardCodeById(getString("id"));
		String cid = getString("id");
		String furnaceType = getString("code6");
		if ("普通炉".equals(furnaceType)) {
			model.put("furnaceType", "pt");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", getString("id"));
		map.put("goto", 1);
		CommissionOrder co =  cs.getCommissionOrderByIdChoose(map);
		model.put("fphone", co.getfPhone());
		model.put("dname", co.getName());
		model.put("fname", co.getfName());
		model.put("code", co.getCode());
		model.put("sex", co.getSexName());
		model.put("age", co.getAge());
		if(co.getCremationTime()!=null&&!"".equals(co.getCremationTime())){
			model.put("cremationtime", DateTools.getTimeFormatString("yyyy-MM-dd HH:mm",co.getCremationTime()));
		}else{
			model.put("cremationtime","");
		}
		model.put("recordId", getString("code11"));
		model.put("cremationFlag", cremationFlag);
		model.put("changeto", changeto);
		FireCheckInInfo lf = fireCheckInService.getBasicInfoByCard(cardCode);
		String fireworkerOption = fireCheckInService.getFireWorkerOption(null, false);
		model.put("fireworkerOption", fireworkerOption);
		if (lf != null) {
			if (lf.getFurnaceFlag() == Const.furnace_ty) {//针对特约炉的操作(入炉或者转炉)
				String fireid = fireCheckInService.findFurnaceIdBycommissionId(cid);
				if (fireid != null) {
					String furnaceOption = null;
					if (changeto.equals("pt")) {//特约炉转普通炉，则拿到所有普通炉号
						furnaceOption = fireCheckInService.getAllSpecialOrCommonFurnaceOption(null, false,"普通炉");//
					} else {//特约炉入炉或者特约转特约，则拿到所有特约炉号
						furnaceOption = fireCheckInService.getAllSpecialOrCommonFurnaceOption(fireid, false,"特约炉");//
						
						furService.updateNumber(getString("code11"),Const.One);
						/*//首页查看姿态
						FurnaceRecord furnace=furService.getFurnaceById(getString("id"));
						if(furnace.getViewsTY()==0){
							furnace.setViewsTY(Const.One);
						}*/
					}
					model.put("FurnaceOption", furnaceOption);
					model.put("fireid", fireid);// 特约炉id
					model.put("spe_or_com_flag", true);// true代表特约炉
				}

			} else if (lf.getFurnaceFlag() == Const.furnace_pt) {
				String furnaceOption = null;
				if (changeto.equals("ty")) {//普通炉转特约炉
					furnaceOption = fireCheckInService.getAllSpecialOrCommonFurnaceOption(null, false, "特约");
				} else {//普通炉入炉
					model.put("type", "change");//加了这个变量，普通炉入炉时可以改变炉号
					model.put("ptrulu", "yes");//普通炉入炉专有标志，防止在入炉方法里被判断为普通炉转特约炉
					furnaceOption = fireCheckInService.getAllSpecialOrCommonFurnaceOption(null, false, "普通");
					
					//首页查看姿态
					furService.updateNumber(getString("code11"),Const.One);
					/*FurnaceRecord furnace=furService.getFurnaceById(getString("id"));
					if(furnace.getViewsPT()==0){
						furnace.setViewsPT(Const.One);
					}
					*/
				}
				model.put("FurnaceOption", furnaceOption);
				model.put("spe_or_com_flag", false);// false代表普通炉
			}
			model.put("startTime", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new java.util.Date()));
		}
		User u = getCurrentUser();
		model.put("uname", u.getName());
		model.put("cid", cid);
		model.put("cardCode", cardCode);
		model.put("select", getString("select"));
		model.put("noRef", getString("noRef"));
		return new ModelAndView("ye/third_department/registration/arrangeRegrationList", model);
	}
	/**
	 * 确认入炉 \转炉
	 */
	@RequestMapping(params = "method=insertOrUpdate")
	@SystemControllerLog(description = "保存入炉,转炉信息")
	@ResponseBody
	public JSONObject insertOrUpdate() {
		/**
		 * 对于普通炉是insert，因为没有记录存在，而且普通炉在这里不存在转炉；
		 * 对于特约炉分两种情况，一种是未更改炉号，即没有转炉，此时是update，因为已经有记录存在；另一种是更改了炉号，即转炉，此时既有insert又有update，
		 * 插入新记录（change_flag为2），更改已存在记录（change_flag改为1）。
		 * 其中以上的insert可以共用，两个update不能共用
		 */
		JSONObject json = new JSONObject();
		try {
			String spe_or_com_flag = getString("spe_or_com_flag");
			String cid = getString("cid");
			Timestamp cremationtime = getTimeM("orderTime");
			Timestamp fireStartTime = getTimeM("fireStartTime");
			String funeralNum = getString("funeralNum");
			String fireworker = getString("fireworker");
			String ptrulu = getString("ptrulu");
			String remark = getString("remark");
			String recordId = getString("recordId");//旧记录ID
			User u = getCurrentUser();
			String type = getString("type");
			ComOrSpeFuneralRecord cr = new ComOrSpeFuneralRecord();
			String fireid = getString("fireid");
			String changeto = getString("changeto");
			fireCheckInService.insertOrUpdate2(ptrulu,changeto,type,spe_or_com_flag,cr,cid,fireid,funeralNum,remark,fireworker,cremationtime,fireStartTime,u,recordId);
			setJsonBySuccess(json, "保存成功", true);
			//下面用于进行首页列表入炉之后的操作
			json.put("rel", "model");
			json.put("refresh", getSession().getAttribute("refresh"));
			String noRefresh = getString("noRef");
			if (noRefresh!=null && !noRefresh.equals("")) {
				json.put("noRefresh", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "系统错误");
		}
		return json;
	}

}
