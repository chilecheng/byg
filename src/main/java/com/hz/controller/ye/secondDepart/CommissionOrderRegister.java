package com.hz.controller.ye.secondDepart;

import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.DeadType;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.FreezerType;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.payDivision.BusinessFees;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.ItemOrder;
import com.alibaba.fastjson.JSONObject;
import com.hz.service.base.AppellationService;
import com.hz.service.base.BillUnitService;
import com.hz.service.base.CarTypeService;
import com.hz.service.base.CertificateService;
import com.hz.service.base.CorpseAddressService;
import com.hz.service.base.CorpseUnitService;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.service.base.FreezerService;
import com.hz.service.base.FreezerTypeService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.NationService;
import com.hz.service.base.ProveUnitService;
import com.hz.service.base.RegisterService;
import com.hz.service.base.ToponymService;
import com.hz.service.base.TransportTypeService;
import com.hz.service.payDivision.BusinessFeesService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.ItemOrderService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;


/**
 * 业务 二科 登记
 * @author Gpf
 *
 */
@Controller
@RequestMapping("/commissionOrderRegister.do")
public class CommissionOrderRegister extends BaseController {
	@Autowired
	private CommissionOrderService commissionOrderService;
	//证件类型
	@Autowired
	private CertificateService certificateService;
	//死亡类型
	@Autowired
	private DeadTypeService deadTypeService;
	//死亡原因
	@Autowired
	private DeadReasonService deadReasonService;
	//证明单位
	@Autowired
	private ProveUnitService proveUnitService;
	//称谓关系
	@Autowired
	private AppellationService appellationService;
	//接尸单位
	@Autowired
	private CorpseUnitService corpseUnitService;
	//冷藏柜信息
	@Autowired
	private FreezerService freezerService;
	//户籍
	@Autowired
	private RegisterService registerService;
	//运输类型
	@Autowired
	private TransportTypeService transportTypeService;
	//收费项目类别
	@Autowired
	private ItemTypeService itemTypeService;
	//收费项目
	@Autowired
	private ItemService itemService;
	//火化委托单服务项目
	@Autowired
	private ItemOrderService itemOrderService;
	//车辆类型
	@Autowired
	private CarTypeService carTypeService;
	//车辆调度
	@Autowired
	private CarSchedulRecordService carSchedulRecordService;
	//冷藏柜使用记录
	@Autowired
	private FreezerRecordService freezerRecordService;
	//冷藏柜使用记录
	@Autowired
	private FreezerTypeService freezerTypeService;
	//挂账单位
	@Autowired
	private BillUnitService billUnitService;
	//地区
	@Autowired
	private ToponymService toponymService;
	//名族
	@Autowired
	private NationService nationService;
	//接尸地址
	@Autowired
	private CorpseAddressService corpseAddService;
	//收费记录
	@Autowired
	private BusinessFeesService businessFeesService;

	
	
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "commissionOrderRegister.do");
    }
	@RequestMapping
    public ModelAndView unspecified() {
		return listCommissionOrder();
    }

	/**
	 * 二科登记
	 * 火化委托单列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listCommissionOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		String isLock=getString("isLock");
		maps.put("isLock", "locked");
		if(isLock !=null &&!"".equals(isLock)){
			maps.put("isLock", "unlock");
		}else{
			maps.put("isLock", "locked");
		}
		maps.put("name", getString("name"));
		maps.put("checkFlag", getByte("checkFlag"));
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		maps.put("goto", Const.GOTO1);
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_IceNumber});
		PageInfo<CommissionOrder> page=commissionOrderService.getCommissionOrderPageInfo(maps,pages[0],pages[1],"arrive_time desc");
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("checkFlagOption", Const.getCheckFlagOption(getByte("checkFlag"),true));
		model.put("page", page);
		model.put("searchOption", searchOption);
        model.put("method", "list");
		return new ModelAndView("ye/second_department/register/listCommissionOrderRegister",model);
    }
    
    
    /**
	 * 链接到修改添加页面
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	  public ModelAndView editCommissionOrder(){
			Map<String,Object> model=new HashMap<String,Object>();
			String id = getString("id");
			String deadReasonOption="";
			CommissionOrder commissionOrder=new CommissionOrder();
			if(id!=null&&!id.equals("")){
				commissionOrder=commissionOrderService.getCommissionOrderById(id);
				model.put("province", commissionOrder.getProvince());
				model.put("city", commissionOrder.getCity());
				model.put("area", commissionOrder.getArea());
				model.put("deadReasonId", commissionOrder.getDeadReasonId());
				deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
				if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
					String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
					if(nationName != null){
						commissionOrder.setdNationId(nationName);
					}
				}
				//收费项目
				List<Object[]> service_list=new ArrayList<Object[]>();
				List<Object[]> articles_list=new ArrayList<Object[]>();
				if(id!=null&&!id.equals("")){
					Map<String, Object> maps=new HashMap<String, Object>();
					maps.put("commissionOrderId", id);
					List<ItemOrder> item_list=itemOrderService.getItemOrderList(maps);
					for (int i = 0; i < item_list.size(); i++) {
						ItemOrder itemOrder = item_list.get(i);
						String itemId=itemOrder.getItemId();
						Item item=itemService.getItemById(itemId);
						ItemType itemType=itemTypeService.getItemTypeById(item.getTypeId());
						//服务项目
						if (itemType.getType()==Const.Type_Service) {
							Map<String, Object>smaps=new HashMap<String, Object>();
							smaps.put("typeId", item.getTypeId());
							smaps.put("type", Const.Type_Service);
							smaps.put("isdel", Const.Isdel_No);
					    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
					    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
					    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
					    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
					    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
					    	int sonIndex=item.getIndexFlag();
					    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
						}
						//丧葬用品
						if (itemType.getType()==Const.Type_Articles) {
							Map<String, Object> smaps=new HashMap<String, Object>();
							smaps.put("typeId", item.getTypeId());
							smaps.put("type", Const.Type_Articles);
							smaps.put("isdel", Const.Isdel_No);
					    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
					    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
					    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
					    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
					    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
					    	int sonIndex=item.getIndexFlag();
					    	articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
						}	
					}
					model.put("service_list", service_list);
					model.put("articles_list", articles_list);
				}
			}else {
				List<DeadType> list=deadTypeService.getDeadTypeList(null);
				if (list.size()>0) {
					DeadType deadType=list.get(0);
					deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId(),"");
				}
				//默认服务项目
				List<Object[]> serviceList=new ArrayList<Object[]>();
				Map<String,Object> serviceMaps=new HashMap<String, Object>();
				serviceMaps.put("type", Const.Type_Service);
				serviceMaps.put("defaultFlag", Const.Is_Yes);
				serviceMaps.put("isdel", Const.Isdel_No);
				List<Item> itemList=itemService.getTypeItemList(serviceMaps);
				Map<String, Object> maps1=new HashMap<String, Object>();
			    for(int i=0;i<itemList.size();i++){
			    	Item item=itemList.get(i);
			    	maps1.put("typeId", item.getTypeId());
			    	maps1.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
			    	String isOption=Const.getIsOption((byte)0, false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	serviceList.add(new Object[]{"service",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
			    }
				model.put("serviceList", serviceList);
				
				
				//默认丧葬用品
				List<Object[]> articlesList=new ArrayList<Object[]>();
				Map<String, Object> articlesMaps=new HashMap<String, Object>();
				articlesMaps.put("type", Const.Type_Articles);
				articlesMaps.put("defaultFlag", Const.Is_Yes);
				articlesMaps.put("isdel", Const.Isdel_No);
				List<Item> list1=itemService.getTypeItemList(articlesMaps);
				Map<String, Object> maps2=new HashMap<String, Object>();
			    for(int i=0;i<list1.size();i++){
			    	Item item=list1.get(i);
			    	maps2.put("typeId", item.getTypeId());
			    	maps2.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(maps2,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(articlesMaps,item.getTypeId(), false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(articlesMaps,item.getId(),false);
			    	String isOption=Const.getIsOption((byte)0, false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	articlesList.add(new Object[]{"articles",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
			    }
				model.put("articlesList", articlesList);
			}
			//证件类型
			String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
			String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
			String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
			String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
			String dFlagOption="";
			if(id!=null &&!"".equals(id)){
				dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
			}else{
				dFlagOption=Const.getIsHaveOption(Const.Is_No, false);//默认未收死亡证明
			}
			String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
			String corpseUnitOption=corpseUnitService.getCorpseUnitOption(commissionOrder.getCorpseUnitId(),false);
			//String registerOption=registerService.getRegisterOption(commissionOrder.getRegisterId(), false);
			String toponymOption=toponymService.getToponymOption(commissionOrder.getToponymId(),false);
			//挂账单位
			String billUnitOption=billUnitService.getBillUnitOption(commissionOrder.getBillUnitId(), true);
			//民族
			String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);
			//接尸地址
			String corpseAddOption=corpseAddService.getCorpseAddressOption(commissionOrder.getPickAddr(), false);
		//车辆调度
		String carTypeOption=carTypeService.getCarTypeOption("", false);
		String transportTypeOption=transportTypeService.getTransportTypeOption("", false);
		model.put("carTypeOption", carTypeOption);
		model.put("transportTypeOption", transportTypeOption);
		if(id!=null&&!id.equals("")){
			Map<String,Object> carMaps=new HashMap<String, Object>();
			carMaps.put("commissionOrderId", id);
			List<CarSchedulRecord> carSchedulRecordList=new ArrayList<CarSchedulRecord>();
			List<Object[]> car_List=new ArrayList<Object[]>();
			carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
			for (int i = 0; i < carSchedulRecordList.size(); i++) {
				CarSchedulRecord carSchedulRecord=carSchedulRecordList.get(i);
				String carType_option=carTypeService.getCarTypeOption(carSchedulRecord.getCarTypeId(), false);
				String transportType_option=transportTypeService.getTransportTypeOption(carSchedulRecord.getTransportTypeId(), false);
				Timestamp pickTi=carSchedulRecord.getPickTime();
				String pickTime = "";
				if (pickTi != null) {
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
					pickTime=sdf.format(pickTi);
				}
				car_List.add(new Object[]{carSchedulRecord.getId(),transportType_option,carType_option,pickTime,carSchedulRecord.getComment()});
			}
			model.put("car_List", car_List);
		}
		
		if (id != null && !id.equals("")) {
			//冷藏柜使用记录
			Map<String, Object> freezerMaps = new HashMap<String, Object>();
			freezerMaps.put("commissionOrderId", id);
			List<FreezerRecord> freezerRecordList = new ArrayList<FreezerRecord>();
			FreezerRecord freezerRecord = new FreezerRecord();
			freezerRecordList = freezerRecordService.getFreezerRecordList(freezerMaps);
			if (freezerRecordList.size() > 0) {
				if (id != null && !id.equals("")) {
					freezerRecord = (FreezerRecord) freezerRecordList.get(0);
				}
			}
			model.put("freezerRecord", freezerRecord);
		}
		byte checkFlag=commissionOrder.getCheckFlag();
		model.put("checkFlag", checkFlag);
		//model.put("registerOption", registerOption);
		model.put("toponymOption", toponymOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("fAppellationOption", fAppellationOption);
		model.put("dFlagOption", dFlagOption);
		model.put("proveUnitOption", proveUnitOption);
		model.put("sexOption", sexOption);
		model.put("deadReasonOption", deadReasonOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("certificateOption", certificateOption);
		model.put("commissionOrder", commissionOrder);
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("id", id);
		model.put("billUnitOption", billUnitOption);
		model.put("nationOption", nationOption);
		model.put("corpseAddOption", corpseAddOption);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("isOption", Const.getIsOption((byte)0, false));
		model.put("agentUser", getCurrentUser().getName());
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=format.format(date);
		Timestamp now=new Timestamp(System.currentTimeMillis());
		model.put("now", format.format(now));
		model.put("time",time);
        model.put("method", "save");
		return new ModelAndView("ye/second_department/register/editCommissionOrder",model);
    }
	
	 /**
     * 冷藏柜调度页面
     * @return
     */
    @RequestMapping(params = "method=editFreezer")
    public ModelAndView editFreezer(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		List<Object> typeList=new ArrayList<Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object>freezermaps=new HashMap<String, Object>();
		List<FreezerRecord> freezerRecordList = freezerRecordService.getNotOutFreezerRecordList(null);
		List<FreezerType> freezerTypeList = freezerTypeService.getFreezerTypeList(null,"index_flag asc,name asc");//冷藏柜类型信息
		for (int i = 0; i < freezerTypeList.size(); i++) {
			FreezerType freezerType=freezerTypeList.get(i);
			typeList.add(freezerType.getName());
			freezermaps.put("typeId",freezerType.getId());
			freezermaps.put("isdel",Const.Isdel_No);
			List<Freezer> freezerList = freezerService.getFreezerList(freezermaps,"index_flag asc,name asc");
			Object[] ob=new Object[freezerList.size()];
			for (int j = 0; j < freezerList.size(); j++) {
				ob[j]=freezerList.get(j);
				for (int j2 = 0; j2 < freezerRecordList.size(); j2++) {

					if (freezerList.get(j).getId()==freezerRecordList.get(j2).getFreezerId()||freezerList.get(j).getId().equals(freezerRecordList.get(j2).getFreezerId())) {
						ob[j]=freezerRecordList.get(j2);
					}
				}
			}
			list.add(ob);
		}

		model.put("list", list);
		model.put("typeList", typeList);
        model.put("method", "editFreezer");
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
    	return new ModelAndView("ye/commissionOrder/editFreezer",model);
    }
    
    
    /**
     * 冷藏柜单击
     * @return
     */
    @RequestMapping(params = "method=freezer_click")
    @ResponseBody
    public JSONObject freezer_click(){
    	JSONObject json = new JSONObject();
    	int type=getInt("type");
    	int freezer=getInt("freezer");
		List<List<Freezer>> list=new ArrayList<List<Freezer>>();
		Map<String, Object>freezermaps=new HashMap<String, Object>();
		List<FreezerType> freezerTypeList = freezerTypeService.getFreezerTypeList(null,"index_flag asc,name asc");//冷藏柜类型信息
		for (int i = 0; i < freezerTypeList.size(); i++) {
			FreezerType freezerType=freezerTypeList.get(i);
			freezermaps.put("typeId",freezerType.getId());
			/*freezermaps.put("outFlag",Const.Is_No);
			freezermaps.put("splitFlag",Const.Is_No);*/
			freezermaps.put("isdel",Const.Isdel_No);
			List<Freezer> freezerList = freezerService.getFreezerList(freezermaps,"index_flag asc,name asc");
			list.add(freezerList);
		}
		/*Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("isdel",Const.Isdel_No );
		maps.put("outFlag",Const.Is_Yes );
		maps.put("isdel",Const.Isdel_No );
		maps.put("splitFlag",Const.Is_No );
		List<Freezer> freezerDList = freezerService.getFreezerList(maps,"index_flag asc,name asc");//外送单体
		list.add(freezerDList);
		maps.put("splitFlag",Const.Is_Yes );
		List<Freezer> freezerFList = freezerService.getFreezerList(maps,"index_flag asc,name asc");//外送分体
		list.add(freezerFList);*/
		List<Freezer> tList=list.get(type);
		Freezer freezer1=(Freezer) tList.get(freezer);
		json.put("name", freezer1.getName());
		json.put("id", freezer1.getId());
		return json;
    }
    
    /**
   	 * 链接到修改添加服务项目
   	 * @return
   	 */
       @RequestMapping(params = "method=selectItems")
       public ModelAndView editItemOrders(){
   		Map<String,Object> model=new HashMap<String,Object>();
   		List<Object[]> list=new ArrayList<Object[]>();
   		Map<String, Object>maps=new HashMap<String, Object>();
   		maps.put("type", getByte("type"));
   		maps.put("isdel", Const.Isdel_No);
   		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
   		for (int i = 0; i < itemTypeList.size(); i++) {
   			ItemType itemType=(ItemType)itemTypeList.get(i);
   			maps.put("typeId",itemType.getId());
   			maps.put("isdel", Const.Isdel_No);
   			String itemOption=itemService.getItemOption(maps, "",true);
   			list.add(new Object[]{itemType.getName(),itemOption});
   		}
   		//添加服务项目
   		List<Object[]> itemList=new ArrayList<Object[]>();
   		for (int i = 0; i < itemTypeList.size(); i++) {
   			ItemType itemType=(ItemType)itemTypeList.get(i);
   			Map<String, Object>itemMaps=new HashMap<String, Object>();
   			itemMaps.put("typeId", itemType.getId());
   			itemMaps.put("isdel", Const.Isdel_No);
   			List<Item> itemList1=itemService.getItemList(itemMaps,"index_flag asc");
   			itemList.add(new Object[]{itemType,itemList1});
   		}
   		model.put("itemTree", getTree(itemList));
           model.put("list", list);
           model.put("type", getByte("type"));
           model.put("id", getString("id"));
           model.put("Type_Articles", Const.Type_Articles);
   		model.put("Type_Service", Const.Type_Service);
   		return new ModelAndView("ye/commissionOrder/selectItems",model);
       }
    
    
       
       /**
        * 电话 预约 
        *@return
        */
       @RequestMapping(params = "method=phoneBook")
 	  public ModelAndView phoneBokkCommissionOrder(){
 			Map<String,Object> model=new HashMap<String,Object>();
 			String id = getString("id");
 			String deadReasonOption="";
 			CommissionOrder commissionOrder=new CommissionOrder();
 			if(id!=null&&!id.equals("")){
 				commissionOrder=commissionOrderService.getCommissionOrderById(id);
 				deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
 				if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
					String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
					if(nationName != null){
						commissionOrder.setdNationId(nationName);
					}
				}
 			}else {
 				List<DeadType> list=deadTypeService.getDeadTypeList(null);
 				if (list.size()>0) {
 					DeadType deadType=list.get(0);
 					deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId(),"");
 				}
 			}
 			//证件类型
 			String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
 			String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
 			String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
 			//证明单位
 			String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
 			//死亡 证明 
 			String dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
 			//称谓关系
 			String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
 			//接  尸  单位 
 			String corpseUnitOption=corpseUnitService.getCorpseUnitOption(commissionOrder.getCorpseUnitId(),false);
 			//String registerOption=registerService.getRegisterOption(commissionOrder.getRegisterId(), false);
 			String toponymOption=toponymService.getToponymOption(commissionOrder.getToponymId(),false);
 			//挂账单位
 			String billUnitOption=billUnitService.getBillUnitOption(commissionOrder.getBillUnitId(), true);
 			//民族
 			String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);
 			//接尸地址
 			String corpseAddOption=corpseAddService.getCorpseAddressOption(commissionOrder.getPickAddr(), false);
 			
 		//车辆调度
 		String carTypeOption=carTypeService.getCarTypeOption("", false);
 		String transportTypeOption=transportTypeService.getTransportTypeOption("", false);
 		model.put("carTypeOption", carTypeOption);
 		model.put("transportTypeOption", transportTypeOption);
 		if(id!=null&&!id.equals("")){
 			Map<String,Object> carMaps=new HashMap<String, Object>();
 			carMaps.put("commissionOrderId", id);
 			List<CarSchedulRecord> carSchedulRecordList=new ArrayList<CarSchedulRecord>();
 			List<Object[]> car_List=new ArrayList<Object[]>();
 			carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
 			for (int i = 0; i < carSchedulRecordList.size(); i++) {
 				CarSchedulRecord carSchedulRecord=carSchedulRecordList.get(i);
 				String carType_option=carTypeService.getCarTypeOption(carSchedulRecord.getCarTypeId(), false);
 				String transportType_option=transportTypeService.getTransportTypeOption(carSchedulRecord.getTransportTypeId(), false);
 				Timestamp pickTi=carSchedulRecord.getPickTime();
 				String pickTime = "";
 				if (pickTi != null) {
 					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
 					pickTime=sdf.format(pickTi);
 				}
 				car_List.add(new Object[]{carSchedulRecord.getId(),transportType_option,carType_option,pickTime,carSchedulRecord.getComment()});
 			}
 			model.put("car_List", car_List);
 		}
 		byte checkFlag=commissionOrder.getCheckFlag();
 		
 		
 		model.put("checkFlag", checkFlag);
 		//model.put("registerOption", registerOption);
 		model.put("toponymOption", toponymOption);
 		model.put("corpseUnitOption", corpseUnitOption);
 		model.put("fAppellationOption", fAppellationOption);
 		model.put("dFlagOption", dFlagOption);
 		model.put("proveUnitOption", proveUnitOption);
 		model.put("sexOption", sexOption);
		model.put("corpseAddOption", corpseAddOption);
 		model.put("deadReasonOption", deadReasonOption);
 		model.put("deadTypeOption", deadTypeOption);
 		model.put("certificateOption", certificateOption);
 		model.put("commissionOrder", commissionOrder);
 		model.put("Check_No", Const.Check_No);
 		model.put("Check_Yes", Const.Check_Yes);
 		model.put("IsHave_Yes", Const.IsHave_Yes);
 		model.put("IsHave_No", Const.IsHave_No);
 		model.put("Is_Yes", Const.Is_Yes);
 		model.put("Is_No", Const.Is_No);
 		model.put("id", id);
 		model.put("billUnitOption", billUnitOption);
 		model.put("nationOption", nationOption);
 		model.put("Type_Articles", Const.Type_Articles);
 		model.put("Type_Service", Const.Type_Service);
 		model.put("isOption", Const.getIsOption((byte)0, false));
 		model.put("agentUser", getCurrentUser().getName());
 		
 
 		
 		Date date=new Date();
 		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
 		String time=format.format(date);
 		model.put("time",time);
         model.put("method", "save");
 		return new ModelAndView("ye/second_department/register/phoneBook",model);
     }
	
       
       /**
        * 删除火化委托单
        * @return
        */
       @RequestMapping(params = "method=delete")
       @SystemControllerLog(description = "删除二科火化委托单")
       @ResponseBody
       public JSONObject deleteCommissionOrder(){
   		JSONObject json = new JSONObject();
   		String id = getString("ids");
       	try {
       		String[] ids = id.split(",");
			for (String coId : ids) {
				byte checkFlag = commissionOrderService.getCheckFlag(coId);
				if (1 == checkFlag) {
					json.put("statusCode", 302);
					json.put("message", "已审核，不能删除");
					return json;
				}
				commissionOrderService.deleteCommissionOrder(coId);
			}
       		setJsonBySuccess(json, "删除成功", true);
   		} catch (Exception e) {
   			e.printStackTrace();
   			setJsonByFail(json, "删除失败，错误"+e.getMessage());
   		}
   		return json;
       }
      
       /**
        * 保存火化委托单
        * @return
        */
       @RequestMapping(params = "method=save")
       @SystemControllerLog(description = "保存二科火化委托单")
       @ResponseBody
       public JSONObject saveCommissionOrder(){
   		JSONObject json = new JSONObject();
       	try {
       		String userId=getCurrentUser().getUserId();
       		String userName=getCurrentUser().getName();
       		String id =getString("id");
       		int age=getInt("age");
       		Timestamp arriveTime=getTimeM("arriveTime");
       		String cardCode=getString("cardCode");
       		if (id == null || id.equals("")) {
   				int num = commissionOrderService.getCommissionOrderByCard(cardCode);
   				if (num >= 1) {
   					setJsonByFail(json, "该卡号已经存在");
   					return json;
   				}
   			}else{//有id号进行判断是否收费
				List<BusinessFees> busList= businessFeesService.getBusinessFeesByComId(id);
				if(busList.size()>0){//已有收费，限制保存
					setJsonByFail(json, "该死者已收费，无法保存，请返回");
					return json;
				}
			}
       		String province=getString("province");
    		String city=getString("city");
    		String area=getString("area");
       		String certificateCode=getString("certificateCode");
       		String certificateId=getString("certificateId");
       		Timestamp checkTime=getTimeM("checkTime");
       		String checkUserId=getString("checkUserId");
       		String cComment=getString("comment");
       		String corpseUnitId=getString("corpseUnitId");
       		Timestamp orderTime=getTimeM("orderTime");
       		String dAddr=getString("dAddr");
       		String deadReasonId=getString("deadReasonId");
       		String deadTypeId=getString("deadTypeId");
       		byte dFlag=getByte("dFlag");
       		//死者身份证图片路径
       		String dIdcardPic =getString("filename");
       		//System.out.println(dIdcardPic);
       		//经办人身份证图片路径
       		String eIdcardPic=getString("filenameF");
       		//System.out.println(eIdcardPic);
       		
       		String dNation=getString("dNation");
       		if(dNation!=null && dNation!=""){
       			String nationId = nationService.getdnationId(dNation);
       			if(nationId != null){
       				dNation = nationId;
       			}
       		}
       		Timestamp dTime=getTimeM("dTime");
       		String fAddr=getString("fAddr");
       		String fAppellationId=getString("fAppellationId");
       		String fName=getString("fName");
       		String fPhone=getString("fPhone");
       		String fUnit=getString("fUnit");
       		String fCardCode=getString("fCardCode");
       		String furnaceTypeId=getString("furnaceTypeId");
       		String furnaceId=getString("furnaceId");
       		String name=getString("name");
       		String pickAddr=getString("pickAddr");
       		String proveUnitId=getString("proveUnitId");
       		String proveUnitContent=getString("proveUnitContent");
       		String registerId=getString("registerId");
       		String toponymId = getString("toponymId");
       		byte scheduleFlag=getByte("scheduleFlag");
       		byte sex=getByte("sex");
       		byte radio=getByte("radio");
   			
   			//挂账单位
       		String billUnitId=getString("billUnitId");
       		
       		
       		
       		
       		
       		
       		//收费项目
       		String[] service=getArrayString("service_id");//服务项目
       		String[] articles=getArrayString("articles_id");//丧葬用品
       		String[] itemId=getArrayString("itemId");
       		String[] number=getArrayString("number");
       		String[] bill=getArrayString("bill");
       		String[] total=getArrayString("total");
       		String[] comment=getArrayString("comment");
       		String[] pice=getArrayString("pice");
       		//车辆调度
       		String[] transportType=getArrayString("transportTypeId");
       		String[] carType=getArrayString("carTypeId");
       		//此处因为 时间从原来的精确秒,统一改成到分钟,还用之前的方法会报错
       		//Timestamp[] pickTime=getArrayTimestamp("pickTime");
    		String[] pickpick=getArrayString("pickTime");
    		Timestamp[] pickTime=null;
    		if(pickpick!=null && pickpick.length>0){
    			pickTime=DateTools.stringToTimeArray(pickpick);
    		}
       		String[] carComment=getArrayString("carComment");
       		String[] carSchedulRecordId=getArrayString("csr_id");
       		
   			//冷藏柜使用记录
       		
   			String freezerId=getString("freezerId");
   			//冷藏柜使用记录    入柜经办人
   			String assiginUser   =  getString("assiginUser");
   		//业务二科到馆  二科登记默认为  到馆 即为入柜
       		byte  freezerFlag  = Const.IsFlag_Yse;
   			
   			
   			//告别厅
   			Timestamp farewellBeginDate=getTimeM("farewellBeginDate");
   			Timestamp farewellEndDate=getTimeM("farewellEndDate");
   			String farewellId=getString("farewellId");
   			//守灵室
   			Timestamp mourningBeginTime=getTimeM("mourningBeginTime");
   			Timestamp mourningEndTime=getTimeM("mourningEndTime");
   			String mourningId=getString("mourningId");
   			
   			
   			String[] itemIdR=getArrayString("reduction_itemId");
   			String[] numberR=getArrayString("reduction_number");
   			String[] totalR=getArrayString("reduction_total");
   			String[] commentR=getArrayString("reduction_comment");
   			//以下新增,用于记录困难减免记录
			String[] hardIdR=getArrayString("hard_itemId");
			String[] hardnumberR=getArrayString("hard_number");
			String[] hardtotalR=getArrayString("hard_total");
			String[] hardcommentR=getArrayString("hard_comment");
   			//基本减免
   			String[] base=getArrayString("base_id");//基本减免项目
   			String baseReductionId=getString("baseReductionId");//基本减免Id
   			String[] freePersonId=getArrayString("freePersonId");//免费对象类别
   			String[] hardPersonId=getArrayString("hardPersonId");//重点救助对象类别
       		byte baseIs=getByte("baseIs");
   			String fidcard=getString("fidcard");
   			
   			
   			//困难减免
   			String[] hard=getArrayString("hard_id");//困难减免项目
   			String[] proveIds=getArrayString("proveIds");
   			String hardReductionId=getString("hardReductionId");
   			byte hardIs=getByte("hardIs");
   			String hard_proveUnitId=getString("hard_proveUnitId");
   			String reason=getString("reason");
   			String hComment=getString("comment");
   			Timestamp createTime=getTimeM("createTime");
   			String appellationId=getString("appellationId");
   			String applicant=getString("applicant");
   			String phone=getString("phone");
   			byte special=getByte("special");
   			
   			
   			// 新增  三项   业务二科 登记   登记人员
   			String pbookAgentId = getString("pbookAgentId");
       		String bookAgentId = getString("bookAgentId");
       		

       		
   			// 跳转页面的判断
       		//******************************************************
   			String source = new String();
   			source=getString("sourcepage");
   			if (source.equals("phoneBook")){
   				pbookAgentId = getCurrentUser().getName();
   			}else{
   				if(source.equals("editCommissionOrder")){
   					bookAgentId = getCurrentUser().getName();
   				}
   			}
   			String aisle="erKe";
       		String coId=commissionOrderService.addOrUpdateMethod(userId,userName, id, age, arriveTime, cardCode
       				, certificateCode, certificateId, checkTime, checkUserId
       				, cComment, corpseUnitId, orderTime, dAddr, deadReasonId
       				, deadTypeId, dFlag, dIdcardPic, dNation, dTime, eIdcardPic
       				, fAddr, fAppellationId, fName, fPhone, fUnit,fCardCode
       				, furnaceTypeId, furnaceId, name, pickAddr, proveUnitId
       				, proveUnitContent, registerId, toponymId, scheduleFlag, sex, radio
       				, service, articles, itemId, number, bill, total, comment
       				, transportType, carType, pickTime, carComment, carSchedulRecordId
       				, farewellBeginDate, farewellEndDate, farewellId
       				, freezerId, mourningBeginTime, mourningEndTime
       				, mourningId, orderTime, itemIdR, numberR, totalR
       				, commentR, base, baseReductionId, freePersonId
       				, hardPersonId, baseIs, fidcard, hard, proveIds
       				, hardReductionId, hardIs, hard_proveUnitId, reason
       				, hComment, createTime, appellationId, applicant
       				, phone, special,billUnitId,pbookAgentId,bookAgentId,assiginUser,freezerFlag 
       				, hardIdR,hardnumberR,hardtotalR,hardcommentR,province
       				, city,area,null,null,pice,null,null,null,aisle,null);
       		setJsonBySuccess(json, "保存成功", "commissionOrderRegister.do?method=list");
       		if(id!=null && !"".equals(id)){
       			json.put("coId", id);
       		}else{
       			json.put("coId", coId);    			
       		}
   		} catch (Exception e) {
   			e.printStackTrace();
   			setJsonByFail(json, "保存失败，错误"+e.getMessage());
   		}
   		return json;
       }
       
       
       
       /**
   	 * 链接到火化委托单页面（只读页面）
   	 * @return
   	 */
   	@RequestMapping(params = "method=readOnly")
       public ModelAndView editCommissionOrderReadOnly(){
   		Map<String,Object> model=new HashMap<String,Object>();
   		String adr=getString("adr");
   		model.put("adr", adr);
   		String id = getString("id");
   		String deadReasonOption="";
   		CommissionOrder commissionOrder=new CommissionOrder();
   		if(id!=null&&!id.equals("")){
   			commissionOrder=commissionOrderService.getCommissionOrderById(id);
   			String deadArea = commissionOrder.getProvince() + " " + commissionOrder.getCity() + " " + commissionOrder.getArea();
   			model.put("deadArea", deadArea);
   			deadReasonOption=deadReasonService.getDeadReason(commissionOrder.getDeadReasonId(),false);
   			if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
				String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
				if(nationName != null){
					commissionOrder.setdNationId(nationName);
				}
			}
   		}else {
   			List<DeadType> list=deadTypeService.getDeadTypeList(null);
   			if (list.size()>0) {
   				DeadType deadType=(DeadType)list.get(0);
   				deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId(),"");
   			}
   		}
   	/*	String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);*/
   		String certificateOption=certificateService.getCertificate(commissionOrder.getCertificateId(),false);
		String deadTypeOption=deadTypeService.getDeadType(commissionOrder.getDeadTypeId(),false);
		String sexOption=Const.getSex(commissionOrder.getSex(),false);
		String proveUnitOption=proveUnitService.getProveUnit(commissionOrder.getProveUnitId(),false);
		String dFlagOption=Const.getIsHave(commissionOrder.getdFlag(), false);
		String fAppellationOption=appellationService.getAppellation(commissionOrder.getfAppellationId(),false);
		String corpseUnitOption=corpseUnitService.getCorpseUnit(commissionOrder.getCorpseUnitId(),false);
		String registerOption=registerService.getRegister(commissionOrder.getRegisterId(), false);
		String toponymOption=toponymService.getToponym(commissionOrder.getToponymId(),false);
		//默认服务项目
   		List<Object[]> serviceList=new ArrayList<Object[]>();
   		Map<String, Object>serviceMaps=new HashMap<String, Object>();
   		serviceMaps.put("type", Const.Type_Service);
   		serviceMaps.put("defaultFlag", Const.Is_Yes);
   		List<Item> list=itemService.getTypeItemList(serviceMaps);
   		Map<String, Object>maps1=new HashMap<String, Object>();
   	    for(int i=0;i<list.size();i++){
   	    	Item item=(Item)list.get(i);
   	    	maps1.put("typeId", item.getTypeId());
   	    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
   	    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
   	    	String isOption=Const.getIsOption((byte)0, false);
   	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
   	    	serviceList.add(new Object[]{"service",itemOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
   	    }
   		model.put("serviceList", serviceList);
   		
   		
   		//默认丧葬用品
   		List<Object[]> articlesList=new ArrayList<Object[]>();
   		Map<String, Object>articlesMaps=new HashMap<String, Object>();
   		articlesMaps.put("type", Const.Type_Articles);
   		articlesMaps.put("defaultFlag", Const.Is_Yes);
   		List<Item> list1=itemService.getTypeItemList(articlesMaps);
   		Map<String, Object>maps2=new HashMap<String, Object>();
   	    for(int i=0;i<list1.size();i++){
   	    	Item item=list1.get(i);
   	    	maps2.put("typeId", item.getTypeId());
   	    	String itemOption=itemService.getItemOption(maps2,item.getId(),false);
   	    	String itemTypeOption=itemTypeService.getItemTypeOption(articlesMaps,item.getTypeId(), false);
   	    	String isOption=Const.getIsOption((byte)0, false);
   	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
   	    	articlesList.add(new Object[]{"articles",itemOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
   	    }
   		model.put("articlesList", articlesList);
   		
   		
   		//收费项目
   		List<Object[]> service_list=new ArrayList<Object[]>();
   		List<Object[]> articles_list=new ArrayList<Object[]>();
   		if(id!=null&&!id.equals("")){
   			Map<String, Object>maps=new HashMap<String, Object>();
   			maps.put("commissionOrderId", id);
   			List<ItemOrder> item_list=itemOrderService.getItemOrderList(maps);
   			for (int i = 0; i < item_list.size(); i++) {
   				ItemOrder itemOrder=item_list.get(i);
   				String itemId=itemOrder.getItemId();
   				Item item=itemService.getItemById(itemId);
   				ItemType itemType=itemTypeService.getItemTypeById(item.getTypeId());
   				//服务项目
   				if (itemType.getType()==Const.Type_Service) {
   					Map<String, Object>smaps=new HashMap<String, Object>();
   					smaps.put("typeId", item.getTypeId());
   					smaps.put("type", Const.Type_Service);
   				
   			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
   			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
   			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
   			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
   			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
   			    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
   				}
   				//丧葬用品
   				if (itemType.getType()==Const.Type_Articles) {
   					Map<String, Object>smaps=new HashMap<String, Object>();
   					smaps.put("typeId", item.getTypeId());
   					smaps.put("type", Const.Type_Articles);
   			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
   			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
   			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
   			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
   			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
   			    	articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
   				}	
   			}
   			model.put("service_list", service_list);
   			model.put("articles_list", articles_list);
   		}
   		

   		//车辆调度
   		String carTypeOption=carTypeService.getCarTypeOption("", false);
   		String transportTypeOption=transportTypeService.getTransportTypeOption("", false);
   		model.put("carTypeOption", carTypeOption);
   		model.put("transportTypeOption", transportTypeOption);
   		if(id!=null&&!id.equals("")){
   			Map<String,Object> carMaps=new HashMap<String, Object>();
   			carMaps.put("commissionOrderId", id);
   			List<CarSchedulRecord> carSchedulRecordList=new ArrayList<CarSchedulRecord>();
   			List<Object[]> car_List=new ArrayList<Object[]>();
   			carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
   			for (int i = 0; i < carSchedulRecordList.size(); i++) {
   				CarSchedulRecord carSchedulRecord=carSchedulRecordList.get(i);
   				String carType_option=carTypeService.getCarTypeOption(carSchedulRecord.getCarTypeId(), false);
   				String transportType_option=transportTypeService.getTransportTypeOption(carSchedulRecord.getTransportTypeId(), false);
   				Timestamp pickTi=carSchedulRecord.getPickTime();
   				String pickTime = "";
   				if (pickTi != null) {
   					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
   					pickTime=sdf.format(pickTi);
   				}
   				car_List.add(new Object[]{carSchedulRecord.getId(),transportType_option,carType_option,pickTime,carSchedulRecord.getComment()});
   			}
   			model.put("car_List", car_List);
   		}
  
   		
   		
   		
   		
   		//冷藏柜使用记录
   		Map<String, Object>freezerMaps=new HashMap<String, Object>();
   		
   		freezerMaps.put("commissionOrderId", id);
   		List<FreezerRecord> freezerRecordList=new  ArrayList<FreezerRecord>();
   		FreezerRecord freezerRecord=new FreezerRecord();
   		freezerRecordList=freezerRecordService.getFreezerRecordList(freezerMaps);
   		if (freezerRecordList.size()>0) {
   			if(id!=null&&!id.equals("")){
   				freezerRecord=(FreezerRecord) freezerRecordList.get(0);
   			}
   		}
   		model.put("freezerRecord", freezerRecord);
		
   		byte checkFlag=commissionOrder.getCheckFlag();
   		model.put("toponymOption",toponymOption);
   		model.put("checkFlag", checkFlag);
   		model.put("registerOption", registerOption);
   		model.put("corpseUnitOption", corpseUnitOption);
   		model.put("fAppellationOption", fAppellationOption);
   		model.put("dFlagOption", dFlagOption);
   		model.put("proveUnitOption", proveUnitOption);
   		model.put("sexOption", sexOption);
   		model.put("deadReasonOption", deadReasonOption);
   		model.put("deadTypeOption", deadTypeOption);
   		model.put("certificateOption", certificateOption);
   		model.put("commissionOrder", commissionOrder);
   		model.put("Check_No", Const.Check_No);
   		model.put("Check_Yes", Const.Check_Yes);
   		model.put("IsHave_Yes", Const.IsHave_Yes);
   		model.put("IsHave_No", Const.IsHave_No);
   		model.put("Is_Yes", Const.Is_Yes);
   		model.put("Is_No", Const.Is_No);
   		model.put("id", id);
   		model.put("Type_Articles", Const.Type_Articles);
   		model.put("Type_Service", Const.Type_Service);
   		model.put("isOption", Const.getIsOption((byte)0, false));
   		model.put("agentUser", getCurrentUser().getName());
   		Timestamp update=commissionOrder.getUpdateTime();
//   		Date date=new Date();
//   		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
//   		String time=format.format(date);
   		model.put("time",update);
     //      model.put("method", "save");
   		return new ModelAndView("ye/second_department/register/readOnlyCommissionorderRegister",model);
       }
       
       
       
       
       /**
        * 获得tree结构
        * @param list
        * @return
        */
   	public String getTree(List<Object[]> list){
       	int sum=0;
       	String content="";
       	for (int i = 0; i < list.size(); i++) {
       		if (i>0) {
       			content+=",";
   			}
       		Object[] objects=list.get(i);
       		ItemType itemType=(ItemType)objects[0];
   	    	content+= "{";
   	    	content+="text:'"+itemType.getName()+"'";
//   	    	content+=",tags:['"+itemType.getId()+"']";
   	    	content+=",selectable: false";
   	    	List<?> itemList=(List<?>)objects[1];
   	    	content+=",nodes: [";
   	    	for (int j = 0; j < itemList.size(); j++) {
   	    		if (j>0) {
   	    			content+=",";
   				}
   	    		Item item=(Item)itemList.get(j);
   	    		content+="{";
   	    		content+="text:'"+item.getName()+"'";
   	    		content+=",tags:['"+item.getId()+"_"+sum+"']";
   	    		sum++;
   	    		content+="}";
   	    	}
   	    	content+="]";
   	    	content+="}";
       	}
       	return content;
       }
	
	
}

