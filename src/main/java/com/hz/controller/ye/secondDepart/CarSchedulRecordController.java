package com.hz.controller.ye.secondDepart;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Car;
import com.hz.entity.system.User;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.service.base.CarService;
import com.hz.service.base.CarTypeService;
import com.hz.service.base.TransportTypeService;
import com.hz.service.system.UserService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 车辆调度
 * @author cjb
 *
 */
@Controller
@RequestMapping("/carCarSchedul.do")
public class CarSchedulRecordController extends BaseController {

	@Autowired
	private UserService userService;
	@Autowired
	private CarSchedulRecordService csrs;
	@Autowired
	private CarTypeService carTypeService;
	@Autowired
	TransportTypeService transportTypeService;
	@Autowired
	CarService cs;
	//车辆派送
	@Autowired
	private CarSchedulRecordService carService;
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "carCarSchedul.do");
    }
	@RequestMapping
    public ModelAndView unspecified() {
		return listCarRecord();
    }
	
	@RequestMapping(params="method=list")
	public ModelAndView listCarRecord() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		List<Object[]> list = new ArrayList<Object[]>();
		//预设当前日期
		Date date = DateTools.getThisDateUtil();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date = getDate("date");
		}
		Object[] firstLine = new Object[25];
		firstLine[0]="车牌号";
		for (int i = 0; i < 24; i++) {
			firstLine[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 0, i, Calendar.HOUR);
		}
		list.add(firstLine);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isdel", 1);
		List<Car> carList = cs.getCarList(map,"name");
		Object[] o = null;
		for (Car car : carList) {
			o = new Object[25];
			o[0] = car;
			list.add(o);
		}
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("startTime", DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(date)));
		paramMap.put("endTime", DateTools.getDayEndTime(DateTools.utilDateToSqlDate(date)));
		List<CarSchedulRecord> carSList = csrs.getCarSchedulRecordList(paramMap, "");
		for (CarSchedulRecord carS : carSList) {
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;
			for (int i = 1; i < list.size(); i++) {
				Car car = (Car)list.get(i)[0];
				if (carS.getCarId().equals(car.getId())) {
					row = i;
					break;
				}
			}
			for (int i = 1; i < firstLine.length; i++) {
				Date de =(Date) firstLine[i];
				if (DateTools.isSameHour(carS.getStartTime(),de)) {
					col = i;
				}
				if (DateTools.isSameHour(carS.getReturnTime(),de)) {
					endCol = i;
				}
			}
			if(row!=0){
				if (col == 0) {
					col = 1;
				} 
				if (endCol == 0) {
					endCol = firstLine.length-1;
				} 
				Object[] ob = list.get(row);
				for (int i = col; i <= endCol; i++) {
					ob[i] = carS;
				}
			}
		}
		model.put("date", date);
		model.put("list", list);
		model.put("CarSFlag_GO", Const.CarSFlag_GO);
        model.put("CarSFlag_NO", Const.CarSFlag_NO);
        model.put("CarSFlag_OK", Const.CarSFlag_OK);
        model.put("CarSFlag_RP", Const.CarSFlag_RP);
        if ("select".equals(getString("type"))) {
        	return new ModelAndView("ye/second_department/car/selectCar",model);
		}
		return new ModelAndView("ye/second_department/car/car",model); 
	}
	
	/**
	 * 进入车辆调度列表页
	 * @return
	 */
	@RequestMapping(params="method=listInfo")
	public ModelAndView listCarInfo() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String, Object> maps=new HashMap<String, Object>();
		Date beginDate = getDate("beginDate");
		Date endDate = getDate("endDate");
		if (beginDate != null && endDate!= null) {
			maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(beginDate.getTime())));
			maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(endDate.getTime())));
		}
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
		String type = getString("checkType");
		String order = "pick_time desc";
		if (("1".equals(type))) {//全部
			
		} else if ("2".equals(type)) {//已派车
			maps.put("flag", "go");
			maps.put("value",Const.CarSFlag_GO );
		} else if ("3".equals(type)) {//接运完成
			maps.put("flag", "yes");
			maps.put("value",Const.CarSFlag_OK );
		} else if("4".equals(type)) {//未派车
			order = "pick_time asc";
			maps.put("flag", "no");
			maps.put("value",Const.CarSFlag_NO );
		} else {//默认为已派车和未派车
			order = "pick_time asc";
			maps.put("flag", "noAndGo");
			maps.put("value1",Const.CarSFlag_NO );
			maps.put("value2",Const.CarSFlag_GO );
		}
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_Fname});
//		maps.put("checkFlag", Const.Check_Yes);
		PageInfo<CarSchedulRecord> page=csrs.getCarListPageInfo(maps, pages[0],pages[1], order);
		model.put("checkType", type);
		model.put("searchOption", searchOption);
		model.put("beginDate", beginDate);
		model.put("endDate", endDate);
		model.put("name", getString("searchVal"));
		model.put("method","listInfo");
		model.put("page", page);
		model.put("CarSFlag_GO", Const.CarSFlag_GO);
        model.put("CarSFlag_NO", Const.CarSFlag_NO);
        model.put("CarSFlag_OK", Const.CarSFlag_OK);
		return new ModelAndView("ye/second_department/car/mlistCar",model);
	}
	/**
	 * 进行派车操作
	 * @return
	 */
	@RequestMapping(params="method=dispatch")
	public ModelAndView sendCar() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
//		String homeType=getString("homeType");
		String id = getString("id");
		CarSchedulRecord cs = csrs.getCommissionOrder(id);
		
		String driverOption = userService.getRoleOption(Const.DRIVER,cs.getDriverId());
		
		//首页查看状态
		CarSchedulRecord newCs=csrs.getCarSchedulRecordHome(id);
		if(newCs.getViewsCar()==0){
			newCs.setViewsCar(Const.One);
			carService.updateNumber(newCs);
		}
		model.put("info", cs);
		model.put("enter", (User)getSession().getAttribute(Const.SESSION_USER));
		model.put("driverOption", driverOption);
		model.put("method", "save");
		model.put("trOption", transportTypeService.getTransportTypeOption(cs.getTransportTypeId(), false));
		model.put("carOption", carTypeService.getCarTypeOption(cs.getCarTypeId(), false));
		model.put("CarSFlag_GO", Const.CarSFlag_GO);
        model.put("CarSFlag_NO", Const.CarSFlag_NO);
        model.put("CarSFlag_OK", Const.CarSFlag_OK);
        model.put("noRef", getString("noRef"));
		return new ModelAndView("ye/second_department/car/dispatchCar",model);
	}
	
	/**
	 * 保存派车操作
	 * @return
	 */
	@RequestMapping(params="method=save")
	@SystemControllerLog(description = "保存派车操作")
	@ResponseBody
	public JSONObject saveCarSchedual() {
		JSONObject json = new JSONObject();
		try {
			CarSchedulRecord csr = csrs.getCarSchedulRecordById(getString("id"));
			csr.setCarId(getString("carId"));
			csr.setTransportTypeId(getString("transType"));
			csr.setCarTypeId(getString("carType"));
			csr.setComment(getString("comment"));
			csr.setStartTime(getTimeM("startTime"));
			csr.setReturnTime(getTimeM("returnTime"));
			csr.setDriverId(getString("driverId"));
//			csr.setFuneralId(getString("funeralId"));
			csr.setFuneralId(getString("assiginUser"));
			csr.setEnterId(getString("enterId"));
			csr.setRegTime(DateTools.getThisDateTimestamp());
			csr.setFlag(Const.CarSFlag_GO);
			csrs.upOrAdRecorder(csr);
			setJsonBySuccess(json, "派车成功", true);
			json.put("refresh", getSession().getAttribute("refresh"));
			String noRefresh = getString("noRef");
			if (noRefresh!=null && !noRefresh.equals("")) {
				json.put("noRefresh", true);
			}
		} catch (Exception e) {
			setJsonByFail(json, "派车失败，错误"+e.getMessage());
			e.printStackTrace();
		}
		return json;
	}
	
	/**
	 * 点击格子查看详情
	 * @return
	 */
	@RequestMapping(params="method=info")
	public ModelAndView showInfo() {
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String id = getString("id");
		CarSchedulRecord cr = new CarSchedulRecord();
		cr = csrs.getCarSchedulRecordById(id);
		String driver = userService.getNameById(cr.getDriverId());
//		String funeraler = userService.getNameById(cr.getFuneralId());
		String enter = userService.getNameById(cr.getEnterId());
		model.put("info", cr);
		model.put("enter", enter);
		model.put("driver", driver);
//		model.put("funeraler", funeraler);
		model.put("CarSFlag_GO", Const.CarSFlag_GO);
        model.put("CarSFlag_NO", Const.CarSFlag_NO);
        model.put("CarSFlag_OK", Const.CarSFlag_OK);
		return new ModelAndView("ye/second_department/car/dispatchCar",model);
	}
	/**
	 * 撤销调度/完成调度
	 * @return
	 */
	@RequestMapping(params="method=change")
	@SystemControllerLog(description = "撤销调度/完成调度")
	@ResponseBody
	public JSONObject change() {
		JSONObject json = new JSONObject();
		String type = getString("type");
		String id = getString("id");
		int result = 0;
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("id", id);
		try {
			if ("complete".equals(type)) {
				map.put("flag", Const.CarSFlag_OK);
				result = csrs.updateCarSchedulRecordFlag(map);
			} else if ("cancel".equals(type)) {
				map.put("flag", Const.CarSFlag_NO);
				map.put("cancel", "cancel");
				result= csrs.updateCarSchedulRecordFlag(map);
			}
			if (result != 0) {
				setJsonBySuccess(json, "操作成功", true);
			} else if (result == 0) {
				setJsonByFail(json, "操作失败");
			}
		} catch (Exception e) {
			setJsonByFail(json, "操作失败");
			e.printStackTrace();
		}
		return json;
	}
	/**
     * 选中返回时间
     * @return
     */
    @RequestMapping(params = "method=selectedTime")
    @ResponseBody
    public JSONObject selectedTime(){
    	JSONObject json = new JSONObject();
    	Date date = getDate("date");
    	if (date == null || "".equals(date)) {
    		date = DateTools.getThisDate();
		}
    	int col=getInt("col");//列
    	Object[] dOb = new Object[25];
		for (int i = 0; i < 24; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 0, i, Calendar.HOUR);
		}
		Date da=(Date) dOb[col];
		String time = DateTools.getTimeFormatString("yyyy-MM-dd HH:mm",da);
		int row=getInt("row");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isdel", 1);
		List<Car> carList = cs.getCarList(map,"name");
		Car car=carList.get(row-1);
		json.put("time", time);
		json.put("carId", car.getId());
		json.put("carNumber", car.getCarNumber());
		return json;
    }
    /**
     * 殡仪人员
     * @return
     */
    @RequestMapping(params = "method=choose")
    public ModelAndView chooseUser(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	List<Object[]> list = new ArrayList<Object[]>();
    	List<User> users = userService.getUserListByRoleName(Const.FUNERAL1);//入柜人员就是殡仪人员
    	users.addAll(userService.getUserListByRoleName(Const.FUNERAL2));
    	for(int i=0;i<users.size();i++){
    		list.add(new Object[]{users.get(i).getUserId(),users.get(i).getName()});
    	}
    	model.put("groupName", Const.ARRANGER);
    	model.put("list",list);
    	return new ModelAndView("../../common/worker",model);
    }
}
