package com.hz.controller.ye;

import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.DeadType;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.Furnace;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.payDivision.BusinessFees;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.MourningRecord;
import com.alibaba.fastjson.JSONObject;
import com.hz.service.base.AppellationService;
import com.hz.service.base.BillUnitService;
import com.hz.service.base.CertificateService;
import com.hz.service.base.CorpseAddressService;
import com.hz.service.base.CorpseUnitService;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.service.base.FreezerService;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.NationService;
import com.hz.service.base.ProveUnitService;
import com.hz.service.base.RegisterService;
import com.hz.service.base.SpecialBusinessTypeService;
import com.hz.service.base.ToponymService;
import com.hz.service.payDivision.BusinessFeesService;
import com.hz.service.system.DeptService;
import com.hz.service.system.UserService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.FuneralRecordService;
import com.hz.service.ye.FurnaceRecordService;
import com.hz.service.ye.ItemOrderService;
import com.hz.service.ye.MourningRecordService;


/**
 * 永嘉窗口
 * @author Gpf
 *
 */
@Controller
@RequestMapping("/yjcommissionOrder.do")
public class YJcommissionOrderControll extends BaseController {
	@Autowired
	private FuneralRecordService funeralRecordService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	//证件类型
	@Autowired
	private CertificateService certificateService;
	//死亡类型
	@Autowired
	private DeadTypeService deadTypeService;
	//死亡原因
	@Autowired
	private DeadReasonService deadReasonService;
	//证明单位
	@Autowired
	private ProveUnitService proveUnitService;
	//称谓关系
	@Autowired
	private AppellationService appellationService;
	//接尸单位
	@Autowired
	private CorpseUnitService corpseUnitService;
	//冷藏柜信息
	@Autowired
	private FreezerService freezerService;
	//户籍
	@Autowired
	private RegisterService registerService;
	//收费项目类别
	@Autowired
	private ItemTypeService itemTypeService;
	//收费项目
	@Autowired
	private ItemService itemService;
	//火化委托单服务项目
	@Autowired
	private ItemOrderService itemOrderService;
	//挂账单位
	@Autowired
	private BillUnitService billUnitService;
	//地区
	@Autowired
	private ToponymService toponymService;
	//名族
	@Autowired
	private NationService nationService;
	//接尸地址
	@Autowired
	private CorpseAddressService corpseAddService;
	//特殊业务类型
	@Autowired
	private SpecialBusinessTypeService specialBusinessTypeService;
	@Autowired
	private FurnaceService furnaceService;
	@Autowired
	private FurnaceRecordService furnaceRecordService;
	@Autowired 
	private FreezerRecordService freezerRecordService;
	@Autowired 
	private FarewellRecordService farewellRecordService;
	@Autowired 
	private MourningRecordService mourningRecordService;
	@Autowired
	private DeptService deptService;
	//收费记录
	@Autowired
	private BusinessFeesService businessFeesService;
	
	@ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "yjcommissionOrder.do");
    }
	@RequestMapping
    public ModelAndView unspecified() {
		return listCommissionOrder();
    }

	/**
	 * 永嘉登记
	 * 火化委托单列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listCommissionOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("checkFlag", getByte("checkFlag"));
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
//		String yongJia = new String();
//		yongJia  =	"永嘉县";
		
		String deptId=deptService.getDeptIdByName("永嘉馆");
		
//		maps.put("area", yongJia);
//		maps.put("city", "温州市");
		maps.put("deptId",deptId);
		maps.put("goto", Const.GOTO4);
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
		PageInfo<CommissionOrder> page=commissionOrderService.getCommissionOrderPageInfo(maps,pages[0],pages[1],"creat_time desc");
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name});
		model.put("searchOption", searchOption);
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("checkFlagOption", Const.getCheckFlagOption(getByte("checkFlag"),true));
		model.put("page", page);
        model.put("method", "list");
		return new ModelAndView("ye/yongjia/yjListCommissionOrder",model);
    }
    
    
    /**
	 * 链接到修改添加页面
	 * @return
	 */
	@RequestMapping(params = "method=edit")
	  public ModelAndView editCommissionOrder(){
			Map<String,Object> model=new HashMap<String,Object>();
			String id = getString("id");
			String deadReasonOption="";
			CommissionOrder commissionOrder=new CommissionOrder();
			if(id!=null&&!id.equals("")){
				commissionOrder=commissionOrderService.getCommissionOrderById(id);
				model.put("province", commissionOrder.getProvince());
				model.put("city", commissionOrder.getCity());
				model.put("area", commissionOrder.getArea());
				deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
				if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
					String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
					if(nationName != null){
						commissionOrder.setdNationId(nationName);
					}
				}
				
			}else {
				List<DeadType> list=deadTypeService.getDeadTypeList(null);
				if (list.size()>0) {
					DeadType deadType=list.get(0);
					deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId(),"");
				}
				
				//默认服务项目
				List<Object[]> serviceList=new ArrayList<Object[]>();
				Map<String,Object> serviceMaps=new HashMap<String, Object>();
				serviceMaps.put("type", Const.Type_Service);
				serviceMaps.put("defaultFlag", Const.Is_Yes);
				serviceMaps.put("isdel", Const.Isdel_No);
				List<Item> itemList=itemService.getTypeItemList(serviceMaps);
				Map<String, Object> maps1=new HashMap<String, Object>();
			    for(int i=0;i<itemList.size();i++){
			    	Item item=(Item)itemList.get(i);
			    	maps1.put("typeId", item.getTypeId());
			    	maps1.put("isdel",Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps1,item.getId(),false);
			    	String isOption=Const.getIsOption((byte)0, false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	serviceList.add(new Object[]{"service",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
			    }
				model.put("serviceList", serviceList);
				
				
				//默认丧葬用品
				List<Object[]> articlesList=new ArrayList<Object[]>();
				Map<String, Object> articlesMaps=new HashMap<String, Object>();
				articlesMaps.put("type", Const.Type_Articles);
				articlesMaps.put("defaultFlag", Const.Is_Yes);
				articlesMaps.put("isdel", Const.Isdel_No);
				List<Item> list1=itemService.getTypeItemList(articlesMaps);
				Map<String, Object> maps2=new HashMap<String, Object>();
			    for(int i=0;i<list1.size();i++){
			    	Item item=list1.get(i);
			    	maps2.put("typeId", item.getTypeId());
			    	maps2.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(maps2,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(articlesMaps,item.getTypeId(), false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps2,item.getId(),false);
			    	String isOption=Const.getIsOption((byte)0, false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	articlesList.add(new Object[]{"articles",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
			    }
				model.put("articlesList", articlesList);
			}
			//证件类型
			String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
			String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
			String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
			String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
			String dFlagOption="";
			if(id!=null &&!"".equals(id)){
				dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
			}else{
				dFlagOption=Const.getIsHaveOption(Const.Is_No, false);//默认未收死亡证明
			}
			String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
			String corpseUnitOption=corpseUnitService.getCorpseUnitOption(commissionOrder.getCorpseUnitId(),false);
			//String registerOption=registerService.getRegisterOption(commissionOrder.getRegisterId(), false);
			String toponymOption=toponymService.getToponymOption(commissionOrder.getToponymId(),false);
			//挂账单位
			String billUnitOption=billUnitService.getBillUnitOption(commissionOrder.getBillUnitId(), false);
			//民族
			String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);
			//接尸地址
			String corpseAddOption=corpseAddService.getCorpseAddressOption(commissionOrder.getPickAddr(), false);
			//特殊业务类型
			String specialBusinessOption=specialBusinessTypeService.getSpecialBusinessTypeOption(commissionOrder.getFurnaceComment(),false);
			
			
//			//默认服务项目
//			List<Object[]> serviceList=new ArrayList<Object[]>();
//			Map<String,Object> serviceMaps=new HashMap<String, Object>();
//			serviceMaps.put("type", Const.Type_Service);
//			serviceMaps.put("defaultFlag", Const.Is_Yes);
//			serviceMaps.put("isdel", Const.Isdel_No);
//			List<Item> list=itemService.getTypeItemList(serviceMaps);
//			Map<String, Object> maps1=new HashMap<String, Object>();
//		    for(int i=0;i<list.size();i++){
//		    	Item item=(Item)list.get(i);
//		    	maps1.put("typeId", item.getTypeId());
//		    	maps1.put("isdel", Const.Isdel_No);
//		    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
//		    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
//		    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps1,item.getId(),false);
//		    	String isOption=Const.getIsOption((byte)0, false);
//		    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
//		    	int sonIndex=item.getIndexFlag();
//		    	serviceList.add(new Object[]{"service",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
//		    }
//			model.put("serviceList", serviceList);
//			
//			
//			//默认丧葬用品
//			List<Object[]> articlesList=new ArrayList<Object[]>();
//			Map<String, Object> articlesMaps=new HashMap<String, Object>();
//			articlesMaps.put("type", Const.Type_Articles);
//			articlesMaps.put("defaultFlag", Const.Is_Yes);
//			articlesMaps.put("isdel", Const.Isdel_No);
//			List<Item> list1=itemService.getTypeItemList(articlesMaps);
//			Map<String, Object> maps2=new HashMap<String, Object>();
//		    for(int i=0;i<list1.size();i++){
//		    	Item item=list1.get(i);
//		    	maps2.put("typeId", item.getTypeId());
//		    	maps2.put("isdel", Const.Isdel_No);
//		    	String itemOption=itemService.getItemOption(maps2,item.getId(),false);
//		    	String itemTypeOption=itemTypeService.getItemTypeOption(articlesMaps,item.getTypeId(), false);
//		    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps2,item.getId(),false);
//		    	String isOption=Const.getIsOption((byte)0, false);
//		    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
//		    	int sonIndex=item.getIndexFlag();
//		    	articlesList.add(new Object[]{"articles",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
//		    }
//			model.put("articlesList", articlesList);
			
			//收费项目
			List<Object[]> service_list=new ArrayList<Object[]>();
			List<Object[]> articles_list=new ArrayList<Object[]>();
			if(id!=null&&!id.equals("")){
				Map<String, Object> maps=new HashMap<String, Object>();
				maps.put("commissionOrderId", id);
				List<ItemOrder> item_list=itemOrderService.getItemOrderList(maps);
				for (int i = 0; i < item_list.size(); i++) {
					ItemOrder itemOrder = item_list.get(i);
					String itemId=itemOrder.getItemId();
					Item item=itemService.getItemById(itemId);
					ItemType itemType=itemTypeService.getItemTypeById(item.getTypeId());
					//服务项目
					if (itemType.getType()==Const.Type_Service) {
						Map<String, Object>smaps=new HashMap<String, Object>();
						smaps.put("typeId", item.getTypeId());
						smaps.put("type", Const.Type_Service);
						smaps.put("isdel", Const.Isdel_No);
				    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
				    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
				    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
				    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
				    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
				    	int sonIndex=item.getIndexFlag();
				    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
					}
					//丧葬用品
					if (itemType.getType()==Const.Type_Articles) {
						Map<String, Object> smaps=new HashMap<String, Object>();
						smaps.put("typeId", item.getTypeId());
						smaps.put("type", Const.Type_Articles);
						smaps.put("isdel", Const.Isdel_No);
				    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
				    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
				    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
				    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
				    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
				    	int sonIndex=item.getIndexFlag();
				    	articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
					}	
				}
				model.put("service_list", service_list);
				model.put("articles_list", articles_list);
			}
			String checked="checked=checked";
	   		//火化炉
			List<Object[]> furnaceList=Const.getFurnaceList();
			List<Object[]> furnace_list=new ArrayList<Object[]>();
			FurnaceRecord furnaceRecord = new FurnaceRecord();
			if (id != null && !id.equals("")) {
				Map<String, Object>furnaceRecordMaps=new HashMap<String, Object>();
				furnaceRecordMaps.put("commissionOrderId", id);
				furnaceRecord = furnaceService.getFurnaceRecord(furnaceRecordMaps);
			} else {
				furnaceRecord=new FurnaceRecord();
			}
			for (int i = 0; i < furnaceList.size(); i++) {
				Object[] ob=furnaceList.get(i);
				byte by=(Byte) ob[0];
				
				if (by==commissionOrder.getFurnaceFlag()) {
					
					furnace_list.add(new Object[]{ob[0],ob[1],checked});
				}else {
					furnace_list.add(new Object[]{ob[0],ob[1],""});
				}		
			}
			//普通炉添加收费项目(点击单选按钮)
			Item item=itemService.getItemByHelpCode("301");	    	
			Map<String,Object> serMaps=new HashMap<String, Object>();
			serMaps.put("typeId", item.getTypeId());
			serMaps.put("isdel", Const.Isdel_No);
	    	String itemOption=itemService.getItemOption(serMaps,item.getId(),false);
	    	String itemTypeOption=itemTypeService.getItemTypeOption(serMaps,item.getTypeId(), false);
	    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serMaps,item.getId(),false);
	    	String isOption=Const.getIsOption((byte)0, false);
	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
			List<Object> funList=new ArrayList<Object>();
			funList.add("service");
			funList.add(itemOption);
			funList.add(itemHelpCodeOption);
			funList.add(item.getPice());
			funList.add(itemTypeOption);
			funList.add(1);
			funList.add(isOption);
			funList.add(item.getPice());
			funList.add(item.getComment());
			funList.add(faIndex);
			funList.add(sonIndex);
			model.put("funList", funList);
			
			//特约炉添加收费项目
			item=itemService.getItemByHelpCode("420");	    	
	    	itemOption=itemService.getItemOption(serMaps,item.getId(),false);
	    	itemTypeOption=itemTypeService.getItemTypeOption(serMaps,item.getTypeId(), false);
	    	itemHelpCodeOption=itemService.getItemHelpCodeOption(serMaps,item.getId(),false);
	    	isOption=Const.getIsOption((byte)0, false);
	    	
			List<Object> funListTY=new ArrayList<Object>();
			funListTY.add("service");
			funListTY.add(itemOption);
			funListTY.add(itemHelpCodeOption);
			funListTY.add(item.getPice());
			funListTY.add(itemTypeOption);
			funListTY.add(1);
			funListTY.add(isOption);
			funListTY.add(item.getPice());
			funListTY.add(item.getComment());
			funListTY.add(faIndex);
			funListTY.add(sonIndex);
			model.put("funListTY", funListTY);
			
			
			model.put("furnaceRecord", furnaceRecord);
			model.put("furnaceList", furnaceList);
			model.put("furnace_list", furnace_list);
			model.put("furnace_ty", Const.furnace_ty);
			
		
		byte checkFlag=commissionOrder.getCheckFlag();
		model.put("checkFlag", checkFlag);
		//model.put("registerOption", registerOption);
		model.put("toponymOption", toponymOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("fAppellationOption", fAppellationOption);
		model.put("dFlagOption", dFlagOption);
		model.put("proveUnitOption", proveUnitOption);
		model.put("sexOption", sexOption);
		model.put("deadReasonOption", deadReasonOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("specialBusinessOption", specialBusinessOption);
		model.put("certificateOption", certificateOption);
		model.put("commissionOrder", commissionOrder);
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("id", id);
		model.put("billUnitOption", billUnitOption);
		model.put("nationOption", nationOption);
		model.put("corpseAddOption", corpseAddOption);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("isOption", Const.getIsOption((byte)0, false));
		model.put("agentUser", getCurrentUser().getName());
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=format.format(date);
		Timestamp now=new Timestamp(System.currentTimeMillis());
		model.put("now", format.format(now));
		model.put("time",time);
        model.put("method", "save");
		return new ModelAndView("ye/yongjia/yJEditCommissionOrder",model);
    }
	
	 
    
    /**
   	 * 链接到修改添加服务项目
   	 * @return
   	 */
       @RequestMapping(params = "method=selectItems")
       public ModelAndView editItemOrders(){
   		Map<String,Object> model=new HashMap<String,Object>();
   		List<Object[]> list=new ArrayList<Object[]>();
   		Map<String, Object>maps=new HashMap<String, Object>();
   		maps.put("type", getByte("type"));
   		maps.put("isdel", Const.Isdel_No);
   		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
   		for (int i = 0; i < itemTypeList.size(); i++) {
   			ItemType itemType=(ItemType)itemTypeList.get(i);
   			maps.put("typeId",itemType.getId());
   			maps.put("isdel", Const.Isdel_No);
   			String itemOption=itemService.getItemOption(maps, "",true);
   			list.add(new Object[]{itemType.getName(),itemOption});
   		}
   		//添加服务项目
   		List<Object[]> itemList=new ArrayList<Object[]>();
   		for (int i = 0; i < itemTypeList.size(); i++) {
   			ItemType itemType=(ItemType)itemTypeList.get(i);
   			Map<String, Object>itemMaps=new HashMap<String, Object>();
   			itemMaps.put("typeId", itemType.getId());
   			itemMaps.put("isdel", Const.Isdel_No);
   			List<Item> itemList1=itemService.getItemList(itemMaps,"index_flag asc");
   			itemList.add(new Object[]{itemType,itemList1});
   		}
   		model.put("itemTree", getTree(itemList));
           model.put("list", list);
           model.put("type", getByte("type"));
           model.put("id", getString("id"));
           model.put("Type_Articles", Const.Type_Articles);
   		model.put("Type_Service", Const.Type_Service);
   		return new ModelAndView("ye/commissionOrder/selectItems",model);
       }
    
       
       /**
        * 删除火化委托单
        * @return
        */
       @RequestMapping(params = "method=delete")
       @SystemControllerLog(description = "删除永嘉火化委托单")
       @ResponseBody
       public JSONObject deleteCommissionOrder(){
   		JSONObject json = new JSONObject();
   		String id = getString("ids");
       	try {
       		String[] ids = id.split(",");
			for (String coId : ids) {
				byte checkFlag = commissionOrderService.getCheckFlag(coId);
				if (1 == checkFlag) {
					json.put("statusCode", 302);
					json.put("message", "已审核，不能删除");
					return json;
				}
				commissionOrderService.deleteCommissionOrder(coId);
			}
       		setJsonBySuccess(json, "删除成功", true);
   		} catch (Exception e) {
   			e.printStackTrace();
   			setJsonByFail(json, "删除失败，错误"+e.getMessage());
   		}
   		return json;
       }
      
       /**
        * 保存火化委托单
        * @return
        */
       @RequestMapping(params = "method=save")
       @SystemControllerLog(description = "保存永嘉火化委托单")
       @ResponseBody
       public JSONObject saveCommissionOrder(){
   		JSONObject json = new JSONObject();
       	try {
       		String userId=getCurrentUser().getUserId();
       		String userName=getCurrentUser().getName();
       		String id =getString("id");
       		int age=getInt("age");
       		Timestamp arriveTime=getTimeM("arriveTime");
       		String cardCode=getString("cardCode");
       		if (id == null || id.equals("")) {
   				int num = commissionOrderService.getCommissionOrderByCard(cardCode);
   				if (num >= 1) {
   					setJsonByFail(json, "该卡号已经存在");
   					return json;
   				}
   			}else{//有id号进行判断是否收费
				List<BusinessFees> busList= businessFeesService.getBusinessFeesByComId(id);
				if(busList.size()>0){//已有收费，限制保存
					setJsonByFail(json, "该死者已收费，无法保存，请返回");
					return json;
				}
			}
       		String province=getString("province");
    		String city=getString("city");
    		String area=getString("area");
       		String certificateCode=getString("certificateCode");
       		String certificateId=getString("certificateId");
       		Timestamp checkTime=getTimeM("checkTime");
       		String checkUserId=getString("checkUserId");
       		String cComment=getString("comment");
       		String corpseUnitId=getString("corpseUnitId");
       		Timestamp orderTime=getTimeM("orderTime");
       		String dAddr=getString("dAddr");
       		String deadReasonId=getString("deadReasonId");
       		String deadTypeId=getString("deadTypeId");
       		byte dFlag=getByte("dFlag");
       		//死者身份证图片路径
       		String dIdcardPic =getString("filename");
       		//System.out.println(dIdcardPic);
       		//经办人身份证图片路径
       		String eIdcardPic=getString("filenameF");
       		//System.out.println(eIdcardPic);
       		
       		String dNation=getString("dNation");
       		if(dNation!=null && dNation!=""){
       			String nationId = nationService.getdnationId(dNation);
       			if(nationId != null){
       				dNation = nationId;
       			}
       		}
       		Timestamp dTime=getTimeM("dTime");
       		String fAddr=getString("fAddr");
       		String fAppellationId=getString("fAppellationId");
       		String fName=getString("fName");
       		String fPhone=getString("fPhone");
       		String fUnit=getString("fUnit");
       		String fCardCode=getString("fCardCode");
       		String furnaceTypeId=getString("furnaceTypeId");
       		String furnaceId=getString("furnaceId");
       		String name=getString("name");
       		String pickAddr=getString("pickAddr");
       		String proveUnitId=getString("proveUnitId");
       		String proveUnitContent=getString("proveUnitContent");
       		String registerId=getString("registerId");
       		String toponymId = getString("toponymId");
       		String specialBusinessValue=getString("specialBusinessValue");
       		byte scheduleFlag=getByte("scheduleFlag");
       		byte sex=getByte("sex");
       		byte radio=getByte("radio");
   			
   			//挂账单位
       		String billUnitId=getString("billUnitId");
       	
       		
       		//收费项目
       		String[] service=getArrayString("service_id");//服务项目
       		String[] articles=getArrayString("articles_id");//丧葬用品
       		String[] itemId=getArrayString("itemId");
       		String[] number=getArrayString("number");
       		String[] bill=getArrayString("bill");
       		String[] total=getArrayString("total");
       		String[] pice=getArrayString("pice");
       		String[] comment=getArrayString("comment");
       		//车辆调度
       		String[] transportType=getArrayString("transportTypeId");
       		String[] carType=getArrayString("carTypeId");
       		Timestamp[] pickTime=getArrayTimestamp("pickTime");
       		String[] carComment=getArrayString("carComment");
       		String[] carSchedulRecordId=getArrayString("csr_id");
       		
   			//冷藏柜使用记录
       		
   			String freezerId=getString("freezerId");
   			//冷藏柜使用记录    入柜经办人
   			String assiginUser   =  getString("assiginUser");
   		//业务二科到馆  二科登记默认为  到馆 即为入柜
       		byte  freezerFlag  = Const.IsFlag_Yse;
   			
   			
   			//告别厅
   			Timestamp farewellBeginDate=getTimeM("farewellBeginDate");
   			Timestamp farewellEndDate=getTimeM("farewellEndDate");
   			String farewellId=getString("farewellId");
   			//守灵室
   			Timestamp mourningBeginTime=getTimeM("mourningBeginTime");
   			Timestamp mourningEndTime=getTimeM("mourningEndTime");
   			String mourningId=getString("mourningId");
   			
   			
   			String[] itemIdR=getArrayString("reduction_itemId");
   			String[] numberR=getArrayString("reduction_number");
   			String[] totalR=getArrayString("reduction_total");
   			String[] commentR=getArrayString("reduction_comment");
   			//以下新增,用于记录困难减免记录
			String[] hardIdR=getArrayString("hard_itemId");
			String[] hardnumberR=getArrayString("hard_number");
			String[] hardtotalR=getArrayString("hard_total");
			String[] hardcommentR=getArrayString("hard_comment");
   			//基本减免
   			String[] base=getArrayString("base_id");//基本减免项目
   			String baseReductionId=getString("baseReductionId");//基本减免Id
   			String[] freePersonId=getArrayString("freePersonId");//免费对象类别
   			String[] hardPersonId=getArrayString("hardPersonId");//重点救助对象类别
       		byte baseIs=getByte("baseIs");
   			String fidcard=getString("fidcard");
   			
   			
   			//困难减免
   			String[] hard=getArrayString("hard_id");//困难减免项目
   			String[] proveIds=getArrayString("proveIds");
   			String hardReductionId=getString("hardReductionId");
   			byte hardIs=getByte("hardIs");
   			String hard_proveUnitId=getString("hard_proveUnitId");
   			String reason=getString("reason");
   			String hComment=getString("comment");
   			Timestamp createTime=getTimeM("createTime");
   			String appellationId=getString("appellationId");
   			String applicant=getString("applicant");
   			String phone=getString("phone");
   			byte special=getByte("special");
   			
   			
   			// 新增  三项   业务二科 登记   登记人员
   			String pbookAgentId = getString("pbookAgentId");
       		String bookAgentId = getString("bookAgentId");
       		

       		
   			// 跳转页面的判断
       		//******************************************************
   			String source = new String();
   			source=getString("sourcepage");
   			if (source.equals("phoneBook")){
   				pbookAgentId = getCurrentUser().getName();
   			}else{
   				if(source.equals("editCommissionOrder")){
   					bookAgentId = getCurrentUser().getName();
   				}
   			}
   			String aisle="yj";
       		String coId=commissionOrderService.addOrUpdateMethod(userId,userName, id, age, arriveTime, cardCode
       				, certificateCode, certificateId, checkTime, checkUserId
       				, cComment, corpseUnitId, orderTime, dAddr, deadReasonId
       				, deadTypeId, dFlag, dIdcardPic, dNation, dTime, eIdcardPic
       				, fAddr, fAppellationId, fName, fPhone, fUnit,fCardCode
       				, furnaceTypeId, furnaceId, name, pickAddr, proveUnitId
       				, proveUnitContent, registerId, toponymId, scheduleFlag, sex, radio
       				, service, articles, itemId, number, bill, total, comment
       				, transportType, carType, pickTime, carComment, carSchedulRecordId
       				, farewellBeginDate, farewellEndDate, farewellId
       				, freezerId, mourningBeginTime, mourningEndTime
       				, mourningId, orderTime, itemIdR, numberR, totalR
       				, commentR, base, baseReductionId, freePersonId
       				, hardPersonId, baseIs, fidcard, hard, proveIds
       				, hardReductionId, hardIs, hard_proveUnitId, reason
       				, hComment, createTime, appellationId, applicant
       				, phone, special,billUnitId,pbookAgentId,bookAgentId,assiginUser,freezerFlag 
       				, hardIdR,hardnumberR,hardtotalR,hardcommentR,province,city,area,specialBusinessValue
       				, null,pice,null,null,null,aisle,null);
       		setJsonBySuccess(json, "保存成功", "yjcommissionOrder.do?method=list");
       		if(id!=null && !"".equals(id)){
       			json.put("coId", id);
       		}else{
       			json.put("coId", coId);    			
       		}
   		} catch (Exception e) {
   			e.printStackTrace();
   			setJsonByFail(json, "保存失败，错误"+e.getMessage());
   		}
   		return json;
       }
       
       
       
       /**
   	 * 链接到火化委托单页面（只读页面）
   	 * @return
   	 */
   	@RequestMapping(params = "method=readOnly")
       public ModelAndView editCommissionOrderReadOnly(){
   		Map<String,Object> model=new HashMap<String,Object>();
   		String adr=getString("adr");
   		model.put("adr", adr);
   		String id = getString("id");
   		//首页未审核更改查看状态
		if("D".equals(adr)){
			commissionOrderService.updateNumber2(id,Const.One);
		}
		//首页礼仪出殡更改查看状态
		String upItemId=getString("itemId");
		if(upItemId!=null&&!"".equals(upItemId)){
			funeralRecordService.updateViewsFuneral(upItemId,Const.One);
		}
		String ifCheck=getString("ifCheck");//此处变量决定是否需要审核
   		
   		String deadReasonOption="";
   		CommissionOrder commissionOrder=new CommissionOrder();
   		if(id!=null&&!id.equals("")){
   			commissionOrder=commissionOrderService.getCommissionOrderById(id);
   			String deadArea = commissionOrder.getProvince() + " " + commissionOrder.getCity() + " " + commissionOrder.getArea();
   			model.put("deadArea", deadArea);
   			deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
   			if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
				String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
				if(nationName != null){
					commissionOrder.setdNationId(nationName);
				}
			}
   		}else {
   			List<DeadType> list=deadTypeService.getDeadTypeList(null);
   			if (list.size()>0) {
   				DeadType deadType=(DeadType)list.get(0);
   				deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId(),"");
   			}
   		}
   		String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
   		String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
   		String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
   		String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
   		String dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
   		String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
   		String corpseUnitOption=corpseUnitService.getCorpseUnitOption(commissionOrder.getCorpseUnitId(),false);
   		String registerOption=registerService.getRegisterOption(commissionOrder.getRegisterId(), false);
   		String toponymOption=toponymService.getToponym(commissionOrder.getToponymId(),false);
   		//挂账单位
   		String billUnitOption=billUnitService.getBillUnitOption(commissionOrder.getBillUnitId(), false);
   		//民族
   		/*String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);*/
   		
   		
   		
   		//默认服务项目
   		List<Object[]> serviceList=new ArrayList<Object[]>();
   		Map<String, Object>serviceMaps=new HashMap<String, Object>();
   		serviceMaps.put("type", Const.Type_Service);
   		serviceMaps.put("defaultFlag", Const.Is_Yes);
   		List<Item> list=itemService.getTypeItemList(serviceMaps);
   		Map<String, Object>maps1=new HashMap<String, Object>();
   	    for(int i=0;i<list.size();i++){
   	    	Item item=(Item)list.get(i);
   	    	maps1.put("typeId", item.getTypeId());
   	    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
   	    	String itemTypeOption=itemTypeService.getItemTypeOption(maps1,item.getTypeId(), false);
   	    	String isOption=Const.getIsOption((byte)0, false);
   	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
   	    	serviceList.add(new Object[]{"service",itemOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
   	    }
   		model.put("serviceList", serviceList);
   		
   		
   		//默认丧葬用品
   		List<Object[]> articlesList=new ArrayList<Object[]>();
   		Map<String, Object>articlesMaps=new HashMap<String, Object>();
   		articlesMaps.put("type", Const.Type_Articles);
   		articlesMaps.put("defaultFlag", Const.Is_Yes);
   		List<Item> list1=itemService.getTypeItemList(articlesMaps);
   		Map<String, Object>maps2=new HashMap<String, Object>();
   	    for(int i=0;i<list1.size();i++){
   	    	Item item=list1.get(i);
   	    	maps2.put("typeId", item.getTypeId());
   	    	String itemOption=itemService.getItemOption(maps2,item.getId(),false);
   	    	String itemTypeOption=itemTypeService.getItemTypeOption(articlesMaps,item.getTypeId(), false);
   	    	String isOption=Const.getIsOption((byte)0, false);
   	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
   	    	articlesList.add(new Object[]{"articles",itemOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
   	    }
   		model.put("articlesList", articlesList);
   		
   		
   		//收费项目
   		List<Object[]> service_list=new ArrayList<Object[]>();
   		List<Object[]> articles_list=new ArrayList<Object[]>();
   		if(id!=null&&!id.equals("")){
   			Map<String, Object>maps=new HashMap<String, Object>();
   			maps.put("commissionOrderId", id);
   			List<ItemOrder> item_list=itemOrderService.getItemOrderList(maps);
   			for (int i = 0; i < item_list.size(); i++) {
   				ItemOrder itemOrder=item_list.get(i);
   				String itemId=itemOrder.getItemId();
   				Item item=itemService.getItemById(itemId);
   				ItemType itemType=itemTypeService.getItemTypeById(item.getTypeId());
   				//服务项目
   				if (itemType.getType()==Const.Type_Service) {
   					Map<String, Object>smaps=new HashMap<String, Object>();
   					smaps.put("typeId", item.getTypeId());
   					smaps.put("type", Const.Type_Service);
   				
   			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
   			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
   			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
   			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
   			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
   			    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
   				}
   				//丧葬用品
   				if (itemType.getType()==Const.Type_Articles) {
   					Map<String, Object>smaps=new HashMap<String, Object>();
   					smaps.put("typeId", item.getTypeId());
   					smaps.put("type", Const.Type_Articles);
   			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
   			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
   			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
   			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
   			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
   			    	articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
   				}	
   			}
   			model.put("service_list", service_list);
   			model.put("articles_list", articles_list);
   		}
   		
   		String checked="checked=checked";
   		//火化炉
		List<Object[]> furnaceList=Const.getFurnaceList();
		List<Object[]> furnace_list=new ArrayList<Object[]>();
		FurnaceRecord furnaceRecord = new FurnaceRecord();
		if (id != null && !id.equals("")) {
			Map<String, Object>furnaceRecordMaps=new HashMap<String, Object>();
			furnaceRecordMaps.put("commissionOrderId", id);
			furnaceRecord = furnaceService.getFurnaceRecord(furnaceRecordMaps);
		} else {
			furnaceRecord=new FurnaceRecord();
		}
		for (int i = 0; i < furnaceList.size(); i++) {
			Object[] ob=furnaceList.get(i);
			byte by=(Byte) ob[0];
			
			if (by==commissionOrder.getFurnaceFlag()) {
				
				furnace_list.add(new Object[]{ob[0],ob[1],checked});
			}else {
				furnace_list.add(new Object[]{ob[0],ob[1],""});
			}		
		}
		String A="普通炉";
		String B="特约炉";
		if(commissionOrder.getFurnaceFlag()==1){
			model.put("tname",A);
		}
		
		if(commissionOrder.getFurnaceFlag()==2){
			model.put("tname",B);
		}
		model.put("furnaceRecord", furnaceRecord);
		model.put("furnaceList", furnaceList);
		model.put("furnace_list", furnace_list);
		model.put("furnace_ty", Const.furnace_ty);
   		byte checkFlag=commissionOrder.getCheckFlag();
   		model.put("checkFlag", checkFlag);
   		model.put("registerOption", registerOption);
   		model.put("corpseUnitOption", corpseUnitOption);
   		model.put("fAppellationOption", fAppellationOption);
   		model.put("dFlagOption", dFlagOption);
   		model.put("proveUnitOption", proveUnitOption);
   		model.put("sexOption", sexOption);
   		model.put("deadReasonOption", deadReasonOption);
   		model.put("deadTypeOption", deadTypeOption);
   		model.put("certificateOption", certificateOption);
   		model.put("commissionOrder", commissionOrder);
   		model.put("Check_No", Const.Check_No);
   		model.put("Check_Yes", Const.Check_Yes);
   		model.put("IsHave_Yes", Const.IsHave_Yes);
   		model.put("IsHave_No", Const.IsHave_No);
   		model.put("Is_Yes", Const.Is_Yes);
   		model.put("Is_No", Const.Is_No);
   		model.put("id", id);
   		model.put("billUnitOption", billUnitOption);
   		/*model.put("nationOption", nationOption);*/
   		model.put("Type_Articles", Const.Type_Articles);
   		model.put("Type_Service", Const.Type_Service);
   		model.put("isOption", Const.getIsOption((byte)0, false));
   		model.put("agentUser", getCurrentUser().getName());
   		Date date=new Date();
   		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
   		String time=format.format(date);
   		model.put("time",time);
   	    model.put("ifCheck", ifCheck);
   	    model.put("toponymOption", toponymOption);
   		return new ModelAndView("ye/yongjia/readOnlyCommissionorderYongJia",model);
       }
       
    /**
     * 是否审核
     * @return
     */
    @RequestMapping(params = "method=checkFlag")
    @SystemControllerLog(description = "修改永嘉火化委托单审核与否")
    @ResponseBody
    public JSONObject isdelCarType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	String[] ids = id.split(",");
	    	byte checkFlag=getByte("checkFlag");
	    	Timestamp nowTime=new Timestamp(System.currentTimeMillis());
	    	Map<String, Object> maps = null;
		    for(int i=0;i<ids.length;i++){
		    	maps = new HashMap<String, Object>();
				maps.put("commissionOrderId", ids[i]);
				//冷藏柜记录
				List<FreezerRecord> fList=freezerRecordService.getFreezerRecordList(maps);
				//告别厅记录
				List<FarewellRecord> faList=farewellRecordService.getFarewellRecordList(maps,"");
				//灵堂记录
				List<MourningRecord> mList=mourningRecordService.getMourningRecordList(maps,"");
				//火化炉调度
				maps.put("changeFlag", Const.ChangeFlag_No);
				List<FurnaceRecord> fuList=furnaceRecordService.getFurnaceRecordList(maps, "");
				if (checkFlag==Const.Check_No) {
					maps.put("checkFlag", Const.Check_No);
				}else if(checkFlag==Const.Check_Yes){
					maps.put("checkFlag", Const.Check_Yes);
				}
				maps.put("checkTime", nowTime);
				
				
				//以下判断 挂账火化委托单里的非挂账业务是否已付款
				Map<String,Object> paramMap=new HashMap<String,Object>();
				paramMap.put("tickFlag", Const.Is_Yes);
				paramMap.put("commissionOrderId", ids[i]);
				List<ItemOrder> listItem=itemOrderService.getItemOrderList(paramMap);
				boolean flag=false;//非挂账都为false
				if(listItem!=null){
					flag=true;//挂账业务默认为true
					paramMap.put("tickFlag", Const.Is_No);
					List<ItemOrder> NoTicklist=itemOrderService.getItemOrderList(paramMap);
					for(int j=0;j<NoTicklist.size();j++){
						byte payFlag=NoTicklist.get(j).getPayFlag();
						if(payFlag==2){//若在挂账业务中有非挂账的,并且未付款
							flag=false;//则此挂账业务也为false
						}
					}
				}
				CommissionOrder commissionOrder = new CommissionOrder();
	    		commissionOrder=commissionOrderService.getCommissionOrderById(ids[i]);
	    		List<ItemOrder> list = new ArrayList<ItemOrder>();
	    		Map<String,Object> map=new HashMap<String,Object>();
	    		map.put("commissionOrderId", id);
	    		map.put("type", Const.Type_Articles);
	    		list = itemOrderService.getItemOrderListForNotice(map);
		    	commissionOrderService.checkMethod(maps, checkFlag,fList,faList,mList,fuList,flag,commissionOrder,list);
		    	
		    }
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
			e.printStackTrace();
		}
		return json;
    }
    
    /**
     * 死亡类型改变
     * @return
     */
    @RequestMapping(params = "method=deadTypeChange")
    @ResponseBody
    public JSONObject deadTypeChange(){
    	JSONObject json = new JSONObject();
    	String typeId=getString("id");
    	String reasonId=getString("reasonId");
    	String str=deadReasonService.getDeadReasonChange(typeId,reasonId);
    	json.put("str", str);
		return json;
    }
    
    /**
     * 项目类别改变
     * @return
     */
    @RequestMapping(params = "method=itemTypeChange")
    @ResponseBody
    public JSONObject itemTypeChange(){
		JSONObject json = new JSONObject();
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId", getString("id"));
		maps.put("isdel", Const.Isdel_No);
		List<Item> list=itemService.getItemList(maps);
		Item item=new Item();
		if (list.size()>0) {
			item=list.get(0);
		}
    	String str=itemService.getItemOption(maps, "", false);
		json.put("pice", item.getPice());
		json.put("str", str);
		return json;
    }
    
    
    /**
     * 项目名称改变
     * @return
     */
    @RequestMapping(params = "method=itemNameChange")
    @ResponseBody
    public JSONObject itemNameChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId",item.getTypeId());
		maps.put("isdel", Const.Isdel_No);
    	String str =itemService.getItemHelpCodeOption(maps, item.getId(), false);
		json.put("pice", item.getPice());
		json.put("str", str);
		return json;
    }
    
    /**
     * 项目名称改变
     * @return
     */
    @RequestMapping(params = "method=itemHelpCodeChange")
    @ResponseBody
    public JSONObject itemHelpCodeChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId",item.getTypeId());
    	String str =itemService.getItemOption(maps, item.getId(), false);
    	json.put("pice", item.getPice());
		json.put("str", str);
		return json;
    }
    
    /**
	 * 链接到修改添加服务项目
	 * @return
	 */
    @RequestMapping(params = "method=selectItem")
    public ModelAndView editItemOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=itemTypeList.get(i);
			maps.put("typeId",itemType.getId());
			String itemOption=itemService.getItemOption(maps, "",true);
			list.add(new Object[]{itemType.getName(),itemOption});
		}
        model.put("list", list);
        model.put("type", getByte("type"));
        model.put("id", getString("id"));
        model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		return new ModelAndView("ye/commissionOrder/selectItem",model);
    }
    
 
    /**
     * 添加服务项目
     * @return
     */
    @RequestMapping(params = "method=addItems")
    @ResponseBody
    public List<Object[]> saveItemOrder(){
		List<Object[]> list = new ArrayList<Object[]>();
		List<String> itemIist = new ArrayList<String>();
		String id =getString("ids");
		String[] ids = id.split(",");
		for(int i=0;i<ids.length;i++){
			if (!ids[i].equals("")) {
				itemIist.add(ids[i]);
			}
		}
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		Map<String, Object>maps1=new HashMap<String, Object>();
	    for(int i=0;i<itemIist.size();i++){
	    	Item item=itemService.getItemById(itemIist.get(i).toString());
	    	maps1.put("typeId", item.getTypeId());
	    	String itemOption=itemService.getItemOption(null,itemIist.get(i),false);
	    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(null,item.getId(),false);
	    	String itemTypeOption=itemTypeService.getItemTypeOption(maps,item.getTypeId(), false);
	    	String isOption=Const.getIsOption((byte)0, false);
	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
	    	list.add(new Object[]{itemOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),itemHelpCodeOption,faIndex,sonIndex});
	    }
		return list;
    }
    /**
     * 添加服务项目
     * @return
     */
    @RequestMapping(params = "method=addItem")
    @ResponseBody
    public String addItem(){
//    	int[] pages = getPage();
		Map<String, Object>maps=new HashMap<String, Object>();
		String id =getString("ids");
		String[] ids =  id.split(",");//Id数组
		String ind =getString("ind");
		String[] checkId =  ind.split(",");//复选框Id数组
		List<String> idsList=new ArrayList<String>();
		int[] index=new int[ids.length];
		String[] inds=new String[ids.length];
		int a=0;
		for (int i = 0; i < ids.length; i++) {
			idsList.add(ids[i]);
			if (!ids[i].equals("")&&ids[i]!="") {
				index[a]=i;
				inds[a]=checkId[i];
				a++;
			}
		}
		maps.put("ids", idsList);
//		List list = new ArrayList();
		String str="";
		List<Item> itemList =itemService.getItemList(maps,"b.index_flag asc,a.index_flag asc");
//		while(itemList.size()>0){
//			
//			Item item=(Item)itemList.get(0);
//			for (int j = 0; j < itemList.size(); j++) {
//				Item item1=(Item)itemList.get(j);
//				if (item.getIndexFlag()>item1.getIndexFlag()) {
//					item=item1;
//				}
//			}
//			itemList.remove(0); 
//			list.add(item);
//		}
		str+="{";
		int x=0;
		for (int i = 0; i < itemList.size(); i++) {
			if (i>0) {
				str+=",";
			}
			Item item=(Item)itemList.get(i);
			x++;
			str+=""+'"'+x+"."+'"'+":{"+'"'+"id"+'"'+":"+'"'+item.getId()+'"'+","
			+'"'+"indexFlag"+'"'+":"+'"'+x+'"'+","
			+'"'+"name"+'"'+":"+'"'+item.getName()+'"'+","
			+'"'+"index"+'"'+":"+'"'+index[i]+'"'+","
			+'"'+"inds"+'"'+":"+'"'+inds[i]+'"'+"}";
		}
		str+="}";
		return str;
    } 
       
       
       /**
        * 获得tree结构
        * @param list
        * @return
        */
   	public String getTree(List<Object[]> list){
       	int sum=0;
       	String content="";
       	for (int i = 0; i < list.size(); i++) {
       		if (i>0) {
       			content+=",";
   			}
       		Object[] objects=list.get(i);
       		ItemType itemType=(ItemType)objects[0];
   	    	content+= "{";
   	    	content+="text:'"+itemType.getName()+"'";
//   	    	content+=",tags:['"+itemType.getId()+"']";
   	    	content+=",selectable: false";
   	    	List<?> itemList=(List<?>)objects[1];
   	    	content+=",nodes: [";
   	    	for (int j = 0; j < itemList.size(); j++) {
   	    		if (j>0) {
   	    			content+=",";
   				}
   	    		Item item=(Item)itemList.get(j);
   	    		content+="{";
   	    		content+="text:'"+item.getName()+"'";
   	    		content+=",tags:['"+item.getId()+"_"+sum+"']";
   	    		sum++;
   	    		content+="}";
   	    	}
   	    	content+="]";
   	    	content+="}";
       	}
       	return content;
       }


    /**
     * 火化炉调度页面
     * @return
     */
    @RequestMapping(params = "method=editFurnace")
    public ModelAndView editFurnace(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int time=getInt("time");
		Date date = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("current_time")!=null){
			date = getDate("current_time");
		}
		if (time!=0) {
			date=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), time, Calendar.DATE);
		}
		model.put("date", date);
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("furnaceBeginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("furnaceEndDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		List<FurnaceRecord> frList = furnaceService.getFurnaceRecordList(maps);
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[19];
		dOb[0]="炉号";
		for (int i = 0; i < dOb.length-1; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 1, i, Calendar.HOUR);
		}
		list.add(dOb);
		for (Furnace furnace : furnaceList) {
			Object[] o = new Object[dOb.length];
			o[0] = furnace;
			list.add(o);
		}
		for (FurnaceRecord furnaceRecord : frList) {
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;//列
			for (int i = 1; i < list.size(); i++) {
				Object[] o = list.get(i);
				Furnace furnace = (Furnace) o[0];
				if (furnace.getId().equals(furnaceRecord.getFurnaceId())) {
					row = i;
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if (furnaceRecord.getEndTime() != null && !(furnaceRecord.getEndTime().equals(""))) {
					if(DateTools.isSameHour(da, new Date(furnaceRecord.getBeginTime().getTime()))) {
						col = d;
					}
					if(DateTools.isSameHour(da, new Date(furnaceRecord.getEndTime().getTime()))){
						endCol = d;
					}
				} else {
					if (furnaceRecord.getOrderTime() != null && !(furnaceRecord.getOrderTime().equals(""))) {
						if(DateTools.isSameHour(da, new Date(furnaceRecord.getOrderTime().getTime()))) {
							col = d;
						}
					}
				}
			}
			if(row!=0){
				Object[] o = (Object[])list.get(row);
				if (furnaceRecord.getEndTime() != null && !(furnaceRecord.getEndTime().equals(""))) {
					if(endCol==0){
						endCol = dOb.length-1;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						o[x] = furnaceRecord;
					}
				} else {
					if (col != 0) {
						o[col] = furnaceRecord;
					}
				}
				
			}
		}
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Wc", Const.IsFlag_Wc);
		model.put("IsFlag_Jx", Const.IsFlag_Jx);
		model.put("method", list);
		model.put("list", list);
		model.put("dOb", dOb);
		model.put("list_size", list.size());
		if (getString("where").equals("changeFur")) {
			return new ModelAndView("ye/commissionOrder/editFurnace2",model);//入炉转炉时候用
		} else {
			return new ModelAndView("ye/commissionOrder/editFurnace",model);
		}
    }
    
    
    
    /**
     * 火化炉点击
     * @return
     */
    @RequestMapping(params = "method=furnace_click")
    @ResponseBody
    public JSONObject furnace_click(){
    	JSONObject json = new JSONObject();
    	Date date = getDate("date");
    	int col=getInt("col");//列
    	int row=getInt("row");
		//判断页面是否有传日期回来，是则赋值给date
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Object[] dOb = new Object[19];
		for (int i = 0; i < dOb.length-1; i++) {
			dOb[i]=DateTools.getHourAfterHour(new Date(date.getTime()), 1, i, Calendar.HOUR);
		}
		Date date1=(Date)dOb[col-1];
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=sdf.format(date1);
		Furnace furnace=furnaceList.get(row-1);
		json.put("time", time);
		json.put("id", furnace.getId());
		json.put("name", furnace.getName());
		return json;
    }
    
    /**
     * 解锁
     * @return
     */
    @RequestMapping(params = "method=unlock")
    @SystemControllerLog(description = "永嘉火化委托单解销")
    @ResponseBody
    public JSONObject unlock(){
    	JSONObject json = new JSONObject();
		List<Freezer> freezerList = freezerService.getFreezerList(null,"index_flag asc,name asc");
		for (int i = 0; i < freezerList.size(); i++) {
			Freezer freezer2=freezerList.get(i);
			if (freezer2.getFlag()==Const.IsFlag_Lock) {
				if (freezer2.getUserName().equals(getCurrentUser().getName())||getCurrentUser().getName()==freezer2.getUserName()) {
					freezer2.setFlag(Const.IsFlag_No);
					freezerService.updateFreezer(freezer2);
				}
			}
		}
		return json;
    }
	
}

