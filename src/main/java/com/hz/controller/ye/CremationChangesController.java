package com.hz.controller.ye;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Certificate;
import com.hz.entity.base.Farewell;
import com.hz.entity.base.FarewellType;
import com.hz.entity.base.FreePerson;
import com.hz.entity.base.Furnace;
import com.hz.entity.base.HardPerson;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.base.Mourning;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.MourningRecord;
import com.hz.service.base.AppellationService;
import com.hz.service.base.BillUnitService;
import com.hz.service.base.CarTypeService;
import com.hz.service.base.CertificateService;
import com.hz.service.base.CorpseAddressService;
import com.hz.service.base.CorpseUnitService;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.service.base.FarewellService;
import com.hz.service.base.FarewellTypeService;
import com.hz.service.base.FreePersonService;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.HardPersonService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.MourningService;
import com.hz.service.base.NationService;
import com.hz.service.base.ProveUnitService;
import com.hz.service.base.SpecialBusinessTypeService;
import com.hz.service.base.TransportTypeService;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.ye.BaseReductionDService;
import com.hz.service.ye.BaseReductionService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.HardReductionDService;
import com.hz.service.ye.HardReductionService;
import com.hz.service.ye.ItemOrderService;
import com.hz.service.ye.MourningRecordService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
/**
 * 火化单修改(收费之后)
 * @author jgj
 *
 */
@Controller
@RequestMapping("/cremationChanges.do")
public class CremationChangesController extends BaseController{
	
	private static Logger logger = LoggerFactory.getLogger(CremationChangesController.class); 
	@Autowired
	private CommissionOrderService commissionOrderService;
	//证件类型
	@Autowired
	private CertificateService certificateService;
	//死亡类型
	@Autowired
	private DeadTypeService deadTypeService;
	//死亡原因
	@Autowired
	private DeadReasonService deadReasonService;
	//证明单位
	@Autowired
	private ProveUnitService proveUnitService;
	//称谓关系
	@Autowired
	private AppellationService appellationService;
	//告别厅
	@Autowired
	private FarewellService farewellService;
	//告别厅类型
	@Autowired
	private FarewellTypeService farewellTypeService;
	//接尸单位
	@Autowired
	private CorpseUnitService corpseUnitService;
	//灵堂信息
	@Autowired
	private MourningService mourningService;
	//收费项目类别
	@Autowired
	private ItemTypeService itemTypeService;
	//收费项目
	@Autowired
	private ItemService itemService;
	//火化委托单服务项目
	@Autowired
	private ItemOrderService itemOrderService;
	//免费对象类别
	@Autowired
	private FreePersonService freePersonService;
	//重点救助对象类别
	@Autowired
	private HardPersonService hardPersonService;
	//告别厅使用记录
	@Autowired
	private FarewellRecordService farewellRecordService;
	//冷藏柜使用记录
	@Autowired
	private FreezerRecordService freezerRecordService;
	//守灵室使用记录
	@Autowired
	private MourningRecordService mourningRecordService;
	//基本减免申请
	@Autowired
	private BaseReductionService baseReductionService;
	//基本减免项目
	@Autowired
	private BaseReductionDService baseReductionDService;
	//困难减免
	@Autowired
	private HardReductionDService hardReductionDService;
	//困难减免
	@Autowired
	private HardReductionService hardReductionService;
	//火化炉信息
	@Autowired
	private FurnaceService furnaceService;
	//挂账单位
	@Autowired
	private BillUnitService billUnitService;
	//名族
	@Autowired
	private NationService nationService;
	//接尸地址
	@Autowired
	private CorpseAddressService corpseAddService;
	//特殊业务类型
	@Autowired
	private SpecialBusinessTypeService specialBusinessTypeService;
	//车辆类型
	@Autowired
	private CarTypeService carTypeService;
	//车辆调度
	@Autowired
	private CarSchedulRecordService carSchedulRecordService;
	//运输类型
	@Autowired
	private TransportTypeService transportTypeService;
	@Autowired
	UserNoticeService userNoticeService;
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "cremationChanges.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listCommissionOrder();
    }

	/**
	 * 火化委托单列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listCommissionOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("show","yes");//默认当成管理员身份显示所有的记录
		maps.put("checkFlag", getByte("checkFlag"));
		maps.put("goto", Const.GOTO1);
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
//		maps.put("payFlag", Const.Pay_Yes);
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_IceNumber});
		PageInfo<CommissionOrder> page=commissionOrderService.getCommissionOrderPageInfo(maps,pages[0],pages[1],"update_time desc,cremation_time desc");
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("checkFlagOption", Const.getCheckFlagOption(getByte("checkFlag"),true));
		model.put("page", page);
		model.put("searchOption", searchOption);
        model.put("method", "list");
		return new ModelAndView("ye/cremationChanges/listCremationChanges",model);
    }
    
    /**
     * 更改火化委托基本信息
     * 与冰柜、火化炉、服务项目等分开修改
     * @return
     */
    @RequestMapping(params ="method=editBase")
    public ModelAndView editBase(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	String id=getString("id");
    	CommissionOrder commissionOrder=new CommissionOrder();
    	if(id!=null && !"".equals(id)){
    		commissionOrder=commissionOrderService.getCommissionOrderById(id);
    	}
    	model.put("province", commissionOrder.getProvince());
		model.put("city", commissionOrder.getCity());
		model.put("area", commissionOrder.getArea());
		//死亡原因
		String deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
		//证件类型
		String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
		//死亡类型
		String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
		//性别
		String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
		//证明单位
		String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
		//死亡证明
		String dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
		//与死者关系
		String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
		//民族
		String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);
		//接尸地址
		String corpseAddOption=corpseAddService.getCorpseAddressOption(commissionOrder.getPickAddr(), false);
		model.put("certificateOption",certificateOption);
		model.put("corpseAddOption",corpseAddOption);
		model.put("deadTypeOption",deadTypeOption);
		model.put("deadReasonOption",deadReasonOption);
		model.put("sexOption",sexOption);
		model.put("nationOption",nationOption);
		model.put("proveUnitOption",proveUnitOption);
		model.put("dFlagOption",dFlagOption);
		model.put("fAppellationOption",fAppellationOption);
		model.put("checkFlag",commissionOrder.getCheckFlag());
		model.put("Check_Yes",Const.Check_Yes);
		
//    	certificateOption
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=format.format(commissionOrder.getCreatTime());
		model.put("time",time);
    	model.put("commissionOrder", commissionOrder);
    	model.put("method", "saveBase");
    	model.put("menu", getString("menu"));
		return new ModelAndView("ye/commissionOrder/editBase",model);
    }
    
	/**
	 * 链接到修改页面
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(params = "method=edit")
    public ModelAndView editCommissionOrder() throws ParseException{
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		double serTotal=0.0;
		double artTotal=0.0;
		double baseTotal=0.0;
		double hardTotal=0.0;
		//首页审核
		CommissionOrder commissionOrder=new CommissionOrder();
		if(id!=null&&!id.equals("")){
			commissionOrder=commissionOrderService.getCommissionOrderById(id);
			//所有已收费过的项目,且根据类型区分开
			Map<String,Object> itemMap=new HashMap<String,Object>();
			itemMap.put("itemType", Const.Type_Service);
			itemMap.put("commissionOrderId", id);
			itemMap.put("payFlag", Const.Pay_Yes);
			itemMap.put("sort", "payTime");
			List<ItemOrder> serviceList=itemOrderService.getItemOrderList(itemMap);
			itemMap.put("itemType", Const.Type_Articles);
			List<ItemOrder> articlesList=itemOrderService.getItemOrderList(itemMap);
			//若有新追加未收费的，也一起查询出来
			itemMap.put("payFlag", Const.Pay_No);
			List<ItemOrder> articlesListNew=itemOrderService.getItemOrderList(itemMap);
			itemMap.put("itemType", Const.Type_Service);
			List<ItemOrder> serviceListNew=itemOrderService.getItemOrderList(itemMap);
			
			
			List<Object[]> service_list=new ArrayList<Object[]>();
			List<Object[]> articles_list=new ArrayList<Object[]>();
//			Map<String, Object> maps=new HashMap<String, Object>();
			for (int i = 0; i < serviceListNew.size(); i++) {
				//服务项目
				ItemOrder itemOrder = serviceListNew.get(i);
				String itemId=itemOrder.getItemId();
				Item item=itemService.getItemById(itemId);
				Map<String, Object>smaps=new HashMap<String, Object>();
				smaps.put("typeId", serviceListNew.get(i).getItem().getTypeId());
				smaps.put("type", Const.Type_Service);
				smaps.put("isdel", Const.Isdel_No);
		    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
		    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
		    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
		    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
		    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
		    	int sonIndex=item.getIndexFlag();
		    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
			}
			for (int i = 0; i < articlesListNew.size(); i++) {
				//丧葬用品
				ItemOrder itemOrder = articlesListNew.get(i);
				String itemId=itemOrder.getItemId();
				Item item=itemService.getItemById(itemId);
				Map<String, Object> smaps=new HashMap<String, Object>();
				smaps.put("typeId", item.getTypeId());
				smaps.put("type", Const.Type_Articles);
				smaps.put("isdel", Const.Isdel_No);
				String itemOption=itemService.getItemOption(smaps,item.getId(),false);
				String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
				String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
				String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
				int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
				int sonIndex=item.getIndexFlag();
				articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
			}
			
			for(int i=0;i<serviceList.size();i++){
				serTotal+=serviceList.get(i).getTotal();
			}
			for(int i=0;i<articlesList.size();i++){
				artTotal+=articlesList.get(i).getTotal();
			}
			model.put("serviceList", serviceList);
			model.put("articlesList", articlesList);
			model.put("artTotal", artTotal);
			model.put("serTotal", serTotal);
			model.put("service_list", service_list);//新添加且未收费的，进入页面加载
			model.put("articles_list", articles_list);//新添加且未收费的，进入页面加载


			//收费项目
//			model.put("service_list", service_list);
//			model.put("articles_list", articles_list);
		
		}
//		String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
		String corpseUnitOption=corpseUnitService.getCorpseUnitOption(commissionOrder.getCorpseUnitId(),false);
		//挂账单位
		String billUnitOption=billUnitService.getBillUnitOption(commissionOrder.getBillUnitId(), true);
		//特殊业务类型
		String specialBusinessOption=specialBusinessTypeService.getSpecialBusinessTypeOption(commissionOrder.getFurnaceComment(), false);
		//基本减免
		List<FreePerson> freePersonList=freePersonService.getFreePersonList(null);
		List<HardPerson> hardPersonList=hardPersonService.getHardPersonList(null);
		Item baseItem=new Item();
		Map<String, Object>baseMaps=new HashMap<String, Object>();
		baseMaps.put("baseFlag", Const.Is_Yes);
		baseMaps.put("isdel", Const.Isdel_No);
		Map<String, Object>baseMaps2=new HashMap<String, Object>();
		baseMaps2.put("isdel", Const.Isdel_No);
		String baseItemOption=itemService.getItemOption(baseMaps2, "", false);
		String baseItemHelpCodeOption=itemService.getItemHelpCodeOption(baseMaps2, "", false);
		List<Item> baselist=itemService.getItemList(baseMaps);
		if (baselist.size()>0) {
			baseItem=baselist.get(0);
		}
		
		model.put("freePersonList", freePersonList);
		model.put("hardPersonList", hardPersonList);
		model.put("baseItemHelpCodeOption", baseItemHelpCodeOption);
		model.put("baseItemOption", baseItemOption);
		model.put("baseItem", baseItem);
		
		
		//困难减免
		List<Object[]> hlist=new ArrayList<Object[]>();
		List<Certificate> certificateList=certificateService.getCertificateList();
		Item hardItem=new Item();
		//此处应客户需要更改不再限制困难减免的项目
		Map<String, Object>hardMaps=new HashMap<String, Object>();
		hardMaps.put("hardFlag", Const.Is_Yes);
		hardMaps.put("isdel", Const.Isdel_No);
		Map<String, Object>hardmap=new HashMap<String, Object>();
		hardmap.put("isdel", Const.Isdel_No);
//		String hardItemOption=itemService.getItemOption(hardMaps, "", false);
		String hardItemOption=itemService.getItemOption(null, "", false);
		String hardItemHelpCodeOption=itemService.getItemHelpCodeOption(null, "", false);
		List<Item> hardlist=itemService.getItemList(hardMaps);
		if (hardlist.size()>0) {
			 hardItem=hardlist.get(0);
		}
		model.put("certificateList", certificateList);
		model.put("hardItemOption", hardItemOption);
		model.put("hardItemHelpCodeOption", hardItemHelpCodeOption);
		model.put("hardItem", hardItem);
		for (int i = 0; i < hardlist.size(); i++) {
			Item item=(Item) hardlist.get(i);
			String hard_itemOption=itemService.getItemOption(hardmap, item.getId(), false);
			String hard_itemHelpCodeOption=itemService.getItemHelpCodeOption(hardmap, item.getId(), false);
			int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
			hlist.add(new Object[]{"hard","",hard_itemOption,hard_itemHelpCodeOption,1,item.getPice(),"",faIndex,sonIndex});
		}
		model.put("hardlist", hlist);
		
		
		//减免项目
		byte base_Is=Const.Is_No;
		byte hard_Is=Const.Is_No;
		byte special_Is=Const.Is_No;
		String checked="checked=checked";
		BaseReduction baseReduction=new BaseReduction();
		HardReduction hardReduction=new HardReduction();
		List<Object[]> base_list=new ArrayList<Object[]>();//基本减免项目
		List<Object[]> hard_list=new ArrayList<Object[]>();//困难减免项目
		List<Object[]> prove_list=new ArrayList<Object[]>();//证件类型
		List<Object[]> freePerson_list=new ArrayList<Object[]>();//免费对象类别
		List<Object[]> hardPerson_list=new ArrayList<Object[]>();//重点救助对象类别
		String hard_proveUnitOption=proveUnitService.getProveUnitOption("",false);//困难减免证明单位
		String hard_appellationOption=appellationService.getAppellationOption("",false);//困难减免称谓
		boolean isFree=false;//判断是否有免费对象类型,初始没有,则在前端默认居民选中.
		
		if(id!=null&&!id.equals("")){
			//已收费的基本减免
			Map<String,Object> baseBeforeMap=new HashMap<String,Object>();
			baseBeforeMap.put("commissionOrderId", id);
			baseBeforeMap.put("payTime", "notNull");
			List<BaseReductionD> baseBeforeList=baseReductionDService.getBaseReductionDFList(baseBeforeMap);
			for(int i=0;i<baseBeforeList.size();i++){
				baseTotal+=baseBeforeList.get(i).getTotal();
			}
			//已收费的困难减免
			Map<String,Object> hardBeforeMap=new HashMap<String,Object>();
			hardBeforeMap.put("commissionOrderId", id);
			hardBeforeMap.put("payTime", "notNull");
			List<HardReductionD> hardBeforeList=hardReductionDService.getHardReductionDFeeList(hardBeforeMap);
			for(int i=0;i<hardBeforeList.size();i++){
				hardTotal+=hardBeforeList.get(i).getTotal();
			}
			model.put("baseBeforeList", baseBeforeList);
			model.put("hardBeforeList", hardBeforeList);
			model.put("baseTotal", baseTotal);
			model.put("hardTotal", hardTotal);
			//基本减免
			Map<String, Object>maps=new HashMap<String, Object>();
			Map<String, Object>bMaps=new HashMap<String, Object>();
			List<BaseReductionD> baseList=new ArrayList<BaseReductionD>();
			maps.put("commissionOrderId", id);
			List<BaseReduction> bList=baseReductionService.getBaseReductionList(maps);
			if (bList.size()>0) {
				base_Is=Const.Is_Yes;
				baseReduction=(BaseReduction) bList.get(0);
				bMaps.put("baseReductionId", baseReduction.getId());
				bMaps.put("payTime", 1);
				baseList=baseReductionDService.getBaseReductionDList(bMaps);
			}
			for (int i = 0; i < baseList.size(); i++) {
				BaseReductionD baseReductionD=(BaseReductionD) baseList.get(i);
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("isdel", Const.Isdel_No);
				String baseItem_option=itemService.getItemOption(bMaps1, baseReductionD.getItemId(), false);
				String baseItemHelpCode_option=itemService.getItemHelpCodeOption(bMaps1, baseReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(baseReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(baseReductionD.getItemId()).getIndexFlag();
				base_list.add(new Object[]{"base",baseReductionD.getId(),baseItem_option,baseItemHelpCode_option,baseReductionD.getNumber(),baseReductionD.getTotal(),baseReductionD.getComment(),faIndex,sonIndex,baseReductionD.getPice()});
			}
			
			//免费对象类别
			List<FreePerson> freePersonList2=freePersonService.getFreePersonList(null);
			String[] free=null;
			if (baseReduction.getFreePersonIds()!=null&&!baseReduction.getFreePersonIds().equals("")) {
				free=baseReduction.getFreePersonIds().split(",");
			}
			
			for (int i = 0; i < freePersonList2.size(); i++) {
				boolean bool=true;
				FreePerson freePerson=(FreePerson) freePersonList2.get(i);
				if (free!=null) {
					for (int j = 0; j < free.length; j++) {
						if (free[j]==freePerson.getId()||free[j].equals(freePerson.getId())) {
							freePerson_list.add(new Object[]{"freePersonId",checked,freePerson.getId(),freePerson.getName()});
							bool=false;
							isFree=true;
						}
					}
				}
				if (bool) {
					freePerson_list.add(new Object[]{"freePersonId","",freePerson.getId(),freePerson.getName()});
				}
			}
			
			//重点救助对象类别
			List<HardPerson> hardPersonList2=hardPersonService.getHardPersonList(null);
			String[] hard=null;
			if (baseReduction.getHardPersonIds()!=null&&!baseReduction.getHardPersonIds().equals("")) {
				hard=baseReduction.getHardPersonIds().split(",");
			}
			for (int i = 0; i < hardPersonList2.size(); i++) {
				boolean bool=true;
				HardPerson hardPerson=(HardPerson) hardPersonList2.get(i);
				if (hard!=null) {
					for (int j = 0; j < hard.length; j++) {
						if (hard[j]==hardPerson.getId()||hard[j].equals(hardPerson.getId())) {
							hardPerson_list.add(new Object[]{"hardPersonId",checked,hardPerson.getId(),hardPerson.getName()});
							bool=false;
						}
					}
				}
				if (bool) {
					hardPerson_list.add(new Object[]{"hardPersonId","",hardPerson.getId(),hardPerson.getName()});
				}
			}
			//困难减免
			List<HardReductionD> hardList=new ArrayList<HardReductionD>();
			Map<String, Object>hMaps=new HashMap<String, Object>();
			hMaps.put("commissionOrderId", id);
			List<HardReduction> hList=hardReductionService.getHardReductionList(hMaps);
			if (hList.size()>0) {
				hard_Is=Const.Is_Yes;
				hardReduction= hList.get(0);
				hMaps.put("hardReductionId", hardReduction.getId());
				hMaps.put("payTime", 1);
				hardList=hardReductionDService.getHardReductionDList(hMaps);
				special_Is=hardReduction.getSpecial();
			}
			for (int i = 0; i < hardList.size(); i++) {
				HardReductionD hardReductionD=(HardReductionD) hardList.get(i);
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("isdel", Const.Isdel_No);
				String hardItem_option=itemService.getItemOption(bMaps1, hardReductionD.getItemId(), false);
				String hardItemHelpCode_option=itemService.getItemHelpCodeOption(bMaps1, hardReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(hardReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(hardReductionD.getItemId()).getIndexFlag();
				hard_list.add(new Object[]{"hard",hardReductionD.getId(),hardItem_option,hardItemHelpCode_option,hardReductionD.getNumber(),hardReductionD.getTotal(),hardReductionD.getComment(),faIndex,sonIndex,hardReductionD.getPice()});
			}
			//证件类型
			String[] prove=null;
			if (hardReduction.getProveIds()!=null&&!hardReduction.getProveIds().equals("")) {
				
				prove=hardReduction.getProveIds().split(",");
			}
			for (int i = 0; i < certificateList.size(); i++) {
				boolean bool=true;
				Certificate certificate=(Certificate) certificateList.get(i);
				if (prove!=null) {
					for (int j = 0; j < prove.length; j++) {
						if (prove[j]==certificate.getCertificateId()||prove[j].equals(certificate.getCertificateId())) {
							prove_list.add(new Object[]{"proveIds",checked,certificate.getCertificateId(),certificate.getName()});
							bool=false;
						}
					}
				}
				if (bool) {
					prove_list.add(new Object[]{"proveIds","",certificate.getCertificateId(),certificate.getName()});
				}
			}
			hard_proveUnitOption=proveUnitService.getProveUnitOption(hardReduction.getProveComment(),false);//困难减免证明单位
			hard_appellationOption=appellationService.getAppellationOption(hardReduction.getAppellationId(),false);//困难减免称谓
			
			
			
		}
		model.put("isFree",isFree);
		model.put("baseReduction", baseReduction);
		model.put("hard_proveUnitOption", hard_proveUnitOption);
		model.put("hard_appellationOption", hard_appellationOption);
		model.put("hardReduction", hardReduction);
		model.put("hard_list", hard_list);
		model.put("base_list", base_list);
		model.put("prove_list", prove_list);
		model.put("freePerson_list", freePerson_list);
		model.put("hardPerson_list", hardPerson_list);
		model.put("base_Is", base_Is);
		model.put("hard_Is", hard_Is);
		model.put("furnace_ty", Const.furnace_ty);
		model.put("baseIsOption", Const.getIsOption(base_Is, false));
		model.put("hardIsOption", Const.getIsOption(hard_Is, false));
		model.put("specialOption", Const.getIsOption(special_Is, false));
		
		
		//车辆调度
		String carTypeOption=carTypeService.getCarTypeOption("", false);
		String transportTypeOption=transportTypeService.getTransportTypeOption("", false);
		model.put("carTypeOption", carTypeOption);
		model.put("transportTypeOption", transportTypeOption);
		if(id!=null&&!id.equals("")){
			Map<String,Object> carMaps=new HashMap<String, Object>();
			carMaps.put("commissionOrderId", id);
			List<CarSchedulRecord> carSchedulRecordList=new ArrayList<CarSchedulRecord>();
			List<Object[]> car_List=new ArrayList<Object[]>();
			carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
			for (int i = 0; i < carSchedulRecordList.size(); i++) {
				CarSchedulRecord carSchedulRecord=carSchedulRecordList.get(i);
				String carType_option=carTypeService.getCarTypeOption(carSchedulRecord.getCarTypeId(), false);
				String transportType_option=transportTypeService.getTransportTypeOption(carSchedulRecord.getTransportTypeId(), false);
				Timestamp pickTi=carSchedulRecord.getPickTime();
				String pickTime = "";
				if (pickTi != null) {
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					pickTime=sdf.format(pickTi);
				}
				car_List.add(new Object[]{carSchedulRecord.getId(),transportType_option,carType_option,pickTime,carSchedulRecord.getComment()});
			}
			model.put("car_List", car_List);
		}
		//火化炉
		List<Object[]> furnaceList=Const.getFurnaceList();
		model.put("furnaceList", furnaceList);
		//普通炉添加收费项目
		
		
		
		if (id != null && !id.equals("")) {
			//告别厅使用记录
			Map<String, Object>farewellMaps=new HashMap<String, Object>();
			farewellMaps.put("commissionOrderId", id);
			List<FarewellRecord>farewellRecordList=new  ArrayList<FarewellRecord>();
			FarewellRecord farewellRecord=new FarewellRecord();
			farewellRecordList=farewellRecordService.getFarewellRecordList(farewellMaps ,"id asc");
			if (farewellRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					farewellRecord=(FarewellRecord) farewellRecordList.get(0);
				}
			}
			model.put("farewellRecord", farewellRecord);
			//冷藏柜使用记录
			Map<String, Object>freezerMaps=new HashMap<String, Object>();
			freezerMaps.put("commissionOrderId", id);
			List<FreezerRecord> freezerRecordList=new ArrayList<FreezerRecord>();
			FreezerRecord freezerRecord=new FreezerRecord();
			freezerRecordList=freezerRecordService.getFreezerRecordList(freezerMaps);
			if (freezerRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					freezerRecord=(FreezerRecord) freezerRecordList.get(0);
				}
			}
			model.put("freezerRecord", freezerRecord);
			//守灵室使用记录
			Map<String, Object>mourningMaps=new HashMap<String, Object>();
			mourningMaps.put("commissionOrderId", id);
			List<MourningRecord> mourningRecordList=new  ArrayList<MourningRecord>();
			MourningRecord mourningRecord=new MourningRecord();
			mourningRecordList=mourningRecordService.getMourningRecordList(mourningMaps,"id asc");
			if (mourningRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					mourningRecord=(MourningRecord) mourningRecordList.get(0);
				}
			}
			model.put("mourningRecord", mourningRecord);
			//火化记录
			FurnaceRecord furnaceRecord = new FurnaceRecord();
			Map<String, Object>furnaceRecordMaps=new HashMap<String, Object>();
			furnaceRecordMaps.put("commissionOrderId", id);
			furnaceRecord = furnaceService.getFurnaceRecord(furnaceRecordMaps);
			model.put("furnaceRecord", furnaceRecord);
			List<Object[]> furnace_list = new ArrayList<Object[]>();
			for (int i = 0; i < furnaceList.size(); i++) {
				Object[] ob = furnaceList.get(i);
				byte by = (Byte) ob[0];
				if (by == commissionOrder.getFurnaceFlag()) {
					furnace_list.add(new Object[] { ob[0], ob[1], checked });
				} else {
					furnace_list.add(new Object[] { ob[0], ob[1], "" });
				}
			}
			model.put("furnace_list", furnace_list);
		} 
		byte checkFlag=commissionOrder.getCheckFlag();
		model.put("checkFlag", checkFlag);
		//model.put("registerOption", registerOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("commissionOrder", commissionOrder);
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("id", id);
		model.put("billUnitOption", billUnitOption);
		model.put("specialBusinessOption", specialBusinessOption);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("isOption", Const.getIsOption((byte)0, false));
		model.put("agentUser", getCurrentUser().getName());
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time;
		if(id!=null && !"".equals(id)){
			time=format.format(commissionOrder.getCreatTime());
		}else{
			time=format.format(date);
		}
		Timestamp now=new Timestamp(System.currentTimeMillis());
		model.put("now", format.format(now));
		model.put("time",time);
        model.put("method", "save");
		return new ModelAndView("ye/cremationChanges/editCremationChanges",model);
    }
	/**
	 * 修改基本信息
	 * @return
	 */
	@RequestMapping(params = "method=saveBase")
    @SystemControllerLog(description = "保存修改之后的火化委托单基本信息")
    @ResponseBody
	public JSONObject saveBase(){
		JSONObject json=new JSONObject();
		try{
			String id=getString("id");
			CommissionOrder commissionOrder=new CommissionOrder();
			if(id!=null &&!"".equals(id)){
				commissionOrder=commissionOrderService.getCommissionOrderById(id);
			}
			
    		String cardCode=getString("cardCode");
    		if (id == null || id.equals("")) {
				int num = commissionOrderService.getCommissionOrderByCard(cardCode);
				if (num >= 1) {
					setJsonByFail(json, "该卡号已经存在");
					return json;
				}
			}
    		String certificateCode=getString("certificateCode");
    		String certificateId=getString("certificateId");
    		String dAddr=getString("dAddr");
    		String deadReasonId=getString("deadReasonId");
    		String deadTypeId=getString("deadTypeId");
    		String province=getString("province");
    		String city=getString("city");
    		String area=getString("area");
    		byte dFlag=getByte("dFlag");
    		
    		//死者身份证图片路径
    		String dIdcardPic =getString("filename");
    		//经办人身份证图片路径
    		String eIdcardPic=getString("filenameF");
    		//拍照存档图片路径
    		String photoPic=getString("filenameP");
    		
    		String dNation=getString("dNation");
    		if(dNation!=null && !"".equals(dNation)){
       			String nationId = nationService.getdnationId(dNation);
       			if(nationId != null){
       				dNation = nationId;
       			}
       		}
    		Timestamp dTime=getTimeM("dTime");
    		String fAddr=getString("fAddr");
    		String fAppellationId=getString("fAppellationId");
    		String fName=getString("fName");
    		String fPhone=getString("fPhone");
    		String fUnit=getString("fUnit");
    		String fCardCode=getString("fCardCode");
    		String name=getString("name");
    		String pickAddr=getString("pickAddr");
    		String proveUnitId=getString("proveUnitId");
    		String proveUnitContent=getString("proveUnitContent");
    		byte sex=getByte("sex");
    		int age=getInt("age");
    		
    		
    		if (!cardCode.equals(commissionOrder.getCardCode()) && !"".equals(cardCode)) {
				int num = commissionOrderService.getCommissionOrderByCard(cardCode);
				if (num >= 1) {
					setJsonByFail(json, "该卡号已经存在");
					return json;
				}
			}
    		
    		commissionOrder.setAge(age);
    		commissionOrder.setSex(sex);
    		commissionOrder.setName(name);
    		commissionOrder.setCardCode(cardCode);
    		commissionOrder.setCertificateId(certificateId);
    		commissionOrder.setCertificateCode(certificateCode);
    		commissionOrder.setdAddr(dAddr);
    		commissionOrder.setPickAddr(pickAddr);
    		commissionOrder.setDeadTypeId(deadTypeId);
    		commissionOrder.setDeadReasonId(deadReasonId);
    		commissionOrder.setdTime(dTime);
    		commissionOrder.setdNationId(dNation);
    		commissionOrder.setProvince(province);
    		commissionOrder.setCity(city);
    		commissionOrder.setArea(area);
    		commissionOrder.setdFlag(dFlag);
    		commissionOrder.setdIdcardPic(dIdcardPic);
    		commissionOrder.seteIdcardPic(eIdcardPic);
    		commissionOrder.setPhotoPic(photoPic);
    		commissionOrder.setfName(fName);
    		commissionOrder.setfPhone(fPhone);
    		commissionOrder.setfAppellationId(fAppellationId);
    		commissionOrder.setfAddr(fAddr);
    		commissionOrder.setfUnit(fUnit);
    		commissionOrder.setfCardCode(fCardCode);
    		commissionOrder.setProveUnitId(proveUnitId);
    		commissionOrder.setProveUnitContent(proveUnitContent);
    		
    		commissionOrderService.updateCommissionOrder(commissionOrder);
    		String menu=getString("menu");
    		if("check".equals(menu)){
    			setJsonBySuccess(json, "保存成功", "commissionOrderCheck.do?method=list");
    		}else if("com".equals(menu)){
    			setJsonBySuccess(json, "保存成功", "commissionOrder.do?method=list");
    		}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(DataTools.getStackTrace(e));
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
    /**
     * 保存火化委托单
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存火化修正信息")
    @ResponseBody
    public JSONObject saveCommissionOrder(){
		JSONObject json = new JSONObject();
    	try {
    		String userId=getCurrentUser().getUserId();
    		String userName=getCurrentUser().getName();
    		String id =getString("id");
    		Timestamp arriveTime=getTimeM("arriveTime");
//    		String cComment=getString("comment");
    		String corpseUnitId=getString("corpseUnitId");
    		Timestamp orderTime=getTimeM("orderTime");
    		
    		//保存前先判断一下是否有收费的项目，如果没有不保存，提示在火化委托单里修改
    		Map<String,Object> maps=new HashMap<String,Object>();
    		maps.put("commissionOrderId", id);
    		maps.put("payFlag", Const.Pay_Yes);
    		List<ItemOrder> itList=itemOrderService.getItemOrderList(maps);
    		if(itList.size()<=0){
    			setJsonByFail(json, "保存失败，该用户没有已收费的项目，请在火化委托单更改！");
    		}else{
    			String furnaceTypeId=getString("furnaceTypeId");
    			String furnaceId=getString("furnaceId");
//    		String pickAddr=getString("pickAddr");
//    		String registerId=getString("registerId");
//    		byte scheduleFlag=getByte("scheduleFlag");
    			String specialBusinessValue=getString("specialBusinessValue"); //火化炉选择无时的备注
    			byte radio=getByte("radio");
    			//挂账单位
    			String billUnitId=getString("billUnitId");
    			
    			
    			//收费项目
    			String[] service=getArrayString("service_id");//服务项目
    			String[] articles=getArrayString("articles_id");//丧葬用品
    			String[] itemId=getArrayString("itemId");
    			String[] pice=getArrayString("pice");
    			String[] number=getArrayString("number");
    			String[] bill=getArrayString("bill");
    			String[] total=getArrayString("total");
    			String[] comment=getArrayString("comment");
    			
    			
    			//告别厅
    			Timestamp farewellBeginDate=getTimeM("farewellBeginDate");
//			Timestamp farewellEndDate=getTimeM("farewellEndDate");
    			String farewellId=getString("farewellId");
    			//守灵室
    			Timestamp mourningBeginTime=getTimeM("mourningBeginTime");
    			Timestamp mourningEndTime=getTimeM("mourningEndTime");
    			String mourningId=getString("mourningId");
    			
    			
    			//车辆调度
    			String[] transportType=getArrayString("transportTypeId");
    			String[] carType=getArrayString("carTypeId");
    			//此处因为 时间从原来的精确秒,统一改成到分钟,还用之前的方法会报错
//    		Timestamp[] pickTime=getArrayTimestamp("pickTime");
    			String[] pickpick=getArrayString("pickTime");
    			Timestamp[] pickTime=null;
    			if(pickpick!=null && pickpick.length>0){
    				pickTime=DateTools.stringToTimeArray(pickpick);
    			}
    			String[] carComment=getArrayString("carComment");
    			String[] carSchedulRecordId=getArrayString("csr_id");
    			
    			
    			
    			//基本减免
    			String[] itemIdR=getArrayString("reduction_itemId");
    			String[] numberR=getArrayString("reduction_number");
    			String[] piceR=getArrayString("reduction_pice");
    			String[] totalR=getArrayString("reduction_total");
    			String[] commentR=getArrayString("reduction_comment");
    			String[] base=getArrayString("base_id");//基本减免项目
    			String baseReductionId=getString("baseReductionId");//基本减免Id
    			String[] freePersonId=getArrayString("freePersonId");//免费对象类别
    			String[] hardPersonId=getArrayString("hardPersonId");//重点救助对象类别
    			byte baseIs=getByte("baseIs");
    			String fidcard=getString("fidcard");
    			//预约登记 id
    			
    			//困难减免
    			String[] hard=getArrayString("hard_id");//困难减免项目
    			String[] proveIds=getArrayString("proveIds");
    			String hardReductionId=getString("hardReductionId");
    			byte hardIs=getByte("hardIs");
    			String hard_proveUnitId=getString("hard_proveUnitId");
    			String reason=getString("reason");
    			String hComment=getString("comment");
    			Timestamp createTime=getTimeM("createTime");
    			String appellationId=getString("appellationId");
    			String applicant=getString("applicant");
    			String phone=getString("phone");
    			byte special=getByte("special");
    			//以下新增,用于记录困难减免记录
    			String[] hardIdR=getArrayString("hard_itemId");
    			String[] hardnumberR=getArrayString("hard_number");
    			String[] hardpiceR=getArrayString("hard_pice");
    			String[] hardtotalR=getArrayString("hard_total");
    			String[] hardcommentR=getArrayString("hard_comment");
    			
    			
    			commissionOrderService.saveCremationChanges(userId,userName,id,arriveTime,corpseUnitId,
    					billUnitId,mourningId,mourningBeginTime,mourningEndTime,farewellId,
    					farewellBeginDate,radio,furnaceTypeId,orderTime,furnaceId,specialBusinessValue,
    					service,articles,itemId,pice,number,bill,total,comment,
    					transportType, carType, pickTime, carComment, carSchedulRecordId,
    					itemIdR, numberR, totalR
    					, commentR, base, baseReductionId, freePersonId
    					, hardPersonId, baseIs, fidcard, hard, proveIds
    					, hardReductionId, hardIs, hard_proveUnitId, reason
    					, hComment, createTime, appellationId, applicant
    					, phone, special, hardIdR,hardnumberR,hardpiceR,hardtotalR,hardcommentR,piceR);
    			
    			setJsonBySuccess(json, "保存成功", "cremationChanges.do?method=list");
    			if(id!=null && !"".equals(id)){
    				json.put("coId", id);
    			}
    		}
    		
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(DataTools.getStackTrace(e));
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
			
		}
		return json;
    }
    
    
    /**
     * 销卡(author：hw)
     * @return
     */
    
    /**
     * 死亡类型改变
     * @return
     */
    @RequestMapping(params = "method=deadTypeChange")
    @ResponseBody
    public JSONObject deadTypeChange(){
    	JSONObject json = new JSONObject();
    	String typeId=getString("id");
    	String reasonId=getString("reasonId");
    	String str=deadReasonService.getDeadReasonChange(typeId,reasonId);
    	int indexFlag=deadTypeService.getDeadTypeId(typeId).getIndexFlag();
    	if(indexFlag==2){
    		json.put("deadFlag", 2);
    	}else{
    		json.put("deadFlag", 1);
    	}
    	json.put("str", str);
		return json;
    }
    
    /**
     * 项目类别改变
     * @return
     */
    @RequestMapping(params = "method=itemTypeChange")
    @ResponseBody
    public JSONObject itemTypeChange(){
		JSONObject json = new JSONObject();
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId", getString("id"));
		maps.put("isdel", Const.Isdel_No);
		List<Item> list=itemService.getItemList(maps);
		Item item=new Item();
		if (list.size()>0) {
			item=list.get(0);
		}
    	String str=itemService.getItemOption(maps, "", false);
    	String helpOption=itemService.getItemHelpCodeOption(maps,"",false);
    	
		json.put("pice", item.getPice());
		json.put("helpOption", helpOption);
		json.put("str", str);
		json.put("faIndex",itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag());
		json.put("sonIndex",item.getIndexFlag());
		return json;
    }
    
    
    /**
     * 项目名称改变
     * @return
     */
    @RequestMapping(params = "method=itemNameChange")
    @ResponseBody
    public JSONObject itemNameChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId",item.getTypeId());
		maps.put("isdel", Const.Isdel_No);
    	String str =itemService.getItemHelpCodeOption(maps, item.getId(), false);
//    	String typeOption=itemTypeService.getItemTypeOption(maps,item.getTypeId(), false);
		json.put("pice", item.getPice());
		json.put("str", str);
		json.put("sonIndex",item.getIndexFlag());
		return json;
    }
    
    /**
     * 助记码改变
     * @return
     */
    @RequestMapping(params = "method=itemHelpCodeChange")
    @ResponseBody
    public JSONObject itemHelpCodeChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId",item.getTypeId());
    	String str =itemService.getItemOption(maps, item.getId(), false);
    	json.put("pice", item.getPice());
		json.put("str", str);
		json.put("sonIndex",item.getIndexFlag());
		return json;
    }
    
    /**
	 * 链接到修改添加服务项目
	 * @return
	 */
    @RequestMapping(params = "method=selectItem")
    public ModelAndView editItemOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=itemTypeList.get(i);
			maps.put("typeId",itemType.getId());
			String itemOption=itemService.getItemOption(maps, "",true);
			list.add(new Object[]{itemType.getName(),itemOption});
		}
        model.put("list", list);
        model.put("type", getByte("type"));
        model.put("id", getString("id"));
        model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		return new ModelAndView("ye/cremationChanges/selectItem",model);
    }
    
    
    /**
	 * 链接到修改添加服务项目
	 * @return
	 */
    @RequestMapping(params = "method=selectItems")
    public ModelAndView editItemOrders(){
		Map<String,Object> model=new HashMap<String,Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			maps.put("typeId",itemType.getId());
			maps.put("isdel", Const.Isdel_No);
			String itemOption=itemService.getItemOption(maps, "",true);
			list.add(new Object[]{itemType.getName(),itemOption});
		}
		//添加服务项目
		List<Object[]> itemList=new ArrayList<Object[]>();
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			Map<String, Object>itemMaps=new HashMap<String, Object>();
			itemMaps.put("typeId", itemType.getId());
			itemMaps.put("isdel", Const.Isdel_No);
			List<Item> itemList1=itemService.getItemList(itemMaps,"index_flag asc");
			itemList.add(new Object[]{itemType,itemList1});
		}
		model.put("itemTree", getTree(itemList));
        model.put("list", list);
        model.put("type", getByte("type"));
        model.put("id", getString("id"));
        model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		return new ModelAndView("ye/cremationChanges/selectItems",model);
    }
    /**
     * 添加服务项目
     * @return
     */
    @RequestMapping(params = "method=addItems")
    @ResponseBody
    public List<Object[]> saveItemOrder(){
		List<Object[]> list = new ArrayList<Object[]>();
		List<String> itemIist = new ArrayList<String>();
		String id =getString("ids");
		String[] ids = id.split(",");
		for(int i=0;i<ids.length;i++){
			if (!ids[i].equals("")) {
				itemIist.add(ids[i]);
			}
		}
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		Map<String, Object>maps1=new HashMap<String, Object>();
	    for(int i=0;i<itemIist.size();i++){
	    	Item item=itemService.getItemById(itemIist.get(i).toString());
	    	/*if("508".equals(item.hashCode())){
	    		
	    	}else{
	    		
	    	}*/
	    	maps1.put("typeId", item.getTypeId());
	    	maps1.put("isdel", Const.Isdel_No);
	    	String itemOption=itemService.getItemOption(maps1,itemIist.get(i),false);
	    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps1,item.getId(),false);
	    	String itemTypeOption=itemTypeService.getItemTypeOption(maps,item.getTypeId(), false);
	    	String isOption=Const.getIsOption((byte)0, false);
	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
	    	list.add(new Object[]{itemOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),itemHelpCodeOption,faIndex,sonIndex});
	    }
		return list;
    }
    /**
     * 添加服务项目
     * @return
     */
    @RequestMapping(params = "method=addItem")
    @ResponseBody
    public String addItem(){
//    	int[] pages = getPage();
		Map<String, Object>maps=new HashMap<String, Object>();
		String id =getString("ids");
		String[] ids =  id.split(",");//Id数组
		String ind =getString("ind");
		String[] checkId =  ind.split(",");//复选框Id数组
		List<String> idsList=new ArrayList<String>();
		int[] index=new int[ids.length];
		String[] inds=new String[ids.length];
		int a=0;
		for (int i = 0; i < ids.length; i++) {
			idsList.add(ids[i]);
			if (!ids[i].equals("")&&ids[i]!="") {
				index[a]=i;
				inds[a]=checkId[i];
				a++;
			}
		}
		maps.put("ids", idsList);
		String str="";
		List<Item> itemList =itemService.getItemList(maps,"b.index_flag asc,a.index_flag asc");
		str+="{";
		int x=0;
		for (int i = 0; i < itemList.size(); i++) {
			if (i>0) {
				str+=",";
			}
			Item item=(Item)itemList.get(i);
			x++;
			str+=""+'"'+x+"."+'"'+":{"+'"'+"id"+'"'+":"+'"'+item.getId()+'"'+","
			+'"'+"indexFlag"+'"'+":"+'"'+x+'"'+","
			+'"'+"name"+'"'+":"+'"'+item.getName()+'"'+","
			+'"'+"index"+'"'+":"+'"'+index[i]+'"'+","
			+'"'+"inds"+'"'+":"+'"'+inds[i]+'"'+"}";
		}
		str+="}";
		return str;
    }
    /**
     * 获得tree结构
     * @param list
     * @return
     */
	public String getTree(List<Object[]> list){
    	int sum=0;
    	String content="";
    	for (int i = 0; i < list.size(); i++) {
    		if (i>0) {
    			content+=",";
			}
    		Object[] objects=list.get(i);
    		ItemType itemType=(ItemType)objects[0];
	    	content+= "{";
	    	content+="text:'"+itemType.getName()+"'";
//	    	content+=",tags:['"+itemType.getId()+"']";
	    	content+=",indexFlag:'"+itemType.getIndexFlag()+"'";
	    	content+=",selectable: false";
	    	List<?> itemList=(List<?>)objects[1];
	    	content+=",nodes: [";
	    	for (int j = 0; j < itemList.size(); j++) {
	    		if (j>0) {
	    			content+=",";
				}
	    		Item item=(Item)itemList.get(j);
	    		content+="{";
	    		content+="text:'"+item.getName()+"'";
	    		content+=",indexFlag:'"+item.getIndexFlag()+"'";
	    		content+=",tags:['"+item.getId()+"_"+sum+"']";
	    		sum++;
	    		content+="}";
	    	}
	    	content+="]";
	    	content+="}";
    	}
    	return content;
    }
    
    
    
    /**
     * 灵堂调度页面
     * @return
     */
    @RequestMapping(params = "method=editMourning")
    public ModelAndView editMourning(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int time=getInt("time");
		Date date = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("current_time")!=null){
			date = getDate("current_time");
		}
		if (time!=0) {
			date=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), time, Calendar.DATE);
		}
		Map<String, Object> maps=new HashMap<String, Object>();
		List<Mourning> mList = mourningService.getMourningList(null,"");
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("endDate", DateTools.getDayEndTime(DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), 6, Calendar.DATE)));
		List<MourningRecord> rList = mourningRecordService.getMourningRecordList(maps,"begin_time");
		//获取搜索日期的之后7天的所有日期List
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[8];
		dOb[0]="灵堂";
		//设置第一行（日期）
		for(int i=0;i<7;i++){
			dOb[i+1]=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), i, Calendar.DATE);
		}
		list.add(dOb);
		//设置默认列表数组
		for(int i=0;i<mList.size();i++){
			Mourning mouring = (Mourning)mList.get(i);
			list.add(new Object[]{mouring,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历所有记录，放在相应的位置
		for(int i=0;i<rList.size();i++){
			MourningRecord mr = (MourningRecord)rList.get(i);
			int row = 0;//行
			int col = 0;//列
			int endCol=0;//最后一列
			//对比灵堂，确认行数
			for(int j=1;j<list.size();j++){
				//把数组放在list中
				Object[] ob = list.get(j);
				Mourning m = (Mourning)ob[0];
				if(m.getId().equals(mr.getMourningId())){
					row = j;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++){
				Date da = (Date)dOb[d];
				if(isSomeDay(da, new Date(mr.getBeginTime().getTime()))){
					col = (d-1)*2+isPmOrAm(mr.getBeginTime());
				}
				if(isSomeDay(da, new Date(mr.getEndTime().getTime()))){
					endCol = (d-1)*2+isPmOrAm(mr.getEndTime());
				}
			}
			if(row!=0){
				Object[] o = (Object[])list.get(row);
				if(endCol==0){
					endCol = 14;
				}
				if(col==0){
					col=1;
				}
				for(int x=col;x<=endCol;x++){
					o[x] = mr;
				}
			}
			
		}
		model.put("date", date);
		model.put("list", list);
        model.put("method", "editMourning");
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
        model.put("IsFlag_Bzwc",Const.IsFlag_Bzwc);
        model.put("IsFlag_YY", Const.IsFlag_YY);
        model.put("IsFlag_QC", Const.IsFlag_QC);
    	return new ModelAndView("ye/cremationChanges/editMourning",model);
    }
    
    /**
     * 选中返回时间
     * @return
     */
    @RequestMapping(params = "method=selectedTime")
    @ResponseBody
    public JSONObject selectedTime(){
    	JSONObject json = new JSONObject();
    	Date date = getDate("tDate");
    	int col=getInt("col");//列
    	Object[] dOb = new Object[15];
		//从当前时间开始的后七天
    	int r=1;
		for(int i=0;i<7;i++){
			dOb[r]=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), i, Calendar.DATE);
			r++;
			dOb[r]=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), i, Calendar.DATE);
			r++;
		}
		Date date1=(Date) dOb[col];
		String time="";
		col=col%2;
		if (col!=0) {
			time=date1+" 11:00";
		}else {
			time=date1+" 15:00";
		}
		int row=getInt("row");
		List<Mourning> mList = mourningService.getMourningList(null,"");
		Mourning mourning=mList.get(row-1);
		json.put("time", time);
		json.put("id", mourning.getId());
		json.put("name", mourning.getName());
		//以下判断灵堂类型
    	//以下选择灵堂之后,追加灵堂冷藏费(水晶棺)
		return json;
    }
    
    /**
     * 告别厅调度页面
     * @return
     */
    @RequestMapping(params = "method=editFarewell")
    public ModelAndView editFarewell(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int time=getInt("time");
		Date date = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("current_time")!=null){
			date = getDate("current_time");
		}
		if (time!=0) {
			date=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), time, Calendar.DATE);
		}
		model.put("date", date);
		List<Farewell> fList = farewellService.getFarewellList(null,"");
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		List<FarewellRecord> fRList=farewellRecordService.getFarewellRecordList(maps,"begin_date asc");
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[14];
		dOb[0]="告别厅";
		for (int i = 0; i < 13; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
		}
		list.add(dOb);
		//设置默认列表数组
		for(int i=0;i<fList.size();i++) {
			Farewell farewell = fList.get(i);
			list.add(new Object[]{farewell,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		for (FarewellRecord fr : fRList) {
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;
			//对比告别厅，确认行数
			for (int i = 1; i < list.size(); i++) {
				Object[] ob = list.get(i);
				Farewell farewell = (Farewell) ob[0];
				if(farewell.getId().equals(fr.getFarewellId())) {
					row = i;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if(DateTools.isSameHour(da, new Date(fr.getBeginDate().getTime()))) {
					col = 2*(d-1)+ifHalft(fr.getBeginDate());
				}
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					if(DateTools.isSameHour(da, new Date(fr.getEndDate().getTime()))){
						endCol = (d-1)*2+ifHalft(fr.getEndDate());
					}
				}
			}
			if (row != 0) {
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					Object[] o = (Object[])list.get(row);
					if(endCol==0){
						endCol = 28;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						o[x] = fr;
					}
				} else {
					if(row!=0){
						Object[] o = (Object[])list.get(row);
						if (col != 0) {
							o[col] = fr;
						}
					}
				}
			}
		}
		model.put("list", list);
        model.put("method", "editFarewell");
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
        model.put("IsFlag_Bzwc", Const.IsFlag_Bzwc);
        model.put("IsFlag_YY",Const.IsFlag_YY);
    	return new ModelAndView("ye/cremationChanges/editFarewell",model);
    }
    /**
     * 告别厅时间改变
     * @return
     */
    @RequestMapping(params = "method=farewellTime_change")
    @ResponseBody
    public JSONObject farewellTime_change(){
    	JSONObject json = new JSONObject();
    	try {
			Date date = null;
			date = getTime("time");
			String time="";
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.HOUR,1);
			date = calendar.getTime();
			time=sdf.format(date)+"";
			json.put("time", time);
			json.put("statusCode", 200);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "");
		}
		return json;
    }
    /**
     * 告别厅点击
     * @return
     */
    @RequestMapping(params = "method=farewell_click")
    @ResponseBody
    public JSONObject farewell_click(){
    	JSONObject json = new JSONObject();
    	Date date = DateTools.getThisDate();
    	if(getDate("date")!=null){
			date = getDate("date");
		}
    	int col=getInt("col");//列
    	Object[] dOb = new Object[30];
		//从当前时间开始的后七天
    	int r=1;
		for(int i=0;i<13;i++){
			dOb[r]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
			r++;
			dOb[r]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
			r++;
		}
		Date date1=(Date) dOb[col];
		String time="";
		String fTime="";
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date1);
		int c=col%2;
		if (c!=0) {
			time=sdf.format(date1)+"";
			calendar.add(Calendar.HOUR,1);
			date1 =calendar.getTime();
			fTime=sdf.format(date1)+"";
		}else {
			calendar.add(Calendar.MINUTE,30);
			date1 =calendar.getTime();
			time=sdf.format(date1)+"";
			calendar.add(Calendar.HOUR,1);
			date1 =calendar.getTime();
			fTime=sdf.format(date1)+"";
			
		}
		
		int row=getInt("row");
		List<Farewell> fList = farewellService.getFarewellList(null,"");
		Farewell farewell=fList.get(row-1);
		json.put("fTime", fTime);
		json.put("time", time);
		json.put("id", farewell.getId());
		json.put("name", farewell.getName());
		//以下开始判断告别厅
		Farewell fare=farewellService.getFarewellById(farewell.getId());
		String typeId=fare.getTypeId();
		FarewellType fareType=farewellTypeService.getFarewellTypeById(typeId);
		String typeName=fareType.getName();
		Item item=new Item();
		if("大厅".equals(typeName)){
			item=itemService.getItemByHelpCode("416");
		}else if("中厅".equals(typeName)){
			item=itemService.getItemByHelpCode("417");
		}else if("小厅".equals(typeName)){
			item=itemService.getItemByHelpCode("418");
		}
		Map<String,Object> serviceMaps=new HashMap<String, Object>();
		serviceMaps.put("typeId", item.getTypeId());
		serviceMaps.put("isdel", Const.Isdel_No);
    	String itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	String isOption=Const.getIsOption((byte)0, false);
    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
    	int sonIndex=item.getIndexFlag();
		List<Object> list=new ArrayList<Object>();
		list.add("service");
		list.add(itemOption);
		list.add(itemHelpCodeOption);
		list.add(item.getPice());
		list.add(itemTypeOption);
		list.add(1);
		list.add(isOption);
		list.add(item.getPice());
		list.add(item.getComment());
		list.add(faIndex);
		list.add(sonIndex);
		json.put("fareWellList", list);		
		return json;
    }
    
    
    
    /**
     * 判断是过半还是没有过半
     * @param time
     * @return 1：上半时 2：下半时
     */
    public byte ifHalft(Timestamp time) {
    	//时间转换对象
    	Calendar cal=Calendar.getInstance();
    	//设置时间
    	cal.setTime(new Date(time.getTime()));
    	int min=cal.get(Calendar.MINUTE);
    	if(min>=0 && min<30){
    		return (byte)1;
    	}else{
    		return (byte)2;
    	}
    } 
    
    

    

    /**
     * 火化炉调度页面
     * @return
     */
    @RequestMapping(params = "method=editFurnace")
    public ModelAndView editFurnace(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int time=getInt("time");
		Date date = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("current_time")!=null){
			date = getDate("current_time");
		}
		if (time!=0) {
			date=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), time, Calendar.DATE);
		}
		model.put("date", date);
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("furnaceBeginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("furnaceEndDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		List<FurnaceRecord> frList = furnaceService.getFurnaceRecordList(maps);
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[25];		
		dOb[0]="炉号";
		for (int i = 0; i < dOb.length-1; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 3, i, Calendar.HOUR);
		}
		list.add(dOb);
		Object[] dObTen = new Object[25];		
		dObTen[0]="炉号";
		for (int i = 0; i < dObTen.length-1; i++) {
			dObTen[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,20,Calendar.HOUR);
		}
		Object[] dObNine = new Object[25];		
		dObNine[0]="炉号";
		for (int i = 0; i < dObNine.length-1; i++) {
			dObNine[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,30,Calendar.HOUR);
		}
		Object[] dObEight = new Object[25];		
		dObEight[0]="炉号";
		for (int i = 0; i < dObEight.length-1; i++) {
			dObEight[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,40, Calendar.HOUR);
		}
		for (Furnace furnace : furnaceList) {
			Object[] o = new Object[dOb.length];
			o[0] = furnace;
			list.add(o);
		}
		for (FurnaceRecord furnaceRecord : frList) {
			Timestamp endTime = furnaceRecord.getEndTime();
			Timestamp orderTime = furnaceRecord.getOrderTime();
			if (endTime != null && !(endTime.equals(""))) {
				String endTimeStr = DateTools.getTimeFormatString("mm", endTime);
				if (endTimeStr.equals("30")||"20".equals(endTimeStr)||"40".equals(endTimeStr)) {
					endTime = DateTools.quzheng(endTime);
				}
			}
			if (orderTime != null && !(orderTime.equals(""))) {
				String orderTimeStr = DateTools.getTimeFormatString("mm", orderTime);
				if (orderTimeStr.equals("30")||"20".equals(orderTimeStr)||"40".equals(orderTimeStr)) {
					orderTime = DateTools.quzheng(orderTime);
				}
			}
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;//列
			for (int i = 1; i < list.size(); i++) {
				Object[] o = list.get(i);
				Furnace furnace = (Furnace) o[0];
				if (furnace.getId().equals(furnaceRecord.getFurnaceId())) {
					row = i;
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if (endTime != null && !(endTime.equals(""))) {
					if(DateTools.isSameHour(da, new Date(furnaceRecord.getBeginTime().getTime()))) {
						col = d;
					}
					if(DateTools.isSameHour(da, new Date(endTime.getTime()))){
						endCol = d;
					}
				} else {
					if (orderTime != null && !(orderTime.equals(""))) {
						if(DateTools.isSameHour(da, new Date(orderTime.getTime()))) {
							col = d;
						}
					}
				}
			}
			if(row!=0){
				Object[] o = (Object[])list.get(row);
				if (furnaceRecord.getEndTime() != null && !(furnaceRecord.getEndTime().equals(""))) {
					if(endCol==0){
						endCol = dOb.length-1;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						o[x] = furnaceRecord;
					}
				} else {
					if (col != 0) {
						o[col] = furnaceRecord;
					}
				}
				
			}
		}
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Wc", Const.IsFlag_Wc);
		model.put("IsFlag_Jx", Const.IsFlag_Jx);
		model.put("method", list);
		model.put("list", list);
		model.put("dOb", dOb);
		model.put("dObTen",dObTen);
		model.put("dObNine",dObNine);
		model.put("dObEight",dObEight);
		model.put("list_size", list.size());
//		if (getString("where").equals("changeFur")) {
//			return new ModelAndView("ye/commissionOrder/editFurnace2",model);//入炉转炉时候用
//		} else {
			return new ModelAndView("ye/cremationChanges/editFurnace",model);
//		}
    }
    
    
    
    /**
     * 火化炉点击
     * @return
     */
    @RequestMapping(params = "method=furnace_click")
    @ResponseBody
    public JSONObject furnace_click(){
    	JSONObject json = new JSONObject();
    	Date date = getDate("date");
    	int col=getInt("col");//列
    	int row=getInt("row");
		//判断页面是否有传日期回来，是则赋值给date
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Object[] dOb = new Object[25];
		for (int i = 0; i < dOb.length-1; i++) {
			dOb[i]=DateTools.getHourAfterHour(new Date(date.getTime()), 3, i, Calendar.HOUR);
		}
		
		Object[] dObTen = new Object[25];		
		dObTen[0]="炉号";
		for (int i = 0; i < dObTen.length-1; i++) {
			dObTen[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,20,Calendar.HOUR);
		}
		Object[] dObNine = new Object[25];		
		dObNine[0]="炉号";
		for (int i = 0; i < dObNine.length-1; i++) {
			dObNine[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,30,Calendar.HOUR);
		}
		Object[] dObEight = new Object[25];		
		dObEight[0]="炉号";
		for (int i = 0; i < dObEight.length-1; i++) {
			dObEight[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,40, Calendar.HOUR);
		}
		
		
		Date date1=(Date)dOb[col-1];
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=sdf.format(date1);
		Furnace furnace=furnaceList.get(row-1);
		
		if("8".equals(furnace.getCode())){
			Date date2=(Date)dObEight[col];
			time=sdf.format(date2);
		}
		if("9".equals(furnace.getCode())){
			Date date2=(Date)dObNine[col];
			time=sdf.format(date2);
		}
		if("10".equals(furnace.getCode())){
			Date date2=(Date)dObTen[col];
			time=sdf.format(date2);
		}
		json.put("time", time);
		json.put("id", furnace.getId());
		json.put("name", furnace.getName());
		
		return json;
    }
    
    
    
    /**
     * 判断是否为同一天
     * @param day1
     * @param day2
     * @return
     */
    private boolean isSomeDay(Date day1,Date day2){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ds1 = sdf.format(day1);
        String ds2 = sdf.format(day2);
        if (ds1.equals(ds2)) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * 判断上午下午
     * @param time
     * @return 1：上午 2：下午
     */
    private byte isPmOrAm(Timestamp time){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(time.getTime()));
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if(hour>=0 && hour<=12){
			return (byte)1;
		}else{
			return (byte)2;
		}
		
    }
    /**
     * 解锁
     * @return
     */
//    @RequestMapping(params = "method=unlock")
//    @SystemControllerLog(description = "火化委托单解锁")
//    @ResponseBody
//    public JSONObject unlock(){
//    	JSONObject json = new JSONObject();
//		List<Freezer> freezerList = freezerService.getFreezerList(null,"index_flag asc,name asc");
//		for (int i = 0; i < freezerList.size(); i++) {
//			Freezer freezer2=freezerList.get(i);
//			if (freezer2.getFlag()==Const.IsFlag_Lock) {
//				if (freezer2.getUserName().equals(getCurrentUser().getName())||getCurrentUser().getName()==freezer2.getUserName()) {
//					freezer2.setFlag(Const.IsFlag_No);
//					freezerService.updateFreezer(freezer2);
//				}
//			}
//		}
//		return json;
//    }
    
	/**
	 * 图片上传
	 * @param file
	 * @return
	 * @throws Exception
	 * 
	 */
	/**
	 * @param a 判断的状态
	 * @return 是或否
	 */
	public static String charge(byte a){
		String b="否";
		if(a==1){
			b="是";
		}
		return b;
	}
}
