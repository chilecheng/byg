package com.hz.controller.ye;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;

@Controller
@RequestMapping("/dispatcher.do")
public class DispatcherController extends BaseController {
	
	@RequestMapping
    public ModelAndView unspecified() {
		String string= getString("list");
		String[] arr = string.split("-");
		StringBuilder send = new StringBuilder("ye");
		for (int i = 0; i < arr.length; i++) {
				send.append("/").append(arr[i]);
		}
		return new ModelAndView(send.toString());
    }
}
