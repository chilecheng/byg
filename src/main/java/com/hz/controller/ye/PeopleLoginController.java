package com.hz.controller.ye;

import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSONObject;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.FireCheckInInfo;
import com.hz.entity.ye.PaperInfo;
import com.hz.entity.ye.PolitenessInfo;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FireCheckInService;
import com.hz.util.Const;
import com.hz.util.ItemConst;
/**
 * 游客登记
 * @author mbz
 *
 */
@Controller
@RequestMapping("/peopleLogin.do")
public class PeopleLoginController extends BaseController{
	@Autowired
	private FireCheckInService fireCheckInService;
	
	@Autowired
	private CommissionOrderService commissionOrderService;
	
	@ModelAttribute
	public void populateModel(Model model){
		model.addAttribute("url", "peopleLogin.do");
	}

	@RequestMapping
	public ModelAndView unspecified(){
		return new ModelAndView("ye/peopleLogin/peopleLoginPage",null);	
	}
	
	@RequestMapping(params = "method=people")
	@ResponseBody
	public JSONObject judge()
	{
		JSONObject json = new JSONObject();
		String cardCode=getString("readCard");
		int count = commissionOrderService.getCommissionOrderByCard(cardCode);
		if (count > 1) {
			setJsonByFail(json, "存在多个此卡号的用户，请再次确认卡号！");
			return json;
		}
		FireCheckInInfo listfc = null;
		try {
			listfc = fireCheckInService.getBasicInfoByCard(cardCode);
			if (listfc == null) {
				setJsonByFail(json, "没有对应的卡号");
			} else if (listfc.getPincardFlag() == Const.pincard_Yes) {
				setJsonByFail(json, "已销卡");
			} else if (listfc.getCheckFlag() == Const.Check_No) {
				setJsonByFail(json, "该卡尚未审核");
			} else if (listfc.getPayFlag() == Const.Pay_No) {
				setJsonByFail(json, "还未收费");
			} else if (listfc.getFurnaceFlag() == 3 && listfc.getCremationTime() ==null ) {
				setJsonByFail(json, "该遗体尚不需要火化");
			} else if (listfc.getCremationFlag() ==Const.furnace_flag_Yes){
				setJsonByFail(json, "该遗体已火化");
			}
			else {
				setJsonBySuccess(json, "存在卡号", "peopleLogin.do?method=list&cardCode=" + cardCode);
			}
		} catch (Exception e) {
			setJsonByFail(json, "卡号重复");
			e.printStackTrace();
		}
		return json;
	}
	
	@RequestMapping(params = "method=list")
	public ModelAndView showCheckCard()
	{
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String cardCode=getString("cardCode");
		if (getString("id") != null && !getString("id").equals("")) {
			cardCode = commissionOrderService.getCardCodeById(getString("id"));
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cardCode", cardCode);
		map.put("changeFlag", Const.ChangeFlag_No);
		model.put("recordId", fireCheckInService.getRecordIdByCardcode(map));//取出y_furnace_record里面的id
		FireCheckInInfo listfc=fireCheckInService.getBasicInfoByCard(cardCode);
		
		if(listfc!=null)
		{
			model.put("listfc", listfc);
			model.put("listfc_sex", listfc.getSexName());//!!!
			model.put("cremation_flag", listfc.getCremationOrNot());
			model.put("fireType", listfc.getFurnaceFlag());
			model.put("furnaceName", listfc.getFurnaceName());
		}
		PaperInfo pinfo=fireCheckInService.getPaperInfoByCard(cardCode, "纸棺");
		if(pinfo!=null&&pinfo.getPaperlist()!=null&&pinfo.getPaperlist().size()>0)//>0
		{
			model.put("pinfo", pinfo.getPaperlist());//.get(0).getPaperName()
		}
		PolitenessInfo poinfo=fireCheckInService.getPolitenessInfoByCard(cardCode, ItemConst.LiyiChubin_Sort);
		if(poinfo!=null&&poinfo.getPolitenesslist()!=null&&poinfo.getPolitenesslist().size()>0)//>0
		{
			model.put("poinfo", "有");
		}
		else
		{
			model.put("poinfo", "无");
		}
		return new ModelAndView("ye/peopleLogin/listPeopleLogin",model);
	}
	
	
}
