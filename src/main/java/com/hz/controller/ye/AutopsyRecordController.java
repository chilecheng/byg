package com.hz.controller.ye;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.system.User;
import com.hz.entity.ye.AutoFreezerDetail;
import com.hz.entity.ye.AutopsyRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.UserService;
import com.hz.service.ye.AutoFreezerService;
import com.hz.service.ye.AutopsyRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 *政法验尸
 */
@Controller
@RequestMapping("/autopsyRecord.do")
public class AutopsyRecordController extends BaseController {

	@Autowired
	private AutopsyRecordService autopsyRecordService;
	
	@Autowired
	private AutoFreezerService autoFreezerService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserNoticeService userNoticeService;
	
	
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	private void populateModel(Model model) {
		model.addAttribute("url","autopsyRecord.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified(){
		return listAllAutopsyRecord();
	}
	
	@RequestMapping(params="method=list")
	public ModelAndView listAllAutopsyRecord(){
		list.remove(list);
		
		Map<String , Object> model=new HashMap<String, Object>();
		model=getModel(model);
		
		
		Date startDate=getDate("startdate");
		if(startDate == null){
			startDate = DateTools.getThisDate();
		}
				
		int[] pages1=getPage("1");
		int[] pages2=getPage("2");
		int[] pages3=getPage("3");

		String select_kind=getString("select_kind");
		String search_val=getString("search_val");
		String startTime=getString("startTime");
		String endTime=getString("endTime");
		
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("select_kind", select_kind);
		maps.put("search_val", search_val);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime+"23:59:59");
		
		String searchOption=Const.getSearchTypeOptionByString(select_kind,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_Code,Const.Search_Type_IceNumber});
		Map<String, Object> bmap=new HashMap<String, Object>();
		Date selectTime2 = getDate("selectTime2");
		if(selectTime2 ==null){
			selectTime2 =  DateTools.getThisDate();
		}
		Timestamp selectStartTime =new Timestamp(DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(selectTime2)).getTime());
		Timestamp selectEndTime =DateTools.getDayEndTime(DateTools.utilDateToSqlDate(selectTime2));
		bmap.put("selectTime", selectTime2);
		bmap.put("selectStartTime", selectStartTime);
		bmap.put("selectEndTime", selectEndTime);
		Map<String, Object> cmap=new HashMap<String, Object>();
		Date selectTime3 = getDate("selectTime3");
		if(selectTime3 ==null){
			selectTime3 =  DateTools.getThisDate();
		}
		Timestamp selectStartTime3 =new Timestamp(DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(selectTime3)).getTime());
		Timestamp selectEndTime3 =DateTools.getDayEndTime(DateTools.utilDateToSqlDate(selectTime3));
		cmap.put("selectTime", selectTime3);
		cmap.put("selectStartTime", selectStartTime3);
		cmap.put("selectEndTime", selectEndTime3);
		
		model.put("searchOption", searchOption);
		model.put("selectTime2", selectTime2);
		model.put("selectTime3",selectTime3);
		/*
		 * 申请
		 */
		PageInfo<FreezerRecord> apage=autoFreezerService.getListAutoFreezer(maps,pages1[0],pages1[1],null);
		model.put("apage", apage);	
		model.put("page1", apage);
		
		/**
		 *解冻
		 */
	
		PageInfo<AutopsyRecord>  bpage=autopsyRecordService.getListAutoFreezerRecord(bmap,pages2[0],pages2[1],null);
		
		model.put("bpage", bpage);	
		model.put("page2", bpage);
		
		/**
		 * 验尸
		 */
		PageInfo<AutopsyRecord>  cpage=autopsyRecordService.getListAutopsyRecord(cmap,pages3[0],pages3[1],null);		
		for (AutopsyRecord autopsyRecord : cpage.getList()) {
			StringBuilder helpUser = new StringBuilder("");
			String assignUserId = autopsyRecord.getHelpUserId();
			if (assignUserId != null) {
				String[] assignUserIds = assignUserId.split(",");
				for(int i=0;i<assignUserIds.length;i++){
					if(i>0){
						helpUser.append(","); 
					}
					String name=userService.getUserNameById(assignUserIds[i]);
					if(name !=null){
						helpUser.append(name);		    		
					}
				}
			}
			autopsyRecord.setHelpUserNames(helpUser.toString());
		}
		model.put("page3", cpage);	
		return new ModelAndView("ye/second_department/autopsy/autopsy",model);	
	}
	
	/**
	 * 日期前移函数
	 */
	public String getDateBefore(Timestamp t,int day) {
		Calendar dates=Calendar.getInstance();
		dates.setTime(t);
		dates.set(Calendar.DATE,dates.get(Calendar.DATE) - day);
		
		java.util.Date dateTimestamp=dates.getTime();
	    SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//24閻忓繐绻戝鍌炲礆閿燂拷  
	    String LgTime = sdformat.format(dateTimestamp);   
		
		//java.util.Date dateTimestamp=dates.getTime();
		return LgTime;	
	}
	
	/**
	 * 验尸
	 * @return
	 */
	@RequestMapping(params="method=detail")
	public ModelAndView addAutopsyRecord(){
		String id=getString("id");
		Map<String, Object> model=new HashMap<String, Object>();
		User u=getCurrentUser();
		model.put("u", u);
		
		List<AutopsyRecord> listapr=autopsyRecordService.getAutopsyRecordById(id);
		if(listapr!=null&&listapr.size()!=0)
		{
			if(listapr.get(0)!=null)
			{
				model.put("listapr", listapr.get(0));
			}
		}
		model.put("fid", id);
		model.put("method", "update");
		/*List<Object[]> list1=new ArrayList<Object[]>();
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("groupType", (byte)5);
		List<User> helpUser=userService.getUserList(maps);//
		for(int i=0;i<helpUser.size();i++){
    		list1.add(new Object[]{helpUser.get(i).getUserId(),helpUser.get(i).getName()});
    	}
		String help_user=DataTools.getOptionByList(list1, null, false);
    	model.put("help_user",help_user);*/
		
		return new ModelAndView("ye/second_department/autopsy/toCheckAutopsy",model);
	}
	
	/**
	 *   法政验尸验尸保存
	 */
	@RequestMapping(params="method=update")
	@SystemControllerLog(description = "法政验尸验尸保存")
	@ResponseBody
	public JSONObject updateAutopsyRecord(){
		JSONObject json = new JSONObject();
		AutopsyRecord apr=new AutopsyRecord();
		String agentUserId=getString("agentUserId");
		try{
			if(agentUserId !=null && agentUserId !=""){
				apr.setAgentUserId(agentUserId);
				apr.setId(getString("fid"));
				apr.setComment(getString("comment")); 
				apr.setHelpUserId(getString("checkpsyId"));
				apr.setFlag((byte)1);
				User user = getCurrentUser();
				apr.setAgentUserId(user.getUserId());
				autopsyRecordService.updateAutopsyRecord(apr);
			
			setJsonBySuccess(json, "保存成功", "autopsyRecord.do?method=list");}

		} catch (Exception e) {
			setJsonByFail(json, "保存失败"+e.getMessage());

		}
		return json;
	}
	
	/**
	 * 解冻验尸名字链接
	 */
	@RequestMapping(params="method=todetail")
	public ModelAndView tolistauto(){
		String id=getString("id");
		Map<String, Object> model=new HashMap<String, Object>();
		List<AutopsyRecord> listapr=autopsyRecordService.getAutopsyRecordById(id);
		if(listapr!=null&&listapr.size()!=0)
		{
			if(listapr.get(0)!=null)
			{
				model.put("listapr", listapr.get(0));
			}
		}
		return new ModelAndView("ye/second_department/autopsy/tolistauto",model);
	}
	
	/**
	 * 申请页面名字链接
	 * @return
	 */
	@RequestMapping(params="method=todetail12")
	public ModelAndView tolistauto2(){
		String id=getString("id");
		Map<String, Object> map=new HashMap<String, Object>();
		Map<String,Object> model=new HashMap<String,Object>();
		map.put("id", id);
		AutoFreezerDetail detail = autopsyRecordService.getAutoFD(map);
		model.put("name", detail.getName());
		model.put("code", detail.getCode());
		model.put("comment", detail.getComment());
		model.put("age",detail.getAge());
		model.put("sex", detail.getSex());
		model.put("freezerName" ,detail.getFreezerName()  );
		model.put("proveName" ,detail.getProveName()  );
		model.put("registerTime" ,detail.getRegisterTime()  );
		model.put("yTime" , detail.getyTime() );
		if(detail.getSerialNumber()!=null){
			model.put("serialNumber", detail.getSerialNumber());
		}
		else{
			model.put("serialNumber", null);
		}
		if(detail.getRegisterUserId() != null){
			model.put("registerUser", userService.getUserById(detail.getRegisterUserId()).getName());	
		}
		else{
			model.put("registerUser", null);	
		}
			
		return new ModelAndView("ye/second_department/autopsy/charlistauto",model);
	}
	
	/**
	 *   ajax刷新验尸解冻消息
	 */
		@RequestMapping(params="method=refreshNumber")
		@SystemControllerLog(description = "修改验尸解冻消息")
		@ResponseBody
		public JSONObject refreshNumber(){
			JSONObject json = new JSONObject();
			java.sql.Date date = DateTools.getThisDate();
			getSession().setAttribute("autoFreezerNumber", 0);
			
			userNoticeService.updateUserNoticeByUser(getCurrentUser().getUserId(), Const.NoticeType_YsjdTask, date);
			json.put("isRefresh", true);
			return json;
		}

		
		/**
	     * 删除火化委托单
	     * @return
	     */
	    @RequestMapping(params = "method=delete")
	    @SystemControllerLog(description = "删除验尸信息")
	    @ResponseBody
		public JSONObject deleteCommissionOrder() {
			JSONObject json = new JSONObject();
			String id = getString("id1");
			try {	
				autopsyRecordService.deleteAutopsyRecord(id);
				setJsonBySuccess(json, "删除成功", true);
				
			} catch (Exception e) {
				e.printStackTrace();
				setJsonByFail(json, "系统异常" + e.getMessage());
			}
			return json;
		}
		
	
	
	/**
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	response.addHeader("Content-Disposition", "attachment;filename="+ new String(filename.getBytes()));    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.get(0).length];  //閻炴稏鍔嶉悧鎼佹儍閸曨剛鍨煎Λ鐗埳戦悥锟

        for (int i= 0;i<list.get(0).length;i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //闁哄瀚伴敓鐣屽Т椤曢亶宕欓搹纭咁潶


            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "政法验尸统计报表";  //title闂傚浄鎷烽悷鏇氭祰閸ゆ粌顔忔潏銊ョ樄閻庣櫢鎷 婵絾鏌ㄩ々褔宕樺鐬檈et


            ex.exportExcel(title,headers, list, out);  //title闁哄嫮妫渪cel閻炴稏鍔嬮懙鎴炴償閺囥垹鍔ラ柡鍕⒔閵囨岸鎯冮崟顔猴拷鍐冀閻撳孩鍊抽柨娑樿嫰椤╊湙heet
            out.close();
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	response.addHeader("Content-Disposition", "attachment;filename="+ new String(filename.getBytes()));    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");

        String[] headers = new String[list.size()];  //

        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {

            ExportWord ex = new ExportWord();  


            OutputStream  out = new BufferedOutputStream(response.getOutputStream());

            String  title = "政法验尸统计报表";
            ex.exportWord(title,headers,list, out);  //
            out.close();
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @RequestMapping(params = "method=choose")
    public ModelAndView chooseUser(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	List<Object[]> list = new ArrayList<Object[]>();
    	List<User> users = userService.getUserListByRoleName(Const.HELPER1);
    	users.addAll(userService.getUserListByRoleName(Const.HELPER2));
    	for(int i=0;i<users.size();i++){
    		list.add(new Object[]{users.get(i).getUserId(),users.get(i).getName()});
    	}
    	model.put("groupName", Const.HELPER);
    	model.put("list",list);
    	return new ModelAndView("../../common/worker",model);
    }
}
