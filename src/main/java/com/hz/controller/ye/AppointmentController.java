package com.hz.controller.ye;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Certificate;
import com.hz.entity.base.DeadType;
import com.hz.entity.base.Farewell;
import com.hz.entity.base.FarewellType;
import com.hz.entity.base.FreePerson;
import com.hz.entity.base.Furnace;
import com.hz.entity.base.HardPerson;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.base.Mourning;
import com.hz.entity.base.MourningType;
import com.hz.entity.ye.Appointment;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.MourningRecord;
import com.hz.service.base.AppellationService;
import com.hz.service.base.BillUnitService;
import com.hz.service.base.CarTypeService;
import com.hz.service.base.CertificateService;
import com.hz.service.base.CorpseAddressService;
import com.hz.service.base.CorpseUnitService;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.service.base.FarewellService;
import com.hz.service.base.FarewellTypeService;
import com.hz.service.base.FreePersonService;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.HardPersonService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.MourningService;
import com.hz.service.base.MourningTypeService;
import com.hz.service.base.NationService;
import com.hz.service.base.ProveUnitService;
import com.hz.service.base.SpecialBusinessTypeService;
import com.hz.service.base.ToponymService;
import com.hz.service.base.TransportTypeService;
import com.hz.service.ye.AppointmentService;
import com.hz.service.ye.BaseReductionDService;
import com.hz.service.ye.BaseReductionService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.HardReductionDService;
import com.hz.service.ye.HardReductionService;
import com.hz.service.ye.ItemOrderService;
import com.hz.service.ye.MourningRecordService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 综合调度(预约登记)
 * @author jgj
 *
 */
@Controller
@RequestMapping("/appointment.do")
public class AppointmentController extends BaseController{
	//告别厅
	@Autowired
	private FarewellService farewellService;
	//灵堂信息
	@Autowired
	private MourningService mourningService;
	//告别厅类型
	@Autowired
	private FarewellTypeService farewellTypeService;
	//预约登记
	@Autowired
	private AppointmentService appointmentService;
	//告别厅使用记录
	@Autowired
	private FarewellRecordService farewellRecordService;
	//守灵室使用记录
	@Autowired
	private MourningRecordService mourningRecordService;
	//灵堂类型
	@Autowired
	private MourningTypeService mourningTypeService;
	//火化炉信息
	@Autowired
	private FurnaceService furnaceService;
	//特殊业务类型
	@Autowired
	private SpecialBusinessTypeService specialBusinessTypeService;
//	
	
	@Autowired
	private CommissionOrderService commissionOrderService;
	//证件类型
	@Autowired
	private CertificateService certificateService;
	//死亡类型
	@Autowired
	private DeadTypeService deadTypeService;
	//死亡原因
	@Autowired
	private DeadReasonService deadReasonService;
	//证明单位
	@Autowired
	private ProveUnitService proveUnitService;
	//称谓关系
	@Autowired
	private AppellationService appellationService;
	//接尸单位
	@Autowired
	private CorpseUnitService corpseUnitService;
	//运输类型
	@Autowired
	private TransportTypeService transportTypeService;
	//收费项目类别
	@Autowired
	private ItemTypeService itemTypeService;
	//收费项目
	@Autowired
	private ItemService itemService;
	//火化委托单服务项目
	@Autowired
	private ItemOrderService itemOrderService;
	//车辆类型
	@Autowired
	private CarTypeService carTypeService;
	//免费对象类别
	@Autowired
	private FreePersonService freePersonService;
	//重点救助对象类别
	@Autowired
	private HardPersonService hardPersonService;
	//车辆调度
	@Autowired
	private CarSchedulRecordService carSchedulRecordService;
	//冷藏柜使用记录
	@Autowired
	private FreezerRecordService freezerRecordService;
	//困难减免申请
	@Autowired
	private HardReductionService hardReductionService;
	//困难减免项目
	@Autowired
	private HardReductionDService hardReductionDService;
	//基本减免申请
	@Autowired
	private BaseReductionService baseReductionService;
	//基本减免项目
	@Autowired
	private BaseReductionDService baseReductionDService;
	//挂账单位
	@Autowired
	private BillUnitService billUnitService;
	//地区
	@Autowired
	private ToponymService toponymService;
	//名族
	@Autowired
	private NationService nationService;
	//接尸地址
	@Autowired
	private CorpseAddressService corpseAddService;

	
    @ModelAttribute  
    public void populateModel(Model model) {  
       model.addAttribute("url", "appointment.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listCommissionOrder();
    }

	/**
	 * 预约三合一视图
	 * @return
	 */
    @RequestMapping(params = "method=view")
    public ModelAndView listCommissionOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		String deptName=getCurrentUser().getDept().getName();
		if("管理部".equals(deptName)){
			model.put("isShow", Const.Is_Yes);
		}else{
			model.put("isShow", Const.Is_No);
		}
		String id =getString("id");
		model.put("id", id);
		//此处判断是调度科调用还是三科调用
		String dept=getString("dept");
		model.put("dept", dept);
		//此处是做为判断,是否是修改页面
		Appointment appointment=null;
		String isEditAppointment=getString("isEditAppointment");
		if(isEditAppointment !=null && !"".equals(isEditAppointment)){
			appointment =appointmentService.getAppointmentById(isEditAppointment);
			model.put("isEdit", Const.Is_Yes);
		}
		
		/**
		 * 以下告别厅
		 */
		//预设当前日期
		Date date = DateTools.getThisDateUtil();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date = getDate("date");
		}
		model.put("date", date);
		List<Farewell> fList = farewellService.getFarewellList(null,"");//获得所有的告别厅
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		
		if(appointment !=null){//排除此预约情况,不在视图中显示以便重选更改
			maps.put("exceptId",appointment.getFarewellId());
		}
		List<FarewellRecord> fRList=farewellRecordService.getFarewellRecordList(maps,"begin_date asc");//获得所有的告别厅记录
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[14];
		dOb[0]="告别厅";
		//将时间分成需要的 时间段  并装入数组 
		for (int i = 0; i < 13; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
		}
		list.add(dOb);
		//设置默认列表数组             告别厅个数    每个告别厅放入一个Object数组
		for(int i=0;i<fList.size();i++) {
			Farewell farewell = fList.get(i);
			list.add(new Object[]{farewell,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历 所有的   告别厅记录条数
		for (FarewellRecord fr : fRList) {
			byte frflag=fr.getFlag();
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;
			//对比告别厅，确认行数
			for (int i = 1; i < list.size(); i++) {
				Object[] ob = list.get(i);
				Farewell farewell = (Farewell) ob[0];
				if(farewell.getId().equals(fr.getFarewellId())) {
					row = i;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if(DateTools.isSameHour(da, new Date(fr.getBeginDate().getTime()))) {
					col = 2*(d-1)+ifHalft(fr.getBeginDate());
				}
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					if(DateTools.isSameHour(da, new Date(fr.getEndDate().getTime()))){
						endCol = (d-1)*2+ifHalft(fr.getEndDate());
					}
				}
			}
			if (row != 0) {
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					Object[] o = (Object[])list.get(row);
					if(endCol==0){
						endCol = 28;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						//业务需求：如果维修与业务同时存在，优先显示业务
						if(frflag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
							o[x] = fr;
						}
						if(frflag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
							o[x] = fr;
						}
					}
				} else {
					if(row!=0){
						Object[] o = (Object[])list.get(row);
						if (col != 0) {
							o[col] = fr;
						}
					}
				}
			}
		}
		model.put("list", list);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Bzwc", Const.IsFlag_Bzwc);
		
		/**
		 * 以下火化炉调度
		 */
		//预设当前日期
		Date date2 = DateTools.getThisDateUtil();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date2 = getDate("date");
		}
		model.put("date", date2);
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Map<String, Object> maps2=new HashMap<String, Object>();
		maps2.put("furnaceBeginDate", DateTools.getDayFirstTime(new java.sql.Date(date2.getTime())));
		maps2.put("furnaceEndDate", DateTools.getDayEndTime(new java.sql.Date(date2.getTime())));
		
		if(appointment !=null){//排除此预约情况,不在视图中显示以便重选更改
			maps2.put("exceptId",appointment.getFuneralId());
		}
		List<FurnaceRecord> frList = furnaceService.getFurnaceRecordList(maps2);
		List<Object[]> list2 = new ArrayList<Object[]>();
		Object[] dOb2 = new Object[25];		
		dOb2[0]="炉号";
		for (int i = 0; i < dOb2.length-1; i++) {
			dOb2[i+1]=DateTools.getHourAfterHour(new Date(date2.getTime()), 3, i, Calendar.HOUR);
		}
		list2.add(dOb2);
		Object[] dObTen = new Object[25];		
		dObTen[0]="炉号";
		for (int i = 0; i < dObTen.length-1; i++) {
			dObTen[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,20,Calendar.HOUR);
		}
		Object[] dObNine = new Object[25];		
		dObNine[0]="炉号";
		for (int i = 0; i < dObNine.length-1; i++) {
			dObNine[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,30,Calendar.HOUR);
		}
		Object[] dObEight = new Object[25];		
		dObEight[0]="炉号";
		for (int i = 0; i < dObEight.length-1; i++) {
			dObEight[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,40, Calendar.HOUR);
		}
		for (Furnace furnace : furnaceList) {
			Object[] o = new Object[dOb2.length];
			o[0] = furnace;
			list2.add(o);
		}
		for (FurnaceRecord furnaceRecord : frList) {
			Timestamp endTime = furnaceRecord.getEndTime();
			Timestamp orderTime = furnaceRecord.getOrderTime();
			byte flag=furnaceRecord.getFlag();
			if (endTime != null && !(endTime.equals(""))) {
				String endTimeStr = DateTools.getTimeFormatString("mm", endTime);
				if (endTimeStr.equals("30")||"20".equals(endTimeStr)||"40".equals(endTimeStr)) {
					endTime = DateTools.quzheng(endTime);
				}
			}
			if (orderTime != null && !(orderTime.equals(""))) {
				String orderTimeStr = DateTools.getTimeFormatString("mm", orderTime);
				if (orderTimeStr.equals("30")||"20".equals(orderTimeStr)||"40".equals(orderTimeStr)) {
					orderTime = DateTools.quzheng(orderTime);
				}
			}
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;//列
			for (int i = 1; i < list2.size(); i++) {
				Object[] o = list2.get(i);
				Furnace furnace = (Furnace) o[0];
				if (furnace.getId().equals(furnaceRecord.getFurnaceId())) {
					row = i;
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb2.length;d++) {
				Date da = (Date)dOb2[d];
				if (endTime != null && !(endTime.equals(""))) {
					if(DateTools.isSameHour(da, new Date(furnaceRecord.getBeginTime().getTime()))) {
						col = d;
					}
					if(DateTools.isSameHour(da, new Date(endTime.getTime()))){
						endCol = d;
					}
				} else {
					if (orderTime != null && !(orderTime.equals(""))) {
						if(DateTools.isSameHour(da, new Date(orderTime.getTime()))) {
							col = d;
						}
					}
				}
			}
			if(row!=0){
				Object[] o = (Object[])list2.get(row);
				if (endTime != null && !(endTime.equals(""))) {
					if(endCol==0){
						endCol = dOb2.length-1;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						//业务需求：如果维修与业务同时存在，优先显示业务
						if(flag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
							o[x] = furnaceRecord;
						}
						if(flag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
							o[x] = furnaceRecord;
						}
					}
				} else {
					if (col != 0) {
						o[col] = furnaceRecord;
					}
				}
				
			}
		}
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Wc", Const.IsFlag_Wc);
		model.put("IsFlag_Jx", Const.IsFlag_Jx);
		model.put("IsFlag_YY", Const.IsFlag_YY);
		model.put("IsFlag_QC", Const.IsFlag_QC);
		model.put("list2", list2);
		model.put("dOb2", dOb2);
		model.put("dObTen",dObTen);
		model.put("dObNine",dObNine);
		model.put("dObEight",dObEight);
		
		/**
		 * 以下灵堂调度
		 */
		//预设当前日期
		Date date3 = DateTools.getThisDate();
		Map<String, Object> maps3=new HashMap<String, Object>();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date3 = getDate("date");
		}
		model.put("date", date3);
		List<Mourning> mList = mourningService.getMourningList(null,"");
		maps3.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date3.getTime())));
		
		maps3.put("endDate", DateTools.getDayEndTime(DateTools.getDayAfterDay(new java.sql.Date(date3.getTime()), 6, Calendar.DATE)));
		if(appointment !=null){//排除此预约情况,不在视图中显示以便重选更改
			maps3.put("exceptId",appointment.getMourningId());
		}
		List<MourningRecord> rList = mourningRecordService.getMourningRecordList(maps3,"begin_time");
		//获取搜索日期的之后7天的所有日期List
		List<Object[]> list3 = new ArrayList<Object[]>();
		Object[] dOb3 = new Object[8];
		dOb3[0]="灵堂";
		//设置第一行（日期）
		for(int i=0;i<7;i++){
			dOb3[i+1]=DateTools.getDayAfterDay(new java.sql.Date(date3.getTime()), i, Calendar.DATE);
		}
		list3.add(dOb3);
		//设置默认列表数组
		for(int i=0;i<mList.size();i++){
			Mourning mouring = (Mourning)mList.get(i);
			list3.add(new Object[]{mouring,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历所有记录，放在相应的位置
		for(int i=0;i<rList.size();i++){
			MourningRecord mr = rList.get(i);
			byte mrflag=mr.getFlag();
			int row = 0;//行
			int col = 0;//列
			int endCol=0;//最后一列
			//对比灵堂，确认行数
			for(int j=1;j<list3.size();j++){
				//把数组放在list中
				Object[] ob = list3.get(j);
				Mourning m = (Mourning)ob[0];
				if(m.getId().equals(mr.getMourningId())){
					row = j;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb3.length;d++){
				Date da = (Date)dOb3[d];
				if(isSomeDay(da, new Date(mr.getBeginTime().getTime()))){
					col = (d-1)*2+isPmOrAm(mr.getBeginTime());
				}
				if(isSomeDay(da, new Date(mr.getEndTime().getTime()))){
					endCol = (d-1)*2+isPmOrAm(mr.getEndTime());
				}
			}
			if(row!=0){
				Object[] o = (Object[])list3.get(row);
				if(endCol==0){
					endCol = 14;
				}
				if(col==0){
					col=1;
				}
				for(int x=col;x<=endCol;x++){
					//业务需求：如果维修与业务同时存在，优先显示业务
					if(mrflag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
						o[x] = mr;
					}
					if(mrflag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
						o[x] = mr;
					}
					o[x] = mr;
				}
			}			
		}
		model.put("list3", list3);
		
		return new ModelAndView("ye/appointment/comprehensive",model);
    }
	
	/**
	 * 链接到预约登记页面
	 * @return
	 */
	@RequestMapping(params = "method=order")
    public ModelAndView orderAppointment(){
		Map<String,Object> model=new HashMap<String,Object>();
		model.put("method", "save");
		String id=getString("id");
		Appointment appointment=appointmentService.getAppointmentById(id);
		if(appointment!=null){
			model.put("name", appointment.getName());
			model.put("phone", appointment.getPhone());		
			model.put("editId", id);
			model.put("method", "editSave");
		}
		return new ModelAndView("ye/appointment/order",model);
	}
	
	
	/**
	 * 查看页面
	 * @return
	 */
	@RequestMapping(params = "method=show")
    public ModelAndView showAppointment(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id=getString("id");
		Appointment appointment=appointmentService.getAppointmentById(id);
		model.put("appointment", appointment);
		return new ModelAndView("ye/appointment/showorder",model);
	}
	
	
	/**
	 * 选择界面(生成委托单OR数据转移卡号)
	 * @return
	 */
	@RequestMapping(params = "method=choose")
	public ModelAndView chooseAppointment(){
		Map<String,Object> model=new HashMap<String,Object>();
		model.put("id", getString("id"));
		model.put("method", "jumpPage");
		return new ModelAndView("ye/appointment/chooseOrder",model);
	}
	
	/**
     * 修改预约登记信息
     * @return
     */
    @RequestMapping(params = "method=editSave")
    @SystemControllerLog(description = "修改预约登记信息")
    @ResponseBody
    public JSONObject editSave(){
    	JSONObject json = new JSONObject();
    	try{
    		String id=getString("id");
    		String name=getString("dName");
    		String phone=getString("fphone");
    		String mourningCode=getString("mourningCode");
    		String farewellCode=getString("farewellCode");
    		String funeralCode=getString("funeralCode");
    		Timestamp mourningBeginTime=getTimeM("mourningBeginTime");
    		Timestamp mourningEndTime=getTimeM("mourningEndTime");
    		Timestamp farewellTime=getTimeM("farewellTime");
    		Timestamp funeralTime=getTimeM("funeralTime");    		
    		Timestamp nowTime=DateTools.getThisDateTimestamp();
    		String userId=getCurrentUser().getUserId();    		
    		
    		Appointment appointment=appointmentService.getAppointmentById(id);
    		appointment.setName(name);
    		appointment.setPhone(phone);
    		appointment.setFarewellCode(farewellCode);
    		appointment.setFarewellTime(farewellTime);
    		appointment.setFuneralCode(funeralCode);
    		appointment.setFuneralTime(funeralTime);
    		appointment.setMourningCode(mourningCode);
    		appointment.setMourningBeginTime(mourningBeginTime);
    		appointment.setMourningEndTime(mourningEndTime);
    		
    		appointmentService.updateAllRecord(appointment, farewellCode, funeralCode, mourningCode, nowTime, 
    				userId, farewellTime, funeralTime, mourningBeginTime, mourningEndTime);   		
    		
    		setJsonBySuccess(json, "修改成功", "appointment.do?method=listOrder");
    	} catch (Exception e) {    		
			e.printStackTrace();
			setJsonByFail(json, "修改失败，错误"+e.getMessage());
    	}
    	return json;
    }
	
	/**
	 * 删除预约登记 信息
	 * @return
	 */
	@RequestMapping(params = "method=deleteMessage")
    @SystemControllerLog(description = "删除预约登记信息")
    @ResponseBody
    public JSONObject delAppointment(){
    	JSONObject json = new JSONObject();
    	try{
    		String id=getString("id");
    		Appointment appointment=appointmentService.getAppointmentById(id);
    		appointmentService.delAllRecord(appointment);
    		setJsonBySuccess(json, "删除成功", "appointment.do?method=listOrder");
    	} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "删除失败，错误"+e.getMessage());
    	}
    	return json;
	}
	
	@RequestMapping(params = "method=isExist")
	@ResponseBody
	public JSONObject isExist(){
		JSONObject json= new JSONObject();
		try{
			String cardCode=getString("code");
			CommissionOrder commissionOrder=commissionOrderService.getCommissionOrderByCardCode(cardCode);
			if(commissionOrder!=null){
				setJsonBySuccess(json, "卡号正确");
			}else{
				setJsonByWarning(json, "该卡号没有数据");
			}
		}catch (Exception e){
			e.printStackTrace();
			setJsonByFail(json, "查询失败，错误"+e.getMessage());
		}
		return json;
	}
	/**
	 * 转化成火化委托单
	 * @return
	 */
	@RequestMapping(params = "method=jumpPage")
	public ModelAndView jumpPageAppointment(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id =null;
		String cardCode= getString("commissionCode");
		CommissionOrder cardCom=null;
		if(cardCode!=null && !"".equals(cardCode)){
			cardCom=commissionOrderService.getCommissionOrderByCardCode(cardCode);
		}
		if(cardCom!=null){
			id=cardCom.getId();
		}
		//首页审核
		String deadReasonOption="";
		CommissionOrder commissionOrder=new CommissionOrder();
		if(id!=null&&!id.equals("")){
//			CommissionOrder com =commissionOrderService.getCommissionOrderById(id);
			/*if(com.getViewsUncheck()==0){
				com.setViewsUncheck(Const.One);
				commissionOrderService.updateNumber(com);
			}*/
//			commissionOrderService.updateNumber2(id,Const.One);
			commissionOrder=commissionOrderService.getCommissionOrderById(id);
			model.put("province", commissionOrder.getProvince());
			model.put("city", commissionOrder.getCity());
			model.put("area", commissionOrder.getArea());
			deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
			if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
				String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
				if(nationName != null){
					commissionOrder.setdNationId(nationName);
				}
			}
			
			//收费项目
			List<Object[]> service_list=new ArrayList<Object[]>();
			List<Object[]> articles_list=new ArrayList<Object[]>();
			Map<String, Object> maps=new HashMap<String, Object>();
			maps.put("commissionOrderId", id);
			List<ItemOrder> item_list=itemOrderService.getItemOrderList(maps);
			for (int i = 0; i < item_list.size(); i++) {
				ItemOrder itemOrder =  new ItemOrder();
				itemOrder = item_list.get(i);
				String itemId=itemOrder.getItemId();
				Item item=itemService.getItemById(itemId);
				ItemType itemType=itemTypeService.getItemTypeById(item.getTypeId());
				//服务项目
				if (itemType.getType()==Const.Type_Service) {
					Map<String, Object>smaps=new HashMap<String, Object>();
					smaps.put("typeId", item.getTypeId());
					smaps.put("type", Const.Type_Service);
				//	smaps.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
				}
				//丧葬用品
				if (itemType.getType()==Const.Type_Articles) {
					Map<String, Object> smaps=new HashMap<String, Object>();
					smaps.put("typeId", item.getTypeId());
					smaps.put("type", Const.Type_Articles);
				//	smaps.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
				}	
			}
			model.put("service_list", service_list);
			model.put("articles_list", articles_list);			
		
		}else {
			//以下开始正常添加火化委托单
			List<DeadType> list=deadTypeService.getDeadTypeList(null);
			if (list.size()>0) {
				DeadType deadType=list.get(0);
				deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId(),"");
			}

			//默认服务项目
			List<Object[]> serviceList=new ArrayList<Object[]>();
			Map<String,Object> serviceMaps=new HashMap<String, Object>();
			serviceMaps.put("type", Const.Type_Service);
			serviceMaps.put("defaultFlag", Const.Is_Yes);
			serviceMaps.put("isdel", Const.Isdel_No);
			List<Item> itemList=itemService.getTypeItemList(serviceMaps);
			Map<String, Object> maps1=new HashMap<String, Object>();
		    for(int i=0;i<itemList.size();i++){
		    	Item item=itemList.get(i);
		    	maps1.put("typeId", item.getTypeId());
		    	maps1.put("isdel", Const.Isdel_No);
		    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
		    	String itemTypeOption=itemTypeService.getItemTypeOption(maps1,item.getTypeId(), false);
		    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps1,item.getId(),false);
		    	String isOption=Const.getIsOption((byte)0, false);
		    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
		    	int sonIndex=item.getIndexFlag();
		    	serviceList.add(new Object[]{"service",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
		    }
			model.put("serviceList", serviceList);
			
			
			//默认丧葬用品
			List<Object[]> articlesList=new ArrayList<Object[]>();
			Map<String, Object> articlesMaps=new HashMap<String, Object>();
			articlesMaps.put("type", Const.Type_Articles);
			articlesMaps.put("defaultFlag", Const.Is_Yes);
			articlesMaps.put("isdel", Const.Isdel_No);
			List<Item> list1=itemService.getTypeItemList(articlesMaps);
			Map<String, Object> maps2=new HashMap<String, Object>();
		    for(int i=0;i<list1.size();i++){
		    	Item item=list1.get(i);
		    	maps2.put("typeId", item.getTypeId());
		    	maps2.put("isdel", Const.Isdel_No);
		    	String itemOption=itemService.getItemOption(maps2,item.getId(),false);
		    	String itemTypeOption=itemTypeService.getItemTypeOption(articlesMaps,item.getTypeId(), false);
		    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(articlesMaps,item.getId(),false);
		    	String isOption=Const.getIsOption((byte)0, false);
		    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
		    	int sonIndex=item.getIndexFlag();
		    	articlesList.add(new Object[]{"articles",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
		    }
			model.put("articlesList", articlesList);
		}
		String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
		String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
		String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
		String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
		String dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
		String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
		String corpseUnitOption=corpseUnitService.getCorpseUnitOption(commissionOrder.getCorpseUnitId(),false);
		//String registerOption=registerService.getRegisterOption(commissionOrder.getRegisterId(), false);
		String toponymOption=toponymService.getToponymOption(commissionOrder.getToponymId(),false);
		//挂账单位
		String billUnitOption=billUnitService.getBillUnitOption(commissionOrder.getBillUnitId(), true);
		//民族
		String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);
		//接尸地址
		String corpseAddOption=corpseAddService.getCorpseAddressOption(commissionOrder.getPickAddr(), false);
		//特殊业务类型
		String specialBusinessOption=specialBusinessTypeService.getSpecialBusinessTypeOption(commissionOrder.getFurnaceComment(), false);
		
		
		//基本减免
		List<Object[]> blist=new ArrayList<Object[]>();
		List<FreePerson> freePersonList=freePersonService.getFreePersonList(null);
		List<HardPerson> hardPersonList=hardPersonService.getHardPersonList(null);
		Item baseItem=new Item();
		Map<String, Object>baseMaps=new HashMap<String, Object>();
		baseMaps.put("baseFlag", Const.Is_Yes);
		baseMaps.put("isdel", Const.Isdel_No);
		Map<String, Object>baseMaps2=new HashMap<String, Object>();
		baseMaps2.put("isdel", Const.Isdel_No);
		String baseItemOption=itemService.getItemOption(baseMaps2, "", false);
		String baseItemHelpCodeOption=itemService.getItemHelpCodeOption(baseMaps2, "", false);
		List<Item> baselist=itemService.getItemList(baseMaps);
		if (baselist.size()>0) {
			baseItem=baselist.get(0);
		}
		model.put("freePersonList", freePersonList);
		model.put("hardPersonList", hardPersonList);
		model.put("baseItemHelpCodeOption", baseItemHelpCodeOption);
		model.put("baseItemOption", baseItemOption);
		model.put("baseItem", baseItem);
		for (int i = 0; i < baselist.size(); i++) {
			Item item=baselist.get(i);
			
			
			String base_itemOption=itemService.getItemOption(null, item.getId(), false);
			String base_itemHelpCodeOption=itemService.getItemHelpCodeOption(null, item.getId(), false);
			int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
			blist.add(new Object[]{"base","",base_itemOption,base_itemHelpCodeOption,1,item.getPice(),"",faIndex,sonIndex});
			
		}
		
		model.put("baselist", blist);
		//困难减免
		List<Object[]> hlist=new ArrayList<Object[]>();
		List<Certificate> certificateList=certificateService.getCertificateList();
		Item hardItem=new Item();
		//此处应客户需要更改不再限制困难减免的项目
		Map<String, Object>hardMaps=new HashMap<String, Object>();
		hardMaps.put("hardFlag", Const.Is_Yes);
		hardMaps.put("isdel", Const.Isdel_No);
		String hardItemOption=itemService.getItemOption(null, "", false);
		String hardItemHelpCodeOption=itemService.getItemHelpCodeOption(null, "", false);
		List<Item> hardlist=itemService.getItemList(hardMaps);
		if (hardlist.size()>0) {
			 hardItem=hardlist.get(0);
		}
		model.put("certificateList", certificateList);
		model.put("hardItemOption", hardItemOption);
		model.put("hardItemHelpCodeOption", hardItemHelpCodeOption);
		model.put("hardItem", hardItem);
		for (int i = 0; i < hardlist.size(); i++) {
			Item item=(Item) hardlist.get(i);
			String hard_itemOption=itemService.getItemOption(null, item.getId(), false);
			String hard_itemHelpCodeOption=itemService.getItemHelpCodeOption(null, item.getId(), false);
			int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
			hlist.add(new Object[]{"hard","",hard_itemOption,hard_itemHelpCodeOption,1,item.getPice(),"",faIndex,sonIndex});
		}
		model.put("hardlist", hlist);
		
		
		//减免项目
		byte base_Is=Const.Is_No;
		byte hard_Is=Const.Is_No;
		byte special_Is=Const.Is_No;
		String checked="checked=checked";
		BaseReduction baseReduction=new BaseReduction();
		HardReduction hardReduction=new HardReduction();
		List<Object[]> base_list=new ArrayList<Object[]>();//基本减免项目
		List<Object[]> hard_list=new ArrayList<Object[]>();//困难减免项目
		List<Object[]> prove_list=new ArrayList<Object[]>();//证件类型
		List<Object[]> freePerson_list=new ArrayList<Object[]>();//免费对象类别
		List<Object[]> hardPerson_list=new ArrayList<Object[]>();//重点救助对象类别
		String hard_proveUnitOption=proveUnitService.getProveUnitOption("",false);//困难减免证明单位
		String hard_appellationOption=appellationService.getAppellationOption("",false);//困难减免称谓
		boolean isFree=false;//判断是否有免费对象类型,初始没有,则在前端默认居民选中.
		if(id!=null&&!id.equals("")){
			//基本减免
			Map<String, Object>maps=new HashMap<String, Object>();
			Map<String, Object>bMaps=new HashMap<String, Object>();
			List<BaseReductionD> baseList=new ArrayList<BaseReductionD>();
			maps.put("commissionOrderId", id);
			List<BaseReduction> bList=baseReductionService.getBaseReductionList(maps);
			if (bList.size()>0) {
				base_Is=Const.Is_Yes;
				baseReduction=(BaseReduction) bList.get(0);
				bMaps.put("baseReductionId", baseReduction.getId());
				baseList=baseReductionDService.getBaseReductionDList(bMaps);
			}
			for (int i = 0; i < baseList.size(); i++) {
				BaseReductionD baseReductionD=(BaseReductionD) baseList.get(i);
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("baseFlag", Const.Is_Yes);
				String baseItem_option=itemService.getItemOption(null, baseReductionD.getItemId(), false);
				String baseItemHelpCode_option=itemService.getItemHelpCodeOption(null, baseReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(baseReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(baseReductionD.getItemId()).getIndexFlag();
				base_list.add(new Object[]{"base",baseReductionD.getId(),baseItem_option,baseItemHelpCode_option,baseReductionD.getNumber(),baseReductionD.getTotal(),baseReductionD.getComment(),faIndex,sonIndex});
			}
			
			//免费对象类别
			List<FreePerson> freePersonList2=freePersonService.getFreePersonList(null);
			String[] free=null;
			if (baseReduction.getFreePersonIds()!=null&&!baseReduction.getFreePersonIds().equals("")) {
				free=baseReduction.getFreePersonIds().split(",");
			}
			for (int i = 0; i < freePersonList2.size(); i++) {
				boolean bool=true;
				FreePerson freePerson=(FreePerson) freePersonList2.get(i);
				if (free!=null) {
					for (int j = 0; j < free.length; j++) {
						if (free[j]==freePerson.getId()||free[j].equals(freePerson.getId())) {
							freePerson_list.add(new Object[]{"freePersonId",checked,freePerson.getId(),freePerson.getName()});
							bool=false;
							isFree=true;
						}
					}
				}
				if (bool) {
					freePerson_list.add(new Object[]{"freePersonId","",freePerson.getId(),freePerson.getName()});
				}
			}
			//重点救助对象类别
			List<HardPerson> hardPersonList2=hardPersonService.getHardPersonList(null);
			String[] hard=null;
			if (baseReduction.getHardPersonIds()!=null&&!baseReduction.getHardPersonIds().equals("")) {
				hard=baseReduction.getHardPersonIds().split(",");
			}
			for (int i = 0; i < hardPersonList2.size(); i++) {
				boolean bool=true;
				HardPerson hardPerson=(HardPerson) hardPersonList2.get(i);
				if (hard!=null) {
					for (int j = 0; j < hard.length; j++) {
						if (hard[j]==hardPerson.getId()||hard[j].equals(hardPerson.getId())) {
							hardPerson_list.add(new Object[]{"hardPersonId",checked,hardPerson.getId(),hardPerson.getName()});
							bool=false;
						}
					}
				}
				if (bool) {
					hardPerson_list.add(new Object[]{"hardPersonId","",hardPerson.getId(),hardPerson.getName()});
				}
			}
			//困难减免
			List<HardReductionD> hardList=new ArrayList<HardReductionD>();
			Map<String, Object>hMaps=new HashMap<String, Object>();
			hMaps.put("commissionOrderId", id);
			List<HardReduction> hList=hardReductionService.getHardReductionList(hMaps);
			if (hList.size()>0) {
				hard_Is=Const.Is_Yes;
				hardReduction= hList.get(0);
				hMaps.put("hardReductionId", hardReduction.getId());
				hardList=hardReductionDService.getHardReductionDList(hMaps);
				special_Is=hardReduction.getSpecial();
			}
			for (int i = 0; i < hardList.size(); i++) {
				HardReductionD hardReductionD=(HardReductionD) hardList.get(i);
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("hardFlag", Const.Is_Yes);
				String hardItem_option=itemService.getItemOption(null, hardReductionD.getItemId(), false);
				String hardItemHelpCode_option=itemService.getItemHelpCodeOption(null, hardReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(hardReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(hardReductionD.getItemId()).getIndexFlag();
				hard_list.add(new Object[]{"hard",hardReductionD.getId(),hardItem_option,hardItemHelpCode_option,hardReductionD.getNumber(),hardReductionD.getTotal(),hardReductionD.getComment(),faIndex,sonIndex});
			}
			//证件类型
			String[] prove=null;
			if (hardReduction.getProveIds()!=null&&!hardReduction.getProveIds().equals("")) {
				
				prove=hardReduction.getProveIds().split(",");
			}
			for (int i = 0; i < certificateList.size(); i++) {
				boolean bool=true;
				Certificate certificate=(Certificate) certificateList.get(i);
				if (prove!=null) {
					for (int j = 0; j < prove.length; j++) {
						if (prove[j]==certificate.getCertificateId()||prove[j].equals(certificate.getCertificateId())) {
							prove_list.add(new Object[]{"proveIds",checked,certificate.getCertificateId(),certificate.getName()});
							bool=false;
						}
					}
				}
				if (bool) {
					prove_list.add(new Object[]{"proveIds","",certificate.getCertificateId(),certificate.getName()});
				}
			}
			hard_proveUnitOption=proveUnitService.getProveUnitOption(hardReduction.getProveComment(),false);//困难减免证明单位
			hard_appellationOption=appellationService.getAppellationOption(hardReduction.getAppellationId(),false);//困难减免称谓
			
			
		}
		model.put("isFree",isFree);
		model.put("baseReduction", baseReduction);
		model.put("hard_proveUnitOption", hard_proveUnitOption);
		model.put("hard_appellationOption", hard_appellationOption);
		model.put("specialBusinessOption", specialBusinessOption);
		model.put("hardReduction", hardReduction);
		model.put("hard_list", hard_list);
		model.put("base_list", base_list);
		model.put("prove_list", prove_list);
		model.put("freePerson_list", freePerson_list);
		model.put("hardPerson_list", hardPerson_list);
		model.put("base_Is", base_Is);
		model.put("hard_Is", hard_Is);
		model.put("furnace_ty", Const.furnace_ty);
		model.put("baseIsOption", Const.getIsOption(base_Is, false));
		model.put("hardIsOption", Const.getIsOption(hard_Is, false));
		model.put("specialOption", Const.getIsOption(special_Is, false));
		//车辆调度
		String carTypeOption=carTypeService.getCarTypeOption("", false);
		String transportTypeOption=transportTypeService.getTransportTypeOption("", false);
		model.put("carTypeOption", carTypeOption);
		model.put("transportTypeOption", transportTypeOption);
		if(id!=null&&!id.equals("")){
			Map<String,Object> carMaps=new HashMap<String, Object>();
			carMaps.put("commissionOrderId", id);
			List<CarSchedulRecord> carSchedulRecordList=new ArrayList<CarSchedulRecord>();
			List<Object[]> car_List=new ArrayList<Object[]>();
			carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
			for (int i = 0; i < carSchedulRecordList.size(); i++) {
				CarSchedulRecord carSchedulRecord=carSchedulRecordList.get(i);
				String carType_option=carTypeService.getCarTypeOption(carSchedulRecord.getCarTypeId(), false);
				String transportType_option=transportTypeService.getTransportTypeOption(carSchedulRecord.getTransportTypeId(), false);
				Timestamp pickTi=carSchedulRecord.getPickTime();
				String pickTime = "";
				if (pickTi != null) {
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
					pickTime=sdf.format(pickTi);
				}
				car_List.add(new Object[]{carSchedulRecord.getId(),transportType_option,carType_option,pickTime,carSchedulRecord.getComment()});
			}
			model.put("car_List", car_List);
		}
		
		//火化炉
		List<Object[]> furnaceList=Const.getFurnaceList();
		model.put("furnaceList", furnaceList);
		//普通炉添加收费项目
		Item item=itemService.getItemByHelpCode("301");	    	
		Map<String,Object> serviceMaps=new HashMap<String, Object>();
		serviceMaps.put("typeId", item.getTypeId());
		serviceMaps.put("isdel", Const.Isdel_No);
    	String itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	String isOption=Const.getIsOption((byte)0, false);
    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
    	int sonIndex=item.getIndexFlag();
		List<Object> funList=new ArrayList<Object>();
		funList.add("service");
		funList.add(itemOption);
		funList.add(itemHelpCodeOption);
		funList.add(item.getPice());
		funList.add(itemTypeOption);
		funList.add(1);
		funList.add(isOption);
		funList.add(item.getPice());
		funList.add(item.getComment());
		funList.add(faIndex);
		funList.add(sonIndex);
		model.put("funList", funList);
		
		//特约炉添加收费项目
		item=itemService.getItemByHelpCode("420");	    	
    	itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	isOption=Const.getIsOption((byte)0, false);
    	
		List<Object> funListTY=new ArrayList<Object>();
		funListTY.add("service");
		funListTY.add(itemOption);
		funListTY.add(itemHelpCodeOption);
		funListTY.add(item.getPice());
		funListTY.add(itemTypeOption);
		funListTY.add(1);
		funListTY.add(isOption);
		funListTY.add(item.getPice());
		funListTY.add(item.getComment());
		funListTY.add(faIndex);
		funListTY.add(sonIndex);
		model.put("funListTY", funListTY);
		
		
		if (id != null && !id.equals("")) {
			//告别厅使用记录
			Map<String, Object>farewellMaps=new HashMap<String, Object>();
			farewellMaps.put("commissionOrderId", id);
			List<FarewellRecord>farewellRecordList=new  ArrayList<FarewellRecord>();
			FarewellRecord farewellRecord=new FarewellRecord();
			farewellRecordList=farewellRecordService.getFarewellRecordList(farewellMaps ,"id asc");
			if (farewellRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					farewellRecord=(FarewellRecord) farewellRecordList.get(0);
				}
			}
			model.put("farewellRecord", farewellRecord);
			
			//守灵室使用记录
			Map<String, Object>mourningMaps=new HashMap<String, Object>();
			mourningMaps.put("commissionOrderId", id);
			List<MourningRecord> mourningRecordList=new  ArrayList<MourningRecord>();
			MourningRecord mourningRecord=new MourningRecord();
			mourningRecordList=mourningRecordService.getMourningRecordList(mourningMaps,"id asc");
			if (mourningRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					mourningRecord=(MourningRecord) mourningRecordList.get(0);
				}
			}
			model.put("mourningRecord", mourningRecord);
			//冷藏柜使用记录
			Map<String, Object>freezerMaps=new HashMap<String, Object>();
			freezerMaps.put("commissionOrderId", id);
			List<FreezerRecord> freezerRecordList=new ArrayList<FreezerRecord>();
			FreezerRecord freezerRecord=new FreezerRecord();
			freezerRecordList=freezerRecordService.getFreezerRecordList(freezerMaps);
			
			if (freezerRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					freezerRecord=(FreezerRecord) freezerRecordList.get(0);
				}
			}
			model.put("freezerRecord", freezerRecord);
			//火化记录
			FurnaceRecord furnaceRecord = new FurnaceRecord();
			Map<String, Object>furnaceRecordMaps=new HashMap<String, Object>();
			furnaceRecordMaps.put("commissionOrderId", id);
			furnaceRecord = furnaceService.getFurnaceRecord(furnaceRecordMaps);
			model.put("furnaceRecord", furnaceRecord);
			List<Object[]> furnace_list = new ArrayList<Object[]>();
			for (int i = 0; i < furnaceList.size(); i++) {
				Object[] ob = furnaceList.get(i);
				byte by = (Byte) ob[0];
				if (by == commissionOrder.getFurnaceFlag()) {
					furnace_list.add(new Object[] { ob[0], ob[1], checked });
				} else {
					furnace_list.add(new Object[] { ob[0], ob[1], "" });
				}
			}
			model.put("furnace_list", furnace_list);
		} 
		
		//此处是预约登记连接过来的数据
		String appointmentId=getString("appointmentId");
		Appointment appointment=appointmentService.getAppointmentById(appointmentId);
		String appointmentName=appointment.getName();
		String appointmentPhone=appointment.getPhone();
		
		String farewellCode = appointment.getFarewellCode();
		Timestamp farewellTime = appointment.getFarewellTime();
		
		String funeralCode = appointment.getFuneralCode();
		Timestamp funeralTime = appointment.getFuneralTime();
		
		String mourningCode = appointment.getMourningCode();
		Timestamp mourningBeginTime = appointment.getMourningBeginTime();
		Timestamp mourningEndTime = appointment.getMourningEndTime();
		
		
		if(farewellCode!=null &&!"".equals(farewellCode)){//若有预定告别厅
			Farewell farewell=farewellService.getFarewellByName(farewellCode);
			String farewellId=farewell.getId();
			model.put("farewellId2", farewellId);
			String fTime="";
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(farewellTime);
			Date xx=DateTools.getDate(farewellTime);
			calendar.add(Calendar.HOUR,1);
			xx =calendar.getTime();
			fTime=sdf.format(xx)+"";
			//使用valueOf 对格式要求很严,暂时不用
//			Timestamp xxs=Timestamp.valueOf(fTime);
			
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setLenient(false);
			try {
			Timestamp ts = new Timestamp(format.parse(fTime).getTime());
			model.put("fireTime",ts);
			} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			
			//以下开始判断告别厅
			Farewell fare=farewellService.getFarewellById(farewellId);
			String typeId=fare.getTypeId();
			FarewellType fareType=farewellTypeService.getFarewellTypeById(typeId);
			String typeName=fareType.getName();
			Item newItem=new Item();
			if("大厅".equals(typeName)){
				newItem=itemService.getItemByHelpCode("416");
			}else if("中厅".equals(typeName)){
				newItem=itemService.getItemByHelpCode("417");
			}else if("小厅".equals(typeName)){
				newItem=itemService.getItemByHelpCode("418");
			}
			Map<String,Object> newMaps=new HashMap<String, Object>();
			newMaps.put("typeId", newItem.getTypeId());
			newMaps.put("isdel", Const.Isdel_No);
	    	String newItemOption=itemService.getItemOption(newMaps,newItem.getId(),false);
	    	String newTypeOption=itemTypeService.getItemTypeOption(newMaps,newItem.getTypeId(), false);
	    	String newHelpCodeOption=itemService.getItemHelpCodeOption(newMaps,newItem.getId(),false);
//	    	String isOption=Const.getIsOption((byte)0, false);
	    	int newFaIndex=itemTypeService.getItemTypeById(newItem.getTypeId()).getIndexFlag();
	    	int newSonIndex=newItem.getIndexFlag();
			List<Object> list=new ArrayList<Object>();
			list.add("service");
			list.add(newItemOption);
			list.add(newHelpCodeOption);
			list.add(newItem.getPice());
			list.add(newTypeOption);
			list.add(1);
			list.add(isOption);
			list.add(newItem.getPice());
			list.add(newItem.getComment());
			list.add(newFaIndex);
			list.add(newSonIndex);
			model.put("appointmentFarewell", list);	
		}
		if(mourningCode!=null &&!"".equals(mourningCode)){//若有预定灵堂
			Mourning mourning=mourningService.getMourningByName(mourningCode);
			String mourningId=mourning.getId();
			model.put("mourningId2", mourningId);
			
			//以下判断灵堂类型
			Mourning lingtang=mourningService.getMourningById(mourningId);
			String typeId=lingtang.getTypeId();
			MourningType mourningType=mourningTypeService.getMourningTypeId(typeId);
			String mType=mourningType.getName();
			Item newItem=new Item();
			if("普通".equals(mType)){
				newItem=itemService.getItemByHelpCode("501");
			}else if("豪华".equals(mType)){
				newItem=itemService.getItemByHelpCode("503");
			}else if("中高档".equals(mType)){
				newItem=itemService.getItemByHelpCode("502");
			}
			Map<String,Object> newMaps=new HashMap<String, Object>();
			newMaps.put("typeId", newItem.getTypeId());
			newMaps.put("isdel", Const.Isdel_No);
	    	String newOption=itemService.getItemOption(newMaps,newItem.getId(),false);
	    	String newTypeOption=itemTypeService.getItemTypeOption(newMaps,newItem.getTypeId(), false);
	    	String newHelpCodeOption=itemService.getItemHelpCodeOption(newMaps,newItem.getId(),false);
//	    	String isOption=Const.getIsOption((byte)0, false);
	    	int newFaIndex=itemTypeService.getItemTypeById(newItem.getTypeId()).getIndexFlag();
	    	int newSonIndex=newItem.getIndexFlag();
	    	//以下选择灵堂之后,追加灵堂冷藏费(水晶棺)
	    	Item item2=new Item();
	    	item2=itemService.getItemByHelpCode("402");
	    	Map<String,Object> serviceMaps2=new HashMap<String, Object>();
	    	serviceMaps2.put("typeId", item2.getTypeId());
	    	serviceMaps2.put("isdel", Const.Isdel_No);
	    	String itemOption2=itemService.getItemOption(serviceMaps2, item2.getId(),false);
	    	String itemTypeOption2=itemTypeService.getItemTypeOption(serviceMaps2,item2.getTypeId(), false);
	    	String itemHelpCodeOption2=itemService.getItemHelpCodeOption(serviceMaps2,item2.getId(),false);
	    	int faIndex2=itemTypeService.getItemTypeById(newItem.getTypeId()).getIndexFlag();
	    	int sonIndex2=newItem.getIndexFlag();
	    	
			List<Object> list=new ArrayList<Object>();
			list.add("service");
			list.add(newOption);
			list.add(newHelpCodeOption);
			list.add(newItem.getPice());
			list.add(newTypeOption);
			list.add(1);
			list.add(isOption);
			list.add(newItem.getPice());
			list.add(newItem.getComment());
			list.add(newFaIndex);
			list.add(newSonIndex);
			//追加的冷藏收费
			List<Object> list2=new ArrayList<Object>();
			list2.add("service");
			list2.add(itemOption2);
			list2.add(itemHelpCodeOption2);
			list2.add(item2.getPice());
			list2.add(itemTypeOption2);
			list2.add(1);
			list2.add(isOption);
			list2.add(item2.getPice());
			list2.add(item2.getComment());
			list2.add(faIndex2);
			list2.add(sonIndex2);
			model.put("iceList", list2);
			model.put("appointmentMourning", list);
		}
		if(funeralCode!=null &&!"".equals(funeralCode)){//若有预定火化炉
			Furnace furnace=furnaceService.getFurnaceByName(funeralCode);
			String funeralId=furnace.getId();
			model.put("funeralId2", funeralId);
		}
		model.put("appointmentName", appointmentName);
		model.put("appointmentPhone", appointmentPhone);
		model.put("farewellCode2", farewellCode);
		model.put("farewellTime2", farewellTime);
		model.put("funeralCode2", funeralCode );
		model.put("funeralTime2", funeralTime);
		model.put("mourningCode2", mourningCode);
		model.put("mourningBeginTime2", mourningBeginTime);
		model.put("mourningEndTime2", mourningEndTime);
		model.put("appointmentId", appointmentId);
		//以下 是车辆预定
		if(appointment.getIsCar()==Const.Is_Yes){
			String pickAddr=appointment.getPickAddress();
			Timestamp appTime=appointment.getDepartureTime();
			String appDepartureTime=DateTools.getTimeFormatString("yyyy-MM-dd HH:mm",appTime);
			//车辆类型
			String appCarTypeOption=carTypeService.getCarTypeOption(appointment.getCarType(),false);
			//运输类型
			String appTransportOption=transportTypeService.getTransportTypeOption(appointment.getTransportType(), false);
			model.put("carPick", Const.Is_Yes);
			model.put("pickAddr", pickAddr);
			model.put("appDepartureTime", appDepartureTime);
			model.put("appCarTypeOption", appCarTypeOption);
			model.put("appTransportOption", appTransportOption);
		}
		
		//
		byte checkFlag=commissionOrder.getCheckFlag();
		model.put("checkFlag", checkFlag);
		model.put("toponymOption", toponymOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("fAppellationOption", fAppellationOption);
		model.put("dFlagOption", dFlagOption);
		model.put("proveUnitOption", proveUnitOption);
		model.put("sexOption", sexOption);
		model.put("deadReasonOption", deadReasonOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("certificateOption", certificateOption);
		model.put("commissionOrder", commissionOrder);
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("id", id);
		model.put("billUnitOption", billUnitOption);
		model.put("nationOption", nationOption);
		model.put("corpseAddOption", corpseAddOption);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("isOption", Const.getIsOption((byte)0, false));
		model.put("agentUser", getCurrentUser().getName());
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=format.format(date);
		Timestamp now=new Timestamp(System.currentTimeMillis());
		model.put("now", format.format(now));
		model.put("time",time);
        model.put("method", "save");
        model.put("url", "commissionOrder.do");
		return new ModelAndView("ye/commissionOrder/editCommissionOrder",model);
	}
	
	
	/**
     * 保存预约登记信息
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存预约登记信息")
    @ResponseBody
    public JSONObject saveAppointment(){
    	JSONObject json = new JSONObject();
    	try{
    		
    		String userName=getCurrentUser().getName();
    		String name=getString("dName");
    		String phone=getString("fphone");
    		String mourningCode=getString("mourningCode");
    		String farewellCode=getString("farewellCode");
    		String funeralCode=getString("funeralCode");
    		Timestamp mourningBeginTime=getTimeM("mourningBeginTime");
    		Timestamp mourningEndTime=getTimeM("mourningEndTime");
    		Timestamp farewellTime=getTimeM("farewellTime");
    		Timestamp funeralTime=getTimeM("funeralTime");    		
    		Timestamp nowTime=DateTools.getThisDateTimestamp();
    		String userId=getCurrentUser().getUserId();
    		
    		Appointment appointment=null;
    		String id=getString("id");
    		if(id!=null && id!=""){
    			 appointment=appointmentService.getAppointmentById(id);
    		}else{
    			appointment=new Appointment();
    			appointment.setId(UuidUtil.get32UUID());
    			appointment.setCreateName(userName);
    			appointment.setCreateTime(DateTools.getThisDateTimestamp());
    		}
    		appointment.setName(name);
    		appointment.setPhone(phone);
    		appointment.setFarewellCode(farewellCode);
    		appointment.setFarewellTime(farewellTime);
    		appointment.setFuneralCode(funeralCode);
    		appointment.setFuneralTime(funeralTime);
    		appointment.setMourningCode(mourningCode);
    		appointment.setMourningBeginTime(mourningBeginTime);
    		appointment.setMourningEndTime(mourningEndTime);
    		appointment.setIsAppointment(Const.Is_Yes);
    		
    		appointmentService.saveAllRecord(id,appointment, farewellCode, funeralCode, mourningCode, nowTime, 
    				userId, farewellTime, funeralTime, mourningBeginTime, mourningEndTime);   		
    		
    		setJsonBySuccess(json, "保存成功", "appointment.do");
    	} catch (Exception e) {    		
			e.printStackTrace();
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
    	}
    	return json;
    }
    /**
     * 修改界面
     * @return
     */
    @RequestMapping(params ="method=editAppointment")
    public ModelAndView editAppointment(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		return new ModelAndView("ye/appointment/editAppointment",model);
    }
    /**
     * 告别厅时间改变
     * @return
     */
    @RequestMapping(params = "method=farewellDateChange")
    public ModelAndView farewellDateChange(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		Appointment appointment=null;
		String isEditAppointment=getString("id");
		if(isEditAppointment !=null && !"".equals(isEditAppointment)){
			appointment =appointmentService.getAppointmentById(isEditAppointment);
		}
    	//预设当前日期
		Date date = DateTools.getThisDateUtil();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date = getDate("date");
		}
		model.put("date", date);
		List<Farewell> fList = farewellService.getFarewellList(null,"");//获得所有的告别厅
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		if(appointment !=null){//排除此预约情况,不在视图中显示以便重选更改
			maps.put("exceptId",appointment.getFarewellId());
		}
		List<FarewellRecord> fRList=farewellRecordService.getFarewellRecordList(maps,"begin_date asc");//获得所有的告别厅记录
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[14];
		dOb[0]="告别厅";
		//将时间分成需要的 时间段  并装入数组 
		for (int i = 0; i < 13; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
		}
		list.add(dOb);
		//设置默认列表数组             告别厅个数    每个告别厅放入一个Object数组
		for(int i=0;i<fList.size();i++) {
			Farewell farewell = fList.get(i);
			list.add(new Object[]{farewell,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历 所有的   告别厅记录条数
		for (FarewellRecord fr : fRList) {
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;
			byte flag=fr.getFlag();
			//对比告别厅，确认行数
			for (int i = 1; i < list.size(); i++) {
				Object[] ob = list.get(i);
				Farewell farewell = (Farewell) ob[0];
				if(farewell.getId().equals(fr.getFarewellId())) {
					row = i;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if(DateTools.isSameHour(da, new Date(fr.getBeginDate().getTime()))) {
					col = 2*(d-1)+ifHalft(fr.getBeginDate());
				}
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					if(DateTools.isSameHour(da, new Date(fr.getEndDate().getTime()))){
						endCol = (d-1)*2+ifHalft(fr.getEndDate());
					}
				}
			}
			if (row != 0) {
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					Object[] o = (Object[])list.get(row);
					if(endCol==0){
						endCol = 28;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						//业务需求：如果维修与业务同时存在，优先显示业务
						if(flag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
							o[x] = fr;
						}
						if(flag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
							o[x] = fr;
						}
					}
				} else {
					if(row!=0){
						Object[] o = (Object[])list.get(row);
						if (col != 0) {
							o[col] = fr;
						}
					}
				}
			}
		}
		model.put("list", list);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Bzwc", Const.IsFlag_Bzwc);
		model.put("IsFlag_YY",Const.IsFlag_YY);
		return new ModelAndView("ye/appointment/farewell",model);
    }
    /**
     * 火化炉时间改变
     * @return
     */
    @RequestMapping(params = "method=funeralDateChange")
    public ModelAndView funeralDateChange(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		Appointment appointment=null;
		String isEditAppointment=getString("id");
		if(isEditAppointment !=null && !"".equals(isEditAppointment)){
			appointment =appointmentService.getAppointmentById(isEditAppointment);
		}
		/**
		 * 以下火化炉调度
		 */
		//预设当前日期
		Date date2 = DateTools.getThisDateUtil();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date2 = getDate("date");
		}
		model.put("date", date2);
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Map<String, Object> maps2=new HashMap<String, Object>();
		maps2.put("furnaceBeginDate", DateTools.getDayFirstTime(new java.sql.Date(date2.getTime())));
		maps2.put("furnaceEndDate", DateTools.getDayEndTime(new java.sql.Date(date2.getTime())));
		if(appointment !=null){//排除此预约情况,不在视图中显示以便重选更改
			maps2.put("exceptId",appointment.getFuneralId());
		}
		List<FurnaceRecord> frList = furnaceService.getFurnaceRecordList(maps2);
		List<Object[]> list2 = new ArrayList<Object[]>();
		Object[] dOb2 = new Object[25];		
		dOb2[0]="炉号";
		for (int i = 0; i < dOb2.length-1; i++) {
			dOb2[i+1]=DateTools.getHourAfterHour(new Date(date2.getTime()), 3, i, Calendar.HOUR);
		}
		list2.add(dOb2);
		Object[] dObTen = new Object[25];		
		dObTen[0]="炉号";
		for (int i = 0; i < dObTen.length-1; i++) {
			dObTen[i+1]=DateTools.getHourAfterHourHalf(new Date(date2.getTime()), 2, i,20,Calendar.HOUR);
		}
		Object[] dObNine = new Object[25];		
		dObNine[0]="炉号";
		for (int i = 0; i < dObNine.length-1; i++) {
			dObNine[i+1]=DateTools.getHourAfterHourHalf(new Date(date2.getTime()), 2, i,30,Calendar.HOUR);
		}
		Object[] dObEight = new Object[25];		
		dObEight[0]="炉号";
		for (int i = 0; i < dObEight.length-1; i++) {
			dObEight[i+1]=DateTools.getHourAfterHourHalf(new Date(date2.getTime()), 2, i,40, Calendar.HOUR);
		}
		for (Furnace furnace : furnaceList) {
			Object[] o = new Object[dOb2.length];
			o[0] = furnace;
			list2.add(o);
		}
		for (FurnaceRecord furnaceRecord : frList) {
			Timestamp endTime = furnaceRecord.getEndTime();
			Timestamp orderTime = furnaceRecord.getOrderTime();
			byte flag=furnaceRecord.getFlag();
			if (endTime != null && !(endTime.equals(""))) {
				String endTimeStr = DateTools.getTimeFormatString("mm", endTime);
				if (endTimeStr.equals("30")||"20".equals(endTimeStr)||"40".equals(endTimeStr)) {
					endTime = DateTools.quzheng(endTime);
				}
			}
			if (orderTime != null && !(orderTime.equals(""))) {
				String orderTimeStr = DateTools.getTimeFormatString("mm", orderTime);
				if (orderTimeStr.equals("30")||"20".equals(orderTimeStr)||"40".equals(orderTimeStr)) {
					orderTime = DateTools.quzheng(orderTime);
				}
			}
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;//列
			for (int i = 1; i < list2.size(); i++) {
				Object[] o = list2.get(i);
				Furnace furnace = (Furnace) o[0];
				if (furnace.getId().equals(furnaceRecord.getFurnaceId())) {
					row = i;
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb2.length;d++) {
				Date da = (Date)dOb2[d];
				if (endTime != null && !(endTime.equals(""))) {
					if(DateTools.isSameHour(da, new Date(furnaceRecord.getBeginTime().getTime()))) {
						col = d;
					}
					if(DateTools.isSameHour(da, new Date(endTime.getTime()))){
						endCol = d;
					}
				} else {
					if (orderTime != null && !(orderTime.equals(""))) {
						if(DateTools.isSameHour(da, new Date(orderTime.getTime()))) {
							col = d;
						}
					}
				}
			}
			if(row!=0){
				Object[] o = (Object[])list2.get(row);
				if (furnaceRecord.getEndTime() != null && !(furnaceRecord.getEndTime().equals(""))) {
					if(endCol==0){
						endCol = dOb2.length-1;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						//业务需求：如果维修与业务同时存在，优先显示业务
						if(flag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
							o[x] = furnaceRecord;
						}
						if(flag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
							o[x] = furnaceRecord;
						}
					}
				} else {
					if (col != 0) {
						o[col] = furnaceRecord;
					}
				}
				
			}
		}
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Wc", Const.IsFlag_Wc);
		model.put("IsFlag_Jx", Const.IsFlag_Jx);
		model.put("IsFlag_YY", Const.IsFlag_YY);
		model.put("list2", list2);
		model.put("dOb2", dOb2);
		model.put("dObTen",dObTen);
		model.put("dObNine",dObNine);
		model.put("dObEight",dObEight);
    	return new ModelAndView("ye/appointment/funeral",model);
    }
    /**
     * 灵堂时间改变
     * @return
     */
    @RequestMapping(params = "method=mourningDateChange")
    public ModelAndView mourningDateChange(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		Appointment appointment=null;
		String isEditAppointment=getString("id");
		if(isEditAppointment !=null && !"".equals(isEditAppointment)){
			appointment =appointmentService.getAppointmentById(isEditAppointment);
		}
		/**
		 * 以下灵堂调度
		 */
		//预设当前日期
		Date date3 = DateTools.getThisDate();
		Map<String, Object> maps3=new HashMap<String, Object>();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("date")!=null){
			date3 = getDate("date");
		}
		model.put("date", date3);
		List<Mourning> mList = mourningService.getMourningList(null,"");
		maps3.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date3.getTime())));
		
		maps3.put("endDate", DateTools.getDayEndTime(DateTools.getDayAfterDay(new java.sql.Date(date3.getTime()), 6, Calendar.DATE)));
		if(appointment !=null){//排除此预约情况,不在视图中显示以便重选更改
			maps3.put("exceptId",appointment.getMourningId());
		}
		List<MourningRecord> rList = mourningRecordService.getMourningRecordList(maps3,"begin_time");
		//获取搜索日期的之后7天的所有日期List
		List<Object[]> list3 = new ArrayList<Object[]>();
		Object[] dOb3 = new Object[8];
		dOb3[0]="灵堂";
		//设置第一行（日期）
		for(int i=0;i<7;i++){
			dOb3[i+1]=DateTools.getDayAfterDay(new java.sql.Date(date3.getTime()), i, Calendar.DATE);
		}
		list3.add(dOb3);
		//设置默认列表数组
		for(int i=0;i<mList.size();i++){
			Mourning mouring = (Mourning)mList.get(i);
			list3.add(new Object[]{mouring,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历所有记录，放在相应的位置
		for(int i=0;i<rList.size();i++){
			MourningRecord mr = rList.get(i);
			byte flag=mr.getFlag();
			int row = 0;//行
			int col = 0;//列
			int endCol=0;//最后一列
			//对比灵堂，确认行数
			for(int j=1;j<list3.size();j++){
				//把数组放在list中
				Object[] ob = list3.get(j);
				Mourning m = (Mourning)ob[0];
				if(m.getId().equals(mr.getMourningId())){
					row = j;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb3.length;d++){
				Date da = (Date)dOb3[d];
				if(isSomeDay(da, new Date(mr.getBeginTime().getTime()))){
					col = (d-1)*2+isPmOrAm(mr.getBeginTime());
				}
				if(isSomeDay(da, new Date(mr.getEndTime().getTime()))){
					endCol = (d-1)*2+isPmOrAm(mr.getEndTime());
				}
			}
			if(row!=0){
				Object[] o = (Object[])list3.get(row);
				if(endCol==0){
					endCol = 14;
				}
				if(col==0){
					col=1;
				}
				for(int x=col;x<=endCol;x++){
					//业务需求：如果维修与业务同时存在，优先显示业务
					if(flag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
						o[x] = mr;
					}
					if(flag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
						o[x] = mr;
					}
				}
			}			
		}
		model.put("list3", list3);
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Wc", Const.IsFlag_Wc);
		model.put("IsFlag_Bzwc", Const.IsFlag_Bzwc);
		model.put("IsFlag_Jx", Const.IsFlag_Jx);
		model.put("IsFlag_YY", Const.IsFlag_YY);
		model.put("IsFlag_QC", Const.IsFlag_QC);
    	return new ModelAndView("ye/appointment/mourning",model);
    }
    
    /**
     * 根据登记姓名查找信息 单结果
     * @return
     */
    @RequestMapping(params = "method=dNameSearch")
    @ResponseBody
    public JSONObject dNameSearch(){
		JSONObject json = new JSONObject();
    	String dName = getString("dName");
    	Map<String, Object> maps = new HashMap<String, Object>();
    	maps.put("name", dName);
    	List<Appointment> list = appointmentService.getAppointmentList(maps);
    	Appointment appointment=null;
    	if(list.size()==1){
    		appointment = list.get(0);
    		String serialNumber=appointment.getSerialNumber();
    		String name = appointment.getName();
    		String phone = appointment.getPhone();	
    		String id = appointment.getId();
	    	json.put("name", name);
	    	json.put("phone",phone);
	    	json.put("serialNumber", serialNumber);
	    	json.put("id", id);
    	}
    	json.put("size", list.size());
		return json;
    }
    
    /**
	 *登记姓名查找  多结果 
	 * @return
	 */
    @RequestMapping(params = "method=NameList")
    public ModelAndView dNameListbuyWreathRecord(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	model=getModel(model);
    	//获得页面页码信息
		int[] pages = getPage();
    	String dName = getString("name");
    	Map<String, Object> maps = new HashMap<String, Object>();
    	maps.put("name", dName);
    	PageHelper.startPage(pages[0], pages[1]);
    	List<Appointment> list = appointmentService.getAppointmentList(maps);
    	PageInfo<Appointment> page1 = new PageInfo<Appointment>(list);
    	List<Object[]> list0 = new ArrayList<Object[]>();
    	for(int i=0;i<list.size();i++){
	    	list0.add(new Object[]{list.get(i).getId(),list.get(i).getSerialNumber(),list.get(i).getName(),
	    			list.get(i).getPhone()});
    	}
    	PageInfo<Object[]> page = new PageInfo<Object[]>(list0);
    	page.setPageNum(page1.getPageNum());
		page.setSize(page1.getSize());
		page.setPageSize(page1.getPageSize());
		page.setStartRow(page1.getStartRow());
		page.setEndRow(page1.getEndRow());
		page.setTotal(page1.getTotal());
		page.setPages(page1.getPages());
		model.put("page", page);
    	return new ModelAndView("ye/appointment/seachResult",model);
    }
    /**
     * 预约列表
     * @return
     */
    @RequestMapping(params = "method=listOrder")
    public ModelAndView listOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int[] pages = getPage();
		String search=getString("search");
		String beginDate=getString("startTime");
		String endDate=getString("endTime");
		Map<String,Object> maps =new HashMap<>();
		if(!"".equals(search) && search!=null){
//			maps.put("findBy",getString("find"));	
			maps.put("name", search);
		}
		if(!"".equals(beginDate) && beginDate !=null){
			Date startTime=DateTools.stringToSqlDate(beginDate, "yyyy-MM-dd");
			maps.put("startTime",startTime);
		}
		if(!"".equals(endDate) && endDate !=null){
			Date endTime=DateTools.stringToSqlDate(endDate, "yyyy-MM-dd");
			maps.put("endTime", endTime);
		}
		maps.put("isAppointment", Const.Is_Yes);
		PageInfo page=appointmentService.getAppointmentListPageInfo(maps,pages[0],pages[1],"create_time desc");
		
		model.put("page", page);
		model.put("method","listOrder");
		return new ModelAndView("ye/appointment/listOrder",model);
    }
    /**
     * 判断是过半还是没有过半
     * @param time
     * @return 1：上半时 2：下半时
     */
    public byte ifHalft(Timestamp time) {
    	//时间转换对象
    	Calendar cal=Calendar.getInstance();
    	//设置时间
    	cal.setTime(new Date(time.getTime()));
    	int min=cal.get(Calendar.MINUTE);
    	if(min>=0 && min<30){
    		return (byte)1;
    	}else{
    		return (byte)2;
    	}
    }
    /**
     * 判断是否为同一天
     * @param day1
     * @param day2
     * @return
     */
    private boolean isSomeDay(Date day1,Date day2){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ds1 = sdf.format(day1);
        String ds2 = sdf.format(day2);
        if (ds1.equals(ds2)) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * 判断上午下午
     * @param time
     * @return 1：上午 2：下午
     */
    private byte isPmOrAm(Timestamp time){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(time.getTime()));
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if(hour>=0 && hour<12){
			return (byte)1;
		}else{
			return (byte)2;
		}
		
    }
}
