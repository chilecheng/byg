package com.hz.controller.ye;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.ye.ListCarService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ExportExcel;
import com.hz.util.ExportWord;

/**
 * 车辆列表
 * @author mbz
 *
 */
@Controller
@Scope("session")
@RequestMapping("/ListCar.do")
public class ListCarController extends BaseController {
	@Autowired
	private ListCarService listCarService;
	@Autowired
	private UserNoticeService userNoticeService;
	List<Object[]> list = new ArrayList<Object[]>();
	
	@ModelAttribute
	public void populateModel(Model model) {  
		model.addAttribute("url", "ListCar.do");
	}
	
	@RequestMapping
	public ModelAndView unspecified() {
		return listListCar();
    }
	
	/**
	 * 车辆列表
	 * @return
	 */
	@RequestMapping(params = "method=list")
	private ModelAndView listListCar() {
		Map<String,Object> model=new HashMap<String, Object>();
		model=getModel(model);
		list.removeAll(list);
		//获取分页
		int pages[]=getPage();
		//查询
		Map<String, Object> map=new HashMap<String, Object>();
		Date start = getDate("startTime");
		Date end = getDate("endTime");
		if (start == null) {
			start = DateTools.getDayFirstTime(DateTools.getThisDate());
		} else {
			start = DateTools.getDayFirstTime(DateTools.utilDateToSqlDate(start));
		}
		if (end == null || ("").equals(end)) {
			end = DateTools.getDayEndTime(DateTools.getDayAfterDay(DateTools.getThisDate(), 1, Calendar.DATE));
		} else {
			end = DateTools.getDayEndTime(DateTools.utilDateToSqlDate(end));
		}
		map.put("start", start);
		map.put("end", end);
		PageHelper.startPage(pages[0], pages[1]);
		PageHelper.orderBy("creat_time");
        List<CarSchedulRecord> csr = listCarService.findListCar(map);
        PageInfo<CarSchedulRecord> page1 = new PageInfo<CarSchedulRecord>(csr);
		for(int i=0; i<csr.size(); i++){
			Object[] o = new Object[7];
			Date pickTime = csr.get(i).getPickTime();
			String dAddr = csr.get(i).getCommissionOrder().getdAddr();
			String fPhone = csr.get(i).getCommissionOrder().getfPhone();
			String oName = csr.get(i).getCommissionOrder().getName();
			String tName = csr.get(i).getTransportType().getName();
			String cName = csr.get(i).getCarType().getName();
			String comment = csr.get(i).getComment();
			o[0] = pickTime;
			o[1] = dAddr;
			o[2] = fPhone;
			o[3] = oName;
			o[4] = tName;
			o[5] = cName;
			o[6] = comment;
			list.add(o);
		}
		PageInfo<Object[]> page = new PageInfo<Object[]>(list);
		page.setPageNum(page1.getPageNum());
		page.setSize(page1.getSize());
		page.setPageSize(page1.getPageSize());
		page.setStartRow(page1.getStartRow());
		page.setEndRow(page1.getEndRow());
		page.setTotal(page1.getTotal());
		page.setPages(page1.getPages());
		java.sql.Date nextDay = DateTools.getDayAfterDay(DateTools.getThisDate(), 1, Calendar.DATE);
		userNoticeService.updateUserNoticeByUser(getCurrentUser().getUserId(), Const.NoticeType_CarTask, DateTools.getThisDate());
		userNoticeService.updateUserNoticeByUser(getCurrentUser().getUserId(), Const.NoticeType_CarTask, nextDay);
		model.put("method", "list");
        model.put("page", page);
		model.put("start", start);
		model.put("end", end);
		return new ModelAndView("ye/second_department/carList/carList",model);
	}
	
	/**
     * 导出Excel测试(mbz)
     * @return
     */
    @RequestMapping(params = "method=exportExcel")
    public void exportExcel(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "车辆列表统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".xls";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportExcel ex = new ExportExcel();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            //OutputStream out = new FileOutputStream("E://c.xls");  
            String  title = "车辆列表统计";  //title需要自己指定 比如写Sheet

            ex.exportExcel(title,headers, list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 导出Word测试(mbz)
     * @return
     * @throws Exception 
     */
    @RequestMapping(params = "method=exportWord")
    public void exportWord(HttpServletResponse response){
    	Timestamp ts = DateTools.getThisDateTimestamp();
    	int year = ts.getYear()+1900;
    	int month = ts.getMonth()+1;
    	String filename = "车辆列表统计"+year+""+month+""+ts.getDay()+""+ts.getHours()+""+ts.getMinutes()+""+ts.getSeconds()+".docx";
    	try {
			response.addHeader("Content-Disposition", "attachment;filename="+new String(filename.getBytes("gb2312"),"ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    
        response.setContentType("application/vnd.ms-excel;charset=gb2312");
        String[] headers = new String[list.size()];  //表格的标题栏
        for (int i= 0;i<list.size();i++){
        	headers[i] = list.get(0)[i].toString();
        }
        try {
            ExportWord ex = new ExportWord();  //构造导出类

            OutputStream  out = new BufferedOutputStream(response.getOutputStream());
            String  title = "车辆列表统计";
            ex.exportWord(title,headers,list, out);  //title是excel表中底部显示的表格名，如Sheet
            out.close();
            //JOptionPane.showMessageDialog(null, "导出成功!");
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
