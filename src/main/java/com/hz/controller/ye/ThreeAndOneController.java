package com.hz.controller.ye;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hz.controller.system.BaseController;
import com.hz.entity.system.User;
import com.hz.entity.ye.ThreeAndOne;
import com.hz.service.system.RoleService;
import com.hz.service.system.UserService;
import com.hz.service.ye.ThreeAndOneService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;

/**
 * 礼厅服务质量反馈表
 * @author Administrator
 *
 */
@Controller
@RequestMapping("threeAndOne.do")
public class ThreeAndOneController extends BaseController{

	@Autowired
	private ThreeAndOneService threeAndOneService; 
	@Autowired
	private UserService userService;
	@ModelAttribute  
	    public void populateModel( Model model) {  
	       model.addAttribute("url", "threeAndOne.do");
	    }
	 @RequestMapping
	    public ModelAndView unspecified() {
			return loadingQuaityService();
	    }
	 @RequestMapping(params="method=dead")
	private ModelAndView loadingQuaityService() {
		 Map<String,Object> model=new HashMap<String,Object>();
			model=getModel(model);
			//通过页面传回用户的id
			String farewellId=getString("deadId");
			String id=threeAndOneService.getCommissionOrderId(farewellId);
			//通过id进行查询相关的信息
			ThreeAndOne tao= threeAndOneService.getAllInformation(id);
			Date printTime=DateTools.stringToSqlDate(tao.getFarewellTime(),"yyyy-MM-dd HH:mm");
//			String[] s=getThisDate();
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("name", "花圈花篮个数统计人");
			map.put("isdel", Const.Is_Yes);
//			List<User> list=userService.getUserList(map);
			//String flowersOption=userService.getItemOption(map,null,false);
			//一科人员下拉选
			String flowersOption = userService.getFirstBeforeNameOption(map,null,false);
			map.put("name", "礼厅工作人员");
			String liOption=userService.getFirstNameOption(map,null,false);
			Date nowTime=DateTools.getThisDate();
			model.put("tao", tao);
			model.put("flowersOption", flowersOption);
			model.put("liOption", liOption);
			model.put("printTime", printTime);
			model.put("method","dead" );
//			model.put("dateYear",s[0] );
//			model.put("datemonth",s[1] );
//			model.put("datedate",s [2]);
			model.put("nowTime",nowTime);
		return new ModelAndView("ye/threeAndOne/charFarewell", model);
	}
	 /**
	  * 获取 今天的日期 以   - 年-月-日 格式返回  
	 * @return
	 */
	public static String[] getThisDate(){
		String[] s=new String [3];
		Date now = new Date(); 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
		int year = c.get(Calendar.YEAR); 
		int month = c.get(Calendar.MONTH)+1; 
		int date = c.get(Calendar.DATE);
		s[0]=year+"";
		s[1]=String.valueOf(month);
		s[2]=date+"";
//		String DATE=year+"" +"年"+""+month+""+"月"+""+date+""+"日";
		
	return s; 
	}
	 
}
 
