package com.hz.controller.ye;

import java.awt.Image;
import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.hz.annotation.SystemControllerLog;
import com.hz.controller.system.BaseController;
import com.hz.entity.base.Certificate;
import com.hz.entity.base.DeadType;
import com.hz.entity.base.Farewell;
import com.hz.entity.base.FarewellType;
import com.hz.entity.base.FreePerson;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.FreezerType;
import com.hz.entity.base.Furnace;
import com.hz.entity.base.HardPerson;
import com.hz.entity.base.Item;
import com.hz.entity.base.ItemType;
import com.hz.entity.base.Mourning;
import com.hz.entity.base.MourningType;
import com.hz.entity.base.SpecialBusinessType;
import com.hz.entity.base.Toponym;
import com.hz.entity.payDivision.BusinessFees;
import com.hz.entity.system.User;
import com.hz.entity.ye.Appointment;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.MourningRecord;
import com.hz.service.base.AppellationService;
import com.hz.service.base.BillUnitService;
import com.hz.service.base.CarTypeService;
import com.hz.service.base.CertificateService;
import com.hz.service.base.CorpseAddressService;
import com.hz.service.base.CorpseUnitService;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.service.base.FarewellService;
import com.hz.service.base.FarewellTypeService;
import com.hz.service.base.FreePersonService;
import com.hz.service.base.FreezerService;
import com.hz.service.base.FreezerTypeService;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.HardPersonService;
import com.hz.service.base.ItemService;
import com.hz.service.base.ItemTypeService;
import com.hz.service.base.MourningService;
import com.hz.service.base.MourningTypeService;
import com.hz.service.base.NationService;
import com.hz.service.base.ProveUnitService;
import com.hz.service.base.RegisterService;
import com.hz.service.base.SpecialBusinessTypeService;
import com.hz.service.base.ToponymService;
import com.hz.service.base.TransportTypeService;
import com.hz.service.payDivision.BusinessFeesService;
import com.hz.service.payDivision.DelegateService;
import com.hz.service.ye.AppointmentService;
import com.hz.service.ye.BaseReductionDService;
import com.hz.service.ye.BaseReductionService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.FarewellRecordService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.service.ye.FuneralRecordService;
import com.hz.service.ye.FurnaceRecordService;
import com.hz.service.ye.HardReductionDService;
import com.hz.service.ye.HardReductionService;
import com.hz.service.ye.ItemOrderService;
import com.hz.service.ye.MourningRecordService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;
/**
 * 火化委托单
 * @author tsp
 *
 */
@Controller
@RequestMapping("/commissionOrder.do")
public class CommissionOrderController extends BaseController{
	
	private static Logger logger = LoggerFactory.getLogger(CommissionOrderController.class); 
	@Autowired
	private CommissionOrderService commissionOrderService;
	//证件类型
	@Autowired
	private CertificateService certificateService;
	//死亡类型
	@Autowired
	private DeadTypeService deadTypeService;
	//死亡原因
	@Autowired
	private DeadReasonService deadReasonService;
	//证明单位
	@Autowired
	private ProveUnitService proveUnitService;
	//称谓关系
	@Autowired
	private AppellationService appellationService;
	//告别厅
	@Autowired
	private FarewellService farewellService;
	//告别厅类型
	@Autowired
	private FarewellTypeService farewellTypeService;
	//接尸单位
	@Autowired
	private CorpseUnitService corpseUnitService;
	//冷藏柜信息
	@Autowired
	private FreezerService freezerService;
	//灵堂信息
	@Autowired
	private MourningService mourningService;
	//灵堂类型
	@Autowired
	private MourningTypeService mourningTypeService;
	//户籍
	@Autowired
	private RegisterService registerService;
	//运输类型
	@Autowired
	private TransportTypeService transportTypeService;
	//收费项目类别
	@Autowired
	private ItemTypeService itemTypeService;
	//收费项目
	@Autowired
	private ItemService itemService;
	//火化委托单服务项目
	@Autowired
	private ItemOrderService itemOrderService;
	//车辆类型
	@Autowired
	private CarTypeService carTypeService;
	//免费对象类别
	@Autowired
	private FreePersonService freePersonService;
	//重点救助对象类别
	@Autowired
	private HardPersonService hardPersonService;
	//车辆调度
	@Autowired
	private CarSchedulRecordService carSchedulRecordService;
	//告别厅使用记录
	@Autowired
	private FarewellRecordService farewellRecordService;
	//冷藏柜使用记录
	@Autowired
	private FreezerRecordService freezerRecordService;
	//冷藏柜使用记录
	@Autowired
	private FreezerTypeService freezerTypeService;
	//守灵室使用记录
	@Autowired
	private MourningRecordService mourningRecordService;
	//困难减免申请
	@Autowired
	private HardReductionService hardReductionService;
	//困难减免项目
	@Autowired
	private HardReductionDService hardReductionDService;
	//基本减免申请
	@Autowired
	private BaseReductionService baseReductionService;
	//基本减免项目
	@Autowired
	private BaseReductionDService baseReductionDService;
	//火化炉信息
	@Autowired
	private FurnaceService furnaceService;
	//火化炉调度
	@Autowired
	private FurnaceRecordService furnaceRecordService;
	//挂账单位
	@Autowired
	private BillUnitService billUnitService;
	//地区
	@Autowired
	private ToponymService toponymService;
	//名族
	@Autowired
	private NationService nationService;
	//接尸地址
	@Autowired
	private CorpseAddressService corpseAddService;
	//礼仪出殡
	@Autowired
	private FuneralRecordService funeralRecordService;
	//预约登记
	@Autowired
	private AppointmentService appointmentService;
	//特殊业务类型
	@Autowired
	private SpecialBusinessTypeService specialBusinessTypeService;
	//收费记录
	@Autowired
	private BusinessFeesService businessFeesService;

//----------日志--------------
	/*	//委托单
		@Autowired
		private CommissionOrderServiceLog commissionOrderServiceLog;
		//车辆调度
		@Autowired
		private CarSchedulRecordServiceLog carSchedulRecordServiceLog;
		//告别厅使用记录
		@Autowired
		private FarewellRecordServiceLog farewellRecordServiceLog;
		//冷藏柜使用记录
		@Autowired
		private FreezerRecordServiceLog freezerRecordServiceLog;
		//守灵室使用记录
		@Autowired
		private MourningRecordServiceLog mourningRecordServiceLog;
		//困难减免申请
		@Autowired
		private HardReductionServiceLog hardReductionServiceLog;
		//困难减免项目
		@Autowired
		private HardReductionDServiceLog hardReductionDServiceLog;
		//基本减免申请
		@Autowired
		private BaseReductionServiceLog baseReductionServiceLog;
		//基本减免项目
		@Autowired
		private BaseReductionDServiceLog baseReductionDServiceLog;
		//收费项目
		@Autowired
		private ItemOrderServiceLog itemOrderServiceLog;
		//火化炉调度
		@Autowired
		private FurnaceRecordServiceLog furnaceRecordServiceLog;
		private MultipartFile file;
		private FurnaceRecordServiceLog furnaceRecordServiceLog;*/
		
	
	
    @ModelAttribute  
    public void populateModel( Model model) {  
       model.addAttribute("url", "commissionOrder.do");
    }
	
	@RequestMapping
    public ModelAndView unspecified() {
		return listCommissionOrder();
    }

	/**
	 * 火化委托单列表
	 * @return
	 */
    @RequestMapping(params = "method=list")
    public ModelAndView listCommissionOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		//获得页面页码信息
		int[] pages = getPage();
		//填入查询内容
		Map<String,Object> maps=new HashMap<String, Object>();
		String isShow=getCurrentUser().getDeptName();
		if("管理部".equals(isShow)){
			maps.put("show","yes");
		}else{
			maps.put("show","no");
		}
		String isLock=getString("isLock");
		maps.put("isLock", "locked");
		if(isLock !=null &&!"".equals(isLock)){
			maps.put("isLock", "unlock");
		}else{
			maps.put("isLock", "locked");
		}
		maps.put("checkFlag", getByte("checkFlag"));
		maps.put("goto", Const.GOTO1);
		String searchType = getString("searchType");
		String searchVal = getString("searchVal");
		maps.put("searchType", searchType);
		maps.put("searchVal", searchVal);
		String searchOption=Const.getSearchTypeOptionByString(searchType,new byte[]{Const.Search_Type_Card,Const.Search_Type_Name,Const.Search_Type_IceNumber});
		PageInfo<CommissionOrder> page=commissionOrderService.getCommissionOrderPageInfo(maps,pages[0],pages[1],"update_time desc,cremation_time desc");
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("checkFlagOption", Const.getCheckFlagOption(getByte("checkFlag"),true));
		model.put("page", page);
		model.put("searchOption", searchOption);
        model.put("method", "list");
		return new ModelAndView("ye/commissionOrder/listCommissionOrder",model);
    }
    
    /**
     * 更改火化委托基本信息
     * 与冰柜、火化炉、服务项目等分开修改
     * @return
     */
    @RequestMapping(params ="method=editBase")
    public ModelAndView editBase(){
    	Map<String,Object> model=new HashMap<String,Object>();
    	String id=getString("id");
    	CommissionOrder commissionOrder=new CommissionOrder();
    	if(id!=null && !"".equals(id)){
    		commissionOrder=commissionOrderService.getCommissionOrderById(id);
    	}
    	model.put("province", commissionOrder.getProvince());
		model.put("city", commissionOrder.getCity());
		model.put("area", commissionOrder.getArea());
		//死亡原因
		String deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
		//证件类型
		String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
		//死亡类型
		String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
		//性别
		String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
		//证明单位
		String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
		//死亡证明
		String dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
		//与死者关系
		String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
		//民族
		String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);
		//接尸地址
		String corpseAddOption=corpseAddService.getCorpseAddressOption(commissionOrder.getPickAddr(), false);
		model.put("certificateOption",certificateOption);
		model.put("corpseAddOption",corpseAddOption);
		model.put("deadTypeOption",deadTypeOption);
		model.put("deadReasonOption",deadReasonOption);
		model.put("sexOption",sexOption);
		model.put("nationOption",nationOption);
		model.put("proveUnitOption",proveUnitOption);
		model.put("dFlagOption",dFlagOption);
		model.put("fAppellationOption",fAppellationOption);
		model.put("checkFlag",commissionOrder.getCheckFlag());
		model.put("Check_Yes",Const.Check_Yes);
		
//    	certificateOption
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=format.format(commissionOrder.getCreatTime());
		model.put("time",time);
    	model.put("commissionOrder", commissionOrder);
    	model.put("method", "saveBase");
    	model.put("menu", getString("menu"));
		return new ModelAndView("ye/commissionOrder/editBase",model);
    }
    
	/**
	 * 链接到修改添加页面
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(params = "method=edit")
    public ModelAndView editCommissionOrder() throws ParseException{
		Map<String,Object> model=new HashMap<String,Object>();
		String id = getString("id");
		//首页审核
		String deadReasonOption="";
		CommissionOrder commissionOrder=new CommissionOrder();
		if(id!=null&&!id.equals("")){
//			CommissionOrder com =commissionOrderService.getCommissionOrderById(id);
			/*if(com.getViewsUncheck()==0){
				com.setViewsUncheck(Const.One);
				commissionOrderService.updateNumber(com);
			}*/
//			commissionOrderService.updateNumber2(id,Const.One);
			commissionOrder=commissionOrderService.getCommissionOrderById(id);
			model.put("province", commissionOrder.getProvince());
			model.put("city", commissionOrder.getCity());
			model.put("area", commissionOrder.getArea());
			deadReasonOption=deadReasonService.getDeadReasonOption(commissionOrder.getDeadReasonId(),false);
			if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
				String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
				if(nationName != null){
					commissionOrder.setdNationId(nationName);
				}
			}
			
			//收费项目
			List<Object[]> service_list=new ArrayList<Object[]>();
			List<Object[]> articles_list=new ArrayList<Object[]>();
			Map<String, Object> maps=new HashMap<String, Object>();
			maps.put("commissionOrderId", id);
			List<ItemOrder> item_list=itemOrderService.getItemOrderList(maps);
			for (int i = 0; i < item_list.size(); i++) {
				ItemOrder itemOrder =  new ItemOrder();
				itemOrder = item_list.get(i);
				String itemId=itemOrder.getItemId();
				Item item=itemService.getItemById(itemId);
				ItemType itemType=itemTypeService.getItemTypeById(item.getTypeId());
				//服务项目
				if (itemType.getType()==Const.Type_Service) {
					Map<String, Object>smaps=new HashMap<String, Object>();
					smaps.put("typeId", item.getTypeId());
					smaps.put("type", Const.Type_Service);
					smaps.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
				}
				//丧葬用品
				if (itemType.getType()==Const.Type_Articles) {
					Map<String, Object> smaps=new HashMap<String, Object>();
					smaps.put("typeId", item.getTypeId());
					smaps.put("type", Const.Type_Articles);
					smaps.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItemOption(smaps,item.getId(),false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(smaps,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypeOption(smaps,item.getTypeId(), false);
			    	String isOption=Const.getIsOption(itemOrder.getTickFlag(), false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
				}	
			}
			model.put("service_list", service_list);
			model.put("articles_list", articles_list);
		
		}else {
			//此处是预约登记连接过来的数据
			String jsonStr = getString("appointment");
			if(jsonStr!=null&&!"".equals(jsonStr)){
				JSONObject object = JSON.parseObject(jsonStr);
				String farewellCode=(String)object.get("farewellCode");
				String farewellTime=(String)object.get("farewellTime");
				String funeralCode=(String)object.get("funeralCode");
				String funeralTime=(String)object.get("funeralTime");
				String mourningCode=(String)object.get("mourningCode");
				String mourningBeginTime=(String)object.get("mourningBeginTime");
				String mourningEndTime=(String)object.get("mourningEndTime");
				
				if(farewellCode!=null &&!"".equals(farewellCode)){
					Farewell farewell=farewellService.getFarewellByName(farewellCode);
					String farewellId=farewell.getId();
					model.put("farewellId1", farewellId);
					//以下开始判断告别厅
					Farewell fare=farewellService.getFarewellById(farewellId);
					String typeId=fare.getTypeId();
					FarewellType fareType=farewellTypeService.getFarewellTypeById(typeId);
					String typeName=fareType.getName();
					Item newItem=new Item();
					if("大厅".equals(typeName)){
						newItem=itemService.getItemByHelpCode("416");
					}else if("中厅".equals(typeName)){
						newItem=itemService.getItemByHelpCode("417");
					}else if("小厅".equals(typeName)){
						newItem=itemService.getItemByHelpCode("418");
					}
					Map<String,Object> newMaps=new HashMap<String, Object>();
					newMaps.put("typeId", newItem.getTypeId());
					newMaps.put("isdel", Const.Isdel_No);
			    	String newItemOption=itemService.getItemOption(newMaps,newItem.getId(),false);
			    	String newTypeOption=itemTypeService.getItemTypeOption(newMaps,newItem.getTypeId(), false);
			    	String newHelpCodeOption=itemService.getItemHelpCodeOption(newMaps,newItem.getId(),false);
			    	String isOption=Const.getIsOption((byte)0, false);
			    	int newFaIndex=itemTypeService.getItemTypeById(newItem.getTypeId()).getIndexFlag();
			    	int newSonIndex=newItem.getIndexFlag();
					List<Object> list=new ArrayList<Object>();
					list.add("service");
					list.add(newItemOption);
					list.add(newHelpCodeOption);
					list.add(newItem.getPice());
					list.add(newTypeOption);
					list.add(1);
					list.add(isOption);
					list.add(newItem.getPice());
					list.add(newItem.getComment());
					list.add(newFaIndex);
					list.add(newSonIndex);
					model.put("appointmentFarewell", list);
				}
				if(mourningCode!=null &&!"".equals(mourningCode)){
					Mourning mourning=mourningService.getMourningByName(mourningCode);
					String mourningId=mourning.getId();
					model.put("mourningId1", mourningId);
					//以下判断灵堂类型
					Mourning lingtang=mourningService.getMourningById(mourningId);
					String typeId=lingtang.getTypeId();
					MourningType mourningType=mourningTypeService.getMourningTypeId(typeId);
					String mType=mourningType.getName();
					Item newItem=new Item();
					if("普通".equals(mType)){
						newItem=itemService.getItemByHelpCode("501");
					}else if("豪华".equals(mType)){
						newItem=itemService.getItemByHelpCode("503");
					}else if("中高档".equals(mType)){
						newItem=itemService.getItemByHelpCode("502");
					}
					Map<String,Object> newMaps=new HashMap<String, Object>();
					newMaps.put("typeId", newItem.getTypeId());
					newMaps.put("isdel", Const.Isdel_No);
			    	String newOption=itemService.getItemOption(newMaps,newItem.getId(),false);
			    	String newTypeOption=itemTypeService.getItemTypeOption(newMaps,newItem.getTypeId(), false);
			    	String newHelpCodeOption=itemService.getItemHelpCodeOption(newMaps,newItem.getId(),false);
			    	String isOption=Const.getIsOption((byte)0, false);
			    	int newFaIndex=itemTypeService.getItemTypeById(newItem.getTypeId()).getIndexFlag();
			    	int newSonIndex=newItem.getIndexFlag();
			    	//以下选择灵堂之后,追加灵堂冷藏费(水晶棺)
			    	Item item2=new Item();
			    	item2=itemService.getItemByHelpCode("402");
			    	Map<String,Object> serviceMaps2=new HashMap<String, Object>();
			    	serviceMaps2.put("typeId", item2.getTypeId());
			    	serviceMaps2.put("isdel", Const.Isdel_No);
			    	String itemOption2=itemService.getItemOption(serviceMaps2, item2.getId(),false);
			    	String itemTypeOption2=itemTypeService.getItemTypeOption(serviceMaps2,item2.getTypeId(), false);
			    	String itemHelpCodeOption2=itemService.getItemHelpCodeOption(serviceMaps2,item2.getId(),false);
			    	int faIndex2=itemTypeService.getItemTypeById(newItem.getTypeId()).getIndexFlag();
			    	int sonIndex2=newItem.getIndexFlag();
			    	
					List<Object> list=new ArrayList<Object>();
					list.add("service");
					list.add(newOption);
					list.add(newHelpCodeOption);
					list.add(newItem.getPice());
					list.add(newTypeOption);
					list.add(1);
					list.add(isOption);
					list.add(newItem.getPice());
					list.add(newItem.getComment());
					list.add(newFaIndex);
					list.add(newSonIndex);
					//追加的冷藏收费
					List<Object> list2=new ArrayList<Object>();
					list2.add("service");
					list2.add(itemOption2);
					list2.add(itemHelpCodeOption2);
					list2.add(item2.getPice());
					list2.add(itemTypeOption2);
					list2.add(1);
					list2.add(isOption);
					list2.add(item2.getPice());
					list2.add(item2.getComment());
					list2.add(faIndex2);
					list2.add(sonIndex2);
					model.put("iceList", list2);
					model.put("appointmentMourning", list);
				}
				if(funeralCode!=null &&!"".equals(funeralCode)){
					Furnace furnace=furnaceService.getFurnaceByName(funeralCode);
					String funeralId=furnace.getId();
					model.put("funeralId1", funeralId);
				}
				model.put("farewellCode", farewellCode);
				model.put("farewellTime", farewellTime);
				model.put("funeralCode", funeralCode );
				model.put("funeralTime", funeralTime);
				model.put("mourningCode", mourningCode);
				model.put("mourningBeginTime", mourningBeginTime);
				model.put("mourningEndTime", mourningEndTime);
			}
			
			
			//以下开始正常添加火化委托单
			List<DeadType> list=deadTypeService.getDeadTypeList(null);
			if (list.size()>0) {
				DeadType deadType=list.get(0);
				deadReasonOption=deadReasonService.getDeadReasonChange(deadType.getId(),"");
			}

			//默认服务项目
			List<Object[]> serviceList=new ArrayList<Object[]>();
			Map<String,Object> serviceMaps=new HashMap<String, Object>();
			serviceMaps.put("type", Const.Type_Service);
			serviceMaps.put("defaultFlag", Const.Is_Yes);
			serviceMaps.put("isdel", Const.Isdel_No);
			List<Item> itemList=itemService.getTypeItemList(serviceMaps);
			Map<String, Object> maps1=new HashMap<String, Object>();
		    for(int i=0;i<itemList.size();i++){
		    	Item item=itemList.get(i);
		    	maps1.put("typeId", item.getTypeId());
		    	maps1.put("isdel", Const.Isdel_No);
		    	String itemOption=itemService.getItemOption(maps1,item.getId(),false);
		    	String itemTypeOption=itemTypeService.getItemTypeOption(maps1,item.getTypeId(), false);
		    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps1,item.getId(),false);
		    	String isOption=Const.getIsOption((byte)0, false);
		    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
		    	int sonIndex=item.getIndexFlag();
		    	serviceList.add(new Object[]{"service",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
		    }
			model.put("serviceList", serviceList);
			
			
			//默认丧葬用品
			List<Object[]> articlesList=new ArrayList<Object[]>();
			Map<String, Object> articlesMaps=new HashMap<String, Object>();
			articlesMaps.put("type", Const.Type_Articles);
			articlesMaps.put("defaultFlag", Const.Is_Yes);
			articlesMaps.put("isdel", Const.Isdel_No);
			List<Item> list1=itemService.getTypeItemList(articlesMaps);
			Map<String, Object> maps2=new HashMap<String, Object>();
		    for(int i=0;i<list1.size();i++){
		    	Item item=list1.get(i);
		    	maps2.put("typeId", item.getTypeId());
		    	maps2.put("isdel", Const.Isdel_No);
		    	String itemOption=itemService.getItemOption(maps2,item.getId(),false);
		    	String itemTypeOption=itemTypeService.getItemTypeOption(articlesMaps,item.getTypeId(), false);
		    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(articlesMaps,item.getId(),false);
		    	String isOption=Const.getIsOption((byte)0, false);
		    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
		    	int sonIndex=item.getIndexFlag();
		    	articlesList.add(new Object[]{"articles",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment(),faIndex,sonIndex});
		    }
			model.put("articlesList", articlesList);
		}
		String certificateOption=certificateService.getCertificateOption(commissionOrder.getCertificateId(),false);
		String deadTypeOption=deadTypeService.getDeadTypeOption(commissionOrder.getDeadTypeId(),false);
		String sexOption=Const.getSexOption(commissionOrder.getSex(),false);
		String proveUnitOption=proveUnitService.getProveUnitOption(commissionOrder.getProveUnitId(),false);
		String dFlagOption="";
		if(id!=null &&!"".equals(id)){
			dFlagOption=Const.getIsHaveOption(commissionOrder.getdFlag(), false);
		}else{
			dFlagOption=Const.getIsHaveOption(Const.Is_No, false);//默认未收死亡证明
		}
		String fAppellationOption=appellationService.getAppellationOption(commissionOrder.getfAppellationId(),false);
		String corpseUnitOption=corpseUnitService.getCorpseUnitOption(commissionOrder.getCorpseUnitId(),false);
		//String registerOption=registerService.getRegisterOption(commissionOrder.getRegisterId(), false);
		String toponymOption=toponymService.getToponymOption(commissionOrder.getToponymId(),false);
		//挂账单位
		String billUnitOption=billUnitService.getBillUnitOption(commissionOrder.getBillUnitId(), true);
		//民族
		String nationOption = nationService.getNationOption(commissionOrder.getdNationId(), false);
		//接尸地址
		String corpseAddOption=corpseAddService.getCorpseAddressOption(commissionOrder.getPickAddr(), false);
		//特殊业务类型
		String specialBusinessOption=specialBusinessTypeService.getSpecialBusinessTypeOption(commissionOrder.getFurnaceComment(), false);
		//基本减免
		List<Object[]> blist=new ArrayList<Object[]>();
		List<FreePerson> freePersonList=freePersonService.getFreePersonList(null);
		List<HardPerson> hardPersonList=hardPersonService.getHardPersonList(null);
		Item baseItem=new Item();
		Map<String, Object>baseMaps=new HashMap<String, Object>();
		baseMaps.put("baseFlag", Const.Is_Yes);
		baseMaps.put("isdel", Const.Isdel_No);
		Map<String, Object>baseMaps2=new HashMap<String, Object>();
		baseMaps2.put("isdel", Const.Isdel_No);
		String baseItemOption=itemService.getItemOption(baseMaps2, "", false);
		String baseItemHelpCodeOption=itemService.getItemHelpCodeOption(baseMaps2, "", false);
		List<Item> baselist=itemService.getItemList(baseMaps);
		if (baselist.size()>0) {
			baseItem=baselist.get(0);
		}
		model.put("freePersonList", freePersonList);
		model.put("hardPersonList", hardPersonList);
		model.put("baseItemHelpCodeOption", baseItemHelpCodeOption);
		model.put("baseItemOption", baseItemOption);
		model.put("baseItem", baseItem);
		for (int i = 0; i < baselist.size(); i++) {
			Item item=baselist.get(i);
			
//			String baseItemOption=itemService.getItemOption(baseMaps, "", false);
//			String baseItemHelpCodeOption=itemService.getItemHelpCodeOption(baseMaps, "", false);
			
			String base_itemOption=itemService.getItemOption(baseMaps2, item.getId(), false);
			String base_itemHelpCodeOption=itemService.getItemHelpCodeOption(baseMaps2, item.getId(), false);
			int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
			blist.add(new Object[]{"base","",base_itemOption,base_itemHelpCodeOption,1,item.getPice(),"",faIndex,sonIndex});
		}
		
		model.put("baselist", blist);
		//困难减免
		List<Object[]> hlist=new ArrayList<Object[]>();
		List<Certificate> certificateList=certificateService.getCertificateList();
		Item hardItem=new Item();
		//此处应客户需要更改不再限制困难减免的项目
		Map<String, Object>hardMaps=new HashMap<String, Object>();
		hardMaps.put("hardFlag", Const.Is_Yes);
		hardMaps.put("isdel", Const.Isdel_No);
		Map<String, Object>hardmap=new HashMap<String, Object>();
		hardmap.put("isdel", Const.Isdel_No);
//		String hardItemOption=itemService.getItemOption(hardMaps, "", false);
		String hardItemOption=itemService.getItemOption(null, "", false);
		String hardItemHelpCodeOption=itemService.getItemHelpCodeOption(null, "", false);
		List<Item> hardlist=itemService.getItemList(hardMaps);
		if (hardlist.size()>0) {
			 hardItem=hardlist.get(0);
		}
		model.put("certificateList", certificateList);
		model.put("hardItemOption", hardItemOption);
		model.put("hardItemHelpCodeOption", hardItemHelpCodeOption);
		model.put("hardItem", hardItem);
		for (int i = 0; i < hardlist.size(); i++) {
			Item item=(Item) hardlist.get(i);
			String hard_itemOption=itemService.getItemOption(hardmap, item.getId(), false);
			String hard_itemHelpCodeOption=itemService.getItemHelpCodeOption(hardmap, item.getId(), false);
			int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
			hlist.add(new Object[]{"hard","",hard_itemOption,hard_itemHelpCodeOption,1,item.getPice(),"",faIndex,sonIndex});
		}
		model.put("hardlist", hlist);
		
		
		//减免项目
		byte base_Is=Const.Is_No;
		byte hard_Is=Const.Is_No;
		byte special_Is=Const.Is_No;
		String checked="checked=checked";
		BaseReduction baseReduction=new BaseReduction();
		HardReduction hardReduction=new HardReduction();
		List<Object[]> base_list=new ArrayList<Object[]>();//基本减免项目
		List<Object[]> hard_list=new ArrayList<Object[]>();//困难减免项目
		List<Object[]> prove_list=new ArrayList<Object[]>();//证件类型
		List<Object[]> freePerson_list=new ArrayList<Object[]>();//免费对象类别
		List<Object[]> hardPerson_list=new ArrayList<Object[]>();//重点救助对象类别
		String hard_proveUnitOption=proveUnitService.getProveUnitOption("",false);//困难减免证明单位
		String hard_appellationOption=appellationService.getAppellationOption("",false);//困难减免称谓
		boolean isFree=false;//判断是否有免费对象类型,初始没有,则在前端默认居民选中.
		if(id!=null&&!id.equals("")){
			//基本减免
			Map<String, Object>maps=new HashMap<String, Object>();
			Map<String, Object>bMaps=new HashMap<String, Object>();
			List<BaseReductionD> baseList=new ArrayList<BaseReductionD>();
			maps.put("commissionOrderId", id);
			List<BaseReduction> bList=baseReductionService.getBaseReductionList(maps);
			if (bList.size()>0) {
				base_Is=Const.Is_Yes;
				baseReduction=(BaseReduction) bList.get(0);
				bMaps.put("baseReductionId", baseReduction.getId());
				baseList=baseReductionDService.getBaseReductionDList(bMaps);
			}
			for (int i = 0; i < baseList.size(); i++) {
				BaseReductionD baseReductionD=(BaseReductionD) baseList.get(i);
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("isdel", Const.Isdel_No);
				String baseItem_option=itemService.getItemOption(bMaps1, baseReductionD.getItemId(), false);
				String baseItemHelpCode_option=itemService.getItemHelpCodeOption(bMaps1, baseReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(baseReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(baseReductionD.getItemId()).getIndexFlag();
				base_list.add(new Object[]{"base",baseReductionD.getId(),baseItem_option,baseItemHelpCode_option,baseReductionD.getNumber(),baseReductionD.getTotal(),baseReductionD.getComment(),faIndex,sonIndex});
			}
			
			//免费对象类别
			List<FreePerson> freePersonList2=freePersonService.getFreePersonList(null);
			String[] free=null;
			if (baseReduction.getFreePersonIds()!=null&&!baseReduction.getFreePersonIds().equals("")) {
				free=baseReduction.getFreePersonIds().split(",");
			}
			
			for (int i = 0; i < freePersonList2.size(); i++) {
				boolean bool=true;
				FreePerson freePerson=(FreePerson) freePersonList2.get(i);
				if (free!=null) {
					for (int j = 0; j < free.length; j++) {
						if (free[j]==freePerson.getId()||free[j].equals(freePerson.getId())) {
							freePerson_list.add(new Object[]{"freePersonId",checked,freePerson.getId(),freePerson.getName()});
							bool=false;
							isFree=true;
						}
					}
				}
				if (bool) {
					freePerson_list.add(new Object[]{"freePersonId","",freePerson.getId(),freePerson.getName()});
				}
			}
			
			//重点救助对象类别
			List<HardPerson> hardPersonList2=hardPersonService.getHardPersonList(null);
			String[] hard=null;
			if (baseReduction.getHardPersonIds()!=null&&!baseReduction.getHardPersonIds().equals("")) {
				hard=baseReduction.getHardPersonIds().split(",");
			}
			for (int i = 0; i < hardPersonList2.size(); i++) {
				boolean bool=true;
				HardPerson hardPerson=(HardPerson) hardPersonList2.get(i);
				if (hard!=null) {
					for (int j = 0; j < hard.length; j++) {
						if (hard[j]==hardPerson.getId()||hard[j].equals(hardPerson.getId())) {
							hardPerson_list.add(new Object[]{"hardPersonId",checked,hardPerson.getId(),hardPerson.getName()});
							bool=false;
						}
					}
				}
				if (bool) {
					hardPerson_list.add(new Object[]{"hardPersonId","",hardPerson.getId(),hardPerson.getName()});
				}
			}
			//困难减免
			List<HardReductionD> hardList=new ArrayList<HardReductionD>();
			Map<String, Object>hMaps=new HashMap<String, Object>();
			hMaps.put("commissionOrderId", id);
			List<HardReduction> hList=hardReductionService.getHardReductionList(hMaps);
			if (hList.size()>0) {
				hard_Is=Const.Is_Yes;
				hardReduction= hList.get(0);
				hMaps.put("hardReductionId", hardReduction.getId());
				hardList=hardReductionDService.getHardReductionDList(hMaps);
				special_Is=hardReduction.getSpecial();
			}
			for (int i = 0; i < hardList.size(); i++) {
				HardReductionD hardReductionD=(HardReductionD) hardList.get(i);
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("isdel", Const.Isdel_No);
//				String hardItem_option=itemService.getItemOption(bMaps1, hardReductionD.getItemId(), false);
//				String hardItemHelpCode_option=itemService.getItemHelpCodeOption(bMaps1, hardReductionD.getId(), false);
				String hardItem_option=itemService.getItemOption(bMaps1, hardReductionD.getItemId(), false);
				String hardItemHelpCode_option=itemService.getItemHelpCodeOption(bMaps1, hardReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(hardReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(hardReductionD.getItemId()).getIndexFlag();
				hard_list.add(new Object[]{"hard",hardReductionD.getId(),hardItem_option,hardItemHelpCode_option,hardReductionD.getNumber(),hardReductionD.getTotal(),hardReductionD.getComment(),faIndex,sonIndex});
			}
			//证件类型
			String[] prove=null;
			if (hardReduction.getProveIds()!=null&&!hardReduction.getProveIds().equals("")) {
				
				prove=hardReduction.getProveIds().split(",");
			}
			for (int i = 0; i < certificateList.size(); i++) {
				boolean bool=true;
				Certificate certificate=(Certificate) certificateList.get(i);
				if (prove!=null) {
					for (int j = 0; j < prove.length; j++) {
						if (prove[j]==certificate.getCertificateId()||prove[j].equals(certificate.getCertificateId())) {
							prove_list.add(new Object[]{"proveIds",checked,certificate.getCertificateId(),certificate.getName()});
							bool=false;
						}
					}
				}
				if (bool) {
					prove_list.add(new Object[]{"proveIds","",certificate.getCertificateId(),certificate.getName()});
				}
			}
			hard_proveUnitOption=proveUnitService.getProveUnitOption(hardReduction.getProveComment(),false);//困难减免证明单位
			hard_appellationOption=appellationService.getAppellationOption(hardReduction.getAppellationId(),false);//困难减免称谓
			
			
		}
		model.put("isFree",isFree);
		model.put("baseReduction", baseReduction);
		model.put("hard_proveUnitOption", hard_proveUnitOption);
		model.put("hard_appellationOption", hard_appellationOption);
		model.put("hardReduction", hardReduction);
		model.put("hard_list", hard_list);
		model.put("base_list", base_list);
		model.put("prove_list", prove_list);
		model.put("freePerson_list", freePerson_list);
		model.put("hardPerson_list", hardPerson_list);
		model.put("base_Is", base_Is);
		model.put("hard_Is", hard_Is);
		model.put("furnace_ty", Const.furnace_ty);
		model.put("baseIsOption", Const.getIsOption(base_Is, false));
		model.put("hardIsOption", Const.getIsOption(hard_Is, false));
		model.put("specialOption", Const.getIsOption(special_Is, false));
		//车辆调度
		String carTypeOption=carTypeService.getCarTypeOption("", false);
		String transportTypeOption=transportTypeService.getTransportTypeOption("", false);
		model.put("carTypeOption", carTypeOption);
		model.put("transportTypeOption", transportTypeOption);
		if(id!=null&&!id.equals("")){
			Map<String,Object> carMaps=new HashMap<String, Object>();
			carMaps.put("commissionOrderId", id);
			List<CarSchedulRecord> carSchedulRecordList=new ArrayList<CarSchedulRecord>();
			List<Object[]> car_List=new ArrayList<Object[]>();
			carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
			for (int i = 0; i < carSchedulRecordList.size(); i++) {
				CarSchedulRecord carSchedulRecord=carSchedulRecordList.get(i);
				String carType_option=carTypeService.getCarTypeOption(carSchedulRecord.getCarTypeId(), false);
				String transportType_option=transportTypeService.getTransportTypeOption(carSchedulRecord.getTransportTypeId(), false);
				Timestamp pickTi=carSchedulRecord.getPickTime();
				String pickTime = "";
				if (pickTi != null) {
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					pickTime=sdf.format(pickTi);
				}
				car_List.add(new Object[]{carSchedulRecord.getId(),transportType_option,carType_option,pickTime,carSchedulRecord.getComment()});
			}
			model.put("car_List", car_List);
		}
		
		//火化炉
		List<Object[]> furnaceList=Const.getFurnaceList();
		model.put("furnaceList", furnaceList);
		//普通炉添加收费项目
		Item item=itemService.getItemByHelpCode("301");	    	
		Map<String,Object> serviceMaps=new HashMap<String, Object>();
		serviceMaps.put("typeId", item.getTypeId());
		serviceMaps.put("isdel", Const.Isdel_No);
    	String itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	String isOption=Const.getIsOption((byte)0, false);
    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
    	int sonIndex=item.getIndexFlag();
		List<Object> funList=new ArrayList<Object>();
		funList.add("service");
		funList.add(itemOption);
		funList.add(itemHelpCodeOption);
		funList.add(item.getPice());
		funList.add(itemTypeOption);
		funList.add(1);
		funList.add(isOption);
		funList.add(item.getPice());
		funList.add(item.getComment());
		funList.add(faIndex);
		funList.add(sonIndex);
		model.put("funList", funList);
		
		//特约炉添加收费项目
		item=itemService.getItemByHelpCode("420");	    	
    	itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	isOption=Const.getIsOption((byte)0, false);
    	
		List<Object> funListTY=new ArrayList<Object>();
		funListTY.add("service");
		funListTY.add(itemOption);
		funListTY.add(itemHelpCodeOption);
		funListTY.add(item.getPice());
		funListTY.add(itemTypeOption);
		funListTY.add(1);
		funListTY.add(isOption);
		funListTY.add(item.getPice());
		funListTY.add(item.getComment());
		funListTY.add(faIndex);
		funListTY.add(sonIndex);
		model.put("funListTY", funListTY);
		
		
		if (id != null && !id.equals("")) {
			//告别厅使用记录
			Map<String, Object>farewellMaps=new HashMap<String, Object>();
			farewellMaps.put("commissionOrderId", id);
			List<FarewellRecord>farewellRecordList=new  ArrayList<FarewellRecord>();
			FarewellRecord farewellRecord=new FarewellRecord();
			farewellRecordList=farewellRecordService.getFarewellRecordList(farewellMaps ,"id asc");
			if (farewellRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					farewellRecord=(FarewellRecord) farewellRecordList.get(0);
				}
			}
			model.put("farewellRecord", farewellRecord);
			//冷藏柜使用记录
			Map<String, Object>freezerMaps=new HashMap<String, Object>();
			freezerMaps.put("commissionOrderId", id);
			List<FreezerRecord> freezerRecordList=new ArrayList<FreezerRecord>();
			FreezerRecord freezerRecord=new FreezerRecord();
			freezerRecordList=freezerRecordService.getFreezerRecordList(freezerMaps);
			if (freezerRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					freezerRecord=(FreezerRecord) freezerRecordList.get(0);
				}
			}
			model.put("freezerRecord", freezerRecord);
			//守灵室使用记录
			Map<String, Object>mourningMaps=new HashMap<String, Object>();
			mourningMaps.put("commissionOrderId", id);
			List<MourningRecord> mourningRecordList=new  ArrayList<MourningRecord>();
			MourningRecord mourningRecord=new MourningRecord();
			mourningRecordList=mourningRecordService.getMourningRecordList(mourningMaps,"id asc");
			if (mourningRecordList.size()>0) {
				if(id!=null&&!id.equals("")){
					mourningRecord=(MourningRecord) mourningRecordList.get(0);
				}
			}
			model.put("mourningRecord", mourningRecord);
			//火化记录
			FurnaceRecord furnaceRecord = new FurnaceRecord();
			Map<String, Object>furnaceRecordMaps=new HashMap<String, Object>();
			furnaceRecordMaps.put("commissionOrderId", id);
			furnaceRecord = furnaceService.getFurnaceRecord(furnaceRecordMaps);
			model.put("furnaceRecord", furnaceRecord);
			List<Object[]> furnace_list = new ArrayList<Object[]>();
			for (int i = 0; i < furnaceList.size(); i++) {
				Object[] ob = furnaceList.get(i);
				byte by = (Byte) ob[0];
				if (by == commissionOrder.getFurnaceFlag()) {
					furnace_list.add(new Object[] { ob[0], ob[1], checked });
				} else {
					furnace_list.add(new Object[] { ob[0], ob[1], "" });
				}
			}
			model.put("furnace_list", furnace_list);
		} 
		byte checkFlag=commissionOrder.getCheckFlag();
		model.put("checkFlag", checkFlag);
		//model.put("registerOption", registerOption);
		model.put("toponymOption", toponymOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("fAppellationOption", fAppellationOption);
		model.put("dFlagOption", dFlagOption);
		model.put("proveUnitOption", proveUnitOption);
		model.put("sexOption", sexOption);
		model.put("deadReasonOption", deadReasonOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("certificateOption", certificateOption);
		model.put("commissionOrder", commissionOrder);
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("id", id);
		model.put("billUnitOption", billUnitOption);
		model.put("specialBusinessOption", specialBusinessOption);
		model.put("nationOption", nationOption);
		model.put("corpseAddOption", corpseAddOption);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("isOption", Const.getIsOption((byte)0, false));
		model.put("agentUser", getCurrentUser().getName());
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time;
		if(id!=null && !"".equals(id)){
			time=format.format(commissionOrder.getCreatTime());
		}else{
			time=format.format(date);
		}
		Timestamp now=new Timestamp(System.currentTimeMillis());
		model.put("now", format.format(now));
		model.put("time",time);
        model.put("method", "save");
		return new ModelAndView("ye/commissionOrder/editCommissionOrder",model);
    }
	
	/**
	 * 新加，业务视图界面，用于查看
	 * @return
	 */
	@RequestMapping(params = "method=businessView")
	public ModelAndView businessView(){
		Map<String,Object> model=new HashMap<String,Object>();
		String id=getString("id");
		CommissionOrder commissionOrder=new CommissionOrder();
		if(!"".equals(id) && id!=null){
			commissionOrder=commissionOrderService.getCommissionOrderById(id);
		}
		model.put("order", commissionOrder);
		
		//收费信息
		List<BusinessFees> businessList=businessFeesService.getBusinessFeesByComId(id);
		model.put("businessList", businessList);
		//减免信息
		Map<String,Object> baseMap=new HashMap<String,Object>();
		baseMap.put("commissionOrderId", id);
		List<BaseReductionD> baseList=baseReductionDService.getBaseReductionDFList(baseMap);
		List<HardReductionD> hardList=hardReductionDService.getHardReductionDFeeList(baseMap);
		model.put("baseList", baseList);
		model.put("hardList", hardList);
		double baseTotal=baseReductionDService.getBaseTotal(baseMap);
		double hardTotal=hardReductionDService.getHardTotal(baseMap);
		//收费金额  未收费总金额
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("commissionOrderId", id);
		paramMap.put("payFlag", Const.Pay_No);
		paramMap.put("tickFlag", Const.Is_No);
		double sumTotal=itemOrderService.getTotalByOrderNumber(paramMap);
//		paramMap.put("groupByCommissionOrder", "按委托单分组");
		
		List<ItemOrder> itemList=itemOrderService.getItemOrderList(paramMap);
		if(itemList.size()>0){
			model.put("ifPay", true);
		}
		model.put("sumTotal", sumTotal);
		model.put("baseTotal", baseTotal);
		model.put("hardTotal", hardTotal);
		//车辆信息
		List<CarSchedulRecord> carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(baseMap,"pick_time asc");
//		List<Object[]> car_List=new ArrayList<Object[]>();
//		carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
		model.put("carList", carSchedulRecordList);
		//冰柜信息
		FreezerRecord freezer=freezerRecordService.getFreezerRecordByCoId(id);
		model.put("freezer", freezer);
		return new ModelAndView("ye/commissionOrder/businessView",model);
	}
	/**
	 * 链接到火化委托单页面（只读页面）
	 * @return
	 */
	@RequestMapping(params = "method=readOnly")
    public ModelAndView editCommissionOrderReadOnly(){
		Map<String,Object> model=new HashMap<String,Object>();
		String styleType=getString("styleType");
		String adr=getString("adr");
		String id = getString("id");
		model.put("adr", adr);
		//首页未审核更改查看状态
		if("D".equals(adr)){
			commissionOrderService.updateNumber2(id,Const.One);
		}
		//首页礼仪出殡更改查看状态
		String upItemId=getString("itemId");
		if(upItemId!=null&&!"".equals(upItemId)){
			funeralRecordService.updateViewsFuneral(upItemId,Const.One);
		}
		String ifCheck=getString("ifCheck");//此处变量决定是否需要审核
		String deadReasonOption="";
		CommissionOrder commissionOrder=new CommissionOrder();
		if(id!=null&&!id.equals("")){
			if("B".equals(adr)){
				commissionOrder=commissionOrderService.getComByIdNewAndOld(id);
			}else{
				commissionOrder=commissionOrderService.getCommissionOrderById(id);
			}
			String deadArea = commissionOrder.getProvince() + " " + commissionOrder.getCity() + " " + commissionOrder.getArea();
			model.put("payFlag", commissionOrder.getPayFlag());
			model.put("deadArea", deadArea);
			deadReasonOption=deadReasonService.getDeadReason(commissionOrder.getDeadReasonId(),false);
			if(commissionOrder.getdNationId()!=null && commissionOrder.getdNationId()!=""){
				String nationName = nationService.getNationNameById(commissionOrder.getdNationId());
				if(nationName != null){
					commissionOrder.setdNationId(nationName);
				}
			}
			//收费项目
			List<Object[]> service_list=new ArrayList<Object[]>();
			List<Object[]> articles_list=new ArrayList<Object[]>();
			Map<String, Object>maps=new HashMap<String, Object>();
			maps.put("commissionOrderId", id);
			List<ItemOrder> item_list=null;
			if("B".equals(adr)){
				item_list=itemOrderService.getItemOrderOldAndNew(maps);
			}else{
				item_list=itemOrderService.getItemOrderList(maps);
			}
//			List<ItemOrder> item_list=itemOrderService.getItemOrderOldAndNew(maps);
			for (int i = 0; i < item_list.size(); i++) {
				ItemOrder itemOrder=item_list.get(i);
				String itemId=itemOrder.getItemId();
				Item item=itemService.getItemById(itemId);
				ItemType itemType=itemTypeService.getItemTypeById(item.getTypeId());
				//服务项目
				if (itemType.getType()==Const.Type_Service) {
					Map<String, Object>smaps=new HashMap<String, Object>();
					smaps.put("typeId", item.getTypeId());
					smaps.put("type", Const.Type_Service);
//					smaps.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItem(smaps,item.getId(),false);
			    	String itemHelpCodeOption=itemService.getItemHelpCode(smaps,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemType(smaps,item.getTypeId(), false);
			    	String isOption=Const.getIs(itemOrder.getTickFlag(), false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	service_list.add(new Object[]{"service",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
				   
				}
				//丧葬用品
				if (itemType.getType()==Const.Type_Articles) {
					Map<String, Object>smaps=new HashMap<String, Object>();
					smaps.put("typeId", item.getTypeId());
					smaps.put("type", Const.Type_Articles);
//					smaps.put("isdel", Const.Isdel_No);
			    	String itemOption=itemService.getItems(smaps,item.getId(),false);
			    	String itemHelpCodeOption=itemService.getItemHelpCodes(smaps,item.getId(),false);
			    	String itemTypeOption=itemTypeService.getItemTypes(smaps,item.getTypeId(), false);
			    	String isOption=Const.getIs(itemOrder.getTickFlag(), false);
			    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
			    	int sonIndex=item.getIndexFlag();
			    	articles_list.add(new Object[]{"articles",itemOrder.getId(),itemOption,itemHelpCodeOption,itemOrder.getPice(),itemTypeOption,itemOrder.getNumber(),isOption,itemOrder.getTotal(),itemOrder.getComment(),faIndex,sonIndex});
				}	
			}
			model.put("service_list", service_list);
			model.put("articles_list", articles_list);
		
		}
		String certificateOption=certificateService.getCertificate(commissionOrder.getCertificateId(),false);
		String deadTypeOption=deadTypeService.getDeadType(commissionOrder.getDeadTypeId(),false);
		String sexOption=Const.getSex(commissionOrder.getSex(),false);
		String proveUnitOption=proveUnitService.getProveUnit(commissionOrder.getProveUnitId(),false);
		String dFlagOption=Const.getIsHave(commissionOrder.getdFlag(), false);
		String fAppellationOption=appellationService.getAppellation(commissionOrder.getfAppellationId(),false);
		String corpseUnitOption=corpseUnitService.getCorpseUnit(commissionOrder.getCorpseUnitId(),false);
		String registerOption=registerService.getRegister(commissionOrder.getRegisterId(), false);
		String toponymOption=toponymService.getToponym(commissionOrder.getToponymId(),false);
		//挂账单位
		String billUnitOption=billUnitService.getBillUnit(commissionOrder.getBillUnitId(), false);
		List<Certificate> certificateList=certificateService.getCertificateList();
		//减免项目
		byte base_Is=Const.Is_No;
		byte hard_Is=Const.Is_No;
		byte special_Is=Const.Is_No;
		String checked="checked=checked";
		BaseReduction baseReduction=new BaseReduction();
		HardReduction hardReduction=new HardReduction();
		List<Object[]> base_list=new ArrayList<Object[]>();//基本减免项目
		List<Object[]> hard_list=new ArrayList<Object[]>();//困难减免项目
		List<Object[]> prove_list=new ArrayList<Object[]>();//证件类型
		List<Object[]> freePerson_list=new ArrayList<Object[]>();//免费对象类别
		List<Object[]> hardPerson_list=new ArrayList<Object[]>();//重点救助对象类别
		String hard_proveUnitOption=proveUnitService.getProveUnits("",false);//困难减免证明单位
		String hard_appellationOption=appellationService.getAppellations("",false);//困难减免称谓
		String Cername=null;
		String Free=null;
		String Weightp=null;
		if(id!=null&&!id.equals("")){
			//基本减免
			Map<String, Object>maps=new HashMap<String, Object>();
			Map<String, Object>bMaps=new HashMap<String, Object>();
			List<BaseReductionD> baseList=new ArrayList<BaseReductionD>();
			maps.put("commissionOrderId", id);
			List<BaseReduction> bList=baseReductionService.getBaseReductionList(maps);
			if (bList.size()>0) {
				base_Is=Const.Is_Yes;
				baseReduction=(BaseReduction) bList.get(0);
				bMaps.put("baseReductionId", baseReduction.getId());
				baseList=baseReductionDService.getBaseReductionDList(bMaps);
			}
			for (int i = 0; i < baseList.size(); i++) {
				BaseReductionD baseReductionD=(BaseReductionD) baseList.get(i);
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("baseFlag", Const.Is_Yes);
				String baseItem_option=itemService.getItems(null, baseReductionD.getItemId(), false);
				String baseItemHelpCode_option=itemService.getItemHelpCodes(null, baseReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(baseReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(baseReductionD.getItemId()).getIndexFlag();
				base_list.add(new Object[]{"base",baseReductionD.getId(),baseItem_option,baseItemHelpCode_option,baseReductionD.getNumber(),baseReductionD.getTotal(),baseReductionD.getComment(),faIndex,sonIndex,baseReductionD.getPice()});
			}
			
			//免费对象类别
			List<FreePerson> freePersonList2=freePersonService.getFreePersonList(null);
			String[] free=null;
			if (baseReduction.getFreePersonIds()!=null&&!baseReduction.getFreePersonIds().equals("")) {
				free=baseReduction.getFreePersonIds().split(",");
			}
			for (int i = 0; i < freePersonList2.size(); i++) {
//				boolean bool=true;
				FreePerson freePerson=(FreePerson) freePersonList2.get(i);
				if (free!=null) {
					for (int j = 0; j < free.length; j++) {
						if (free[j]==freePerson.getId()||free[j].equals(freePerson.getId())) {
							freePerson_list.add(new Object[]{"freePersonId",checked,freePerson.getId(),freePerson.getName()});
							Free=freePerson.getName();
//							bool=false;
						}
					}
				}
//				if (bool) {
//					freePerson_list.add(new Object[]{"freePersonId","",freePerson.getId(),freePerson.getName()});
//				}
			}
			//重点救助对象类别
			List<HardPerson> hardPersonList2=hardPersonService.getHardPersonList(null);
			String[] hard=null;
			if (baseReduction.getHardPersonIds()!=null&&!baseReduction.getHardPersonIds().equals("")) {
				hard=baseReduction.getHardPersonIds().split(",");
			}
			for (int i = 0; i < hardPersonList2.size(); i++) {
//				boolean bool=true;
				HardPerson hardPerson= hardPersonList2.get(i);
				if (hard!=null) {
					for (int j = 0; j < hard.length; j++) {
						if (hard[j]==hardPerson.getId()||hard[j].equals(hardPerson.getId())) {
							hardPerson_list.add(new Object[]{"hardPersonId",checked,hardPerson.getId(),hardPerson.getName()});
							Weightp=hardPerson.getName();
//							bool=false;
						}
					}
				}
//				if (bool) {
//					hardPerson_list.add(new Object[]{"hardPersonId","",hardPerson.getId(),hardPerson.getName()});
//				}
			}
			
			
			
			//困难减免
			
			List<HardReductionD> hardList=new ArrayList<HardReductionD>();
			Map<String, Object>hMaps=new HashMap<String, Object>();
			hMaps.put("commissionOrderId", id);
			List<HardReduction> hList=hardReductionService.getHardReductionList(hMaps);
			if (hList.size()>0) {
				hard_Is=Const.Is_Yes;
				hardReduction=hList.get(0);
				hMaps.put("hardReductionId", hardReduction.getId());
				hardList=hardReductionDService.getHardReductionDList(hMaps);
				special_Is=hardReduction.getSpecial();
				byte hardCheckIs=hardReduction.getCheckFlag();
				model.put("hardCheckIs", hardCheckIs);
			}
			double hardTotal=0.0;//这里记录困难减免的总金额,用于 审核时判断是否可以审核
			for (int i = 0; i < hardList.size(); i++) {
				HardReductionD hardReductionD=hardList.get(i);
				hardTotal+=hardReductionD.getTotal();
				Map<String, Object>bMaps1=new HashMap<String, Object>();
				bMaps1.put("isdel", Const.Isdel_No);
				String hardItem_option=itemService.getItems(bMaps1, hardReductionD.getItemId(), false);
				String hardItemHelpCode_option=itemService.getItemHelpCodess(bMaps1, hardReductionD.getItemId(), false);
				int faIndex=itemTypeService.getItemTypeById(itemService.getItemById(hardReductionD.getItemId()).getTypeId()).getIndexFlag();
		    	int sonIndex=itemService.getItemById(hardReductionD.getItemId()).getIndexFlag();
				hard_list.add(new Object[]{"hard",hardReductionD.getId(),hardItem_option,hardItemHelpCode_option,hardReductionD.getNumber(),hardReductionD.getTotal(),hardReductionD.getComment(),faIndex,sonIndex,hardReductionD.getPice()});
			}
			model.put("hardTotal", hardTotal);
			//证件类型
			String[] prove=null;
			
			if (hardReduction.getProveIds()!=null&&!hardReduction.getProveIds().equals("")) {
				
				prove=hardReduction.getProveIds().split(",");
			}
			for (int i = 0; i < certificateList.size(); i++) {
//				boolean bool=true;
				Certificate certificate=(Certificate) certificateList.get(i);
				if (prove!=null) {
					for (int j = 0; j < prove.length; j++) {
						if (prove[j]==certificate.getCertificateId()||prove[j].equals(certificate.getCertificateId())) {
							prove_list.add(new Object[]{"proveIds",checked,certificate.getCertificateId(),certificate.getName()});
							Cername=certificate.getName();
//							bool=false;
						}
					}
				}
//				if (bool) {
//					prove_list.add(new Object[]{"proveIds","",certificate.getCertificateId(),certificate.getName()});
//				}
			}
			hard_proveUnitOption=proveUnitService.getProveUnits(hardReduction.getProveComment(),false);//困难减免证明单位
			hard_appellationOption=appellationService.getAppellations(hardReduction.getAppellationId(),false);//困难减免称谓
		}
		
		model.put("hard", charge(hard_Is));
		model.put("base", charge(base_Is));
		model.put("special",charge(special_Is));
		model.put("Weightp", Weightp);
		model.put("Free", Free);
		model.put("Cname", Cername);
		model.put("baseReduction", baseReduction);
		model.put("hard_proveUnitOption", hard_proveUnitOption);
		model.put("hard_appellationOption", hard_appellationOption);
		model.put("hardReduction", hardReduction);
		model.put("hard_list", hard_list);
		model.put("base_list", base_list);
		model.put("prove_list", prove_list);
		model.put("freePerson_list", freePerson_list);
		model.put("hardPerson_list", hardPerson_list);
		model.put("base_Is", base_Is);
		model.put("hard_Is", hard_Is);
		model.put("special_Is", special_Is);
		model.put("furnace_ty", Const.furnace_ty);
		model.put("baseIsOption", Const.getIsOption(base_Is, false));
		model.put("hardIsOption", Const.getIsOption(hard_Is, false));
		model.put("specialOption", Const.getIsOption(special_Is, false));
		
		
		
		
		//车辆调度
		String carTypeOption=carTypeService.getCarTypeOption("", false);
		String transportTypeOption=transportTypeService.getTransportTypeOption("", false);
		model.put("carTypeOption", carTypeOption);
		model.put("transportTypeOption", transportTypeOption);
		if(id!=null&&!id.equals("")){
			Map<String,Object> carMaps=new HashMap<String, Object>();
			carMaps.put("commissionOrderId", id);
			List<CarSchedulRecord> carSchedulRecordList=new ArrayList<CarSchedulRecord>();
			List<Object[]> car_List=new ArrayList<Object[]>();
			carSchedulRecordList=carSchedulRecordService.getCarSchedulRecordList(carMaps,"pick_time asc");
			for (int i = 0; i < carSchedulRecordList.size(); i++) {
				CarSchedulRecord carSchedulRecord=carSchedulRecordList.get(i);
				String carType_option=carTypeService.getCarTypes(carSchedulRecord.getCarTypeId(), false);
				String transportType_option=transportTypeService.getTransportTypes(carSchedulRecord.getTransportTypeId(), false);
				Timestamp pickTi=carSchedulRecord.getPickTime();
				String pickTime = "";
				if (pickTi != null) {
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					pickTime=sdf.format(pickTi);
				}
				car_List.add(new Object[]{carSchedulRecord.getId(),transportType_option,carType_option,pickTime,carSchedulRecord.getComment()});
			}
			model.put("car_List", car_List);
		}
		//告别厅使用记录
		Map<String, Object>farewellMaps=new HashMap<String, Object>();
		farewellMaps.put("commissionOrderId", id);
		List<FarewellRecord> farewellRecordList=new  ArrayList<FarewellRecord>();
		FarewellRecord farewellRecord=new FarewellRecord();
		farewellRecordList=farewellRecordService.getFarewellRecordList(farewellMaps ,"id asc");
		if (farewellRecordList.size()>0) {
			if(id!=null&&!id.equals("")){
				farewellRecord=(FarewellRecord) farewellRecordList.get(0);
			}
		}
		model.put("farewellRecord", farewellRecord);
		
		
		
		
		//冷藏柜使用记录
		Map<String, Object>freezerMaps=new HashMap<String, Object>();
		
		freezerMaps.put("commissionOrderId", id);
		List<FreezerRecord> freezerRecordList=new  ArrayList<FreezerRecord>();
		FreezerRecord freezerRecord=new FreezerRecord();
		freezerRecordList=freezerRecordService.getFreezerRecordList(freezerMaps);
		if (freezerRecordList.size()>0) {
			if(id!=null&&!id.equals("")){
				freezerRecord=(FreezerRecord) freezerRecordList.get(0);
			}
		}
		model.put("freezerRecord", freezerRecord);
		
		
		//守灵室使用记录
		Map<String, Object>mourningMaps=new HashMap<String, Object>();
		mourningMaps.put("commissionOrderId", id);
		List<MourningRecord> mourningRecordList=new  ArrayList<MourningRecord>();
		MourningRecord mourningRecord=new MourningRecord();
		mourningRecordList=mourningRecordService.getMourningRecordList(mourningMaps,"id asc");
		if (mourningRecordList.size()>0) {
			if(id!=null&&!id.equals("")){
				mourningRecord=(MourningRecord) mourningRecordList.get(0);
			}
		}
		model.put("mourningRecord", mourningRecord);
		
		//火化炉
		Map<String, Object>furnaceRecordMaps=new HashMap<String, Object>();
		List<Object[]> furnaceList=Const.getFurnaceList();
		List<Object[]> furnace_list=new ArrayList<Object[]>();
		furnaceRecordMaps.put("commissionOrderId", id);
		List<FurnaceRecord> flist=new  ArrayList<FurnaceRecord>();
		FurnaceRecord furnaceRecord=new FurnaceRecord();
		flist=furnaceService.getFurnaceRecordList(furnaceRecordMaps);
		if (flist.size()>0) {
				furnaceRecord=(FurnaceRecord) flist.get(0);
				
		}
		for (int i = 0; i < furnaceList.size(); i++) {
			Object[] ob=(Object[]) furnaceList.get(i);
			byte by=(Byte) ob[0];
			if (by==commissionOrder.getFurnaceFlag()) {
				furnace_list.add(new Object[]{ob[0],ob[1],checked});
			}else {
				furnace_list.add(new Object[]{ob[0],ob[1],""});
			}		
		}
		String A="普通炉";
		String B="特约炉";
		String C="特殊业务";//改变成特殊业务的名称
		if(commissionOrder.getFurnaceFlag()==1){
			model.put("tname",A);
		}
		
		if(commissionOrder.getFurnaceFlag()==2){
			model.put("tname",B);
		}
		if(commissionOrder.getFurnaceFlag()==3){//特殊业务
			String commentId=commissionOrder.getFurnaceComment();
			SpecialBusinessType specialType=specialBusinessTypeService.getSpecialBusinessTypeById(commentId);
			if(specialType!=null){
				C=specialType.getName();
			}
			model.put("tname",C);
		}
		model.put("furnaceRecord", furnaceRecord);
		model.put("furnaceList", furnaceList);
		model.put("furnace_list", furnace_list);
		byte checkFlag=commissionOrder.getCheckFlag();
		model.put("checkFlag", checkFlag);
		model.put("toponymOption", toponymOption);
		model.put("registerOption", registerOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("fAppellationOption", fAppellationOption);
		model.put("dFlagOption", dFlagOption);
		model.put("proveUnitOption", proveUnitOption);
		model.put("sexOption", sexOption);
		model.put("deadReasonOption", deadReasonOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("certificateOption", certificateOption);
		model.put("commissionOrder", commissionOrder);
		model.put("Check_No", Const.Check_No);
		model.put("Check_Yes", Const.Check_Yes);
		model.put("IsHave_Yes", Const.IsHave_Yes);
		model.put("IsHave_No", Const.IsHave_No);
		model.put("Is_Yes", Const.Is_Yes);
		model.put("Is_No", Const.Is_No);
		model.put("id", id);
		model.put("billUnitOption", billUnitOption);
		model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		model.put("isOption", Const.getIsOption((byte)0, false));
		model.put("agentUser", getCurrentUser().getName());
//		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time="";
		if(commissionOrder.getUpdateTime()!=null){
			time=format.format(commissionOrder.getUpdateTime());
		}else{
			time=format.format(commissionOrder.getCreatTime());
		}
		
		model.put("time",time);
        model.put("method", "save");
        model.put("ifCheck", ifCheck);
        if("print".equals(styleType)){
        	return new ModelAndView("ye/commissionOrder/printCommissionOrder",model);
        }
        return new ModelAndView("ye/commissionOrder/editCommissionOrderReadOnly",model);        	
        
    }
	/**
	 * 修改基本信息
	 * @return
	 */
	@RequestMapping(params = "method=saveBase")
    @SystemControllerLog(description = "保存修改之后的火化委托单基本信息")
    @ResponseBody
	public JSONObject saveBase(){
		JSONObject json=new JSONObject();
		try{
			String id=getString("id");
			CommissionOrder commissionOrder=new CommissionOrder();
			if(id!=null &&!"".equals(id)){
				commissionOrder=commissionOrderService.getCommissionOrderById(id);
			}
			
    		String cardCode=getString("cardCode");
    		if (id == null || id.equals("")) {
				int num = commissionOrderService.getCommissionOrderByCard(cardCode);
				if (num >= 1) {
					setJsonByFail(json, "该卡号已经存在");
					return json;
				}
			}
    		String certificateCode=getString("certificateCode");
    		String certificateId=getString("certificateId");
    		String dAddr=getString("dAddr");
    		String deadReasonId=getString("deadReasonId");
    		String deadTypeId=getString("deadTypeId");
    		String province=getString("province");
    		String city=getString("city");
    		String area=getString("area");
    		byte dFlag=getByte("dFlag");
    		
    		//死者身份证图片路径
    		String dIdcardPic =getString("filename");
    		//经办人身份证图片路径
    		String eIdcardPic=getString("filenameF");
    		//拍照存档图片路径
    		String photoPic=getString("filenameP");
    		
    		String dNation=getString("dNation");
    		if(dNation!=null && !"".equals(dNation)){
       			String nationId = nationService.getdnationId(dNation);
       			if(nationId != null){
       				dNation = nationId;
       			}
       		}
    		Timestamp dTime=getTimeM("dTime");
    		String fAddr=getString("fAddr");
    		String fAppellationId=getString("fAppellationId");
    		String fName=getString("fName");
    		String fPhone=getString("fPhone");
    		String fUnit=getString("fUnit");
    		String fCardCode=getString("fCardCode");
    		String name=getString("name");
    		String pickAddr=getString("pickAddr");
    		String proveUnitId=getString("proveUnitId");
    		String proveUnitContent=getString("proveUnitContent");
    		byte sex=getByte("sex");
    		int age=getInt("age");
    		
    		
    		if (!cardCode.equals(commissionOrder.getCardCode()) && !"".equals(cardCode)) {
				int num = commissionOrderService.getCommissionOrderByCard(cardCode);
				if (num >= 1) {
					setJsonByFail(json, "该卡号已经存在");
					return json;
				}
			}
    		
    		commissionOrder.setAge(age);
    		commissionOrder.setSex(sex);
    		commissionOrder.setName(name);
    		commissionOrder.setCardCode(cardCode);
    		commissionOrder.setCertificateId(certificateId);
    		commissionOrder.setCertificateCode(certificateCode);
    		commissionOrder.setdAddr(dAddr);
    		commissionOrder.setPickAddr(pickAddr);
    		commissionOrder.setDeadTypeId(deadTypeId);
    		commissionOrder.setDeadReasonId(deadReasonId);
    		commissionOrder.setdTime(dTime);
    		commissionOrder.setdNationId(dNation);
    		commissionOrder.setProvince(province);
    		commissionOrder.setCity(city);
    		commissionOrder.setArea(area);
    		commissionOrder.setdFlag(dFlag);
    		commissionOrder.setdIdcardPic(dIdcardPic);
    		commissionOrder.seteIdcardPic(eIdcardPic);
    		commissionOrder.setPhotoPic(photoPic);
    		commissionOrder.setfName(fName);
    		commissionOrder.setfPhone(fPhone);
    		commissionOrder.setfAppellationId(fAppellationId);
    		commissionOrder.setfAddr(fAddr);
    		commissionOrder.setfUnit(fUnit);
    		commissionOrder.setfCardCode(fCardCode);
    		commissionOrder.setProveUnitId(proveUnitId);
    		commissionOrder.setProveUnitContent(proveUnitContent);
    		
    		commissionOrderService.updateCommissionOrder(commissionOrder);
    		String menu=getString("menu");
    		if("check".equals(menu)){
    			setJsonBySuccess(json, "保存成功", "commissionOrderCheck.do?method=list");
    		}else if("com".equals(menu)){
    			setJsonBySuccess(json, "保存成功", "commissionOrder.do?method=list");
    		}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(DataTools.getStackTrace(e));
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
	}
    /**
     * 保存火化委托单
     * @return
     */
    @RequestMapping(params = "method=save")
    @SystemControllerLog(description = "保存火化委托单信息")
    @ResponseBody
    public JSONObject saveCommissionOrder(){
		JSONObject json = new JSONObject();
    	try {
    		String isSH=getString("isSH");
    		String userId=getCurrentUser().getUserId();
    		String userName=getCurrentUser().getName();
    		String id =getString("id");
    		int age=getInt("age");
    		Timestamp arriveTime=getTimeM("arriveTime");
    		String cardCode=getString("cardCode");
    		if (id == null || id.equals("")) {
				int num = commissionOrderService.getCommissionOrderByCard(cardCode);
				if (num >= 1) {
					setJsonByFail(json, "该卡号已经存在");
					return json;
				}
			}else{//有id号进行判断是否收费
				List<BusinessFees> busList= businessFeesService.getBusinessFeesByComId(id);
				if(busList.size()>0){//已有收费，限制保存
					setJsonByFail(json, "该死者已收费，无法保存，请返回");
					return json;
				}
			}
    		String certificateCode=getString("certificateCode");
    		String certificateId=getString("certificateId");
    		Timestamp checkTime=getTimeM("checkTime");
    		String checkUserId=getString("checkUserId");
    		String cComment=getString("comment");
    		String corpseUnitId=getString("corpseUnitId");
    		Timestamp orderTime=getTimeM("orderTime"); 
    		String dAddr=getString("dAddr");
    		String deadReasonId=getString("deadReasonId");
    		String deadTypeId=getString("deadTypeId");
    		String province=getString("province");
    		String city=getString("city");
    		String area=getString("area");
    		byte dFlag=getByte("dFlag");
    		//死者身份证图片路径
    		String dIdcardPic =getString("filename");
    		//经办人身份证图片路径
    		String eIdcardPic=getString("filenameF");
    		//拍照存档图片路径
    		String photoPic=getString("filenameP");
    		
    		String dNation=getString("dNation");
    		if(dNation!=null && dNation!=""){
       			String nationId = nationService.getdnationId(dNation);
       			if(nationId != null){
       				dNation = nationId;
       			}
       		}
    		Timestamp dTime=getTimeM("dTime");
    		String fAddr=getString("fAddr");
    		String fAppellationId=getString("fAppellationId");
    		String fName=getString("fName");
    		String fPhone=getString("fPhone");
    		String fUnit=getString("fUnit");
    		String fCardCode=getString("fCardCode");
    		String furnaceTypeId=getString("furnaceTypeId");
    		String furnaceId=getString("furnaceId");
    		String name=getString("name");
    		String pickAddr=getString("pickAddr");
    		String proveUnitId=getString("proveUnitId");
    		String proveUnitContent=getString("proveUnitContent");
    		String registerId=getString("registerId");
    		String toponymId = getString("toponymId");
    		String specialBusinessValue=getString("specialBusinessValue"); //火化炉选择无时的备注
    		byte scheduleFlag=getByte("scheduleFlag");
    		byte sex=getByte("sex");
    		byte radio=getByte("radio");
			
			//挂账单位
    		String billUnitId=getString("billUnitId");
    		
    		// 新增  项  科二   登记人  电话预约人   
//    		String pbookAgentId = getString("pbookAgentId");
//       		String bookAgentId = getString("bookAgentId");
     
    		
    		//收费项目
    		String[] service=getArrayString("service_id");//服务项目
    		String[] articles=getArrayString("articles_id");//丧葬用品
    		String[] itemId=getArrayString("itemId");
    		String[] pice=getArrayString("pice");
    		String[] number=getArrayString("number");
    		String[] bill=getArrayString("bill");
    		String[] total=getArrayString("total");
    		String[] comment=getArrayString("comment");
    		//车辆调度
    		String[] transportType=getArrayString("transportTypeId");
    		String[] carType=getArrayString("carTypeId");
    		//此处因为 时间从原来的精确秒,统一改成到分钟,还用之前的方法会报错
//    		Timestamp[] pickTime=getArrayTimestamp("pickTime");
    		String[] pickpick=getArrayString("pickTime");
    		Timestamp[] pickTime=null;
    		if(pickpick!=null && pickpick.length>0){
    			pickTime=DateTools.stringToTimeArray(pickpick);
    		}
    		String[] carComment=getArrayString("carComment");
    		String[] carSchedulRecordId=getArrayString("csr_id");
			//冷藏柜使用记录
			String freezerId=getString("freezerId");
			//冷藏柜使用记录    入柜经办人 (为配合业务二科  二科登记)
   			String assiginUser   =  getString("assiginUser");
   			// 为配合 二科登记  登记既为 入柜 
   			byte freezerFlag= 0;
			//告别厅
			Timestamp farewellBeginDate=getTimeM("farewellBeginDate");
			Timestamp farewellEndDate=getTimeM("farewellEndDate");
			String farewellId=getString("farewellId");
			//守灵室
			Timestamp mourningBeginTime=getTimeM("mourningBeginTime");
			Timestamp mourningEndTime=getTimeM("mourningEndTime");
			String mourningId=getString("mourningId");
			
			
			String[] itemIdR=getArrayString("reduction_itemId");
			String[] numberR=getArrayString("reduction_number");
			String[] piceR=getArrayString("reduction_pice");
			String[] totalR=getArrayString("reduction_total");
			String[] commentR=getArrayString("reduction_comment");
			//以下新增,用于记录困难减免记录
			String[] hardIdR=getArrayString("hard_itemId");
			String[] hardnumberR=getArrayString("hard_number");
			String[] hardpiceR=getArrayString("hard_pice");
			String[] hardtotalR=getArrayString("hard_total");
			String[] hardcommentR=getArrayString("hard_comment");
			//基本减免
			String[]	 base=getArrayString("base_id");//基本减免项目
			String baseReductionId=getString("baseReductionId");//基本减免Id
			String[] freePersonId=getArrayString("freePersonId");//免费对象类别
			String[] hardPersonId=getArrayString("hardPersonId");//重点救助对象类别
    		byte baseIs=getByte("baseIs");
			String fidcard=getString("fidcard");
			//预约登记 id
			String appointmentId=getString("appointmentId");
			Appointment appointment=appointmentService.getAppointmentById(appointmentId);
			
			//困难减免
			String[] hard=getArrayString("hard_id");//困难减免项目
			String[] proveIds=getArrayString("proveIds");
			String hardReductionId=getString("hardReductionId");
			byte hardIs=getByte("hardIs");
			String hard_proveUnitId=getString("hard_proveUnitId");
			String reason=getString("reason");
			String hComment=getString("comment");
			Timestamp createTime=getTimeM("createTime");
			String appellationId=getString("appellationId");
			String applicant=getString("applicant");
			String phone=getString("phone");
			byte special=getByte("special");
			//区别从哪个通道进入下面这个方法
			String aisle="diaoDuKe";
    		String coId=commissionOrderService.addOrUpdateMethod(userId,userName, id, age, arriveTime, cardCode
    				, certificateCode, certificateId, checkTime, checkUserId
    				, cComment, corpseUnitId, orderTime, dAddr, deadReasonId
    				, deadTypeId, dFlag, dIdcardPic, dNation, dTime, eIdcardPic
    				, fAddr, fAppellationId, fName, fPhone, fUnit,fCardCode
    				, furnaceTypeId, furnaceId, name, pickAddr, proveUnitId
    				, proveUnitContent, registerId, toponymId, scheduleFlag, sex, radio
    				, service, articles, itemId, number, bill, total, comment
    				, transportType, carType, pickTime, carComment, carSchedulRecordId
    				, farewellBeginDate, farewellEndDate, farewellId
    				, freezerId, mourningBeginTime, mourningEndTime
    				, mourningId, orderTime, itemIdR, numberR, totalR
    				, commentR, base, baseReductionId, freePersonId
    				, hardPersonId, baseIs, fidcard, hard, proveIds
    				, hardReductionId, hardIs, hard_proveUnitId, reason
    				, hComment, createTime, appellationId, applicant
    				, phone, special,billUnitId, null,null,assiginUser,freezerFlag
    				, hardIdR,hardnumberR,hardtotalR,hardcommentR,province,city,area,specialBusinessValue
    				, appointment,pice,hardpiceR,piceR,photoPic,aisle,isSH);
    		//限制收费之后再保存火化委托单操作
    		setJsonBySuccess(json, "保存成功", "commissionOrder.do?method=list");
    		if(id!=null && !"".equals(id)){
    			json.put("coId", id);
    		}else{
    			json.put("coId", coId);
    		}
//		} catch (ParseException e) {
//			e.printStackTrace();
//			json.put("statusCode", 302);
//			json.put("message", "时间格式输入有误");
		}catch (NullPointerException n){
			setJsonByFail(json, "该委托单已收费，无法修改保存，请返回！");
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(DataTools.getStackTrace(e));
			
			setJsonByFail(json, "保存失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 删除火化委托单
     * @return
     */
    @RequestMapping(params = "method=delete")
    @SystemControllerLog(description = "删除火化委托单信息")
    @ResponseBody
	public JSONObject deleteCommissionOrder() {
		JSONObject json = new JSONObject();
		String id = getString("ids");
		try {
			String[] ids = id.split(",");
			for (String coId : ids) {
				byte checkFlag = commissionOrderService.getCheckFlag(coId);
				if (1 == checkFlag) {
					json.put("statusCode", 302);
					json.put("message", "已审核，不能删除");
					return json;
				}
				commissionOrderService.deleteCommissionOrder(coId);
			}
			setJsonBySuccess(json, "删除成功", true);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "系统异常" + e.getMessage());
		}
		return json;
	}
    
    /**
     * 销卡(author：hw)
     * @return
     */
    @RequestMapping(params = "method=pincard")
    @SystemControllerLog(description = "火化委托单销卡")
    @ResponseBody
    public JSONObject pincard(){
		JSONObject json = new JSONObject();
    	try {
	    	String ids = getString("ids");
	    	String[] id = ids.split(",");
		    for(int i=0;i<id.length;i++){
		    	CommissionOrder co = commissionOrderService.getCommissionOrderById(id[i]);
		    	User user =getCurrentUser();
		    	commissionOrderService.pinCardczl(co, user);
		    }
		    
    		setJsonBySuccess(json, "销卡成功", false);
		} catch (Exception e) {
			setJsonByFail(json, "销卡失败，错误"+e.getMessage());
		}
		return json;
    }
    
    /**
     * 是否审核
     * @return
     */
    @RequestMapping(params = "method=checkFlag")
    @SystemControllerLog(description = "火化委托单审核与取消")
    @ResponseBody
    public JSONObject isdelCarType(){
		JSONObject json = new JSONObject();
    	try {
	    	String id = getString("id");
	    	String[] ids = id.split(",");
	    	byte checkFlag=getByte("checkFlag");
	    	Timestamp nowTime=new Timestamp(System.currentTimeMillis());
	    	Map<String, Object> maps = null;
		    for(int i=0;i<ids.length;i++){
		    	maps = new HashMap<String, Object>();
				maps.put("commissionOrderId", ids[i]);
				//冷藏柜记录
				List<FreezerRecord> fList=freezerRecordService.getFreezerRecordList(maps);
				//告别厅记录
				List<FarewellRecord> faList=farewellRecordService.getFarewellRecordList(maps,"");
				//灵堂记录
				List<MourningRecord> mList=mourningRecordService.getMourningRecordList(maps,"");
				//火化炉调度
				maps.put("changeFlag", Const.ChangeFlag_No);
				List<FurnaceRecord> fuList=furnaceRecordService.getFurnaceRecordList(maps, "");
				if (checkFlag==Const.Check_No) {
					maps.put("checkFlag", Const.Check_No);
				}else if(checkFlag==Const.Check_Yes){
					maps.put("checkFlag", Const.Check_Yes);
				}
				maps.put("checkTime", nowTime);
				
				
				//以下判断 挂账火化委托单里的非挂账业务是否已付款
				Map<String,Object> paramMap=new HashMap<String,Object>();
				paramMap.put("tickFlag", Const.Is_Yes);
				paramMap.put("commissionOrderId", ids[i]);
				List<ItemOrder> listItem=itemOrderService.getItemOrderList(paramMap);
				boolean flag=false;//非挂账都为false
				if(listItem!=null && listItem.size()>0){
					flag=true;//挂账业务默认为true
					paramMap.put("tickFlag", Const.Is_No);
					List<ItemOrder> NoTicklist=itemOrderService.getItemOrderList(paramMap);
					for(int j=0;j<NoTicklist.size();j++){
						byte payFlag=NoTicklist.get(j).getPayFlag();
						if(payFlag==2){//若在挂账业务中有非挂账的,并且未付款
							flag=false;//则此挂账业务也为false
						}
					}
				}
				CommissionOrder commissionOrder = new CommissionOrder();
	    		commissionOrder=commissionOrderService.getCommissionOrderById(ids[i]);
	    		List<ItemOrder> list = new ArrayList<ItemOrder>();
	    		Map<String,Object> map=new HashMap<String,Object>();
	    		map.put("commissionOrderId", id);
	    		map.put("type", Const.Type_Articles);
	    		list = itemOrderService.getItemOrderListForNotice(map);
		    	commissionOrderService.checkMethod(maps, checkFlag,fList,faList,mList,fuList,flag,commissionOrder,list);
		    	
		    }
    		setJsonBySuccess(json, "操作成功", true);
		} catch (Exception e) {
			setJsonByFail(json, "操作失败，错误"+e.getMessage());
			e.printStackTrace();
		}
		return json;
    }
    /**
     * 根据地区判断是否基本减免
     * @return
     */
    @RequestMapping(params = "method=areaChange")
    @ResponseBody
    public JSONObject areaChange(){
    	JSONObject json=new JSONObject();
    	try {
	    	String areaName=getString("id");
	    	Map<String,Object> paramMap=new HashMap<String,Object>();
	    	paramMap.put("name", areaName);
	    	List<Toponym> list=toponymService.getToponymList(paramMap, null);
	    	if(list.size()>0){
	    		Toponym toponym=list.get(0);
	    		byte isBase=toponym.getIsBase();
	    		if(isBase==1){
	    			json.put("isBase", 1);
	    		}else{
	    			json.put("isBase", 2);
	    		}
	    	}
    	}catch (Exception e) {
    		e.printStackTrace();
    	}
    	return json;
    }
    /**
     * 死亡类型改变
     * @return
     */
    @RequestMapping(params = "method=deadTypeChange")
    @ResponseBody
    public JSONObject deadTypeChange(){
    	JSONObject json = new JSONObject();
    	String typeId=getString("id");
    	String reasonId=getString("reasonId");
    	String str=deadReasonService.getDeadReasonChange(typeId,reasonId);
    	int indexFlag=0;
    	if(typeId!=null && !"".equals(typeId)){
    		indexFlag=deadTypeService.getDeadTypeId(typeId).getIndexFlag();
    	}
    	if(indexFlag==2){
    		json.put("deadFlag", 2);
    	}else{
    		json.put("deadFlag", 1);
    	}
    	json.put("str", str);
		return json;
    }
    
    /**
     * 项目类别改变
     * @return
     */
    @RequestMapping(params = "method=itemTypeChange")
    @ResponseBody
    public JSONObject itemTypeChange(){
		JSONObject json = new JSONObject();
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId", getString("id"));
		maps.put("isdel", Const.Isdel_No);
		List<Item> list=itemService.getItemList(maps);
		Item item=new Item();
		if (list.size()>0) {
			item=list.get(0);
		}
    	String str=itemService.getItemOption(maps, "", false);
    	String helpOption=itemService.getItemHelpCodeOption(maps,"",false);
    	
		json.put("pice", item.getPice());
		json.put("helpOption", helpOption);
		json.put("str", str);
		json.put("faIndex",itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag());
		json.put("sonIndex",item.getIndexFlag());
		return json;
    }
    
    
    /**
     * 项目名称改变
     * @return
     */
    @RequestMapping(params = "method=itemNameChange")
    @ResponseBody
    public JSONObject itemNameChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId",item.getTypeId());
		maps.put("isdel", Const.Isdel_No);
    	String str =itemService.getItemHelpCodeOption(maps, item.getId(), false);
//    	String typeOption=itemTypeService.getItemTypeOption(maps,item.getTypeId(), false);
		json.put("pice", item.getPice());
		json.put("str", str);
		json.put("sonIndex",item.getIndexFlag());
		return json;
    }
    
    /**
     * 项目名称改变
     * @return
     */
    @RequestMapping(params = "method=itemHelpCodeChange")
    @ResponseBody
    public JSONObject itemHelpCodeChange(){
    	JSONObject json = new JSONObject();
    	Item item=itemService.getItemById(getString("id"));
    	Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("typeId",item.getTypeId());
    	String str =itemService.getItemOption(maps, item.getId(), false);
    	json.put("pice", item.getPice());
		json.put("str", str);
		json.put("sonIndex",item.getIndexFlag());
		return json;
    }
    
    /**
	 * 链接到修改添加服务项目
	 * @return
	 */
    @RequestMapping(params = "method=selectItem")
    public ModelAndView editItemOrder(){
		Map<String,Object> model=new HashMap<String,Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=itemTypeList.get(i);
			maps.put("typeId",itemType.getId());
			String itemOption=itemService.getItemOption(maps, "",true);
			list.add(new Object[]{itemType.getName(),itemOption});
		}
        model.put("list", list);
        model.put("type", getByte("type"));
        model.put("id", getString("id"));
        model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		return new ModelAndView("ye/commissionOrder/selectItem",model);
    }
    
    
    /**
	 * 链接到修改添加服务项目
	 * @return
	 */
    @RequestMapping(params = "method=selectItems")
    public ModelAndView editItemOrders(){
		Map<String,Object> model=new HashMap<String,Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		List<ItemType> itemTypeList=itemTypeService.getItemTypeList(maps);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			maps.put("typeId",itemType.getId());
			maps.put("isdel", Const.Isdel_No);
			String itemOption=itemService.getItemOption(maps, "",true);
			list.add(new Object[]{itemType.getName(),itemOption});
		}
		//添加服务项目
		List<Object[]> itemList=new ArrayList<Object[]>();
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			Map<String, Object>itemMaps=new HashMap<String, Object>();
			itemMaps.put("typeId", itemType.getId());
			itemMaps.put("isdel", Const.Isdel_No);
			List<Item> itemList1=itemService.getItemList(itemMaps,"index_flag asc");
			itemList.add(new Object[]{itemType,itemList1});
		}
		model.put("itemTree", getTree(itemList));
        model.put("list", list);
        model.put("type", getByte("type"));
        model.put("id", getString("id"));
        model.put("Type_Articles", Const.Type_Articles);
		model.put("Type_Service", Const.Type_Service);
		return new ModelAndView("ye/commissionOrder/selectItems",model);
    }
    /**
     * 添加服务项目
     * @return
     */
    @RequestMapping(params = "method=addItems")
    @ResponseBody
    public List<Object[]> saveItemOrder(){
		List<Object[]> list = new ArrayList<Object[]>();
		List<String> itemIist = new ArrayList<String>();
		String id =getString("ids");
		String[] ids = id.split(",");
		for(int i=0;i<ids.length;i++){
			if (!ids[i].equals("")) {
				itemIist.add(ids[i]);
			}
		}
		Map<String, Object>maps=new HashMap<String, Object>();
		maps.put("type", getByte("type"));
		maps.put("isdel", Const.Isdel_No);
		Map<String, Object>maps1=new HashMap<String, Object>();
	    for(int i=0;i<itemIist.size();i++){
	    	Item item=itemService.getItemById(itemIist.get(i).toString());
	    	/*if("508".equals(item.hashCode())){
	    		
	    	}else{
	    		
	    	}*/
	    	maps1.put("typeId", item.getTypeId());
	    	maps1.put("isdel", Const.Isdel_No);
	    	String itemOption=itemService.getItemOption(maps1,itemIist.get(i),false);
	    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(maps1,item.getId(),false);
	    	String itemTypeOption=itemTypeService.getItemTypeOption(maps,item.getTypeId(), false);
	    	String isOption=Const.getIsOption((byte)0, false);
	    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
	    	int sonIndex=item.getIndexFlag();
	    	list.add(new Object[]{itemOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),itemHelpCodeOption,faIndex,sonIndex});
	    }
		return list;
    }
    /**
     * 添加服务项目
     * @return
     */
    @RequestMapping(params = "method=addItem")
    @ResponseBody
    public String addItem(){
//    	int[] pages = getPage();
		Map<String, Object>maps=new HashMap<String, Object>();
		String id =getString("ids");
		String[] ids =  id.split(",");//Id数组
		String ind =getString("ind");
		String[] checkId =  ind.split(",");//复选框Id数组
		List<String> idsList=new ArrayList<String>();
		int[] index=new int[ids.length];
		String[] inds=new String[ids.length];
		int a=0;
		for (int i = 0; i < ids.length; i++) {
			idsList.add(ids[i]);
			if (!ids[i].equals("")&&ids[i]!="") {
				index[a]=i;
				inds[a]=checkId[i];
				a++;
			}
		}
		maps.put("ids", idsList);
//		List list = new ArrayList();
		String str="";
		List<Item> itemList =itemService.getItemList(maps,"b.index_flag asc,a.index_flag asc");
//		while(itemList.size()>0){
//			
//			Item item=(Item)itemList.get(0);
//			for (int j = 0; j < itemList.size(); j++) {
//				Item item1=(Item)itemList.get(j);
//				if (item.getIndexFlag()>item1.getIndexFlag()) {
//					item=item1;
//				}
//			}
//			itemList.remove(0); 
//			list.add(item);
//		}
		str+="{";
		int x=0;
		for (int i = 0; i < itemList.size(); i++) {
			if (i>0) {
				str+=",";
			}
			Item item=(Item)itemList.get(i);
			x++;
			str+=""+'"'+x+"."+'"'+":{"+'"'+"id"+'"'+":"+'"'+item.getId()+'"'+","
			+'"'+"indexFlag"+'"'+":"+'"'+x+'"'+","
			+'"'+"name"+'"'+":"+'"'+item.getName()+'"'+","
			+'"'+"index"+'"'+":"+'"'+index[i]+'"'+","
			+'"'+"inds"+'"'+":"+'"'+inds[i]+'"'+"}";
		}
		str+="}";
		return str;
    }
    /**
     * 获得tree结构
     * @param list
     * @return
     */
	public String getTree(List<Object[]> list){
    	int sum=0;
    	String content="";
    	for (int i = 0; i < list.size(); i++) {
    		if (i>0) {
    			content+=",";
			}
    		Object[] objects=list.get(i);
    		ItemType itemType=(ItemType)objects[0];
	    	content+= "{";
	    	content+="text:'"+itemType.getName()+"'";
//	    	content+=",tags:['"+itemType.getId()+"']";
	    	content+=",indexFlag:'"+itemType.getIndexFlag()+"'";
	    	content+=",selectable: false";
	    	List<?> itemList=(List<?>)objects[1];
	    	content+=",nodes: [";
	    	for (int j = 0; j < itemList.size(); j++) {
	    		if (j>0) {
	    			content+=",";
				}
	    		Item item=(Item)itemList.get(j);
	    		content+="{";
	    		content+="text:'"+item.getName()+"'";
	    		content+=",indexFlag:'"+item.getIndexFlag()+"'";
	    		content+=",tags:['"+item.getId()+"_"+sum+"']";
	    		sum++;
	    		content+="}";
	    	}
	    	content+="]";
	    	content+="}";
    	}
    	return content;
    }
    
    
    
    /**
     * 灵堂调度页面
     * @return
     */
    @RequestMapping(params = "method=editMourning")
    public ModelAndView editMourning(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int time=getInt("time");
		Date date = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("current_time")!=null){
			date = getDate("current_time");
		}
		if (time!=0) {
			date=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), time, Calendar.DATE);
		}
		Map<String, Object> maps=new HashMap<String, Object>();
		List<Mourning> mList = mourningService.getMourningList(null,"");
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("endDate", DateTools.getDayEndTime(DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), 6, Calendar.DATE)));
		List<MourningRecord> rList = mourningRecordService.getMourningRecordList(maps,"begin_time");
		//获取搜索日期的之后7天的所有日期List
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[8];
		dOb[0]="灵堂";
		//设置第一行（日期）
		for(int i=0;i<7;i++){
			dOb[i+1]=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), i, Calendar.DATE);
		}
		list.add(dOb);
		//设置默认列表数组
		for(int i=0;i<mList.size();i++){
			Mourning mouring = (Mourning)mList.get(i);
			list.add(new Object[]{mouring,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		//遍历所有记录，放在相应的位置
		for(int i=0;i<rList.size();i++){
			MourningRecord mr = (MourningRecord)rList.get(i);
			int row = 0;//行
			int col = 0;//列
			int endCol=0;//最后一列
			//对比灵堂，确认行数
			for(int j=1;j<list.size();j++){
				//把数组放在list中
				Object[] ob = list.get(j);
				Mourning m = (Mourning)ob[0];
				if(m.getId().equals(mr.getMourningId())){
					row = j;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++){
				Date da = (Date)dOb[d];
				if(isSomeDay(da, new Date(mr.getBeginTime().getTime()))){
					col = (d-1)*2+isPmOrAm(mr.getBeginTime());
				}
				if(isSomeDay(da, new Date(mr.getEndTime().getTime()))){
					endCol = (d-1)*2+isPmOrAm(mr.getEndTime());
				}
			}
			if(row!=0){
				Object[] o = (Object[])list.get(row);
				if(endCol==0){
					endCol = 14;
				}
				if(col==0){
					col=1;
				}
				for(int x=col;x<=endCol;x++){
					o[x] = mr;
				}
			}
			
		}
		model.put("date", date);
		model.put("list", list);
        model.put("method", "editMourning");
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
        model.put("IsFlag_Bzwc",Const.IsFlag_Bzwc);
        model.put("IsFlag_YY", Const.IsFlag_YY);
        model.put("IsFlag_QC", Const.IsFlag_QC);
    	return new ModelAndView("ye/commissionOrder/editMourning",model);
    }
    
    /**
     * 选中返回时间
     * @return
     */
    @RequestMapping(params = "method=selectedTime")
    @ResponseBody
    public JSONObject selectedTime(){
    	JSONObject json = new JSONObject();
    	Date date = getDate("tDate");
    	int col=getInt("col");//列
    	Object[] dOb = new Object[15];
		//从当前时间开始的后七天
    	int r=1;
		for(int i=0;i<7;i++){
			dOb[r]=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), i, Calendar.DATE);
			r++;
			dOb[r]=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), i, Calendar.DATE);
			r++;
		}
		Date date1=(Date) dOb[col];
		String time="";
		col=col%2;
		if (col!=0) {
			time=date1+" 11:00";
		}else {
			time=date1+" 15:00";
		}
		int row=getInt("row");
		List<Mourning> mList = mourningService.getMourningList(null,"");
		Mourning mourning=mList.get(row-1);
		json.put("time", time);
		json.put("id", mourning.getId());
		json.put("name", mourning.getName());
		//以下判断灵堂类型
		Mourning lingtang=mourningService.getMourningById(mourning.getId());
		String typeId=lingtang.getTypeId();
		MourningType mourningType=mourningTypeService.getMourningTypeId(typeId);
		String mType=mourningType.getName();
		Item item=new Item();
		if("普通".equals(mType)){
			item=itemService.getItemByHelpCode("501");
		}else if("豪华".equals(mType)){
			item=itemService.getItemByHelpCode("503");
		}else if("中高档".equals(mType)){
			item=itemService.getItemByHelpCode("502");
		}
		Map<String,Object> serviceMaps=new HashMap<String, Object>();
		serviceMaps.put("typeId", item.getTypeId());
		serviceMaps.put("isdel", Const.Isdel_No);
    	String itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	String isOption=Const.getIsOption((byte)0, false);
    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
    	int sonIndex=item.getIndexFlag();
    	//以下选择灵堂之后,追加灵堂冷藏费(水晶棺)
    	Item item2=new Item();
    	item2=itemService.getItemByHelpCode("402");
    	Map<String,Object> serviceMaps2=new HashMap<String, Object>();
    	serviceMaps2.put("typeId", item2.getTypeId());
    	serviceMaps2.put("isdel", Const.Isdel_No);
    	String itemOption2=itemService.getItemOption(serviceMaps2, item2.getId(),false);
    	String itemTypeOption2=itemTypeService.getItemTypeOption(serviceMaps2,item2.getTypeId(), false);
    	String itemHelpCodeOption2=itemService.getItemHelpCodeOption(serviceMaps2,item2.getId(),false);
    	int faIndex2=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
    	int sonIndex2=item.getIndexFlag();
    	
		List<Object> list=new ArrayList<Object>();
		list.add("service");
		list.add(itemOption);
		list.add(itemHelpCodeOption);
		list.add(item.getPice());
		list.add(itemTypeOption);
		list.add(1);
		list.add(isOption);
		list.add(item.getPice());
		list.add(item.getComment());
		list.add(faIndex);
		list.add(sonIndex);
		//追加的冷藏收费
		List<Object> list2=new ArrayList<Object>();
		list2.add("service");
		list2.add(itemOption2);
		list2.add(itemHelpCodeOption2);
		list2.add(item2.getPice());
		list2.add(itemTypeOption2);
		list2.add(1);
		list2.add(isOption);
		list2.add(item2.getPice());
		list2.add(item2.getComment());
		list2.add(faIndex2);
		list2.add(sonIndex2);
		json.put("mourningList2", list2);
		json.put("mourningList", list);
		
		return json;
    }
    
    /**
     * 告别厅调度页面
     * @return
     */
    @RequestMapping(params = "method=editFarewell")
    public ModelAndView editFarewell(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int time=getInt("time");
		Date date = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("current_time")!=null){
			date = getDate("current_time");
		}
		if (time!=0) {
			date=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), time, Calendar.DATE);
		}
		model.put("date", date);
		List<Farewell> fList = farewellService.getFarewellList(null,"");
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("beginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("endDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		List<FarewellRecord> fRList=farewellRecordService.getFarewellRecordList(maps,"begin_date asc");
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[14];
		dOb[0]="告别厅";
		for (int i = 0; i < 13; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
		}
		list.add(dOb);
		//设置默认列表数组
		for(int i=0;i<fList.size();i++) {
			Farewell farewell = fList.get(i);
			list.add(new Object[]{farewell,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null});
		}
		for (FarewellRecord fr : fRList) {
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;
			//对比告别厅，确认行数
			for (int i = 1; i < list.size(); i++) {
				Object[] ob = list.get(i);
				Farewell farewell = (Farewell) ob[0];
				if(farewell.getId().equals(fr.getFarewellId())) {
					row = i;//获得第几行
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if(DateTools.isSameHour(da, new Date(fr.getBeginDate().getTime()))) {
					col = 2*(d-1)+ifHalft(fr.getBeginDate());
				}
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					if(DateTools.isSameHour(da, new Date(fr.getEndDate().getTime()))){
						endCol = (d-1)*2+ifHalft(fr.getEndDate());
					}
				}
			}
			if (row != 0) {
				if (fr.getEndDate() != null && !(fr.getEndDate().equals(""))) {
					Object[] o = (Object[])list.get(row);
					if(endCol==0){
						endCol = 28;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						o[x] = fr;
					}
				} else {
					if(row!=0){
						Object[] o = (Object[])list.get(row);
						if (col != 0) {
							o[col] = fr;
						}
					}
				}
			}
		}
		model.put("list", list);
        model.put("method", "editFarewell");
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
        model.put("IsFlag_Bzwc", Const.IsFlag_Bzwc);
        model.put("IsFlag_YY",Const.IsFlag_YY);
    	return new ModelAndView("ye/commissionOrder/editFarewell",model);
    }
    /**
     * 告别厅时间改变
     * @return
     */
    @RequestMapping(params = "method=farewellTime_change")
    @ResponseBody
    public JSONObject farewellTime_change(){
    	JSONObject json = new JSONObject();
    	try {
			Date date = null;
			date = getTime("time");
			String time="";
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.HOUR,1);
			date = calendar.getTime();
			time=sdf.format(date)+"";
			json.put("time", time);
			json.put("statusCode", 200);
		} catch (Exception e) {
			e.printStackTrace();
			setJsonByFail(json, "");
		}
		return json;
    }
    /**
     * 告别厅点击
     * @return
     */
    @RequestMapping(params = "method=farewell_click")
    @ResponseBody
    public JSONObject farewell_click(){
    	JSONObject json = new JSONObject();
    	Date date = DateTools.getThisDate();
    	if(getDate("date")!=null){
			date = getDate("date");
		}
    	int col=getInt("col");//列
    	Object[] dOb = new Object[30];
		//从当前时间开始的后七天
    	int r=1;
		for(int i=0;i<13;i++){
			dOb[r]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
			r++;
			dOb[r]=DateTools.getHourAfterHour(new Date(date.getTime()), 4, i, Calendar.HOUR);
			r++;
		}
		Date date1=(Date) dOb[col];
		String time="";
		String fTime="";
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date1);
		int c=col%2;
		if (c!=0) {
			time=sdf.format(date1)+"";
			calendar.add(Calendar.HOUR,1);
			date1 =calendar.getTime();
			fTime=sdf.format(date1)+"";
		}else {
			calendar.add(Calendar.MINUTE,30);
			date1 =calendar.getTime();
			time=sdf.format(date1)+"";
			calendar.add(Calendar.HOUR,1);
			date1 =calendar.getTime();
			fTime=sdf.format(date1)+"";
			
		}
		
		int row=getInt("row");
		List<Farewell> fList = farewellService.getFarewellList(null,"");
		Farewell farewell=fList.get(row-1);
		json.put("fTime", fTime);
		json.put("time", time);
		json.put("id", farewell.getId());
		json.put("name", farewell.getName());
		//以下开始判断告别厅
		Farewell fare=farewellService.getFarewellById(farewell.getId());
		String typeId=fare.getTypeId();
		FarewellType fareType=farewellTypeService.getFarewellTypeById(typeId);
		String typeName=fareType.getName();
		Item item=new Item();
		if("大厅".equals(typeName)){
			item=itemService.getItemByHelpCode("416");
		}else if("中厅".equals(typeName)){
			item=itemService.getItemByHelpCode("417");
		}else if("小厅".equals(typeName)){
			item=itemService.getItemByHelpCode("418");
		}
		Map<String,Object> serviceMaps=new HashMap<String, Object>();
		serviceMaps.put("typeId", item.getTypeId());
		serviceMaps.put("isdel", Const.Isdel_No);
    	String itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	String isOption=Const.getIsOption((byte)0, false);
    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
    	int sonIndex=item.getIndexFlag();
		List<Object> list=new ArrayList<Object>();
		list.add("service");
		list.add(itemOption);
		list.add(itemHelpCodeOption);
		list.add(item.getPice());
		list.add(itemTypeOption);
		list.add(1);
		list.add(isOption);
		list.add(item.getPice());
		list.add(item.getComment());
		list.add(faIndex);
		list.add(sonIndex);
		json.put("fareWellList", list);
		//添加临时锁定功能
		//注：String的类型必须形如： yyyy-mm-dd hh:mm:ss[.f...] 这样的格式，中括号表示可选，否则报错！！！
//		SimpleDateFormat newsdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		calendar.add(Calendar.HOUR,-1);
//		date1 =calendar.getTime();
//		String lockTime=newsdf.format(date1);
//		String tsStr = lockTime;
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		time=time+":00";
		try {
			ts = Timestamp.valueOf(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
		FarewellRecord farewellRecord=new FarewellRecord();
		farewellRecord.setId(UuidUtil.get32UUID());
		farewellRecord.setCreateUserId(getCurrentUser().getUserId());
		farewellRecord.setBeginDate(ts);
		farewellRecord.setFarewellId(farewell.getId());
		farewellRecord.setFlag(Const.IsFlag_LSSD);
		farewellRecordService.addFarewellRecord(farewellRecord);
		return json;
    }
    /**
     * 告别厅解除锁定
     * @return
     */
    @RequestMapping(params = "method=unLock")
    @ResponseBody
    @SystemControllerLog(description = "解除告别厅锁定")
    public JSONObject farewellUnLock(){
    	JSONObject json = new JSONObject();
    	String userId=getCurrentUser().getUserId();
    	farewellRecordService.deleteFarewellRecordByLock(userId,Const.IsFlag_LSSD,null);
    	return json;
    }
    /**
     * 火化炉解除锁定
     * @return
     */
    @RequestMapping(params = "method=furnaceUnLock")
    @ResponseBody
    @SystemControllerLog(description = "解除火化炉锁定")
    public JSONObject furnaceUnLock(){
    	JSONObject json = new JSONObject();
    	String userId=getCurrentUser().getUserId();
    	furnaceRecordService.deleteFurnaceRecordUnLock(userId,Const.IsFlag_LSSD,null);
    	return json;
    }
    /**
     * 灵堂解除锁定
     * @return
     */
    @RequestMapping(params = "method=mourningUnLock")
    @ResponseBody
    @SystemControllerLog(description = "解除灵堂锁定")
    public JSONObject mourningUnLock(){
    	JSONObject json = new JSONObject();
    	String userId=getCurrentUser().getUserId();
    	mourningRecordService.deleteMourningRecordUnlock(userId,Const.IsFlag_LSSD,null);
    	return json;
    }
    /**
     * 灵堂锁定
     * @return
     */
    @RequestMapping(params = "method=mourningLock")
    @SystemControllerLog(description = "灵堂锁定")
    @ResponseBody
    public JSONObject mourningLock(){
    	JSONObject json = new JSONObject();
    	
			Timestamp start=getTimeM("startTime");
			Timestamp end=getTimeM("endTime");
			String userId=getCurrentUser().getUserId();
			String mourningId=getString("mourningId");
			MourningRecord mourning=new MourningRecord();
			mourning.setId(UuidUtil.get32UUID());
			mourning.setCreateUserId(userId);
			mourning.setBeginTime(start);
			mourning.setEndTime(end);
			mourning.setFlag(Const.IsFlag_LSSD);
			mourning.setMourningId(mourningId);
			mourningRecordService.addMourningRecord(mourning);
    	
    	return json;
    }
    /**
     * 判断是过半还是没有过半
     * @param time
     * @return 1：上半时 2：下半时
     */
    public byte ifHalft(Timestamp time) {
    	//时间转换对象
    	Calendar cal=Calendar.getInstance();
    	//设置时间
    	cal.setTime(new Date(time.getTime()));
    	int min=cal.get(Calendar.MINUTE);
    	if(min>=0 && min<30){
    		return (byte)1;
    	}else{
    		return (byte)2;
    	}
    } 
    
    

    /**
     * 冷藏柜调度页面
     * @return
     */
    @RequestMapping(params = "method=editFreezer")
    public ModelAndView editFreezer(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		List<Object> typeList=new ArrayList<Object>();
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object>freezermaps=new HashMap<String, Object>();
		List<FreezerRecord> freezerRecordList = freezerRecordService.getNotOutFreezerRecordList(null);
		List<FreezerType> freezerTypeList = freezerTypeService.getFreezerTypeList(null,"index_flag asc,name asc");//冷藏柜类型信息
		for (int i = 0; i < freezerTypeList.size(); i++) {
	/*		FreezerType freezerType=freezerTypeList.get(i);*/	
			FreezerType freezerType=(FreezerType) freezerTypeList.get(i);
			typeList.add(freezerType.getName());
			
			freezermaps.put("typeId",freezerType.getId());
			freezermaps.put("isdel",Const.Isdel_No);
			List<Freezer> freezerList = freezerService.getFreezerList(freezermaps,"index_flag asc,name asc");
			Object[] ob=new Object[freezerList.size()];
			for (int j = 0; j < freezerList.size(); j++) {
				ob[j]=freezerList.get(j);
				for (int j2 = 0; j2 < freezerRecordList.size(); j2++) {	
					if (freezerList.get(j).getId()==freezerRecordList.get(j2).getFreezerId()||freezerList.get(j).getId().equals(freezerRecordList.get(j2).getFreezerId())) {
						ob[j]=freezerRecordList.get(j2);
					}
				}
			}
			list.add(ob);
		}
		
		model.put("list", list);
		model.put("typeList", typeList);
        model.put("method", "editFreezer");
        model.put("IsFlag_Yse", Const.IsFlag_Yse);
        model.put("IsFlag_Lock", Const.IsFlag_Lock);
        model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
    	return new ModelAndView("ye/commissionOrder/editFreezer",model);
    }
    
    
    /**
     * 冷藏柜单击
     * @return
     */
    @RequestMapping(params = "method=freezer_click")
    @ResponseBody
    public JSONObject freezer_click(){
    	JSONObject json = new JSONObject();
    	int type=getInt("type");
    	int freezer=getInt("freezer");
		List<List<Freezer>> list=new ArrayList<List<Freezer>>();
		Map<String, Object>freezermaps=new HashMap<String, Object>();
		List<FreezerType> freezerTypeList = freezerTypeService.getFreezerTypeList(null,"index_flag asc,name asc");//冷藏柜类型信息
		for (int i = 0; i < freezerTypeList.size(); i++) {
			FreezerType freezerType=freezerTypeList.get(i);
			freezermaps.put("typeId",freezerType.getId());
			freezermaps.put("isdel",Const.Isdel_No);
			List<Freezer> freezerList = freezerService.getFreezerList(freezermaps,"index_flag asc,name asc");
			list.add(freezerList);
		}
		List<Freezer> tList=list.get(type);
		Freezer freezer1=(Freezer) tList.get(freezer);
		json.put("name", freezer1.getName());
		json.put("id", freezer1.getId());
		//根据type添加相对应的冷藏费用
				
		Item item=new Item();
		if(type==0){			
			item=itemService.getItemByHelpCode("403");	    	
		}else if(type==1 || type==2){
			item=itemService.getItemByHelpCode("402");		
		}else if(type==3 || type==4){
			item=itemService.getItemByHelpCode("405");		
		}
		Map<String,Object> serviceMaps=new HashMap<String, Object>();
		serviceMaps.put("typeId", item.getTypeId());
		serviceMaps.put("isdel", Const.Isdel_No);
    	String itemOption=itemService.getItemOption(serviceMaps,item.getId(),false);
    	String itemTypeOption=itemTypeService.getItemTypeOption(serviceMaps,item.getTypeId(), false);
    	String itemHelpCodeOption=itemService.getItemHelpCodeOption(serviceMaps,item.getId(),false);
    	String isOption=Const.getIsOption((byte)0, false);
    	int faIndex=itemTypeService.getItemTypeById(item.getTypeId()).getIndexFlag();
    	int sonIndex=item.getIndexFlag();
//    	List<Object[]> serviceList=new ArrayList<Object[]>();			
//    	serviceList.add(new Object[]{"service",itemOption,itemHelpCodeOption,item.getPice(),itemTypeOption,1,isOption,item.getPice(),item.getComment()});
		List<Object> list2=new ArrayList<Object>();
		list2.add("service");
		list2.add(itemOption);
		list2.add(itemHelpCodeOption);
		list2.add(item.getPice());
		list2.add(itemTypeOption);
		list2.add(1);
		list2.add(isOption);
		list2.add(item.getPice());
		list2.add(item.getComment());
		list2.add(faIndex);
		list2.add(sonIndex);
//		freezer1.getTypeId();//冰柜类型ID
		json.put("serviceList", list2);
		return json;
    }
    
    
    

    /**
     * 火化炉调度页面
     * @return
     */
    @RequestMapping(params = "method=editFurnace")
    public ModelAndView editFurnace(){
    	Map<String,Object> model=new HashMap<String,Object>();
		model=getModel(model);
		int time=getInt("time");
		Date date = DateTools.getThisDate();
		//判断页面是否有传日期回来，是则赋值给date
		if(getDate("current_time")!=null){
			date = getDate("current_time");
		}
		if (time!=0) {
			date=DateTools.getDayAfterDay(new java.sql.Date(date.getTime()), time, Calendar.DATE);
		}
		model.put("date", date);
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("furnaceBeginDate", DateTools.getDayFirstTime(new java.sql.Date(date.getTime())));
		maps.put("furnaceEndDate", DateTools.getDayEndTime(new java.sql.Date(date.getTime())));
		List<FurnaceRecord> frList = furnaceService.getFurnaceRecordList(maps);
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] dOb = new Object[25];		
		dOb[0]="炉号";
		for (int i = 0; i < dOb.length-1; i++) {
			dOb[i+1]=DateTools.getHourAfterHour(new Date(date.getTime()), 3, i, Calendar.HOUR);
		}
		list.add(dOb);
		Object[] dObTen = new Object[25];		
		dObTen[0]="炉号";
		for (int i = 0; i < dObTen.length-1; i++) {
			dObTen[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,20,Calendar.HOUR);
		}
		Object[] dObNine = new Object[25];		
		dObNine[0]="炉号";
		for (int i = 0; i < dObNine.length-1; i++) {
			dObNine[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,30,Calendar.HOUR);
		}
		Object[] dObEight = new Object[25];		
		dObEight[0]="炉号";
		for (int i = 0; i < dObEight.length-1; i++) {
			dObEight[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,40, Calendar.HOUR);
		}
		for (Furnace furnace : furnaceList) {
			Object[] o = new Object[dOb.length];
			o[0] = furnace;
			list.add(o);
		}
		for (FurnaceRecord furnaceRecord : frList) {
			Timestamp endTime = furnaceRecord.getEndTime();
			Timestamp orderTime = furnaceRecord.getOrderTime();
			byte flag=furnaceRecord.getFlag();
			if (endTime != null && !(endTime.equals(""))) {
				String endTimeStr = DateTools.getTimeFormatString("mm", endTime);
				if (endTimeStr.equals("30")||"20".equals(endTimeStr)||"40".equals(endTimeStr)) {
					endTime = DateTools.quzheng(endTime);
				}
			}
			if (orderTime != null && !(orderTime.equals(""))) {
				String orderTimeStr = DateTools.getTimeFormatString("mm", orderTime);
				if (orderTimeStr.equals("30")||"20".equals(orderTimeStr)||"40".equals(orderTimeStr)) {
					orderTime = DateTools.quzheng(orderTime);
				}
			}
			int row = 0;//行
			int col = 0;//列
			int endCol = 0;//列
			for (int i = 1; i < list.size(); i++) {
				Object[] o = list.get(i);
				Furnace furnace = (Furnace) o[0];
				if (furnace.getId().equals(furnaceRecord.getFurnaceId())) {
					row = i;
					break;
				}
			}
			//对比日期，确认列数
			for(int d=1;d<dOb.length;d++) {
				Date da = (Date)dOb[d];
				if (endTime != null && !(endTime.equals(""))) {
					if(DateTools.isSameHour(da, new Date(furnaceRecord.getBeginTime().getTime()))) {
						col = d;
					}
					if(DateTools.isSameHour(da, new Date(endTime.getTime()))){
						endCol = d;
					}
				} else {
					if (orderTime != null && !(orderTime.equals(""))) {
						if(DateTools.isSameHour(da, new Date(orderTime.getTime()))) {
							col = d;
						}
					}
				}
			}
			if(row!=0){
				Object[] o = (Object[])list.get(row);
				if (furnaceRecord.getEndTime() != null && !(furnaceRecord.getEndTime().equals(""))) {
					if(endCol==0){
						endCol = dOb.length-1;
					}
					if(col==0){
						col=1;
					}
					for(int x=col;x<=endCol;x++){
						//业务需求：如果维修与业务同时存在，优先显示业务
						if(flag!=Const.IsFlag_Decrate){//如果不是维修，直接显示
							o[x] = furnaceRecord;
						}
						if(flag==Const.IsFlag_Decrate && o[x]== null){//如果是维修，加条件：格子为空，才显示维修
							o[x] = furnaceRecord;
						}
					}
				} else {
					if (col != 0) {
						o[col] = furnaceRecord;
					}
				}
				
			}
		}
		model.put("IsFlag_Lock", Const.IsFlag_Lock);
		model.put("IsFlag_Yse", Const.IsFlag_Yse);
		model.put("IsFlag_Decrate", Const.IsFlag_Decrate);
		model.put("IsFlag_Wc", Const.IsFlag_Wc);
		model.put("IsFlag_Jx", Const.IsFlag_Jx);
		model.put("method", list);
		model.put("list", list);
		model.put("dOb", dOb);
		model.put("dObTen",dObTen);
		model.put("dObNine",dObNine);
		model.put("dObEight",dObEight);
		model.put("list_size", list.size());
		if (getString("where").equals("changeFur")) {
			return new ModelAndView("ye/commissionOrder/editFurnace2",model);//入炉转炉时候用
		} else {
			return new ModelAndView("ye/commissionOrder/editFurnace",model);
		}
    }
    
    
    
    /**
     * 火化炉点击
     * @return
     */
    @RequestMapping(params = "method=furnace_click")
    @ResponseBody
    public JSONObject furnace_click(){
    	JSONObject json = new JSONObject();
    	Date date = getDate("date");
    	int col=getInt("col");//列
    	int row=getInt("row");
		//判断页面是否有传日期回来，是则赋值给date
		Map<String, Object> map = new HashMap<String, Object>();
		String specialId = furnaceService.getSpecialId();
		map.put("id", specialId);
		map.put("isdel", Const.Isdel_No);//No是启用
		List<Furnace> furnaceList = furnaceService.getFurnaceList(map,"");
		Object[] dOb = new Object[25];
		for (int i = 0; i < dOb.length-1; i++) {
			dOb[i]=DateTools.getHourAfterHour(new Date(date.getTime()), 3, i, Calendar.HOUR);
		}
		
		Object[] dObTen = new Object[25];		
		dObTen[0]="炉号";
		for (int i = 0; i < dObTen.length-1; i++) {
			dObTen[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,20,Calendar.HOUR);
		}
		Object[] dObNine = new Object[25];		
		dObNine[0]="炉号";
		for (int i = 0; i < dObNine.length-1; i++) {
			dObNine[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,30,Calendar.HOUR);
		}
		Object[] dObEight = new Object[25];		
		dObEight[0]="炉号";
		for (int i = 0; i < dObEight.length-1; i++) {
			dObEight[i+1]=DateTools.getHourAfterHourHalf(new Date(date.getTime()), 2, i,40, Calendar.HOUR);
		}
		
		
		Date date1=(Date)dOb[col-1];
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String time=sdf.format(date1);
		Furnace furnace=furnaceList.get(row-1);
		
		if("8".equals(furnace.getCode())){
			Date date2=(Date)dObEight[col];
			time=sdf.format(date2);
		}
		if("9".equals(furnace.getCode())){
			Date date2=(Date)dObNine[col];
			time=sdf.format(date2);
		}
		if("10".equals(furnace.getCode())){
			Date date2=(Date)dObTen[col];
			time=sdf.format(date2);
		}
		json.put("time", time);
		json.put("id", furnace.getId());
		json.put("name", furnace.getName());
		//临时锁定前进行判断是否已被锁定或占用
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("furnaceId", furnace.getId());
		paramMap.put("fireTime", time);
		List<FurnaceRecord> list=furnaceRecordService.getFurnaceRecordList(paramMap, null);
		if(list.size()>0){
			setJsonByFail(json, "锁定失败，该火化炉已被锁定，请重新选择！");
			return json;
		}
		//添加临时锁定功能
		//注：String的类型必须形如： yyyy-mm-dd hh:mm:ss[.f...] 这样的格式，中括号表示可选，否则报错！！！
		SimpleDateFormat newsdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String lockTime=newsdf.format(date1);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		String tsStr = lockTime;
		try {
			ts = Timestamp.valueOf(tsStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		FurnaceRecord furnaceRecord=new FurnaceRecord();
		furnaceRecord.setId(UuidUtil.get32UUID());
		furnaceRecord.setCreateUserId(getCurrentUser().getUserId());
		furnaceRecord.setFurnaceId(furnace.getId());
		furnaceRecord.setOrderTime(ts);
		furnaceRecord.setFlag(Const.IsFlag_LSSD);
		furnaceRecordService.addFurnaceRecord(furnaceRecord);
		return json;
    }
    
    
    
    /**
     * 判断是否为同一天
     * @param day1
     * @param day2
     * @return
     */
    private boolean isSomeDay(Date day1,Date day2){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ds1 = sdf.format(day1);
        String ds2 = sdf.format(day2);
        if (ds1.equals(ds2)) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * 判断上午下午
     * @param time
     * @return 1：上午 2：下午
     */
    private byte isPmOrAm(Timestamp time){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(time.getTime()));
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if(hour>=0 && hour<=12){
			return (byte)1;
		}else{
			return (byte)2;
		}
		
    }
    
    /**
     * 解锁
     * @return
     */
//    @RequestMapping(params = "method=unlock")
//    @SystemControllerLog(description = "火化委托单解锁")
//    @ResponseBody
//    public JSONObject unlock(){
//    	JSONObject json = new JSONObject();
//		List<Freezer> freezerList = freezerService.getFreezerList(null,"index_flag asc,name asc");
//		for (int i = 0; i < freezerList.size(); i++) {
//			Freezer freezer2=freezerList.get(i);
//			if (freezer2.getFlag()==Const.IsFlag_Lock) {
//				if (freezer2.getUserName().equals(getCurrentUser().getName())||getCurrentUser().getName()==freezer2.getUserName()) {
//					freezer2.setFlag(Const.IsFlag_No);
//					freezerService.updateFreezer(freezer2);
//				}
//			}
//		}
//		return json;
//    }
    
	/**
	 * 图片上传
	 * @param file
	 * @return
	 * @throws Exception
	 * 
	 */
	@RequestMapping(params = "method=fileupload")  
	@SystemControllerLog(description = "火化委托单图片上传")
	@ResponseBody
    public String fildUpload(@RequestParam(value="file",required=false) MultipartFile file)throws Exception{  
		CommonsMultipartFile cf= (CommonsMultipartFile)file; 
	    DiskFileItem fi = (DiskFileItem)cf.getFileItem(); 
	    File f = fi.getStoreLocation();
		Image img = null; 
		try {  
	        img = ImageIO.read(f);  
	        if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
	        	System.out.println("img:"+img);
	        	System.out.println("imgW:"+img.getWidth(null));
	        	System.out.println("imgH:"+img.getHeight(null));
	            return "";  
	        } else {
	        	//文件存储地址
	    		Calendar cal = Calendar.getInstance();
	    		cal.setTime(new Date());
	    		int m = cal.get(Calendar.MONTH)+1;
	    		String fp = File.separator + "upload"+File.separator+cal.get(Calendar.YEAR)+File.separator+m+File.separator+cal.get(Calendar.DATE); 
	    		//获得物理路径webapp所在路径 
	    		ServletContext context =getRequest().getSession().getServletContext();
	    		String filePath = context.getRealPath("");
	    		int i = 0;
	    		while (i < 3) {
	    			int lastFitst = filePath.lastIndexOf(File.separator);
	    			filePath = filePath.substring(0, lastFitst);
	    			i++;
	    		}
	    		filePath += fp;
	    		File saveDirFile = new File(filePath);
	    		//检查文件夹是否存在，存在返回false，不存在返回true
	    		if (!saveDirFile.exists()) {
	    		saveDirFile.mkdirs();
	    		}
	    		//改名后的文件名
	            String nne="";
	            //相对路径
	            String xpath="";
	            //绝对路径
	            String rpath="";
	            if(!file.isEmpty()){  
	                //生成uuid作为文件名称  
	                String uuid = UUID.randomUUID().toString().replaceAll("-","");  
	                //获得文件类型（可以判断如果不是图片，禁止上传）  
	                String contentType=file.getContentType();  
	                //获得文件后缀名称  
	                String imageName=contentType.substring(contentType.indexOf("/")+1);  
	                nne=""+uuid+"."+imageName;  
	                rpath=filePath+File.separator+nne;
	                xpath= fp+File.separator+nne;
	                file.transferTo(new File(rpath));  
	            }  
	            return xpath;
	        }
	    } catch (Exception e) { 
	    	e.printStackTrace();
	        return "";  
	    } finally {  
	        img = null;  
	    }  
    } 
	/**
	 * @param a 判断的状态
	 * @return 是或否
	 */
	public static String charge(byte a){
		String b="否";
		if(a==1){
			b="是";
		}
		return b;
	}
}
