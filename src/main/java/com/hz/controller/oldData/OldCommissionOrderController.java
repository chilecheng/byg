package com.hz.controller.oldData;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.oldData.OldData;
import com.hz.service.oldData.OldDataService;
import com.hz.util.Const;

/**
 * 老系统数据数据查询
 * @author jgj
 */
@Controller
@RequestMapping("/oldCommissionOrder.do")
public class OldCommissionOrderController extends BaseController {
	//老系统数据查询方法
	@Autowired
	private OldDataService oldDataService;
	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "oldCommissionOrder.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listOldCommissionOrder();
	}

	/**
	 * 老数据--综合查询列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listOldCommissionOrder() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		// 填入查询内容

		Map<String, Object> maps = new HashMap<String, Object>();
		//死者姓名
		String name = getString("name");
		maps.put("name", name);
		//卡号
		String cardCode = getString("cardCode");
		maps.put("cardCode", cardCode);
		//死亡时间
		String dTime=getString("dTime");
		String dTimeEnd=getString("dTimeEnd");//结束
		if(!"".equals(dTime) && dTime!=null){
			maps.put("dTimeB", dTime);
		}
		if(!"".equals(dTimeEnd) && dTimeEnd!=null){
			maps.put("dTimeE", dTimeEnd+"23:59:59");
		}
		//接尸地址
		String  pickAddr =getString("pickAddr");
		maps.put("pickAddress", pickAddr);
		//接尸单位
		String  corpseUnitId= getString("corpseUnitId");
		maps.put("transportUnit", corpseUnitId);
		//死亡原因
		String deadReasonId = getString("deadReasonId");
		maps.put("death", deadReasonId);
		//死亡类型
		String  deadTypeId =  getString("deadTypeId");
		maps.put("deathType", deadTypeId);
		//死者户籍
		String toponymId = getString("toponymId");
		maps.put("add", toponymId);
		//业务号
		String deadID = getString("deadID");
		maps.put("deadID", deadID);
		model.put("deadID", deadID);
		//身份证号
		String  certificateCode= getString("certificateCode");
		maps.put("idCard", certificateCode);
		model.put("certificateCode",certificateCode );
		//性别
		String sex= getString("sex");
		maps.put("sex", getString("sex"));
		//家属姓名
		String fName = getString("fName");
		maps.put("fName", fName);
		model.put("fName", fName);
		//家属 电话
		String  fPhone= getString("fPhone");
		maps.put("fPhone", fPhone);
		model.put("fPhone",  fPhone);
		//证明单位
		String proveUnitId=getString("proveUnitId");
		maps.put("certifyingAuthority", proveUnitId);
		model.put("proveUnitId", proveUnitId);
		
		
		//火化预约时间
		String cremationTime=getString("cremationTime");
		String cremationTimeEnd=getString("cremationTimeEnd");
		if(!"".equals(cremationTime) && cremationTime!=null){
			maps.put("cremationTime", cremationTime);
		}
		if(!"".equals(cremationTimeEnd) && cremationTimeEnd!=null){
			maps.put("cremationTimeEnd", cremationTimeEnd+"23:59:59");
		}
		//登记日期
		String creatTime=getString("creatTime");
		String creatTimeEnd=getString("creatTimeEnd");
		if(!"".equals(creatTime) && creatTime!=null){
			maps.put("creatTime", creatTime);
		}
		if(!"".equals(creatTimeEnd) && creatTimeEnd!=null){
			maps.put("creatTimeEnd", creatTimeEnd+"23:59:59");
		}
		//到馆 时间
		String arriveTime=getString("arriveTime");
		String arriveTimeEnd=getString("arriveTimeEnd");
		if(!"".equals(arriveTime) && arriveTime!=null){
			maps.put("arriveTime", arriveTime);
		}
		if(!"".equals(arriveTimeEnd) && arriveTimeEnd !=null){
			maps.put("arriveTimeEnd", arriveTimeEnd+"23:59:59");
		}
		//冰柜号
		String freezerName = getString("freezerName");
		maps.put("freezerName",freezerName);
		model.put("freezerName", freezerName);
		//灵堂号
		String  mourningName =  getString("mourningName");
		maps.put("mourningName",mourningName);
		model.put("mourningName", mourningName );
		//告别厅号
		String  farewellName = getString("farewellName");
		maps.put("farewellName", farewellName);
		model.put("farewellName",farewellName );
		//火化炉类型
		String furance_flag =getString("furnaceTypeId");
		maps.put("furnaceType", furance_flag);
		//火化炉号
		String  furnaceName = getString("furnaceName");
		maps.put("furnaceName", furnaceName);
		model.put("furnaceName", furnaceName);
		//火化时间
		String furnaceTime=getString("furnaceTime");
		String furnaceTimeEnd=getString("furnaceTimeEnd");
		if(!"".equals(furnaceTime) && furnaceTime!=null){
			maps.put("furnaceTime", furnaceTime);
		}
		if(!"".equals(furnaceTimeEnd) && furnaceTimeEnd!=null){
			maps.put("furnaceTimeEnd", furnaceTimeEnd+"23:59:59");
		}
		
		
		PageInfo<OldData> page = oldDataService.getgetOldDataPageInfoPageInfo(maps, pages[0], pages[1],"");
		String corpseUnitOption = Const.getOldPickUnitOption(corpseUnitId, false);
		String deathOption = Const.getOldDeathOption(deadReasonId, false);
		String deadTypeOption = Const.getOldDeadTypeOption(deadTypeId, false);
		String toponymOption=Const.getOldTownOption(toponymId,false);
		String sexOption = Const.getOldSexOption(sex, false);
		String furnaceTypeOption  = Const.getOldFurnaceTypeOption(furance_flag,false);
//		String proveUnitOption=proveUnitService.getProveUnitOption(proveUnitId,false);
		model.put("sexOption", sexOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("deadReasonOption", deathOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("toponymOption", toponymOption);
		model.put("furnaceTypeOption", furnaceTypeOption);
//		model.put("proveUnitOption", proveUnitOption);
		model.put("page", page);
		model.put("method", "list");
		model.put("name",name );
		model.put("cardCode",cardCode);
		return new ModelAndView("oldData/listOldCommissionOrder", model);
	}
	/**
	 * 点击姓名查看信息（打印）
	 * @return
	 */
	@RequestMapping(params = "method=show")
	public ModelAndView showOldCommissionOrder() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String id=getString("id");
		OldData old=oldDataService.getOldDataByID(id);
		model.put("oldData", old);
		model.put("url", "oldCommissionOrder.do");
		return new ModelAndView("oldData/readOldCommissionOrder", model);
	}
}
