package com.hz.controller.oldData;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.oldData.OldData;
import com.hz.entity.oldData.SingleData;
import com.hz.service.oldData.OldDataService;
import com.hz.util.Const;

/**
 * 老系统之前的单机数据查询
 * @author jgj
 */
@Controller
@RequestMapping("/singleCommissionOrder.do")
public class SingleCommissionOrderController extends BaseController {
	//老系统数据查询方法
	@Autowired
	private OldDataService oldDataService;
	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "singleCommissionOrder.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listOldCommissionOrder();
	}

	/**
	 * 单机数据--综合查询列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listOldCommissionOrder() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		// 填入查询内容

		Map<String, Object> maps = new HashMap<String, Object>();
		//死者姓名
		String name = getString("name");
		maps.put("name", name);
		model.put("name", name);
		//死亡时间
		String dTime=getString("dTime");
		String dTimeEnd=getString("dTimeEnd");//结束
		if(!"".equals(dTime) && dTime!=null){
			maps.put("dTimeB", dTime);
		}
		if(!"".equals(dTimeEnd) && dTimeEnd!=null){
			maps.put("dTimeE", dTimeEnd+"23:59:59");
		}
		//死亡地址
		String  dieAddress =getString("dieAddress");
		maps.put("dieAddress", dieAddress);
		model.put("dieAddress", dieAddress);
		//死亡原因
		String death = getString("death");
		maps.put("death", death);
		model.put("death", death);
		//死者户籍
		String registerAdd = getString("registerAdd");
		maps.put("registerAdd", registerAdd);
		model.put("registerAdd", registerAdd);
		//业务号
		String deadID = getString("deadID");
		maps.put("deadID", deadID);
		model.put("deadID", deadID);
		//性别
		String sex= getString("sex");
		maps.put("sex", getString("sex"));
		//家属姓名
		String fName = getString("fName");
		maps.put("fName", fName);
		model.put("fName", fName);
		//家属 电话
		String  fPhone= getString("fPhone");
		maps.put("fPhone", fPhone);
		model.put("fPhone",  fPhone);
		//证明单位
		String certifyingAuthority=getString("certifyingAuthority");
		maps.put("certifyingAuthority", certifyingAuthority);
		model.put("certifyingAuthority", certifyingAuthority);
		
		
		//登记日期
		String creatTime=getString("creatTime");
		String creatTimeEnd=getString("creatTimeEnd");
		if(!"".equals(creatTime) && creatTime!=null){
			maps.put("creatTime", creatTime);
		}
		if(!"".equals(creatTimeEnd) && creatTimeEnd!=null){
			maps.put("creatTimeEnd", creatTimeEnd+"23:59:59");
		}
		//到馆 时间
		String arriveTime=getString("arriveTime");
		String arriveTimeEnd=getString("arriveTimeEnd");
		if(!"".equals(arriveTime) && arriveTime!=null){
			maps.put("arriveTime", arriveTime);
		}
		if(!"".equals(arriveTimeEnd) && arriveTimeEnd !=null){
			maps.put("arriveTimeEnd", arriveTimeEnd+"23:59:59");
		}
		//火化时间
		String furnaceTime=getString("furnaceTime");
		String furnaceTimeEnd=getString("furnaceTimeEnd");
		if(!"".equals(furnaceTime) && furnaceTime!=null){
			maps.put("furnaceTime", furnaceTime);
		}
		if(!"".equals(furnaceTimeEnd) && furnaceTimeEnd!=null){
			maps.put("furnaceTimeEnd", furnaceTimeEnd+"23:59:59");
		}
		
		
		PageInfo<SingleData> page = oldDataService.getgetSingleDataPageInfoPageInfo(maps, pages[0], pages[1],"");
		String sexOption = Const.getSingleSexOption(sex, false);
		model.put("sexOption", sexOption);
		model.put("page", page);
		model.put("method", "list");
		return new ModelAndView("oldData/listSingleCommissionOrder", model);
	}
	/**
	 * 点击姓名查看信息（打印）
	 * @return
	 */
	@RequestMapping(params = "method=show")
	public ModelAndView showOldCommissionOrder() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String id=getString("id");
		SingleData single=oldDataService.getSingleDataByID(id);
		model.put("single", single);
		model.put("url", "singleCommissionOrder.do");
		return new ModelAndView("oldData/readSingleCommissionOrder", model);
	}
}
