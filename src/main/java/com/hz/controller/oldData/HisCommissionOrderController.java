package com.hz.controller.oldData;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.hz.controller.system.BaseController;
import com.hz.entity.oldData.HisData;
import com.hz.service.base.CorpseUnitService;
import com.hz.service.base.DeadReasonService;
import com.hz.service.base.DeadTypeService;
import com.hz.service.base.ToponymService;
import com.hz.service.oldData.OldDataService;
import com.hz.util.Const;

/**
 * 6.5系统历史数据数据查询
 * @author jgj
 */
@Controller
@RequestMapping("/hisCommissionOrder.do")
public class HisCommissionOrderController extends BaseController {
	//老系统数据查询方法
	@Autowired
	private OldDataService oldDataService;
	@Autowired
	private ToponymService toponymService;
	@Autowired
	private CorpseUnitService corpseUnitService;
	@Autowired
	private DeadReasonService deadReasonService;
	@Autowired
	private DeadTypeService deadTypeService;
	
	@ModelAttribute
	public void populateModel(Model model) {
		model.addAttribute("url", "hisCommissionOrder.do");
	}

	@RequestMapping
	public ModelAndView unspecified() {
		return listHisCommissionOrder();
	}

	/**
	 * 6.5系统历史数据--综合查询列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "method=list")
	public ModelAndView listHisCommissionOrder() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		// 获得页面页码信息
		int[] pages = getPage();
		// 填入查询内容

		Map<String, Object> maps = new HashMap<String, Object>();
		//死者姓名
		String name = getString("name");
		maps.put("name", name);
		//卡号
		String cardCode = getString("cardCode");
		maps.put("cardCode", cardCode);
		//死亡时间
		String dTime=getString("dTime");
		String dTimeEnd=getString("dTimeEnd");//结束
		if(!"".equals(dTime) && dTime!=null){
			maps.put("dTimeB", dTime);
		}
		if(!"".equals(dTimeEnd) && dTimeEnd!=null){
			maps.put("dTimeE", dTimeEnd+"23:59:59");
		}
		//接尸地址
		String  pickAddr =getString("pickAddr");
		maps.put("pickAddress", pickAddr);
		//接尸单位
		String  corpseUnitId= getString("corpseUnitId");
		maps.put("transportUnit", corpseUnitId);
		//死亡原因
		String deadReasonId = getString("deadReasonId");
		maps.put("death", deadReasonId);
		//死亡类型
		String  deadTypeId =  getString("deadTypeId");
		maps.put("deathType", deadTypeId);
		//死者户籍
		String toponymId = getString("toponymId");
		maps.put("toponymId", toponymId);
		//业务号
		String code = getString("deadID");
		maps.put("code", code);
		model.put("code", code);
		//身份证号
		String  certificateCode= getString("certificateCode");
		maps.put("idCard", certificateCode);
		model.put("certificateCode",certificateCode );
		//性别
		byte sex= getByte("sex");
		maps.put("sex", sex);
		//家属姓名
		String fName = getString("fName");
		maps.put("fName", fName);
		model.put("fName", fName);
		//家属 电话
		String  fPhone= getString("fPhone");
		maps.put("fPhone", fPhone);
		model.put("fPhone",  fPhone);
		//证明单位
		String proveUnitId=getString("proveUnitId");
		maps.put("proveUnitId", proveUnitId);
		model.put("proveUnitId", proveUnitId);
		
		
		//火化预约时间
		String cremationTime=getString("cremationTime");
		String cremationTimeEnd=getString("cremationTimeEnd");
		if(!"".equals(cremationTime) && cremationTime!=null){
			maps.put("cremationTime", cremationTime);
		}
		if(!"".equals(cremationTimeEnd) && cremationTimeEnd!=null){
			maps.put("cremationTimeEnd", cremationTimeEnd+"23:59:59");
		}
		//登记日期
		String creatTime=getString("creatTime");
		String creatTimeEnd=getString("creatTimeEnd");
		if(!"".equals(creatTime) && creatTime!=null){
			maps.put("creatTime", creatTime);
		}
		if(!"".equals(creatTimeEnd) && creatTimeEnd!=null){
			maps.put("creatTimeEnd", creatTimeEnd+"23:59:59");
		}
		//到馆 时间
		String arriveTime=getString("arriveTime");
		String arriveTimeEnd=getString("arriveTimeEnd");
		if(!"".equals(arriveTime) && arriveTime!=null){
			maps.put("arriveTime", arriveTime);
		}
		if(!"".equals(arriveTimeEnd) && arriveTimeEnd !=null){
			maps.put("arriveTimeEnd", arriveTimeEnd+"23:59:59");
		}
		//冰柜号
		String freezerName = getString("freezerName");
		maps.put("freezerName",freezerName);
		model.put("freezerName", freezerName);
		//灵堂号
		String  mourningName =  getString("mourningName");
		maps.put("mourningName",mourningName);
		model.put("mourningName", mourningName );
		//告别厅号
		String  farewellName = getString("farewellName");
		maps.put("farewellName", farewellName);
		model.put("farewellName",farewellName );
		//火化炉类型
		byte furance_flag =getByte("furnaceTypeId");
		maps.put("furnaceType", furance_flag);
		//火化炉号
		String  furnaceName = getString("furnaceName");
		maps.put("furnaceName", furnaceName);
		model.put("furnaceName", furnaceName);
		//火化时间
		String furnaceTime=getString("furnaceTime");
		String furnaceTimeEnd=getString("furnaceTimeEnd");
		if(!"".equals(furnaceTime) && furnaceTime!=null){
			maps.put("furnaceTime", furnaceTime);
		}
		if(!"".equals(furnaceTimeEnd) && furnaceTimeEnd!=null){
			maps.put("furnaceTimeEnd", furnaceTimeEnd+"23:59:59");
		}
		
		
		PageInfo<HisData> page = oldDataService.getHisDataPageInfoPageInfo(maps, pages[0], pages[1],"");
//		String corpseUnitOption = Const.getOldPickUnitOption(corpseUnitId, false);
//		String deathOption = Const.getOldDeathOption(deadReasonId, false);
//		String deadTypeOption = Const.getOldDeadTypeOption(deadTypeId, false);
//		String toponymOption=Const.getOldTownOption(toponymId,false);
//		String sexOption = Const.getOldSexOption(sex, false);
//		String furnaceTypeOption  = Const.getOldFurnaceTypeOption(furance_flag,false);
		String corpseUnitOption = corpseUnitService.getCorpseUnitOption(corpseUnitId, false);
		String deadReasonOption = deadReasonService.getDeadReasonOption(deadReasonId, false);
		String deadTypeOption = deadTypeService.getDeadTypeOption(deadTypeId, false);
		String toponymOption=toponymService.getToponymOption(toponymId,false);
		String sexOption = Const.getSexOption(sex, false);
		String furnaceTypeOption  = Const.getFuranceTypeOption(furance_flag,false);
		model.put("sexOption", sexOption);
		model.put("corpseUnitOption", corpseUnitOption);
		model.put("deadReasonOption", deadReasonOption);
		model.put("deadTypeOption", deadTypeOption);
		model.put("toponymOption", toponymOption);
		model.put("furnaceTypeOption", furnaceTypeOption);
		model.put("page", page);
		model.put("method", "list");
		model.put("name",name );
		model.put("cardCode",cardCode);
		return new ModelAndView("oldData/listHisCommissionOrder", model);
	}
	/**
	 * 点击姓名查看信息（打印）
	 * @return
	 */
	@RequestMapping(params = "method=show")
	public ModelAndView showHisCommissionOrder() {
		Map<String, Object> model = new HashMap<String, Object>();
		model = getModel(model);
		String id=getString("id");
		HisData his=oldDataService.getHisDataByID(id);
		model.put("hisData", his);
		model.put("url", "hisCommissionOrder.do");
		return new ModelAndView("oldData/readHisCommissionOrder", model);
	}
}
