package com.hz.entity.cemetery;

import java.sql.Date;

/**
 * 陵园购买记录
 *
 * @author jgj
 */
public class BuyRecord {
    /**
     * id
     */
    private String id;
    /**
     * 持证人姓名
     */
    private String cardHolder;
    /**
     * 墓穴证号
     */
    private String tombsCard;
    /**
     * 家庭地址
     */
    private String address;
    /**
     * 身份证号
     */
    private String IDNumber;
    /**
     * 联系电话
     */
    private String contactNumber;
    /**
     * 手机号
     */
    private String phoneNumber;
    /**
     * 备注信息
     */
    private String comment;
    /**
     * 陵园ID
     */
    private String cemeteryId;
    /**
     * 区域ID
     */
    private String areaId;
    /**
     * 排ID
     */
    private String rowId;
    /**
     * 墓穴ID
     */
    private String tombId;
    /**
     * 经办人
     */
    private String createPeople;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 有效期至
     */
    private Date validityPeriod;
    /**
     * 墓穴名称
     */
    private String graveInfo;
    /**
     * 墓穴费金额
     */
    private String totalAmount;
    /**
     * 雕刻安放封龙门迁移费金额
     */
    private String total;
    /**
     * 雕刻安放金额
     */
    private String crave;
    /**
     * 封龙门迁移费金额
     */
    private String dragon;
    /**
     * 墓穴维护金额
     */
    private String payMoney;
    /**
     * 当前已售墓穴数量
     */
    private String numbers;
    /**
     * 墓穴使用者姓名
     */
    private String userName;
    /**
     * 墓穴信息
     */
    private Tomb tomb;
    /**
     * 维护费记录
     */
    private MaintainRecord maintainRecord;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(Date validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getTombsCard() {
        return tombsCard;
    }

    public void setTombsCard(String tombsCard) {
        this.tombsCard = tombsCard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIDNumber() {
        return IDNumber;
    }

    public void setIDNumber(String iDNumber) {
        IDNumber = iDNumber;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCemeteryId() {
        return cemeteryId;
    }

    public void setCemeteryId(String cemeteryId) {
        this.cemeteryId = cemeteryId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getTombId() {
        return tombId;
    }

    public void setTombId(String tombId) {
        this.tombId = tombId;
    }

    public String getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(String createPeople) {
        this.createPeople = createPeople;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getGraveInfo() {
        return graveInfo;
    }

    public void setGraveInfo(String graveInfo) {
        this.graveInfo = graveInfo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(String payMoney) {
        this.payMoney = payMoney;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public String getDragon() {
        return dragon;
    }

    public void setDragon(String dragon) {
        this.dragon = dragon;
    }

    public String getCrave() {
        return crave;
    }

    public void setCrave(String crave) {
        this.crave = crave;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Tomb getTomb() {
        return tomb;
    }

    public void setTomb(Tomb tomb) {
        this.tomb = tomb;
    }

    public MaintainRecord getMaintainRecord() {
        return maintainRecord;
    }

    public void setMaintainRecord(MaintainRecord maintainRecord) {
        this.maintainRecord = maintainRecord;
    }
}
