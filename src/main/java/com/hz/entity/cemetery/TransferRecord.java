package com.hz.entity.cemetery;

import com.hz.util.Const;

import java.sql.Date;


/**
 * 墓穴迁出记录
 *
 * @author jgj
 */
public class TransferRecord {
    //id
    private String id;
    //申请人
    private String applicant;
    //申请人电话
    private String phoneNumber;
    //迁出人姓名
    private String userName;
    //迁出时间
    private Date transferTime;
    //操作时间
    private Date createTime;
    //操作人
    private String createPeople;
    //预约墓穴ID
    private String tombId;
    //备注信息
    private String comment;
    //是否回收墓穴证
    private byte isRecovery;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(Date transferTime) {
        this.transferTime = transferTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(String createPeople) {
        this.createPeople = createPeople;
    }

    public String getTombId() {
        return tombId;
    }

    public void setTombId(String tombId) {
        this.tombId = tombId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public byte getIsRecovery() {
        return isRecovery;
    }

    public void setIsRecovery(byte isRecovery) {
        this.isRecovery = isRecovery;
    }

    /**
     * 获取是否回收墓穴证名称
     *
     * @return
     */
    public String getIsRecoveryName() {
        return Const.getIsRecovery(this.isRecovery);
    }
}
