package com.hz.entity.cemetery;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 陵园信息表
 * @author jgj
 */
public class Cemetery {
	//陵园id
	private String id;
	//别名
	private String name;
	//创建时间
	private Timestamp createTime;
	//创建人
	private String createPeople;
	//陵园地址
	private String address;
	//照片地址
	private String photo;
	//顺序号
	private int index;
	//启用标志
	private byte isdel;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCreatePeople() {
		return createPeople;
	}
	public void setCreatePeople(String createPeople) {
		this.createPeople = createPeople;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * 是否启用标志
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
}
