package com.hz.entity.cemetery;

import java.sql.Date;

/**
 * @Classname CemeteryBack
 * @Description TODO
 * @Date 2019/6/27 17:18
 * @Created by ZhixiangWang
 */
public class CemeteryBack {
    //ID
    private String id;
    //墓穴ID
    private String tombId;
    //购买者姓名
    private String buyerName;
    //死者姓名
    private String deadName;
    //开始使用时间
    private Date startTime;
    //退回时间
    private Date endTime;
    //操作人
    private String operator;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTombId() {
        return tombId;
    }

    public void setTombId(String tombId) {
        this.tombId = tombId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getDeadName() {
        return deadName;
    }

    public void setDeadName(String deadName) {
        this.deadName = deadName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
