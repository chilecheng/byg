package com.hz.entity.cemetery;

import java.sql.Date;


/**
 * 墓穴预约记录
 * @author jgj
 */
public class AppointmentRecord {
	/**
	 * id
	 */
	private String id;
	/**
	 * 预约人
	 */
	private String name;
	/**
	 * 预约到期
	 */
	private Date endTime;
	/**
	 * 预约墓穴ID
	 */
	private String tombId;
	/**
	 * 预约人身份证号
	 */
	private String idCard;
	/**
	 * 预约人电话
	 */
	private String phoneNumber;
	/**
	 * 预约人地址
	 */
	private String address;
	/**
	 * 定金
	 */
	private double earnestMoney;
	/**
	 * 备注信息
	 */
	private String comment;
	/**
	 * 操作时间
	 */
	private Date createTime;
	/**
	 * 操作人姓名
	 */
	private String createPeople;
	/**
	 * 墓位名称
	 */
	private String tombName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getTombId() {
		return tombId;
	}

	public void setTombId(String tombId) {
		this.tombId = tombId;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getEarnestMoney() {
		return earnestMoney;
	}

	public void setEarnestMoney(double earnestMoney) {
		this.earnestMoney = earnestMoney;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreatePeople() {
		return createPeople;
	}

	public void setCreatePeople(String createPeople) {
		this.createPeople = createPeople;
	}

	public String getTombName() {
		return tombName;
	}

	public void setTombName(String tombName) {
		this.tombName = tombName;
	}

}
