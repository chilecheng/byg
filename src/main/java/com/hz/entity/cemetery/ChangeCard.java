package com.hz.entity.cemetery;

import java.sql.Date;
import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 换证补证
 * @author jgj
 */
public class ChangeCard {
	//id
	private String id;
	//换证类型
	private byte changeType;
	//身份证号
	private String IDCard;
	//身份证号 新
	private String newIDCard;
	//持证人姓名
	private String cardHolder;
	//持证人姓名 新
	private String newCardHolder;
	//墓穴证号
	private String tombsCard;
	//墓穴证号 新
	private String newTombsCard;
	//联系电话
	private String contactNumber;
	//联系电话 新
	private String newContactNumber;
	//备注信息
	private String comment;
	//墓穴ID
	private String tombId;
	//经办人
	private String createPeople;
	//创建时间
	private Date createTime;
	//有效期至
	private Date validityPeriod;
	//有效期至 新
	private Date newValidityPeriod;
	
	public byte getChangeType() {
		return changeType;
	}
	public void setChangeType(byte changeType) {
		this.changeType = changeType;
	}
	public String getIDCard() {
		return IDCard;
	}
	public void setIDCard(String iDCard) {
		IDCard = iDCard;
	}
	public String getNewIDCard() {
		return newIDCard;
	}
	public void setNewIDCard(String newIDCard) {
		this.newIDCard = newIDCard;
	}
	public String getNewCardHolder() {
		return newCardHolder;
	}
	public void setNewCardHolder(String newCardHolder) {
		this.newCardHolder = newCardHolder;
	}
	public String getNewTombsCard() {
		return newTombsCard;
	}
	public void setNewTombsCard(String newTombsCard) {
		this.newTombsCard = newTombsCard;
	}
	public String getNewContactNumber() {
		return newContactNumber;
	}
	public void setNewContactNumber(String newContactNumber) {
		this.newContactNumber = newContactNumber;
	}
	public Date getNewValidityPeriod() {
		return newValidityPeriod;
	}
	public void setNewValidityPeriod(Date newValidityPeriod) {
		this.newValidityPeriod = newValidityPeriod;
	}
	public Date getValidityPeriod() {
		return validityPeriod;
	}
	public void setValidityPeriod(Date validityPeriod) {
		this.validityPeriod = validityPeriod;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCardHolder() {
		return cardHolder;
	}
	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}
	public String getTombsCard() {
		return tombsCard;
	}
	public void setTombsCard(String tombsCard) {
		this.tombsCard = tombsCard;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getTombId() {
		return tombId;
	}
	public void setTombId(String tombId) {
		this.tombId = tombId;
	}
	public String getCreatePeople() {
		return createPeople;
	}
	public void setCreatePeople(String createPeople) {
		this.createPeople = createPeople;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	
}
