package com.hz.entity.cemetery;

import java.sql.Date;
import java.sql.Timestamp;


/**
 * 维护记录信息
 * @author jgj
 */
public class MaintainRecord {
	//维护记录ID
	private String id;
	//购买记录ID
	private String recordId;
	//开始时间
	private Date beginTime;
	//结束时间
	private Date endTime;
	//年限
	private int longTime;
	//缴费时间
	private Date payTime;
	//缴费人
	private String payName;
	//墓穴ID
	private String tombId;
	//发票号
	private String invoiceNumber;
	//交费金额
	private double payMoney;
	
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public double getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(double payMoney) {
		this.payMoney = payMoney;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getLongTime() {
		return longTime;
	}
	public void setLongTime(int longTime) {
		this.longTime = longTime;
	}
	public Date getPayTime() {
		return payTime;
	}
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public String getTombId() {
		return tombId;
	}
	public void setTombId(String tombId) {
		this.tombId = tombId;
	}
}
