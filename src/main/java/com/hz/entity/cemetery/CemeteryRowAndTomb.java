package com.hz.entity.cemetery;


import java.util.List;

import com.hz.util.Const;

/**
 * 陵园排与墓穴信息表
 * @author jgj
 */
public class CemeteryRowAndTomb {
	//排id
	private String id;
	//别名
	private String name;
	//区域ID
	private String areaId;
	//顺序号
	private int index;
	//启用标志
	private byte isdel;
	//陵园id
	private String cid;
	//墓穴实体类
	private List<Tomb> tomb;
	
	
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public List<Tomb> getTomb() {
		return tomb;
	}
	public void setTomb(List<Tomb> tomb) {
		this.tomb = tomb;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	
	/**
	 * 是否启用标志
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
}
