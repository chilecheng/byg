package com.hz.entity.cemetery;


import com.hz.util.Const;

/**
 * 陵园排信息表
 *
 * @author jgj
 */
public class CemeteryRow {
    //排id
    private String id;
    //别名
    private String name;
    //区域ID
    private String areaId;
    //备注说明
    private String comment;
    //照片地址
    private String photo;
    //顺序号
    private int index;
    //启用标志
    private byte isdel;
    //（排列是从左到右1 或是从右到左2）
    private int sort;
    //第几排
    private String row;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public byte getIsdel() {
        return isdel;
    }

    public void setIsdel(byte isdel) {
        this.isdel = isdel;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    /**
     * 是否启用标志
     *
     * @return
     */
    public String getIsdelName() {
        return Const.getIsdel(this.isdel);

    }
}
