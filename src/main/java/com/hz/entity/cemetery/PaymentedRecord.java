package com.hz.entity.cemetery;

import java.sql.Date;

/**
*Describtion:已支付墓穴缴费记录类

*Creatime:2019年6月25日

*Author:wujiayang

*Comment:数据来源 gm_payment_record表

**/
public class PaymentedRecord {

	//墓穴id
		private String id;
		//付款人姓名
		private String name;
		//购买记录ID
		private String recordId;
		//发票号
		private String invoiceNumber;
		//支付金额
		private double totalAmount;
		//支付时间
		private Date payTime;
		//墓穴名称
		private String graveInfo;
		
		public String getGraveInfo() {
			return graveInfo;
		}
		
		public void setGraveInfo(String graveInfo) {
			this.graveInfo = graveInfo;
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getRecordId() {
			return recordId;
		}
		public void setRecordId(String recordId) {
			this.recordId = recordId;
		}
		public String getInvoiceNumber() {
			return invoiceNumber;
		}
		public void setInvoiceNumber(String invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}
		public double getTotalAmount() {
			return totalAmount;
		}
		public void setTotalAmount(double totalAmount) {
			this.totalAmount = totalAmount;
		}
		public Date getPayTime() {
			return payTime;
		}
		public void setPayTime(Date payTime) {
			this.payTime = payTime;
		}
		@Override
		public String toString() {
			return "PaymentedRecord [id=" + id + ", name=" + name + ", recordId=" + recordId + ", invoiceNumber="
					+ invoiceNumber + ", totalAmount=" + totalAmount + ", payTime=" + payTime + "]";
		}
		
	
	
}
