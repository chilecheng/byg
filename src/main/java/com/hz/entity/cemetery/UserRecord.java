package com.hz.entity.cemetery;

import java.sql.Date;

import com.hz.util.Const;


/**
 * 墓穴使用者记录
 *
 * @author jgj
 */
public class UserRecord {
    /**
     * 使用记录ID
     */
    private String id;
    /**
     * 使用者姓名
     */
    private String userName;
    /**
     * 死亡类型
     */
    private byte deadType;
    /**
     * 性别
     */
    private byte sex;
    /**
     * 生于时间
     */
    private Date borthTime;
    /**
     * 死亡时间
     */
    private Date deadTime;
    /**
     * 封龙门时间
     */
    private Date longmenTime;
    /**
     * 安置时间
     */
    private Date releaseTime;
    /**
     * 墓穴ID
     */
    private String tombId;
    /**
     * 购买记录ID
     */
    private String buyRecordId;
    /**
     * 是否迁移
     */
    private byte isTransfer;
    /**
     * 购买记录
     */
    private BuyRecord buyRecord;
    /**
     * 墓穴信息
     */
    private Tomb tomb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public byte getDeadType() {
        return deadType;
    }

    public void setDeadType(byte deadType) {
        this.deadType = deadType;
    }

    public byte getSex() {
        return sex;
    }

    public void setSex(byte sex) {
        this.sex = sex;
    }

    public Date getBorthTime() {
        return borthTime;
    }

    public void setBorthTime(Date borthTime) {
        this.borthTime = borthTime;
    }

    public Date getDeadTime() {
        return deadTime;
    }

    public void setDeadTime(Date deadTime) {
        this.deadTime = deadTime;
    }

    public Date getLongmenTime() {
        return longmenTime;
    }

    public void setLongmenTime(Date longmenTime) {
        this.longmenTime = longmenTime;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getTombId() {
        return tombId;
    }

    public void setTombId(String tombId) {
        this.tombId = tombId;
    }

    public String getBuyRecordId() {
        return buyRecordId;
    }

    public void setBuyRecordId(String buyRecordId) {
        this.buyRecordId = buyRecordId;
    }

    public byte getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(byte isTransfer) {
        this.isTransfer = isTransfer;
    }

    public BuyRecord getBuyRecord() {
        return buyRecord;
    }

    public void setBuyRecord(BuyRecord buyRecord) {
        this.buyRecord = buyRecord;
    }

    public Tomb getTomb() {
        return tomb;
    }

    public void setTomb(Tomb tomb) {
        this.tomb = tomb;
    }

    /**
     * 获取性别名称
     *
     * @return
     */
    public String getSexName() {
        return Const.getSex(this.sex);
    }

    /**
     * 获取是否迁移名称
     *
     * @return
     */
    public String getIsTransferName() {
        return Const.getIs(this.isTransfer);
    }

    /**
     * 获取公墓死者死亡类型名称
     *
     * @return
     */
    public String getDeadTypeName() {
        return Const.getDeadType(this.deadType);
    }

}
