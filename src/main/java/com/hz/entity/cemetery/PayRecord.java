package com.hz.entity.cemetery;
/**
 * Describtion:普通收费记录类
 * <p>
 * Creatime:2019年6月28日
 * <p>
 * Author:wujiayang
 * <p>
 * Comment:
 **/

import java.sql.Date;
import java.sql.Timestamp;

public class PayRecord {
    //支付id
    private String id;
    //购买记录ID
    private String buyRecordId;
    //死者id
    private String userId;
    //费用名称
    private String name;
    //支付人姓名
    private String payerName;
    //费用类别 1:雕刻安放费; 2:墓穴费; 3:维护费; 4:迁移费; 5:雕刻费; 6:维修费;
    private byte type;
    //总金额
    private double total;
    //订单创建时间
    private Timestamp createTime;
    //支付时间
    private Date payTime;
    //联系电话
    private String phone;
    //发票号
    private String invoiceNumber;
    //费用名称
    private String costName;
    //操作人
    private String operator;
    //备注
    private String remark;
    private String graveInfo;

    
    public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyRecordId() {
        return buyRecordId;
    }

    public void setBuyRecordId(String buyRecordId) {
        this.buyRecordId = buyRecordId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	public String getCostName() {
		return costName;
	}

	public void setCostName(String costName) {
		this.costName = costName;
	}

	public String getGraveInfo() {
		return graveInfo;
	}

	public void setGraveInfo(String graveInfo) {
		this.graveInfo = graveInfo;
	}
}
