package com.hz.entity.cemetery;

import java.sql.Date;
import java.util.List;

/**
 * 墓穴持证人与使用者记录
 *
 * @ClassName BuyAndUserRecord
 * @Description TODO
 * @Author ZhixiangWang
 * @Date 2019/7/24 17:21
 * @Version 1.0
 **/
public class BuyAndUserRecord {
    /**
     * id
     */
    private String id;
    /**
     * 持证人姓名
     */
    private String cardHolder;
    /**
     * 墓穴证号
     */
    private String tombsCard;
    /**
     * 家庭地址
     */
    private String address;
    /**
     * 身份证号
     */
    private String idNumber;
    /**
     * 联系电话
     */
    private String contactNumber;
    /**
     * 手机号
     */
    private String phoneNumber;
    /**
     * 有效期至
     */
    private Date validityPeriod;
    /**
     * 墓穴名称
     */
    private String graveInfo;
    /**
     * 墓穴信息
     */
    private Tomb tomb;
    /**
     * 使用者记录实体类
     */
    private List<UserRecord> userRecord;
    /**
     * 维护费记录
     */
    private MaintainRecord maintainRecord;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getTombsCard() {
        return tombsCard;
    }

    public void setTombsCard(String tombsCard) {
        this.tombsCard = tombsCard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(Date validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public String getGraveInfo() {
        return graveInfo;
    }

    public void setGraveInfo(String graveInfo) {
        this.graveInfo = graveInfo;
    }

    public Tomb getTomb() {
        return tomb;
    }

    public void setTomb(Tomb tomb) {
        this.tomb = tomb;
    }

    public List<UserRecord> getUserRecord() {
        return userRecord;
    }

    public void setUserRecord(List<UserRecord> userRecord) {
        this.userRecord = userRecord;
    }

    public MaintainRecord getMaintainRecord() {
        return maintainRecord;
    }

    public void setMaintainRecord(MaintainRecord maintainRecord) {
        this.maintainRecord = maintainRecord;
    }
}
