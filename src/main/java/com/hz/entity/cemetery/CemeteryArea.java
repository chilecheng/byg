package com.hz.entity.cemetery;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 陵园区域信息表
 *
 * @author jgj
 */
public class CemeteryArea {
    //区域id
    private String id;
    //别名
    private String name;
    //陵园ID
    private String cemeteryId;
    //备注说明
    private String comment;
    //创建时间
    private Timestamp createTime;
    //创建人
    private String createPeople;
    //照片地址
    private String photo;
    //所在区域的墓穴  排序情况：1--从左到右   2--从右到左
    private byte sort;
    //顺序号
    private int index;
    //启用标志
    private byte isdel;
    //已售数量
    private int soldCount;
    //未售数量
    private int notSoldCount;
    //已预约数量
    private int bookedCount;

    public int getSoldCount() {
        return soldCount;
    }

    public void setSoldCount(int soldCount) {
        this.soldCount = soldCount;
    }

    public int getNotSoldCount() {
        return notSoldCount;
    }

    public void setNotSoldCount(int notSoldCount) {
        this.notSoldCount = notSoldCount;
    }

    public int getBookedCount() {
        return bookedCount;
    }

    public void setBookedCount(int bookedCount) {
        this.bookedCount = bookedCount;
    }

    public byte getSort() {
        return sort;
    }

    public void setSort(byte sort) {
        this.sort = sort;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public byte getIsdel() {
        return isdel;
    }

    public void setIsdel(byte isdel) {
        this.isdel = isdel;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(String createPeople) {
        this.createPeople = createPeople;
    }

    public String getCemeteryId() {
        return cemeteryId;
    }

    public void setCemeteryId(String cemeteryId) {
        this.cemeteryId = cemeteryId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    /**
     * 是否启用标志
     *
     * @return
     */
    public String getIsdelName() {
        return Const.getIsdel(this.isdel);

    }
}
