package com.hz.entity.cemetery;

import com.hz.util.Const;

import java.sql.Date;


/**
 * 墓穴维修记录
 *
 * @author jgj
 */
public class RepairRecord {
    //id
    private String id;
    //维修报备人
    private String name;
    //联系电话
    private String phoneNumber;
    //维修内容
    private String comment;
    //开始时间
    private Date beginTime;
    //预计结束时间
    private Date endTime;
    //维修完成时间
    private Date completeTime;
    //维修墓穴ID
    private String tombId;
    //维修状态
    private byte repairType;
    //维修墓穴信息
    private Tomb tomb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public String getTombId() {
        return tombId;
    }

    public void setTombId(String tombId) {
        this.tombId = tombId;
    }

    public byte getRepairType() {
        return repairType;
    }

    public void setRepairType(byte repairType) {
        this.repairType = repairType;
    }

    public Tomb getTomb() {
        return tomb;
    }

    public void setTomb(Tomb tomb) {
        this.tomb = tomb;
    }

    /**
     * 是否维修标志
     *
     * @return
     */
    public String getIsRepairName() {
        return Const.getIsRepair(this.repairType);
    }
}
