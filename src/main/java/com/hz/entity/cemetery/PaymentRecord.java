package com.hz.entity.cemetery;

import java.sql.Date;


/**
 * 墓穴支付记录表
 *
 * @author jgj
 */
public class PaymentRecord {
    //墓穴id
    private String id;
    //付款人姓名
    private String name;
    //购买记录ID
    private String recordId;
    //发票号
    private String invoiceNumber;
    //支付金额
    private double totalAmount;
    //支付时间
    private Date payTime;
    //修改人
    private String operator;
    //备注
    private String remark;
    //创建时间
    private Date creaTime;

    public Date getCreaTime() {
        return creaTime;
    }

    public void setCreaTime(Date creaTime) {
        this.creaTime = creaTime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }
}
