package com.hz.entity.cemetery;

import com.hz.util.Const;

import java.sql.Date;

/**
 * 墓穴信息表
 *
 * @author jgj
 */
public class Tomb {
    //墓穴id
    private String id;
    //别名
    private String name;
    //所属排ID
    private String rowId;
    //照片地址
    private String photo;
    //单价
    private double price;
    //状态
    private byte state;
    //顺序号
    private int index;
    //启用标志
    private byte isdel;
    //描述
    private String comment;
    //维护费
    private double maintenance;
    //排名称
    private String rowName;
    //操作人
    private String opertor;
    //墓区id
    private String areaId;
    //排序号
    private String number;
    //陵园id
    private String cId;
    //实际销售价格
    private float salePrice;
    //到期时间
    private Date expireDate;
    //创建时间
    private Date creatime;
    //排中排
    private int row2;

    public double getMaintenance() {
        return maintenance;
    }

    public void setMaintenance(double maintenance) {
        this.maintenance = maintenance;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public byte getIsdel() {
        return isdel;
    }

    public void setIsdel(byte isdel) {
        this.isdel = isdel;
    }

    public String getRowName() {
        return rowName;
    }

    public void setRowName(String rowName) {
        this.rowName = rowName;
    }

    public String getOpertor() {
        return opertor;
    }

    public void setOpertor(String opertor) {
        this.opertor = opertor;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(float salePrice) {
        this.salePrice = salePrice;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getCreatime() {
        return creatime;
    }

    public void setCreatime(Date creatime) {
        this.creatime = creatime;
    }

    public int getRow2() {
        return row2;
    }

    public void setRow2(int row2) {
        this.row2 = row2;
    }

    /**
     * 是否启用标志
     *
     * @return
     */
    public String getIsdelName() {
        return Const.getIsdel(this.isdel);
    }

    /**
     * 获取状态名称
     *
     * @return
     */
    public String getStateName() {
        return Const.getState(this.state);
    }
}
