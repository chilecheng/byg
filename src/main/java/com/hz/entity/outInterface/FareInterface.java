package com.hz.entity.outInterface;


import java.util.Date;

import com.hz.util.Const;

public class FareInterface {
	public String id;
	
	public String name;//死者姓名
	
	public String fareName;//告别厅号
	
	public Date farewellTime;//告别时间
	
	public byte sex;//性别
	
	public int age;//年龄
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFareName() {
		return fareName;
	}
	public void setFareName(String fareName) {
		this.fareName = fareName;
	}
	public Date getFarewellTime() {
		return farewellTime;
	}
	public void setFarewellTime(Date farewellTime) {
		this.farewellTime = farewellTime;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * 获取性别名称
	 * 
	 * @return
	 */
	public String getSexName() {
		return Const.getSex(this.sex);
	}
	
	
}
