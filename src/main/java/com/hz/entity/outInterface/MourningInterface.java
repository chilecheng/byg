package com.hz.entity.outInterface;


import java.util.Date;

import com.hz.util.Const;

public class MourningInterface {
	public String id;
	
	public String name;//死者姓名
	
	public String mourningName;//告别厅号
	
	public Date beginTime;//开始时间
	
	public Date endTime;//结束时间
	
	public byte sex;//性别
	
	public int age;//年龄
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getMourningName() {
		return mourningName;
	}
	public void setMourningName(String mourningName) {
		this.mourningName = mourningName;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	/**
	 * 获取性别名称
	 * 
	 * @return
	 */
	public String getSexName() {
		return Const.getSex(this.sex);
	}
	
}
