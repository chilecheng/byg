package com.hz.entity.log;

import java.sql.Timestamp;

import com.hz.entity.base.Furnace;
/**
 * 火化炉调度
 * @author tsp
 *
 */
public class FurnaceRecordLog {
	private String id;
	/**
	 * 火化炉ID
	 */
	private String furnaceId;
	/**
	 * 火化炉ID
	 */
	private Furnace furnace;
	/**
	 * 预约火化时间
	 */
	private Timestamp orderTime;
	/**
	 * 火化开始时间(维修开始时间)
	 */
	private Timestamp beginTime;
	/**
	 * 维修结束时间
	 */
	private Timestamp endTime;
	/**
	 * 火化工ID
	 */
	private String workerId;
	/**
	 * 添加时间
	 */
	private Timestamp createTime;
	/**
	 * 添加人ID
	 */
	private String createUserId;
	/**
	 * 业务单ID
	 */
	private String commissionOrderId;
	/**
	 * 火化委托单
	 */
	private CommissionOrderLog order;
	/**
	 * 状态
	 */
	private byte flag;
	/**
	 * 备注(维修原因)
	 */
	private String comment;
	/**
	 * 维修费用
	 */
	private double repairFee;
	
	
	public Furnace getFurnace() {
		return furnace;
	}
	public void setFurnace(Furnace furnace) {
		this.furnace = furnace;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public double getRepairFee() {
		return repairFee;
	}
	public void setRepairFee(double repairFee) {
		this.repairFee = repairFee;
	}

	public byte getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFurnaceId() {
		return furnaceId;
	}
	public void setFurnaceId(String furnaceId) {
		this.furnaceId = furnaceId;
	}
	public Timestamp getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}
	public String getWorkerId() {
		return workerId;
	}
	public void setWorkerId(String workerId) {
		this.workerId = workerId;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrderLog getOrder() {
		return order;
	}
	public void setOrder(CommissionOrderLog order) {
		this.order = order;
	}
	public Timestamp getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
}
