package com.hz.entity.log;

import java.sql.Date;
import java.sql.Timestamp;

import com.hz.entity.base.Freezer;

/**
 * 冷藏柜记录
 * @author rgy
 *
 */
public class FreezerRecordLog {
	//id
	private String id;
	//冷藏柜id
	private String freezerId;
	//冷藏柜
	private Freezer freezer;
	//开始时间
	private Timestamp beginDate;
	//结束时间
	private Timestamp endDate;
	//添加时间
	private Timestamp createTime;
	//创建人
	private String createUserId;
	//业务单id
	private String commissionOrderId;
	//标志
	private byte flag;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public FreezerRecordLog(){
		
	}

	

	public FreezerRecordLog(String id, String freezerId, Freezer freezer, Timestamp beginDate, Timestamp endDate, Timestamp createTime,
			String createUserId, String commissionOrderId, byte flag) {
		super();
		this.id = id;
		this.freezerId = freezerId;
		this.freezer = freezer;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.createTime = createTime;
		this.createUserId = createUserId;
		this.commissionOrderId = commissionOrderId;
		this.flag = flag;
	}



	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFreezerId() {
		return freezerId;
	}

	public void setFreezerId(String freezerId) {
		this.freezerId = freezerId;
	}

	public Freezer getFreezer() {
		return freezer;
	}

	public void setFreezer(Freezer freezer) {
		this.freezer = freezer;
	}


	public Timestamp getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Timestamp beginDate) {
		this.beginDate = beginDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}

	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}

	public byte getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}
	
}
