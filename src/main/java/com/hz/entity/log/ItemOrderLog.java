package com.hz.entity.log;

import java.sql.Date;

import com.hz.entity.base.CarType;
import com.hz.entity.base.Item;
import com.hz.entity.base.TransportType;
/**
 * 火化委托单收费项目
 * @author tsp
 *
 */
public class ItemOrderLog {
	/**
	 * id
	 */
	private String id;
	/**
	 * 服务项目
	 */
	private String itemId;
	/**
	 * 服务项目
	 */
	private Item item;
	/**
	 * 数量
	 */
	private int number;
	/**
	 * 挂账标志
	 */
	private byte tickFlag;
	/**
	 * 合计
	 */
	private double total;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 收款标志
	 */
	private byte payFlag;
	/**
	 * 收款时间
	 */
	private Date payTime;
	/**
	 * 业务单Id
	 */
	private String commissionOrderId;
	/**
	 * 业务单
	 */
	private CommissionOrderLog commissionOrder;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public byte getTickFlag() {
		return tickFlag;
	}
	public void setTickFlag(byte tickFlag) {
		this.tickFlag = tickFlag;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getPayTime() {
		return payTime;
	}
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrderLog getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrderLog commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public byte getPayFlag() {
		return payFlag;
	}
	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}
	
}
