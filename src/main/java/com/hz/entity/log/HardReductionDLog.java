package com.hz.entity.log;

/**
 * 困难减免项目
 * @author rgy
 *
 */
public class HardReductionDLog {
	//困难减免项目id
	private String id;
	//困难减免申请id
	private String hardReductionId;
	//困难减免申请
	private HardReductionLog hardReduction;
	//服务项目id
	private String itemId;
	//数量
	private int number;
	//合计
	private double total;
	//备注
	private String comment;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public HardReductionLog getHardReduction() {
		return hardReduction;
	}
	public void setHardReduction(HardReductionLog hardReduction) {
		this.hardReduction = hardReduction;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHardReductionId() {
		return hardReductionId;
	}
	public void setHardReductionId(String hardReductionId) {
		this.hardReductionId = hardReductionId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public HardReductionDLog(){
		
	}
	public HardReductionDLog(String id, String hardReductionId, String itemId, int number, double total) {
		super();
		this.id = id;
		this.hardReductionId = hardReductionId;
		this.itemId = itemId;
		this.number = number;
		this.total = total;
	}
	
	
}
