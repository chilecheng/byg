package com.hz.entity.log;

public class BaseReductionDLog {
	//基本减免申请项目id
	private String id;
	//基本减免申请id
	private String baseReductionId;
	//基本减免申请
	private BaseReductionLog baseReduction;
	//服务项目id
	private String itemId;
	//数量
	private int number;
	//合计
	private double total;
	//备注
	private String comment;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBaseReductionId() {
		return baseReductionId;
	}
	public void setBaseReductionId(String baseReductionId) {
		this.baseReductionId = baseReductionId;
	}
	public BaseReductionLog getBaseReduction() {
		return baseReduction;
	}
	public void setBaseReduction(BaseReductionLog baseReduction) {
		this.baseReduction = baseReduction;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	
}
