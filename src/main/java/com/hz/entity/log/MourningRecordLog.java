package com.hz.entity.log;

import java.sql.Date;
import java.sql.Timestamp;

import com.hz.entity.base.Mourning;

/**
 * 灵堂记录
 * @author rgy
 *
 */
public class MourningRecordLog {
	//id
	private String id;
	//守灵室id
	private String mourningId;
	//灵堂
	private Mourning mourning;
	//守灵开始时间
	private Timestamp beginTime;
	//守灵结束时间
	private Timestamp endTime;
	//添加时间
	private Timestamp createTime;
	//添加人
	private String createUserId;
	//添加业务单id
	private String commissionOrderId;
	//业务单
	private CommissionOrderLog order;
	//标志
	private byte flag;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public CommissionOrderLog getOrder() {
		return order;
	}
	public void setOrder(CommissionOrderLog order) {
		this.order = order;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMourningId() {
		return mourningId;
	}
	public void setMourningId(String mourningId) {
		this.mourningId = mourningId;
	}
	public Mourning getMourning() {
		return mourning;
	}
	public void setMourning(Mourning mourning) {
		this.mourning = mourning;
	}
	public Timestamp getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
}
