package com.hz.entity.log;

import java.sql.Timestamp;

import com.hz.entity.base.Farewell;
/**
 * 告别厅记录
 * @author rgy
 *
 */
public class FarewellRecordLog {
	//id
	private String id;
	//告别厅id
	private String farewellId;
	//告别厅
	private Farewell farewell;
	//告别时间
	private Timestamp beginDate;
	//添加时间
	private Timestamp createTime;
	//添加人
	private String createUserId;
	//业务单id
	private String commissionOrderId;
	//火化委托单
	private CommissionOrderLog order;
	//标志
	private byte flag;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public FarewellRecordLog(){
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFarewellId() {
		return farewellId;
	}

	public void setFarewellId(String farewellId) {
		this.farewellId = farewellId;
	}
	public CommissionOrderLog getOrder() {
		return order;
	}

	public void setOrder(CommissionOrderLog order) {
		this.order = order;
	}
	public Farewell getFarewell() {
		return farewell;
	}

	public void setFarewell(Farewell farewell) {
		this.farewell = farewell;
	}

	public Timestamp getBeginDate() {
		return beginDate;
		
	}

	public void setBeginDate(Timestamp date) {
		this.beginDate = date;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCommissionOrderId() {
		return commissionOrderId;
	}

	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}

	public byte getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}

	
}
