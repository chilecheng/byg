package com.hz.entity.log;

import java.sql.Date;

/**
 * 基本减免申请
 * @author rgy
 *
 */

public class BaseReductionLog{
//基本减免申请id
	private String id;
	//业务单id
	private String commissionOrderId;
	//火化委托单
	private CommissionOrderLog commissionOrder;
	//经办人身份证
	private String Fidcard;
	//免费对象
	private String freePersonIds;
	//重点救助对象
	private String hardPersonIds;
	//申请时间
	private Date createTime;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrderLog getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrderLog commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public String getFidcard() {
		return Fidcard;
	}
	public void setFidcard(String fidcard) {
		Fidcard = fidcard;
	}
	public String getFreePersonIds() {
		return freePersonIds;
	}
	public void setFreePersonIds(String freePersonIds) {
		this.freePersonIds = freePersonIds;
	}
	public String getHardPersonIds() {
		return hardPersonIds;
	}
	public void setHardPersonIds(String hardPersonIds) {
		this.hardPersonIds = hardPersonIds;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
