package com.hz.entity.log;

import java.sql.Date;
import java.sql.Timestamp;

import com.hz.entity.base.CarType;
import com.hz.entity.base.TransportType;
/**
 * 车辆调度记录
 * @author tsp
 *
 */
public class CarSchedulRecordLog {
	/**
	 * id
	 */
	private String id;
	/**
	 * 运输类型
	 */
	private String transportTypeId;
	/**
	 * 运输类型
	 */
	private TransportType transportType;
	/**
	 * 车辆类型
	 */
	private String carTypeId;
	/**
	 * 车辆类型
	 */
	private CarType carType;
	/**
	 * 接运时间
	 */
	private Timestamp pickTime;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 业务单Id
	 */
	private String commissionOrderId;
	/**
	 * 业务单
	 */
	private CommissionOrderLog commissionOrder;
	/**
	 * 原委托单Id
	 */
	private String cOId;
	
	public String getcOId() {
		return cOId;
	}
	public void setcOId(String cOId) {
		this.cOId = cOId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTransportTypeId() {
		return transportTypeId;
	}
	public void setTransportTypeId(String transportTypeId) {
		this.transportTypeId = transportTypeId;
	}
	public TransportType getTransportType() {
		return transportType;
	}
	public void setTransportType(TransportType transportType) {
		this.transportType = transportType;
	}
	public String getCarTypeId() {
		return carTypeId;
	}
	public void setCarTypeId(String carTypeId) {
		this.carTypeId = carTypeId;
	}
	public CarType getCarType() {
		return carType;
	}
	public void setCarType(CarType carType) {
		this.carType = carType;
	}
	public Timestamp getPickTime() {
		return pickTime;
	}
	public void setPickTime(Timestamp pickTime) {
		this.pickTime = pickTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrderLog getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrderLog commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	
	
}
