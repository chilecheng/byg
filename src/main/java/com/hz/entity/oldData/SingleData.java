package com.hz.entity.oldData;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 单机系统数据 实体类
 * @author jgj
 *
 */

public class SingleData{
	//业务单号
	private String deadID;
	//死者姓名
	private String name;
	//年龄
	private String age;
	//性别
	private String sex;
	//火化开始时间
	private Timestamp fireTime;
	//户籍地址
	private String registerAdd;
	//身份证地址 
	private String address;
	/**
	 * 以下信息具体信息显示时使用
	 * @return
	 */
	//死亡时间
	private Timestamp deathDate;
	//到馆时间
	private Timestamp arriveTime;
	//死亡原因
	private String death;
	//证明单位
	private String certifyingAuthority;
	//家属姓名
	private String fName;
	//家属电话
	private String fPhone;
	//家属地址
	private String fAddress;
	//家属关系
	private String relation;
	
	//接尸地址
	private String dieAddress;
	
	
	
	
	public String getRegisterAdd() {
		return registerAdd;
	}
	public void setRegisterAdd(String registerAdd) {
		this.registerAdd = registerAdd;
	}
	public Timestamp getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(Timestamp deathDate) {
		this.deathDate = deathDate;
	}
	public Timestamp getArriveTime() {
		return arriveTime;
	}
	public void setArriveTime(Timestamp arriveTime) {
		this.arriveTime = arriveTime;
	}
	public String getDeadID() {
		return deadID;
	}
	public void setDeadID(String deadID) {
		this.deadID = deadID;
	}
	public Timestamp getFireTime() {
		return fireTime;
	}
	public void setFireTime(Timestamp fireTime) {
		this.fireTime = fireTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDieAddress() {
		return dieAddress;
	}
	public void setDieAddress(String dieAddress) {
		this.dieAddress = dieAddress;
	}
	public String getDeath() {
		return death;
	}
	public void setDeath(String death) {
		this.death = death;
	}
	public String getCertifyingAuthority() {
		return certifyingAuthority;
	}
	public void setCertifyingAuthority(String certifyingAuthority) {
		this.certifyingAuthority = certifyingAuthority;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getfPhone() {
		return fPhone;
	}
	public void setfPhone(String fPhone) {
		this.fPhone = fPhone;
	}
	public String getfAddress() {
		return fAddress;
	}
	public void setfAddress(String fAddress) {
		this.fAddress = fAddress;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getSexName(){
		return Const.getSingleSex(this.sex);
	}
}
