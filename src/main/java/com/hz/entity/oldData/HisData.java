package com.hz.entity.oldData;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 6.5系统历史数据 实体类
 * @author jgj
 *
 */

public class HisData{
	//业务单号
	private String id;
	//死者编号
	private String code;
	//预约火化时间
	private Timestamp cremationTime;
	//火化开始时间
	private Timestamp fireTime;
	//死者姓名
	private String name;
	//年龄
	private String age;
	//性别
	private byte sex;
	//火化炉号
	private String crematorID;
	//业务状态
	private String status;
	//身份证号
	private String idCard;
	/**
	 * 以下信息具体信息显示时使用
	 * @return
	 */
	//死亡时间
	private Timestamp deathDate;
	//到馆时间
	private Timestamp arriveTime;
	//身份证地址 
	private String address;
	//接尸地址
	private String pickAddress;
	//死亡类型
	private String deadType;
	//死亡原因
	private String death;
	//证明单位
	private String certifyingAuthority;
	//家属姓名
	private String fName;
	//家属电话
	private String fPhone;
	//家属地址
	private String fAddress;
	//家属关系
	private String relation;
	/**
	 * 省、市、区
	 * @return
	 */
	private String province;
	private String city;
	private String area;

	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public Timestamp getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(Timestamp deathDate) {
		this.deathDate = deathDate;
	}
	public Timestamp getArriveTime() {
		return arriveTime;
	}
	public void setArriveTime(Timestamp arriveTime) {
		this.arriveTime = arriveTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Timestamp getCremationTime() {
		return cremationTime;
	}
	public void setCremationTime(Timestamp cremationTime) {
		this.cremationTime = cremationTime;
	}
	public Timestamp getFireTime() {
		return fireTime;
	}
	public void setFireTime(Timestamp fireTime) {
		this.fireTime = fireTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public String getCrematorID() {
		return crematorID;
	}
	public void setCrematorID(String crematorID) {
		this.crematorID = crematorID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPickAddress() {
		return pickAddress;
	}
	public void setPickAddress(String pickAddress) {
		this.pickAddress = pickAddress;
	}
	public String getDeadType() {
		return deadType;
	}
	public void setDeadType(String deadType) {
		this.deadType = deadType;
	}
	public String getDeath() {
		return death;
	}
	public void setDeath(String death) {
		this.death = death;
	}
	public String getCertifyingAuthority() {
		return certifyingAuthority;
	}
	public void setCertifyingAuthority(String certifyingAuthority) {
		this.certifyingAuthority = certifyingAuthority;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getfPhone() {
		return fPhone;
	}
	public void setfPhone(String fPhone) {
		this.fPhone = fPhone;
	}
	public String getfAddress() {
		return fAddress;
	}
	public void setfAddress(String fAddress) {
		this.fAddress = fAddress;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	public String getSexName(){
		return Const.getSex(this.sex);
	}
	
	
}
