package com.hz.entity.ye;

import java.util.Date;

//import java.sql.Date;

import com.hz.entity.system.User;

/**
 * 补卡
 * @author lipengpeng
 *
 */
public class FillCardRecord {
	/**
	 * id
	 */
	private String id;
	/**
	 * 旧卡号
	 */
	private String oldcard;
	/**
	 * 新卡号
	 */
	private String newcard;
	/**
	 * 办理人姓名
	 */
	private String contactname;
	/**
	 * 办理人联系方式
	 */
	private String contact;
	/**
	 * 添加时间
	 */
	private Date createTime;
	/**
	 * 添加人
	 */
	private User user;
	/**
	 * 业务单
	 */
	//private CommissionOrder commissionorder;
	
	private String userid;
	
	private String commissionorderid;

	public String getCommissionorderid() {
		return commissionorderid;
	}

	public void setCommissionorderid(String commissionorderid) {
		this.commissionorderid = commissionorderid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOldcard() {
		return oldcard;
	}

	public void setOldcard(String oldcard) {
		this.oldcard = oldcard;
	}

	public String getNewcard() {
		return newcard;
	}

	public void setNewcard(String newcard) {
		this.newcard = newcard;
	}

	public String getContactname() {
		return contactname;
	}

	public void setContactname(String contactname) {
		this.contactname = contactname;
	}
	
	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

//	public CommissionOrder getCommissionorder() {
//		return commissionorder;
//	}
//
//	public void setCommissionorder(CommissionOrder commissionorder) {
//		this.commissionorder = commissionorder;
//	}
	
	
	

}
