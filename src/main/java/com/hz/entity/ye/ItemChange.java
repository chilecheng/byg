package com.hz.entity.ye;

public class ItemChange {
	
	private String id;
	/**
	 * 丧葬用品小类
	 */
	private String sort;
	/**
	 * 火化委托单ID
	 */
	private String commissionOrderId;
	/**
	 * 丧葬用品购买单号
	 */
	private String buyOrderId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public String getBuyOrderId() {
		return buyOrderId;
	}
	public void setBuyOrderId(String buyOrderId) {
		this.buyOrderId = buyOrderId;
	}
	
}
