package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.base.DeadReason;
import com.hz.entity.base.DeadType;
import com.hz.entity.base.Farewell;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.ProveUnit;
import com.hz.entity.base.Mourning;
import com.hz.entity.system.User;

/**
 * 冷藏柜记录
 * 
 * @author rgy
 * 
 */
public class FreezerRecord {
	// id
	private String id;
	// 冷藏柜id
	private String freezerId;
	// 冷藏柜
	private Freezer freezer;
	// 开始时间
	private Timestamp beginDate;
	// 结束时间
	private Timestamp endDate;
	// 添加时间
	private Timestamp creatTime;
	// 创建人
	private String createUserId;
	// 业务单id
	private String commissionOrderId;
	// 冰柜 名
	private String freezerName;
	//查看次数   冰柜调度 首页
	private byte viewsDispatch;
	//查看次数  出柜	首页
	private byte viewsOut;
	// 火化委托单
	private CommissionOrder order;
	// 死亡类型
	private DeadType deadType;
	// 死亡原因
	private DeadReason deadReason;
	//
	private ProveUnit proveUnit;
	// 标志
	private byte flag;
	// 橱柜人I
	private String outStaff;
	// 处理人员
	private String dillStaff;
	// 出柜时间
	private Timestamp outTime;
	// 出柜备注
	private String outRemark;
	// 入柜 备注
	private String intoRemark;
	// 转柜 备注
	private String transferRemark;
	// 入柜人ID
	private String intoStaff;
	// 入柜录入员
	private String intoOperator;
	//转柜	录入员
	private  String transferOperator;
	//c出柜录入员
	private String  outOperator;
	//遗体解冻时间
	private  Timestamp autoTime;
	//验尸申请单
	private AutopsyRecord autopsyRecord;
	private String name ;
	private String sex ;
	private int age  ;
	private String fName ;
	private String  fPhone;
	private String code;
	private Timestamp arriveTime ;
	private String farewellName  ;
	private Timestamp farewellRecordBD;
	private String mourningName;
	private Timestamp mourningRecordBD;
	private  double repairFee;
	private Timestamp cremationTime;
	private Farewell farewell;
	// 告别厅使用信息
	private FarewellRecord farewellRecord;
	// l灵堂信息
	private Mourning mourning;
	// l灵堂使用信息
	private MourningRecord mourningRecord;
	// 人员 option
	private User staffOption;
	private String freezerOption;
	private String userOptionId;
	
	
	public CommissionOrder getOrder() {
		return order;
	}
	
	public void setOrder(CommissionOrder order) {
		this.order = order;
	}
	
	public AutopsyRecord getAutopsyRecord() {
		return autopsyRecord;
	}
	
	public void setAutopsyRecord(AutopsyRecord autopsyRecord) {
		this.autopsyRecord = autopsyRecord;
	}
	
	public Timestamp getAutoTime() {
		return autoTime;
	}
	
	public void setAutoTime(Timestamp autoTime) {
		this.autoTime = autoTime;
	}
	
	public String getSex() {
		return sex;
	}
	
	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getfPhone() {
		return fPhone;
	}

	public void setfPhone(String fPhone) {
		this.fPhone = fPhone;
	}

	public Timestamp getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(Timestamp arriveTime) {
		this.arriveTime = arriveTime;
	}

	public String getFarewellName() {
		return farewellName;
	}

	public void setFarewellName(String farewellName) {
		this.farewellName = farewellName;
	}

	public Timestamp getFarewellRecordBD() {
		return farewellRecordBD;
	}

	public void setFarewellRecordBD(Timestamp farewellRecordBD) {
		this.farewellRecordBD = farewellRecordBD;
	}

	public String getMourningName() {
		return mourningName;
	}

	public void setMourningName(String mourningName) {
		this.mourningName = mourningName;
	}

	public Timestamp getMourningRecordBD() {
		return mourningRecordBD;
	}

	public void setMourningRecordBD(Timestamp mourningRecordBD) {
		this.mourningRecordBD = mourningRecordBD;
	}

	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getCremationTime() {
		return cremationTime;
	}

	public void setCremationTime(Timestamp cremationTime) {
		this.cremationTime = cremationTime;
	}

	public double getRepairFee() {
		return repairFee;
	}

	public void setRepairFee(double repairFee) {
		this.repairFee = repairFee;
	}
	
	public byte getViewsDispatch() {
		return viewsDispatch;
	}

	public void setViewsDispatch(byte viewsDispatch) {
		this.viewsDispatch = viewsDispatch;
	}

	public byte getViewsOut() {
		return viewsOut;
	}

	public void setViewsOut(byte viewsOut) {
		this.viewsOut = viewsOut;
	}

	public Farewell getFarewell() {
		return farewell;
	}

	public void setFarewell(Farewell farewell) {
		this.farewell = farewell;
	}

	public String getFreezerOption() {
		return freezerOption;
	}

	public void setFreezerOption(String freezerOption) {
		this.freezerOption = freezerOption;
	}

	public String getIntoOperator() {
		return intoOperator;
	}

	public void setIntoOperator(String intoOperator) {
		this.intoOperator = intoOperator;
	}

	public String getTransferOperator() {
		return transferOperator;
	}

	public void setTransferOperator(String transferOperator) {
		this.transferOperator = transferOperator;
	}

	public String getOutOperator() {
		return outOperator;
	}

	public void setOutOperator(String outOperator) {
		this.outOperator = outOperator;
	}

	public String getOutStaff() {
		return outStaff;
	}

	public void setOutStaff(String outStaff) {
		this.outStaff = outStaff;
	}

	public String getDillStaff() {
		return dillStaff;
	}

	public void setDillStaff(String dillStaff) {
		this.dillStaff = dillStaff;
	}

	public Timestamp getOutTime() {
		return outTime;
	}

	public void setOutTime(Timestamp outTime) {
		this.outTime = outTime;
	}

	public String getOutRemark() {
		return outRemark;
	}

	public void setOutRemark(String outRemark) {
		this.outRemark = outRemark;
	}

	public String getIntoRemark() {
		return intoRemark;
	}

	public void setIntoRemark(String intoRemark) {
		this.intoRemark = intoRemark;
	}

	public String getTransferRemark() {
		return transferRemark;
	}

	public void setTransferRemark(String transferRemark) {
		this.transferRemark = transferRemark;
	}

	public String getIntoStaff() {
		return intoStaff;
	}

	public void setIntoStaff(String intoStaff) {
		this.intoStaff = intoStaff;
	}

	public String getUserOptionId() {
		return userOptionId;
	}

	public void setUserOptionId(String userOptionId) {
		this.userOptionId = userOptionId;
	}

	public User getStaffOption() {
		return staffOption;
	}

	public void setStaffOption(User staffOption) {
		this.staffOption = staffOption;
	}

	public String getFreezerName() {
		return freezerName;
	}

	public FarewellRecord getFarewellRecord() {
		return farewellRecord;
	}

	public void setFarewellRecord(FarewellRecord farewellRecord) {
		this.farewellRecord = farewellRecord;
	}

	public Mourning getMourning() {
		return mourning;
	}

	public void setMourning(Mourning mourning) {
		this.mourning = mourning;
	}

	public MourningRecord getMourningRecord() {
		return mourningRecord;
	}

	public void setMourningRecord(MourningRecord mourningRecord) {
		this.mourningRecord = mourningRecord;
	}

	public void setFreezerName(String freezerName) {
		this.freezerName = freezerName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFreezerId() {
		return freezerId;
	}

	public void setFreezerId(String freezerId) {
		this.freezerId = freezerId;
	}

	public Freezer getFreezer() {
		return freezer;
	}

	public void setFreezer(Freezer freezer) {
		this.freezer = freezer;
	}

	public Timestamp getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Timestamp beginDate) {
		this.beginDate = beginDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Timestamp getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}

	public String getCommissionOrderId() {
		return commissionOrderId;
	}

	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}

	public byte getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}

	public DeadType getDeadType() {
		return deadType;
	}

	public void setDeadType(DeadType deadType) {
		this.deadType = deadType;
	}

	public DeadReason getDeadReason() {
		return deadReason;
	}

	public void setDeadReason(DeadReason deadReason) {
		this.deadReason = deadReason;
	}

	public ProveUnit getProveUnit() {
		return proveUnit;
	}

	public void setProveUnit(ProveUnit proveUnit) {
		this.proveUnit = proveUnit;
	}

}
