package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.base.Item;

public class BaseReductionD {
	//基本减免申请项目id
	private String id;
	//基本减免申请id
	private String baseReductionId;
	//基本减免申请
	private BaseReduction baseReduction;
	//服务项目id
	private String itemId;
	//数量
	private int number;
	//合计
	private double total;
	//备注
	private String comment;
	//item项
	private Item item;
	/**
	 * 基本减免扣除时间/计入时间
	 * 收费系统更改之后新加
	 * 将基本减免扣除时间与收费记录时间相关联，避免多次计算
	 * 
	 */
	private Timestamp payTime;
	//订单流水号
	private String orderNumber;
	/**
	 * 收费科 需求项
	 * f分别为  项目的名称
	 * 单价
	 * 申请创建时间
	 */
	private String  name;
	private String  helpCode;
	private byte  sort;
	private double pice;
	private Timestamp creatTime;
	
	
	
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public byte getSort() {
		return sort;
	}
	public void setSort(byte sort) {
		this.sort = sort;
	}
	public String getHelpCode() {
		return helpCode;
	}
	public void setHelpCode(String helpCode) {
		this.helpCode = helpCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPice() {
		return pice;
	}
	public void setPice(double pice) {
		this.pice = pice;
	}
	public Timestamp getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBaseReductionId() {
		return baseReductionId;
	}
	public void setBaseReductionId(String baseReductionId) {
		this.baseReductionId = baseReductionId;
	}
	public BaseReduction getBaseReduction() {
		return baseReduction;
	}
	public void setBaseReduction(BaseReduction baseReduction) {
		this.baseReduction = baseReduction;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
