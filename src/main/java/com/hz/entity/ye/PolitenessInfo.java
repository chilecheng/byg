package com.hz.entity.ye;

import java.util.List;

public class PolitenessInfo {
	
	private String cid;
	
	private String cardCode;
	
	private List<Politenessdetail> politenesslist;

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public List<Politenessdetail> getPolitenesslist() {
		return politenesslist;
	}

	public void setPolitenesslist(List<Politenessdetail> politenesslist) {
		this.politenesslist = politenesslist;
	}
	
	

}
