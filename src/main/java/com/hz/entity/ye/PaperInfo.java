package com.hz.entity.ye;

import java.util.List;

public class PaperInfo {
	
	private String cid;
	
	private String cardCode;
	
	private List<Paperdetail> paperlist;

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public List<Paperdetail> getPaperlist() {
		return paperlist;
	}

	public void setPaperlist(List<Paperdetail> paperlist) {
		this.paperlist = paperlist;
	}
	
	
	
	

}
