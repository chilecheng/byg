package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.util.Const;

public class AutoFreezerDetail {
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getFreezerName() {
		return FreezerName;
	}
	public void setFreezerName(String freezerName) {
		FreezerName = freezerName;
	}
	public String getProveName() {
		return proveName;
	}
	public void setProveName(String proveName) {
		this.proveName = proveName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Timestamp getyTime() {
		return yTime;
	}
	public void setyTime(Timestamp yTime) {
		this.yTime = yTime;
	}
	public Timestamp getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Timestamp registerTime) {
		this.registerTime = registerTime;
	}
	private String id;
	private String Name;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	private byte  sex;
	private int age;
	private String code;
	private String FreezerName;
	private String proveName;
	private String comment;
	private Timestamp yTime;
	private Timestamp registerTime;
	private String registerUserId;
	private  String proveUnitId;
	private String serialNumber;
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getProveUnitId() {
		return proveUnitId;
	}
	public void setProveUnitId(String proveUnitId) {
		this.proveUnitId = proveUnitId;
	}
	public String getRegisterUserId() {
		return registerUserId;
	}
	public void setRegisterUserId(String registerUserId) {
		this.registerUserId = registerUserId;
	}
	/**
	 * 获取性别名称
	 * 
	 * @return
	 */
	public String getSexName() {
		return Const.getSex(this.sex);
	}
	

}
