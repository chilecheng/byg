package com.hz.entity.ye;

/**
 * 丧葬用品单个用品
 * @author lipengpeng
 *
 */

public class ListFuneralOrderRecordDetail {
	
	private String id;
	
	private String goodsRecordId;
	
	private String itemId;
	
	private String num;
	
	private String total;
	
	private String remarkText;
	
	private String isCredit;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGoodsRecordId() {
		return goodsRecordId;
	}

	public void setGoodsRecordId(String goodsRecordId) {
		this.goodsRecordId = goodsRecordId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public String getIsCredit() {
		return isCredit;
	}

	public void setIsCredit(String isCredit) {
		this.isCredit = isCredit;
	}
	
	

}
