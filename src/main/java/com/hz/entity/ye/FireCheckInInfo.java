package com.hz.entity.ye;
import java.sql.Timestamp;

import com.hz.util.Const;

public class FireCheckInInfo {
	/**
	 * 委托单id
	 */
	private String cid;
	/**
	 * 死者姓名
	 */
	private String dname;
	
	/**
	 * 卡号
	 */
	private String cardCode;
	
	/**
	 * 死者编号
	 */
	private String code;
	
	/**
	 * 死者年龄
	 */
	private Integer age; 
	/**
	 * 死者性别
	 */
	private byte sex;
	/**
	 * 死者住址
	 */
	private String address;
	/**
	 * 家属姓名
	 */
	private String fname;
	/**
	 * 家属电话
	 */
	private String fphone;
	/**
	 * 告别厅
	 */
	private String fareWellName;
	/**
	 * 付费状态(1:已付费 2未付费)
	 */
	private byte payFlag;
	/**
	 * 火化预约开始时间
	 */
	private Timestamp cremationTime;
	
	private byte cremationFlag;
	
	private String btName;
	/**
	 * 火化炉类型
	 */
	private byte furnaceFlag;
	/**
	 * 火化炉名称
	 */
	private String furnaceName;
	/**
	 * 销卡字段
	 */
	private byte pincardFlag; 
	/**
	 * 审核字段
	 */
	private byte checkFlag;
	/**
	 * 火化收费
	 */
	private byte furPayFlag;
	
	//身份证号
	private String certificateCode;
	//接运地址
	private String pickAddr;
	//死亡类型
	private String deadTypeName;
	//死亡原因
	private String deadReasonName;
	//证明单位
	private String proveUnitName;
	//死亡时间
	private Timestamp dTime;
	//到馆时间
	private Timestamp aTime;
	//火化时间
	private Timestamp fTime;
	//亲属关系
	private String fAppellation;
	//亲属住址
	private String fAddr;
	
	public String getFurnaceName() {
		return furnaceName;
	}

	public void setFurnaceName(String furnaceName) {
		this.furnaceName = furnaceName;
	}

	public byte getFurPayFlag() {
		return furPayFlag;
	}

	public void setFurPayFlag(byte furPayFlag) {
		this.furPayFlag = furPayFlag;
	}

	public byte getFurnaceFlag() {
		return furnaceFlag;
	}

	public void setFurnaceFlag(byte furnaceFlag) {
		this.furnaceFlag = furnaceFlag;
	}

	public byte getPayFlag() {
		return payFlag;
	}

	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}

	public byte getCheckFlag() {
		return checkFlag;
	}

	public void setCheckFlag(byte checkFlag) {
		this.checkFlag = checkFlag;
	}

	public byte getPincardFlag() {
		return pincardFlag;
	}

	public void setPincardFlag(byte pincardFlag) {
		this.pincardFlag = pincardFlag;
	}

	public String getBtName() {
		return btName;
	}

	public void setBtName(String btName) {
		this.btName = btName;
	}

	public String getCremationOrNot()
	{
		if (this.cremationFlag==Const.Is_Yes) {
			return "已火化";
		}else if(this.cremationFlag==Const.Is_No){
			return "未火化";
		}else{
			return "错误";
		}
	}
	
	
	public byte getCremationFlag() {
		return cremationFlag;
	}
	public void setCremationFlag(byte cremationFlag) {
		this.cremationFlag = cremationFlag;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getCardCode() {
		return cardCode;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getFphone() {
		return fphone;
	}
	public void setFphone(String fphone) {
		this.fphone = fphone;
	}
	public String getFareWellName() {
		return fareWellName;
	}
	public void setFareWellName(String fareWellName) {
		this.fareWellName = fareWellName;
	}
	public Timestamp getCremationTime() {
		return cremationTime;
	}
	public void setCremationTime(Timestamp cremationTime) {
		this.cremationTime = cremationTime;
	}

	public String getSexName() {
		return Const.getSex(this.sex);
	}
	public String getPayFlagName() {
		return Const.getPayFlagName(this.payFlag);
	}

	public String getCertificateCode() {
		return certificateCode;
	}

	public void setCertificateCode(String certificateCode) {
		this.certificateCode = certificateCode;
	}

	public String getDeadTypeName() {
		return deadTypeName;
	}

	public void setDeadTypeName(String deadTypeName) {
		this.deadTypeName = deadTypeName;
	}

	public String getDeadReasonName() {
		return deadReasonName;
	}

	public void setDeadReasonName(String deadReasonName) {
		this.deadReasonName = deadReasonName;
	}

	public Timestamp getdTime() {
		return dTime;
	}

	public void setdTime(Timestamp dTime) {
		this.dTime = dTime;
	}

	public Timestamp getaTime() {
		return aTime;
	}

	public void setaTime(Timestamp aTime) {
		this.aTime = aTime;
	}

	public String getProveUnitName() {
		return proveUnitName;
	}

	public void setProveUnitName(String proveUnitName) {
		this.proveUnitName = proveUnitName;
	}

	public Timestamp getfTime() {
		return fTime;
	}

	public void setfTime(Timestamp fTime) {
		this.fTime = fTime;
	}

	public String getPickAddr() {
		return pickAddr;
	}

	public void setPickAddr(String pickAddr) {
		this.pickAddr = pickAddr;
	}

	public String getfAppellation() {
		return fAppellation;
	}

	public void setfAppellation(String fAppellation) {
		this.fAppellation = fAppellation;
	}

	public String getfAddr() {
		return fAddr;
	}

	public void setfAddr(String fAddr) {
		this.fAddr = fAddr;
	}

	
}
