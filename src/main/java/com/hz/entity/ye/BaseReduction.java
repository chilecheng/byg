package com.hz.entity.ye;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * 基本减免申请
 * @author rgy
 *
 */

public class BaseReduction{
//基本减免申请id
	private String id;
	//业务单id
	private String commissionOrderId;
	//火化委托单
	private CommissionOrder commissionOrder;
	//经办人身份证
	private String Fidcard;
	//免费对象
	private String freePersonIds;
	//重点救助对象
	private String hardPersonIds;
	//申请时间
	private Date createTime;
	//申请时间
	private String comment;
	//收费时间，y_order_pay_record表中
	private Timestamp payTime;
	
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public String getFidcard() {
		return Fidcard;
	}
	public void setFidcard(String fidcard) {
		Fidcard = fidcard;
	}
	public String getFreePersonIds() {
		return freePersonIds;
	}
	public void setFreePersonIds(String freePersonIds) {
		this.freePersonIds = freePersonIds;
	}
	public String getHardPersonIds() {
		return hardPersonIds;
	}
	public void setHardPersonIds(String hardPersonIds) {
		this.hardPersonIds = hardPersonIds;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
