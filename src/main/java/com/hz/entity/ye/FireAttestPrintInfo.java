package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 火化证明打印基本信息
 * @author lipengpeng
 *
 */
public class FireAttestPrintInfo {
	
	private String cid;
	
	private String dname;
	
	private byte sex;
	
	private Integer age; 
	
	private String cardCode;
	
	/*private String beginTime;*/
	
	private byte cremationFlag;
	
	private String certificateCode;
	//火化打印状态
	private byte scheduleFlag;
	//领取人
	private String takeName;
	//领取人证件号
	private String takeCode;
	//打印时间
	private Timestamp printTime;
	//火化时间
	private Timestamp beginTime;
	//火化证明打印记录ID
	private String printId;

	public String getPrintId() {
		return printId;
	}

	public void setPrintId(String printId) {
		this.printId = printId;
	}

	public Timestamp getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}

	public String getTakeName() {
		return takeName;
	}

	public void setTakeName(String takeName) {
		this.takeName = takeName;
	}

	public String getTakeCode() {
		return takeCode;
	}

	public void setTakeCode(String takeCode) {
		this.takeCode = takeCode;
	}

	public Timestamp getPrintTime() {
		return printTime;
	}

	public void setPrintTime(Timestamp printTime) {
		this.printTime = printTime;
	}

	public byte getScheduleFlag() {
		return scheduleFlag;
	}

	public void setScheduleFlag(byte scheduleFlag) {
		this.scheduleFlag = scheduleFlag;
	}
	/**
	 * 销卡字段
	 */
	private byte pincardFlag; 

	/**
	 * 审核字段
	 */
	private byte checkFlag;
	
	public byte getCheckFlag() {
		return checkFlag;
	}

	public void setCheckFlag(byte checkFlag) {
		this.checkFlag = checkFlag;
	}
	
	public byte getPincardFlag() {
		return pincardFlag;
	}

	public void setPincardFlag(byte pincardFlag) {
		this.pincardFlag = pincardFlag;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

/*	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}*/

	public byte getCremationFlag() {
		return cremationFlag;
	}

	public void setCremationFlag(byte cremationFlag) {
		this.cremationFlag = cremationFlag;
	}

	public String getCertificateCode() {
		return certificateCode;
	}

	public void setCertificateCode(String certificateCode) {
		this.certificateCode = certificateCode;
	}
	
	public String getSexName() {
		return Const.getSex(this.sex);
	}
	
	public String getBusinessState()
	{
		if(cremationFlag==Const.Is_Yes)
			return "火化完成";
		else if(cremationFlag==Const.Is_No)
			return "火化未完成";
		else
			return "错误";
	}
	

}
