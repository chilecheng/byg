package com.hz.entity.ye;

import java.sql.Timestamp;
import java.util.List;

import com.hz.entity.base.Farewell;
import com.hz.util.Const;

/**
 * 委托业务缴费记录
 * 
 * @author gpf
 */
public class DelegateRecord {

	private String name;
	private String code;
	private int age;
	private byte sex;
	private byte payflag;
	/**
	 * s死亡证明是否已收
	 */
	private byte dflag;
	/**
	 * y_order_pay_record 的id
	 */
	private String id;
	/**
	 * 支付时间
	 */

	private Timestamp payTime;
	/**
	 * 总计价格
	 */
	private double stotal;

	/**
	 * 收款方式
	 */
	private byte payType;

	public byte getPayType() {
		return payType;
	}

	public void setPayType(byte payType) {
		this.payType = payType;
	}

	/**
	 * 付款人姓名
	 */
	private String payName;
	/**
	 * 实收金额
	 */
	private double payAmount;
	/**
	 * 找零
	 */
	private double odd;
	/**
	 * 收款人
	 */
	private String payeeId;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 火化委托单id
	 */
	private String orderId;
	/**
	 * 基本项目申 免 费用
	 */
	private double ybrTotal;
	/**
	 * 困难减免项 费用
	 */
	private double hardTotal;
	/**
	 * 业务单号
	 */
	private String serviceCode;
	/**
	 * 服务项目 收费
	 */
	private double srTotal;
	/**
	 * 丧葬用品收费
	 */
	private double goodTotal;

	/**
	 * 前期已付
	 * 
	 * @return
	 */
	private double frontPay;

	public double getFrontPay() {
		return frontPay;
	}

	public void setFrontPay(double frontPay) {
		this.frontPay = frontPay;
	}

	public String getPayTypeName() {
		return Const.getPayType(payType);
	}

	public Timestamp getCremationTime() {
		return cremationTime;
	}

	public void setCremationTime(Timestamp cremationTime) {
		this.cremationTime = cremationTime;
	}

	private Timestamp cremationTime;

	public Timestamp getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Timestamp checkTime) {
		this.checkTime = checkTime;
	}

	private Timestamp checkTime;

	public List<ItemOrder> getListItemOrder() {
		return listItemOrder;
	}

	public void setListItemOrder(List<ItemOrder> listItemOrder) {
		this.listItemOrder = listItemOrder;
	}

	private List<ItemOrder> listItemOrder;

	// 火化委托单丧葬用品统计状态
	private byte dealIsdel;

	public byte getDealIsdel() {
		return dealIsdel;
	}

	public void setDealIsdel(byte dealIsdel) {
		this.dealIsdel = dealIsdel;
	}

	/**
	 * 告别厅
	 * 
	 * @return
	 */
	private Farewell farewell;

	public Farewell getFarewell() {
		return farewell;
	}

	/**
	 * 获取性别名称
	 * 
	 * @return
	 */
	public String getSexName() {
		return Const.getSex(this.sex);
	}

	public void setFarewell(Farewell farewell) {
		this.farewell = farewell;
	}

	public Timestamp getFarewellTime() {
		return farewellTime;
	}

	public void setFarewellTime(Timestamp farewellTime) {
		this.farewellTime = farewellTime;
	}

	public String getFarewellId() {
		return farewellId;
	}

	public void setFarewellId(String farewellId) {
		this.farewellId = farewellId;
	}

	/**
	 * 告别 时间
	 * 
	 * @return
	 */
	private Timestamp farewellTime;
	private String farewellId;

	public double getSrTotal() {
		return srTotal;
	}

	public void setSrTotal(double srTotal) {
		this.srTotal = srTotal;
	}

	public double getGoodTotal() {
		return goodTotal;
	}

	public void setGoodTotal(double goodTotal) {
		this.goodTotal = goodTotal;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public double getHardTotal() {
		return hardTotal;
	}

	public void setHardTotal(double hardTotal) {
		this.hardTotal = hardTotal;
	}

	public double getYbrTotal() {
		return ybrTotal;
	}

	public void setYbrTotal(double ybrTotal) {
		this.ybrTotal = ybrTotal;
	}

	public double getStotal() {
		return stotal;
	}

	public void setStotal(double stotal) {
		this.stotal = stotal;
	}

	public byte getDflag() {
		return dflag;
	}

	public void setDflag(byte dflag) {
		this.dflag = dflag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public byte getPayflag() {
		return payflag;
	}

	public void setPayflag(byte payflag) {
		this.payflag = payflag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getPayTime() {
		return payTime;
	}

	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}

	public double getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(double payAmount) {
		this.payAmount = payAmount;
	}

	public double getOdd() {
		return odd;
	}

	public void setOdd(double odd) {
		this.odd = odd;
	}

	public String getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
