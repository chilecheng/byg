package com.hz.entity.ye;

import java.sql.Date;

import com.hz.entity.system.User;

/**
 * 销卡记录
 * @author hw
 *
 */
public class PincardRecord {
	//id
	private String id;
	//火化委托单id
	private String commissionOrderId;
	//旧卡号
	private String oldCard;
	//销卡时间
	private Date creatTime;
	//销卡用户
	private String creatUserId;
	//火化委托单
	private CommissionOrder commissionOrder;
	//用户
	private User creatUser;
	public String getId() {
		return id;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public User getCreatUser() {
		return creatUser;
	}
	public void setCreatUser(User creatUser) {
		this.creatUser = creatUser;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public String getOldCard() {
		return oldCard;
	}
	public void setOldCard(String oldCard) {
		this.oldCard = oldCard;
	}
	public Date getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	public String getCreatUserId() {
		return creatUserId;
	}
	public void setCreatUserId(String creatUserId) {
		this.creatUserId = creatUserId;
	}
		
			
}
