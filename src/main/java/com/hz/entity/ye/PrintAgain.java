package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.system.User;

/**
 * 火化证明补打记录
 * @author jgj
 *
 */
public class PrintAgain {

	/**
	 * 补打id
	 */
	private String id;
	/**
	 * 火化打印id
	 */
	private String printId;
	/**
	 * 操作员id
	 */
	private String userId;
	/**
	 * 补打领取人
	 */
	private String takeNameAgain;
	/**
	 * 补打领取人证件号
	 */
	private String takeCodeAgain;
	/**
	 * 火化证明 补打时间
	 */
	private Timestamp printTimeAgain;
	/**
	 * 操作用户
	 */
	private User user;
	

	public String getTakeNameAgain() {
		return takeNameAgain;
	}

	public void setTakeNameAgain(String takeNameAgain) {
		this.takeNameAgain = takeNameAgain;
	}

	public String getTakeCodeAgain() {
		return takeCodeAgain;
	}

	public void setTakeCodeAgain(String takeCodeAgain) {
		this.takeCodeAgain = takeCodeAgain;
	}

	public Timestamp getPrintTimeAgain() {
		return printTimeAgain;
	}

	public void setPrintTimeAgain(Timestamp printTimeAgain) {
		this.printTimeAgain = printTimeAgain;
	}
	public String getPrintId() {
		return printId;
	}
	public void setPrintId(String printId) {
		this.printId = printId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
}
