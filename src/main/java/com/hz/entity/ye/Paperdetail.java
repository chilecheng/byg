package com.hz.entity.ye;

public class Paperdetail {
	
	/**
	 * 纸棺id，即在ba_item中的id
	 */
	private String id;
	
	/**
	 * 纸棺具体名称，如豪华型
	 */
	private String paperName;
	/**
	 * 纸棺类型，该值就是“纸棺”
	 */
	//private String typeName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPaperName() {
		return paperName;
	}
	public void setPaperName(String paperName) {
		this.paperName = paperName;
	}
/*	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}*/
	
	

}
