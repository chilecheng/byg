package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.base.Farewell;
import com.hz.entity.system.User;
/**
 * 告别厅记录
 * @author rgy
 *
 */
public class FarewellRecord {
	//id
	private String id;
	//告别厅id
	private String farewellId;
	//告别厅
	private Farewell farewell;
	//告别开始时间
	private Timestamp beginDate;
	//告别结束时间
	private Timestamp endDate;
	//添加时间
	private Timestamp createTime;
	//添加人
	private String createUserId;
	//业务单id
	private String commissionOrderId;
	//火化委托单
	private CommissionOrder order;
	//预约登记名字
	private String appointmentName;
	//标志
	private byte flag;
	//备注
	private String comment;
	//司仪人员ID
	private String emceeId;
	//主持费
	private double chargeFee;
	//录入人员ID
	private String enterId;
	//布置时间
	private Timestamp arrangeTime;
	//维修费用
	private double repairFee;
	//状态
	private Byte isdel;
	//用户信息
	private User user;
	/**
	 * 是否具有礼仪出殡项
	 * @return
	 */
	private byte isLIYI;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAppointmentName() {
		return appointmentName;
	}

	public void setAppointmentName(String appointmentName) {
		this.appointmentName = appointmentName;
	}

	public double getRepairFee() {
		return repairFee;
	}

	public void setRepairFee(double repairFee) {
		this.repairFee = repairFee;
	}

	public double getChargeFee() {
		return chargeFee;
	}

	public void setChargeFee(double chargeFee) {
		this.chargeFee = chargeFee;
	}

	public String getEnterId() {
		return enterId;
	}

	public void setEnterId(String enterId) {
		this.enterId = enterId;
	}

	public Timestamp getArrangeTime() {
		return arrangeTime;
	}

	public void setArrangeTime(Timestamp arrangeTime) {
		this.arrangeTime = arrangeTime;
	}

	public String getEmceeId() {
		return emceeId;
	}

	public void setEmceeId(String emceeId) {
		this.emceeId = emceeId;
	}

	public Timestamp getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFarewellId() {
		return farewellId;
	}

	public void setFarewellId(String farewellId) {
		this.farewellId = farewellId;
	}
	public CommissionOrder getOrder() {
		return order;
	}

	public void setOrder(CommissionOrder order) {
		this.order = order;
	}
	public Farewell getFarewell() {
		return farewell;
	}

	public void setFarewell(Farewell farewell) {
		this.farewell = farewell;
	}

	public Timestamp getBeginDate() {
		return beginDate;
		
	}

	public void setBeginDate(Timestamp date) {
		this.beginDate = date;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCommissionOrderId() {
		return commissionOrderId;
	}

	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}

	public byte getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}

	public Byte getIsdel() {
		return isdel;
	}

	public void setIsdel(Byte isdel) {
		this.isdel = isdel;
	}

	public byte getIsLIYI() {
		return isLIYI;
	}

	public void setIsLIYI(byte isLIYI) {
		this.isLIYI = isLIYI;
	}

	
	
}
