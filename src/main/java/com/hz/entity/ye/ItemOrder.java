package com.hz.entity.ye;

import java.sql.Timestamp;
import com.hz.entity.base.BillUnit;
import com.hz.entity.base.Item;
import com.hz.entity.system.User;
import com.hz.util.Const;
/**
 * 火化委托单收费项目
 * @author tsp
 *
 */
public class ItemOrder {
	/**
	 * id
	 */
	private String id;
	/**
	 * 服务项目
	 */
	private String itemId;
	/**
	 * 服务项目
	 */
	private Item item;
	/**
	 * 单价(与item表的单价可以不同)
	 */
	private double pice;
	/**
	 * 订单流水号
	 */
	private String orderNumber;
	/**
	 * 数量
	 */
	private int number;
	/**
	 * 挂账标志
	 */
	private byte tickFlag;
	/**
	 * 合计
	 */
	private double total;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 收款标志
	 */
	private byte payFlag;
	/**
	 * 收款时间
	 */
	private Timestamp payTime;
	/**
	 * 业务单Id
	 */
	private String commissionOrderId;
	/**
	 * 业务单
	 */
	private CommissionOrder commissionOrder;
	/**
	 * 布置人员ID
	 */
	private String handlerId;
	/**
	 * 布置人员
	 */
	private User handler;
	/**
	 * 挂账单位
	 */
	private BillUnit billUnit;
	/**
	 * 挂账收费标志
	 */
	private byte billPayFlag;

	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public double getPice() {
		return pice;
	}
	public void setPice(double pice) {
		this.pice = pice;
	}
	public BillUnit getBillUnit() {
		return billUnit;
	}
	public void setBillUnit(BillUnit billUnit) {
		this.billUnit = billUnit;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public byte getTickFlag() {
		return tickFlag;
	}
	public String getHandlerId() {
		return handlerId;
	}
	public void setHandlerId(String handlerId) {
		this.handlerId = handlerId;
	}
	public void setTickFlag(byte tickFlag) {
		this.tickFlag = tickFlag;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public byte getPayFlag() {
		return payFlag;
	}
	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}
	public User getHandler() {
		return handler;
	}
	public void setHandler(User handler) {
		this.handler = handler;
	}
	public byte getBillPayFlag() {
		return billPayFlag;
	}
	public void setBillPayFlag(byte billPayFlag) {
		this.billPayFlag = billPayFlag;
	}
	
	
	/**
	 * 是否挂账
	 * @return
	 */
//	public String getIsdelTickName(){
//		return Const.getTickFlag(this.tickFlag);
//		
//	}
	
}
