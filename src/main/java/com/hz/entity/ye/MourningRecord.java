package com.hz.entity.ye;

import java.sql.Timestamp;
import com.hz.entity.base.Mourning;
import com.hz.entity.system.User;

/**
 * 灵堂记录
 * @author rgy
 *
 */
public class MourningRecord {
	//id
	private String id;
	//守灵室id
	private String mourningId;
	//灵堂
	private Mourning mourning;
	//守灵开始时间
	private Timestamp beginTime;
	//守灵结束时间
	private Timestamp endTime;
	//添加时间
	private Timestamp createTime;
	//添加人
	private String createUserId;
	//添加业务单id
	private String commissionOrderId;
	//业务单
	private CommissionOrder order;
	//标志
	private byte flag;
	//录入人员ID
	private String enterId;
	//布置时间
	private Timestamp arrangeTime; 
	//维修费用
	private double repairFee;
	//维修原因(备注)
	private String comment;
	//状态
	private Byte isdel;
	//预约登记名字
	private String appointmentName;
	//用户
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getAppointmentName() {
		return appointmentName;
	}
	public void setAppointmentName(String appointmentName) {
		this.appointmentName = appointmentName;
	}
	public Byte getIsdel() {
		return isdel;
	}
	public void setIsdel(Byte isdel) {
		this.isdel = isdel;
	}
	public double getFee() {
		return repairFee;
	}
	public void setFee(double repairFee) {
		this.repairFee = repairFee;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getEnterId() {
		return enterId;
	}
	public void setEnterId(String enterId) {
		this.enterId = enterId;
	}
	public Timestamp getArrangeTime() {
		return arrangeTime;
	}
	public void setArrangeTime(Timestamp arrangeTime) {
		this.arrangeTime = arrangeTime;
	}
	public CommissionOrder getOrder() {
		return order;
	}
	public void setOrder(CommissionOrder order) {
		this.order = order;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMourningId() {
		return mourningId;
	}
	public void setMourningId(String mourningId) {
		this.mourningId = mourningId;
	}
	public Mourning getMourning() {
		return mourning;
	}
	public void setMourning(Mourning mourning) {
		this.mourning = mourning;
	}
	public Timestamp getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	
}
