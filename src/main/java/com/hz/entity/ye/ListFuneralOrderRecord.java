package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 丧葬用品购买数据列表
 * @author lipengpeng
 *
 */
//有些字段没有在y_buy_goods_record表中出现（比如委托单中的一些字段），但是为了便于读取、显示在页面也给出。
public class ListFuneralOrderRecord {
	
	private String id;
	private String dName;
	private String commissionorderid;
	
	private String buyOrderId;
	
	private String flowId;

	private String deadName;
	
	private Timestamp buyTime;//

	private String takeName;
	
	private String farewellName;
	
	private String mourningName;
	
	private Timestamp farewellTime;//
	
	private Timestamp cremationTime;//
	
	private String remarktext;
	
	private String relationship;

	private String relationshipid;
	
	private String createUserId;
	
	private Timestamp takeawayTime;//
	
	private String replaceName;
	
	private String dSex;
	
	private String dAge;
	
	private String cardCode;
	
	private Timestamp mourningTime;//
	
	//丧葬用品领用状态
	private Byte dealIsdel;
	
	public String getdName() {
		return dName;
	}

	public void setdName(String dName) {
		this.dName = dName;
	}

	public Byte getDealIsdel() {
		return dealIsdel;
	}

	public void setDealIsdel(Byte dealIsdel) {
		this.dealIsdel = dealIsdel;
	}
	//支付状态 
	private byte payFlag;
	//支付时间
	private Timestamp payTime;
	//付款人
	private String payName;
	//实收金额
	private double actualAmount;
	//找零
	private double giveChange;
	//支付方式
	private byte payType;
	//实际支付金额
	private double actuallyPaid;
	
	
	
	
	public double getActuallyPaid() {
		return actuallyPaid;
	}

	public void setActuallyPaid(double actuallyPaid) {
		this.actuallyPaid = actuallyPaid;
	}

	public String getPayTypeName(){
		return Const.getPayType(payType);
	}
	
	public byte getPayType() {
		return payType;
	}

	public void setPayType(byte payType) {
		this.payType = payType;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}

	public double getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(double actualAmount) {
		this.actualAmount = actualAmount;
	}

	public double getGiveChange() {
		return giveChange;
	}

	public void setGiveChange(double giveChange) {
		this.giveChange = giveChange;
	}

	public byte getPayFlag() {
		return payFlag;
	}

	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}

	public Timestamp getPayTime() {
		return payTime;
	}

	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}

	public String getRelationshipid() {
		return relationshipid;
	}

	public void setRelationshipid(String relationshipid) {
		this.relationshipid = relationshipid;
	}
	
	public Timestamp getMourningTime() {
		return mourningTime;
	}

	public void setMourningTime(Timestamp mourningTime) {
		this.mourningTime = mourningTime;
	}

	public String getdSex() {
		return dSex;
	}

	public void setdSex(String dSex) {
		this.dSex = dSex;
	}

	public String getdAge() {
		return dAge;
	}

	public void setdAge(String dAge) {
		this.dAge = dAge;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getBuyOrderId() {
		return buyOrderId;
	}

	public void setBuyOrderId(String buyOrderId) {
		this.buyOrderId = buyOrderId;
	}

	
	public String getReplaceName() {
		return replaceName;
	}

	public void setReplaceName(String replaceName) {
		this.replaceName = replaceName;
	}


	
	public Timestamp getTakeawayTime() {
		return takeawayTime;
	}

	public void setTakeawayTime(Timestamp takeawayTime) {
		this.takeawayTime = takeawayTime;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getRemarktext() {
		return remarktext;
	}

	public void setRemarktext(String remarktext) {
		this.remarktext = remarktext;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCommissionorderid() {
		return commissionorderid;
	}

	public void setCommissionorderid(String commissionorderid) {
		this.commissionorderid = commissionorderid;
	}
	
	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getDeadName() {
		return deadName;
	}

	public void setDeadName(String deadName) {
		this.deadName = deadName;
	}

	public Timestamp getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(Timestamp buyTime) {
		this.buyTime = buyTime;
	}

	public String getTakeName() {
		return takeName;
	}

	public void setTakeName(String takeName) {
		this.takeName = takeName;
	}

	public String getFarewellName() {
		return farewellName;
	}

	public void setFarewellName(String farewellName) {
		this.farewellName = farewellName;
	}

	public String getMourningName() {
		return mourningName;
	}

	public void setMourningName(String mourningName) {
		this.mourningName = mourningName;
	}

	public Timestamp getFarewellTime() {
		return farewellTime;
	}

	public void setFarewellTime(Timestamp farewellTime) {
		this.farewellTime = farewellTime;
	}

	public Timestamp getCremationTime() {
		return cremationTime;
	}

	public void setCremationTime(Timestamp cremationTime) {
		this.cremationTime = cremationTime;
	}
	/**
	 * 显示 是否收费
	 * @return
	 */
	public String getIfPay() {
		return Const.getIfPay(this.payFlag);
	}
}
