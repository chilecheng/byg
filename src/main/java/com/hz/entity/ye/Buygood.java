package com.hz.entity.ye;

import java.sql.Timestamp;
import java.util.List;

/**
 * 丧葬用品购买记录
 * @author Administrator
 * 
 */

public class Buygood {
	private String id;
	
	private String remarks;
	
	private String buyOrderId;//购买单号
	
	public String getBuyOrderId() {
		return buyOrderId;
	}

	public void setBuyOrderId(String buyOrderId) {
		this.buyOrderId = buyOrderId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	

	//购买项记录表
	//  y_buy_good_record_d
	//	private List<ItemOrder> listItemOrder;
	private List<BuyGoodD> listbuyGoodD;
	
	public List<BuyGoodD> getListbuyGoodD() {
		return listbuyGoodD;
	}

	public void setListbuyGoodD(List<BuyGoodD> listbuyGoodD) {
		this.listbuyGoodD = listbuyGoodD;
	}



	private String commissionOrderId;

	public String getCommissionOrderId() {
		return commissionOrderId;
	}

	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	// 带走时间
	public Timestamp getTakeawayTime() {
		return takeawayTime;
	}

	public void setTakeawayTime(Timestamp takeawayTime) {
		this.takeawayTime = takeawayTime;
	}



	public Timestamp takeawayTime;
	
	/**
	 * 购买人姓名
	 */
	public String takeName;
	
	public String getTakeName() {
		return takeName;
	}

	public void setTakeName(String takeName) {
		this.takeName = takeName;
	}



	//付款时间
	public Timestamp payTime;

	public Timestamp getPayTime() {
		return payTime;
	}

	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	//fufei人姓名
	public String payName;

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}
	
	public Byte dealIsdel;

	public Byte getDealIsdel() {
		return dealIsdel;
	}

	public void setDealIsdel(Byte dealIsdel) {
		this.dealIsdel = dealIsdel;
	}

}
