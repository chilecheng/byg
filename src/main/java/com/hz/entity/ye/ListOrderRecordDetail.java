package com.hz.entity.ye;

import java.sql.Timestamp;


public class ListOrderRecordDetail {
	
	private String id;
	
	private String buyRecordId;
	
	private String itemId;
	
	private String itemName;
	
	private Double pice;
	
	private Double total;
	
	private Integer number;
	
	private String remarks;
	
	private String iscredit;
	
	//以下用于收费科非委托业务,统一字段使用	ps:注释很重要,注释很重要,注释很重要!!!
	//备注
	private String comment;
	//用品名称
	private String name;
	//购买时间
	private Timestamp creatTime;
	
	
	
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIscredit() {
		return iscredit;
	}

	public void setIscredit(String iscredit) {
		this.iscredit = iscredit;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBuyRecordId() {
		return buyRecordId;
	}

	public void setBuyRecordId(String buyRecordId) {
		this.buyRecordId = buyRecordId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getPice() {
		return pice;
	}

	public void setPice(Double pice) {
		this.pice = pice;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}
	
	
}
