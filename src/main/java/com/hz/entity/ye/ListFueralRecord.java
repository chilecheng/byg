package com.hz.entity.ye;

import java.sql.Timestamp;



/**
 * 礼仪出殡的 火化委托单 属性
 * @author czl
 *
 */
public class ListFueralRecord {
	/**
	 * 火化委托单id
	 */
	private String id;
	/**
	 * 告别时间
	 */
	private Timestamp farewellTime;
	/**
	 * 死者姓名
	 */
	private String name;
	/**
	 * 年龄
	 */
	private int age;
	/**
	 * 性别
	 */
	private byte sex;
	/**
	 * 纸馆的价格
	 */
	private Double price;
	/**
	 * 告别厅信息
	 */
	private String farewellId;
	/**
	 * 预约火化时间
	 */
	private Timestamp cremationTime;
	/**
	 * 火化炉类型
	 */
	private String furnaceTypeId;
	public String getFurnaceFlag() {
		return furnaceFlag;
	}
	public void setFurnaceFlag(String furnaceFlag) {
		this.furnaceFlag = furnaceFlag;
	}
	/**
	 * 礼仪出殡项目 状态
	 */
	private String dealIsdel;
	/**
	 * 火化炉类型名称
	 */
	private String furnaceFlag;
	private String funeralName;
	/**
	 * 礼仪出殡查看次数 首页用
	 */
	private byte viewsFuneral;
	/**
	 * y_item_order id
	 */
	private String itemId;
	/**
	 * 是否收费
	 */
	private String checkFlag;
	/**
	 * ka卡号
	 */
	private String cardCode;
	
	
	
	public String getCardCode() {
		return cardCode;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}
	public String getCheckFlag() {
		return checkFlag;
	}
	public void setCheckFlag(String checkFlag) {
		this.checkFlag = checkFlag;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public byte getViewsFuneral() {
		return viewsFuneral;
	}
	public void setViewsFuneral(byte viewsFuneral) {
		this.viewsFuneral = viewsFuneral;
	}
	public String getFuneralName() {
		return funeralName;
	}
	public void setFuneralName(String funeralName) {
		this.funeralName = funeralName;
	}
	public String getDealIsdel() {
		return dealIsdel;
	}
	public void setDealIsdel(String dealIsdel) {
		this.dealIsdel = dealIsdel;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Timestamp getFarewellTime() {
		return farewellTime;
	}
	public void setFarewellTime(Timestamp farewellTime) {
		this.farewellTime = farewellTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getFarewellId() {
		return farewellId;
	}
	@Override
	public String toString() {
		return "ListFueralRecord [id=" + id + ", farewellTime=" + farewellTime + ", name=" + name + ", age=" + age
				+ ", sex=" + sex + ", price=" + price + ", farewellId=" + farewellId + ", cremationTime="
				+ cremationTime + ", furnaceTypeId=" + furnaceTypeId + ", dealIsdel=" + dealIsdel + ", furnaceFlag="
				+ furnaceFlag + ", funeralName=" + funeralName + ", viewsFuneral=" + viewsFuneral + ", itemId=" + itemId
				+ ", checkFlag=" + checkFlag + ", cardCode=" + cardCode + "]";
	}
	public void setFarewellId(String farewellId) {
		this.farewellId = farewellId;
	}
	public Timestamp getCremationTime() {
		return cremationTime;
	}
	public void setCremationTime(Timestamp cremationTime) {
		this.cremationTime = cremationTime;
	}
	public String getFurnaceTypeId() {
		return furnaceTypeId;
	}
	public void setFurnaceTypeId(String furnaceTypeId) {
		this.furnaceTypeId = furnaceTypeId;
	}
	
	
	
	
	
	
}
