package com.hz.entity.ye.staticalForm;

import java.sql.Timestamp;
import java.util.List;

import com.hz.entity.base.Item;
import com.hz.entity.base.Toponym;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CommissionOrder;

/**
 * 基本殡葬减免统计
 * @author Administrator
 *
 */
public class BaseReductionStaticalForm {
	/**
	 * 统计id
	 */
	private String id;
	/**
	 * 火化委托单
	 */
	private CommissionOrder commissionOrder;
	/**
	 * 困难减免项目
	 */
	private List<BaseReductionD> baseReductionD;
	/**
	 * 项目
	 */
	private List<Item> item;
	/**
	 * 户籍
	 */
	private Toponym toponym;
	private String total;
	private Timestamp payTime;
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public Toponym getToponym() {
		return toponym;
	}
	public void setToponym(Toponym toponym) {
		this.toponym = toponym;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public List<Item> getItem() {
		return item;
	}
	public void setItem(List<Item> item) {
		this.item = item;
	}
	public List<BaseReductionD> getBaseReductionD() {
		return baseReductionD;
	}
	public void setBaseReductionD(List<BaseReductionD> baseReductionD) {
		this.baseReductionD = baseReductionD;
	}
	
	
}
