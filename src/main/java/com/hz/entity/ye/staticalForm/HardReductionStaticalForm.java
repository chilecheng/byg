package com.hz.entity.ye.staticalForm;

import java.sql.Timestamp;
import java.util.List;

import com.hz.entity.base.Item;
import com.hz.entity.base.Register;
import com.hz.entity.base.Toponym;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.HardReductionD;

/**
 * 困难殡葬减免统计
 * @author Administrator
 *
 */
public class HardReductionStaticalForm {
	/**
	 * 统计id
	 */
	private String id;
	/**
	 * 火化委托单
	 */
	private CommissionOrder commissionOrder;
	/**
	 * 困难减免项目
	 */
	private List<HardReductionD> hardReductionD;
	private Toponym toponym;
	private String total;
	private Timestamp payTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public List<HardReductionD> getHardReductionD() {
		return hardReductionD;
	}
	public void setHardReductionD(List<HardReductionD> hardReductionD) {
		this.hardReductionD = hardReductionD;
	}
	public Toponym getToponym() {
		return toponym;
	}
	public void setToponym(Toponym toponym) {
		this.toponym = toponym;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
}
