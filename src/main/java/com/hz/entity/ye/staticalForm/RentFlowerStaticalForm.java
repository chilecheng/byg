package com.hz.entity.ye.staticalForm;

import java.util.Date;


public class RentFlowerStaticalForm {
	/**
	 * 数量
	 */
	private int number;
	/**
	 * 名字
	 */
	private String name;
	/**
	 * 创建时间
	 */
	private Date creatTime;
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	
	
	
	
	
	
}
