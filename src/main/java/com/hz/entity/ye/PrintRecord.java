package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.system.User;

/**
 * 火化证明打印
 * @author lipengpeng
 *
 */
public class PrintRecord {

	/**
	 * y_print_record的id
	 */
	private String id;
	/**
	 * 委托单id
	 */
	private String cid;
	/**
	 * 领取人名字
	 */
	private String takeName;
	/**
	 * 领取人身份证
	 */
	private String takeCode;
	/**
	 * 操作员id
	 */
	private String userId;
	/**
	 * 打印时间
	 */
	private Timestamp printTime;
	/**
	 * 补打领取人
	 */
	private String takeNameAgain;
	/**
	 * 补打领取人证件号
	 */
	private String takeCodeAgain;
	/**
	 * 火化证明 补打时间
	 */
	private Timestamp printTimeAgain;
	/**
	 * 操作用户
	 */
	private User user;
	/**
	 * 是否补打
	 */
	private byte isPrintAgain;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTakeNameAgain() {
		return takeNameAgain;
	}

	public void setTakeNameAgain(String takeNameAgain) {
		this.takeNameAgain = takeNameAgain;
	}

	public String getTakeCodeAgain() {
		return takeCodeAgain;
	}

	public void setTakeCodeAgain(String takeCodeAgain) {
		this.takeCodeAgain = takeCodeAgain;
	}

	public Timestamp getPrintTimeAgain() {
		return printTimeAgain;
	}

	public void setPrintTimeAgain(Timestamp printTimeAgain) {
		this.printTimeAgain = printTimeAgain;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getTakeName() {
		return takeName;
	}

	public void setTakeName(String takeName) {
		this.takeName = takeName;
	}

	public String getTakeCode() {
		return takeCode;
	}

	public void setTakeCode(String takeCode) {
		this.takeCode = takeCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getPrintTime() {
		return printTime;
	}

	public void setPrintTime(Timestamp printTime) {
		this.printTime = printTime;
	}

	public byte getIsPrintAgain() {
		return isPrintAgain;
	}

	public void setIsPrintAgain(byte isPrintAgain) {
		this.isPrintAgain = isPrintAgain;
	}
	
}
