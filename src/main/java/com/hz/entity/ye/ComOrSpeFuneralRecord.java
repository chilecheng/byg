package com.hz.entity.ye;

import java.sql.Timestamp;

public class ComOrSpeFuneralRecord {
	
	private String id;
	/**
	 * 火化炉ID
	 */
	private String furnaceId;
	/**
	 * 预约火化时间
	 */
	private Timestamp orderTime;
	/**
	 * 火化开始时间
	 */
	private Timestamp beginTime;

	/**
	 * 火化工ID
	 */
	private String workerId;
	/**
	 * 添加时间
	 */
	private Timestamp createTime;
	/**
	 * 添加人ID
	 */
	private String createUserId;
	/**
	 * 业务单ID
	 */
	private String commissionOrderId;
	/**
	 * 火化委托单
	 */
	private CommissionOrder order;
	/**
	 * 状态
	 */
	private byte flag;
	/**
	 * 备注
	 */
	private String comment;
	
	private byte changeFlag;
	
	private String originalId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFurnaceId() {
		return furnaceId;
	}

	public void setFurnaceId(String furnaceId) {
		this.furnaceId = furnaceId;
	}

	public Timestamp getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}

	public Timestamp getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}

	public String getWorkerId() {
		return workerId;
	}

	public void setWorkerId(String workerId) {
		this.workerId = workerId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCommissionOrderId() {
		return commissionOrderId;
	}

	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}

	public CommissionOrder getOrder() {
		return order;
	}

	public void setOrder(CommissionOrder order) {
		this.order = order;
	}

	public byte getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public byte getChangeFlag() {
		return changeFlag;
	}

	public void setChangeFlag(byte changeFlag) {
		this.changeFlag = changeFlag;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}


	
	

}
