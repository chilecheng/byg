package com.hz.entity.ye;

import java.sql.Timestamp;
import java.util.List;

import com.hz.entity.base.Appellation;
import com.hz.entity.base.BillUnit;
import com.hz.entity.base.Certificate;
import com.hz.entity.base.CorpseUnit;
import com.hz.entity.base.DeadReason;
import com.hz.entity.base.DeadType;
import com.hz.entity.base.Farewell;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.Furnace;
import com.hz.entity.base.FurnaceType;
import com.hz.entity.base.Mourning;
import com.hz.entity.base.ProveUnit;
import com.hz.entity.base.Register;
import com.hz.entity.base.Toponym;
import com.hz.entity.payDivision.BatchLosses;
import com.hz.entity.system.User;
import com.hz.util.Const;

/**
 * 火化委托单
 * 
 * @author tsp
 *
 */
public class CommissionOrder {
	
	private String id;
	/**
	 * 编号
	 */
	private String code;
	/**
	 * 死者姓名
	 */
	private String name;
	/**
	 * 性别
	 */
	private byte sex;
	/**
	 * 年龄
	 */
	private int age;
	/**
	 * 户籍
	 */
	private Register register;
	/**
	 * 户籍
	 */
	private String registerId;
	/**
	 * 地区
	 */
	private Toponym toponym;
	/**
	 * 省
	 */
	private String province;
	/**
	 * 市
	 */
	private String city;
	/**
	 * 区
	 */
	private String area;
	/**
	 * 地区
	 */
	private String toponymId;
	/**
	 * 卡号
	 */
	private String cardCode;
	/**
	 * 证件类型
	 */
	private Certificate certificate;
	/**
	 * 证件类型
	 */
	private String certificateId;
	/**
	 * 证件号
	 */
	private String certificateCode;
	/**
	 * 死者住址
	 */
	private String dAddr;
	/**
	 * 民族
	 */
	private String dNationId;
	/**
	 * 接尸地址
	 */
	private String pickAddr;
	/**
	 * 死亡类型
	 */
	private DeadType deadType;
	/**
	 * 死亡类型
	 */
	private String deadTypeId;
	/**
	 * 死亡原因
	 */
	private DeadReason deadReason;
	/**
	 * 死亡原因
	 */
	private String deadReasonId;
	/**
	 * 死亡时间
	 */
	private Timestamp dTime;
	/**
	 * 证明单位
	 */
	private ProveUnit proveUnit;
	/**
	 * 证明单位
	 */
	private String proveUnitId;
	/**
	 * 证明单位
	 */
	private String proveUnitContent;
	/**
	 * 挂账单位ID
	 */
	private String billUnitId;
	/**
	 * 挂账单位类
	 */
	private BillUnit billUnit;
	/**
	 * 火化炉使用单 业务扩展新增项
	 */
	private FurnaceRecord furnaceRecord;
	/**
	 * 火化炉名 业务扩展新增项
	 */
	private String furnaceName;
	/**
	 * 火化 时间 业务扩展新增项
	 */
	private Timestamp huohuaTime;
	/**
	 * 委托单最新更新时间
	 */
	private Timestamp updateTime;

	

	/**
	 * 收费科批量挂账类
	 */
	private BatchLosses batchLosses;
	
	/**
	 * 遗体处理时间
	 */
	private Timestamp dealTime;
	
	/**
	 * 经办人
	 */
	private User agentUser;
	/**
	 * 经办人
	 */
	private String agentUserId;
	/**
	 * 添加时间
	 */
	private Timestamp creatTime;
	/**
	 * 审核状态
	 */
	private byte checkFlag;
	/**
	 * 审核时间
	 */
	private Timestamp checkTime;
	/**
	 * 审核人
	 */
	private User checkUser;
	/**
	 * 审核人
	 */
	private String checkUserId;
	/**
	 * 死亡证明书
	 */
	private byte dFlag;

	// 家属信息或委托人信息
	/**
	 * 家属姓名
	 */
	private String fName;
	/**
	 * 与死者关系
	 */
	private Appellation appellation;
	/**
	 * 与死者关系
	 */
	private String fAppellationId;
	/**
	 * 单位
	 */
	private String fUnit;
	/**
	 * 联系号码
	 */
	private String fPhone;
	/**
	 * 住址
	 */
	private String fAddr;
	/**
	 * 家属身份证号
	 */
	private String fCardCode;

	// 业务调度信息
	/**
	 * 到馆时间
	 */
	private Timestamp arriveTime;
	/**
	 * 守灵室信息
	 */
	private Mourning mourning;
	/**
	 * 守灵室信息
	 */
	private String mourningId;
	/**
	 * 守灵结束时间
	 */
	private Timestamp mourningEndTime;
	/**
	 * 守灵时间
	 */
	private Timestamp mourningTime;

	/**
	 * 冷藏柜信息
	 */
	private Freezer freezer;
	/**
	 * 冷藏柜信息
	 */
	private String freezerId;
	/**
	 * 告别厅信息
	 */
	private Farewell farewell;
	/**
	 * 告别厅信息
	 */
	private String farewellId;
	/**
	 * 告别时间
	 */
	private Timestamp farewellTime;
	/**
	 * 接尸单位
	 */
	private CorpseUnit corpseUnit;
	/**
	 * 接尸单位
	 */
	private String corpseUnitId;
	/**
	 * 火化炉信息
	 */
	private Furnace furnace;
	/**
	 * 火化炉信息
	 */
	private String furnaceId;
	/**
	 * 火化炉业务类型
	 */
	private byte furnaceFlag;
	/**
	 * 火化炉类型
	 */
	private FurnaceType furnaceType;
	/**
	 * 火化炉类型
	 */
	private String furnaceTypeId;
	/**
	 * 火化炉备注信息
	 */
	private String furnaceComment;
	/**
	 * 预约火化时间
	 */
	private Timestamp cremationTime;
	/**
	 * 死者身份证图片
	 */
	private String dIdcardPic;
	/**
	 * 经办人身份证图片
	 */
	private String eIdcardPic;
	/**
	 * 拍照存档
	 */
	private String photoPic;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 火化标志
	 */
	private byte cremationFlag;
	/**
	 * 缴费状态
	 */
	private byte payFlag;
	/**
	 * 已收金额
	 */
	private double receivedMoney;
	/**
	 * 电话预约登记人
	 */
	private String pbookAgentId;
	/**
	 * 二科登记经办人
	 */
	private String bookAgentId;
	/**
	 * 查看次数  未收费
	 */
	private byte viewsUnpay;
	/**
	 * 查看次数  未审核
	 */
	private byte viewsUncheck;
	/**
	 * 查看次数  告别厅
	 */
	private byte viewsFarewell;
	/**
	 * 查看次数  灵堂 
	 */
	private byte viewsMourning;
	/**
	 * 火化炉调度ID 首页用
	 */
	private String crematorId;
	/**
	 * 查看次数  特约炉
	 */
	private byte viewsTY;
	/**
	 * 查看次数  普通炉
	 */
	private byte viewsPT;
	/**
	 * 支付的项目数量，用于限制委托单修改，大于0则无法修改
	 */
	private int payNumber;
	
	
	public int getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(int payNumber) {
		this.payNumber = payNumber;
	}

	public Timestamp getDealTime() {
		return dealTime;
	}

	public void setDealTime(Timestamp dealTime) {
		this.dealTime = dealTime;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public BillUnit getBillUnit() {
		return billUnit;
	}

	public void setBillUnit(BillUnit billUnit) {
		this.billUnit = billUnit;
	}
	public String getFurnaceComment() {
		return furnaceComment;
	}

	public void setFurnaceComment(String furnaceComment) {
		this.furnaceComment = furnaceComment;
	}

	public byte getViewsTY() {
		return viewsTY;
	}

	public void setViewsTY(byte viewsTY) {
		this.viewsTY = viewsTY;
	}

	public byte getViewsPT() {
		return viewsPT;
	}

	public void setViewsPT(byte viewsPT) {
		this.viewsPT = viewsPT;
	}

	public byte getViewsUnpay() {
		return viewsUnpay;
	}

	public void setViewsUnpay(byte viewsUnpay) {
		this.viewsUnpay = viewsUnpay;
	}

	public byte getViewsUncheck() {
		return viewsUncheck;
	}

	public void setViewsUncheck(byte viewsUncheck) {
		this.viewsUncheck = viewsUncheck;
	}

	public byte getViewsFarewell() {
		return viewsFarewell;
	}

	public void setViewsFarewell(byte viewsFarewell) {
		this.viewsFarewell = viewsFarewell;
	}

	public byte getViewsMourning() {
		return viewsMourning;
	}

	public void setViewsMourning(byte viewsMourning) {
		this.viewsMourning = viewsMourning;
	}

	public String getCrematorId() {
		return crematorId;
	}
	public void setCrematorId(String crematorId) {
		this.crematorId = crematorId;
	}
	public String getPbookAgentId() {
		return pbookAgentId;
	}

	public void setPbookAgentId(String pbookAgentId) {
		this.pbookAgentId = pbookAgentId;
	}

	public String getBookAgentId() {
		return bookAgentId;
	}

	public void setBookAgentId(String bookAgentId) {
		this.bookAgentId = bookAgentId;
	}



	
	/**
	 * 获取火化炉类型名称
	 */
	public String getFurnaceTypeName() {
		if (this.furnaceFlag == Const.furnace_pt) {
			return "普通炉";
		} else if (this.furnaceFlag == Const.furnace_ty) {
			return "特约炉";
		} else {
			return "未填写";
		}
	}

	public byte getPayFlag() {
		return payFlag;
	}

	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}

	public double getReceivedMoney() {
		return receivedMoney;
	}

	public void setReceivedMoney(double receivedMoney) {
		this.receivedMoney = receivedMoney;
	}

	public FurnaceRecord getFurnaceRecord() {
		return furnaceRecord;
	}

	public void setFurnaceRecord(FurnaceRecord furnaceRecord) {
		this.furnaceRecord = furnaceRecord;
	}

	public String getFurnaceName() {
		return furnaceName;
	}

	public void setFurnaceName(String furnaceName) {
		this.furnaceName = furnaceName;
	}
	/**
	 * 服务项目收费
	 */
	private List<ItemOrder> listItemOrder;

	public List<ItemOrder> getListItemOrder() {
		return listItemOrder;
	}

	public void setListItemOrder(List<ItemOrder> listItemOrder) {
		this.listItemOrder = listItemOrder;
	}

	/**
	 * 处理状态
	 */
	private byte dealIsdel;

	public byte getDealIsdel() {
		return dealIsdel;
	}

	public void setDealIsdel(byte dealIsdel) {
		this.dealIsdel = dealIsdel;
	}

	/**
	 * 进度标志
	 */
	private byte scheduleFlag;
	/**
	 * 销卡标志
	 */
	private byte pincardFlag;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getRegisterId() {
		return registerId;
	}

	public BatchLosses getBatchLosses() {
		return batchLosses;
	}

	public void setBatchLosses(BatchLosses batchLosses) {
		this.batchLosses = batchLosses;
	}

	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}

	public String getCardCode() {
		return cardCode;
	}
	public Timestamp getHuohuaTime() {
		return huohuaTime;
	}

	public void setHuohuaTime(Timestamp huohuaTime) {
		this.huohuaTime = huohuaTime;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getCertificateId() {
		return certificateId;
	}
	public void setCertificateId(String certificateId) {
		this.certificateId = certificateId;
	}

	public String getCertificateCode() {
		return certificateCode;
	}

	public void setCertificateCode(String certificateCode) {
		this.certificateCode = certificateCode;
	}

	public String getBillUnitId() {
		return billUnitId;
	}

	public void setBillUnitId(String billUnitId) {
		this.billUnitId = billUnitId;
	}

	public String getdAddr() {
		return dAddr;
	}

	public void setdAddr(String dAddr) {
		this.dAddr = dAddr;
	}

	public String getdNationId() {
		return dNationId;
	}

	public void setdNationId(String dNationId) {
		this.dNationId = dNationId;
	}

	public String getPickAddr() {
		return pickAddr;
	}

	public void setPickAddr(String pickAddr) {
		this.pickAddr = pickAddr;
	}

	public String getDeadTypeId() {
		return deadTypeId;
	}

	public void setDeadTypeId(String deadTypeId) {
		this.deadTypeId = deadTypeId;
	}

	public String getDeadReasonId() {
		return deadReasonId;
	}

	public void setDeadReasonId(String deadReasonId) {
		this.deadReasonId = deadReasonId;
	}

	public Timestamp getdTime() {
		return dTime;
	}

	public void setdTime(Timestamp dTime) {
		this.dTime = dTime;
	}
	
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getProveUnitId() {
		return proveUnitId;
	}

	public void setProveUnitId(String proveUnitId) {
		this.proveUnitId = proveUnitId;
	}

	public String getAgentUserId() {
		return agentUserId;
	}

	public void setAgentUserId(String agentUserId) {
		this.agentUserId = agentUserId;
	}

	public Timestamp getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}

	public byte getCheckFlag() {
		return checkFlag;
	}

	public void setCheckFlag(byte checkFlag) {
		this.checkFlag = checkFlag;
	}

	public Timestamp getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Timestamp checkTime) {
		this.checkTime = checkTime;
	}

	public String getCheckUserId() {
		return checkUserId;
	}

	public void setCheckUserId(String checkUserId) {
		this.checkUserId = checkUserId;
	}

	public byte getdFlag() {
		return dFlag;
	}

	public void setdFlag(byte dFlag) {
		this.dFlag = dFlag;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getfAppellationId() {
		return fAppellationId;
	}

	public void setfAppellationId(String fAppellationId) {
		this.fAppellationId = fAppellationId;
	}

	public String getfUnit() {
		return fUnit;
	}

	public void setfUnit(String fUnit) {
		this.fUnit = fUnit;
	}

	public String getfPhone() {
		return fPhone;
	}

	public void setfPhone(String fPhone) {
		this.fPhone = fPhone;
	}

	public String getfAddr() {
		return fAddr;
	}

	public String getfCardCode() {
		return fCardCode;
	}

	public void setfCardCode(String fCardCode) {
		this.fCardCode = fCardCode;
	}

	public void setfAddr(String fAddr) {
		this.fAddr = fAddr;
	}

	public Timestamp getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(Timestamp arriveTime) {
		this.arriveTime = arriveTime;
	}

	public String getCorpseUnitId() {
		return corpseUnitId;
	}

	public void setCorpseUnitId(String corpseUnitId) {
		this.corpseUnitId = corpseUnitId;
	}

	public String getFurnaceTypeId() {
		return furnaceTypeId;
	}

	public void setFurnaceTypeId(String furnaceTypeId) {
		this.furnaceTypeId = furnaceTypeId;
	}

	public Timestamp getCremationTime() {
		return cremationTime;
	}

	public void setCremationTime(Timestamp cremationTime) {
		this.cremationTime = cremationTime;
	}

	public String getdIdcardPic() {
		return dIdcardPic;
	}

	public void setdIdcardPic(String dIdcardPic) {
		this.dIdcardPic = dIdcardPic;
	}

	public String geteIdcardPic() {
		return eIdcardPic;
	}

	public void seteIdcardPic(String eIdcardPic) {
		this.eIdcardPic = eIdcardPic;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public byte getCremationFlag() {
		return cremationFlag;
	}

	public void setCremationFlag(byte cremationFlag) {
		this.cremationFlag = cremationFlag;
	}

	public byte getScheduleFlag() {
		return scheduleFlag;
	}

	public void setScheduleFlag(byte scheduleFlag) {
		this.scheduleFlag = scheduleFlag;
	}

	public Register getRegister() {
		return register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}

	public Certificate getCertificate() {
		return certificate;
	}

	public void setCertificate(Certificate certificate) {
		this.certificate = certificate;
	}

	public DeadType getDeadType() {
		return deadType;
	}

	public void setDeadType(DeadType deadType) {
		this.deadType = deadType;
	}

	public DeadReason getDeadReason() {
		return deadReason;
	}

	public void setDeadReason(DeadReason deadReason) {
		this.deadReason = deadReason;
	}

	public ProveUnit getProveUnit() {
		return proveUnit;
	}

	public void setProveUnit(ProveUnit proveUnit) {
		this.proveUnit = proveUnit;
	}

	public User getAgentUser() {
		return agentUser;
	}

	public void setAgentUser(User agentUser) {
		this.agentUser = agentUser;
	}

	public User getCheckUser() {
		return checkUser;
	}

	public void setCheckUser(User checkUser) {
		this.checkUser = checkUser;
	}

	public Appellation getAppellation() {
		return appellation;
	}

	public void setAppellation(Appellation appellation) {
		this.appellation = appellation;
	}

	public CorpseUnit getCorpseUnit() {
		return corpseUnit;
	}

	public void setCorpseUnit(CorpseUnit corpseUnit) {
		this.corpseUnit = corpseUnit;
	}

	public FurnaceType getFurnaceType() {
		return furnaceType;
	}

	public void setFurnaceType(FurnaceType furnaceType) {
		this.furnaceType = furnaceType;
	}

	public String getMourningId() {
		return mourningId;
	}

	public void setMourningId(String mourningId) {
		this.mourningId = mourningId;
	}

	public String getFreezerId() {
		return freezerId;
	}

	public void setFreezerId(String freezerId) {
		this.freezerId = freezerId;
	}

	public String getFarewellId() {
		return farewellId;
	}

	public void setFarewellId(String farewellId) {
		this.farewellId = farewellId;
	}

	public Timestamp getMourningTime() {
		return mourningTime;
	}

	public void setMourningTime(Timestamp mourningTime) {
		this.mourningTime = mourningTime;
	}

	public Timestamp getFarewellTime() {
		return farewellTime;
	}

	public void setFarewellTime(Timestamp farewellTime) {
		this.farewellTime = farewellTime;
	}

	public Mourning getMourning() {
		return mourning;
	}

	public void setMourning(Mourning mourning) {
		this.mourning = mourning;
	}

	public Freezer getFreezer() {
		return freezer;
	}

	public void setFreezer(Freezer freezer) {
		this.freezer = freezer;
	}

	public Farewell getFarewell() {
		return farewell;
	}

	public void setFarewell(Farewell farewell) {
		this.farewell = farewell;
	}

	public String getProveUnitContent() {
		return proveUnitContent;
	}

	public void setProveUnitContent(String proveUnitContent) {
		this.proveUnitContent = proveUnitContent;
	}

	public byte getFurnaceFlag() {
		return furnaceFlag;
	}

	public void setFurnaceFlag(byte furnaceFlag) {
		this.furnaceFlag = furnaceFlag;

	}

	public String getPhotoPic() {
		return photoPic;
	}

	public void setPhotoPic(String photoPic) {
		this.photoPic = photoPic;
	}

	/**
	 * 获取是否审核名称
	 * 
	 * @return
	 */
	public String getCheckFlagName() {
		return Const.getCheckFlag(this.checkFlag);
	}

	/**
	 * 获取是否拥有名称
	 * 
	 * @return
	 */
	public String getFlagName() {
		return Const.getIsHave(this.dFlag);
	}

	/**
	 * 获取是否火化名称
	 * 
	 * @return
	 */
	public String getCremationFlagName() {
		return Const.getIs(this.cremationFlag);
	}

	/**
	 * 获取是否完成名称
	 * 
	 * @return
	 */
	public String getScheduleFlagName() {
		return Const.getIsComplete(this.scheduleFlag);
	}

	/**
	 * 获取性别名称
	 * 
	 * @return
	 */
	public String getSexName() {
		return Const.getSex(this.sex);
	}
	/**
	 * 获取处理状态
	 * 
	 * @return
	 */
	public String getDealIsdelName() {
		return Const.getDealIsdelType(this.dealIsdel);
	}

	public byte getPincardFlag() {
		return pincardFlag;
	}

	public void setPincardFlag(byte pincardFlag) {
		this.pincardFlag = pincardFlag;
	}

	public Timestamp getMourningEndTime() {
		return mourningEndTime;
	}

	public void setMourningEndTime(Timestamp mourningEndTime) {
		this.mourningEndTime = mourningEndTime;
	}

	public Furnace getFurnace() {
		return furnace;
	}

	public void setFurnace(Furnace furnace) {
		this.furnace = furnace;
	}

	public String getFurnaceId() {
		return furnaceId;
	}

	public void setFurnaceId(String furnaceId) {
		this.furnaceId = furnaceId;
	}
	public Toponym getToponym() {
		return toponym;
	}
	public void setToponym(Toponym toponym) {
		this.toponym = toponym;
	}
	public String getToponymId() {
		return toponymId;
	}
	public void setToponymId(String toponymId) {
		this.toponymId = toponymId;
	}
	
	public BuyWreathRecord getBuyWreathRecord() {
		return buyWreathRecord;
	}

	public void setBuyWreathRecord(BuyWreathRecord buyWreathRecord) {
		this.buyWreathRecord = buyWreathRecord;
	}

	private BuyWreathRecord buyWreathRecord;
}
