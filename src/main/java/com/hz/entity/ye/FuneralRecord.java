package com.hz.entity.ye;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author 礼仪出殡成员信息
 *
 */
public class FuneralRecord implements Serializable {

	
	/**
	 * 死者id==火化委托单的id
	 */
	private String id;
	/**
	 *  礼仪出殡 死者姓名
	 */
	private   String name;
	/**
	 * 礼仪出殡 时间
	 */
	private  Timestamp crematime;
	/**
	 * 礼仪出殡 业务办理情况
	 */
	//private String status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getCrematime() {
		return crematime;
	}
	public void setCrematime(Timestamp crematime) {
		this.crematime = crematime;
	}
	
	public FuneralRecord(String id, String name, Timestamp crematime) {
		super();
		this.id = id;
		this.name = name;
		this.crematime = crematime;
	}
	
}
