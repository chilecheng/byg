package com.hz.entity.ye;

import java.sql.Timestamp;
import java.util.List;

import com.hz.entity.system.User;
import com.hz.util.Const;

/**
 * 花圈花篮订购
 * @author hw
 *
 */
public class BuyWreathRecord {
	//id
	private String id;
	//死者姓名
	private String dName;
	//流水号
	private String serialNumber;
	//购买单号
	private String orderNumber;
	//火化委托单id
	private String commissionOrderId;
	//编号
	private String code;
	//亲属联系方式
	private String contact;
	//经办人id
	private String creatUserId;
	//购买人
	private String orderUser;
	//添加时间
	private Timestamp creatTime;
	//挽联书写人员id
	private String writeUserId;
	//书写人员
	private User writeUser;
	//布置人员id
	private String assignUserId;
	//收款标志
	private byte payFlag;
	//付款人
	private String payName;
	//实收金额
	private double actualAmount;
	//实际收入帐金额
	private double actuallyPaid;
	//找零
	private double giveChange;
	//收款时间
	private Timestamp payTime;	
	//付款方式
	private byte payType;
	//火化委托单
	private CommissionOrder commissionOrder;
	//待办人
	private String agentUser;
	//是否购买标志
	private byte unitBuyFlag;
	//备注
	private String comment;
	//计数
	private String sum;
	//花圈花篮记录
	private List<BuyWreathRecordD> buyWreathRecordD;
	//委托单花圈花篮
	private ItemOrder itemOrder;
	
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public List<BuyWreathRecordD> getBuyWreathRecordD() {
		return buyWreathRecordD;
	}
	public void setBuyWreathRecordD(List<BuyWreathRecordD> buyWreathRecordD) {
		this.buyWreathRecordD = buyWreathRecordD;
	}
	public double getActuallyPaid() {
		return actuallyPaid;
	}
	public void setActuallyPaid(double actuallyPaid) {
		this.actuallyPaid = actuallyPaid;
	}
	public String getPayTypeName(){
		return Const.getPayType(payType);
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public byte getPayType() {
		return payType;
	}
	public void setPayType(byte payType) {
		this.payType = payType;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public double getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(double actualAmount) {
		this.actualAmount = actualAmount;
	}
	public double getGiveChange() {
		return giveChange;
	}
	public void setGiveChange(double giveChange) {
		this.giveChange = giveChange;
	}
	public User getWriteUser() {
		return writeUser;
	}
	public void setWriteUser(User writeUser) {
		this.writeUser = writeUser;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getCreatUserId() {
		return creatUserId;
	}
	public void setCreatUserId(String creatUserId) {
		this.creatUserId = creatUserId;
	}
	public String getOrderUser() {
		return orderUser;
	}
	public void setOrderUser(String orderUser) {
		this.orderUser = orderUser;
	}
	public Timestamp getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}
	public String getWriteUserId() {
		return writeUserId;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public void setWriteUserId(String writeUserId) {
		this.writeUserId = writeUserId;
	}
	public String getAssignUserId() {
		return assignUserId;
	}
	public void setAssignUserId(String assignUserId) {
		this.assignUserId = assignUserId;
	}
	public byte getPayFlag() {
		return payFlag;
	}
	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}	
	public String getAgentUser() {
		return agentUser;
	}
	public void setAgentUser(String agentUser) {
		this.agentUser = agentUser;
	}
	/**
	 * 获取是否单位购买名称
	 * @return
	 */
	public String getUnitBuyFlagName() {
		return Const.getIs(this.unitBuyFlag);
	}
	public void setUnitBuyFlag(byte unitBuyFlag) {
		this.unitBuyFlag = unitBuyFlag;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public byte getUnitBuyFlag() {
		return unitBuyFlag;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	
	public ItemOrder getItemOrder() {
		return itemOrder;
	}
	public void setItemOrder(ItemOrder itemOrder) {
		this.itemOrder = itemOrder;
	}
	/**
	 * 显示 是否收费
	 * @return
	 */
	public String getIfPay() {
		return Const.getIfPay(this.payFlag);
	}
	
	
	
}
