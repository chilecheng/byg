package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.base.Appellation;
import com.hz.util.Const;

/**
 * 困难减免申请
 * @author rgy
 *
 */
public class HardReduction {
	//困难减免申请id
	private String id;
	//业务单id
	private String commissionOrderId;
	//业务单
	private CommissionOrder commissionOrder;
	//出具证明id
	private String proveIds;
	//出具证明单位
	private String proveComment;
	//申请原因
	private String reason;
	//备注
	private String comment;
	//审核状态
	private byte checkFlag;
	//审核人
	private String checkUserId;
	//审核时间
	private Timestamp checkTime;
	//申请时间
	private Timestamp createTime;
	//经办人
	private String agentUserId;
	/**
	 * 与死者关系
	 */
	private Appellation appellation;
	/**
	 * 与死者关系
	 */
	private String appellationId;
	/**
	 * 申请人姓名
	 * @return
	 */
	private String applicant;
	/**
	 * 联系电话
	 * @return
	 */
	private String phone;
	/**
	 * 是否特殊减免
	 * @return
	 */
	private byte special;
	//备注
	private String remark;
	
	/**
	 * 困难减免 D
	 */
	private HardReductionD hardReductionD;
	
	
	public HardReductionD getHardReductionD() {
		return hardReductionD;
	}
	public void setHardReductionD(HardReductionD hardReductionD) {
		this.hardReductionD = hardReductionD;
	}
	public byte getSpecial() {
		return special;
	}
	public void setSpecial(byte special) {
		this.special = special;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Appellation getAppellation() {
		return appellation;
	}
	public void setAppellation(Appellation appellation) {
		this.appellation = appellation;
	}
	public String getAppellationId() {
		return appellationId;
	}
	public void setAppellationId(String appellationId) {
		this.appellationId = appellationId;
	}
	public String getApplicant() {
		return applicant;
	}
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public String getProveIds() {
		return proveIds;
	}
	public void setProveIds(String proveIds) {
		this.proveIds = proveIds;
	}
	public String getProveComment() {
		return proveComment;
	}
	public void setProveComment(String proveComment) {
		this.proveComment = proveComment;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public byte getCheckFlag() {
		return checkFlag;
	}
	public void setCheckFlag(byte checkFlag) {
		this.checkFlag = checkFlag;
	}
	public String getCheckUserId() {
		return checkUserId;
	}
	public void setCheckUserId(String checkUserId) {
		this.checkUserId = checkUserId;
	}	
	public Timestamp getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(Timestamp checkTime) {
		this.checkTime = checkTime;
	}	
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getAgentUserId() {
		return agentUserId;
	}
	public void setAgentUserId(String agentUserId) {
		this.agentUserId = agentUserId;
	}
	/**
	 * 获取是否审核名称
	 * @return
	 */
	public String getCheckFlagName() {
		return Const.getCheckFlag(this.checkFlag);
	}
}
