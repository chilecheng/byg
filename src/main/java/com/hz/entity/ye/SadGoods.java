package com.hz.entity.ye;

public class SadGoods {
	private String id;
	
	private String name;
	
	private Double pice;
	
	private String item_id;

	public String getItem_id() {
		return item_id;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPice() {
		return pice;
	}

	public void setPice(Double pice) {
		this.pice = pice;
	}

}
