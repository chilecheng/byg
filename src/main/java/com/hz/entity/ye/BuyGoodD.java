package com.hz.entity.ye;

import com.hz.entity.base.Item;

/**
  * 丧葬用品记录表
  * @author Administrator
  *
  */

public class BuyGoodD {
	private String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	//购买物品醒目ID
	private String itemId;
	//购买物品同一 类型的 数目
	private int number;	
	//ba_item  对应项
	public Item item;


	
	
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
}
