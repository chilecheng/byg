package com.hz.entity.ye;

import java.sql.Timestamp;
import java.util.List;

import com.hz.util.Const;

/**
 * 告别厅任务单
 * @author cjb
 *
 */
public class FarewellTaskList {
	private String id;
	/**
	 * 告别厅号
	 */
	private String fname;
	/**
	 * 告别时间
	 */
	private Timestamp beginDate;
	/**
	 * 委托单id
	 */
	private String cid;
	/**
	 * 姓名
	 */
	private String cname;
	/**
	 * 性别
	 */
	private byte sex;
	/**
	 * 年龄
	 */
	private int age;
	/**
	 * 灵堂号
	 */
	private String mname;
	/**
	 * 状态
	 */
	private byte dealIsdel;
	/**
	 * 服务项目收费
	 */
	private List<ItemOrder> listItemOrder;
	
	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getSexName() {
		return Const.getSex(this.sex);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public Timestamp getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Timestamp beginDate) {
		this.beginDate = beginDate;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public List<ItemOrder> getListItemOrder() {
		return listItemOrder;
	}

	public void setListItemOrder(List<ItemOrder> listItemOrder) {
		this.listItemOrder = listItemOrder;
	}

	public byte getDealIsdel() {
		return dealIsdel;
	}

	public void setDealIsdel(byte dealIsdel) {
		this.dealIsdel = dealIsdel;
	}
	
}
