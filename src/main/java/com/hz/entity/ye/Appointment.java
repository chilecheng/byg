package com.hz.entity.ye;

import java.sql.Timestamp;
/**
 * 预约登记
 * @author  jgj
 *
 */
public class Appointment {
	//id
	private String id;
	//流水号
	private String serialNumber;
	//登记姓名
	private String name;
	//联系电话
	private String phone;
	//告别厅号
	private String farewellCode;
	//告别厅时间
	private Timestamp farewellTime;
	//火化炉号
	private String funeralCode;
	//火化时间
	private Timestamp funeralTime;
	//灵堂号
	private String mourningCode;
	//守灵开始时间
	private Timestamp mourningBeginTime;
	//守灵结束时间
	private Timestamp mourningEndTime;
	//登记时间
	private Timestamp createTime;
	//预约登记 操作人
	private String createName;
	//告别厅记录ID
	private String farewellId;
	//火化炉记录ID
	private String funeralId;
	//灵堂记录ID
	private String mourningId;
	//接尸地址
	private String pickAddress;
	//出车时间
	private Timestamp departureTime;
	//车辆类型
	private String carType;
	//运输类型
	private String transportType;
	//是否出车预约
	private byte isCar;
	//是否预约登记
	private byte isAppointment;
	
	public byte getIsAppointment() {
		return isAppointment;
	}
	public void setIsAppointment(byte isAppointment) {
		this.isAppointment = isAppointment;
	}
	public String getPickAddress() {
		return pickAddress;
	}
	public void setPickAddress(String pickAddress) {
		this.pickAddress = pickAddress;
	}
	public Timestamp getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Timestamp departureTime) {
		this.departureTime = departureTime;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public String getTransportType() {
		return transportType;
	}
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}
	public byte getIsCar() {
		return isCar;
	}
	public void setIsCar(byte isCar) {
		this.isCar = isCar;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFarewellCode() {
		return farewellCode;
	}
	public void setFarewellCode(String farewellCode) {
		this.farewellCode = farewellCode;
	}
	public Timestamp getFarewellTime() {
		return farewellTime;
	}
	public void setFarewellTime(Timestamp farewellTime) {
		this.farewellTime = farewellTime;
	}
	public String getFuneralCode() {
		return funeralCode;
	}
	public void setFuneralCode(String funeralCode) {
		this.funeralCode = funeralCode;
	}
	public Timestamp getFuneralTime() {
		return funeralTime;
	}
	public void setFuneralTime(Timestamp funeralTime) {
		this.funeralTime = funeralTime;
	}
	public String getMourningCode() {
		return mourningCode;
	}
	public void setMourningCode(String mourningCode) {
		this.mourningCode = mourningCode;
	}
	public Timestamp getMourningBeginTime() {
		return mourningBeginTime;
	}
	public void setMourningBeginTime(Timestamp mourningBeginTime) {
		this.mourningBeginTime = mourningBeginTime;
	}
	public Timestamp getMourningEndTime() {
		return mourningEndTime;
	}
	public void setMourningEndTime(Timestamp mourningEndTime) {
		this.mourningEndTime = mourningEndTime;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public String getFarewellId() {
		return farewellId;
	}
	public void setFarewellId(String farewellId) {
		this.farewellId = farewellId;
	}
	public String getFuneralId() {
		return funeralId;
	}
	public void setFuneralId(String funeralId) {
		this.funeralId = funeralId;
	}
	public String getMourningId() {
		return mourningId;
	}
	public void setMourningId(String mourningId) {
		this.mourningId = mourningId;
	}
	
}
