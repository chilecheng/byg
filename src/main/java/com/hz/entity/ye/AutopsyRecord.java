package com.hz.entity.ye;

import java.sql.Date;
import java.sql.Timestamp;

import com.hz.entity.base.DeadReason;
import com.hz.entity.base.DeadType;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.ProveUnit;
import com.hz.entity.system.User;

/**
 * 法政验尸
 * @author non
 *
 */
	
public class AutopsyRecord {
	//id
	private String id;
	//流水单号
	private String code;
	//火化委托单号
	private String commissionOrderId;
	//验尸单位id
	private String proveUnitId;
	//验尸时间
	private Timestamp yTime;
	//备注
	private String comment;
	//标记（已验尸，未验尸，延时）
	private byte flag;
	//协助人员id
	private String helpUserId;
	private String helpUserNames;
	//经办人id
	private String agentUserId;
	//登记人Id 
	private String registerUserId;
	//登记时间
	private Timestamp registerTime;
	
	private CommissionOrder commissionOrder;
	private FreezerRecord freezerRecord;
	private ProveUnit proveUnit;
	private Freezer freezer;
	private DeadReason deadReason;
	private DeadType deadType;
	private User user;
	//解冻时间
	private Timestamp autoFreezerTime;
	//冰柜号
	private String freezerName;
	//流水单号
	private String serialNumber;
	
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getFreezerName() {
		return freezerName;
	}
	public void setFreezerName(String freezerName) {
		this.freezerName = freezerName;
	}
	public Timestamp getAutoFreezerTime() {
		return autoFreezerTime;
	}
	public void setAutoFreezerTime(Timestamp autoFreezerTime) {
		this.autoFreezerTime = autoFreezerTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public String getProveUnitId() {
		return proveUnitId;
	}
	public void setProveUnitId(String proveUnitId) {
		this.proveUnitId = proveUnitId;
	}
	
	public Timestamp getyTime() {
		return yTime;
	}
	public void setyTime(Timestamp yTime) {
		this.yTime = yTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public String getHelpUserId() {
		return helpUserId;
	}
	public void setHelpUserId(String helpUserId) {
		this.helpUserId = helpUserId;
	}
	public String getAgentUserId() {
		return agentUserId;
	}
	public void setAgentUserId(String agentUserId) {
		this.agentUserId = agentUserId;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public FreezerRecord getFreezerRecord() {
		return freezerRecord;
	}
	public void setFreezerRecord(FreezerRecord freezerRecord) {
		this.freezerRecord = freezerRecord;
	}
	public ProveUnit getProveUnit() {
		return proveUnit;
	}
	public void setProveUnit(ProveUnit proveUnit) {
		this.proveUnit = proveUnit;
	}
	public Timestamp getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Timestamp registerTime) {
		this.registerTime = registerTime;
	}
	public Freezer getFreezer() {
		return freezer;
	}
	public void setFreezer(Freezer freezer) {
		this.freezer = freezer;
	}
	public DeadReason getDeadReason() {
		return deadReason;
	}
	public void setDeadReason(DeadReason deadReason) {
		this.deadReason = deadReason;
	}
	public DeadType getDeadType() {
		return deadType;
	}
	public void setDeadType(DeadType deadType) {
		this.deadType = deadType;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getRegisterUserId() {
		return registerUserId;
	}
	public void setRegisterUserId(String registerUserId) {
		this.registerUserId = registerUserId;
	}
	public String getHelpUserNames() {
		return helpUserNames;
	}
	public void setHelpUserNames(String helpUserNames) {
		this.helpUserNames = helpUserNames;
	}
	
	
	
}
