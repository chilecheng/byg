package com.hz.entity.ye;

import java.sql.Timestamp;
import com.hz.entity.base.Item;

/**
 * 困难减免项目
 * @author rgy
 *
 */
public class HardReductionD {
	//困难减免项目id
	private String id;
	//困难减免申请id
	private String hardReductionId;
	//困难减免申请
	private HardReduction hardReduction;
	//服务项目id
	private String itemId;
	//数量
	private int number;
	//合计
	private double total;
	//备注
	private String comment;
	
	//item项
	private Item item;
	/**
	 * 困难减免扣除时间/计入时间
	 * 收费系统更改之后新加
	 * 将困难减免扣除时间与收费记录时间相关联，避免多次计算
	 */
	private Timestamp payTime;
	//订单流水号
	private String orderNumber;
	
	
	/**
	 * 收费科 需求项
	 * f分别为  项目的名称
	 * 单价
	 * 申请创建时间
	 */
	private String  name;
	private double pice;
	private Timestamp creatTime;
	private String  helpCode;
	private byte  sort;
	
	
	
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public String getHelpCode() {
		return helpCode;
	}
	public void setHelpCode(String helpCode) {
		this.helpCode = helpCode;
	}
	public byte getSort() {
		return sort;
	}
	public void setSort(byte sort) {
		this.sort = sort;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPice() {
		return pice;
	}
	public void setPice(double pice) {
		this.pice = pice;
	}
	public Timestamp getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}


	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public HardReduction getHardReduction() {
		return hardReduction;
	}
	public void setHardReduction(HardReduction hardReduction) {
		this.hardReduction = hardReduction;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHardReductionId() {
		return hardReductionId;
	}
	public void setHardReductionId(String hardReductionId) {
		this.hardReductionId = hardReductionId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public HardReductionD(){
		
	}
	public HardReductionD(String id, String hardReductionId, String itemId, int number, double total) {
		super();
		this.id = id;
		this.hardReductionId = hardReductionId;
		this.itemId = itemId;
		this.number = number;
		this.total = total;
	}
	
	
}
