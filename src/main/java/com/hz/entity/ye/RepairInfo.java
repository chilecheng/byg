package com.hz.entity.ye;

import java.sql.Timestamp;

public class RepairInfo {
	private String id;
	/**
	 * 维修目标ID
	 */
	private String itemId;
	/**
	 * 开始时间
	 */
	private Timestamp beginTime;
	/**
	 * 结束时间
	 */
	private Timestamp endTime;
	/**
	 * 报修日期
	 */
	private Timestamp createTime;
	/**
	 * 报修人员ID
	 */
	private String createUserId;
	/**
	 * 维修原因
	 */
	private String comment;
	/**
	 * 维修费用
	 */
	private double repairFee;
	/**
	 * 内部检测人
	 */
	private String repairName;
	/**
	 * 状态:4 表示维修
	 */
	private byte flag;
	
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public String getRepairName() {
		return repairName;
	}
	public void setRepairName(String repairName) {
		this.repairName = repairName;
	}
	public Timestamp getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public double getRepairFee() {
		return repairFee;
	}
	public void setRepairFee(double repairFee) {
		this.repairFee = repairFee;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
}

