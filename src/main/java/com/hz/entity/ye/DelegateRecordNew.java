package com.hz.entity.ye;


import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 委托业务缴费记录
 * 
 * @author jgj
 */
public class DelegateRecordNew {
	//火化委托单Id
	private String orderId;
	//火化流水号
	private String code;
	//死者姓名
	private String name;
	//年龄
	private String age;
	//性别
	private byte sex;
	//订单流水号
	private String orderNumber;
	//收费流水号
	private String payNumber;
	//收费状态
	private byte payFlag;
	//死亡证明状态
	private byte dFlag;
	//总价
	private double sumTotal;
	//基本减免价格
	private double baseTotal;
	//困难减免价格
	private double hardTotal;
	//收费时间
	private Timestamp payTime;
	
	public String getPayNumber() {
		return payNumber;
	}
	public void setPayNumber(String payNumber) {
		this.payNumber = payNumber;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public byte getPayFlag() {
		return payFlag;
	}
	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}
	public byte getdFlag() {
		return dFlag;
	}
	public void setdFlag(byte dFlag) {
		this.dFlag = dFlag;
	}
	public double getSumTotal() {
		return sumTotal;
	}
	public void setSumTotal(double sumTotal) {
		this.sumTotal = sumTotal;
	}
	public double getBaseTotal() {
		return baseTotal;
	}
	public void setBaseTotal(double baseTotal) {
		this.baseTotal = baseTotal;
	}
	public double getHardTotal() {
		return hardTotal;
	}
	public void setHardTotal(double hardTotal) {
		this.hardTotal = hardTotal;
	}
	/**
	 * 获取性别名称
	 * @return
	 */
	public String getSexName() {
		return Const.getSex(this.sex);
	}
}
