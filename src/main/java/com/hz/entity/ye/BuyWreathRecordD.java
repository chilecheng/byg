package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.base.Item;

/**
 * 花圈花篮订购项目
 * @author hw
 *
 */
public class BuyWreathRecordD {
	//id
	private String id;
	//花圈花篮订购记录id
	private String buyWreathRecordId;
	//单位或个人名称
	private String comment;
	//敬献者自称
	private String selfCall;
	//对死者的称呼
	private String deadCall;
	//服务项目Id
	private String itemId;
	//数量
	private int number;
	//合计
	private double total;
	//服务项目
	private Item item;
	
	//服务名称
	private String name;
	//服务单价
	private double pice;
	//创建时间
	private Timestamp creatTime;
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPice() {
		return pice;
	}
	public void setPice(double pice) {
		this.pice = pice;
	}
	public Timestamp getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBuyWreathRecordId() {
		return buyWreathRecordId;
	}
	public void setBuyWreathRecordId(String buyWreathRecordId) {
		this.buyWreathRecordId = buyWreathRecordId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getSelfCall() {
		return selfCall;
	}
	public void setSelfCall(String selfCall) {
		this.selfCall = selfCall;
	}
	public String getDeadCall() {
		return deadCall;
	}
	public void setDeadCall(String deadCall) {
		this.deadCall = deadCall;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
}
