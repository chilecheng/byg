package com.hz.entity.ye;

import java.sql.Date;
import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 骨灰寄存缴费，续费记录
 * @author rgy
 *
 */
public class AshesRecordD {
	//id
	private String id;
	//寄存记录id
	private String recordId;
	//单据编号
	private String code;
	//开始日期
	private Date beginDate;
	//结束日期
	private Date endDate;
	//续时长
	private int longTime;
	//交款日期
	private Timestamp payTime;
	//交款标志
	private byte payFlag;
	//金额
	private double total;
	
	//以下字段使用于收费科非委托业务
	//创建时间
	private Timestamp creatTime;
	//数量
	private int number;
	//备注
	private String comment;
	//名称
	private String name;
	//单价
	private double pice;
	//付款人
	private String payName;
	//实收金额
	private double actualAmount;
	//找零
	private double giveChange;
	//付款方式
	private byte payType;
	//实际入帐金额
	private double actuallyPaid;
	
	
	
	public double getActuallyPaid() {
		return actuallyPaid;
	}

	public void setActuallyPaid(double actuallyPaid) {
		this.actuallyPaid = actuallyPaid;
	}

	public String getPayTypeName(){
		return Const.getPayType(payType);
	}
	
	public byte getPayType() {
		return payType;
	}
	public void setPayType(byte payType) {
		this.payType = payType;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public double getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(double actualAmount) {
		this.actualAmount = actualAmount;
	}
	public double getGiveChange() {
		return giveChange;
	}
	public void setGiveChange(double giveChange) {
		this.giveChange = giveChange;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPice() {
		return pice;
	}
	public void setPice(double pice) {
		this.pice = pice;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Timestamp getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getLongTime() {
		return longTime;
	}
	public void setLongTime(int longTime) {
		this.longTime = longTime;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public byte getPayFlag() {
		return payFlag;
	}
	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	
}
