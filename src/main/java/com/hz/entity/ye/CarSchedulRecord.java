package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.entity.base.Car;
import com.hz.entity.base.CarType;
import com.hz.entity.base.TransportType;
/**
 * 车辆调度记录
 * @author tsp
 *
 */
public class CarSchedulRecord {
	/**
	 * id
	 */
	private String id;
	/**
	 * 运输类型ID
	 */
	private String transportTypeId;
	/**
	 * 车辆ID
	 */
	private String carId;
	/**
	 * 驾驶员ID
	 */
	private String driverId;
	/**
	 * 殡仪人员ID
	 */
	private String funeralId;
	/**
	 * 运输类型
	 */
	private TransportType transportType;
	/**
	 * 车辆类型Id
	 */
	private String carTypeId;
	/**
	 * 车辆类型
	 */
	private CarType carType;
	/**
	 * 接运时间
	 */
	private Timestamp pickTime;
	/**
	 * 出发时间
	 */
	private Timestamp startTime;
	/**
	 * 返回时间
	 */
	private Timestamp returnTime;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 业务单Id
	 */
	private String commissionOrderId;
	/**
	 * 业务单
	 */
	private CommissionOrder commissionOrder;
	/**
	 * 状态(1:接运完成 2：未派车 3:已派车)
	 */
	private byte flag;
	/**
	 * 登记人员
	 */
	private String enterId;
	/**
	 * 流水单号
	 */
	private String flowNumber;
	/**
	 * 登记时间
	 */
	private Timestamp regTime;
	/**
	 * 所派车辆
	 */
	private Car car;
	/**
	 * 首页 车辆列表查看次数
	 */
	private int viewsCar;
	
	
	
	public int getViewsCar() {
		return viewsCar;
	}
	public void setViewsCar(int viewsCar) {
		this.viewsCar = viewsCar;
	}
	public String getFlowNumber() {
		return flowNumber;
	}
	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}
	public String getEnterId() {
		return enterId;
	}
	public void setEnterId(String enterId) {
		this.enterId = enterId;
	}
	public Timestamp getRegTime() {
		return regTime;
	}
	public void setRegTime(Timestamp regTime) {
		this.regTime = regTime;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getReturnTime() {
		return returnTime;
	}
	public void setReturnTime(Timestamp returnTime) {
		this.returnTime = returnTime;
	}
	public String getFuneralId() {
		return funeralId;
	}
	public void setFuneralId(String funeralId) {
		this.funeralId = funeralId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getTransportTypeId() {
		return transportTypeId;
	}
	public void setTransportTypeId(String transportTypeId) {
		this.transportTypeId = transportTypeId;
	}
	public TransportType getTransportType() {
		return transportType;
	}
	public void setTransportType(TransportType transportType) {
		this.transportType = transportType;
	}
	public String getCarTypeId() {
		return carTypeId;
	}
	public void setCarTypeId(String carTypeId) {
		this.carTypeId = carTypeId;
	}
	public CarType getCarType() {
		return carType;
	}
	public void setCarType(CarType carType) {
		this.carType = carType;
	}
	public Timestamp getPickTime() {
		return pickTime;
	}
	public void setPickTime(Timestamp pickTime) {
		this.pickTime = pickTime;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public CommissionOrder getCommissionOrder() {
		return commissionOrder;
	}
	public void setCommissionOrder(CommissionOrder commissionOrder) {
		this.commissionOrder = commissionOrder;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	
}
