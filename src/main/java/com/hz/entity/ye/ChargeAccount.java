package com.hz.entity.ye;

/**
 * 挂帐业务
 * @author jgj
 *
 */
public class ChargeAccount {
	//ID
	private String id;
	//死者姓名
	private String name;
	//证件类型
	private String certificate_id;
	//证件号
	private String certificate_code;		
	//流水号
	private String serialNumber;
	//业务编号
	private String code;
	//证明单位ID
	private String prove_unit_id;
	//证明单位名称
	private String prove_unit_content;
	//挂帐标志
	private byte tick_flag;
	
	//服务项目类
	private ItemOrder itemOrder;

	
	
	
	
	public ChargeAccount() {
		super();
	}





	public ChargeAccount(String id, String name, String certificate_id, String certificate_code, String serialNumber,
			String code, String prove_unit_id, String prove_unit_content, byte tick_flag, ItemOrder itemOrder) {
		super();
		this.id = id;
		this.name = name;
		this.certificate_id = certificate_id;
		this.certificate_code = certificate_code;
		this.serialNumber = code+"3";
		this.code = code;
		this.prove_unit_id = prove_unit_id;
		this.prove_unit_content = prove_unit_content;
		this.tick_flag = tick_flag;
		this.itemOrder = itemOrder;
	}





	public String getId() {
		return id;
	}





	public void setId(String id) {
		this.id = id;
	}





	public String getName() {
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public String getCertificate_id() {
		return certificate_id;
	}





	public void setCertificate_id(String certificate_id) {
		this.certificate_id = certificate_id;
	}





	public String getCertificate_code() {
		return certificate_code;
	}





	public void setCertificate_code(String certificate_code) {
		this.certificate_code = certificate_code;
	}





	public String getSerialNumber() {
		return serialNumber;
	}





	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}





	public String getCode() {
		return code;
	}





	public void setCode(String code) {
		this.code = code;
	}





	public String getProve_unit_id() {
		return prove_unit_id;
	}





	public void setProve_unit_id(String prove_unit_id) {
		this.prove_unit_id = prove_unit_id;
	}





	public String getProve_unit_content() {
		return prove_unit_content;
	}





	public void setProve_unit_content(String prove_unit_content) {
		this.prove_unit_content = prove_unit_content;
	}





	public byte getTick_flag() {
		return tick_flag;
	}





	public void setTick_flag(byte tick_flag) {
		this.tick_flag = tick_flag;
	}





	public ItemOrder getItemOrder() {
		return itemOrder;
	}





	public void setItemOrder(ItemOrder itemOrder) {
		this.itemOrder = itemOrder;
	}

	
	
	
	
	
	
}