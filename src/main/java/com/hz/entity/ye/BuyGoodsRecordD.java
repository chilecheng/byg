package com.hz.entity.ye;

public class BuyGoodsRecordD {
	
	private String id;
	private String buyGoodsRecordId;
	private String itemId;
	private int number;
	private double total;
	private String remarks;
	private char iscredit;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBuyGoodsRecordId() {
		return buyGoodsRecordId;
	}
	public void setBuyGoodsRecordId(String buyGoodsRecordId) {
		this.buyGoodsRecordId = buyGoodsRecordId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public char getIscredit() {
		return iscredit;
	}
	public void setIscredit(char iscredit) {
		this.iscredit = iscredit;
	}
	
	
}
