package com.hz.entity.ye;

import java.sql.Date;
import java.sql.Timestamp;

import com.hz.entity.base.Appellation;
import com.hz.entity.base.AshesPosition;
import com.hz.util.Const;

/**
 * 骨灰寄存记录
 * @author rgy
 *
 */
public class AshesRecord {
	//id
	private String id;
	//火化单id
	private String commissionId;
	//寄存证编号
	private String code;
	//流水号
	private String serialNumber;	
	//火化人姓名
	private String dName;
	//性别
	private byte dSex;
	//年龄
	private int dAge;
	//火化日期
	private Date cremationTime;
	//骨灰盒样式
	private String ashesType;
	//骨灰盒位置id
	private String ashesPositionId;
	//骨灰盒位置
	private AshesPosition ashesPosition;
	//存放日期
	private Date beginDate;
	//到期日期
	private Date endDate;
	//寄存时长
	private int depositLong;
	//添加时间
	private Timestamp createTime;
	//添加人
	private String createUserId;
	//寄存人姓名
	private String sName;
	//寄存人地址
	private String sAddr;
	//寄存人电话
	private String sPhone;
	//与死者关系
	private String fAppellationId;
	//称谓关系
	private Appellation appellation;
	//状态
	private byte statusFlag;
	//领出时间
	private Date takeTime;
	//领出人
	private String takeName;
	//领出人联系电话
	private String takePhone;
	//备注
	private String comment;
	//领出操作员
	private String takeUserId;
	//死者编号
	private String dieCode;
	
	public String getTakePhone() {
		return takePhone;
	}
	public void setTakePhone(String takePhone) {
		this.takePhone = takePhone;
	}
	public String getDieCode() {
		return dieCode;
	}
	public void setDieCode(String dieCode) {
		this.dieCode = dieCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCommissionId() {
		return commissionId;
	}
	public void setCommissionId(String commissionId) {
		this.commissionId = commissionId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public byte getdSex() {
		return dSex;
	}
	public void setdSex(byte dSex) {
		this.dSex = dSex;
	}
	public int getdAge() {
		return dAge;
	}
	public void setdAge(int dAge) {
		this.dAge = dAge;
	}
	public Date getCremationTime() {
		return cremationTime;
	}
	public void setCremationTime(Date cremationTime) {
		this.cremationTime = cremationTime;
	}
	public String getAshesType() {
		return ashesType;
	}
	public void setAshesType(String ashesType) {
		this.ashesType = ashesType;
	}
	public String getAshesPositionId() {
		return ashesPositionId;
	}
	public void setAshesPositionId(String ashesPositionId) {
		this.ashesPositionId = ashesPositionId;
	}
	public AshesPosition getAshesPosition() {
		return ashesPosition;
	}
	public void setAshesPosition(AshesPosition ashesPosition) {
		this.ashesPosition = ashesPosition;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getDepositLong() {
		return depositLong;
	}
	public void setDepositLong(int depositLong) {
		this.depositLong = depositLong;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public String getsAddr() {
		return sAddr;
	}
	public void setsAddr(String sAddr) {
		this.sAddr = sAddr;
	}
	public String getsPhone() {
		return sPhone;
	}
	public void setsPhone(String sPhone) {
		this.sPhone = sPhone;
	}
	public String getfAppellationId() {
		return fAppellationId;
	}
	public void setfAppellationId(String fAppellationId) {
		this.fAppellationId = fAppellationId;
	}
	public Appellation getAppellation() {
		return appellation;
	}
	public void setAppellation(Appellation appellation) {
		this.appellation = appellation;
	}
	public byte getStatusFlag() {
		return statusFlag;
	}
	public void setStatusFlag(byte statusFlag) {
		this.statusFlag = statusFlag;
	}
	public Date getTakeTime() {
		return takeTime;
	}
	public void setTakeTime(Date takeTime) {
		this.takeTime = takeTime;
	}
	public String getTakeName() {
		return takeName;
	}
	public void setTakeName(String takeName) {
		this.takeName = takeName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getTakeUserId() {
		return takeUserId;
	}
	public void setTakeUserId(String takeUserId) {
		this.takeUserId = takeUserId;
	}
	/**
	 * 获取姓名名称
	 * @return
	 */
	public String getSexName(){
		return Const.getSex(this.dSex);	
	}
	/**
	 * 获取状态
	 * @return
	 */
	public String getAshesName(){
		return Const.getIsFlag(this.statusFlag);
		
	}
	
}
