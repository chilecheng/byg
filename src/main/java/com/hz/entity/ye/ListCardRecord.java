package com.hz.entity.ye;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 补卡页面数据列表
 * @author lipengpeng
 *
 */
public class ListCardRecord {
	
	private String id;
	
	private String name;
	
	private String cardCode;
	
	private byte sex;
	
	private int age;
	
	private Timestamp arriveTime;
	
	private String pickAddr;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Timestamp getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(Timestamp arriveTime) {
		this.arriveTime = arriveTime;
	}

	public String getPickAddr() {
		return pickAddr;
	}

	public void setPickAddr(String pickAddr) {
		this.pickAddr = pickAddr;
	}
	
	
	public String getSexName() {
		return Const.getSex(this.sex);
	}
	

}
