package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 挂账单位信息
 * @author jgj
 *
 */
public class BillUnit {
	//挂账单位ID
	private String id;
	//挂账单位名称
	private String name;
	//挂账单位顺序号
	private int indexNumber;
	//是否启用
	private byte isdel;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexNumber() {
		return indexNumber;
	}
	public void setIndexNumber(int indexNumber) {
		this.indexNumber = indexNumber;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
	
}
