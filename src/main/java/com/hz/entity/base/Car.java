package com.hz.entity.base;

import java.sql.Date;

import com.hz.util.Const;

/**
 * 车辆信息
 * @author tsp
 *
 */
public class Car {
	/**
	 * id
	 */
	private String id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 顺序号
	 */
	private int indexFlag;
	/**
	 * 启用标志
	 */
	private byte isdel;
	/**
	 * 车牌号
	 */
	private String carNumber;
	/**
	 * 服务价格
	 */
	private double servicePice;
	/**
	 * 车辆类型
	 */
	private CarType carType;
	/**
	 * 车辆类型
	 */
	private String typeId;
	/**
	 * 购买价格
	 */
	private double buyPice;
	/**
	 * 购买日期
	 */
	private Date buyDate;
	/**
	 * 初检日期
	 */
	private Date firstInspect;
	/**
	 * 复检日期
	 */
	private Date retestInspect;
	/**
	 * 驾驶员姓名
	 */
	private String driverName;
	/**
	 * 驾驶员电话
	 */
	private String driverPhone;
	/**
	 * 驾驶员地址
	 */
	private String driverAddr;
	/**
	 * 保险公司
	 */
	private String insurerCompany;
	/**
	 * 保险证号
	 */
	private String insurerCode;
	/**
	 * 保险证期限
	 */
	private Date insurerTime;
	/**
	 * 保险内容
	 */
	private String insurerContent;
	/**
	 * 经销商
	 */
	private String sellerName;
	/**
	 * 车内附属品
	 */
	private String additional;
	/**
	 * 经销商电话
	 */
	private String sellerPhone;
	/**
	 * 状态
	 */
	private byte flag;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getCarNumber() {
		return carNumber;
	}
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	public double getServicePice() {
		return servicePice;
	}
	public void setServicePice(double servicePice) {
		this.servicePice = servicePice;
	}
	public CarType getCarType() {
		return carType;
	}
	public void setCarType(CarType carType) {
		this.carType = carType;
	}
	public double getBuyPice() {
		return buyPice;
	}
	public void setBuyPice(double buyPice) {
		this.buyPice = buyPice;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public Date getFirstInspect() {
		return firstInspect;
	}
	public void setFirstInspect(Date firstInspect) {
		this.firstInspect = firstInspect;
	}
	public Date getRetestInspect() {
		return retestInspect;
	}
	public void setRetestInspect(Date retestInspect) {
		this.retestInspect = retestInspect;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverPhone() {
		return driverPhone;
	}
	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}
	public String getDriverAddr() {
		return driverAddr;
	}
	public void setDriverAddr(String driverAddr) {
		this.driverAddr = driverAddr;
	}
	public String getInsurerCompany() {
		return insurerCompany;
	}
	public void setInsurerCompany(String insurerCompany) {
		this.insurerCompany = insurerCompany;
	}
	public String getInsurerCode() {
		return insurerCode;
	}
	public void setInsurerCode(String insurerCode) {
		this.insurerCode = insurerCode;
	}
	public Date getInsurerTime() {
		return insurerTime;
	}
	public void setInsurerTime(Date insurerTime) {
		this.insurerTime = insurerTime;
	}
	public String getInsurerContent() {
		return insurerContent;
	}
	public void setInsurerContent(String insurerContent) {
		this.insurerContent = insurerContent;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getAdditional() {
		return additional;
	}
	public void setAdditional(String additional) {
		this.additional = additional;
	}
	public String getSellerPhone() {
		return sellerPhone;
	}
	public void setSellerPhone(String sellerPhone) {
		this.sellerPhone = sellerPhone;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	/**
	 * 获取是否启用名称
	 * @return
	 */
	public String getIsdelName() {
		return Const.getIsdel(this.isdel);
	}
	/**
	 * 获取是否占用名称
	 * @return
	 */
	public String getIsFlagName() {
		return Const.getIsFlag(this.flag);
	}
}
