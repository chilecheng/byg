package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 证件类型
 * @author RGY
 *
 */
public class Certificate {
	//证件id
	private String certificateId;
	//名称
	private String name;
	//顺序号
	private int indexFlag;
	//启用标志
	private byte isdel;
	//证件类型
	private byte type;
	
	public byte getType() {
		return type;
	}
	public void setType(byte type) {
		this.type = type;
	}
	public Certificate(){
		
	}
	public String getCertificateId() {
		return certificateId;
	}
	public void setCertificateId(String certificateId) {
		this.certificateId = certificateId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public Certificate(String certificateId, String name, int indexFlag, byte isdel) {
		super();
		this.certificateId = certificateId;
		this.name = name;
		this.indexFlag = indexFlag;
		this.isdel = isdel;
	}
	
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
	
}
