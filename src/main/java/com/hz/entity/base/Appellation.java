package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 称谓关系
 * @author rgy
 */
public class Appellation {
	//称谓关系id
	private String appellationId;
	//称谓名称
	private String name;
	//顺序号
	private int indexFlag;
	//启用标志
	private byte isdel;
	
	public String getAppellationId() {
		return appellationId;
	}
	public void setAppellationId(String appellationId) {
		this.appellationId = appellationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public Appellation(){
		
	}
	public Appellation(String appellationId, String name, int indexFlag, byte isdel) {
		super();
		this.appellationId = appellationId;
		this.name = name;
		this.indexFlag = indexFlag;
		this.isdel = isdel;
	}
	/**
	 * 是否启用标志
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
}
