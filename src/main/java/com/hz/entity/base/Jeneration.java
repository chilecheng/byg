package com.hz.entity.base;

import java.sql.Date;
/**
 * 编号管理
 * @author tsp
 *
 */
public class Jeneration {
	/**
	 * id
	 */
	private int id;
	/**
	 * 日期
	 */
	private Date time;
	/**
	 * 数量
	 */
	private int number;
	/**
	 * 类型
	 */
	private byte type;
	/**
	 * 火化炉
	 */
	public static final byte COR_TYPE = 1;
	/**
	 * 骨灰寄存
	 */
	public static final byte GHJC_TYPE = 2;
	/**
	 * 丧葬用品
	 */
	public static final byte SZYP_TYPE = 3;
	/**
	 * 花圈花篮
	 */
	public static final byte HQHL_TYPE = 4;
	/**
	 * 车辆调度
	 */
	public static final byte CLDD_TYPE = 5;
	/**
	 * 法证验尸
	 */
	public static final byte FZYS_TYPE =6;
	/**
	 * 预约登记
	 */
	public static final byte YYDJ_TYPE =7;
	/**
	 * 收费记录
	 */
	public static final byte SFJL_TYPE =8;
	/**
	 * 订单流水号
	 */
	public static final byte YWDD_TYPE =9;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public byte getType() {
		return type;
	}
	public void setType(byte type) {
		this.type = type;
	}
	

}
