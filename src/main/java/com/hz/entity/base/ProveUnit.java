package com.hz.entity.base;

import com.hz.util.Const;

public class ProveUnit{
	//单位证明id
	private String proveUnitId;
	//单位证明名称
	private String name;
	//单位证明顺序
	private int indexFlag;
	//证明类型
	private byte proveType;
	//启用标志
	private byte isdel;
	public ProveUnit(){
		
	}
	public String getProveUnitId() {
		return proveUnitId;
	}
	public void setProveUnitId(String proveUnitId) {
		this.proveUnitId = proveUnitId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getProveType() {
		return proveType;
	}
	public void setProveType(byte proveType) {
		this.proveType = proveType;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public ProveUnit(String proveUnitId, String name, int indexFlag, byte proveType, byte isdel) {
		super();
		this.proveUnitId = proveUnitId;
		this.name = name;
		this.indexFlag = indexFlag;
		this.proveType = proveType;
		this.isdel = isdel;
	}
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
	/**
	 * 获取证明单位类型名称
	 * @return
	 */
	public String getProveTypeName(){
		return Const.getProveType(this.proveType);
		
	}
}
