package com.hz.entity.base;

import com.hz.util.Const;
/**
 * 死亡原因
 * @author rgy
 */
public class DeadReason {
	//死亡原因id
	private String id;
	//死亡原因名称
	private String Name;
	//死亡原因顺序
	private int indexFlag;
	//死亡类型
	private String typeId;
	//启用标志
	private byte isdel;
	//死亡类型
	private DeadType deadType;
	
	public DeadType getDeadType() {
		return deadType;
	}
	public void setDeadType(DeadType deadType) {
		this.deadType = deadType;
	}
	public DeadReason(){
		
	}
	public DeadReason(String id, String name, int indexFlag, String typeId, byte isdel) {
		super();
		this.id = id;
		Name = name;
		this.indexFlag = indexFlag;
		this.typeId = typeId;
		this.isdel = isdel;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
}
