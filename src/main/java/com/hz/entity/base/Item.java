package com.hz.entity.base;

import com.hz.entity.system.User;
import com.hz.util.Const;
import com.hz.util.ItemConst;

/**
 * 收费项目
 * @author tsp
 *
 */
public class Item {
	/**
	 * id
	 */
	private String id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 顺序号
	 */
	private int indexFlag;
	/**
	 * 启用标志
	 */
	private byte isdel;
	/**
	 * 名称简拼
	 */
	private String nameJp;
	/**
	 * 布置人
	 */
	private User assign;
	/**
	 * 布置人
	 */
	private String assignId;
	/**
	 * 是否是基本减免项目
	 */
	private byte baseFlag;
	/**
	 * 编号
	 */
	private String code;
	/**
	 * 收费项目类别
	 */
	private String typeId;
	/**
	 * 收费项目类别
	 */
	private ItemType itemType;
	
	/**
	 * 所属部门
	 */
	private String dept;
	/**
	 * 价格
	 */
	private int pice;
	/**
	 * 计量单位
	 */
	private String measure;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 收费项目类型
	 */
	private byte type;
	/**
	 * 助记码
	 */
	private String helpCode;
	/**
	 * 是否是困难减免项目
	 */
	private byte hardFlag;
	/**
	 * 是否默认服务
	 */
	private byte defaultFlag;
	/**
	 * 详细分类
	 */
	private byte sort;
	/**
	 * 收费项目类型
	 */
	private TypeFees typeFees;
	/**
	 * 收费项目类型ID
	 */
	private String typeFeesId;
	
	public byte getSort() {
		return sort;
	}
	public void setSort(byte sort) {
		this.sort = sort;
	}
	public byte getDefaultFlag() {
		return defaultFlag;
	}
	public void setDefaultFlag(byte defaultFlag) {
		this.defaultFlag = defaultFlag;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getNameJp() {
		return nameJp;
	}
	public void setNameJp(String nameJp) {
		this.nameJp = nameJp;
	}
	public User getAssign() {
		return assign;
	}
	public void setAssign(User assign) {
		this.assign = assign;
	}
	public String getAssignId() {
		return assignId;
	}
	public void setAssignId(String assignId) {
		this.assignId = assignId;
	}
	public byte getBaseFlag() {
		return baseFlag;
	}
	public void setBaseFlag(byte baseFlag) {
		this.baseFlag = baseFlag;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public ItemType getItemType() {
		return itemType;
	}
	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public int getPice() {
		return pice;
	}
	public void setPice(int pice) {
		this.pice = pice;
	}
	public String getMeasure() {
		return measure;
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public byte getType() {
		return type;
	}
	public void setType(byte type) {
		this.type = type;
	}
	public String getHelpCode() {
		return helpCode;
	}
	public void setHelpCode(String helpCode) {
		this.helpCode = helpCode;
	}
	public byte getHardFlag() {
		return hardFlag;
	}
	public void setHardFlag(byte hardFlag) {
		this.hardFlag = hardFlag;
	}	
	
	public TypeFees getTypeFees() {
		return typeFees;
	}
	public void setTypeFees(TypeFees typeFees) {
		this.typeFees = typeFees;
	}	
	public String getTypeFeesId() {
		return typeFeesId;
	}
	public void setTypeFeesId(String typeFeesId) {
		this.typeFeesId = typeFeesId;
	}
	/**
	 * 获取是否启用名称
	 * @return
	 */
	public String getIsdelName() {
		return Const.getIsdel(this.isdel);
	}	
	/**
	 * 获取收费项目类型名称
	 * @return
	 */
	public String getTypeName() {
		return Const.getItemType(this.type);
	}
	/**
	 * 获取是否是困难减免项目名称
	 * @return
	 */
	public String getHardFlagName() {
		return Const.getIs(this.hardFlag);
	}
	/**
	 * 获取是否是基本减免项目名称
	 * @return
	 */
	public String getBaseFlagName() {
		return Const.getIs(this.baseFlag);
	}
	/**
	 * 获取是否默认服务名称
	 * @return
	 */
	public String getDefaultFlagName() {
		return Const.getIs(this.defaultFlag);
	}
	/**
	 * 详细分类名称
	 * @return
	 */
	public String getSortName() {
		return ItemConst.getSortName(sort);
	}
}
