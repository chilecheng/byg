package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 告别厅
 * @author rgy
 */
public class Farewell {
	//告别厅id
	private String id;
	//告别厅编号
	private String code;
	//告别厅名称
	private String name;
	//顺序号
	private int indexFlag;
	//启用标志
	private byte isdel;
	//费用
	private double pice;
	//告别厅类型
	private String typeId;
	//告别厅图片
	private String picPath;
	//状态
	private byte flag;
	
	private FarewellType farewellType;
	
	public FarewellType getFarewellType() {
		return farewellType;
	}

	public void setFarewellType(FarewellType farewellType) {
		this.farewellType = farewellType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndexFlag() {
		return indexFlag;
	}

	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}

	public byte getIsdel() {
		return isdel;
	}

	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}

	public double getPice() {
		return pice;
	}

	public void setPice(double pice) {
		this.pice = pice;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public byte getFlag() {
		return flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public Farewell(){
		
	}
	public Farewell(String id, String code, String name, int indexFlag, byte isdel, double pice, String typeId,
			String picPath, byte flag) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.indexFlag = indexFlag;
		this.isdel = isdel;
		this.pice = pice;
		this.typeId = typeId;
		this.picPath = picPath;
		this.flag = flag;
	}
	
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
	/**
	 * 是否占用
	 * @return
	 */
	public String getIsFlagName(){
		return Const.getIsFlag(this.flag);
	}
}
