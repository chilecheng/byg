package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 户籍
 * @author rgy
 *
 */
public class Register {
	
	
	//    添加项目  户籍 id  同 DB  数据相对应 
	private String id;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	//户籍id
	private String registerId;
	//户籍名称
	private String name;
	//父id
	private String fatherId;
	//顺序号
	private int indexFlag;
	//户籍类型
	private byte type;
	//启用标志
	private byte isdel;
	//户籍
	private Register register;
	
	public Register getRegister() {
		return register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}

	public Register(){
		
	}

	public Register(String registerId, String name, String fatherId, int indexFlag, byte type, byte isdel) {
		super();
		this.registerId = registerId;
		this.name = name;
		this.fatherId = fatherId;
		this.indexFlag = indexFlag;
		this.type = type;
		this.isdel = isdel;
	}

	public String getRegisterId() {
		return registerId;
	}

	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFatherId() {
		return fatherId;
	}

	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}

	public int getIndexFlag() {
		return indexFlag;
	}

	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public byte getIsdel() {
		return isdel;
	}

	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	/**
	 * 是否启动
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
	
}

