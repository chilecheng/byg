package com.hz.entity.base;


import java.sql.Date;
import java.sql.Timestamp;

import com.hz.entity.ye.CommissionOrder;
import com.hz.util.Const;

/**
 * 冷藏柜信息
 * @author tsp
 *
 */
public class Freezer {
	/**
	 * id
	 */
	private String id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 冷藏柜类型
	 */
	private FreezerType freezerType;
	/**
	 * 冷藏柜类型id
	 */
	private String typeId;
	/**
	 * 顺序号
	 */
	private int indexFlag;
	/**
	 * 启用标志
	 */
	private byte isdel;
	/**
	 * 购买日期
	 */
	private Date buyTime;
	/**
	 * 是否外送
	 */
	private byte outFlag;
	/**
	 * 是否分体
	 */
	private byte splitFlag;
	/**
	 * 编号
	 */
	private String code;
	/**
	 * 状态
	 */
	private byte flag;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 费用
	 */
	private double pice;
	
	//开始时间
	private Timestamp beginDate;
	//操作人姓名
	private String userName;
	//死者姓名
	private String sName;
	
	// 委托单业务类
	 private CommissionOrder   order;
	
	
	
	public CommissionOrder getOrder() {
		return order;
	}
	public void setOrder(CommissionOrder order) {
		this.order = order;
	}
	public Timestamp getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Timestamp beginDate) {
		this.beginDate = beginDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public Date getBuyTime() {
		return buyTime;
	}
	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}
	public byte getOutFlag() {
		return outFlag;
	}
	public void setOutFlag(byte outFlag) {
		this.outFlag = outFlag;
	}
	public byte getSplitFlag() {
		return splitFlag;
	}
	public void setSplitFlag(byte splitFlag) {
		this.splitFlag = splitFlag;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public double getPice() {
		return pice;
	}
	public void setPice(double pice) {
		this.pice = pice;
	}
	public FreezerType getFreezerType() {
		return freezerType;
	}
	public void setFreezerType(FreezerType freezerType) {
		this.freezerType = freezerType;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	/**
	 * 获取是否启用名称
	 * @return
	 */
	public String getIsdelName() {
		return Const.getIsdel(this.isdel);
	}
	/**
	 * 获取是否占用名称
	 * @return
	 */
	public String getIsFlagName() {
		return Const.getIsFlag(this.flag);
	}
	/**
	 * 获取是否外送名称
	 * @return
	 */
	public String getOutFlagName() {
		return Const.getIs(this.outFlag);
	}
	/**
	 * 获取是否分体名称
	 * @return
	 */
	public String getSplitFlagName() {
		return Const.getIs(this.splitFlag);
	}
}
