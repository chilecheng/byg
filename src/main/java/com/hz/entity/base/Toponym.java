package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 地名
 * @author Administrator
 */
public class Toponym {
	//地名id
	private String toponymId;
	//名称
	private String name;
	//地名
//	private Toponym toponym;
	//父id
	private String fatherId;
	//顺序号
	private int indexFlag;
	//启用标志
	private byte isdel;
	//户籍
	private Register register;
	//火化量
	private int number;
	//是否基本减免
	private byte isBase;
	
	public Toponym(){
		
	}
	/*public Toponym getToponym() {
		return toponym;
	}
	public void setToponym(Toponym toponym) {
		this.toponym = toponym;
	}*/
	
	public String getToponymId() {
		return toponymId;
	}
	/**
	 * 是否基本减免
	 * @return
	 */
	public String getIsBaseName(){
		return Const.getIs(this.isBase);
	}
	public byte getIsBase() {
		return isBase;
	}

	public void setIsBase(byte isBase) {
		this.isBase = isBase;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setToponymId(String toponymId) {
		this.toponymId = toponymId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFatherId() {
		return fatherId;
	}
	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public Toponym(String toponymId, String name, String fatherId, int indexFlag, byte isdel) {
		super();
		this.toponymId = toponymId;
		this.name = name;
		this.fatherId = fatherId;
		this.indexFlag = indexFlag;
		this.isdel = isdel;
	}
	/**
	 * 是否禁用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
	public Register getRegister() {
		return register;
	}
	public void setRegister(Register register) {
		this.register = register;
	}
}
