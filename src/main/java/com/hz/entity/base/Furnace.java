package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 火化炉信息
 * @author tsp
 *
 */
public class Furnace {
	/**
	 * id
	 */
	private String id;
	/**
	 * 编号
	 */
	private String code;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 顺序号
	 */
	private int indexFlag;
	/**
	 * 启用标志
	 */
	private byte isdel;
	/**
	 * 描述
	 */
	private String des;
	/**
	 * 火化炉类型
	 */
	private FurnaceType furnaceType;
	/**
	 * 火化炉类型
	 */
	private String typeId;
	/**
	 * 时间安排
	 */
	private String timeline;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 油量
	 */
	private double fuel;
	/**
	 * 状态
	 */
	private byte flag;
	/**
	 * 是否半点
	 */
	private byte isHalf;
	
	public byte getIsHalf() {
		return isHalf;
	}
	public void setIsHalf(byte isHalf) {
		this.isHalf = isHalf;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public FurnaceType getFurnaceType() {
		return furnaceType;
	}
	public void setFurnaceType(FurnaceType furnaceType) {
		this.furnaceType = furnaceType;
	}
	public String getTimeline() {
		return timeline;
	}
	public void setTimeline(String timeline) {
		this.timeline = timeline;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public double getFuel() {
		return fuel;
	}
	public void setFuel(double fuel) {
		this.fuel = fuel;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	/**
	 * 获取是否启用名称
	 * @return
	 */
	public String getIsdelName() {
		return Const.getIsdel(this.isdel);
	}
	/**
	 * 获取是否占用名称
	 * @return
	 */
	public String getIsFlagName() {
		return Const.getIsFlag(this.flag);
	}
}
