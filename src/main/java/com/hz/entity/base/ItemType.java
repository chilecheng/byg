package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 收费项目类别
 * @author tsp
 *
 */
public class ItemType {
	/**
	 * id
	 */
	private String id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 顺序号
	 */
	private int indexFlag;
	/**
	 * 启用标志
	 */
	private byte isdel;
	/**
	 * 父类
	 */
	private String fatherId;
	/**
	 * 父类
	 */
	private ItemType father;
	/**
	 * 编号
	 */
	private String code;
	/**
	 * 是否进入财务统计
	 */
	private byte countFlag;
	/**
	 * 备注
	 */
	private String comment;
	/**
	 * 类型
	 */
	private byte type;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public byte getCountFlag() {
		return countFlag;
	}
	public void setCountFlag(byte countFlag) {
		this.countFlag = countFlag;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public byte getType() {
		return type;
	}
	public void setType(byte type) {
		this.type = type;
	}
	
	public String getFatherId() {
		return fatherId;
	}
	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}
	public ItemType getFather() {
		return father;
	}
	public void setFather(ItemType father) {
		this.father = father;
	}
	/**
	 * 获取是否启用名称
	 * @return
	 */
	public String getIsdelName() {
		return Const.getIsdel(this.isdel);
	}
	/**
	 * 获取收费项目类型名称
	 * @return
	 */
	public String getItemTypeName() {
		return Const.getItemType(this.type);
	}
	/**
	 * 获取是否进入财务统计名称
	 * @return
	 */
	public String getCountFlagName() {
		return Const.getIs(this.countFlag);
	}
}
