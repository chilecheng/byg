package com.hz.entity.base;

import com.hz.util.Const;

/**
 * ������Ϣ
 * @author rgy
 *
 */
public class Mourning {
	//������Ϣid
	private String id;
	//���ñ��
	private String code;
	//������Ϣ����
	private String name;
	//˳���
	private int indexFlag;
	//���ñ�־
	private byte isdel;
	//��������
	private String typeId;
	//
	private MourningType mourningType;
	//
	private String specId;
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getSpecId() {
		return specId;
	}
	public void setSpecId(String specId) {
		this.specId = specId;
	}
	//���ù��
	private MourningSpec mourningSpec;
	//����ͼƬ
	private String picPath;
	//״̬
	private byte flag;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public MourningType getMourningType() {
		return mourningType;
	}
	public void setMourningType(MourningType mourningType) {
		this.mourningType = mourningType;
	}
	public MourningSpec getMourningSpec() {
		return mourningSpec;
	}
	public void setMourningSpec(MourningSpec mourningSpec) {
		this.mourningSpec = mourningSpec;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getPicPath() {
		return picPath;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public Mourning(){
		
	}
	
	public Mourning(String id, String code, String name, int indexFlag, byte isdel, MourningType mourningType,
			MourningSpec mourningSpec, String picPath, byte flag) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.indexFlag = indexFlag;
		this.isdel = isdel;
		this.mourningType = mourningType;
		this.mourningSpec = mourningSpec;
		this.picPath = picPath;
		this.flag = flag;
	}
	/**
	 * �Ƿ����ñ�־
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
	/**
	 * ռ��״̬
	 * @return
	 */
	public String getIsFlagName(){
		return Const.getIsFlag(this.flag);
		
	}
	
}
