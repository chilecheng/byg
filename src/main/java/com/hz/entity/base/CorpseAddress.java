package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 接尸地址
 * @author jgj
 *
 */
public class CorpseAddress {
	//接尸地址id
	private String id;
	//接尸地址名称
	private String name;
	//顺序号
	private int indexFlag;
	//启用标志
	private byte isdel;
	
	
	public CorpseAddress(){
		
	}	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public CorpseAddress(String id, String name, int indexFlag, byte isdel) {
		super();
		this.id = id;
		this.name = name;
		this.indexFlag = indexFlag;
		this.isdel = isdel;
	}
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
}
