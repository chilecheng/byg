package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 运输类型
 * @author rgy
 *
 */
public class TransportType {
	//运输类型id
	private String transportId;
	//运输名称
	private String name;
	//顺序号
	private int indexFlag;
	//启用标志
	private byte isdel;
	public TransportType(){
		
	}
	public String getTransportId() {
		return transportId;
	}
	public void setTransportId(String transportId) {
		this.transportId = transportId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
}
