package com.hz.entity.base;

import com.hz.util.Const;

/**
 * 医院类
 * @author rgy
 *
 */
public class Hospital {
	//医院id
	private String hospitalId;
	//医院名称
	private String name;
	//医院顺序号
	private int indexFlag;
	//启用标志
	private byte isdel;
	public Hospital(){
		
	}
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public Hospital(String hospitalId, String name, int indexFlag, byte isdel) {
		super();
		this.hospitalId = hospitalId;
		this.name = name;
		this.indexFlag = indexFlag;
		this.isdel = isdel;
	}
	/**
	 * 启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
		
	}
}
