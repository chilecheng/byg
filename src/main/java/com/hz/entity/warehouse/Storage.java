package com.hz.entity.warehouse;

import java.sql.Timestamp;

/**
 * 入库单 实体类
 * @author 金国军
 *
 */
public class Storage {
	//入库单id
	private String id;
	//入库单号
	private String serialNumber;
	//入库时间
	private Timestamp intoTime;
	//仓库ID
	private String warehouseId;
	//供应商ID
	private String supplierId;
	//商品ID
	private String commodityId;
	//数量
	private int number;
	//操作员
	private String operator;
	//入库方式
	private byte intoWay;
	//供应商信息
	private Supplier supplier;
	//商品信息
	private Commodity commodity;
	//仓库信息
	private Warehouse warehouse;
	
	
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Commodity getCommodity() {
		return commodity;
	}
	public void setCommodity(Commodity commodity) {
		this.commodity = commodity;
	}
	public Warehouse getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Timestamp getIntoTime() {
		return intoTime;
	}
	public void setIntoTime(Timestamp intoTime) {
		this.intoTime = intoTime;
	}
	public String getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getCommodityId() {
		return commodityId;
	}
	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public byte getIntoWay() {
		return intoWay;
	}
	public void setIntoWay(byte intoWay) {
		this.intoWay = intoWay;
	}

	
}
