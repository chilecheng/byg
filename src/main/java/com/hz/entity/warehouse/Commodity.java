package com.hz.entity.warehouse;

import com.hz.entity.base.Item;

/**
 * 商品信息实体类
 * @author 金国军
 *
 */
public class Commodity {
	//商品信息id
	private String id;
	//商品代码
	private String commodityId;
	//在售商品ID
	private String itemId;
	//商品名称
	private String name;
	//供应商ID
	private String supplierId;
	//单价
	private double price;
	//是否有效可用
	private byte isDel;
	//商品名称等信息
	private Item item;
	//供应商信息
	private Supplier supplier;
	
	
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public String getCommodityId() {
		return commodityId;
	}
	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public byte getIsDel() {
		return isDel;
	}
	public void setIsDel(byte isDel) {
		this.isDel = isDel;
	}
	
}
