package com.hz.entity.warehouse;

import java.sql.Timestamp;

/**
 * 与供应商结算  帐单实体类
 * @author 金国军
 *
 */
public class Bill {
	//结算单id
	private String id;
	//结算单号
	private String serialNumber;
	//结算时间
	private Timestamp settlementTime;
	//供应商ID
	private String supplierId;
	//商品ID
	private String commodityId;
	//单价
	private double price;
	//回收
	private int recover;
	//售出
	private int sell;
	//损耗
	private int loss;
	//合计
	private double total;
	//操作员
	private String operator;
	//供应商信息
	private Supplier supplier;
	//商品信息
	private Commodity commodity;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Timestamp getSettlementTime() {
		return settlementTime;
	}
	public void setSettlementTime(Timestamp settlementTime) {
		this.settlementTime = settlementTime;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getCommodityId() {
		return commodityId;
	}
	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public int getRecover() {
		return recover;
	}
	public void setRecover(int recover) {
		this.recover = recover;
	}
	public int getSell() {
		return sell;
	}
	public void setSell(int sell) {
		this.sell = sell;
	}
	public int getLoss() {
		return loss;
	}
	public void setLoss(int loss) {
		this.loss = loss;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Commodity getCommodity() {
		return commodity;
	}
	public void setCommodity(Commodity commodity) {
		this.commodity = commodity;
	}
	
}
