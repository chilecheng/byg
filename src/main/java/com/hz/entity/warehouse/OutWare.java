package com.hz.entity.warehouse;

import java.sql.Timestamp;

/**
 * 出库单 实体类
 * @author 金国军
 *
 */
public class OutWare {
	//出库单id
	private String id;
	//出库单号
	private String serialNumber;
	//出库时间
	private Timestamp outTime;
	//仓库ID
	private String warehouseId;
	//商品ID
	private String commodityId;
	//数量
	private int number;
	//出库方式
	private byte outWay;
	//操作员
	private String operator;
	//商品信息
	private Commodity commodity;
	//仓库信息
	private Warehouse warehouse;
	
	
	public Commodity getCommodity() {
		return commodity;
	}
	public void setCommodity(Commodity commodity) {
		this.commodity = commodity;
	}
	public Warehouse getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Timestamp getOutTime() {
		return outTime;
	}
	public void setOutTime(Timestamp outTime) {
		this.outTime = outTime;
	}
	public String getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getCommodityId() {
		return commodityId;
	}
	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public byte getOutWay() {
		return outWay;
	}
	public void setOutWay(byte outWay) {
		this.outWay = outWay;
	}
	
	
	
	
}
