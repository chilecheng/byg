package com.hz.entity.warehouse;


/**
 * 仓库信息实体类
 * @author 金国军
 *
 */
public class Warehouse {
	//仓库id
	private String id;
	//仓库名称
	private String name;
	//备注信息
	private String comment;
	//是否启用
	private byte isDel;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public byte getIsDel() {
		return isDel;
	}
	public void setIsDel(byte isDel) {
		this.isDel = isDel;
	}
	
	
	
}
