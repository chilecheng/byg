package com.hz.entity.payDivision;

import java.sql.Timestamp;
/**
 * 挂账收费表实体类（已收费）
 * @author jgj
 *
 */
public class BillRecord {
	//挂账单位  收费ID
	private String id;
	//挂账单位ID
	private String billUnitId;
	//挂账收费金额
	private double amount;
	//挂账收费时间
	private Timestamp time;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBillUnitId() {
		return billUnitId;
	}
	public void setBillUnitId(String billUnitId) {
		this.billUnitId = billUnitId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	
	
}
