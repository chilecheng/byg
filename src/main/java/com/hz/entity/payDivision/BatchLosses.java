package com.hz.entity.payDivision;
/**
 * 批量挂账收费 
 * @author jgj
 *
 */
public class BatchLosses {
	//挂账单位名称
	private String billName;
	//挂账单位ID
	private String billId;
	//挂账单位总金额
	private double sumTotal;
	//挂账单位总记录数量
	private int sumNumber;
	
	
	
	public String getBillName() {
		return billName;
	}
	public void setBillName(String billName) {
		this.billName = billName;
	}
	public String getBillId() {
		return billId;
	}
	public void setBillId(String billId) {
		this.billId = billId;
	}
	public double getSumTotal() {
		return sumTotal;
	}
	public void setSumTotal(double sumTotal) {
		this.sumTotal = sumTotal;
	}
	public int getSumNumber() {
		return sumNumber;
	}
	public void setSumNumber(int sumNumber) {
		this.sumNumber = sumNumber;
	}
	
	
}
