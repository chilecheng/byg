package com.hz.entity.payDivision;

import java.sql.Timestamp;

import com.hz.util.Const;

/**
 * 委托业务收费
 * @author jgj
 *
 */
public class BusinessFees {
	//id
	private String id;
	//委托单id
	private String orderId;
	//收费流水号
	private String payNumber;
	//订单业务流水号
	private String orderNumber;
	//支付方式
	private byte payType;
	//付款时间
	private Timestamp payTime;
	//付款人
	private String payName;
	//付款金额
	private double payAmount;
	//操作员
	private String payeeId;
	//备注
	private String comment;
	//业务流水号（委托单流水号）
	private String serviceCode;
	//找零（更改后不使用）
	private double odd;
	//前期收费（更改后不使用）
	private double frontPay;
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPayNumber() {
		return payNumber;
	}
	public void setPayNumber(String payNumber) {
		this.payNumber = payNumber;
	}
	public byte getPayType() {
		return payType;
	}
	public void setPayType(byte payType) {
		this.payType = payType;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public double getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(double payAmount) {
		this.payAmount = payAmount;
	}
	public String getPayeeId() {
		return payeeId;
	}
	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public double getOdd() {
		return odd;
	}
	public void setOdd(double odd) {
		this.odd = odd;
	}
	public double getFrontPay() {
		return frontPay;
	}
	public void setFrontPay(double frontPay) {
		this.frontPay = frontPay;
	}
	public String getPayTypeName(){
		return Const.getPayType(this.payType);
	}
	
}
