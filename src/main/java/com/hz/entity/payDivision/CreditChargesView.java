package com.hz.entity.payDivision;

import java.sql.Timestamp;

import com.hz.entity.system.User;

/**
 * 挂账收费记录   用于列表显示
 * @author jgj
 *
 */
public class CreditChargesView {
	//挂账单位名称
	private String billUnitName;	
	//委托单id
	private String commissionOrderId;
	//流水号
	private String serialNumber;
	//业务编号
//	private String BusinessNumber;
	//死者姓名
	private String dName;
	//付款状态
	private byte payFlag;
	//付款时间
	private Timestamp payTime;
	
	
	
	
	public String getBillUnitName() {
		return billUnitName;
	}
	public void setBillUnitName(String billUnitName) {
		this.billUnitName = billUnitName;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public byte getPayFlag() {
		return payFlag;
	}
	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	
	
	
}
