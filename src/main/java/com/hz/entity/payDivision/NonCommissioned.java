package com.hz.entity.payDivision;

import com.hz.util.Const;

/**
 * 非委托业务收费
 * @author jgj
 *
 */
public class NonCommissioned {
	//id
	private String id;
	//购买单号
	private String buyCode;
	//流水号
	private String serialNumber;
	//死者编号
	private String code;
	//死者姓名
	private String name;
	//死者年龄
	private int age;
	//死者性别
	private byte sex;
	//付款人
	private String payName;
	//总金额
	private double total;
	//收费状态
	private byte payFlag;
	
	
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	/**
	 * 获取性别名称
	 * @return
	 */
	public String getSexName(){
		return Const.getSex(sex);
	}
	public String getPayFlagName(){
		return Const.getPayFlagType(payFlag);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBuyCode() {
		return buyCode;
	}
	public void setBuyCode(String buyCode) {
		this.buyCode = buyCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public byte getPayFlag() {
		return payFlag;
	}
	public void setPayFlag(byte payFlag) {
		this.payFlag = payFlag;
	}
	
}
