package com.hz.entity.payDivision;

import java.sql.Timestamp;

import com.hz.entity.system.User;

/**
 * 挂账收费记录 
 * @author jgj
 *
 */
public class CreditCharges {
	//挂账收费id
	private String id;
	//挂账单位id
	private String billUnitId;	
	//委托单id
	private String commissionOrderId;
	//付款人
	private String payName;
	//付款金额
	private double payMoney;
	//找零
	private double changeMoney;
	//付款时间
	private Timestamp payTime;
	//付款方式
	private byte payMethod;
	//备注
	private String comment;
	//操作员
	private String userId;	
	private User user;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBillUnitId() {
		return billUnitId;
	}
	public void setBillUnitId(String billUnitId) {
		this.billUnitId = billUnitId;
	}
	public String getCommissionOrderId() {
		return commissionOrderId;
	}
	public void setCommissionOrderId(String commissionOrderId) {
		this.commissionOrderId = commissionOrderId;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public double getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(double payMoney) {
		this.payMoney = payMoney;
	}
	public double getChangeMoney() {
		return changeMoney;
	}
	public void setChangeMoney(double changeMoney) {
		this.changeMoney = changeMoney;
	}
	public Timestamp getPayTime() {
		return payTime;
	}
	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}
	public byte getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(byte payMethod) {
		this.payMethod = payMethod;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
