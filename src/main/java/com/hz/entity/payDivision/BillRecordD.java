package com.hz.entity.payDivision;

import java.sql.Timestamp;
/**
 * 挂账收费的具体记录（已收费）
 * @author jgj
 *
 */
public class BillRecordD {
	//挂账收费记录ID
	private String id;
	//挂账收费ID
	private String billRecordId;
	//挂账项目ID
	private String itemId;
	//记录时间
	private Timestamp time;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBillRecordId() {
		return billRecordId;
	}
	public void setBillRecordId(String billRecordId) {
		this.billRecordId = billRecordId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	
	
}
