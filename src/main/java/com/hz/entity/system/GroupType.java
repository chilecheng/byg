package com.hz.entity.system;


/**用户分组类型
 * @author cjb
 * 
 */
public class GroupType {
	/**
	 * id
	 */
	private String id;
	/**
	 * 编号
	 */
	private byte type;
	/**
	 * 组名
	 */
	private String name;
	
	public GroupType() {
	}
	
	public GroupType(String id, byte type, String name) {
		this.id = id;
		this.type = type;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public byte getType() {
		return type;
	}
	
	public void setType(byte type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "GroupType [id=" + id + ", type=" + type + ", name=" + name + "]";
	}
	
	
	
}
