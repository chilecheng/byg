package com.hz.entity.system;
import java.sql.Date;
/**
 * 公开通告管理
 * @author rgy
 *
 */

public class Notification{
	//id
	private String id;
	//题目
	private String title;
	//日期
	private Date date;
	//内容
	private String content;
	//系统添加人
	private String createUserId;
	//管理员
	private User user;
	//部门id
	private String deptId;
	//部门
	private Dept dept;
	//添加时间
	private Date createTime;
	//浏览次数
	private int number;
	//附件保存地址
	private String saveAddress;
	//文件名
	private String fileName;
	
	
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSaveAddress() {
		return saveAddress;
	}

	public void setSaveAddress(String saveAddress) {
		this.saveAddress = saveAddress;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Notification(){
		
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public Dept getDept() {
		return dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
