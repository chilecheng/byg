package com.hz.entity.system;

import java.sql.Timestamp;
import java.util.List;

import com.hz.util.Const;

/**
 * 系统用户
 * @author ljx
 */
public class User {
	//id
	private String userId;
	//编号
	private String code;
	//登录名
	private String userName;
	//真实姓名
	private String name;
	//密码
	private String password;
	//手机号
	private String phone;
	//电话
	private String tel;
	//禁用标志
	private byte isdel;
	//性别
	private byte sex;
	//用户类型
//	private byte type;
	//角色
	private List<UserRole> roles;
	//部门
	private Dept dept;
	//部门编号
	private String deptId;
	//所属组编号
	private byte gtype;
	//分组
	private GroupType groupType;
	//创建时间
	private Timestamp createTime;
	
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public List<UserRole> getRoles() {
		return roles;
	}
	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String string) {
		this.userId = string;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	
	public Dept getDept() {
		return dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

	public GroupType getGroupType() {
		return groupType;
	}

	public void setGroupType(GroupType groupType) {
		this.groupType = groupType;
	}
	
	
	public byte getGtype() {
		return gtype;
	}

	
	public void setGtype(byte gtype) {
		this.gtype = gtype;
	}
	
	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	/**
	 * 性别显示
	 * @return
	 */
	public String getSexStr(){
		return Const.getSex(this.sex);
	}
	/**
	 * 是否启用
	 * @return
	 */
	public String getIsdelName(){
		return Const.getIsdel(this.isdel);
	}
	
	public String getRoleNames(){
		String name = "";
		for(int i=0;i<this.getRoles().size();i++){
			UserRole ur = (UserRole)this.getRoles().get(i);
			if(!name.equals("")){
				name+=",";
			}
			if (ur.getRole() != null) {
				name+=ur.getRole().getName();
			}
		}
		return name;
	}
	
	public String getGroupName(){
		GroupType groupType = this.getGroupType();
		if (groupType == null) {
			return null;
		} else {
			return groupType.getName();
		}
		
	}
	public String getDeptName() {
		Dept dept = this.getDept();
		if (dept == null) {
			return null;
		} else {
			return dept.getName();
		}
	}
}
