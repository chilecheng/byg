package com.hz.entity.system;

/**
 * 部门菜单
 * @author cjb
 */
public class DeptMenu {

	/**
	 * id
	 */
	private String id;
	/**
	 * 职位id
	 */
	private String deptId;
	/**
	 * 职位
	 */
	private Dept dept;
	/**
	 * 菜单id
	 */
	private String menuId;
	/**
	 * 菜单
	 */
	private Menu menu;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	
}
