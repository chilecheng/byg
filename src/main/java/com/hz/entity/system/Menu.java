package com.hz.entity.system;
/**
 * 菜单
 * @author tsp
 *
 */
public class Menu {
	/**
	 * Id
	 */
	private String id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 顺序号
	 */
	private int indexFlag;
	/**
	 * 菜单
	 */
	private Menu menu;
	/**
	 * 菜单Id
	 */
	private String menuId;
	/**
	 * 地址
	 */
	private String url;
	/**
	 * 权限
	 */
	private String powerStr;
	/**
	 * 图标
	 */
	private String icon;
	/**
	 * 职位菜单是否勾选(临时属性)
	 */
	private int roleMenuNum;
	private int deptMenuNum;
	
	public int getDeptMenuNum() {
		return deptMenuNum;
	}
	public void setDeptMenuNum(int deptMenuNum) {
		this.deptMenuNum = deptMenuNum;
	}
	public int getRoleMenuNum() {
		return roleMenuNum;
	}
	public void setRoleMenuNum(int roleMenuNum) {
		this.roleMenuNum = roleMenuNum;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPowerStr() {
		return powerStr;
	}
	public void setPowerStr(String powerStr) {
		this.powerStr = powerStr;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
}
