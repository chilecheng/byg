package com.hz.entity.system;

import com.hz.util.Const;

/**
 * 部门
 * @author tsp
 *
 */
public class Dept {
	/**
	 * Id
	 */
	private String id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 顺序号
	 */
	private int indexFlag;
	/**
	 * 部门
	 */
	private Dept dept;
	/**
	 * 部门Id
	 */
	private String fatherId;
	/**
	 * 
	 * 启用标志
	 */
	private byte isdel;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIndexFlag() {
		return indexFlag;
	}
	public void setIndexFlag(int indexFlag) {
		this.indexFlag = indexFlag;
	}
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	public String getFatherId() {
		return fatherId;
	}
	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	/**
	 * 获取是否启用名称
	 * @return
	 */
	public String getIsdelName() {
		return Const.getIsdel(this.isdel);
	}
}
