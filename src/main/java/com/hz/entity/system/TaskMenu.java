package com.hz.entity.system;

/**
 * 任务列表
 * @author cjb
 *
 */
public class TaskMenu {
	private String id;
	private String name;
	private String url;
	//查看次数
	private int newNumber;
	/**
	 * 职位菜单是否勾选(临时属性)
	 */
	private int deptMenuNum;
	
	
	
	public int getNewNumber() {
		return newNumber;
	}
	public void setNewNumber(int newNumber) {
		this.newNumber = newNumber;
	}
	public int getDeptMenuNum() {
		return deptMenuNum;
	}
	public void setDeptMenuNum(int deptMenuNum) {
		this.deptMenuNum = deptMenuNum;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
