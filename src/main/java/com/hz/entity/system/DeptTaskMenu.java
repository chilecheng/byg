package com.hz.entity.system;

/**
 * 部门菜单
 * @author cjb
 */
public class DeptTaskMenu {

	/**
	 * id
	 */
	private String id;
	/**
	 * 职位id
	 */
	private String deptId;
	/**
	 * 职位
	 */
	private Dept dept;
	/**
	 * 菜单id
	 */
	private String taskMenuId;
	/**
	 * 菜单
	 */
	private TaskMenu taskMenu;
	
	public TaskMenu getTaskMenu() {
		return taskMenu;
	}
	public void setTaskMenu(TaskMenu taskMenu) {
		this.taskMenu = taskMenu;
	}
	public String getTaskMenuId() {
		return taskMenuId;
	}
	public void setTaskMenuId(String taskMenuId) {
		this.taskMenuId = taskMenuId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	
}
