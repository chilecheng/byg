package com.hz.entity.system;

/**
 * 职位菜单
 * @author ljx
 */
public class RoleMenu {

	/**
	 * id
	 */
	private String id;
	/**
	 * 职位id
	 */
	private String roleId;
	/**
	 * 职位
	 */
	private Role role;
	/**
	 * 菜单id
	 */
	private String menuId;
	/**
	 * 菜单
	 */
	private Menu menu;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
}
