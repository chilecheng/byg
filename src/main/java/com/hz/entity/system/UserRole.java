package com.hz.entity.system;

/**
 * 用户职位
 * @author ljx
 *
 */
public class UserRole {

	/**
	 * id
	 */
	private String id;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 用户
	 */
	private User user;
	/**
	 * 职位id
	 */
	private String roleId;
	/**
	 * 职位
	 */
	private Role role;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
}
