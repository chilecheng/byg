package com.hz.socket;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;

import com.hz.entity.base.Item;
import com.hz.entity.socket.Notice;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.ItemOrder;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.service.ye.FreezerRecordService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.ItemConst;

@Service
@Transactional
public class NoticeHandleService {
	@Autowired
	private FreezerRecordService freezerRecordService;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	private DeptService deptService;
	@Autowired 
	private SystemWebSocketHandler systemWebSocketHandler;
	/**
	 * 处理一科消息
	 */
	public void handleFirstDepart(CommissionOrder commissionOrder, List<ItemOrder> list,int i,CommissionOrder oldCom,List<ItemOrder> list2) {
		String day2 = DateTools.getTimeString("yyyy-MM-dd");
    	String day1 = "";
    	//告别厅任务提示
		if (commissionOrder.getFarewellId() != null && !commissionOrder.getFarewellId().equals("")) {
			Timestamp farewellTime = commissionOrder.getFarewellTime();
			if (userNoticeService.ifDateExit(Const.NoticeType_FarewellTask,DateTools.getDate(farewellTime))) {
				if(i==-1){
					updateNoticeToday(farewellTime,Const.NoticeType_FarewellTask);
				}else{
				userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FarewellTask, DateTools.getDate(farewellTime), i);
				}
				
			} else if(!userNoticeService.ifDateExit(Const.NoticeType_FarewellTask,DateTools.getDate(farewellTime)) && i>0) {
				userNoticeService.addNoticeOfDate(Const.NoticeType_FarewellTask, DateTools.getDate(farewellTime));
			}
			//判断是否同天
			day1 = DateTools.getTimeFormatString("yyyy-MM-dd", farewellTime);
			if (oldCom != null && oldCom.getFarewellId() != null && !"".equals(oldCom.getFarewellId())) {//如果原先的告别时间和修改之后的告别时间都是今天的话就不会给用户发送提示，防止修改其他东西的时候提示无限加下去
				Timestamp oldFarewellTime = oldCom.getFarewellTime();
				if (!DateTools.getTimeFormatString("yyyy-MM-dd",oldFarewellTime).equals(day2) && day1.equals(day2) && i >0) {
					String deptId = deptService.getDeptIdByName("一科");
					systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("farewellNumber"));
				}
			} else {
				if (day1.equals(day2) && i>0) {
					String deptId = deptService.getDeptIdByName("一科");
					systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("farewellNumber"));
				}
			}
		}
		//灵堂任务提示
		if (commissionOrder.getMourningId() != null && !commissionOrder.getMourningId().equals("")) {
			Timestamp mourningTime = commissionOrder.getMourningTime();//修改后的灵堂时间
			if (userNoticeService.ifDateExit(Const.NoticeType_MourningTask,DateTools.getDate(mourningTime))) {
				if(i==1){
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_MourningTask,DateTools.getDate(mourningTime), i);
				}else{
					updateNoticeToday(mourningTime,Const.NoticeType_MourningTask);
				}
			} else if(!userNoticeService.ifDateExit(Const.NoticeType_MourningTask,DateTools.getDate(mourningTime)) && i>0) {
				userNoticeService.addNoticeOfDate(Const.NoticeType_MourningTask, DateTools.getDate(mourningTime));
			}
			day1 = DateTools.getTimeFormatString("yyyy-MM-dd", mourningTime);
			if (oldCom != null && oldCom.getMourningId() != null && !"".equals(oldCom.getMourningId())) {
				Timestamp oldMourningTime = oldCom.getMourningTime();
				if (!DateTools.getTimeFormatString("yyyy-MM-dd",oldMourningTime).equals(day2) && day1.equals(day2) && i >0) {
					String deptId = deptService.getDeptIdByName("一科");
					systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("farewellNumber"));
				}
			} else {
				if (day1.equals(day2) && i>0) {
					String deptId = deptService.getDeptIdByName("一科");
					systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("mourningNumber"));
				}
			}
		}
		//丧葬用品项 提示
		byte funeralByte =0;// 判断  丧葬用品任务列表中中是否有对应项
		Timestamp takeawayTime = getTakeawayTime(commissionOrder);
		if (takeawayTime != null) {
			for(int t=0;t<list.size();t++){
				ItemOrder itemOrder = list.get(t);
				Item item =  itemOrder.getItem();
				byte sort=item.getSort();
				if(sort==ItemConst.BaoBuHe_Sort || sort== ItemConst.XiaoHuaQuan_Sort ||
						sort == ItemConst.BaoHuJi_Sort || sort == ItemConst.GuHuiHe_Sort || sort == ItemConst.HongXiaoDai_Sort ||
						sort == ItemConst.ShouBei_Sort || sort == ItemConst.ShouYi_Sort ||sort == ItemConst.YuSan_Sort){
					funeralByte = 1;
					break;
				}
			}
			if (funeralByte == 1) {
				// Timestamp createTime = commissionOrder.getCreatTime();
				if (userNoticeService.ifDateExit(Const.NoticeType_FuneralTask, DateTools.getDate(takeawayTime))) {
					if (i == 1) {
						userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FuneralTask,
								DateTools.getDate(takeawayTime), i);
					} else {
						updateNoticeToday(takeawayTime, Const.NoticeType_FuneralTask);
					}
				} else if (!userNoticeService.ifDateExit(Const.NoticeType_FuneralTask, DateTools.getDate(takeawayTime))
						&& i > 0) {
					userNoticeService.addNoticeOfDate(Const.NoticeType_FuneralTask, DateTools.getDate(takeawayTime));
				}
				funeralByte = 0;//还原为0
				day1 = DateTools.getTimeFormatString("yyyy-MM-dd", takeawayTime);
				if (list2 != null) {
					for (int t = 0; t < list2.size(); t++) {//进行 判断修改之前有没有选择丧葬用品。
						ItemOrder itemOrder = list2.get(t);
						Item item = itemOrder.getItem();
						byte sort = item.getSort();
						if (sort == ItemConst.BaoBuHe_Sort || sort == ItemConst.XiaoHuaQuan_Sort
								|| sort == ItemConst.BaoHuJi_Sort || sort == ItemConst.GuHuiHe_Sort
								|| sort == ItemConst.HongXiaoDai_Sort || sort == ItemConst.ShouBei_Sort
								|| sort == ItemConst.ShouYi_Sort || sort == ItemConst.YuSan_Sort) {
							funeralByte = 1;
							break;
						}
					} 
				}
				if (funeralByte == 1) {//如果修改之前有选择丧葬用品
					Timestamp oldTakeawayTime = getTakeawayTime(oldCom);//修改之前的领用时间
					if (!DateTools.getTimeFormatString("yyyy-MM-dd",oldTakeawayTime).equals(day2) && day1.equals(day2) && i >0) {
						String deptId = deptService.getDeptIdByName("一科");
						systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("funeralNumber"));
					} 
				} else {
					if (day1.equals(day2) && i > 0) {
						String deptId = deptService.getDeptIdByName("一科");
						systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("funeralNumber"));
					}
				}
			}
		}
	}
	/**
	 * 委托 业务 付费  后的  遗体处理的提醒
	 */
	public void delegrateNotice(CommissionOrder commissionOrder) {
		// 遗体解冻消息提醒
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("commissionOrderId", commissionOrder.getId());
		List<FreezerRecord> listFre = freezerRecordService.getFreeComListPayed(map);	
		if (listFre.size() != 0) {
			FreezerRecord freezerRecord = listFre.get(0);
			if (freezerRecord.getAutoTime() != null) {
				Date autoDate =DateTools.getDate(freezerRecord.getAutoTime());
				//判断是否有当日需要提醒 的数据  如果有 number+1  没有就新建一jilu 插入
				if(userNoticeService.ifDateExit(Const.NoticeType_FysytjdTask, autoDate)){
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FysytjdTask, autoDate, 1);
				}
				else{
					userNoticeService.addNoticeOfDate(Const.NoticeType_FysytjdTask, autoDate);
				}
				//判断是否同天
				String day1 = DateTools.getTimeFormatString("yyyy-MM-dd", freezerRecord.getAutoTime());
				String day2 = DateTools.getTimeString("yyyy-MM-dd");
				if (day1.equals(day2)) {
					String deptId = deptService.getDeptIdByName("二科");
					systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("freezerThawNumber"));
				}
			}
		}
	}
	/**
	 * 撤销收费 时   相应的  socker-number状态 -1
	 */
	public void  cancleDelegateNotice(String id){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("commissionOrderId", id);
		List<FreezerRecord> listFre = freezerRecordService.getFreeComListPayed(map);
		if (listFre.size() != 0) {
			FreezerRecord freezerRecord = listFre.get(0);
			if(freezerRecord.getAutoTime()!=null){//限制，解冻时间不为空才执行，否则将无法撤消，会报null
					updateNoticeToday(freezerRecord.getAutoTime(),Const.NoticeType_FysytjdTask);
					
					//判断是否同天
					String day1 = DateTools.getTimeFormatString("yyyy-MM-dd", freezerRecord.getAutoTime());
					String day2 = DateTools.getTimeString("yyyy-MM-dd");
					if (day1.equals(day2)) {
						String deptId = deptService.getDeptIdByName("二科");
						systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("freezerThawNumber"));
					}
			}
			
		}
			
	}

	/**
	 * 对当日的消息  按照userId  分别对进行操作
	 * 适用于  撤销审核  s删除 或更新 记录时  对当日消息的影响  避免出现消息为负数的情况  
	 * @param dilltime
	 * @param noticeType
	 */
	public void updateNoticeToday(Timestamp  dilltime ,String noticeType){
		if (dilltime != null) {
			Date date = DateTools.getThisDate();
			Date date2 = DateTools.getDate(dilltime);
			String day1 = DateTools.getTimeFormatString("yyyy-MM-dd", dilltime);
			String day2 = DateTools.getTimeString("yyyy-MM-dd");
			if (day1.equals(day2)) {
				List<Notice> listNotice = userNoticeService.getAllNumberByDep(noticeType, date);
				for (int i = 0; i < listNotice.size(); i++) {
					Notice notice = listNotice.get(i);
					if (notice.getNumber() > 0) {
						userNoticeService.updateUserNoticeByUserId(notice.getUserId(), noticeType, date);
					}
				}
			} else {
				userNoticeService.updateUserNoticeByNoticeType(noticeType, date2, -1);
			} 
		} else {
			return;
		}
	}

	/**
	 *  对车辆调度信息 进行 变更后的更改操作
	 * @param dilltime
	 * @param noticeType
	 */
	public void updateCarNotice(Timestamp  dilltime ,String noticeType){
		Date date = DateTools.getThisDate();
		Date date2= DateTools.getDate(dilltime);
		java.sql.Date afterDay = DateTools.getDayAfterDay(date, 1, Calendar.DATE);
		String day3 =  DateTools.getTimeFromatString("yyyy-MM-dd", afterDay);
		String day2 = DateTools.getTimeFormatString("yyyy-MM-dd", dilltime );
		String day1 = DateTools.getTimeString("yyyy-MM-dd");
		if(day1.equals(day2)  ){
			// 对当天的消息进行处理
			List<Notice> listNotice  = 	 userNoticeService.getAllNumberByDep(noticeType,date); 
			for(int i=0; i<listNotice.size();i++){
				Notice  notice = listNotice.get(i);
				if(notice.getNumber()>0){
					userNoticeService.updateUserNoticeByUserId(notice.getUserId(), noticeType, date);
				}
			}
		}else{
			if(day2.equals(day3)){
				//对第二天的消息进行处理
				List<Notice> listNotice  = 	 userNoticeService.getAllNumberByDep(noticeType,afterDay); 
				for(int i=0; i<listNotice.size();i++){
					Notice  notice = listNotice.get(i);
					if(notice.getNumber()>0){
						userNoticeService.updateUserNoticeByUserId(notice.getUserId(), noticeType, afterDay);
					}
				}
			}else{
				//两天以外的消息
				userNoticeService.updateUserNoticeByNoticeType(noticeType, date2, -1);
			}
		}
		
		
	}
	
	
	
	
	/**
	 * 委托业务   丧葬  中 获取领用时间的提醒函数
	 * 顺序分别为告别时间  火化时间守灵时间 审核时间
	 */
	public  Timestamp getTakeawayTime(CommissionOrder  commissionOrder){
		Timestamp  farewellTime = commissionOrder.getFarewellTime();
		Timestamp mourningTime = commissionOrder.getMourningTime();
		Timestamp checkTime = commissionOrder.getCheckTime();
		Timestamp  creamtionTime = commissionOrder.getCremationTime();
		Timestamp takeawayTime = null;
		if (farewellTime != null) {
			takeawayTime = farewellTime;
		} else if (creamtionTime != null) {
			takeawayTime = creamtionTime;
		} else if (mourningTime != null) {
			takeawayTime = mourningTime;
		} else {
			takeawayTime = checkTime;
		}
		return takeawayTime;	
	}
	/**
	 * 
	 */
	public void updateNoticeRsetAutotTime(Timestamp autoTime ,Timestamp oldAutoTime){
		//更改后日期未发生变更，不做处理
		Date autoDate = DateTools.getDate(autoTime);
		Date  oldDay = DateTools.getDate(oldAutoTime);
		if(!autoDate.equals(oldDay)){
					updateNoticeToday(oldAutoTime,Const.NoticeType_FysytjdTask);
					/*Date autoDate = DateTools.getDate(autoTime);*/
					if(userNoticeService.ifDateExit(Const.NoticeType_FysytjdTask, autoDate)){
						userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FysytjdTask, autoDate, 1);
					}
					else{
						userNoticeService.addNoticeOfDate(Const.NoticeType_FysytjdTask, autoDate);
					}
					//判断是否同天
					String day1 = DateTools.getTimeFormatString("yyyy-MM-dd",autoTime );
					String day2 = DateTools.getTimeString("yyyy-MM-dd");
					if (day1.equals(day2)) {
						String deptId = deptService.getDeptIdByName("二科");
						systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("freezerThawNumber"));
					}
		}
	
		
	}
	
	
	
	
	
}
