package com.hz.socket;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import com.hz.util.Const;

@Component
public class WebSocketHandshakeInterceptor implements HandshakeInterceptor {
	private static Logger logger = LoggerFactory.getLogger(HandshakeInterceptor.class);

	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler handler,
			Exception exception) {
		logger.info("握手之后+++++++++++++++++++++++++++");
	}

	/**
	 * @desp 将HttpSession中对象放入WebSocketSession中
	 */
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler handler,
			Map<String, Object> map) throws Exception {
		logger.info("握手之前+++++++++++++++++++++++++++");
		if (request instanceof ServerHttpRequest) {
			ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
			HttpSession session = servletRequest.getServletRequest().getSession();
			if (session != null) {
				// 区分socket连接以定向发送消息
				map.put("user", session.getAttribute(Const.SESSION_USER));
			}
		}
		return true;
	}
}
