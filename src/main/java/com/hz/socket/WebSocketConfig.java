package com.hz.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;


@Configuration
@EnableWebMvc
@EnableWebSocket
public class WebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {
	
	private static Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);
	
	@Autowired
	private SystemWebSocketHandler socketHandler;
	
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		logger.info("ע�ᴦ��������");
		// ע�ᴦ��������,����urlΪsocketServer������
		registry.addHandler(socketHandler, "/serverSocke1t.do").addInterceptors(new WebSocketHandshakeInterceptor());

		// ע��SockJs�Ĵ���������,����urlΪ/sockjs/socketServer������
		registry.addHandler(socketHandler, "/sockjs/serverSocket.do").addInterceptors(new WebSocketHandshakeInterceptor())
				.withSockJS();
	}
}
