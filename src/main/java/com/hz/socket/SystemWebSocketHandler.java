package com.hz.socket;

import java.io.IOException;
import java.util.ArrayList;
import com.hz.entity.system.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;


@Service
public class SystemWebSocketHandler implements WebSocketHandler {
	private static final ArrayList<WebSocketSession> users;
	private static final Logger logger;
	static {
		users = new ArrayList<WebSocketSession>();
		logger = LoggerFactory.getLogger(SystemWebSocketHandler.class);
	}

	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		logger.info("成功建立socket连接");
		users.add(session);
		String username = session.getAttributes().get("user").toString();
		if (username != null) {
			session.sendMessage(new TextMessage("我们已经成功建立soket通信了"));
		}

	}

	public void handleMessage(WebSocketSession arg0, WebSocketMessage<?> arg1) throws Exception {
		// TODO Auto-generated method stub

	}

	public void handleTransportError(WebSocketSession session, Throwable error) throws Exception {
		if (session.isOpen()) {
			session.close();
		}
		logger.error("连接出现错误:" + error.toString());
		users.remove(session);
	}

	public void afterConnectionClosed(WebSocketSession session, CloseStatus arg1) throws Exception {
		logger.info("连接已关闭");
		users.remove(session);
	}

	public boolean supportsPartialMessages() {
		return false;
	}

	/**
	 * 给所有在线用户发送消息
	 */
	public void sendMessageToUsers(String deptId, TextMessage message) {
		for (WebSocketSession user : users) {
			if (((User)(user.getAttributes().get("user"))).getDeptId().equals(deptId) || ((User)(user.getAttributes().get("user"))).getDeptName().contains("管理")) {
				try { 
					if (user.isOpen()) {
						user.sendMessage(message);
					}
				} catch (IOException e) {
					e.printStackTrace();
				} 
			}
		}
	}

	/**
	 * 给某个用户发送消息
	 *
	 * @param userName
	 * @param message
	 */
	public void sendMessageToUser(String userName, TextMessage message) {
		for (WebSocketSession user : users) {
			if (user.getAttributes().get("user").equals(userName)) {
				try {
					if (user.isOpen()) {
						user.sendMessage(message);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
		}
	}

	
}

