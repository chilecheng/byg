package com.hz.util;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;



/**
 * 工具
 * @author storm
 *
 */
public class DataTools{
	
	/**
	 * 把字符串中有多个空格的变为单个空格
	 * @param str
	 */
	public static String stringConvertOneBlank(String str){
		boolean flagBlank = false;
		String s = "";
		str = str.trim();
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i);
			if((int)c==32){
				if(!flagBlank){
					s+=c;
				}
				flagBlank=true;
			}else{
				flagBlank=false;
				s+=c;
			}
		}
		return s;
	}
	
	/**
	 * 把String 转化成int类型 ID 如果String为null返回为-1
	 * @param str （从request中取得的数据）
	 * @return 返回int值的ID
	 */
	public static int stringToInt(String str){
		int i = -1;
		if(str!=null&&!("".equals(str))){
			i = Integer.parseInt(str);
		} 
		return i;
	}
	
	/**
	 * 把String 转化成long类型 ID 如果String为null返回为-1
	 * @param str （从request中取得的数据）
	 * @return 返回long值
	 */
	public static long stringToLong(String str){
		long i = -1;
		if(str!=null&&!("".equals(str))){
			i = Long.parseLong(str);
		} 
		return i;
	}
	
	public static byte stringToByte(String str){
		byte i=-1;
		if(str!=null&&!("".equals(str))){
			i = Byte.parseByte(str);
		} 
		return i;
	}

	
	/**
	 * 把String 转化成double类型  如果String为null返回为-1
	 * @param str 
	 * @return 返回double值
	 */
	public static double stringToDouble(String str){
		double i = -1;
		if(str!=null && !("".equals(str))){
			i =Double.parseDouble(str);
		}
		return i;
	}
	
	/**
	 * 把字符串数组转成int数组
	 * @param strings
	 * @return
	 */
	public static int[] stringArrayToIntArray(String[] strings){
		if(strings!=null&&!strings.equals("")){
			int[] intArray = new int[strings.length];
			for(int i = 0;i<strings.length;i++){
				intArray[i] = stringToInt(strings[i]);
			}
			return intArray;
		}else{
			return null;
		}
	}
	
	public static String DoubleToString(Double p){
		String str=p.toString();
		for(int i=str.length()-1;i>=0;i--){
			if(str.substring(i).equals("0")){
				str=str.substring(0, i);
			}
			else{
				if(str.substring(i).equals(".")){
					str=str.substring(0,i);
					break;
				}
			}
		}
		if(str.equals("-0")){
			str="0";
		}
		return str;
	}
	
	/**
	 * 把字符串0变为-
	 * @param d
	 * @return
	 */
	public static String changeZero(String d){
		if(d.equals("0")){
			return "-";
		}else{
			return d;
		}
	}
	
	/**
	 * 把字符串数组变为字符串
	 * @param s 字符串数组
	 * @param join 连接符
	 */
	public static String stringArrayJoinString(String[] s,String join){
		StringBuffer str = new StringBuffer();
		if(s!=null){
			for(int i=0;i<s.length;i++){
				str.append(s[i]);
				if(i!=s.length-1){
					str.append(join);
				}
			}
		}
		return str.toString();
	}
	

	/**
	 * 字符串替换（用\"替换")
	 * @param s
	 * @return
	 */
	public static String getReplaceDouble(String s){
		String p="\\\"";
		String str=s.replace("\"", p);
		return str;
	}

	/**
	 * 字符串替换（用\'替换')
	 * @param s
	 * @return
	 */
	public static String getReplaceSingle(String s){
		String p="\\'";
		String str=s.replace("'", p);
		return str;

	}
	
	
	
	/**
	 * 把乱码的中文字符转化成中文
	 * @param str
	 * @return
	 */
	public static String changeToGBK(String str){ 
		return changeToEcode(str, "GB18030");
	}
	/**
	 * 把编码转化成中文
	 * @param str
	 * @param encode 编码
	 * @return
	 */
	public static String changeToEcode(String str,String encode){ 
		String st = null;
		try {
			byte[] b = str.getBytes("ISO-8859-1");
			st = new String(b,encode);
		} catch (UnsupportedEncodingException e) {
			
			e.printStackTrace();
		}
		return st;
	}
		
	/**
	 * 把字符串切成int的数组
	 * @param str
	 * @param splitStr
	 * @return
	 */
	public static int[] StringToIntArray(String str,String splitStr){
		int[] i = null;
		String[] iStr= null;
		try {
			iStr = str.split(splitStr);
			i = new int[iStr.length];
			for (int j = 0; j < iStr.length; j++) {
				i[j] = stringToInt(iStr[j]);
			}
		} catch (Exception e) {
			e.printStackTrace();
//			throw new ErrorMessageException("errors.detail","组群异常，请不要擅自改动数组");
		}		
		return i;
	}
	
	/**
	 * 把字符串切成str的数组
	 * @param str
	 * @param splitStr
	 * @return
	 */
	public static String[] StringToStringArray(String str,String splitStr){
		if(str==null||str.equals("")){
			return null;
		}
		String[] i = null;
		String[] iStr= null;
		try {
			iStr = str.split(splitStr);
			i = new String[iStr.length];
			for (int j = 0; j < iStr.length; j++) {
				i[j] = iStr[j];
			}
		} catch (Exception e) {
			e.printStackTrace();
//			throw new ErrorMessageException("errors.detail","组群异常，请不要擅自改动数组");
		}		
		return i;
	}
	
	/**
	 * 得到doubleArray空数组
	 * @param str
	 * @param splitStr
	 * @return
	 */
	public static double[] getDoubleArray(String str,String splitStr){
		if(str==null||str.equals("")){
			return null;
		}
		int i=str.split(splitStr).length;
		double[] d=new double[i];
		return d;
	}
	
	/**
	 * 把yyyy-MM-dd类型的字符串转成sql date类型
	 * @param dateStr：yyyy-MM-dd类型的字符串 
	 * @return 返回sql类型的date
	 */
	public static Date stringToSqlDate(String dateStr){
		return DateTools.stringToSqlDate(dateStr,"yyyy-MM-dd");
	}
	
	/**
	 * byte的数组转化成用逗号相分隔的字段串
	 * @param arr byte[]数组
	 * @return 返回"1,2,3,4"
	 */
	public static String byteArrayToString(byte[] arr){
		StringBuffer sb = new StringBuffer();
		if(arr.length==0){
//			throw new ErrorMessageException("errors.detail","数组异常，不要擅动数组");
		}
		for(int i=0;i<arr.length;i++){
			sb.append(arr[i]);
			if(i!=arr.length-1){
				sb.append(",");
			}
		}
		return sb.toString();
	}
	
	/**
	 * byte的数组转化成用逗号相分隔的字段串
	 * @param arr byte[]数组
	 * @return 返回"1,2,3,4"
	 */
	public static String IntArrayToString(int[] arr){
		StringBuffer sb = new StringBuffer();
		if(arr.length==0){
//			throw new ErrorMessageException("errors.detail","数组异常，不要擅动数组");
		}
		for(int i=0;i<arr.length;i++){
			sb.append(arr[i]);
			if(i!=arr.length-1){
				sb.append(",");
			}
		}
		return sb.toString();
	}
	
	/**
	 * 字符串分隔后转换成byte[],用","分隔
	 * @param str
	 * @return
	 */
	public static byte[] strSplitToByteArray(String str){
		String[] strArray=str.split(",");
		if(strArray.length==0){
//			throw new ErrorMessageException("errors.detail","数组长度不能为空");
		}
		byte[] b=new byte[strArray.length];
		for(int i=0;i<strArray.length;i++){
			b[i]=stringToByte(strArray[i]);
		}
		return b;
	}
	
	
	/**
	 * 取得配置文件中的所有value
	 * @param key
	 * @return
	 */
	public static String getMessageResources(String key) {
		ResourceBundle bundle = ResourceBundle.getBundle("MessageResources");
		String str = bundle.getString(key);
		return str;
	}
	
	/**
	 * 记数转为字符串，不足之处前面补上0
	 * @param count 记数
	 * @param length 数字长度
	 * @return "0001"
	 */
	public static String countToString(int count,int length){
		String countStr = String.valueOf(count);
		int strLength=countStr.length();
		for(int i=0;i<(length-strLength);i++){
			countStr = "0"+countStr;
		}
		return countStr;
	}
	
	/**
	 * 检查这个数组，如果其中有相同的元素就去除
	 * @param arr
	 * @return
	 */
	public static int[] checkIntArray(int[] arr){
		Arrays.sort(arr);
		int old=arr[0];
		List list = new ArrayList();
		list.add(arr[0]);
		for(int i=1;i<arr.length;i++){
			if(arr[i]!=old){
				list.add(arr[i]);
			}
			old=arr[i];
		}
		int[] a = new int[list.size()];
		for(int i=0;i<a.length;i++){
			a[i] =Integer.parseInt(list.get(i).toString());
		}
		return a;
	}


	
	/**
	 * 计算int的数组总和
	 * @param array int数组
	 * @return 返回总和
	 */
	public static int totalIntegerArrays(int[] array){
		int total = 0;
		for(int i = 0 ;i<array.length;i++){
			total += array[i];
		}
		return total;
	}
	
	/**
	 * 取得配置文件中的byte值
	 * @param key 配置文件的key
	 * @return 返回byte
	 */
	public static byte getByteFromMessageResources(String key){
		return Byte.parseByte(getMessageResources(key));
	}
	
	/**
	 * 去字符串空格
	 * @param str
	 * @return
	 */
	public static String trimString(String str){
		if(str!=null&&!"".equals(str)){
			return str.trim();
		}else{
			return str;
		}
	}
	
	/**
	 * 删除磁盘中的文件
	 */
	public static boolean deleteDiskFile(String fileName){
		File file = new File(fileName);
		return file.delete();
	}
	
	
	/**
	 * 求最大公约数
	 * @param arr int的数组
	 * @return 返回最大公约数
	 */
	public static int getMaxDivisor(int[] arr){
		int max = 1;
		Arrays.sort(arr);
		int covert = arr[0];
		int j = 2;
		List list =  new ArrayList();
		while(j<=covert){
			boolean flag = false;
			for(int i=0;i<arr.length;i++){
				if(arr[i]%j!=0){
					flag = true;
				}
			}
			j++;
			if(!flag){
				for(int i=0;i<arr.length;i++){
					arr[i] = arr[i]/(j-1);
				}
				list.add((j-1));
				j = 2;
			}
			covert = arr[0];
		}
		for(Iterator it = list.iterator();it.hasNext();){
			max = max*(Integer)it.next();
		}
		return max;
	}
	
	/**
	 * 求最大公约数
	 * @param arr int的数组
	 * @return 返回最大公约数
	 */
	public static int getMaxDivisor(Object[] arr){
		int max = 1;
		Arrays.sort(arr);
		int covert = (Integer)arr[0];
		int j = 2;
		List list =  new ArrayList();
		while(j<=covert){
			boolean flag = false;
			for(int i=0;i<arr.length;i++){
				if((Integer)arr[i]%j!=0){
					flag = true;
				}
			}
			j++;
			if(!flag){
				for(int i=0;i<arr.length;i++){
					arr[i] = (Integer)arr[i]/(j-1);
				}
				list.add((j-1));
				j = 2;
			}
			covert = (Integer)arr[0];
		}
		for(Iterator it = list.iterator();it.hasNext();){
			max = max*(Integer)it.next();
		}
		return max;
	}
	
	
	/**
	 * 得到当前日期的年份
	 * @return
	 */
	public static int getThisYear(){
		int year=0;
		GregorianCalendar g=new GregorianCalendar();
		year=(int)g.get(Calendar.YEAR);
		return year;
	}
	
	/**
	 * 得到当前日期的月份
	 * @return
	 */
	public static int getThisMonth(){
		int month=0;
		GregorianCalendar g=new GregorianCalendar();
		month=(int)g.get(Calendar.MONTH)+1;
		return month;
	}
	
	/**
	 * 得到当前日期的年月
	 * @return
	 */
	public static String getThisYearMonth(){
		GregorianCalendar g=new GregorianCalendar();
		return ""+g.get(Calendar.YEAR)+""+(g.get(Calendar.MONTH)+1);
	}
	

	
	/**
	 * 对double类型的四舍五入
	 * @param v
	 * @param scale
	 * @return
	 */
	public static double round(double v,int scale){
        if(scale<0){
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
            }
        BigDecimal b = new BigDecimal(Double.toString(v));
        BigDecimal one = new BigDecimal("1");
        return b.divide(one,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
    }

	
	
	/**
	 * 两个byte[]类型取相同的类型
	 * @param types1
	 * @param types2
	 * @return
	 */
	public static byte[] getSameTypes(byte[] types1,byte[] types2){
		if(types1==null||types2==null){
			return null;
		}
		List list=new ArrayList();
		for(int i=0;i<types1.length;i++){
			list.add(types1[i]);
		}
		List list2=new ArrayList();
		for(int i=0;i<types2.length;i++){
			list2.add(types2[i]);
		}
		list.retainAll(list2);
		if(list.size()==0){
			return null;
		}else{
			byte[] newTypes=new byte[list.size()];
			for(int i=0;i<list.size();i++){
				newTypes[i]=((Byte)list.get(i)).byteValue();
			}
			return newTypes;
		}
	}
	
	/**
	 * string数组转化为byte数组
	 * @param types
	 * @return
	 */
	public static byte[] stringToByte(String[] types){
		byte[] newType=new byte[types.length];
		for(int i=0;i<types.length;i++){
			newType[i]=stringToByte(types[i]);
		}
		return newType;
	}
	
	
	/**
	 * 检测某一类型是否在类型数组中
	 * @param type
	 * @param types
	 * @return
	 */
	public static Boolean typeIsInTypes(byte type,byte[] types){
		for(int i=0;i<types.length;i++){
			if(types[i]==type){
				return true;
			}
		}
		return false;
	}
	


	
	/**
	 * 把object对象转化为double型数值
	 * @param obj(obj为空返回0)
	 * @return
	 */
	public static double getDoubleNumber(Object obj){
		if(obj==null){
			return 0.0f;
		}else{
			return ((Double)obj).doubleValue(); 
		}
	}
	
	/**
	 * quantity<=0返回0,否则返回原数
	 * @param quantity
	 * @return
	 */
	public static double getDoubleNumber(double quantity){
		if(quantity>0.0f){
			return quantity;
		}else{
			return 0.0f;
		}
	}
	
	/**
	 * 字符串转换，如：def,frd,ded 转变为 'def','frd','ded'
	 * @param str
	 * @return
	 */
	public static String stringSplitToString(String str){
		String newStr="";
		for(int i=0;i<str.split(",").length;i++){
			if(i!=0){
				newStr+=",";
			}
			newStr+="'"+str.split(",")[i]+"'";
		}
		return newStr;
	}
	
	/**
	 * 把数位生成8位的二进制，不够位数用零补回八位
	 * @param data
	 * @return
	 */
	public static String byteChangeOnebinaryString(int data){
		String str = Integer.toBinaryString(data);
		if(str.length()%8!=0){
			for(int i=0;i<=str.length()%8;i++){
				str = "0"+str;
			}
		}
		return str;
	}
	/**
	 * 给数字前面加上零
	 * @param num　数字　
	 * @param length　长度
	 * @return
	 */
	public static String addZeroToNumberString(int num,int length){
		String code = num+"";
		return addZeroToString(code, length);
	}
	/**
	 * 给号码类前添加零
	 * @param code　号码
	 * @param length　长度
	 * @return
	 */
	public static String addZeroToString(String code,int length){
		for(int i=code.length();i<length;i++){
			code = "0"+code;
		}
		return code;
	}

	/**
	 * list集合转化为string(用","分隔)
	 * @param list
	 * @return
	 */
	public static String listToString(List list){
		String str="";
		for(int i=0;i<list.size();i++){
			if(i!=0){
				str+=",";
			}
			str+=list.get(i);
		}
		return str;
	}
	
	/**
	 * 得到两个日期当中的所有月份（2012-04）
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	public static List getMonthsList(Date beginDate,Date endDate){
		Calendar c=Calendar.getInstance();
		if(beginDate==null||endDate==null){
			return new ArrayList();
		}
		c.setTime(beginDate);
		int year=c.get(Calendar.YEAR);
		int month=c.get(Calendar.MONTH)+1;
		List months=new ArrayList();
		String b=beginDate.toString().substring(0, 7);
		String e=endDate.toString().substring(0, 7);
		while(b.compareTo(e)<=0){
			months.add(b);
			if(month==12){
				month=1;
				year+=1;
			}else{
				month++;
			}
			b=year+"-"+(month<10?"0"+month:""+month);
		}
		return months;
	}
	/**
	 * 按指定的日期格式生成
	 * @param time
	 * @param dateFormat
	 * @return
	 */
	public String dateFormat(Timestamp time,String dateFormat){
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(new Date(time.getTime()));
	}
	
	/**
	 * 小写金额转换成大写金额
	 * @param num
	 * @return
	 */
	public static String accountInWords(double num){
		String str1 = "零壹贰叁肆伍陆柒捌玖";            //0-9所对应的汉字
		String str2 = "万仟佰拾亿仟佰拾万仟佰拾元角分"; //数字位所对应的汉字
		String str3 = "";    //从原num值中取出的值
		String str4 = "";    //数字的字符串形式
		String str5 = "";  //人民币大写金额形式
		int i;    //循环变量
		int j;    //num的值乘以100的字符串长度
		String ch1 = "";    //数字的汉语读法
		String ch2 = "";    //数字位的汉字读法
		int nzero = 0;  //用来计算连续的零值是几个
		int temp;            //从原num值中取出的值
		 
		//将num取绝对值并四舍五入取2位小数
		num=Math.abs(num);
		int n=(int)(num*100);
		num=(double)n/100;
		//	将num乘100并转换成字符串形式
		str4 = ""+(int)(num*100);
		//	找出最高位
		j = str4.length();      
		if (j > 15){return "溢出";}
		//取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分
		str2 = str2.substring(15-j);
           
		//循环取出每一位需要转换的值
		for(i=0;i<j;i++){
			//取出需转换的某一位的值
			str3 = str4.substring(i,i+1); 
			//转换为数字
			temp = DataTools.stringToInt(str3);     
			
			if (i != (j-3) && i != (j-7) && i != (j-11) && i != (j-15)){
				//当所取位数不为元、万、亿、万亿上的数字时
				if (str3 == "0"){
					ch1 = "";
					ch2 = "";
					nzero = nzero + 1;
				}else{
					if(str3 != "0" && nzero != 0){
						ch1 = "零" + str1.substring(temp*1,temp*1+1);
						ch2 = str2.substring(i,i+1);
						nzero = 0;
					}else{
						ch1 = str1.substring(temp*1,temp*1+1);
						ch2 = str2.substring(i,i+1);
						nzero = 0;
					}
				}
			}else{
				//该位是万亿，亿，万，元位等关键位
				if (str3 != "0" && nzero != 0){
					ch1 = "零" + str1.substring(temp*1,temp*1+1);
					ch2 = str2.substring(i,1);
					nzero = 0;
				}else{
					if (str3 != "0" && nzero == 0){
						ch1 = str1.substring(temp*1,temp*1+1);
						ch2 = str2.substring(i,i+1);
						nzero = 0;
					}else{
						if (str3 == "0" && nzero >= 3){
							ch1 = "";
							ch2 = "";
							nzero = nzero + 1;
						}else{
							if (j >= 11){
								ch1 = "";
								nzero = nzero + 1;
							}else{
								ch1 = "";
								ch2 = str2.substring(i,i+1);
								nzero = nzero + 1;
							}
						}
					}
				}
			}
			if (i == (j-11) || i == (j-3)){ 
//				如果该位是亿位或元位，则必须写上
				ch2 = str2.substring(i,i+1);
			}
			str5 = str5 + ch1 + ch2;
//			if (i == j-1 && str3.equals("0")){   
////				最后一位（分）为0时，加上“整”
//				str5 = str5 + '整';
//			}	
		}
		if (num == 0){
			str5 = "零元整";
		}
		return str5;
	}
	
	/**
	 * 解码encode字符
	 * @param encode
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String getDecoder(String encode) throws UnsupportedEncodingException{
		return java.net.URLDecoder.decode(encode, "UTF-8");
	}
	

	/**
	 * 得到选择框，根据集合
	 * @param list (object[] 0:value值 1:显示值)
	 * @param value (传进来的值)
	 * @param flag (true--"全部"　false--"没有全部")
	 * @return
	 */
	public static String getOptionByList(List<Object[]> list,String value,boolean flag){
		StringBuffer  str=new StringBuffer();
		if(flag){
			str.append("<option value=''>全部</option>");
		}
		for(int i=0;i<list.size();i++){
			Object[] obj=list.get(i);
			str.append("<option value='"+obj[0]+"'");
			if(value!=null&&value.equals(obj[0].toString())){
				str.append(" selected");
			}
			str.append(">"+obj[1]+"</option>");
		}
		return str.toString();
	}
	/**
	 * 得到选择框，根据集合(特殊红色显示)
	 * @param list (object[] 0:value值 1:显示值)
	 * @param value (传进来的值)
	 * @param flag (true--"全部"　false--"没有全部")
	 * @return
	 */
	public static String getOptionByList(List<Object[]> list,String value,boolean flag,String isHave){
		StringBuffer  str=new StringBuffer();
		if(flag){
			str.append("<option value=''>全部</option>");
		}
		for(int i=0;i<list.size();i++){
			Object[] obj=list.get(i);
			str.append("<option value='"+obj[0]+"'");
			if(value!=null&&value.equals(obj[0].toString())){
				str.append(" selected");
				if("no".equals(isHave)){
					str.append(" style='color:#DD4B39'");
				}
			}
			str.append(">"+obj[1]+"</option>");
		}
		return str.toString();
	}
	/**
	 * 得到选择框readonly
	 * @param list (object[] 0:value值 1:显示值)
	 * @param value (传进来的值)
	 * @param flag (true--"全部"　false--"没有全部")
	 * @return
	 */
	public static String getNameByList(List<Object[]> list,String value,boolean flag){
		StringBuffer  str=new StringBuffer();
		for(int i=0;i<list.size();i++){
			Object[] obj=list.get(i);
//			str.append("<option value='"+obj[0]+"'");
			if(value!=null&&value.equals(obj[0].toString())){
				str.append(obj[1]);
			}
//			str.append(">"+obj[1]+"</option>");
		}
		return str.toString();
	}
	/**
	 * 根据集合得选择框,用于下拉选择,只有单属性
	 * @param list
	 * @param value
	 * @param flag
	 * @return
	 */
	public static String getOption(List<Object[]> list,String value,boolean flag){
		StringBuffer  str=new StringBuffer();
		if(flag){
			str.append("<option value=''></option>");
		}
		for(int i=0;i<list.size();i++){
			Object[] obj=list.get(i);
			str.append("<option value='"+obj[1]+"'");
			if(value!=null&&value.equals(obj[1].toString())){
				str.append(" selected");
			}
			str.append("></option>");
		}
		return str.toString(); 
	}

	/**
	 * 打印异常堆栈
	 * @param t
	 * @return
	 */
	public static String getStackTrace(Exception t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);

		try {
			t.printStackTrace(pw);
			return sw.toString();
		} finally {
			pw.close();
		}
	}
}
