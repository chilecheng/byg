package com.hz.util;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;


public class ExportWord {
	public void exportWord(String title, String[] headers,List<Object[]> dataset, OutputStream out){
		//if (dataset.size() > 0) {
			exportWord(title, headers, dataset, out, "yyyy-MM-dd");
		//}
	}
	 public void exportWord(String title, String[] headers,List<Object[]> dataset, OutputStream out, String pattern){
		
		XWPFDocument doc = new XWPFDocument();
		XWPFTable table = null;
		if (dataset.size() > 0) {
			table = doc.createTable(dataset.size(),dataset.get(0).length);
			table.setCellMargins(100, 200, 100, 200);
		}
		/*for(int index=0;index<dataset.size();index++){*/
		for(int index=0;index<dataset.size();index++){
		/*	Object[] rowData = dataset.get(index);*/
			Object[] rowData = dataset.get(index);
			for(int i=0;i<rowData.length;i++){
				//XWPFTableCell cell = row.getCell(i);
				XWPFTableCell cell = table.getRow(index).getCell(i);
				try{
					 Object value = rowData[i];
	                    // 判断值的类型后进行强制类型转换
	                  String textValue = null;
	                  if (value instanceof Date) {
	                        Date date = (Date) value;
	                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
	                        textValue = sdf.format(date);
	                    } else if (value != null){
	                    	textValue = value.toString();
	                    } 
	                   
	                    // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成
	                    if (textValue != null) {
	                        cell.setText(textValue);
	                        cell.setVerticalAlignment(XWPFVertAlign.CENTER);
	                    }
				}catch (SecurityException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }finally {
                    // 清理资源
                }
			}
		}
		try {
            doc.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
	 }
}
