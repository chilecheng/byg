package com.hz.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 日期类的工具
 * @author storm
 *
 */
public class DateTools {
	/**
	 * 读取当前时间以指定的格式()
	 * @param format
	 * @return
	 */
	public static String getTimeString(String format){
		return getTimeFromatString(format, new Date(System.currentTimeMillis()));
	}
	/**
	 * 把日期格式化输出(sqlDate类型的时间)
	 * @param format
	 * @param date
	 * @return
	 */
	public static String getTimeFromatString(String format,Date date){
		String dateFormat = "";
		if (date != null) {
			SimpleDateFormat dateFormate = new SimpleDateFormat(format);
			dateFormat =  dateFormate.format(new java.util.Date(date.getTime()));
		}
		return dateFormat;
	}
	
	/**
	 * 把时间格式化输出(Timestamp类型的时间)
	 * @param format
	 * @param time
	 * @return
	 */
	public static String getTimeFormatString(String format,Timestamp time){
		SimpleDateFormat dateFormate = new SimpleDateFormat(format);
		return dateFormate.format(time.getTime());
	}
	/**
	 * 把时间格式化输出(utilDate类型的时间)
	 * @param format
	 * @param time
	 * @return
	 */
	public static String getTimeFormatString(String format,java.util.Date time){
		SimpleDateFormat dateFormate = new SimpleDateFormat(format);
		return dateFormate.format(time.getTime());
	}
	/**
	 * 得到时间
	 * @param year
	 * @param month
	 * @param date
	 * @param hour
	 * @param minute
	 * @param second
	 * @return
	 */
	public static Timestamp getTime(int year,int month,int date,int hour,int minute,int second){
		Calendar c=Calendar.getInstance();
		c.set(year, month-1, date, hour, minute, second);
		return new Timestamp(c.getTimeInMillis());
	}
	
	/**
	 * 添加今天以后的几天的日期月的字符串长度
	 * @param len
	 * @return
	 */
	public static String getMonthDayString(int len){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, len);
		int dayInt = calendar.get(Calendar.DATE);
		String day = null;
		if(dayInt<10){
			day = "0"+String.valueOf(dayInt);
		}else{
			day = String.valueOf(dayInt);
		}
		int monthInt = calendar.get(Calendar.MONTH)+1;
		String month = null;
		if(monthInt<10){
			month = "0"+String.valueOf(monthInt);
		}else{
			month = String.valueOf(monthInt);
		}
		return month+day;
	}

	/**
	 * 取得当前的时间
	 * @return　返回当前的日期
	 */
	public static Date getThisDate(){
		Date date = new Date(System.currentTimeMillis());
		return date;
	}
	/**
	 * 取得当前的时间(util类型)
	 * @return　返回当前的日期
	 */
	public static java.util.Date getThisDateUtil(){
		java.util.Date date = new java.util.Date();
		return date;
	}
	/**
	 * 取得当前的时间(Timestamp类型)
	 * @return　返回当前的日期
	 */
	public static Timestamp getThisDateTimestamp(){
		java.util.Date date = new java.util.Date();
		return new Timestamp(date.getTime());
	}
	/**
	 * 得到当前日期的月份
	 * @return
	 */
	public static int getThisMonth(){
		int month=0;
		GregorianCalendar g=new GregorianCalendar();
		month=(int)g.get(Calendar.MONTH);
		return month+1;
	}
	
	/**
	 * 得到月份option
	 * @param year
	 * @return
	 */
	public static String optionMonth(int month){
		return optionMonth(month,(byte)2);
	}
	
	/**
	 * 得到月份option(查询)
	 * @param year
	 * @return
	 */
	public static String optionMonthSearch(int month){
		return optionMonth(month,(byte)1);
	}
	/**
	 * 
	 * 取得当天时间的最后一秒
	 * @return
	 */
	public static Date getThisDayEndTime(){
		Date date = new Date(getDayEndTime(new Date(System.currentTimeMillis())).getTime());
		return date;
	}

	/**
	 * 得到月份option
	 * @param year
	 * @param flag
	 * @return
	 */
	private static String optionMonth(int month,byte flag){
		StringBuffer sb=new StringBuffer();
		if(flag==1){
			sb.append("<option value='0' ");
			if(month==0){
				sb.append(" selected");
			}
			sb.append(">--</option>");
		}
		for(int i=1;i<=12;i++){
			sb.append("<option value='");
			sb.append(i);
			if(i==month){
				sb.append("' selected='true");
			}
			sb.append("' > ");
			sb.append(""+i);
			sb.append("</option>");
		}
		return sb.toString();
	}
	
	/**
	 * 添加今天以后的几天的日期字符串
	 * @param len 以后几天
	 * @return 
	 */
	public static String getDayString(int len){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, len);
		int dayInt = calendar.get(Calendar.DATE);
		String day = null;
		if(dayInt<10){
			day = "0"+String.valueOf(dayInt);
		}else{
			day = String.valueOf(dayInt);
		}
		return getMonthString()+day;
	}
	/**
	 * 返回年月日的字符串，20080504
	 * @return 
	 */
	public static String getDayString(){
		return getDayString(0);
	}
	/**
	 * 将yyyyMMdd转换成yyyy-MM-dd
	 * @param str
	 * @return
	 */
	public static String formatDate(String str){
		  SimpleDateFormat sf1 = new SimpleDateFormat("yyyyMMdd");
		     SimpleDateFormat sf2 =new SimpleDateFormat("yyyy-MM-dd");
		     String sfstr = "";
		     try {
		      sfstr = sf2.format(sf1.parse(str));
		  } catch (ParseException e) {
		   e.printStackTrace();
		  }
		  return sfstr;
		 }
	
	/**
	 * 取得一天的最后一刻
	 * @param date
	 * @return 邮戳时间
	 */
	public static Timestamp getDayEndTime(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String timeStr = String.valueOf(calendar.get(Calendar.YEAR))+"-"+String.valueOf(calendar.get(Calendar.MONTH)+1)+"-"+String.valueOf(calendar.get(Calendar.DATE));
		Timestamp endTime = new Timestamp(DataTools.stringToSqlDate(timeStr).getTime()+(24*3600*1000-1));
		return endTime;
	}
	/**
	 * 得到某时间之后之前几天的日期
	 * @param time
	 * @param len
	 * @return
	 */
	public static String getAfterDays(String time,int len) {
        // 时间表示格式可以改变，yyyyMMdd需要写例如20160523这种形式的时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String str = "2016/05/23";
        // 将字符串的日期转为Date类型，ParsePosition(0)表示从第一个字符开始解析
        java.util.Date date = sdf.parse(time, new ParsePosition(0));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // add方法中的第二个参数n中，正数表示该日期后n天，负数表示该日期的前n天
        calendar.add(Calendar.DATE, len);
        java.util.Date date1 = calendar.getTime();
        String out = sdf.format(date1);
        return out;
    }
	
	
	
	/**
	 * 返回一天以后的timestamp对象
	 * @param date date时间对象
	 * @return 返回一天以后的时间
	 */
	public static Timestamp oneDayBack(java.util.Date date){
		return new Timestamp(date.getTime()+24*3600*1000-1000);
	}
	
	/**
	 * 返回年的字符串
	 * @return 2008,2007,2006
	 */
	public static String getYearString(){
		return getYearString(0);
	}
	
	/**
	 * 返回年加月的字符串
	 * @return 200805,200712
	 */
	public static String getMonthString(){
		return getMonthString(0);
	}
	/**
	 * 取得返回几天以后的年份
	 * @param len
	 * @return
	 */
	public static String getYearString(int len){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, len);
		return String.valueOf(calendar.get(Calendar.YEAR));
	}
	
	/**
	 * 取得今天以后几天的月分
	 * @param len 延后几天
	 * @return
	 */
	public static String getMonthString(int len){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, len);
		int month = calendar.get(Calendar.MONTH)+1;
		String monthString = null;
		if(month<10){
			monthString ="0"+String.valueOf(month);
		}else{
			monthString = String.valueOf(month);
		}
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		return year+monthString;
	}
	
	/**
	 * 取得某天以后的几天
	 * @param date 某天
	 * @param len 延后长度
	 * @return 返回某天
	 */
	public static Date getDayAfterDay(Date date,int len,int CalendarType){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(CalendarType, len);
		date = new Date(calendar.getTimeInMillis());
		return date;
	}
	/**
	 * 取得某时间之后的几个小时
	 * @param date 某天某时
	 * @param len 延后长度
	 * @return 返回某天某时
	 */
	public static java.util.Date getHourAfterHour(java.util.Date date,int from, int len,int CalendarType){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int date_d = calendar.get(Calendar.DATE);
		calendar.set(Calendar.HOUR_OF_DAY, from);
		calendar.set(Calendar.MINUTE, 0);
		calendar.add(CalendarType, len);
		calendar.set(Calendar.DATE, date_d);
		date = new java.util.Date(calendar.getTimeInMillis());
		return date;
	}
	/**
	 * 取得某时间之后的几个小时(得到半点)
	 * @param date 某天某时
	 * @param len 延后长度
	 * @return 返回某天某时
	 */
	public static java.util.Date getHourAfterHourHalf(java.util.Date date,int from, int len,int minntes,int CalendarType){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, from);
		calendar.set(Calendar.MINUTE, minntes);
		calendar.add(CalendarType, len);
		date = new java.util.Date(calendar.getTimeInMillis());
		return date;
	}
	/**
	 * 取得某时间之后的半小时
	 * @param date 某天某时
	 * @param len 延后长度
	 * @return 返回某天某时
	 */
	public static java.util.Date getHalfHourAfterHalfHour(java.util.Date date,int from, int len,int CalendarType){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, from);
		calendar.set(Calendar.MINUTE,0);
		calendar.add(CalendarType, len);
		date = new java.util.Date(calendar.getTimeInMillis());
		return date;
	}
	/*public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		
		java.util.Date d = new java.util.Date();
		
	}*/
	/**
	 * 取得月日的字符串
	 * @param date 日期
	 * @param join 连接符
	 * @return 返回   "月 连接符  日"
	 */
	public static String getMonthDay(Date date,String join){
		Calendar cal = Calendar.getInstance();
		if(date!=null){
			cal.setTime(date);
		}
		StringBuffer str = new StringBuffer();
		str.append(cal.get(Calendar.MONTH));
		if(join!=null&&!join.equals("")){
			str.append(join);
		}
		str.append(cal.get(Calendar.DATE));
		return str.toString();
	}
	
	/**
	 * 得到小时分，用join链接，如0230
	 * @param timestamp
	 * @param join
	 * @return
	 */
	public static String getHourMiniute(Timestamp timestamp,String join){
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(timestamp.getTime());
		int hour=cal.get(Calendar.HOUR_OF_DAY);
		int miniute=cal.get(Calendar.MINUTE);
		String str="";
		str+=hour<10?"0"+hour:hour;
		str+=join;
		str+=miniute<10?"0"+miniute:miniute;
		return str;
	}
	
	/**
	 * 当前时间的小时加上24个小时后，变成小时分字符串,如2603
	 * @param timestamp
	 * @param join
	 * @return
	 */
	public static String getHourMiniuteAdd24(Timestamp timestamp,String join){
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(timestamp.getTime());
		int hour=cal.get(Calendar.HOUR_OF_DAY);
		int miniute=cal.get(Calendar.MINUTE);
		hour=hour+24;
		String str=""+hour+join;
		str+=miniute<10?"0"+miniute:miniute;
		return str;
	}

	/**
	 * 取得日的字符串
	 * @param date
	 * @param join
	 * @return
	 */
	public static String getDay(Date date,String join){
		Calendar cal = Calendar.getInstance();
		if(date!=null){
			cal.setTime(date);
		}
		String str =  String.valueOf(cal.get(Calendar.DATE));
		return str;
	}
	/**
	 * 对两个sql类型的日期进行比对
	 * @param arg0 
	 * @param arg1
	 * @return 返回就是compare的值大于是1,等于是0小于是-1;
	 */
	public static int compareSqlDate(Date arg0,Date arg1){
			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal1.setTime(arg0);
			cal2.setTime(arg1);
			if(cal1.get(Calendar.YEAR)>cal2.get(Calendar.YEAR)){
				return 1;
			}else if(cal1.get(Calendar.YEAR)<cal2.get(Calendar.YEAR)){
				return -1;
			}else{
				if(cal1.get(Calendar.MONTH)>cal2.get(Calendar.MONTH)){
					return 1;
				}else if(cal1.get(Calendar.MONTH)<cal2.get(Calendar.MONTH)){
					return -1;
				}else{
					if(cal1.get(Calendar.DATE)>cal2.get(Calendar.DATE)){
						return 1;
					}else if(cal1.get(Calendar.DATE)<cal2.get(Calendar.DATE)){
						return -1;
					}else{
						return 0;
					}
				}
		}
	}
	/**
	 * 对比两个时间
	 * @param arg0 
	 * @param arg1
	 * @return 1 表示第一个时间大于第二个时间 0表示等于  -1表示小于
	 */
	public static int compareTime(Timestamp arg0,Timestamp arg1){
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(arg0);
		cal2.setTime(arg1);
		if(cal1.get(Calendar.YEAR)>cal2.get(Calendar.YEAR)){
			return 1;
		}else if(cal1.get(Calendar.YEAR)<cal2.get(Calendar.YEAR)){
			return -1;
		}else{
			if(cal1.get(Calendar.MONTH)>cal2.get(Calendar.MONTH)){
				return 1;
			}else if(cal1.get(Calendar.MONTH)<cal2.get(Calendar.MONTH)){
				return -1;
			}else{
				if(cal1.get(Calendar.DATE)>cal2.get(Calendar.DATE)){
					return 1;
				}else if(cal1.get(Calendar.DATE)<cal2.get(Calendar.DATE)){
					return -1;
				}else{
					if(cal1.get(Calendar.HOUR)>cal2.get(Calendar.HOUR)){
						return 1;
					}else if(cal1.get(Calendar.HOUR)<cal2.get(Calendar.HOUR)){
						return -1;
					}else{
						if(cal1.get(Calendar.MINUTE)>cal2.get(Calendar.MINUTE)){
							return 1;
						}else if(cal1.get(Calendar.MINUTE)<cal2.get(Calendar.MINUTE)){
							return -1;
						}else{
							if(cal1.get(Calendar.SECOND)>cal2.get(Calendar.SECOND)){
								return 1;
							}else if(cal1.get(Calendar.SECOND)<cal2.get(Calendar.SECOND)){
								return -1;
							}else{
								return 0;
							}
						}
					}
					
				}
			}
		}
	}
	
	
	/**
	 * 读出某个时间以后的，某个时长以后的开始日长，yyyy-MM-dd 的时长为 yyyy-MM-dd 00:00:00
	 * @param date
	 * @param len
	 * @param CalandarType
	 * @return
	 */
	public static Date getDayAfterDayBegin(Date date,int len,int CalandarType){
		Date nextDate = getDayAfterDay(date, len, CalandarType);
		return DataTools.stringToSqlDate(nextDate.toString());
	}
	
	/**
	 * 读取某个时间的年初时间返回为 yyyy - 01 - 01
	 * @param date
	 * @return
	 */
	public static Date getYearBegin(Date date){
		SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String dateStr = calendar.get(Calendar.YEAR)+"-01-01";
		Date ndate = null;
		try {
			ndate = new Date( dateFormate.parse(dateStr).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ndate;
	}
	
	/**
	 * 读取某个时间的月初的时间返回为 yyyy - MM - 01
	 * @param date 时间
	 * @return 返回
	 */
	public static Date getMonthBegin(Date date){
		SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String dateStr = calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-01";
		Date ndate = null;
		try {
			ndate = new Date(dateFormate.parse(dateStr).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ndate;
	}
	
	/**
	 * 
	 * 取得这个日期的最后一分一秒
	 * @param date
	 * @return 返回java.sql.date
	 */
	public static java.sql.Date getDayEndSqlDate(Date date){
		Date nd = DataTools.stringToSqlDate(date.toString());
		nd = new Date(nd.getTime()+24*3600*1000-1);
		return nd;
	}

	/**
	 * 取得今天提第一时间
	 * @param date
	 * @return
	 */
	public static java.sql.Date getDayFirstTime(Date date){
		return DataTools.stringToSqlDate(date.toString());
	}

	/**
	 * 取得今天提第一时间
	 * @param date
	 * @return
	 */
	public static java.sql.Date getDayFirstTime(java.util.Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String dateStr = cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DATE);
		return DateTools.stringToSqlDate(dateStr, "yyyy-MM-dd");
	}
	/**
	 * 取得某天的最后一秒
	 * @param date
	 * @return
	 */
	public static java.sql.Date getDayLastTime(Date date){
		return new Date(DataTools.stringToSqlDate(date.toString()).getTime()+24*60*60*1000-1000);
	}
	
	
	/**
	 * 取得某天的最后一秒
	 * @param date
	 * @return
	 */
	public static java.sql.Date getDayLastTime(java.util.Date date){
		return getDayLastTime(getDayFirstTime(date));
	}
	
	/**
	 * 把日期的字符串转化成为sql date类型
	 * @param dateStr sql字符串
	 * @param formatString SimpleDateFormat的匹配字符
	 * @return 返回java.sql.Date
	 */
	public static Date stringToSqlDate(String dateStr,String formatString){
		SimpleDateFormat sdf = new SimpleDateFormat(formatString);
		java.sql.Date date = null;
		try {
			 date = new java.sql.Date(sdf.parse(dateStr).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	/**
	 * 把字符串转化成Timestamp 格式"yyyy-MM-dd hh:mm:ss"
	 * @param dateStr
	 * @return
	 */
	public static Timestamp stringToTimestamp(String dateStr) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Timestamp date = null;
		 date = new Timestamp(sdf.parse(dateStr).getTime());
		return date;
	}
	/**
	 * 批量转换 字符串转化成Timestamp 格式:"yyyy-MM-dd HH:mm"
	 * @param str
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp[] stringToTimeArray(String[] str){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Timestamp[] date = new Timestamp[str.length];
		for (int i = 0; i < str.length; i++) {
			try {
				date[i] = new Timestamp(sdf.parse(str[i]).getTime());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return date;
	}
	/**
	 * 得到某个日期是星期几
	 * @param times
	 * @return
	 */
	public static int getWeek(Timestamp timestamp){
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(timestamp);
		return calendar.get(Calendar.DAY_OF_WEEK)-1;
	}
	
	/**
	 * 得到新的小时分钟字符串
	 * @param hourMiniute(0830)
	 * @param miniute(正数增加，负数减少)
	 * @return 大于2400当2400，小于0000当0000
	 */
	public static String getHourMiniute(String hourMiniute,int miniute){
		String h=hourMiniute.substring(0,2);
		String m=hourMiniute.substring(2,4);
		int mCount=DataTools.stringToInt(h)*60+DataTools.stringToInt(m);
		mCount+=miniute;
		if(mCount<0){
			return "0000";
		}else if(mCount>24*60){
			return "2400";
		}else{
			int h1=mCount/60;
			int m1=mCount%60;
			return (h1<10?"0"+h1:h1+"")+(m1<10?"0"+m1:""+m1);
		}
	}
	
	/**
	 * 得到当前年月的字符串，用split分隔
	 * @param split
	 * @return 2012-02
	 */
	public static String getYearMonth(String split){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 0);
		int month = calendar.get(Calendar.MONTH)+1;
		int year = calendar.get(Calendar.YEAR);
		return getYearMonth(year, month, split);
	}
	
	/**
	 * 得到年月的字符串
	 * @param year
	 * @param month
	 * @param split
	 * @return
	 */
	public static String getYearMonth(int year,int month,String split){
		String monthString="";
		if(month<10){
			monthString ="0"+String.valueOf(month);
		}else{
			monthString = String.valueOf(month);
		}
		return year+split+monthString;
	}
	
	/**
	 * 得到年月的字符串
	 * @param time
	 * @return
	 */
	public static String getYearMonth(Timestamp time){
		Calendar c=Calendar.getInstance();
		c.setTimeInMillis(time.getTime());
		int year=c.get(Calendar.YEAR);
		int month=c.get(Calendar.MONTH);
		return getYearMonth(year, month+1, "-");
	}
	
	/**
	 * 得到年月的字符串（用-分隔）
	 * @param date
	 * @return
	 */
	public static String getYearMonth(Date date){
		Calendar c=Calendar.getInstance();
		c.setTimeInMillis(date.getTime());
		int year=c.get(Calendar.YEAR);
		int month=c.get(Calendar.MONTH);
		return getYearMonth(year, month+1, "-");
	}
	
	/**
	 * 得到某月的最大天数
	 * @param year
	 * @param month
	 * @return
	 */
	public static int getMaxDateMonth(int year,int month){
		Calendar calendar=Calendar.getInstance();
		calendar.set(Calendar.YEAR,year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DATE, 1);
		calendar.roll(Calendar.DATE, -1);
		return calendar.get(Calendar.DATE);
	}
	
	/**
	 * 得到某天是星期几
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static int getWeek(int year,int month,int day){
		Calendar calendar=Calendar.getInstance();
		calendar.set(Calendar.YEAR,year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DATE, day);
		return calendar.get(Calendar.DAY_OF_WEEK)-1;
	}
	
	/**
	 * 得到中文星期（一、二、三、四、五、六、日）
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 */
	public static String getWeekChinese(int year,int month,int day){
		int week=DateTools.getWeek(year, month, day);
		if(week==0){
			return "日";
		}else if(week==1){
			return "一";
		}else if(week==2){
			return "二";
		}
		else if(week==3){
			return "三";
		}
		else if(week==4){
			return "四";
		}
		else if(week==5){
			return "五";
		}else{
			return "六";
		}
	}
	
	/**
	 * 得到月份的天数（是周休的显示是周休）
	 * @param year
	 * @param month
	 * @return
	 */
	public static List<String> getDates(int year,int month){
		List<String> list=new ArrayList<String>();
		int week=0;
		String date="";
		for(int i=1;i<=DateTools.getMaxDateMonth(year,month);i++){
			week=DateTools.getWeek(year, month, i);
			if(week==0){
				date="周日";
			}else if(week==6){
				date="周六";
			}else{
				date=""+i;
			}
			list.add(date);
		}
		return list;
	}
	
	/**
	 * 得到date日期
	 * @param year
	 * @param month
	 * @param date
	 * @return
	 */
	public static Date getDate(int year,int month,int date){
		Calendar c=Calendar.getInstance();
		c.set(year, month-1, date);
		return new Date(c.getTimeInMillis());
	}
	
	/**
	 * timstamp 转 sqlDate
	 * @param time
	 * @return
	 */
	public static Date getDate(Timestamp time){
		Calendar c=Calendar.getInstance();
		c.setTimeInMillis(time.getTime());
		return new Date(c.getTimeInMillis());
	}
	
	/**
	 * 得到当前月份的月末
	 * @return
	 */
	public static String getThisMonthEnd(){
		Calendar calendar=Calendar.getInstance();
		int year=calendar.get(Calendar.YEAR);
		int month=calendar.get(Calendar.MONTH)+1;
		return getThisMonthEnd(year, month);
	}
	
	/**
	 * 得到月末
	 * @param year
	 * @param month(1－－12)
	 * @return
	 */
	public static String getThisMonthEnd(int year,int month) {
        String strY = null;
        String strZ = null;
        boolean leap = false;
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            strZ = "31";
        }
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            strZ = "30";
        }
        if (month == 2) {
            leap = leapYear(year);
            if (leap) {
                strZ = "29";
            }
            else {
                strZ = "28";
            }
        }
        strY = month >= 10 ? String.valueOf(month) : ("0" + month);
        return year + "-" + strY + "-" + strZ;
    }
	
	/**
     * 功能：判断输入年份是否为闰年<br>
     * 
     * @param year
     * @return 是：true  否：false
     * @author pure
     */
    public static boolean leapYear(int year) {
        boolean leap;
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) leap = true;
                else leap = false;
            }
            else leap = true;
        }
        else leap = false;
        return leap;
    }

    /**
     * 得到新的小时分钟数
     * @param hourMiniute
     * @param m(差额，可以为负)
     * @return
     */
    public static String getHourMiniuteNew(String hourMiniute,int m){
    	int hour=DataTools.stringToInt(hourMiniute.substring(0, 2));
    	int miniute=DataTools.stringToInt(hourMiniute.substring(2,4));
    	int totalM=hour*60+miniute;
		totalM+=m;
		if(totalM<0){
			totalM=0;
		}
		if(totalM>60*24){
			totalM=60*24;
		}
    	hour=totalM/60;
    	miniute=totalM%60;
    	return (hour>=10?String.valueOf(hour):"0"+hour)+(miniute>=10?String.valueOf(miniute):"0"+miniute);
    }
    
    /**
     * 得到hourMiniute的总的分钟数
     * @param hourMiniute(0830)
     * @return
     */
    public static int getMiniutes(String hourMiniute){
    	return DataTools.stringToInt(hourMiniute.substring(0,2))*60+DataTools.stringToInt(hourMiniute.substring(2,4));
    }
    
    /**
     * 得出两个日期当中有几周
     * 每周以星期一开始计
     * 用来计算学校学期周数
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int getWeeksByTerm(Date beginDate,Date endDate){
    	Calendar c1=Calendar.getInstance();
		//得出当天的最后一秒
		long time1=DateTools.getDayEndSqlDate(beginDate).getTime();
		long time2=DateTools.getDayEndSqlDate(endDate).getTime();
		if(time1>time2){
//			throw new Exception("开始日期大于结束日期");
		}
		c1.setTime(beginDate);
		long times=time2-time1;
		//相差多少天
		long days=times/(24*60*60*1000)+1;
		//开始日期是周几
		int week=c1.get(Calendar.DAY_OF_WEEK);
		//对应星期－到星期天数字为1到7
		if(week==1){
			week=7;
		}else{
			week--;
		}
		//周数
		int weeks=0;
		if(week!=1){
			//如果开始日期不是周一；周数先加1，总天数减掉n天
			if(days>=8-week){
				days=days-(8-week);
				weeks++;
			}
			
		}
		//周数+总天数除以7的整数
		weeks+=days/7;
		if(days%7!=0){
			//总天数除以7余数不为0，周数+1
			weeks++;
		}
    	return weeks;
    }
    
    /**
     * 得到学期的总月数
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int getMonthsByTerm(Date beginDate,Date endDate){
    	if(DateTools.getDayEndSqlDate(beginDate).getTime()-DateTools.getDayEndSqlDate(endDate).getTime()>0){
//    		throw new Exception("开始日期大于结束日期");
    	}
    	Calendar c1=Calendar.getInstance();
    	Calendar c2=Calendar.getInstance();
    	c1.setTime(beginDate);
    	c2.setTime(endDate);
    	int beginYear=c1.get(Calendar.YEAR);
    	int beginMonth=c1.get(Calendar.MONTH);
    	int endYear=c2.get(Calendar.YEAR);
    	int endMonth=c2.get(Calendar.MONTH);
    	int months=0;
    	if(endYear>beginYear){
    		//先算出开始年的月份
    		months=12-beginMonth+1;
    		//再算多出的整年份*12
    		months+=12*(endYear-beginYear-1);
    		//再算最后一年的月份
    		months+=endMonth;
    	}else if(endYear==beginYear){
    		//两个年份相同
    		months=endMonth-beginMonth+1;
    	}
    	return months;
    }
    
    /**
     * utilDate转化为sqlDate
     * @param date
     * @return
     */
    public static Date utilDateToSqlDate(java.util.Date date){
    	Calendar c=Calendar.getInstance();
		c.setTime(date);
		return new Date(c.getTimeInMillis());
    }
    
    /**
     * 得到当天是月份的第几天
     * @param time
     * @return
     */
    public static int getDateOfMonth(Timestamp time){
		Calendar c=Calendar.getInstance();
		c.setTimeInMillis(time.getTime());
		return c.get(Calendar.DATE);
    }
    
    /**
     * 判断两个时间段 时间是否交叉
     * @param beginTime(0800)
     * @param endTime(1200)
     * @param beginTime1(0900)
     * @param endTime1(1300)
     * @return true:交叉
     */
    public static boolean checkTimeIsCross(String beginTime,String endTime,String beginTime1,String endTime1){
    	if(beginTime.compareTo(beginTime1)>=0&&beginTime.compareTo(endTime1)<=0){
			return true;
		}else if(endTime.compareTo(beginTime1)>=0&&endTime.compareTo(endTime1)<=0){
			return true;
		}else if(beginTime1.compareTo(beginTime)>=0&&beginTime1.compareTo(endTime)<=0){
			return true;
		}else if(endTime1.compareTo(beginTime)>=0&&endTime1.compareTo(endTime)<=0){
			return true;
		}
    	return false;
    }
    /**	判断是否为同1小时
     * @param date1
     * @param date2
     * @return
     */
	public static boolean isSameHour(java.util.Date da, java.util.Date date) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH");
    	String dy1=sdf.format(da);
    	String dy2=sdf.format(date);
    	if(dy1.equals(dy2)){
    		return true;
    	}else{
    		return false;
    	}
	}
	/**
	 * date转timestamp
	 * @param date
	 * @return
	 */
	public static Timestamp dateToTimestamp(java.util.Date date) {
		if (date == null) {
			return null;
		} else {
			return new Timestamp(date.getTime());
		}
	}
	/**
	 * 获取两个时间段内的每一天的日期(精确到天)
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static List<java.util.Date> getEveryDay(java.util.Date startDate, java.util.Date endDate) {
		List<java.util.Date> list = new ArrayList<java.util.Date>();
		Calendar c=Calendar.getInstance();
		c.setTime(startDate);
		int day1 = c.get(Calendar.DAY_OF_YEAR);
		c.setTime(endDate);
		int day2 = c.get(Calendar.DAY_OF_YEAR);
		int days = day2-day1 + 1;
		if (days == 1) {
			list.add(startDate);
			return list;
		} else {
			for (int i = 0; i < days ; i++) {
				if (i == 0) {
					list.add(startDate);
				} else if (i == days-1) {
					list.add(endDate);
				} else {
					list.add(getDayAfterDay(new java.sql.Date(startDate.getTime()), i, Calendar.DATE));
				}
			}
			return list;
		}
	}
	
	public static String getTomorrowDate() {
		String time = null;
		Date date = getDayAfterDay(getThisDate(), 1, Calendar.DATE);
		time = getTimeFromatString("yyyy-MM-dd", date);
		return time;
	}
	public static Timestamp quzheng(Timestamp time) {
		Timestamp t = null;
		Calendar c=Calendar.getInstance();
		c.setTime(new java.util.Date(time.getTime()));
		c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY)+1);
		t = new Timestamp(c.getTime().getTime());
		return t;
	}
}

