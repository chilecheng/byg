package com.hz.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 *
 */
public class ItemConst {

	/**
	 * 无
	 */
	public static final byte Wu_Sort = -1;
	/**
	 * 其他
	 */
	public static final byte QiTa_Sort = 1;
	/**
	 * 纸棺
	 */
	public static final byte ZhiGuan_Sort = 2;
	/**
	 * 礼仪
	 */
	public static final byte LiyiChubin_Sort = 3;
	
	/**
	 * 遗体整容
	 */
	public static final byte YiTiZhengRong_Sort = 4;

	/**
	 * 穿衣化妆
	 */
	public static final byte HuaZhuang_Sort = 5;

	/**
	 * 花圈花篮
	 */
	public static final byte HuaQuanHuaLan_Sort = 6;

	/**
	 * 骨灰盒
	 */
	public static final byte GuHuiHe_Sort = 7;

	/**
	 * 车费
	 */
	public static final byte CheFei_Sort = 8;

	/**
	 * 殡殓费
	 */
	public static final byte BinLianFei_Sort = 9;
	/**
	 * 接尸费
	 *//*
	public static final byte JieShiFei_Sort = 11;*/
	/**
	 * 冷藏
	 */
	public static final byte LengCang_Sort = 10;
	/**
	 * 火化
	 */
	public static final byte HuoHua_Sort = 11;
	/**
	 * 包布盒
	 */
	public static final byte BaoBuHe_Sort = 12;
	
	/**
	 * 寿被
	 */
	public static final byte ShouBei_Sort = 13;
	/**
	 * 寿衣
	 */
	public static final byte ShouYi_Sort = 14;
	/**
	 * 雨伞
	 */
	public static final byte YuSan_Sort = 15;
	/**
	 * 小花圈
	 */
	public static final byte XiaoHuaQuan_Sort =16; 
	/**
	 * 保护剂 
	 */
	public static final byte BaoHuJi_Sort = 17;
	/**
	 * 红孝带
	 */
	public static final byte HongXiaoDai_Sort = 18;
	/**
	 * 告别厅  围花
	 */
	public static final byte GaoBieTingWeiHua_Sort = 19;
	/**
	 *告别厅 黑纱
	 */

	public static final byte GaoBieTingHeiSha_Sort = 20;
	/**
	 * 告别厅 白纱
	 */
	public static final byte GaoBieTingBaiSha_Sort = 21;
/**
	 * 白纱
	 *//*
	public static final byte GaoBieTingBaiSha_Sort = 21;*/
	/**
	 *  告别厅 孝球
	 */
	public static final byte GaoBieTingXiaoQiu_Sort = 22;
	/**
	 * 告别厅 花门
	 */
	public static final byte GaoBieTingHuaMen_Sort = 23;
	/**
	 * 背景 这个应该 是告别厅背景
	 */
	public static final byte BeiJing_Sort = 24;
	/**
	 * 地毯
	 */
	public static final byte DiTan_Sort = 25;
	/**
	 * 背景绢花台
	 */
//	public static final byte JuanHuaTai_Sort = 27;
	/**
	 * 电子遗像
	 */
//	public static final byte DianZiYiXiang_Sort = 28;
	/**
	 * 摄像
	 */
//	public static final byte SheXiang_Sort = 29;
	/**
	 * 其他
	 */
	public static final byte QITa_Sort = 30;
	/**
	 * 对联
	 */
	public static final byte DuiLian_Sort = 31;
	/**
	 * 灵堂围花
	 */
	public static final byte LingTangWeiHua_Sort = 32;
	/**
	 * 灵堂黑纱
	 */
	public static final byte LingTangHeiSha_Sort = 33;
	/**
	 * 灵堂花门
	 */
	public static final byte LingTangHuaMen_Sort = 34;
	/**
	 * 穿衣化妆
	 */
	public static final byte ChuanYiHuaZhuang_Sort = 35;
	/**
	 * 整容
	 */
	public static final byte ZhengRong_Sort = 36;
	/**
	 * 灵堂背景
	 */
	public static final byte LingTangBeiJing = 37;
	/**
	 * 灵堂白纱
	 */
	public static final byte LingTangBaiSha= 38;
	/**
	 * 获取详细分类
	 * @param sort
	 * @return
	 */
	public static String getSortName(byte sort){
		if(sort==ItemConst.ZhiGuan_Sort){
			return "纸棺";
		}else if(sort==ItemConst.Wu_Sort){
			return "无";
		}else if(sort==ItemConst.LiyiChubin_Sort){
			return "礼仪出殡";
		}else if(sort==ItemConst.QiTa_Sort){
			return "其他";
		}else if(sort==ItemConst.YiTiZhengRong_Sort){
			return "遗体整容";
		}else if(sort==ItemConst.HuaZhuang_Sort){
			return "穿衣化妆";
		}else if(sort==ItemConst.LengCang_Sort){
			return "冷藏收费";
		}else if(sort==ItemConst.HuaQuanHuaLan_Sort){
			return "花圈花篮";
		}else if(sort==ItemConst.GuHuiHe_Sort){
			return "骨灰盒";
		}else if(sort==ItemConst.CheFei_Sort){
			return "车辆收费";
		}else if(sort==ItemConst.HuoHua_Sort){
			return "火化收费";
		}else if(sort==ItemConst.BinLianFei_Sort){
			return "殡殓费";
		}else if(sort==ItemConst.BaoBuHe_Sort) {
			return "包布盒";
		}else if(sort==ItemConst.ShouBei_Sort) {
			return "寿被";
		}else if(sort==ItemConst.ShouYi_Sort) {
			return "寿衣";
		}else if(sort==ItemConst.YuSan_Sort) {
			return "雨伞";
		}else if( sort == ItemConst.LengCang_Sort ){
			return "冷藏";
		}
		else if( sort == ItemConst.XiaoHuaQuan_Sort ){
			return "小花圈";
		}
		else if( sort == ItemConst.BaoHuJi_Sort ){
			return "保护剂";
		}
		else if( sort == ItemConst.HongXiaoDai_Sort ){
			return "红孝带";
		}
		else if( sort == ItemConst.GaoBieTingBaiSha_Sort ){
			return "告别厅白纱";
		}
		else if( sort == ItemConst.GaoBieTingHeiSha_Sort ){
			return "告别厅黑纱";
		}
		else if( sort == ItemConst.GaoBieTingHuaMen_Sort ){
			return "告别厅花门";
		}
		else if( sort == ItemConst.GaoBieTingWeiHua_Sort ){
			return "告别厅围花";
		}
		else if( sort == ItemConst.GaoBieTingXiaoQiu_Sort){
			return "告别厅孝球";
		}else if( sort == ItemConst.BeiJing_Sort){
			return "告别厅背景";
		}
		else if( sort == ItemConst.DuiLian_Sort ){
			return "对联";
		}
		else if( sort == ItemConst.LingTangHeiSha_Sort ){
			return "灵堂黑纱";
		}
		else if( sort == ItemConst.LingTangHuaMen_Sort ){
			return "灵堂花门";
		}
		else if( sort == ItemConst.LingTangWeiHua_Sort ){
			return "灵堂围花";
		}
		else if( sort == ItemConst.ChuanYiHuaZhuang_Sort ){
			return "穿衣化妆";
		}
		else if(sort ==ItemConst.LingTangBeiJing){
			return "灵堂背景";
		}
		else if( sort == ItemConst.ZhengRong_Sort ){
			return "整容";
		}
		else if(sort ==ItemConst.LingTangBaiSha){
			return "灵堂白纱";
		}
		else if(sort==ItemConst.GaoBieTingWeiHua_Sort) {
			return "告别厅围花";
		}else if(sort==ItemConst.GaoBieTingHuaMen_Sort) {
			return "告别厅花门";
		}else if(sort==ItemConst.DiTan_Sort) {
			return "地毯";
		}else{
			return "错误";
		}
	}
	
	/**
	 * 获取收费项目详细分类Option
	 * @param sort
	 * @param isAll	(true--"全部"　false--"没有全部")
	 * @return
	 */
	public static String getSortOption(byte type,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		list.add(new Object[]{ItemConst.Wu_Sort,"无"});
		list.add(new Object[]{ItemConst.QiTa_Sort,"其他"});
		list.add(new Object[]{ItemConst.ZhiGuan_Sort,"纸棺"});
		list.add(new Object[]{ItemConst.LiyiChubin_Sort,"礼仪出殡"});
		list.add(new Object[]{ItemConst.YiTiZhengRong_Sort,"遗体整容"});
		list.add(new Object[]{ItemConst.HuaZhuang_Sort,"穿衣化妆"});
		list.add(new Object[]{ItemConst.HuaQuanHuaLan_Sort,"花圈花篮"});
		list.add(new Object[]{ItemConst.GuHuiHe_Sort,"骨灰盒"});
		list.add(new Object[]{ItemConst.CheFei_Sort,"车费"});
		list.add(new Object[]{ItemConst.HuoHua_Sort,"火化收费"});
		list.add(new Object[]{ItemConst.BinLianFei_Sort,"殡殓费"});
		list.add(new Object[]{ItemConst.BaoBuHe_Sort,"包布盒"});
		list.add(new Object[]{ItemConst.ShouBei_Sort,"寿被"});
		list.add(new Object[]{ItemConst.ShouYi_Sort,"寿衣"});
		list.add(new Object[]{ItemConst.YuSan_Sort,"雨伞"});
		list.add(new Object[]{ItemConst.LengCang_Sort,"冷藏收费"});
		list.add(new Object[]{ItemConst.XiaoHuaQuan_Sort,"小花圈"});
		list.add(new Object[]{ItemConst.BaoHuJi_Sort,"保护剂"});
		list.add(new Object[]{ItemConst.HongXiaoDai_Sort,"红孝带"});
		list.add(new Object[]{ItemConst.GaoBieTingWeiHua_Sort,"告别厅围花"});
		list.add(new Object[]{ItemConst.GaoBieTingHeiSha_Sort,"告别厅 黑纱"});
		list.add(new Object[]{ItemConst.GaoBieTingBaiSha_Sort ,"告别厅白纱"});
		list.add(new Object[]{ItemConst.GaoBieTingXiaoQiu_Sort ,"告别厅孝球"});
		list.add(new Object[]{ItemConst.GaoBieTingHuaMen_Sort ,"告别厅花门"});
		list.add(new Object[]{ItemConst.DuiLian_Sort ,"对联"});
		list.add(new Object[]{ItemConst.LingTangHeiSha_Sort ,"灵堂黑纱"});
		list.add(new Object[]{ItemConst.LingTangHuaMen_Sort ,"灵堂花门"});
		list.add(new Object[]{ItemConst.LingTangWeiHua_Sort ,"灵堂围花"});
		list.add(new Object[]{ItemConst.ChuanYiHuaZhuang_Sort ,"穿衣化妆"});
		list.add(new Object[]{ItemConst.ZhengRong_Sort ,"整容"});
		list.add(new Object[]{ItemConst.GaoBieTingWeiHua_Sort,"告别厅围花"});
		list.add(new Object[]{ItemConst.GaoBieTingHuaMen_Sort,"告别厅花门"});
		list.add(new Object[]{ItemConst.BeiJing_Sort ,"告别厅背景"});
		list.add(new Object[]{ItemConst.DiTan_Sort ,"地毯"});
		list.add(new Object[]{ItemConst.LingTangBeiJing,"灵堂背景"});
		String option = DataTools.getOptionByList(list, type+"", isAll);
		return option;
	}
}
