package com.hz.util;

/**
 * 项目类型
 */
public class ItemTypeConst {
	/**
	 * 花圈花篮
	 */
	public static final String HQHL = "";
	/**
	 * 骨灰盒
	 */
	public static final String GHH = "";
	/**
	 * 丧葬用品
	 */
	public static final String SZYP = "SZYP";
	/**
	 * 纸棺
	 */
	public static final String ZG = "ZG";
	/**
	 * 火化收费
	 */
	public static final String HHSF = "HHSF";
	/**
	 * 告别厅收费
	 */
	public static final String GBTSF = "GBTSF";
	/**
	 * 特约加班费
	 */
	public static final String TYJBF = "TYJBF";
	/**
	 * 医院收费
	 */
	public static final String YYSF = "YYSF";
	/**
	 * 其他收费
	 */
	public static final String QTSF = "QTSF";
	/**
	 * 遗体处理
	 */
	public static final String YTCL = "YTCL";
	/**
	 * 车辆收费项目
	 */
	public static final String CLSFXM = "CLSFXM";
	/**
	 * 冷藏收费
	 */
	public static final String LCSF = "LCSF";
	/**
	 * 灵堂收费
	 */
	public static final String LTSF = "LTSF";
	
	
	/**
	 * 普通化妆 
	 */
	public static final String PTHZ = "409";
	/**
	 * 大白花
	 */
	public static final String DBH = "686";
	/**
	 * 灵堂不锈钢花门 
	 */
	public static final String LINGTANGHM = "527";
	/**
	 * 宫殿3060
	 */
	public static final String GD3060 = "824";
	/**
	 * 天堂2710
	 */
	public static final String TT2710 = "822";
	/**
	 * 纸花圈40
	 */
	public static final String ZHQ40 = "642";
	/**
	 * 遗体药物防腐处理费
	 */
	public static final String YTYWFFCLF = "406";
	/**
	 * 灵堂白纱布置
	 */
	public static final String LTBSBZ = "548";
	/**
	 * 礼厅不锈钢花门
	 */
	public static final String LITINGHM = "524";
	/**
	 * 灵堂挽联
	 */
	public static final String LITANGWL = "538";
	/**
	 * 儿童遗体火化费
	 */
	public static final String ETYTHHF = "302";
	/**
	 * 中高档灵堂出租
	 */
	public static final String ZGDLTCZ = "502";
	/**
	 * 背景绢花台
	 */
	public static final String BJJHT = "540";
	/**
	 * 灵堂鲜花门
	 */
	public static final String LTXHM = "525";
	/**
	 * 圣恩6260
	 */
	public static final String SN6260 = "832";
	/**
	 * 骨灰保护盒
	 */
	public static final String GHBHH = "637";
	/**
	 * 纳骨送灵
	 */
	public static final String NGSL = "583";
	/**
	 * 十字架6000
	 */
	public static final String SZJ6000 = "829";
	/**
	 * 福寿永享6180
	 */
	public static final String FSYX6180 = "831";
	/**
	 * 告别厅黑球布置
	 */
	public static final String GBTHQBZ = "544";
	/**
	 * 礼厅挽联
	 */
	public static final String LITINGWL = "535";
	/**
	 * 礼仪出殡
	 */
	public static final String LYCB = "508";
	/**
	 * 特殊遗体消毒防腐处理费100
	 */
	public static final String TSYTXD100 = "427";
	/**
	 * 八仙宫长72000
	 */
	public static final String BXGC72000 = "834";
	/**
	 * 特约服务费
	 */
	public static final String TYFWF = "901";
	/**
	 * 2号被面
	 */
	public static final String EHBM = "678";
	/**
	 * 灵堂鲜围花1000
	 */
	public static final String LTXWH1000 = "518";
	/**
	 * 绢花篮50
	 */
	public static final String QHL50 = "645";
	/**
	 * 普通冷藏费（冰室冷藏）
	 */
	public static final String PTLCF = "403";
	/**
	 * 灵堂白球布置
	 */
	public static final String LTBQBZ = "547";
	/**
	 * 3号寿衣
	 */
	public static final String SHSY = "676";
	/**
	 * 五福棒寿6370
	 */
	public static final String WFBS6370 = "833";
	/**
	 * 刑场整理费
	 */
	public static final String XCZLF = "537";
	/**
	 * 遗体整容费
	 */
	public static final String YTZRF = "412";
	/**
	 * 传统型甲
	 */
	public static final String CTXJ = "666";
	/**
	 * 小白花
	 */
	public static final String XBH = "687";
	/**
	 * 现代型
	 */
	public static final String XDX = "664";
	/**
	 * 将军阁3950
	 */
	public static final String JJG3950 = "827";
	/**
	 * 休息室 
	 */
	public static final String XXS = "419";
	/**
	 * 礼厅空调费
	 */
	public static final String LTKTF = "534";
	/**
	 * 告别厅小厅 
	 */
	public static final String GBTXT = "418";
	/**
	 * 骨灰处理费
	 */
	public static final String GHCLF = "425";
	/**
	 * 灵堂干花围花200 
	 */
	public static final String LTGHWH200 = "519";
	/**
	 * 鸿雁西归（中厅背景880） 
	 */
	public static final String HYXG880 = "588";
	/**
	 * 装尸袋 
	 */
	public static final String ZSD = "699";
	/**
	 * 绢花圈10 
	 */
	public static final String JHQ10 = "641";
	/**
	 * 花篮租用费 
	 */
	public static final String HLZYF = "422";
	/**
	 * 礼厅鲜围花3500
	 */
	public static final String LTXWH3500 = "514";
	/**
	 * 富贵型 
	 */
	public static final String FGX = "663";
	/**
	 * 1号寿被
	 */
	public static final String YHSB = "671";
	/**
	 * 别克灵车
	 */
	public static final String BKLC = "510";
	/**
	 * 经济型
	 */
	public static final String JJX = "661";
	/**
	 * 福寿双全10000
	 */
	public static final String FSSQ10000 = "836";
	/**
	 * 死胎火化费
	 */
	public static final String STHHF = "303";
	
	/**
	 * 灵堂住宿费
	 */
	public static final String LTZSF = "504";
	/**
	 * 唐装
	 */
	public static final String TZ = "695";
	/**
	 * 袖套
	 */
	public static final String XT = "688";
	/**
	 * 遗体火化观瞻专用厅
	 */
	public static final String YTHHGZZYT = "420";
	/**
	 * 豪华型
	 */
	public static final String HHX = "662";
	/**
	 * 冷柜楼层费
	 */
	public static final String LGLCF = "506";
	/**
	 * 2号包布盒
	 */
	public static final String EHBBH = "640";
	/**
	 * 骨灰寄存费
	 */
	public static final String GHJCFX = "423";
	/**
	 * 祈福6050
	 */
	public static final String QF6050 = "830";
	/**
	 * 鲜花篮180
	 */
	public static final String XHL180 = "649";
	/**
	 * 1号包布盒
	 */
	public static final String YHBBH = "639";
	/**
	 * 红毛巾
	 */
	public static final String HMJ = "680";
	/**
	 * 雨伞8
	 */
	public static final String YS8 = "694";
	/**
	 * 袜子
	 */
	public static final String WZ = "685";
	/**
	 * 手捧花80
	 */
	public static final String SPH80 = "655";
	/**
	 * 遗体抽腹水费
	 */
	public static final String YTCFSF = "414";
	/**
	 *  3号寿被
	 */
	public static final String SB3 = "673";
	/**
	 * 水晶棺2000
	 */
	public static final String SJG2000 = "819";
	/**
	 * 医院肢体截肢火化费
	 */
	public static final String YYZTJZHHF = "308";
	/**
	 * 补卡费
	 */
	public static final String BKF = "543";
	/**
	 * 普通车接尸费
	 */
	public static final String PTCJSF = "304";
	/**
	 * 成人遗体火化费
	 */
	public static final String CRYTHHF = "301";
	/**
	 * 龙凤盒9600
	 */
	public static final String LFH = "9600";
	/**
	 * 代写讣告
	 */
	public static final String DXFG = "536";
	/**
	 * 告别厅白纱布置
	 */
	public static final String GBTBSBZ = "586";
	/**
	 * 罗马柱鲜花篮380
	 */
	public static final String LMZXHL380 = "656";
	/**
	 * 抬尸费
	 */
	public static final String TSF = "305";
	/**
	 * 告别厅中厅
	 */
	public static final String GBTZT = "417";
	/**
	 * 礼厅干花围花500
	 */
	public static final String LTGWH500 = "517";
	/**
	 * 人情条(纸)
	 */
	public static final String RQTZ = "719";
	/**
	 * 状元阁16000
	 */
	public static final String ZYG16000 = "840";
	/**
	 * 不锈钢花圈80
	 */
	public static final String BXGHQ80 = "667";
	/**
	 * 告别厅黑纱布置
	 */
	public static final String GBTHSBZ = "532";
	/**
	 * 独立单独冷藏费
	 */
	public static final String DLDDLCF = "404";
	/**
	 * 驿骆梅花（围花1800）
	 */
	public static final String YLMH = "589";
	/**
	 * 2号寿衣
	 */
	public static final String SY2 = "675";
	/**
	 * 水晶棺冷藏
	 */
	public static final String SJGLC = "402";
	/**
	 * 骨灰运送费
	 */
	public static final String GHYSF = "424";
	/**
	 * 电子遗像
	 */
	public static final String DZYX = "581";
	/**
	 * 遗体穿戴更衣费
	 */
	public static final String YTCDGYF = "411";
	/**
	 * 小花圈
	 */
	public static final String XHQ = "638";
	/**
	 * 外运冰柜费
	 */
	public static final String WYBGF = "405";
	/**
	 * 高档灵车
	 */
	public static final String GDLC = "509";
	/**
	 * 寿帽
	 */
	public static final String SM = "683";
	/**
	 * 万福1500
	 */
	public static final String WF1500= "816";
	/**
	 * 礼厅鲜围花10000
	 */
	public static final String LTXWH10000 = "516";
	/**
	 * 财宝源1700
	 */
	public static final String CBY1700 = "817";
	/**
	 * 乐队
	 */
	public static final String YD = "502";
	/**
	 * 礼厅鲜围花5000
	 */
	public static final String LTXWH = "5000";
	
	
	
	//35-68
	/**
	 * 洗衣粉
	 */
	public static final String XYF = "698";
	/**
	 * 小花篮40
	 */
	public static final String XHL40 = "646";
	/**
	 * 灵堂干花围花500
	 */
	public static final String LTGHWH500 = "520";
	/**
	 * 寿鞋
	 */
	public static final String SX = "682";
	/**
	 * 冷柜接运费
	 */
	public static final String LGJYF = "505";
	/**
	 * 灵堂黑纱布置
	 */
	public static final String LTHSBZ = "539";
	/**
	 * 告别厅白球布置
	 */
	public static final String GBTBQBZ = "545";
	/**
	 * 人情条(人代写)
	 */
	public static final String RQTRDX = "690";
	/**
	 * 涛声依旧（大厅背景2180）
	 */
	public static final String TSYJDTBJ2180 = "587";
	/**
	 * 大富贵11800
	 */
	public static final String DFG11800 = "839";
	/**
	 * 鲜花篮280
	 */
	public static final String XHL280 = "650";
	/**
	 * 扇子
	 */
	public static final String SZ = "684";
	/**
	 * 小花篮60
	 */
	public static final String XHL60 = "647";
	/**
	 * 豪华灵堂出租
	 */
	public static final String HHLTCZ = "503";
	/**
	 * 医院成人遗体火化费
	 */
	public static final String YYCRYTHHF = "306";
	/**
	 * 代代平安11200
	 */
	public static final String DDPA11200 = "838";
	/**
	 * 鲜花柱900
	 */
	public static final String XHZ900 = "651";
	/**
	 * 告别厅地毯布置
	 */
	public static final String GBTDTBZ = "531";
	/**
	 * 骨灰盒减免费
	 */
	public static final String GHHJMF = "580";
	/**
	 * 红孝袋
	 */
	public static final String HXD = "696";
	/**
	 * 礼厅司仪
	 */
	public static final String LTSY = "507";
	/**
	 * 枕头脚垫
	 */
	public static final String ZTJD = "681";
	/**
	 * 1号被面
	 */
	public static final String BM1H = "677";
	/**
	 * 礼厅鲜花门3000
	 */
	public static final String LTXHM3000 = "522";
	/**
	 * 礼厅鲜围花1500
	 */
	public static final String LTXWH1500 = "513";
	/**
	 * 遗体清洗整理费
	 */
	public static final String YTQXZLF = "410";
	/**
	 * 特殊遗体消毒防腐处理费200
	 */
	public static final String TSYTXDFFCLF200 = "407";
	/**
	 * 礼厅干花花门
	 */
	public static final String LTGHHM = "523";
	/**
	 * 绢花牌380
	 */
	public static final String JHP380 = "653";
	/**
	 * 雨伞10
	 */
	public static final String YS10 = "693";
	/**
	 * 代写圤告
	 */
	public static final String DXPG = "700";
	/**
	 * 灵堂干花门
	 */
	public static final String LTGHM = "526";
	/**
	 * 鲜花圈1380
	 */
	public static final String XHQ1380 = "644";
	/**
	 * 殡葬礼仪摄像
	 */
	public static final String BZLYSX = "584";
}
