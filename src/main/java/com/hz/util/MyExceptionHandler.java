package com.hz.util;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * 异常处理
 * @author ljx
 *
 */
public class MyExceptionHandler implements HandlerExceptionResolver {  

    private static Logger log = LoggerFactory.getLogger(MyExceptionHandler.class); 
	    public ModelAndView resolveException(HttpServletRequest request,
	        HttpServletResponse response, Object handler, Exception ex) {
	    	log.error(new Date() + "异常信息:", ex);
			Map<String,Object> model=new HashMap<String,Object>();
			model.put("message", ex.getMessage());
	    if (ex instanceof NumberFormatException) {
	        return new ModelAndView("exception/exception",model);
	    } else if (ex instanceof NullPointerException) {
	        return new ModelAndView("exception/exception",model);
	    } else if (ex instanceof SocketTimeoutException
	        || ex instanceof ConnectException) {
	        try {
	        	response.getWriter().write("网络异常");
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
	        return new ModelAndView("exception/exception",model);
	    }
	    return new ModelAndView("exception/exception",model);
	    }
} 