package com.hz.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 常量类
 */
public class Const {
    /**
     * 公墓死者死亡类别：寿
     */
    public static final byte GMDEATH_SHO = 1;
    /**
     * 公墓死者死亡类别：故
     */
    public static final byte GMDEATH_GU = 2;
    /**
     * 公墓收费类别：雕刻安放费
     */
    public static final byte GMCHARGE_DKAFF = 1;
    /**
     * 公墓收费类别：墓穴费
     */
    public static final byte GMCHARGE_MXF = 2;
    /**
     * 公墓收费类别：维护费
     */
    public static final byte GMCHARGE_WHF = 3;
    /**
     * 公墓收费类别：迁移费
     */
    public static final byte GMCHARGE_QYF = 4;
    /**
     * 公墓收费类别：雕刻费
     */
    public static final byte GMCHARGE_DKF = 5;
    /**
     * 公墓收费类别：维修费
     */
    public static final byte GMCHARGE_WXF = 6;

    /**
     * 公墓维修状态：未维修
     */
    public static final byte GMTYPE_NO = 2;
    /**
     * 公墓维修状态：已维修
     */
    public static final byte GMTYPE_YES = 1;
    /**
     * 公墓：空闲
     */
    public static final byte GM_KONG = 1;
    /**
     * 公墓：已售出
     */
    public static final byte GM_SOU = 2;
    /**
     * 公墓：维修
     */
    public static final byte GM_WEI = 3;
    /**
     * 公墓：迁出
     */
    public static final byte GM_QIAN = 4;
    /**
     * 公墓：维护过期
     */
    public static final byte GM_GUO = 5;
    /**
     * 公墓：预定
     */
    public static final byte GM_YUDING = 6;
    /**
     * 公墓：已使用
     */
    public static final byte GM_SHIYONG = 7;
    public static final String COOKIE_USER = "cookieUser";

    public static final String SESSION_USER = "sessionUser";

    /**
     * 没查看过  byte 0
     */
    public static final byte Zero = 0;
    /**
     * 已查看
     */
    public static final byte One = 1;
    /**
     * 身份证
     */
    public static final byte Card_Certificate = 1;
    /**
     * 军人证
     */
    public static final byte Card_Military = 2;
    /**
     * 护照
     */
    public static final byte Card_Passport = 3;


    /**
     * 已审核
     */
    public static final byte Check_Yes = 1;
    /**
     * 未审核
     */
    public static final byte Check_No = 2;
    /**
     * 被取消
     */
    public static final byte Check_Esc = 3;
    /**
     * 已交费
     */
    public static final byte Pay_Yes = 1;
    /**
     * 未交费
     */
    public static final byte Pay_No = 2;

    /**
     * 启用
     */
    public static final byte Isdel_No = 1;
    /**
     * 禁用
     */
    public static final byte Isdel_Yes = 2;
    /**
     * 未销卡
     */
    public static final byte pincard_No = 1;
    /**
     * 销卡
     */
    public static final byte pincard_Yes = 2;

    /**
     * 普通炉
     */
    public static final byte furnace_pt = 1;
    /**
     * 特约炉
     */
    public static final byte furnace_ty = 2;
    /**
     * 特殊业务
     */
    public static final byte furnace_ts = 3;

    /**
     * 占用
     */
    public static final byte IsFlag_Yse = 1;
    /**
     * 空闲
     */
    public static final byte IsFlag_No = 2;
    /**
     * 锁定
     */
    public static final byte IsFlag_Lock = 3;
    /**
     * 装修中
     */
    public static final byte IsFlag_Decrate = 4;
    /**
     * 完成
     */
    public static final byte IsFlag_Wc = 5;
    /**
     * 进行
     */
    public static final byte IsFlag_Jx = 6;
    /**
     * 布置完成
     */
    public static final byte IsFlag_Bzwc = 7;
    /**
     * 预约
     */
    public static final byte IsFlag_YY = 8;
    /**
     * 清场：灵堂出柜之后自动转为清场状态
     */
    public static final byte IsFlag_QC = 9;
    /**
     * 临时锁定:调度科洽谈状态
     */
    public static final byte IsFlag_LSSD = 11;

    /**
     * 是
     */
    public static final byte Is_Yes = 1;
    /**
     * 否
     */
    public static final byte Is_No = 2;

    /**
     * 已收
     */
    public static final byte IsHave_Yes = 1;
    /**
     * 未收
     */
    public static final byte IsHave_No = 2;

    /**
     * 已火化
     */
    public static final byte furnace_flag_Yes = 1;
    /**
     * 未火化
     */
    public static final byte furnace_flag_No = 2;

    /**
     * 完成
     */
    public static final byte IsComplete_Yes = 1;
    /**
     * 未完成
     */
    public static final byte IsComplete_No = 2;
    /**
     * 作废
     */
    public static final byte IsComplete_Del = 3;

    //委托单进度
    /**
     * 已到馆 未收费
     */
    public static final byte schedule_flag_1 = 1;
    /**
     * 收费结清
     */
    public static final byte schedule_flag_2 = 2;
    /**
     * 已火化未打印
     */
    public static final byte schedule_flag_3 = 3;
    /**
     * 已打印火化证明
     */
    public static final byte schedule_flag_4 = 4;
    /**
     * 男
     */
    public static final byte Sex_Fale = 1;
    /**
     * 女
     */
    public static final byte Sex_Female = 2;
    /**
     * 不详
     */
    public static final byte Sex_Unknown = 3;

    /**
     * 用品收费
     */
    public static final byte Type_Articles = 1;
    /**
     * 服务收费
     */
    public static final byte Type_Service = 2;
    /**
     * 告别厅收费
     */
    public static final String GBT_SERVICE = "告别厅收费";
    /**
     * 灵堂收费
     */
    public static final String LT_SERVICE = "灵堂收费";

    /**
     * 公安
     */
    public static final byte Prove_Police = 1;
    /**
     * 法院
     */
    public static final byte Prove_Court = 2;
    /**
     * 管理员
     */
    public static final Byte UserType_System = 1;
    /**
     * 工作人员
     */
    public static final Byte UserType_Staff = 2;
    /**
     * 游客
     */
    public static final Byte UserType_Visitor = 3;
    /**
     * 已处理
     */
    public static final Byte Yes_treated = 1;
    /**
     * 未处理
     */
    public static final Byte No_treated = 2;
    /**
     * 已领用
     */
    public static final Byte Yes_usered = 3;
    /**
     * 普通炉
     */
    public static final Byte fur_common = 1;
    /**
     * 特约炉
     */
    public static final Byte fur_special = 2;
    /**
     * 特殊业务
     */
    public static final Byte fur_teshu = 3;
    /**
     * 布置人员
     */
    public static final String ARRANGER = "一科科员";
    /**
     * 司仪人员
     */
    public static final String EMCEE = "司仪人员";
    /**
     * 驾驶员
     */
    public static final String DRIVER = "驾驶员";
    /**
     * 殡仪人员(二科接尸化妆)
     */
    public static final String FUNERAL1 = "二科接尸化妆";
    /**
     * 殡仪人员(二科卸尸抬尸)
     */
    public static final String FUNERAL2 = "二科卸尸抬尸";
    /**
     * 书写人
     */
    public static final String WRITER = "书写人";
    /**
     * 协助人员
     */
    public static final String HELPER = "协助人员";
    /**
     * 协助人员
     */
    public static final String HELPER1 = "二科卸尸抬尸";
    /**
     * 协助人员
     */
    public static final String HELPER2 = "二科接尸化妆";
    /**
     * 入柜人员
     */
    public static final String INTER = "入柜人员";
    /**
     * 灵堂相关
     */
    public static final String MOUR_HDR = "mour";
    /**
     * 告别厅相关
     */
    public static final String FARE_HDR = "fare";
    /**
     * 火化炉相关
     */
    public static final String FURNACE_HDR = "furnace";
    /**
     * 查询条件:姓名
     */
    public static final byte Search_Type_Name = 1;
    /**
     * 查询条件:卡号
     */
    public static final byte Search_Type_Card = 2;
    /**
     * 查询条件:灵堂号
     */
    public static final byte Search_Type_Mour = 3;
    /**
     * 查询条件:联系人姓名
     */
    public static final byte Search_Type_Fname = 4;
    /**
     * 查询条件:流水号
     */
    public static final byte Search_Type_Number = 5;
    /**
     * 查询条件:助记码
     */
    public static final byte Search_Type_HelpCode = 6;
    /**
     * 查询条件:项目名称
     */
    public static final byte Search_Type_ItemName = 7;
    /**
     * 查询条件:购买单号
     */
    public static final byte Search_Type_BuyNumber = 8;
    /**
     * 查询条件:支付方式
     */
    public static final byte Search_Type_PayType = 9;
    /**
     * 查询：冰柜号
     */
    public static final byte Search_Type_IceNumber = 10;
    /**
     * 查询：寄存编号
     */
    public static final byte Search_Type_RegisterNumber = 11;
    /**
     * 查询：火化人姓名
     */
    public static final byte Search_Type_DieName = 12;
    /**
     * 查询：寄存人姓名
     */
    public static final byte Search_Type_RegisterName = 13;
    /**
     * 查询：购买人
     */
    public static final byte Search_Type_BuyName = 14;
    /**
     * 查询：编号
     */
    public static final byte Search_Type_Code = 15;
    /**
     * 查询：存放位置
     */
    public static final byte Search_Type_RegisterAddr = 16;
    /**
     * 查询条件：地区
     */
    public static final byte Search_Type_Area = 17;
    /**
     * 查询条件：身份证号
     */
    public static final byte Search_Type_IDCard = 18;
    /**
     * 查询条件：告别厅号
     */
    public static final byte Search_Type_FareWell = 19;
    /**
     * 查询条件：挂账单位
     */
    public static final byte Search_Type_BillUnit = 20;
    /**
     * 查询条件:家属
     */
    public static final byte Search_Type_Family = 21;
    /**
     * 查询条件:墓位名称
     */
    public static final byte Search_Type_TombName = 22;
    /**
     * 查询条件:持证人姓名
     */
    public static final byte Search_Type_HolderName = 23;
    /**
     * 查询条件：发票号
     */
    public static final byte Search_Type_InvoiceNumber = 24;
    /**
     * 查询条件:墓穴费
     */
    public static final byte Search_Type_GraveFee = 25;

    /**
     * 车辆调度：已完成
     */
    public static final byte CarSFlag_OK = 1;
    /**
     * 车辆调度：未派车
     */
    public static final byte CarSFlag_NO = 2;
    /**
     * 车辆调度：已派车
     */
    public static final byte CarSFlag_GO = 3;
    /**
     * 车辆调度：维修
     */
    public static final byte CarSFlag_RP = 4;
    /**
     * 是转炉的
     */
    public static final byte ChangeFlag_Yes = 1;
    /**
     * 不是转 的
     */
    public static final byte ChangeFlag_No = 2;

    /**
     * 支付方式:现金
     */
    public static final byte PayType_Cash = 1;
    /**
     * 支付方式:刷卡
     */
    public static final byte PayType_Card = 2;
    /**
     * 支付方式:支付宝
     */
    public static final byte PayType_Alipay = 3;
    /**
     * 支付方式:挂账
     */
    public static final byte PayType_GuaZhan = 4;
    /**
     * 支付方式:代收
     */
    public static final byte PayType_DaiShou = 5;
    /**
     * 支付方式：转支
     */
    public static final byte PayType_ZhuanZhi = 6;
    //item tick_flag
    /**
     * 委托业务收费 flag
     */
    public static final byte TickFlag_Delegate = 2;
    /**
     * g挂账业务 收费项 flag
     */
    public static final byte TickFlag_Bill = 1;

    /**
     * 用于区分查询火化委托单的接口(只查所需数据，提高性能)
     * 调度科 火化委托单 列表
     */
    public static final byte GOTO1 = 1;
    /**
     * 用于区分查询火化委托单的接口(只查所需数据，提高性能)
     * 调度科 骨灰寄存的时候  搜索姓名、id用
     */
    public static final byte GOTO2 = 2;
    /**
     * 用于区分查询火化委托单的接口(只查所需数据，提高性能)
     * 花圈花篮购买页面/丧葬用品购买 点姓名搜索的弹出框
     */
    public static final byte GOTO3 = 3;
    /**
     * 用于区分查询火化委托单的接口(只查所需数据，提高性能)
     * 永嘉登记列表
     */
    public static final byte GOTO4 = 4;
    /**
     * 通知项目：告别厅任务单
     */
    public static final String NoticeType_FarewellTask = "farewell_task";
    /**
     * 通知项目：灵堂任务单
     */
    public static final String NoticeType_MourningTask = "mourning_task";
    /**
     * 通知项目：丧葬用品任务单
     */
    public static final String NoticeType_FuneralTask = "funeral_task";
    /**
     * 通知项目：花圈花篮统计单
     */
    public static final String NoticeType_BuyFlower = "buyFlower_task";
    /**
     * 通知项目：验尸解冻
     */
    public static final String NoticeType_YsjdTask = "ysjd_task";
    /**
     * 通知项目：遗体非验尸解冻
     * 正常程序解冻
     */
    public static final String NoticeType_FysytjdTask = "fysytjd_task";
    /**
     * 通知项目：车辆安排
     */
    public static final String NoticeType_CarTask = "car_task";

    /**
     * 获取处理类型
     *
     * @param type
     * @return
     */
    public static String getDealIsdelType(byte type) {
        if (type == Const.Yes_treated) {
            return "已处理";
        } else if (type == Const.No_treated) {
            return "未处理";
        } else if (type == Const.Yes_usered) {
            return "已领用";
        } else {
            return "错误";
        }

    }

    /**
     * 获取证件类型
     *
     * @param type
     * @return
     */
    public static String getCardType(byte type) {
        if (type == Const.Card_Certificate) {
            return "身份证";
        } else if (type == Const.Card_Military) {
            return "军人证";
        } else if (type == Const.Card_Passport) {
            return "护照";
        } else {
            return "错误";
        }
    }


    /**
     * 获取用户类型别名
     *
     * @param type
     * @return
     */
    public static String getUserType(byte type) {
        if (type == Const.UserType_System) {
            return "管理员";
        } else if (type == Const.UserType_Staff) {
            return "工作人员";
        } else if (type == Const.UserType_Visitor) {
            return "游客";
        } else {
            return "错误";
        }
    }

    /**
     * 获取用户类型Option
     *
     * @param type
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getUserTypeOption(byte type, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.UserType_System, "管理员"});
        list.add(new Object[]{Const.UserType_Staff, "工作人员"});
        list.add(new Object[]{Const.UserType_Visitor, "游客"});
        String option = DataTools.getOptionByList(list, type + "", isAll);
        return option;
    }

    /**
     * 获取性别名称
     *
     * @param sex
     * @return
     */
    public static String getSex(byte sex) {
        if (sex == Const.Sex_Fale) {
            return "男";
        } else if (sex == Const.Sex_Female) {
            return "女";
        } else if (sex == Const.Sex_Unknown) {
            return "不详";
        } else {
            return "未填写";
        }
    }

    /**
     * 显示 是否收费
     *
     * @return
     */
    public static String getIfPay(byte payFlag) {
        if (payFlag == Const.Pay_No) {
            return "未收费";
        } else if (payFlag == Const.Pay_Yes) {
            return "已收费";
        } else {
            return "错误";
        }

    }

    /**
     * 获取性别Option
     *
     * @param sex
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getSexOption(byte sex, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"", ""});
        list.add(new Object[]{Const.Sex_Fale, "男"});
        list.add(new Object[]{Const.Sex_Female, "女"});
        list.add(new Object[]{Const.Sex_Unknown, "不详"});
        String option = DataTools.getOptionByList(list, sex + "", isAll);
        return option;
    }

    /**
     * 获取性别readonly
     *
     * @param sex
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getSex(byte sex, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"", ""});
        list.add(new Object[]{Const.Sex_Fale, "男"});
        list.add(new Object[]{Const.Sex_Female, "女"});
        list.add(new Object[]{Const.Sex_Unknown, "不详"});
        String option = DataTools.getNameByList(list, sex + "", isAll);
        return option;
    }

    /**
     * 获取火化炉类型 option
     *
     * @param furance_flag
     * @param isAll
     * @return
     */
    public static String getFuranceTypeOption(byte furance_flag, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.fur_common, "普通炉"});
        list.add(new Object[]{Const.fur_special, "特约炉"});
        list.add(new Object[]{Const.fur_teshu, "特殊业务"});
        String option = DataTools.getOptionByList(list, furance_flag + "", isAll);
        return option;
    }

    /**
     * 获取支付方式Option
     *
     * @param type
     * @param isAll (true---"全部"  false---"没有全部")
     * @return
     */
    public static String getPayTypeOption(byte type, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.PayType_Cash, "现金"});
        list.add(new Object[]{Const.PayType_Card, "刷卡"});
        list.add(new Object[]{Const.PayType_Alipay, "支付宝"});
        list.add(new Object[]{Const.PayType_DaiShou, "代收"});
        list.add(new Object[]{Const.PayType_GuaZhan, "挂账"});
        list.add(new Object[]{Const.PayType_ZhuanZhi, "转支"});
        String option = DataTools.getOptionByList(list, type + "", isAll);
        return option;
    }

    /**
     * 获取支付方式的下拉框，传进来的参数从Const类里面找
     *
     * @param arr
     * @return
     */
    public static String getPayTypeOptionByByte(byte type, byte[] arr) {
        List<Object[]> list = new ArrayList<Object[]>();
        for (byte b : arr) {
            if (b == Const.PayType_Cash) {
                list.add(new Object[]{Const.PayType_Cash, "现金"});
            } else if (b == Const.PayType_Card) {
                list.add(new Object[]{Const.PayType_Card, "刷卡"});
            } else if (b == Const.PayType_Alipay) {
                list.add(new Object[]{Const.PayType_Alipay, "支付宝"});
            } else if (b == Const.PayType_DaiShou) {
                list.add(new Object[]{Const.PayType_DaiShou, "代收"});
            } else if (b == Const.PayType_GuaZhan) {
                list.add(new Object[]{Const.PayType_GuaZhan, "挂账"});
            } else if (b == Const.PayType_ZhuanZhi) {
                list.add(new Object[]{Const.PayType_ZhuanZhi, "转支"});
            }
        }
        String str = String.valueOf(type);
        String option = DataTools.getOptionByList(list, str, false);
        return option;
    }

    /**
     * 获取支付方式Option(只有支付宝方式)
     *
     * @param type
     * @param isAll (true---"全部"  false---"没有全部")
     * @return
     */
    public static String getAliPayOption(byte type, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.PayType_Alipay, "支付宝"});
        String option = DataTools.getOptionByList(list, type + "", isAll);
        return option;
    }


    /**
     * 获取是否占用名称
     *
     * @param flag
     * @return
     */
    public static String getIsFlag(byte flag) {
        if (flag == Const.IsFlag_Yse) {
            return "占用";
        } else if (flag == Const.IsFlag_No) {
            return "空闲";
        } else {
            return "错误";
        }
    }

    /**
     * 获取是否占用Option
     *
     * @param flag
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getIsFlagOption(byte flag, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.IsFlag_Yse, "占用"});
        list.add(new Object[]{Const.IsFlag_No, "空闲"});
        String option = DataTools.getOptionByList(list, flag + "", isAll);
        return option;
    }


    /**
     * 获取是否启用名称
     *
     * @param isdel
     * @return
     */
    public static String getIsdel(byte isdel) {
        if (isdel == Const.Isdel_No) {
            return "启用";
        } else if (isdel == Const.Isdel_Yes) {
            return "禁用";
        } else {
            return "错误";
        }
    }

    /**
     * 获取是否启用Option
     *
     * @param isdel
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getIsdelOption(byte isdel, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Isdel_No, "启用"});
        list.add(new Object[]{Const.Isdel_Yes, "禁用"});
        String option = DataTools.getOptionByList(list, isdel + "", isAll);
        return option;
    }

    /**
     * 获取排序Option  （从左到右，从右到左）
     *
     * @param isdel
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getSortOption(byte isdel, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Isdel_No, "从左到右"});
        list.add(new Object[]{Const.Isdel_Yes, "从右到左"});
        String option = DataTools.getOptionByList(list, isdel + "", isAll);
        return option;
    }

    /**
     * @param isdel
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getDealIsdelOption(byte isdel, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Yes_treated, "已处理"});
        list.add(new Object[]{Const.No_treated, "未处理"});
        list.add(new Object[]{Const.Yes_usered, "已领用"});
        String option = DataTools.getOptionByList(list, isdel + "", isAll);
        return option;
    }

    /**
     * 获取查询条件的下拉框，传进来的参数从Const类里面找
     *
     * @param arr
     * @return
     */
    public static String getSearchTypeOptionByByte(byte type, byte[] arr) {
        List<Object[]> list = new ArrayList<Object[]>();
        for (byte b : arr) {
            if (b == Const.Search_Type_Name) {
                list.add(new Object[]{Const.Search_Type_Name, "姓名"});
            } else if (b == Const.Search_Type_Card) {
                list.add(new Object[]{Const.Search_Type_Card, "卡号"});
            } else if (b == Const.Search_Type_Mour) {
                list.add(new Object[]{Const.Search_Type_Mour, "灵堂号"});
            } else if (b == Const.Search_Type_Fname) {
                list.add(new Object[]{Const.Search_Type_Fname, "联系人姓名"});
            } else if (b == Const.Search_Type_Number) {
                list.add(new Object[]{Const.Search_Type_Number, "流水号"});
            } else if (b == Const.Search_Type_HelpCode) {
                list.add(new Object[]{Const.Search_Type_HelpCode, "助记码"});
            } else if (b == Const.Search_Type_ItemName) {
                list.add(new Object[]{Const.Search_Type_ItemName, "项目名称"});
            } else if (b == Const.Search_Type_BuyNumber) {
                list.add(new Object[]{Const.Search_Type_BuyNumber, "购买单号"});
            } else if (b == Const.Search_Type_PayType) {
                list.add(new Object[]{Const.Search_Type_PayType, "支付方式"});
            }

        }
        String str = String.valueOf(type);
        String option = DataTools.getOptionByList(list, str, false);
        return option;
    }

    /**
     * 获取查询条件的下拉框，传进来的参数从Const类里面找
     *
     * @param arr
     * @return
     */
    public static String getSearchTypeOptionByString(String type, byte[] arr) {
        List<Object[]> list = new ArrayList<Object[]>();
        for (byte b : arr) {
            if (b == Const.Search_Type_Name) {
                list.add(new Object[]{Const.Search_Type_Name, "姓名"});
            } else if (b == Const.Search_Type_Card) {
                list.add(new Object[]{Const.Search_Type_Card, "卡号"});
            } else if (b == Const.Search_Type_Mour) {
                list.add(new Object[]{Const.Search_Type_Mour, "灵堂号"});
            } else if (b == Const.Search_Type_Fname) {
                list.add(new Object[]{Const.Search_Type_Fname, "联系人姓名"});
            } else if (b == Const.Search_Type_Number) {
                list.add(new Object[]{Const.Search_Type_Number, "流水号"});
            } else if (b == Const.Search_Type_HelpCode) {
                list.add(new Object[]{Const.Search_Type_HelpCode, "助记码"});
            } else if (b == Const.Search_Type_ItemName) {
                list.add(new Object[]{Const.Search_Type_ItemName, "项目名称"});
            } else if (b == Const.Search_Type_BuyNumber) {
                list.add(new Object[]{Const.Search_Type_BuyNumber, "购买单号"});
            } else if (b == Const.Search_Type_PayType) {
                list.add(new Object[]{Const.Search_Type_PayType, "支付方式"});
            } else if (b == Const.Search_Type_IceNumber) {
                list.add(new Object[]{Const.Search_Type_IceNumber, "冰柜号"});
            } else if (b == Const.Search_Type_RegisterNumber) {
                list.add(new Object[]{Const.Search_Type_RegisterNumber, "寄存编号"});
            } else if (b == Const.Search_Type_RegisterName) {
                list.add(new Object[]{Const.Search_Type_RegisterName, "寄存人姓名"});
            } else if (b == Const.Search_Type_DieName) {
                list.add(new Object[]{Const.Search_Type_DieName, "火化人姓名"});
            } else if (b == Const.Search_Type_BuyName) {
                list.add(new Object[]{Const.Search_Type_BuyName, "购买人"});
            } else if (b == Const.Search_Type_Code) {
                list.add(new Object[]{Const.Search_Type_Code, "编号"});
            } else if (b == Const.Search_Type_RegisterAddr) {
                list.add(new Object[]{Const.Search_Type_RegisterAddr, "存放位置"});
            } else if (b == Const.Search_Type_Area) {
                list.add(new Object[]{Const.Search_Type_Area, "地区"});
            } else if (b == Const.Search_Type_IDCard) {
                list.add(new Object[]{Const.Search_Type_IDCard, "身份证号"});
            } else if (b == Const.Search_Type_FareWell) {
                list.add(new Object[]{Const.Search_Type_FareWell, "告别厅号"});
            } else if (b == Const.Search_Type_BillUnit) {
                list.add(new Object[]{Const.Search_Type_BillUnit, "挂账单位"});
            } else if (b == Const.Search_Type_Family) {
                list.add(new Object[]{Const.Search_Type_Family, "家属"});
            } else if (b == Const.Search_Type_TombName) {
                list.add(new Object[]{Const.Search_Type_TombName, "墓位名称"});
            } else if (b == Const.Search_Type_HolderName) {
                list.add(new Object[]{Const.Search_Type_HolderName, "持证人姓名"});
            } else if (b == Const.Search_Type_InvoiceNumber) {
                list.add(new Object[]{Const.Search_Type_InvoiceNumber, "发票号"});
            } else if (b == Const.Search_Type_GraveFee) {
                list.add(new Object[]{Const.Search_Type_GraveFee, "墓穴费"});
            }
        }
        String option = DataTools.getOptionByList(list, type, false);
        return option;
    }

    /**
     * 获取查询条件的下拉框，传进来的参数从Const类里面找
     *
     * @param arr
     * @return
     */
    public static String getSearchTypeOption(byte[] arr) {
        List<Object[]> list = new ArrayList<Object[]>();
        for (byte b : arr) {
            if (b == Const.Search_Type_Name) {
                list.add(new Object[]{Const.Search_Type_Name, "姓名"});
            } else if (b == Const.Search_Type_Card) {
                list.add(new Object[]{Const.Search_Type_Card, "卡号"});
            } else if (b == Const.Search_Type_Mour) {
                list.add(new Object[]{Const.Search_Type_Mour, "灵堂号"});
            } else if (b == Const.Search_Type_Fname) {
                list.add(new Object[]{Const.Search_Type_Fname, "联系人姓名"});
            } else if (b == Const.Search_Type_Number) {
                list.add(new Object[]{Const.Search_Type_Number, "流水号"});
            } else if (b == Const.Search_Type_HelpCode) {
                list.add(new Object[]{Const.Search_Type_HelpCode, "助记码"});
            } else if (b == Const.Search_Type_ItemName) {
                list.add(new Object[]{Const.Search_Type_ItemName, "项目名称"});
            } else if (b == Const.Search_Type_BuyNumber) {
                list.add(new Object[]{Const.Search_Type_BuyNumber, "购买单号"});
            } else if (b == Const.Search_Type_PayType) {
                list.add(new Object[]{Const.Search_Type_PayType, "支付方式"});
            }

        }
        String option = DataTools.getOptionByList(list, null, false);
        return option;
    }


    /**
     * 获取收费项目类型名称
     *
     * @param itemType
     * @return
     */
    public static String getItemType(byte itemType) {
        if (itemType == Const.Type_Articles) {
            return "用品收费";
        } else if (itemType == Const.Type_Service) {
            return "服务收费";
        } else {
            return "错误";
        }
    }

    /**
     * 获取收费项目类型Option
     *
     * @param itemType
     * @param isAll    (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getItemTypeOption(byte itemType, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Type_Articles, "用品收费"});
        list.add(new Object[]{Const.Type_Service, "服务收费"});
        String option = DataTools.getOptionByList(list, itemType + "", isAll);
        return option;
    }


    /**
     * 获取证明单位类型名称
     *
     * @param proveType
     * @return
     */
    public static String getProveType(byte proveType) {
        if (proveType == Const.Prove_Court) {
            return "法院";
        } else if (proveType == Const.Prove_Police) {
            return "公安";
        } else {
            return "错误";
        }
    }

    /**
     * 获取收费项目类型Option
     *
     * @param ProveType
     * @param isAll     (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getProveTypeOption(byte ProveType, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Prove_Police, "公安"});
        list.add(new Object[]{Const.Prove_Court, "法院"});
        String option = DataTools.getOptionByList(list, ProveType + "", isAll);
        return option;
    }

    /**
     * 获取收费名称
     *
     * @param payFlag
     * @return
     */
    public static String getPayFlagType(byte payFlag) {
        if (payFlag == Const.Pay_No) {
            return "未收费";
        } else if (payFlag == Const.Pay_Yes) {
            return "已收费";
        } else {
            return "错误";
        }
    }

    /**
     * 获取支付方式名称
     *
     * @param payType
     * @return
     */
    public static String getPayType(byte payType) {
        if (payType == Const.PayType_Cash) {
            return "现金";
        } else if (payType == Const.PayType_Card) {
            return "刷卡";
        } else if (payType == Const.PayType_Alipay) {
            return "支付宝";
        } else {
            return "错误";
        }
    }


    /**
     * 获取‘是否’名称
     *
     * @param is
     * @return
     */
    public static String getIs(byte is) {
        if (is == Const.Is_Yes) {
            return "是";
        } else if (is == Const.Is_No) {
            return "否";
        } else {
            return "错误";
        }
    }

    /**
     * 获取‘是否’Option
     *
     * @param is
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getIsOption(byte is, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Is_No, "否"});
        list.add(new Object[]{Const.Is_Yes, "是"});
        String option = DataTools.getOptionByList(list, is + "", isAll);
        return option;
    }

    /**
     * 获取‘是否’readOnly
     *
     * @param is
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getIs(byte is, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Is_No, "否"});
        list.add(new Object[]{Const.Is_Yes, "是"});
        String option = DataTools.getNameByList(list, is + "", isAll);
        return option;
    }


    /**
     * 获取是否拥有名称
     *
     * @param isHave
     * @return
     */
    public static String getIsHave(byte isHave) {
        if (isHave == Const.IsHave_Yes) {
            return "已收";
        } else if (isHave == Const.IsHave_No) {
            return "未收";
        } else {
            return "错误";
        }
    }

    /**
     * 获取是否拥有Option
     *
     * @param isHave
     * @param isAll  (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getIsHaveOption(byte isHave, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.IsHave_Yes, "已收"});
        list.add(new Object[]{Const.IsHave_No, "未收"});
        String option = DataTools.getOptionByList(list, isHave + "", isAll);
        return option;
    }

    /**
     * 获取是否拥有readonly
     *
     * @param isHave
     * @param isAll  (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getIsHave(byte isHave, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.IsHave_Yes, "已收"});
        list.add(new Object[]{Const.IsHave_No, "未收"});
        String option = DataTools.getNameByList(list, isHave + "", isAll);
        return option;
    }


    /**
     * 获取是否完成名称
     *
     * @param isComplete
     * @return
     */
    public static String getIsComplete(byte isComplete) {
        if (isComplete == Const.IsComplete_Yes) {
            return "完成";
        } else if (isComplete == Const.IsComplete_No) {
            return "未完成";
        } else if (isComplete == Const.IsComplete_Del) {
            return "作废";
        } else {
            return "错误";
        }
    }

    /**
     * 获取是否完成Option
     *
     * @param isComplete
     * @param isAll      (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getIsCompleteOption(byte isComplete, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.IsComplete_Yes, "完成"});
        list.add(new Object[]{Const.IsComplete_No, "未完成"});
        list.add(new Object[]{Const.IsComplete_Del, "作废"});
        String option = DataTools.getOptionByList(list, isComplete + "", isAll);
        return option;
    }

    /**
     * 获取是否审核名称
     *
     * @param checkFlag
     * @return
     */
    public static String getCheckFlag(byte checkFlag) {
        if (checkFlag == Const.Check_Yes) {
            return "已审核";
        } else if (checkFlag == Const.Check_No) {
            return "未审核";
        } else if (checkFlag == Const.Check_Esc) {
            return "被取消";
        } else {
            return "错误";
        }
    }

    /**
     * 获取是否审核Option
     *
     * @param checkFlag
     * @param isAll     (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getCheckFlagOption(byte checkFlag, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.Check_Yes, "已审核"});
        list.add(new Object[]{Const.Check_No, "未审核"});
        list.add(new Object[]{Const.Check_Esc, "被取消"});
        String option = DataTools.getOptionByList(list, checkFlag + "", isAll);
        return option;
    }

    /**
     * 获取服务项目类型
     *
     * @param para
     * @return
     */
    public static String getItemType(String para) {
        if ("mour".equals(para)) {
            return Const.LT_SERVICE;
        } else if ("fare".equals(para)) {
            return Const.GBT_SERVICE;
        } else {
            return null;
        }
    }


    /**
     * 获取服务项目对应的布置人员组
     *
     * @param para
     * @return
     */
    public static String getItemUserGroup(String para) {
        if (Const.MOUR_HDR.equals(para)) {
            return "灵堂布置人员";
        } else if (Const.FARE_HDR.equals(para)) {
            return "告别厅布置人员";
        } else {
            return null;
        }
    }

    public static byte getRepairIndex(String type) {
        byte b = 0;
        if (Const.FURNACE_HDR.equals(type)) {
            b = 3;
        } else if (Const.FARE_HDR.equals(type)) {
            b = 2;
        } else if (Const.MOUR_HDR.equals(type)) {
            b = 1;
        }
        return b;
    }

    /**
     * 获取火化炉List
     *
     * @return
     */
    public static List<Object[]> getFurnaceList() {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.furnace_pt, "普通炉"});
        list.add(new Object[]{Const.furnace_ty, "特约炉"});
        list.add(new Object[]{Const.furnace_ts, "特殊业务"});
        return list;
    }

    /**
     * 获取收费状态
     */
    public static String getPayFlagName(byte payFlag) {
        if (payFlag == Const.Pay_No) {
            return "未付费";
        } else if (payFlag == Const.Pay_Yes) {
            return "已付费";
        } else {
            return "错误";
        }
    }

    //买入
    public static byte IntoWay_Merchandise = 1;
    //调拨
    public static byte IntoWay_Transfer = 2;
    //纠正，暂不使用，没必要
    public static byte IntoWay_Rectification = 3;
    //回收
    public static byte IntoWay_Recover = 4;
    //损耗
    public static byte OutWay_Loss = 5;

    /**
     * 获取入库方式Option
     *
     * @param type
     * @param isAll (true---"全部"  false---"没有全部")
     * @return
     */
    public static String getIntoWayOption(byte type, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.IntoWay_Merchandise, "买入"});
        list.add(new Object[]{Const.IntoWay_Transfer, "调拨"});
        list.add(new Object[]{Const.IntoWay_Recover, "回收"});
        String option = DataTools.getOptionByList(list, type + "", isAll);
        return option;
    }

    /**
     * 获取出库方式Option
     *
     * @param type
     * @param isAll (true---"全部"  false---"没有全部")
     * @return
     */
    public static String getOutWayOption(byte type, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.IntoWay_Merchandise, "卖出"});
        list.add(new Object[]{Const.IntoWay_Transfer, "调拨"});
        list.add(new Object[]{Const.OutWay_Loss, "损耗"});
        String option = DataTools.getOptionByList(list, type + "", isAll);
        return option;
    }


    /**
     * 以下老数据专用option
     */
    /**
     * 获取性别Option
     *
     * @param sex
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getOldSexOption(String sex, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
//		list.add(new Object[]{"",""});
        list.add(new Object[]{"男", "男"});
        list.add(new Object[]{"女", "女"});
        list.add(new Object[]{"不明", "不明"});
        String option = DataTools.getOptionByList(list, sex + "", isAll);
        return option;
    }

    /**
     * 死亡类型
     *
     * @param deadType
     * @param isAll    (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getOldDeadTypeOption(String deadType, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
//		list.add(new Object[]{"",""});
        list.add(new Object[]{"正常死亡", "正常死亡"});
        list.add(new Object[]{"非正常死亡", "非正常死亡"});
        list.add(new Object[]{"其他", "其他"});
        String option = DataTools.getOptionByList(list, deadType + "", isAll);
        return option;
    }

    /**
     * 接尸单位
     *
     * @param pickUnit
     * @param isAll    (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getOldPickUnitOption(String pickUnit, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
//		list.add(new Object[]{"",""});
        list.add(new Object[]{"本馆", "本馆"});
        list.add(new Object[]{"龙湾", "龙湾"});
        list.add(new Object[]{"瓯海", "瓯海"});
        list.add(new Object[]{"永嘉", "永嘉"});
        list.add(new Object[]{"其他", "其他"});
        String option = DataTools.getOptionByList(list, pickUnit + "", isAll);
        return option;
    }

    /**
     * 死亡原因
     *
     * @param death
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getOldDeathOption(String death, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
//		list.add(new Object[]{"",""});
        list.add(new Object[]{"101", "年迈"});
        list.add(new Object[]{"102", "车祸"});
        list.add(new Object[]{"103", "溺水"});
        list.add(new Object[]{"104", "自杀"});
        list.add(new Object[]{"105", "凶杀"});
        list.add(new Object[]{"106", "意外"});
        list.add(new Object[]{"107", "死刑"});
        String option = DataTools.getOptionByList(list, death + "", isAll);
        return option;
    }

    /**
     * 户籍option
     *
     * @param town
     * @param isAll (true--"全部"　false--"没有全部")
     * @return
     */
    public static String getOldTownOption(String town, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"市辖区", "市辖区"});
        list.add(new Object[]{"鹿城区", "鹿城区"});
        list.add(new Object[]{"龙湾区", "龙湾区"});
        list.add(new Object[]{"瓯海区", "瓯海区"});
        list.add(new Object[]{"洞头县", "洞头县"});
        list.add(new Object[]{"永嘉县", "永嘉县"});
        list.add(new Object[]{"平阳县", "平阳县"});
        list.add(new Object[]{"苍南县", "苍南县"});
        list.add(new Object[]{"文成县", "文成县"});
        list.add(new Object[]{"泰顺县", "泰顺县"});
        list.add(new Object[]{"瑞安市", "瑞安市"});
        list.add(new Object[]{"乐清市", "乐清市"});
        list.add(new Object[]{"外国", "外国"});
        list.add(new Object[]{"外省", "外省"});
        list.add(new Object[]{"不详", "不详"});
        String option = DataTools.getOptionByList(list, town + "", isAll);
        return option;
    }

    /**
     * 火化炉类型
     *
     * @param furnace
     * @param isAll
     * @return
     */
    public static String getOldFurnaceTypeOption(String furnace, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"特约炉", "特约炉"});
        list.add(new Object[]{"普通炉", "普通炉"});
        list.add(new Object[]{"特殊业务", "特殊业务"});
        String option = DataTools.getOptionByList(list, furnace + "", isAll);
        return option;
    }
    //单机系统 男女性别option

    /**
     * 男女性别
     *
     * @param sex
     * @param isAll
     * @return
     */
    public static String getSingleSexOption(String sex, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"F", "F"});
        list.add(new Object[]{"M", "M"});
        String option = DataTools.getOptionByList(list, sex + "", isAll);
        return option;
    }

    /**
     * 获取性别名称
     *
     * @param sex
     * @return
     */
    public static String getSingleSex(String sex) {
        if ("M".equals(sex)) {
            return "男";
        } else if ("F".equals(sex)) {
            return "女";
        } else {
            return "不详";
        }
    }

    /**
     * 公墓系统购买时选择死亡类型
     *
     * @param dead
     * @param isAll
     * @return
     */
    public static String getGMDeadOption(byte dead, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.GMDEATH_SHO, "寿"});
        list.add(new Object[]{Const.GMDEATH_GU, "故"});
        String option = DataTools.getOptionByList(list, dead + "", isAll);
        return option;
    }

    /**
     * 公墓系统 获取男女性别Option
     *
     * @param sex
     * @param isAll
     * @return
     */
    public static String getGMSexOption(byte sex, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"1", "男"});
        list.add(new Object[]{"2", "女"});
        String option = DataTools.getOptionByList(list, sex + "", isAll);
        return option;
    }

    public static String getState(byte state) {
        if (state == Const.GM_KONG) {
            return "空闲";
        } else if (state == Const.GM_GUO) {
            return "维护过期";
        } else if (state == Const.GM_QIAN) {
            return "迁出";
        } else if (state == Const.GM_SHIYONG) {
            return "已使用";
        } else if (state == Const.GM_SOU) {
            return "已售出";
        } else if (state == Const.GM_WEI) {
            return "维护修缮";
        } else if (state == Const.GM_YUDING) {
            return "预定";
        } else {
            return "错误";
        }
    }

    /**
     * 公墓系统获取年限Option
     *
     * @param longTime
     * @param isAll
     * @return
     */
    public static String getGMLongTimeOption(int longTime, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"1", "1"});
        list.add(new Object[]{"2", "2"});
        list.add(new Object[]{"3", "3"});
        list.add(new Object[]{"5", "5"});
        list.add(new Object[]{"10", "10"});
        list.add(new Object[]{"15", "15"});
        list.add(new Object[]{"20", "20"});
        list.add(new Object[]{"25", "25"});
        list.add(new Object[]{"30", "30"});
        String option = DataTools.getOptionByList(list, longTime + "", isAll);
        return option;
    }

    /**
     * 公墓系统获取收费类型名称
     *
     * @param type
     * @return
     */
    public static String getGmChargeType(byte type) {
        if (type == Const.GMCHARGE_DKAFF) {
            return "雕刻安放费";
        } else if (type == Const.GMCHARGE_MXF) {
            return "墓穴费";
        } else if (type == Const.GMCHARGE_WHF) {
            return "维护费";
        } else if (type == Const.GMCHARGE_QYF) {
            return "迁移费";
        } else if (type == Const.GMCHARGE_DKF) {
            return "雕刻费";
        } else if (type == Const.GMCHARGE_WXF) {
            return "维修费";
        } else {
            return "错误";
        }
    }

    /**
     * 获取查询条件的下拉框，传进来的参数从Const类里面找
     *
     * @param arr
     * @return
     */
    public static String getGmChargeTypeOption(byte[] arr) {
        List<Object[]> list = new ArrayList<Object[]>();
        for (byte b : arr) {
            if (b == Const.GMCHARGE_DKAFF) {
                list.add(new Object[]{Const.GMCHARGE_DKAFF, "雕刻安放费"});
            } else if (b == Const.GMCHARGE_MXF) {
                list.add(new Object[]{Const.GMCHARGE_MXF, "墓穴费"});
            } else if (b == Const.GMCHARGE_WHF) {
                list.add(new Object[]{Const.GMCHARGE_WHF, "维护费"});
            } else if (b == Const.GMCHARGE_QYF) {
                list.add(new Object[]{Const.GMCHARGE_QYF, "迁移费"});
            } else if (b == Const.GMCHARGE_DKF) {
                list.add(new Object[]{Const.GMCHARGE_DKF, "雕刻费"});
            } else if (b == Const.GMCHARGE_WXF) {
                list.add(new Object[]{Const.GMCHARGE_WXF, "维修费"});
            }
        }
        String option = DataTools.getOptionByList(list, null, false);
        return option;
    }

    /**
     * 获取维修登记的Option
     *
     * @param type
     * @param isAll (true---"全部"  false---"没有全部")
     * @return
     */
    public static String getRepairTypeOption(byte type, boolean isAll) {
        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{Const.GMTYPE_NO, "未维修"});
        list.add(new Object[]{Const.GMTYPE_YES, "已维修"});
        String option = DataTools.getOptionByList(list, type + "", isAll);
        return option;
    }

    /**
     * 获取是否维修名称
     *
     * @param repair
     * @return
     */
    public static String getIsRepair(byte repair) {
        if (repair == Const.GMTYPE_YES) {
            return "已维修";
        } else if (repair == Const.GMTYPE_NO) {
            return "未维修";
        } else {
            return "不详";
        }
    }

    /**
     * 获取公墓死者死亡类型名称
     *
     * @param deadType
     * @return
     */
    public static String getDeadType(byte deadType) {
        if (deadType == Const.GMDEATH_SHO) {
            return "寿";
        } else if (deadType == Const.GMDEATH_GU) {
            return "故";
        } else {
            return "未填写";
        }
    }

    /**
     * 获取是否回收墓穴证名称
     *
     * @param isRecovery
     * @return
     */
    public static String getIsRecovery(byte isRecovery) {
        if (isRecovery == Const.IsHave_Yes) {
            return "已收";
        } else if (isRecovery == Const.IsHave_No) {
            return "未收";
        } else {
            return "不详";
        }
    }

}
