package com.hz.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * ���ܽ���
 * @author ljx
 *
 */
public class Endecrypt {
	private static String SPKEY = "huizhi16";
	/**
	 * ����MD5����
	 * 
	 * @param String
	 *            ԭʼ��SPKEY
	 * @return byte[] ָ�����ܷ�ʽΪmd5���byte[]
	 */
	private byte[] md5(String strSrc) {
		byte[] returnByte = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			returnByte = md5.digest(strSrc.getBytes("GBK"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnByte;
	}

	/**
	 * �õ�3-DES����Կ�� ���ݽӿڹ淶����Կ��Ϊ24���ֽڣ�md5���ܳ�������16���ֽڣ���˺��油8���ֽڵ�0
	 * 
	 * @param String
	 *            ԭʼ��SPKEY
	 * @return byte[] ָ�����ܷ�ʽΪmd5���byte[]
	 */
	private byte[] getEnKey(String spKey) {
		byte[] desKey = null;
		try {
			byte[] desKey1 = md5(spKey);
			desKey = new byte[24];
			int i = 0;
			while (i < desKey1.length && i < 24) {
				desKey[i] = desKey1[i];
				i++;
			}
			if (i < 24) {
				desKey[i] = 0;
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return desKey;
	}

	/**
	 * 3-DES����
	 * 
	 * @param byte[]
	 *            src Ҫ����3-DES���ܵ�byte[]
	 * @param byte[]
	 *            enKey 3-DES������Կ
	 * @return byte[] 3-DES���ܺ��byte[]
	 */
	public byte[] Encrypt(byte[] src, byte[] enKey) {
		byte[] encryptedData = null;
		try {
			DESedeKeySpec dks = new DESedeKeySpec(enKey);
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
			SecretKey key = keyFactory.generateSecret(dks);
			Cipher cipher = Cipher.getInstance("DESede");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			encryptedData = cipher.doFinal(src);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encryptedData;
	}

	/**
	 * ���ַ�������Base64����
	 * 
	 * @param byte[]
	 *            src Ҫ���б�����ַ�
	 *
	 * @return String ���б������ַ���
	 */
	public String getBase64Encode(byte[] src) {
		String requestValue = "";
		try {
			BASE64Encoder base64en = new BASE64Encoder();
			requestValue = base64en.encode(src);
			// 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestValue;
	}

	/**
	 * ȥ���ַ����Ļ��з��� base64����3-DES������ʱ���õ����ַ����л��з��� ��һ��Ҫȥ��������uni-wiseƽ̨����Ʊ������ɹ���
	 * ��ʾ��sp��֤ʧ�ܡ����ڿ����Ĺ����У���Ϊ����������������޲ߣ� һ�����Ѹ����ҿ�������ͨҪһ�μ��ܺ� �����֣�Ȼ��ȥ���Լ����ɵ��ַ����Ƚϣ�
	 * ���Ǹ�����ĵ��Է����������ȽϷ��������ɵ��ַ���Ψһ��ͬ�� �Ƕ��˻��С� ����c#����Ҳд��Ʊ���������û�з���������⡣
	 *
	 */
	private String filter(String str) {
		String output = null;
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < str.length(); i++) {
			int asc = str.charAt(i);
			if (asc != 10 && asc != 13)
				sb.append(str.subSequence(i, i + 1));
		}
		output = new String(sb);
		return output;
	}

	/**
	 * ���ַ�������URLDecoder.encode(strEncoding)����
	 * 
	 * @param String
	 *            src Ҫ���б�����ַ���
	 *
	 * @return String ���б������ַ���
	 */
	public String getURLEncode(String src) {
		String requestValue = "";
		try {
			requestValue = URLEncoder.encode(src);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestValue;
	}

	/**
	 * 3-DES����
	 * 
	 * @param String
	 *            src Ҫ����3-DES���ܵ�String
	 * @param String
	 *            spkey�����SPKEY
	 * @return String 3-DES���ܺ��String
	 */
	public String get3DESEncrypt(String src, String spkey) {
		String requestValue = "";
		try {
			// �õ�3-DES����Կ��
			byte[] enKey = getEnKey(spkey);
			// Ҫ����3-DES���ܵ������ڽ���/"UTF-16LE/"ȡ�ֽ�
			byte[] src2 = src.getBytes("UTF-16LE");
			// ����3-DES���ܺ�����ݵ��ֽ�
			byte[] encryptedData = Encrypt(src2, enKey);
			// ����3-DES���ܺ�����ݽ���BASE64����
			String base64String = getBase64Encode(encryptedData);
			// BASE64����ȥ�����з���
			String base64Encrypt = filter(base64String);
			// ��BASE64�����е�HTML���������ת��Ĺ���
			requestValue = getURLEncode(base64Encrypt);
			// 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestValue;
	}
	public static void main(String[] args) {
		Endecrypt r=new Endecrypt();
String 	result=	r.get3DESEncrypt("000000", SPKEY);
System.out.println(result);
	}

	/**
	 * ���ַ�������URLDecoder.decode(strEncoding)����
	 * 
	 * @param String
	 *            src Ҫ���н�����ַ���
	 *
	 * @return String ���н������ַ���
	 */
	public String getURLDecoderdecode(String src) {
		String requestValue = "";
		try {
			requestValue = URLDecoder.decode(src);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestValue;
	}

	/**
	 *
	 * ����3-DES���ܣ���Կ�׵�ͬ�ڼ��ܵ���Կ�ף���
	 * 
	 * @param byte[]
	 *            src Ҫ����3-DES����byte[]
	 * @param String
	 *            spkey�����SPKEY
	 * @return String 3-DES���ܺ��String
	 */
	public String deCrypt(byte[] debase64, String spKey) {
		String strDe = null;
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("DESede");
			byte[] key = getEnKey(spKey);
			DESedeKeySpec dks = new DESedeKeySpec(key);
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
			SecretKey sKey = keyFactory.generateSecret(dks);
			cipher.init(Cipher.DECRYPT_MODE, sKey);
			byte ciphertext[] = cipher.doFinal(debase64);
			strDe = new String(ciphertext, "UTF-16LE");
		} catch (Exception ex) {
			strDe = "";
			ex.printStackTrace();
		}
		return strDe;
	}

	/**
	 * 3-DES����
	 * 
	 * @param String
	 *            src Ҫ����3-DES���ܵ�String
	 * @param String
	 *            spkey�����SPKEY
	 * @return String 3-DES���ܺ��String
	 */
	public String get3DESDecrypt(String src, String spkey) {
		String requestValue = "";
		try {
			// �õ�3-DES����Կ��
			// URLDecoder.decodeTML���������ת��Ĺ���
			String URLValue = getURLDecoderdecode(src);
			// ����3-DES���ܺ�����ݽ���BASE64����
			BASE64Decoder base64Decode = new BASE64Decoder();
			byte[] base64DValue = base64Decode.decodeBuffer(URLValue);
			// Ҫ����3-DES���ܵ������ڽ���/"UTF-16LE/"ȡ�ֽ�
			requestValue = deCrypt(base64DValue, spkey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requestValue;
	}
	
	/**
	 * ����
	 * @param content
	 * @return
	 */
	public static String encrypt(String content){
		Endecrypt test = new Endecrypt();
		
		return test.get3DESEncrypt(content, SPKEY);
	}
	
	/**
	 * ����
	 * @param content
	 * @return
	 */
	public static String decrypt(String content){
		Endecrypt test = new Endecrypt();
		return test.get3DESDecrypt(content, SPKEY);
		
	}
}