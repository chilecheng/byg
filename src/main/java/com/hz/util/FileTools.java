package com.hz.util;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import com.alibaba.fastjson.JSONObject;

public class FileTools {
	/**
	 * 获取文件的扩展名
	 * @param dir
	 * @return
	 */
	public static String getExt(String dir){
	    String filename = dir;
	    int lastdot = filename.lastIndexOf(".");
	    String ext = filename.substring(lastdot+1);
	    return ext;
	}
	
	/**
	 * 删除磁盘中的文件
	 */
	public static void deleteDiskFile(String fileName){
		try {
			File file = new File(fileName);
			if(file.exists()){
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


    
}

		
