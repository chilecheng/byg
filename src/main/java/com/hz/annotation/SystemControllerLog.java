package com.hz.annotation;

import java.lang.annotation.*;

/**
 * @author cjb
 * �Զ���ע�� ����Controller
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})  
@Retention(RetentionPolicy.RUNTIME)  
@Documented  
public @interface SystemControllerLog {  
    String description() default "";  
}  