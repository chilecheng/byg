package com.hz.mapper.system;

import java.util.List;
import java.util.Map;

import com.hz.entity.system.Menu;
import com.hz.entity.system.Role;
import com.hz.entity.system.RoleMenu;
import com.hz.entity.system.UserRole;

public interface RoleMapper {
	/**
	 * 添加职位
	 * @param role
	 */
	void saveRole(Role role);
	/**
	 * 删除职位
	 * @param id 
	 */
	void deleteRole(String id);
	/**
	 * 修改职位
	 * @param corpseUnit 
	 */
	void updateRole(Role role);
	/**
	 * 获取职位对象
	 * @param id
	 * @return
	 */
	Role getRoleById(String id);
	/**
	 * 根据条件获取职位List
	 * @return
	 */
	List<Role> getRoleList(Map<String, Object> paramMap);
	
	//职位菜单

	/**
	 * 添加职位菜单
	 * @param role
	 */
	void saveRoleMenu(RoleMenu roleMenu);
	/**
	 * 删除职位菜单
	 * @param id 
	 */
	void deleteRoleMenu(String id);
	/**
	 * 修改职位菜单
	 * @param corpseUnit 
	 */
	void updateRoleMenu(RoleMenu roleMenu);
	/**
	 * 获取职位菜单
	 * @param id
	 * @return
	 */
	RoleMenu getRoleMenuById(String id);
	/**
	 * 根据条件获取职位菜单List
	 * @String roleId(职位id),menuId(菜单id)
	 * @return
	 */
	List<RoleMenu> getRoleMenuList(Map<String, Object> paramMap);
	/**
	 * 包含职位菜单选中标志的所有菜单List
	 * @String roleId(职位id)
	 * @return
	 */
	List<Menu> getMenuList(Map<String, Object> paramMap);

	/**
	 * 获取包含用户勾选标志的所有职位List
	 * @String userId(用户id)
	 * @return
	 */
	List<Role> getRoleListByUser(Map<String, Object> paramMap);
	
	/**
	 * 保存用户职位
	 * @param userRole
	 */
	void saveUserRole(UserRole userRole);
	
	/**
	 * 删除用户职位
	 * @param userRole
	 */
	void deleteUserRole(UserRole userRole);

	/**
	 * 获取用户职位List
	 * @String userId(用户id),roleId(职位id)
	 * @return
	 */
	List<UserRole> getUserRoleList(Map<String, Object> paramMap);
}