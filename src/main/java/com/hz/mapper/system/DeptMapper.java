package com.hz.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.system.Dept;
import com.hz.entity.system.DeptMenu;
import com.hz.entity.system.DeptTaskMenu;
import com.hz.entity.system.Menu;
import com.hz.entity.system.TaskMenu;

public interface DeptMapper {
	/**
	 * 添加部门
	 * @param dept
	 */
	void saveDept(Dept dept);
	/**
	 * 删除部门
	 * @param id 
	 */
	void deleteDept(String id);
	/**
	 * 修改部门
	 * @param corpseUnit 
	 */
	void updateDept(Dept dept);
	/**
	 * 获取部门对象
	 * @param id
	 * @return
	 */
	Dept getDeptById(String id);
	/**
	 * 根据条件获取部门List
	 * @return
	 */
	List<Dept> getDeptList(Map<String, Object> paramMap);
	/**
	 * 包含部门菜单选中标志的所有菜单List
	 */
	List<Menu> getMenuList(Map<String, Object> paramMap);
	/**
	 * 包含部门任务菜单选中标志的所有任务菜单List
	 */
	List<TaskMenu> getTaskMenuList(Map<String, Object> paramMap);
	/**
	 * 根据条件获取部门菜单List
	 */
	List<DeptMenu> getDeptMenuList(Map<String, Object> paramMap);
	/**
	 * 添加部门菜单
	 */
	void saveDeptMenu(DeptMenu deptMenu);
	/**
	 * 删除部门菜单
	 */
	void deleteDeptMenu(String id);
	void deleteDeptTaskMenu(String deptId);
	void saveDeptTaskMenu(DeptTaskMenu dtm);
	@Select("SELECT id FROM sys_dept WHERE name like CONCAT('%',#{deptName},'%') limit 0,1")
	String getDeptIdByName(String deptName);
}