package com.hz.mapper.system;

import java.util.List;
import java.util.Map;

import com.hz.entity.system.Notification;

/**
 * 公开通告管理
 * @author rgy
 *
 */
public interface NotificationMapper {
	/**
	 * 添加公开公告
	 * @param notification
	 */
	void saveNotification(Notification notification);
	/**
	 * 修改公开公告
	 * @param notification
	 */
	void updateNotification(Notification notification);
	/**
	 * 删除公开公告
	 * @param id
	 */
	void deleteNotification(String id);
	/**
	 * 获取公开公告对象
	 * @param userId
	 * @return
	 */
	Notification getNotifictionById(String userId);
	
	/**
	 * 获取公开公告浏览数量
	 * @param userId
	 * @return
	 */
	int getNotifictionByNumber(String id);
	/**
	 * 根据条件获取公开公告List
	 * @param paramMap
	 * @return
	 */
	List<Notification> getNotificationList(Map<String, Object> paramMap);
}