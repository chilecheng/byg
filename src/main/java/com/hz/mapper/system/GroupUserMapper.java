package com.hz.mapper.system;

import java.util.List;
import java.util.Map;

import com.hz.entity.system.GroupType;
import com.hz.entity.system.User;

public interface GroupUserMapper {
	void saveType(GroupType type);
	void updateType(GroupType type);
	void deleteType(String id);
	GroupType getTypeById(String id);
	List<GroupType> getGroupUserList(Map<String,Object> paramMap);
	List<User> getGroupDetailList(Map<String,Object> paramMap);
	void deleteUserFromGroup(String id);
	void deleteUserByGroup(byte type);
	List<GroupType> getGroupTypeList();
	void updateUserType(String id, String type);
	List<User> getUserNoGroup();
	void updateUT(byte type, String string);
	int getTypeCount(String bt);
	int getNameCount(String name);
	int getTypeByTypeName(String name);
	GroupType getGroupByType(byte type);
}