package com.hz.mapper.system;

import java.util.List;
import java.util.Map;

import com.hz.entity.system.TaskMenu;

public interface TaskMenuMapper {
	/**
	 * 添加菜单
	 */
	void saveTaskMenu(TaskMenu taskmenu);
	/**
	 * 删除菜单
	 */
	void deleteTaskMenu(String id);
	/**
	 * 修改菜单
	 */
	void updateTaskMenu(TaskMenu taskmenu);
	/**
	 * 获取菜单对象
	 */
	TaskMenu getTaskMenuById(String id);
	/**
	 * 根据条件获取菜单List
	 */
	List<TaskMenu> getTaskMenuList(Map<String, Object> paramMap);
	
	/**
	 * 根据用户id获取有权限的菜单
	 */
//	List<TaskMenu> getTaskMenuListByUser(String userId);
	List<TaskMenu> getTaskMenuListByUserDept(String userId);
}