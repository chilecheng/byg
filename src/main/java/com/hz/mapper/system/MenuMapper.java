package com.hz.mapper.system;

import java.util.List;
import java.util.Map;

import com.hz.entity.system.Menu;
import com.hz.entity.system.TaskMenu;

public interface MenuMapper {
	/**
	 * 添加菜单
	 * @param menu
	 */
	void saveMenu(Menu menu);
	/**
	 * 删除菜单
	 * @param id 
	 */
	void deleteMenu(String id);
	/**
	 * 修改菜单
	 * @param corpseUnit 
	 */
	void updateMenu(Menu menu);
	/**
	 * 获取菜单对象
	 * @param id
	 * @return
	 */
	Menu getMenuById(String id);
	/**
	 * 根据条件获取菜单List
	 * @return
	 */
	List<Menu> getMenuList(Map<String, Object> paramMap);
	
	/**
	 * 根据用户id获取有权限的菜单
	 * @param userId
	 * @return
	 */
	List<Menu> getMenuListByUser(String userId);
	List<Menu> getMenuListByUserDept(String userId);
	/**
	 * <!--根据部门权限获取首页任务菜单  -->
	 */
	List<TaskMenu> getTaskMenuListByUserDept(String userId);
	List<TaskMenu> getTaskMenuList();
}