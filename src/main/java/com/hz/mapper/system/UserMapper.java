package com.hz.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.system.User;

public interface UserMapper {
	void saveUser(User user);
	void updateUser(User user);
	void deleteUser(String id);
	User getUserById(String userId);
	List<User> getUserList(Map<String,Object> paramMap);
	//获取一科礼厅工作人员
	List<User> getFirstUserList(Map<String,Object> paramMap);
	//获取一科前台科员
	List<User> getFirstBeforeUserList(Map<String,Object> paramMap);
	User getUserByUserName(String userName);
	int getCodeCount(String code);
	String getNameById(String enterId);
	/**
	 * 根据职位名称取出用户
	 */
	List<User> getUserListByRoleName(String arranger);
	/**
	 * 根据部门 编号 获取用户
	 */
	List<User> getDepUser(Map<String,Object> paramMap);
	@Select("SELECT COUNT(1) FROM sys_user WHERE user_name=#{userName}")
	int getuserNameCount(String userName);
	@Select("SELECT `name` FROM sys_user WHERE id=#{id}")
	String getUserNameById(String id);
}