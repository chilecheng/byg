package com.hz.mapper.ye;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.AutoFreezerDetail;
import com.hz.entity.ye.AutopsyRecord;

/**
 * 法政验尸
 * @author non
 *
 */
public interface AutopsyRecordMapper {

	//List listAllAutopsyRecord(Map<String,Object> paramMap);
	
	void insertOrder(AutopsyRecord autopsyRecord);

	List<AutopsyRecord> listAutopsyRecord(Map<String,Object> paramMap);
	
	List<AutopsyRecord> listAutoFreezerRecord(Map<String,Object> paramMap);

	List<AutopsyRecord> getAutopsyRecordById(String id);

	void updateOrder(AutopsyRecord apr);

	List<AutopsyRecord> checkid(String cid);

	void toRevise(AutopsyRecord apr);
	
	
	void  deleteAutopsyRecord(String id);
	
	AutoFreezerDetail autoFreezerDetail(Map<String,Object> paramap);
	
	/*@Select("SELECT count(*) FROM socket_user WHERE user_id=#{0} AND notice_type=#{1} AND ")
	int getCount(String userId, String noticeType);*/
	
	//根据当前日期    取得当日验尸 解冻的 遗体数目

	@Select("select COUNT(*) as sum from  y_autopsy_record a  join y_commission_order y on y.id = a.commission_order_id  and  y.check_flag =1  "
			+ "where a.flag =2 and a.auto_time &gt;= #{0}  and a.auto_time &lt;=  #{1}  ")
	int getSumAutoFreezer(Timestamp startTime, Timestamp endTime);
	//根据当前日期  取得当日应 政法验尸的  遗体数目
	/*@Select("select COUNT(*) as sum from  y_autopsy_record a  join y_commission_order y on y.id = a.commission_order_id   where a.flag =2 and y.check_flag =1 and a.y_time &gt;= #{startTime}  and a.y_time &lt;= #{endTime}  ")
	int getSumhengfa(Timestamp startTime, Timestamp endTime);*/
	
	@Select ( "select y.auto_time from y_autopsy_record y where y.commission_order_id=#{0}")
	Timestamp getAutoTimeById(String id);
	

	
	
}
