package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Toponym;

public interface AreaFuneralStaticalFormMapper {
	/**
	 * 地区列表
	 * @param paramap
	 * @param string
	 * @return
	 */
	List<Toponym> getToponymList(Map<String, Object> paramap);	
	/**
	 * 统计数量
	 * @param paraMap
	 * @return
	 */
	public Toponym getToponymIdCount(Map<String,Object> paraMap);
}
