package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;

import com.hz.entity.ye.FarewellRecord;

/**
 * 告别厅记录
 * @author rgy
 *
 */
public interface FarewellRecordMapper {
	/**
	 * 添加告别厅记录
	 * @param farewellRecord
	 */
	void saveFarewellRecord(FarewellRecord farewellRecord);
	void saveFarewellRecord2(FarewellRecord farewellRecord);
	/**
	 * 删除告别厅记录
	 * @param id
	 */
	void deleteFarewellRecord(String id);
	/**
	 * 更改告别厅记录
	 * @param farewellRecord
	 */
	void updateFarewellRecord(FarewellRecord farewellRecord);
	void updateFarewellRecord2(FarewellRecord farewellRecord);
	/**
	 * 获取告别厅记录对象
	 * @param id
	 * @return
	 */
	FarewellRecord getFarewellRecordById(String id);
	/**
	 * 根据条件获取告别厅记录list
	 * @param paramMap
	 * @return
	 */
	List<FarewellRecord> getFarewellRecordList(Map<String, Object> paramMap);
	/**
	 * 根据业务单ID获取告别厅信息
	 * @param id
	 * @return
	 */
	FarewellRecord getFarewellRecordByOrderId(String id);
	List<FarewellRecord> getFarewellRecordMapList(Map<String, Object> paramMap);
	/**
	 * 布置告别厅
	 * @param farewellRecord
	 */
	void arrangeFarewell(FarewellRecord farewellRecord);
	/**
	 * 获取某天告别厅记录数
	 * @param map
	 */
	int getFarewellRecordCount(Map<String, Object> map);
	@Delete("delete from y_farewell_record where commission_order_id=#{cid}")
	void deleteFarewellRecordByCoId(String cid);
	/**
	 * 根据当前用户解除锁定
	 * @param map
	 */
	@Delete("delete from y_farewell_record where creat_user_id=#{0} AND flag=#{1} and commission_order_id is #{2}")
	void deleteFarewellRecordByLock(String userId,byte flag,String commissionOrderId);
	
	@Update("update y_farewell_record set isdel =#{1}   where id =#{0}")
	void updateFarewellRecordIsDeal(String id, byte isdel);
}
