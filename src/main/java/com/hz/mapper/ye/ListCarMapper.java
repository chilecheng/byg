package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.CarSchedulRecord;

public interface ListCarMapper {
	/**
	 * 获取车辆列表
	 * @param paramMap
	 * @return
	 */
	List<CarSchedulRecord> findListCar(Map<String,Object> paramMap);
}
