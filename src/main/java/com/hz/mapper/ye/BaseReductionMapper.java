package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.BaseReduction;

/**
 * 基本减免申请
 * @author rgy
 *
 */
public interface BaseReductionMapper {
	/**
	 * 添加 基本减免申请
	 * @param BaseReduction
	 */
	void saveBaseReduction(BaseReduction baseReduction);
	/**
	 * 删除 基本减免申请
	 * @param id
	 */
	void deleteBaseReduction(String id);
	/**
	 * 更改 基本减免申请
	 * @param BaseReduction
	 */
	void updateBaseReduction(BaseReduction baseReduction);
	/**
	 * 获取 基本减免申请
	 * @param id
	 * @return
	 */
	BaseReduction getBaseReductionById(String id);
	/**
	 * 根据条件获取 基本减免申请list
	 * @param paramMap
	 * @return
	 */
	List<BaseReduction> getBaseReductionList(Map<String, Object>paramMap);
}
