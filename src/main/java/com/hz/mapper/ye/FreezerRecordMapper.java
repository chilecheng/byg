package com.hz.mapper.ye;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.FreezerRecord;

/**
 * 冷藏柜记录
 * @author rgy
 *
 */
public interface FreezerRecordMapper {
	/**
	 * 添加冷藏柜记录
	 * @param FreezerRecord
	 */
	void saveFreezerRecord(FreezerRecord freezerRecord);
	/**
	 * 删除冷藏柜记录
	 * @param id
	 */
	void deleteFreezerRecord(String id);
	/**
	 * 更改冷藏柜记录
	 * @param FreezerRecord
	 */
	void updateFreezerRecord(FreezerRecord freezerRecord);
	/**
	 * 冷藏 入柜 柜记录
	 * @param FreezerRecord
	 */
	FreezerRecord getFreezerRecordById(String id);
	/**
	 * 根据条件获取冷藏柜记录list
	 * @param paramMap
	 * @return
	 */
	List<FreezerRecord> getFreezerRecordList(Map<String, Object> paramMap);
	/*<!--  查询已付费的  的满足解冻时间的    项目  （遗体解冻 消息提醒） -->*/
	List<FreezerRecord> getFreezerRecordListPayed(Map<String, Object> paramMap);
	
  /**
   * 直接查询 出 冰柜调度记录
	*@param paramMap
	*@return
	*/
	List<FreezerRecord> getFreeComList(Map<String, Object>  paramMap);
	/**
	 * 查询未出库的数据记录集
	 *@param paramMap
	*@return 
	 */
	List<FreezerRecord> getNotoutFreezerRecordList(Map<String, Object> paramMap);
	/**
	 * 查询最近两天需要出柜的 书籍记录
	 */
//	List getLastfreeOrderComList(Map<String, Object> paramMap);
	
	List<FreezerRecord> getlastFreezerRecordList(Map<String, Object> paramMap);
	/**
	 * 首页使用 查找冰柜调度
	 * @param paramMap
	 * @return
	 */
	public List<FreezerRecord> getFreezerRecordHome(Map<String,Object> paramMap);
	public int getNumber(Map<String,Object> paramMap);
	/**
	 * 根据  id  获取到freezerrecord 的 autoTime
	 */
	@Select("select y.auto_time from y_freezer_record  y where  y.id=#{0}")
	Timestamp getAutoTimeById( String id);
	/**
	 * 根据orderId  获取 获取到freezerrecord 的 autoTime
	 * @param id
	 * @return
	 */
	@Select("select y.auto_time from y_freezer_record  y where  y.commission_order_id=#{0} ")
	Timestamp getAutoTimeByOrderId(String orderId);
	
	@Select("select y.id,y.freezer_id,y.begin_date from y_freezer_record  y where  y.commission_order_id=#{id}")
	@Results(
			{
			@Result(id = true, column = "id", property = "id"),
			@Result(column = "freezer_id", property = "freezerId"),
			@Result(column="begin_date",property="beginDate")
		})
	FreezerRecord getFreezerRecordByCoId(String id);
	@Delete("delete from y_freezer_record where commission_order_id=#{id}")
	void deleteFreezerRecordByCoId(String id);
	
	/**
	 * 最新版二科冰柜列表查询方法
	 * @param map
	 * @return
	 */
	List<FreezerRecord> getIceRecondMapper(Map<String,Object> map);
 	
}