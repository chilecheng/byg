package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.payDivision.BatchLosses;
import com.hz.entity.ye.ItemOrder;

public interface ItemOrderMapper {
	/**
	 * 添加火化委托单收费项目
	 * @param itemOrder
	 */
	void saveItemOrder(ItemOrder itemOrder);
	/**
	 * 删除火化委托单收费项目
	 * @param id 
	 */
	void deleteItemOrder(String id);
	/**
	 * 更新火化委托单收费项目
	 * @param corpseUnit 
	 */
	void updateItemOrder(ItemOrder itemOrder);	
	
	/**
	 * 获取火化委托单收费项目对象
	 * @param id
	 * @return
	 */
	ItemOrder getItemOrderById(String id);
	/**
	 * 根据条件获取火化委托单收费项目List
	 * @return
	 */
	List<ItemOrder> getItemOrderList(Map<String,Object> paramMap);
	/**
	 * 根据条件获取火化委托单收费项目List（新加历史）
	 * @return
	 */
	List<ItemOrder> getItemOrderOldAndNew(Map<String,Object> paramMap);
	/**
	 * 加个更新 by jgj
	 * @param itemOrder
	 */
	public void updateItemOrderNew(ItemOrder itemOrder);
	/**
	 * 加个插入数据记录 by jgj
	 * @param itemOrder
	 */
	public void saveItemOrderNew(ItemOrder itemOrder);
	
	/**
	 * 加个删除记录 by jgj
	 * @param String id
	 */
	public void delItemOrderNew(String id);
	
	public ItemOrder findItemOrderById(String id);
	/**
	 * 根据服务项目
	 * @param paramMap
	 * @return
	 */
	List<ItemOrder> getItemOrderListByType(Map<String, Object> paramMap);
	/**
	 * 给服务项目添加布置信息
	 * @param io
	 */
	void arrangeItem(ItemOrder io);
	/**
	 * 收费科批量挂账信息
	 */
	public List<BatchLosses> getBatchLossesList(Map<String,Object> paramMap);
	/**
	 * 批量挂账详细
	 */
	public List<ItemOrder> getBatchLossesListByMap(Map<String,Object> paramMap);
	/**
	 * 收费科  委托业务 非挂账详细
	 */
	public List<ItemOrder> getdelegrateItemOrderList(Map<String,Object> paramMap);
	
	List<ItemOrder> getItemOrderListForNotice(Map<String, Object> map);
	/**
	 * 根据条件删除收费记录，慎用
	 * @param map
	 * @return
	 */
	public int deleteItemByMap(Map<String,Object> map);
	/**
	 * 委托业务收费查看使用：统计金额
	 * @param map
	 * @return
	 */
	double getTotalByOrderNumber(Map<String,Object> map);
}