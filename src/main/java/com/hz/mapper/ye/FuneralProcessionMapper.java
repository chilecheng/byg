package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.FuneralRecord;

/**
 * 礼仪出殡记录
 * @author czl
 *
 */

public interface FuneralProcessionMapper {

	
	/**
	 * @param time 从页面获取的 时间
	 * @return 礼仪出殡死者基本属性 
	 */
	public List<FuneralRecord> findByTime(Map<String,Object> paramMap);
	
	
}
