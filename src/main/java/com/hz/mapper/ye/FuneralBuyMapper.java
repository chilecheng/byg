package com.hz.mapper.ye;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.BuyGoodsRecordD;
import com.hz.entity.ye.Buygood;
import com.hz.entity.ye.ListFuneralOrderRecord;
import com.hz.entity.ye.ListFuneralOrderRecordDetail;
import com.hz.entity.ye.ListOrderRecordDetail;
import com.hz.entity.ye.SadGoods;

/**
 * 丧葬用品购买
 * @author lipengpeng
 *
 */
public interface FuneralBuyMapper {
	
	List<ListFuneralOrderRecord> listAllOrderRecord(Map<String,Object> paramMap);
	
	void insertOrder(ListFuneralOrderRecord lfor);
	
//	List<ListFuneralOrderRecord> findFareWellAndMourningById(String id);////get(0)
	
	List<ListFuneralOrderRecord> findOrderRecordById(String id);//get(0)
	
	
	//通过  火化委托单 id  查询 丧葬用品购买项  （任务单）
	List<Buygood> findItemByOrderId(Map<String,Object> paramMap);

	void updateOrderRecord(ListFuneralOrderRecord lfor);
	
	void deleteOrderRecordById(String id);
	
	void insertOrderDetail(ListFuneralOrderRecordDetail ld);
	
	List<ListOrderRecordDetail> findOrderDetailById(String id);//多条
	
	List<SadGoods> getSadGoodsList(Map<String, Object> paramMap);
	
	void deleteFirstStepById(String id);
	
	/**
	 * 收费科非委托业务使用
	 * @param paramMap
	 * @return
	 */
	public List<ListOrderRecordDetail> getBuyGoodsRecordFee(Map<String,Object> paramMap);
	
	public ListFuneralOrderRecord getBuyGoodsRecordFeeById(String id);
	
	@Select ("select y.takeaway_time from y_buy_goods_record y where y.id=#{0}")
	Timestamp getTakeawayTimeById(String id);
	/**
	 * 获取丧葬用品的sort 值
	 * 获取丧葬用品项目里面是否有满足消息提醒的
	 * @param orderId
	 * @return
	 */
	@Select (" select i.sort from y_buy_goods_record_d d  	"
			+ "join  y_buy_goods_record b  	on d.buy_goods_record_id= b.id "
			+ " join  ba_item  i  on i.id = d.item_id "
			+ "where 1=1 and b.commission_order_id=#{0} ")
	List<Byte>  getSortByOrderId(String orderId);
	
	
	List<BuyGoodsRecordD> getGoodsRecordDListByBuyOrderId(String orderId);
	
	
}