package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.FillCardRecord;
import com.hz.entity.ye.ListCardRecord;

/**
 * ����
 * @author lipengpeng
 *
 */
public interface RecardMapper {
	
	/**
	 * ����
	 * @param fillCardRecord
	 */
	void insertFillCard(FillCardRecord fillCardRecord);
	

	List<ListCardRecord> listAllCardCount(Map<String,Object> paramMap);
	
	void updateCommissionCard(FillCardRecord fcr);
	/**
	 * �ж��Ƿ���ڿ���
	 * 0 ���ظ�
	 * 1 �ظ�
	 * @param id
	 * @param flag
	 * @return
	 */
	@Select("select  COUNT(a.Id)  from  y_commission_order a where a.card_code =#{0}") 
	int getBoolCardCode(String cardCode);

	
}
