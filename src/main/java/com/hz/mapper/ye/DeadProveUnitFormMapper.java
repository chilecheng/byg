package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.ProveUnit;
/**
 * 非正常死亡证明单位报表
 * @author mbz
 *
 */
public interface DeadProveUnitFormMapper {
	/**
	 * 非正常死亡证明单位报表
	 * @param paramMap
	 */
	List<ProveUnit> getDeadProveUnitList(Map<String,Object> paramMap);
	
	/**
	 * 统计数量
	 * @param paraMap
	 * @return
	 */
	int getDeadProveUnitIdCount(Map<String,Object> paraMap);
}
