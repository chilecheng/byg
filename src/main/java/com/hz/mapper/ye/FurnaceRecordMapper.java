package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;

import com.hz.entity.ye.FurnaceRecord;


/**
 * 火化炉信息
 * @author tsp
 *
 */
public interface FurnaceRecordMapper {
	/**
	 * 添加火化炉信息
	 * @param furnaceRecord
	 */
	void saveFurnaceRecord(FurnaceRecord furnaceRecord);
	/**
	 * 删除火化炉信息
	 * @param id 
	 */
	void deleteFurnaceRecord(String id);
	/**
	 * 修改火化炉信息
	 * @param furnaceRecord 
	 */
	void updateFurnaceRecord(FurnaceRecord furnaceRecord);

	/**
	 * 获取火化炉信息对象
	 * @param id
	 * @return
	 */	
	FurnaceRecord getFurnaceRecordById(String id);
	/**
	 * 根据条件获取火化炉信息List
	 * @return
	 */
	List<FurnaceRecord> getFurnaceRecordList(Map<String, Object> paramMap);
	/**
	 * 获取火化炉的总记录
	 * @param map
	 * @return
	 */
	int getFurnaceRecordCount(Map<String,Object> map);
	@Update("update y_furnace_record set views_pt=#{1},views_ty=#{1} where id=#{0}")
	public void updateNumber(String id, byte b);
	@Delete("delete from y_furnace_record where commission_order_id=#{id}")
	void deleteFurnaceRecordByCoId(String id);
	/**
	 * 解除当前用户 锁定限制
	 * @param userId
	 * @param flag
	 * @param commissionOrderId
	 */
	@Delete("delete from y_furnace_record where creat_user_id=#{0} and flag=#{1} and commission_order_id is #{2}")
	void deleteFurnaceRecordUnLock(String userId,byte flag,String commissionOrderId);
}
