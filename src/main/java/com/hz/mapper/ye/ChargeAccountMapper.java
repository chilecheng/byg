package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.ChargeAccount;
import com.hz.entity.ye.ItemOrder;


/**
 * 挂账业务 持久层设置
 * @author jgj
 *
 */
public interface ChargeAccountMapper {
	/**
	 * 查询挂账业务列表
	 */
	public List<ChargeAccount> getChargeAccountList();
	
	/**
	 * 根据id查找挂账业务
	 * @param id
	 * @return
	 */
	public ChargeAccount getChargeAccountById(String id);
	
	/**
	 * 根据业务单号查找服务项目及丧葬用品收费
	 * @param map
	 * @return
	 */
	public List<ItemOrder> getProjectFeesByItem(Map<String,Object> paramMap);
	
	/**
	 * 根据条件删除挂账业务
	 * @param map
	 */
	public void delChargeAccount(Map<String,Object> paramMap);
}
