package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.MourningRecord;

/**
 * 困难减免申请项目
 * @author rgy
 *
 */
public interface HardReductionDMapper {
	/**
	 * 添加困难减免申请项目
	 * @param hardReductionD
	 */
	void saveHardReductionD(HardReductionD hardReductionD);
	/**
	 * 删除 困难减免申请项目
	 * @param id
	 */
	void deleteHardReductionD(String id);
	/**
	 * 更改 困难减免申请项目
	 * @param hardReductionD
	 */
	void updateHardReductionD(HardReductionD hardReductionD);
	/**
	 * 获取困难减免申请项目
	 * @param id
	 * @return
	 */
	HardReductionD getHardReductionDById(String id);
	/**
	 * 根据条件获取 困难减免申请项目list
	 * @param paramMap
	 * @return
	 */
	List<HardReductionD> getHardReductionDList(Map<String, Object>paramMap);
	/**
	 * 委托收费
	 * 根据条件获取 困难减免申请项目list
	 * @param paramMap
	 * @return
	 */
	List<HardReductionD> getHardReductionDFeeList(Map<String, Object>paramMap);
	/**
	 * 根据业务订单流水号 更改困难减免的扣除时间
	 * @param paramMap
	 */
	@Update("update	y_hard_reduction_d set pay_time=#{payTime} where order_number=#{orderNumber}")
	void updateHardPayTime(Map<String,Object> paramMap);
	/**
	 * 根据业务订单流水号 统计金额
	 * @param orderNumber
	 * @return
	 */
	@Select("select IFNULL(SUM(total),0) from y_hard_reduction_d where order_number=#{orderNumber}")
	double gethardTotalByOrderNumber(String orderNumber);
	/**
	 * 根据条件 统计基本减免金额
	 * @param map
	 * @return
	 */
	double getHardTotal(Map<String,Object> map);
}
