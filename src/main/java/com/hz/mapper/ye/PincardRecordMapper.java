package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.PincardRecord;

/**
 * 销卡记录
 * @author hw
 *
 */
public interface PincardRecordMapper {
	/**
	 * 添加销卡记录
	 * @param pincardRecord
	 */
	void savePincardRecord(PincardRecord pincardRecord);
	/**
	 * 删除销卡记录
	 * @param id
	 */
	void deletePincardRecord(String id);
	/**
	 * 更改销卡记录
	 * @param pincardRecord
	 */
	void updatePincardRecord(PincardRecord pincardRecord);
	/**
	 * 获取销卡记录
	 * @param id
	 * @return
	 */
	PincardRecord getPincardRecordById(String id);
	/**
	 * 根据条件获取销卡记录list
	 * @param paramMap
	 * @return
	 */
	List<PincardRecord> getPincardRecordList(Map<String,Object> paramMap);
}
