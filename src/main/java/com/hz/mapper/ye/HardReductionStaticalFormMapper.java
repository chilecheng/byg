package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.staticalForm.HardReductionStaticalForm;

public interface HardReductionStaticalFormMapper {
	
	List<HardReductionStaticalForm> findHardReductionSF(Map<String,Object> paramMap);
}
