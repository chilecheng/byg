package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.FireAttestPrintInfo;
import com.hz.entity.ye.PrintRecord;
/**
 * 三科火化证明打印
 * @author lpp
 *
 */
public interface FireAttestPrintMapper {
	
	/**
	 * 得到卡号对应基本信息
	 * @param cardCode
	 * @return
	 */
	FireAttestPrintInfo getBasicInfoByCard(String cardCode);
	/**
	 * 插入打印记录
	 * @param pr
	 */
	void addPrintRecord(PrintRecord pr);
	
	/**
	 * 根据cid得到实际火化时间
	 * @param cid
	 * @return
	 */
	String getActualFireTimeByCId(String cid);
	
	/**
	 * 销卡
	 * @param cid
	 */
	void destoryCardById(String cid);
	/**
	 * 查询火化证明打印信息
	 * @param map
	 * @return
	 */
	public List<PrintRecord> getPrintRecordByMap(Map map);
	/**
	 * 更新火化证明打印信息
	 * @param id
	 */
	public void updatePrintRecord(PrintRecord pr);
	public PrintRecord getPrintRecordById(String id);
	/**
	 * 根据条件获取火化证明打印信息
	 * @param map
	 * @return
	 */
	public List<FireAttestPrintInfo> getBasicInfoList(Map<String,Object> map);
}
