package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.CarSchedulRecord;

public interface CarSchedulRecordMapper {
	/**
	 * 添加车辆调度记录
	 * @param carSchedulRecord
	 */
	void saveCarSchedulRecord(CarSchedulRecord carSchedulRecord);
	/**
	 * 删除车辆调度记录
	 * @param id 
	 */
	void deleteCarSchedulRecord(String id);
	/**
	 * 更新车辆调度记录
	 * @param corpseUnit 
	 */
	void updateCarSchedulRecord(CarSchedulRecord carSchedulRecord);
	/**
	 * 获取车辆调度记录对象
	 * @param id
	 * @return
	 */
	CarSchedulRecord getCarSchedulRecordById(String id);
	/**
	 * 根据条件获取车辆调度记录List
	 * @return
	 */
	List<CarSchedulRecord> getCarSchedulRecordList(Map<String,Object> paramMap);
	/**
	 * 车辆调度进入列表方法
	 * @param paramMap
	 * @return
	 */
	List<CarSchedulRecord> getCarList(Map<String, Object> paramMap);
	/**
	 * 根据车辆调度表ID查询死者信息
	 */
	CarSchedulRecord getCarSchedulRecord(String id);
	/**
	 * 根据车辆调度表ID查询记录首页用
	 */
	@Select ("select * from y_car_schedul_record WHERE id=#{id}")
	CarSchedulRecord getCarSchedulRecordHome(String id);
	/**
	 * 撤销调度/完成调度
	 * @param id 
	 */
	int updateCarSchedulRecordFlag(Map<String, Object> map);
	
	public int getNumber(Map<String, Object> map);
	public void updateNumber(CarSchedulRecord car);
}