package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;

/**
 * 骨灰寄存缴费，续费记录
 * @author rgy
 *
 */
public interface AshesRecordDMapper {
	/**
	 * 添加骨灰寄存缴费，续费记录
	 * @param ashesRecordD
	 */
	void saveAshesRecordD(AshesRecordD ashesRecordD);
	/**
	 * 删除骨灰寄存缴费，续费记录
	 * @param id
	 */
	void deleteAshesRecordD(String id);
	/**
	 * 更改骨灰寄存缴费，续费记录
	 * @param ashesRecordD
	 */
	void updateAshesRecordD(AshesRecordD ashesRecordD);
	/**
	 * 获取骨灰寄存缴费，续费记录
	 * @param id
	 * @return
	 */
	AshesRecordD getAshesRecordDById(String id);
	/**
	 * 根据条件获取骨灰寄存缴费，续费记录list
	 * @param paramMap
	 * @return
	 */
	List<AshesRecordD> getAshesRecordDList(Map<String,Object> paramMap);
	/**
	 * 收费科非委托业务查询之骨灰寄存
	 * @param paramMap
	 * @return
	 */
	public List<AshesRecordD> getAshesRecordDFee(Map<String,Object> paramMap);
}
