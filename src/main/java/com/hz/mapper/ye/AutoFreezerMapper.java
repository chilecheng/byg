package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.AutopsyRecord;
import com.hz.entity.ye.FreezerRecord;

/**
 * 验尸申请
 * @author non
 *
 */
public interface AutoFreezerMapper {
	List<FreezerRecord> listAutoFreezer(Map paramMap);
	
	List findAutoFreezerById(String id);

	List<AutopsyRecord> listunAutoFreezer(Map paramMap);
	/**
	 * 通过id 来判断是否存在 未验尸的遗体
	 * @param id
	 * @param flag
	 * @return
	 */
	@Select("select  a.Id  from  y_autopsy_record a where a.commission_order_id =#{0}  and a.flag= #{1}") 
	String getBoolAuto(String id,byte flag);
	
	
}
