package com.hz.mapper.ye.staticalForm;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.PincardRecord;

/**
 * 销卡记录
 * @author rgy
 *
 */
public interface CloseCardRecordFormMapper {

	/**
	 * 获取销卡记录
	 * @param map
	 * @return 
	 */
	List<PincardRecord> getCloseCardRecordList(Map<String, Object> map);
	
}
