package com.hz.mapper.ye.staticalForm;

import java.util.List;
import java.util.Map;
import com.hz.entity.ye.CarSchedulRecord;

public interface CarSchedualFormMapper {
	/**
	 * 获取车辆调度列表
	 * @param paramMap
	 * @return
	 */
	List<CarSchedulRecord> getCarList(Map<String,Object> paramMap);
	
	
}
