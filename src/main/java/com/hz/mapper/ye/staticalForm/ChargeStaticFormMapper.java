package com.hz.mapper.ye.staticalForm;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.DelegateRecord;
import com.hz.entity.ye.staticalForm.ChargeStaticForm;
/**
 * 收费项目统计表
 * @author gpf
 *
 */

public  interface ChargeStaticFormMapper {
	
	//获取收费项目list表 
	List<ChargeStaticForm> getchargeStaticList(Map<String,Object> paramMap) ;
	//获取 骨灰寄存 表 list
	List<ChargeStaticForm> getashchargeStaticList(Map<String,Object> paramMap) ;
	
	DelegateRecord getOrderById(String id , byte checkFlag ,byte tickFlag);
	ChargeStaticForm sumStaticChargeRecord(Map<String,Object> paramMap);

}
