package com.hz.mapper.ye.staticalForm;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.FarewellTaskList;

public interface FarewellTaskMapper {

	List<FarewellTaskList> getFarewellTaskList(Map<String, Object> map);
	/**
	 * 更改
	 * @param ftl
	 */
	void updateFarewellTaskList(FarewellTaskList ftl);
	/**
	 * 获取对象
	 * @param id
	 * @return
	 */
	FarewellTaskList getFarewellTaskListById(String id);


}
