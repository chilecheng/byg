package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;
import com.hz.entity.ye.staticalForm.BaseReductionStaticalForm;
public interface BaseReductionStaticalFormMapper {
	
	List<BaseReductionStaticalForm> findBaseReductionSF(Map<String,Object> paramMap);
}
