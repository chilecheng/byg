package com.hz.mapper.ye;

import java.util.Map;

import com.hz.entity.ye.ThreeAndOne;

public interface ThreeAndOneMapper {
 
	/**
	 * 获取火化委单 可以直接获取的属性值
	 * @param id
	 * @return
	 */
	ThreeAndOne getInformation (String id);
	/**
	 * 获取 一类收费项目的总和
	 * @param param
	 * @return
	 */
	String  getOneItemPrice(Map<String,Object> param);
	/**
	 * 获取  火化委托单 id
	 * @param param
	 * @return
	 */
	String getCommissionOrderId(Map<String,Object> param);
}
