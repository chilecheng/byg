package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Update;

import com.hz.entity.ye.ListFueralRecord;

/**
 * 礼仪出殡的火化委托单记录
 * @author czl
 *
 */
public interface FuneralRecordMapper {
	public List<ListFueralRecord>  findFuneralRecord(Map<String,Object> paramMap);
	
	
	/**
	 * 首页使用:礼仪出殡列表
	 * @param map
	 * @return
	 */
	public List<ListFueralRecord> getFuneralRecordList(Map<String,Object> map);
	public int getNumber(Map<String,Object> map);
	public void updateNumber(ListFueralRecord record);
	@Update("update y_item_order set views_funeral=#{1} where id=#{0}")
	public void updateViewsFuneral(String id,byte one);
}

