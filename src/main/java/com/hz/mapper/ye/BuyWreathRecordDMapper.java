package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.BuyWreathRecordD;

/**
 * 花圈花篮订购项目
 * @author hw
 *
 */
public interface BuyWreathRecordDMapper {
	/**
	 * 添加花圈花篮订购记录
	 * @param buyWreathRecordD
	 */
	void saveBuyWreathRecordD(BuyWreathRecordD buyWreathRecordD);
	/**
	 * 删除花圈花篮订购记录
	 * @param id
	 */
	void deleteBuyWreathRecordD(String id);
	/**
	 * 更改花圈花篮订购记录
	 * @param buyWreathRecordD
	 */
	void updateBuyWreathRecordD(BuyWreathRecordD buyWreathRecordD);
	/**
	 * 获取花圈花篮订购记录
	 * @param id
	 * @return
	 */
	BuyWreathRecordD getBuyWreathRecordDById(String id);
	/**
	 * 根据条件获取花圈花篮订购记录list
	 * @param paramMap
	 * @return
	 */
	List<BuyWreathRecordD> getBuyWreathRecordDList(Map<String, Object> paramMap);
	/**
	 * 收费科非委托业务使用
	 * @param paramMap
	 * @return
	 */
	public List<BuyWreathRecordD> getBuyWreathRecordDFee(Map<String,Object> paramMap);
}
