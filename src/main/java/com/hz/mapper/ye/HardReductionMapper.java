package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.HardReduction;

/**
 * ���Ѽ�������
 * @author rgy
 *
 */
public interface HardReductionMapper {
	/**
	 * �������Ѽ�������
	 * @param hardReduction
	 */
	void saveHardReduction(HardReduction hardReduction);
	/**
	 * ɾ�� ���Ѽ�������
	 * @param id
	 */
	void deleteHardReduction(String id);
	/**
	 * ���� ���Ѽ�������
	 * @param hardReduction
	 */
	void updateHardReduction(HardReduction hardReduction);
	/**
	 * ��ȡ���Ѽ�������
	 * @param id
	 * @return
	 */
	HardReduction getHardReductionById(String id);
	/**
	 * ����������ȡ ���Ѽ�������list
	 * @param paramMap
	 * @return
	 */
	List<HardReduction> getHardReductionList(Map<String, Object>paramMap);
	/**
	 * ����ί�е�id��ѯ�����״̬
	 * @param commissionOrderId
	 * @return
	 */
	@Select("select check_flag from y_hard_reduction where commission_order_id=#{commissionOrderId}")
	String getHardCheckFlagByComId(String commissionOrderId);
}
