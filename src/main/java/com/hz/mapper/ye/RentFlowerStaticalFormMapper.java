package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Item;
import com.hz.entity.ye.staticalForm.RentFlowerStaticalForm;

public interface RentFlowerStaticalFormMapper {
	/**
	 * 查询集合
	 * @param paramMap
	 * @return
	 */
	List<RentFlowerStaticalForm> findRentFlowerSF(Map<String,Object> paramMap);
	/**
	 * 项目集合
	 * @param paramMap
	 * @return
	 */
	List<Item> findItemNameList(Map<String, Object> paramMap);
	
	/**
	 * 统计数量
	 * @param paraMap
	 * @return
	 */
	int getItemNameIdCount(Map<String,Object> paraMap); 
}
