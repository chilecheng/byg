package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;

/**
 * 基本减免申请项目
 * @author rgy
 *
 */
public interface BaseReductionDMapper {
	/**
	 * 添加 基本减免申请项目
	 * @param BaseReductionD
	 */
	void saveBaseReductionD(BaseReductionD baseReductionD);
	/**
	 * 删除 基本减免申请项目
	 * @param id
	 */
	void deleteBaseReductionD(String baseId);
	/**
	 * 更改 基本减免申请项目
	 * @param BaseReductionD
	 */
	void updateBaseReductionD(BaseReductionD baseReductionD);
	/**
	 * 获取 基本减免申请项目
	 * @param id
	 * @return
	 */
	BaseReductionD getBaseReductionDById(String id);
	/**
	 * 根据条件获取 基本减免申请项目list
	 * @param paramMap
	 * @return
	 */
	List<BaseReductionD> getBaseReductionDList(Map<String, Object> paramMap);
	/**
	 * 收费科
	 * 根据条件获取 基本减免申请项目list
	 * @param paramMap
	 * @return
	 */
	List<BaseReductionD> getBaseReductionDFeeList(Map<String, Object> paramMap);
	/**
	 * 撤消收费时同时更改 基本减免扣除时间
	 * @param paramMap
	 */
	@Update("update	y_base_reduction_d set pay_time=#{payTime} where order_number=#{orderNumber}")
	void updateBasePayTime(Map<String,Object> paramMap);
	/**
	 * 根据业务订单流水号 统计金额
	 * @param orderNumber
	 * @return
	 */
	@Select("select IFNULL(SUM(total),0) from y_base_reduction_d where order_number=#{orderNumber}")
	double getbaseTotalByOrderNumber(String orderNumber);
	/**
	 * 根据条件 统计基本减免金额
	 * @param map
	 * @return
	 */
	double getBaseTotal(Map<String,Object> map);
}
