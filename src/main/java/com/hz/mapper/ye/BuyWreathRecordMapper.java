package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.BuyWreathRecord;

/**
 * 花圈花篮订购
 * @author hw
 *
 */
public interface BuyWreathRecordMapper {
	/**
	 * 添加花圈花篮订购记录
	 * @param buyWreathRecord
	 */
	void saveBuyWreathRecord(BuyWreathRecord buyWreathRecord);
	/**
	 * 删除花圈花篮订购记录
	 * @param id
	 */
	void deleteBuyWreathRecord(String id);
	/**
	 * 更改花圈花篮订购记录
	 * @param buyWreathRecord
	 */
	void updateBuyWreathRecord(BuyWreathRecord buyWreathRecord);
	/**
	 * 获取花圈花篮订购记录
	 * @param id
	 * @return
	 */
	BuyWreathRecord getBuyWreathRecordById(String id);
	/**
	 * 根据条件获取花圈花篮订购记录list
	 * @param paramMap
	 * @return
	 */
	List<BuyWreathRecord> getBuyWreathRecordList(Map<String, Object> paramMap);
	List<BuyWreathRecord> getListBuyWreathRecord(Map<String, Object> paramMap);

}
