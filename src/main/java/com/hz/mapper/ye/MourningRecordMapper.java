package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;

import com.hz.entity.ye.MourningRecord;

/**
 * 灵堂记录
 * @author rgy
 *
 */
public interface MourningRecordMapper {
	/**
	 * 添加灵堂记录
	 * @param mourningRecord
	 */
	void saveMourningRecord(MourningRecord mourningRecord);
	/**
	 * 删除灵堂记录
	 * @param id
	 */
	void deleteMourningRecord(String id);
	/**
	 * 更改灵堂记录
	 * @param mourningRecord
	 */
	void updateMourningRecord(MourningRecord mourningRecord);
	/**
	 * 获取灵堂记录
	 * @param id
	 * @return
	 */
	MourningRecord getMourningRecordById(String id);
	/**
	 * 根据条件获取灵堂记录list
	 * @param paramMap
	 * @return
	 */
	List<MourningRecord> getMourningRecordList(Map<String, Object> paramMap);
	List<MourningRecord> getMourningRecordListMap(Map<String, Object> paramMap);
	/**
	 * getMourningRecordByOId
	 * @param id
	 * @return
	 */
	MourningRecord getMourningRecordByOId(String id);
	/**
	 * 只查灵堂记录和灵堂信息
	 * @param id
	 * @return
	 */
	MourningRecord getMourningRecord(String id);
	/**
	 * 布置灵堂信息
	 * @param mourningRecord
	 */
	void arrangeFarewell(MourningRecord mourningRecord);
	
	int getMourningRecordCount(Map<String, Object> map);
	@Delete("delete from y_mourning_record where commission_order_id=#{coId}")
	void deleteMourningRecordByCoId(String coId);
	
	@Update("update y_mourning_record set isdel =#{1}   where commission_order_id =#{0}")
	void updateMourningRecordIsDeal(String id, byte isdel);
	/**
	 * 更改灵堂记录状态，出柜保存处在使用
	 * @param maps
	 */
	@Update("update y_mourning_record set flag =#{1} where commission_order_id=#{0}")
	void updateMourningRecordFlag(String commissionOrderId,byte flag);
	/**
	 * 根据当前用户   灵堂解锁
	 * @param userId
	 * @param flag
	 * @param commissionOrderId
	 */
	@Delete("delete from y_mourning_record where creat_user_id=#{0} and flag=#{1} and commission_order_id is #{2}")
	void deleteMourningRecordUnlock(String userId,byte flag,String commissionOrderId);
}
