package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hz.entity.ye.CommissionOrder;

public interface CommissionOrderMapper {
	/**
	 * 添加火化委托单
	 * @param commissionOrder
	 */
	void saveCommissionOrder(CommissionOrder commissionOrder);
	/**
	 * 删除火化委托单
	 * @param id 
	 */
	void deleteCommissionOrder(String id);
	/**
	 * 更新火化委托单
	 * @param corpseUnit 
	 */
	void updateCommissionOrder(CommissionOrder commissionOrder);
	/**
	 *  销卡专用
	 * @param id
	 * @param cardCode
	 * @param pincardFlag
	 */	
	@Update("update y_commission_order set card_code =#{1} ,pincard_flag=#{2}  where id=#{0}")
	void deleateCardCode(String id,String cardCode,byte pincardFlag);
	/**
	 * 挂账业务在审核时,更改pay_flag状态
	 * @param maps
	 */
	@Update("update y_commission_order set pay_flag =#{payFlag} where id=#{commissionOrderId}")
	void updateCommissionOrderPayFlag(Map<String,Object> maps);
	/**
	 * 更新火化委托单 的状态 
	 * 已到馆  ，已收费，已火化未打印 ，已火化已打印
	 * @param id
	 * @param scheduleFlag
	 * @return 
	 */
	@Update("update y_commission_order set schedule_flag =#{1} where id=#{0}")
	void updateCommissionOrderScheduleFlag(String id,byte scheduleFlag);
	
	/**
	 * 审核/取消审核 火化委托单
	 */
	@Update("UPDATE y_commission_order SET check_flag = #{checkFlag },check_time = #{checkTime} WHERE id=#{commissionOrderId}")
	void checkOrNoCommissionOrder(Map<String, Object> maps);
	/**
	 * 获取火化委托单对象
	 * @param id
	 * @return
	 */
	CommissionOrder getCommissionOrderById(String id);
	/**
	 * 同样获取火化委托单对象（查询历史与最新）
	 * @param id
	 * @return
	 */
	CommissionOrder getComByIdNewAndOld(String id);
	@Select("select id,f_phone as fPhone,name,f_name as fName,code,sex,age,cremation_time as cremationTime from y_commission_order where id = #{id}")
	CommissionOrder getCommissionOrderByIdChoose1(Map<String, Object> map);
	@Select("select name from y_commission_order where id = #{id}")
	CommissionOrder getCommissionOrderByIdChoose2(Map<String, Object> map);//骨灰寄存保存时
	/**
	 * 根据卡号获取火化委托单对象
	 * @param cardCode
	 * @return
	 */
	CommissionOrder getCommissionOrderByCardCode(String cardCode);
	/**
	 * 获取火化委托单对象(审核专用)
	 * @param id
	 * @return
	 */
	CommissionOrder getCommissionOrderByIdForCheck(String id);
	/**
	 * 根据条件获取火化委托单List
	 * @return
	 */
	List<CommissionOrder> getCommissionOrderList(Map<String, Object> paramMap);
	/**委托业务综合查询
	 * @param paramMap
	 * @return
	 */
	List<CommissionOrder> getWeiTuoList(Map<String, Object> paramMap);
	/**
	 * 查询此卡号有没有重复
	 */
	int getCommissionOrderByCard(String cardCode);
	/**
	 * 统计数量
	 * @param map
	 * @return
	 */
	int getProveUnitIdCount(Map<String, Object> map);
	/**
	 * 根据ID获取卡号
	 */
	String getCardCodeById(String id);
	/**
	 * 首页未收费列表
	 * @return
	 */
	public List<CommissionOrder> getUnPayList(Map<String,Object> map);
	public List<CommissionOrder> getCrematorList(Map<String,Object> map);
	public int getNumber(Map<String,Object> map);
	public void updateNumber(CommissionOrder com);
	public int getNumberCremator(Map<String,Object> map);
	/**
	 * 获取火化委托单List(调度科火化委托单列表用)
	 */
	List<CommissionOrder> getCoList1(Map<String, Object> paramMap);
	/**
	 * 调度科 骨灰寄存的时候  搜索姓名、id用
	 */
	@Select("SELECT co.id,co.`name` FROM y_commission_order co")
	@Results(
		{
		@Result(id = true, column = "id", property = "id"),
		@Result(column = "name", property = "name")
	})
	List<CommissionOrder> getCoList2();
	/**
	 * 花圈花篮购买页面 点姓名搜索的弹出框
	 */
	List<CommissionOrder> getCoList3(Map<String, Object> paramMap);
	/**
	 * 永嘉登记列表
	 */
	List<CommissionOrder> getCoList4(Map<String, Object> paramMap);
	/**
	 * 获取审核状态(判断能否删除火化委托单用)
	 */
	@Select("SELECT check_flag FROM y_commission_order co WHERE id = #{coId}")
	byte getCheckFlag(String coId);
	@Update("UPDATE y_commission_order SET views_uncheck=#{1} WHERE id=#{0}")
	void updateNumber2(String id, byte one);
	@Update("UPDATE y_commission_order SET views_unpay=#{1} WHERE id=#{0}")
	void updateNumber3(String id, byte one);
	/**
	 * 改变收费状态
	 * @param id
	 * @param flag
	 */
	@Update("UPDATE y_commission_order SET pay_flag=#{1} WHERE id=#{0}")
	void updatePayFlag(String id ,byte flag);
	@Select("SELECT id,farewell_id,farewell_time,mourning_id,mourning_time,creat_time FROM y_commission_order WHERE id=#{coId}")
	@Results(
		{
		@Result(id = true, column = "id", property = "id"),
		@Result(column = "farewell_id", property = "farewellId"),
		@Result(column = "farewell_time", property = "farewellTime"),
		@Result(column = "mourning_id", property = "mourningId"),
		@Result(column = "mourning_time", property = "mourningTime"),
		@Result(column = "creat_time", property = "creatTime")
	})
	CommissionOrder getCommissionOrderByIdToDeletNotice(String coId);
	/**
	 * 综合业务 打印查询功能
	 * @param id
	 * @return
	 */
	CommissionOrder getCommissionCheckPrintById(String id);

}