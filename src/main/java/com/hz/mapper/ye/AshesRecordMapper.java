package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.AshesRecord;

/**
 * �ǻҼĴ��¼
 * @author hw
 *
 */
public interface AshesRecordMapper {
	/**
	 * ���ӹǻҼĴ��¼
	 * @param ashesRecord
	 */
	void saveAshesRecord(AshesRecord ashesRecord);
	/**
	 * ɾ���ǻҼĴ��¼
	 * @param id
	 */
	void deleteAshesRecord(String id);
	/**
	 * ���ĹǻҼĴ��¼
	 * @param ashesRecord
	 */
	void updateAshesRecord(AshesRecord ashesRecord);
	/**
	 * ��ȡ�ǻҼĴ��¼
	 * @param id
	 * @return
	 */
	AshesRecord getAshesRecordById(String id);
	/**
	 * ����������ȡ�ǻҼĴ��¼list
	 * @param paramMap
	 * @return
	 */
	List<AshesRecord> getAshesRecordList(Map<String,Object> paramMap);
	/**
	 * �жϸ������Ƿ��Ѿ��Ĵ�
	 */
	@Select("SELECT COUNT(1) FROM y_ashes_record WHERE commission_id=#{coId}")
	int getIfexit(String coId);

}
