package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.ItemChange;
/**
 * 丧葬用品更改记录
 * @author jgj
 *
 */
public interface ItemChangeMapper {
	
	void saveItemChange(ItemChange itemChange);
	void delItemChange(String id);
	void delItemChangeBycomId(String id);
	ItemChange getItemChangeById(String id);
	List<ItemChange> getItemChangeList(Map<String,Object> paramMap);
	void delItemChangeByBuyId(String id);
	

	
}