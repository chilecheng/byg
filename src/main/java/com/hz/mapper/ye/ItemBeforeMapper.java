package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.ItemOrder;
/**
 * 丧葬用品更改之前的记录
 * @author jgj
 *
 */
public interface ItemBeforeMapper {
	/**
	 * 添加火化委托单收费项目
	 * @param itemOrder
	 */
	void saveItemOrderBefore(ItemOrder itemOrder);
	/**
	 * 删除火化委托单收费项目
	 * @param id 
	 */
	void delItemOrderBefore(String id);
	void delItemOrderBeforeBycomId(String id);
	/**
	 * 更新火化委托单收费项目
	 * @param corpseUnit 
	 */
	void updateItemOrderBefore(ItemOrder itemOrder);	
	
	/**
	 * 获取火化委托单收费项目对象
	 * @param id
	 * @return
	 */
	ItemOrder getItemOrderBeforeById(String id);
	/**
	 * 根据条件获取火化委托单收费项目List
	 * @return
	 */
	List<ItemOrder> getItemOrderBeforeList(Map<String,Object> paramMap);
	

	
}