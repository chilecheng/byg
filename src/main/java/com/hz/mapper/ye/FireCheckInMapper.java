package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.ComOrSpeFuneralRecord;
import com.hz.entity.ye.FireCheckInInfo;
import com.hz.entity.ye.FireFurnaceInfo;
import com.hz.entity.ye.FireWorkerInfo;
import com.hz.entity.ye.PaperInfo;
import com.hz.entity.ye.PolitenessInfo;

/**
 * 三科火化登记
 * @author lipengpeng
 *
 */
public interface FireCheckInMapper {
	
	/**
	 * 根据卡号查死者基本信息
	 */
	FireCheckInInfo getBasicInfoByCard(String cardCode);
	
	/**
	 * 根据卡号查询纸棺和出殡礼仪两项信息
	 */
	PaperInfo getPaperInfoByCard(String cardCode,String Paper);

	/**
	 * 根据卡号得到礼仪出殡信息
	 */
	PolitenessInfo getPolitenessInfoByCard(String cardCode,int sort);
	/**
	 * 查找所有火化工
	 */
	List<FireWorkerInfo> findAllFireWorker(String firename);//firename是固定值，为“火化工”
	/**
	 * 查找所有特约和普通炉
	 */
	List<FireFurnaceInfo> findAllSpecialOrCommonFurnace(String furnacename);
	/**
	 * 根据委托单id找火化炉id
	 */
	String findFurnaceIdBycommissionId(String cid);
	/**
	 * 插入火化炉记录
	 */
	void insertCommonOrSpecialFurnaceRecord(ComOrSpeFuneralRecord csfr);
	/**
	 * 更改未转炉记录
	 */
	void updateNotChangeFurnaceRecord(ComOrSpeFuneralRecord csfr);
	//备选
	void updateNotChangeFurnaceRecord2(ComOrSpeFuneralRecord cr);
	/**
	 * 更新转炉记录
	 */
	void updateChangeFurnaceRecord(ComOrSpeFuneralRecord csfr);
	//备选
	void updateChangeFurnaceRecord2(ComOrSpeFuneralRecord cr);
	/**
	 * 根据原火化炉id找火化炉和委托单信息
	 * @param csfr
	 * @return
	 */
	String findOriginalIdByFurnaceAndCommission(ComOrSpeFuneralRecord csfr);
	/**
	 * 更改委托单火化标志
	 * @param map
	 */
	void updateCommissionFlagById(Map<String, Object> map);
	/**
	 * 根据委托单id找火化炉信息
	 * @param cid
	 * @return
	 */
	ComOrSpeFuneralRecord findFurnaceInfoBycommissionId(String cid);

	/**
	 * 根据卡号拿到furnace_record中的非转炉记录ID
	 */
	String getRecordIdByCardcode(Map<String, Object> map);


}
