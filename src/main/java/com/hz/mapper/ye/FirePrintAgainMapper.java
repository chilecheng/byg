package com.hz.mapper.ye;

import java.util.List;
import com.hz.entity.ye.PrintAgain;
/**
 * 三科火化证明补打
 * @author jgj
 *
 */
public interface FirePrintAgainMapper {
	/**
	 * 插入打印记录
	 * @param pr
	 */
	void addPrintAgain(PrintAgain printAgain);
	/**
	 * 查询火化证明打印信息
	 * @param map
	 * @return
	 */
	public List<PrintAgain> getPrintAgainList(String printId);
}
