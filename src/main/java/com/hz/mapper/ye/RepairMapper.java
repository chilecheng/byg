package com.hz.mapper.ye;

import java.util.Map;

import com.hz.entity.ye.RepairInfo;

/**
 * 维修
 * @author cjb
 *
 */
public interface RepairMapper {

	/**
	 * 报修
	 * @param map
	 */
	void repair(Map<String, Object> map);

	/**
	 * 获取灵堂维修信息
	 * @param id
	 * @return
	 */
	RepairInfo getMourningRecord(String id);

	/**
	 * 获取告别厅维修信息
	 * @param id
	 * @return
	 */
	RepairInfo getFarewellRecord(String id);

	/**
	 * 获取火化炉维修信息
	 * @param id
	 * @return
	 */
	RepairInfo getFurnaceRecord(String id);
	/**
	 * 获取车辆维修信息
	 * @param id
	 * @return
	 */
	RepairInfo getCarRecord(String id);
	/**
	 * 获取 冰柜
	 * @param id
	 * @return
	 */
	RepairInfo getFreezerRecord(String id);
	/**
	 * 维修完成
	 * @param map
	 */
	void completeRepair(Map<String, Object> map);

	/**
	 * 更改维修信息
	 * @param map
	 */
	void changeInfo(Map<String, Object> map);

	
	
}
