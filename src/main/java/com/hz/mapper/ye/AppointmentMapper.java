package com.hz.mapper.ye;

import java.util.List;
import java.util.Map;

import com.hz.entity.ye.Appointment;

public interface AppointmentMapper {
	/**
	 * 根据条件查询 预约情况
	 * @param paramap
	 * @return
	 */
	List<Appointment> getAppointmentList(Map<String, Object> paramap);
	/**
	 * 保存预约记录
	 * @param appointment
	 */
	void saveAppointment(Appointment appointment);
	/**
	 * 更改预约记录
	 * @param appointment
	 */
	void updateAppointment(Appointment appointment);
	/**
	 * 删除预约记录
	 * @param id
	 */
	void delAppointment(String id);
	/**
	 * 根据ID查找预约记录
	 * @param id
	 * @return
	 */
	Appointment getAppointmentById(String id);
}
