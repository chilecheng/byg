package com.hz.mapper.socket;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hz.entity.socket.Notice;
import com.hz.entity.socket.UserNotice;

/**
 * @author cjb
 *
 */
public interface UserNoticeMapper {
	
	@Insert("INSERT INTO socket_user (id,user_id,dept_id,notice_type) VALUES(#{id},#{userId},#{deptId},#{noticeType})")
	void saveUserNotice(UserNotice un);
	@Delete("DELETE FROM socket_user WHERE user_id=#{userId}")
	int deleteUserNotice(String userId);
	/**
	 * 登录时获取通知数量
	 * 
	 * 
	 * 原本这里有错误，取值偶尔有2条以上的情况发生，暂不清楚是什么原因，用limit限制下
	 */
	@Select("SELECT sn.number FROM socket_number sn LEFT JOIN socket_user su ON sn.socket_user_id=su.id"
			+ " WHERE su.user_id=#{0} AND su.notice_type=#{1} AND sn.notice_date=#{2} limit 0,1")
	String getCount(String userId, String noticeType, java.sql.Date date);
	/**
	 * 点击查看后当天number清零
	 */
	@Update("UPDATE socket_number SET number=0 WHERE socket_user_id=(SELECT id FROM socket_user WHERE user_id=#{0} AND notice_type=#{1}) AND notice_date=#{2}")
	int updateUserNoticeByUser(String userId,String noticeType, java.sql.Date date);
	/**
	 * 查看是否存在所需通知的日期
	 */
	@Select("SELECT COUNT(1) FROM socket_number WHERE socket_user_id IN"
			+ " (SELECT su.id FROM(SELECT id FROM socket_user WHERE notice_type = #{0} LIMIT 1) AS su)"
			+ " AND notice_date = #{1}")
	int ifDateExit(String noticeType, java.sql.Date date);
	@Select("SELECT id FROM socket_user WHERE notice_type=#{noticeType}")
	List<String> getSocketIdList(String noticeType);
	/**
	 * 插入某模块某天的数据
	 */
	@Insert("INSERT INTO socket_number (id,socket_user_id,notice_date,number) VALUES(#{0},#{1},#{2},#{3})")
	int insertSocketNumber(String id,String socketUserId,java.sql.Date noticeDate,int number );
	/**
	 * 加1 或 减1
	 */
	@Update("UPDATE socket_number SET number=number+#{2} WHERE socket_user_id IN(SELECT id FROM socket_user WHERE notice_type=#{0}) AND notice_date=#{1}")
	int updateUserNoticeByNoticeType(String noticeType,java.sql.Date date, int i);
	
	@Update("update socket_number s "
			+ "set  s.number = s.number-1 "
			+ "where  s.socket_user_id in (  select  u.id from socket_user u where u.user_id = #{0}  and  u.notice_type=#{1}  ) "
			+ "and s.notice_date=#{2}")
	void updateUserNoticeByUserId(String userId,String notice_type,java.sql.Date notice_date);
	
	//获得某科室 所有员工的当日消息读取状态
	@Select ("select  u.id,u.user_id , n.number ,n.notice_date  from socket_user u "
			+ " join socket_number  n on  u.id = n.socket_user_id "
			+ "where  u.notice_type =#{0} and n.notice_date = #{1}")
	@Results(
			{
			@Result(id = true, column = "id", property = "id"),
			@Result(column = "number", property = "number"),
			@Result(column = "notice_date", property = "noticeDate"),
			@Result(column = "user_id", property = "userId"),
		})
	List<Notice>  getAllDayNotice(String noticeType,java.sql.Date date);
	
}
