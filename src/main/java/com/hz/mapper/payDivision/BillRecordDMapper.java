package com.hz.mapper.payDivision;

import java.util.List;
import java.util.Map;

import com.hz.entity.payDivision.BillRecordD;

/**
 * 收费科挂账项目收费
 * 
 */
public interface BillRecordDMapper {
	public void saveBillRecordD(BillRecordD billRecordD);
	public void deleteBillRecordD(String id);
	public void updateBillRecordD(BillRecordD billRecordD);
	public BillRecordD getBillRecordDById(String id);
	public List<BillRecordD> getBillRecordDList(Map<String,Object> paramMap);
}
