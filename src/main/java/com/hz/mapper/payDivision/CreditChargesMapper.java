package com.hz.mapper.payDivision;


import com.hz.entity.payDivision.CreditCharges;

/**
 *   新建挂账业务收费
 * @author jgj
 *
 */
public interface CreditChargesMapper {
	
	public void saveCreditCharges(CreditCharges creditCharges);
	
	public CreditCharges getCreditChargesByOrderId(String commissionOrderId);
}
