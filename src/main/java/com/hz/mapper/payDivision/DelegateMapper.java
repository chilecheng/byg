package com.hz.mapper.payDivision;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.ye.DelegateRecord;
import com.hz.entity.ye.DelegateRecordNew;


/**
 *   委托业务收费
 * @author gpf
 *
 */
public interface DelegateMapper {
	//获取 火化委托单相关联的 
	List<DelegateRecord> getdelegateList(Map<String,Object> paramMap);
	
	List<DelegateRecord> getOrderPayRecordList(Map<String,Object> paramMap);
	
	/**
	 * 新加：根据收费时间来搜索收费列表 
	 * @param paramMap
	 * @return
	 */
	List<DelegateRecordNew> getDelegateListGroupByTime(Map<String,Object> paramMap);
	/**
	 * // 生成    委托业务 支付记录
	 * @param DelegrateRecord
	 */
	void saveDelegateRecord(DelegateRecord delegrateRecord);
	
	/**
	 *   委托业务   记录
	 *   获取 火化委托单信息

	 */
	DelegateRecord getOrderById(String id , byte checkFlag ,byte tickFlag);
	/**
	 * 删除  记录
	 */
	void deleateDelegateRecord(String id);
	
	@Select ( "select y.pay_amount from y_order_pay_record y  where y.order_id=#{0} ")
	Double getpayAmountByorder(String orderId);
	@Select ( "select y.odd  from y_order_pay_record y  where y.order_id=#{0} ")
	Double getpayOddByorder(String orderId);
	//查询 frontpay
	@Select("select y.front_pay from y_order_pay_record y  where y.order_id=#{0}")
	Double getFreontPay(String orderId);
	//判断是否有付费记录
	@Select ("select count(y.pay_time) from y_order_pay_record y  where y.order_id=#{0}")
	int getBoolPayed(String orderId);
	// 查询 price bill  来判别是否属于第二次以后的收费
	@Select ("select  COALESCE(SUM( r.pay_type) ,0) from y_order_pay_record r  where r.order_id=#{0} and r.front_pay !='' ")
	int  getpricrDifferById(String orderId);
	
	//基本减免申请里用到，取个结算日期
	@Select ("select pay_time from y_order_pay_record where order_id=#{0}")
	Timestamp getPayTimeByOrderId(String orderId);
	//h火化委托单变更后 的收费
	void updateDelegateRecord(DelegateRecord delegrateRecord);


	
}
