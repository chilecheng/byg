package com.hz.mapper.payDivision;

import java.util.List;
import java.util.Map;

import com.hz.entity.payDivision.BillRecord;

/**
 * 收费科挂账收费记录
 * @author jgj
 *
 */
public interface BillRecordMapper {
	public void saveBillRecord(BillRecord billRecord);
	public void deleteBillRecord(String id);
	public void updateBillRecord(BillRecord billRecord);
	public BillRecord getBillRecordById(String id);
	public List<BillRecord> getBillRecordList(Map<String,Object> paramMap);
}
