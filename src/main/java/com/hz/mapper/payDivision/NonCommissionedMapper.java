package com.hz.mapper.payDivision;

import java.util.List;
import java.util.Map;

import com.hz.entity.payDivision.NonCommissioned;
/**
 * 非委托业务收费
 * @author jgj
 *
 */
public interface NonCommissionedMapper {
	
	/**
	 * 根据条件查找符合的非委托业务
	 * @param paramMap
	 * @return
	 */
	public List<NonCommissioned> getNonCommissionedChargeList(Map<String,Object> paramMap);
}
