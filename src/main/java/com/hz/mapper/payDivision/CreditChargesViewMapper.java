package com.hz.mapper.payDivision;

import java.util.List;
import java.util.Map;

import com.hz.entity.payDivision.CreditChargesView;

/**
 *   新建挂账业务收费  查看功能
 * @author jgj
 *
 */
public interface CreditChargesViewMapper {
	public List<CreditChargesView> getCreditChargesViewList(Map<String,Object> map);

	
}
