package com.hz.mapper.payDivision;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.payDivision.BusinessFees;

/**
 *   委托业务收费重做后
 * @author jgj
 *
 */
public interface BusinessFeesMapper {
	/**
	 * 搜索查看
	 * @param orderId
	 * @return
	 */
	BusinessFees getBusinessFeesByid(String orderId);
	/**
	 * 根据收费流水号搜索收费记录
	 * @param payNumber
	 * @return
	 */
	BusinessFees getBusinessFeesByPayNumber(String payNumber);
	/**
	 * 根据订单流水号查询收费记录
	 * @param payNumber
	 * @return
	 */
	@Select("select * from y_order_pay_record where order_number=#{orderNumber}")
	BusinessFees getBusinessFeesByOrderNumber(String payNumber);
	/**
	 * 根据火化委托单id查询收费记录（业务全视图用）
	 * @param commissionOrderId
	 * @return
	 */
	List<BusinessFees> getBusinessFeesByComId(String commissionOrderId);
	/**
	 * 保存收费
	 * @param businessFees
	 */
	void saveBusinessFees(BusinessFees businessFees);
	/**
	 * 撤消收费（删除）
	 * @param payNumber
	 */
	void deleateBusinessFees(String payNumber);
	/**
	 * 更改收费
	 * @param businessFees
	 */
	void updateBusinessFees(BusinessFees businessFees);
	
}
