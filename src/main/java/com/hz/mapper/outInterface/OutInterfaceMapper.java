package com.hz.mapper.outInterface;

import java.util.List;

import com.hz.entity.outInterface.FareInterface;
import com.hz.entity.outInterface.MourningInterface;

/**
 * 对 殡仪网的接口
 * @author jgj
 *
 */
public interface OutInterfaceMapper {
	
	List<FareInterface> getFarewellList(String beginTime);//获取告别信息
	
	List<MourningInterface> getMourningList(String beginTime);//获取灵堂信息
}
