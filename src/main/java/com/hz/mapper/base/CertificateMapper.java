package com.hz.mapper.base;

import java.util.List;
import java.util.Map;
import com.hz.entity.base.Certificate;
/**
 * 证件类
 * @author rgy
 *
 */
public interface CertificateMapper {
	/**
	 * 添加证件
	 * @param certificate 证件类
	 */
	void saveCertificate(Certificate certificate);
	/**
	 * 修改证件
	 * @param certificate 证件类
	 */
	void updateCertificate(Certificate certificate);
	/**
	 * 删除证件
	 * @param id 证件类id
	 */
	void deleteCertificate(String id);
	/**
	 * 查询证件Id
	 * @param id 证件类id
	 */
	Certificate getCertificateById(String id);
	/**
	 * 查询所有的证件
	 * @param paramMap  
	 */
	List<Certificate> getCertificateList(Map<String, Object> paramMap);
	/**
	 * 查询所有的证件
	 * @param paramMap  
	 */
	List<Certificate> getCertificateList();
}
 
