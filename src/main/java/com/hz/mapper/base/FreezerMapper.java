package com.hz.mapper.base;

import com.hz.entity.base.Freezer;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Update;

public interface FreezerMapper {
	void saveFreezer(Freezer freezer);//添加冷藏柜信息
	
	void updateFreezer(Freezer freezer);//修改冷藏柜信息
	
	void deleteFreezer(String id);//删除冷藏柜信息
	
	Freezer getFreezerById(String id);//获取冷藏柜信息对象
	
	List<Freezer> getFreezerList(Map<String, Object> paramMap);//查询冷藏柜信息

	List<Freezer> getBlankFreezxer(Map<String, Object> paramMap); //查询处于空闲状态的冰柜相关信息

	@Update("update ba_freezer set user_name=null,s_name=null,flag=#{1} where id=#{0}")
	void clearFreezer(String freezerId, byte isflagNo);
	
	int getFreezerNumber(Map<String,Object> map);//按条件查询冰柜使用情况
	
}