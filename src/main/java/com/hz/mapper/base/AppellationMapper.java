package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Appellation;
/**
 * 称谓关系类
 * @author rgy
 *
 */
public interface AppellationMapper {
	/**
	 * 添加称谓关系
	 * @param appellation 称谓关系类
	 */
	void saveAppellation(Appellation appellation);
	/**
	 * 修改称谓关系
	 * @param appellation 称谓关系类
	 */
	void updateAppellation(Appellation appellation);
	/**
	 * 删除称谓关系
	 * @param id 称谓关系类id
	 */
	void deleteAppellation(String id);
	/**
	 * 查询称谓关系Id
	 * @param id 称谓关系类id
	 */
	Appellation getAppellationById(String id);
	/**
	 * 查询所有的称谓关系
	 * @param paramMap
	 */
	List<Appellation> getAppellationList(Map<String,Object> paramMap);
}
