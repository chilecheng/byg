package com.hz.mapper.base;
import java.util.List;
import java.util.Map;

import com.hz.entity.base.ProveUnit;
/**
 * 证明单位
 * @author rgy
 * */
public interface ProveUnitMapper {
	/**
	 * 添加证明单位
	 * @param proveUnit 
	 */
	void saveProveUnit(ProveUnit proveUnit);
	/**
	 * 删除证明单位
	 * @param id 
	 */
	void deleteProveUnit(String id);
	/**
	 * 修改证明单位
	 * @param proveUnit 
	 */
	void updateProveUnit(ProveUnit proveUnit);
	/**
	 * 查询证明单位id
	 * @param id
	 */
	ProveUnit getProveUnitById(String id);
	/**
	 * 查询所有证明单位
	 * @param paramMap
	 */
	List<ProveUnit> getProveUnitList(Map<String,Object> paramMap);
	
}
