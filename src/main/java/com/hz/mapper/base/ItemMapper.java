package com.hz.mapper.base;

import com.hz.entity.base.Item;

import java.util.List;
import java.util.Map;

public interface ItemMapper {
	/**
	 * 添加收费项目
	 */
	void saveItem(Item item);
	/**
	 * 删除收费项目
	 */
	void deleteItem(String id);
	/**
	 * 修改收费项目
	 */
	void updateItem(Item item);
	/**
	 * 获取收费项目对象
	 */
	Item getItemById(String id);
	/**
	 * 根据助记码来获取收费项目对象
	 * @param helpCode
	 * @return
	 */
	public Item getItemByHelpCode(String helpCode);
	/**
	 * 根据条件获取收费项目List
	 */
	List<Item> getItemList(Map<String,Object> paramMap);
	/**
	 * 根据服务类型获取收费项目List
	 */
	List<Item> getTypeItemList(Map<String, Object> paramMap);
	/**
	 * 判断该类中是否已经存在这个名字的项目
	 */
	int getCount(String name, String typeId);
	/**
	 * 判断助记码是否存在
	 */
	int ifHelpCodeExit(String string);
	/**
	 * 根据id查找类别
	 * @param map
	 * @return
	 */
	byte getNumberByTypeFlag(Map<String,Object> map);
}