package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Mourning;
/**
 * 灵堂信息
 * @author rgy
 */
public interface MourningMapper {
	/**
	 * 添加灵堂信息
	 * @param mourning灵堂信息
	 */
	void saveMourning(Mourning mourning);
	/**
	 * 修改灵堂信息
	 * @param mourning 灵堂信息
	 */
	void updateMourning(Mourning mourning);
	/**
	 * 删除灵堂信息
	 * @param id 灵堂信息的id
	 */
	void deleteMourning(String id);
	/**
	 * 查询灵堂信息id
	 * @param id 灵堂信息id
	 */
	Mourning getMourningById(String id);
	/**
	 * 根据名字查找
	 * @param name
	 * @return
	 */
	Mourning getMourningByName(String name);
	/**
	 * 查询所有灵堂信息
	 * @param paramMap
	 */
	List<Mourning> getMourningList(Map<String, Object> paramMap);
}
