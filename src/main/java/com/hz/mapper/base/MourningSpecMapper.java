package com.hz.mapper.base;
import java.util.List;
import java.util.Map;

import com.hz.entity.base.MourningSpec;
/**
 * 灵堂规格
 * @author rgy
 * */
public interface MourningSpecMapper {
	/**
	 * 添加灵堂规格
	 * @param mourningSpec 
	 */
	void saveMourningSpec(MourningSpec mourningSpec);
	/**
	 * 删除灵堂规格
	 * @param id 
	 */
	void deleteMourningSpec(String id);
	/**
	 * 修改灵堂规格
	 * @param mourningSpec 
	 */
	void updateMourningSpec(MourningSpec mourningSpec);
	/**
	 * 查询灵堂规格id
	 * @param id
	 */
	MourningSpec getMourningSpecById(String id);
	/**
	 * 查询所有灵堂规格
	 * @param paramMap
	 */
	List<MourningSpec> getMourningSpecList(Map<String,Object> paramMap);
	
}
