package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Toponym;
/**
 * 地名
 * @author rgy
 */
public interface ToponymMapper {
	/**
	 * 添加地名
	 * @param toponym 地名类
	 */
	void saveToponym(Toponym toponym);
	/**
	 * 修改地名
	 * @param toponym 地名类
	 */
	void updateToponym(Toponym toponym);
	/**
	 * 删除地名
	 * @param id 地名类的id
	 */
	void deleteToponym(String id);
	/**
	 * 查询地名id
	 * @param id 地名类的id
	 */
	Toponym getToponymById(String id);
	/**
	 * 查询所有地名
	 * @param paramMap
	 */
	List<Toponym> getToponymList(Map<String, Object> paramMap);
}
