package com.hz.mapper.base;

import java.util.List;
import java.util.Map;
import com.hz.entity.base.SpecialBusinessType;

/**
 * 特殊业务类型
 * @author jgj
 *
 */
public interface SpecialBusinessTypeMapper {
	/**
	 * 添加特殊业务类型信息
	 * @param Nation 民族信息
	 */
	void saveSpecialBusinessType(SpecialBusinessType specialBusinessType);
	/**
	 * 修改特殊业务类型信息
	 * @param nation 民族信息
	 */
	void updateSpecialBusinessType(SpecialBusinessType specialBusinessType);
	/**
	 * 删除特殊业务类型
	 * @param id 民族信息id
	 */
	void deleteSpecialBusinessType(String id);
	/**
	 * 查询特殊业务类型Id
	 * @param id 民族信息id
	 */
	SpecialBusinessType getSpecialBusinessTypeById(String id);
	/**
	 * 查询所有的特殊业务类型信息
	 * @param paramMap
	 */
	List<SpecialBusinessType> getSpecialBusinessTypeList(Map<String, Object> paramMap);
	
}
