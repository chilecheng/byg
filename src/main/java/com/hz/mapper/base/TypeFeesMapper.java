package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.TypeFees;
/**
 * 收费项目类型
 * @author jgj
 *
 */
public interface TypeFeesMapper {
	/**
	 * 添加收费项目类型信息
	 * @param TypeFees 
	 * 	 */
	public void saveTypeFees(TypeFees typeFees);
	/**
	 * 修改收费项目类型信息
	 * @param TypeFees
	 */
	public void updateTypeFees(TypeFees typeFees);
	/**
	 * 删除收费项目类型信息
	 * @param id 
	 */
	void deleteTypeFees(String id);
	/**
	 * 根据ID查询收费项目类型信息
	 * @param id
	 */
	public TypeFees getTypeFeesById(String id);
	/**
	 * 查询所有收费项目类型信息
	 * @param paramMap
	 */
	public List<TypeFees> getTypeFeesList(Map<String, Object> paramMap);
}
