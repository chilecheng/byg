package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Car;

public interface CarMapper {
	/**
	 * 添加车辆信息
	 * @param car
	 */
	void saveCar(Car car);
	/**
	 * 删除车辆信息
	 * @param id 
	 */
	void deleteCar(String id);
	/**
	 * 更新车辆信息
	 * @param corpseUnit 
	 */
	void updateCar(Car car);
	/**
	 * 获取车辆信息对象
	 * @param id
	 * @return
	 */
	Car getCarById(String id);
	/**
	 * 根据条件获取车辆信息List
	 * @return
	 */
	List<Car> getCarList(Map<String,Object> paramMap);
}
