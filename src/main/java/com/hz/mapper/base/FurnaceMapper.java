package com.hz.mapper.base;

import com.hz.entity.base.Furnace;
import com.hz.entity.ye.FurnaceRecord;
import java.util.List;
import java.util.Map;

public interface FurnaceMapper {
	/**
	 * 添加火化炉信息
	 * @param furnace
	 */
	void saveFurnace(Furnace furnace);
	/**
	 * 删除火化炉信息
	 * @param id 
	 */
	void deleteFurnace(String id);
	/**
	 * 修改火化炉信息
	 * @param corpseUnit 
	 */
	void updateFurnace(Furnace furnace);
	/**
	 * 获取火化炉信息对象
	 * @param id
	 * @return
	 */
	Furnace getFurnaceById(String id);
	/**
	 * 根据名字查找
	 * @param name
	 * @return
	 */
	Furnace getFurnaceByName(String name);
	/**
	 * 根据条件获取火化炉信息List
	 * @return
	 */
	List<Furnace> getFurnaceList(Map<String, Object> paramMap);
	/**
	 * 查找火化炉记录表
	 * @param maps
	 * @return
	 */
	List<FurnaceRecord> getFurnaceRecordList(Map<String, Object> maps);
	/**
	 * 获取特约炉ID
	 * @return
	 */
	String getSpecialId();
	/**
	 * 获取火化炉的记录
	 * @return
	 */
	int getFurnaceRecordCount(Map<String,Object> map);
	/**
	 * 判断是否存在
	 */
	FurnaceRecord getFurnaceRecord(Map<String, Object> furnaceRecordMaps);
}