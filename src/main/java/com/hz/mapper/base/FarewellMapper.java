package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Farewell;
/**
 * 告别厅
 * @author rgy
 */
public interface FarewellMapper {
	/**
	 * 添加告别厅
	 * @param farewell 告别厅
	 */
	void saveFarewell(Farewell farewell);
	/**
	 * 修改告别厅
	 * @param farewellType 告别厅
	 */
	void updateFarewell(Farewell farewell);
	/**
	 * 删除告别厅
	 * @param id 告别厅的id
	 */
	void deleteFarewell(String id);
	/**
	 * 查询告别厅id
	 * @param id 告别厅id
	 */
	Farewell getFarewellById(String id);
	/**
	 * 根据名字查找
	 * @param name
	 * @return
	 */
	Farewell getFarewellByName(String name);
	/**
	 * 查询所有告别厅
	 * @param paramMap
	 */
	List<Farewell> getFarewellList(Map<String,Object> paramMap);
}
