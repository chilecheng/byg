package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.TransportType;
/**
 * 运输类型
 * @author rgy
 */
public interface TransportTypeMapper {
	/**
	 * 添加运输类型
	 * @param transportType 运输类型
	 */
	void saveTransportType(TransportType transportType);
	/**
	 * 修改运输类型
	 * @param transportType 运输类型
	 */
	void updateTransportType(TransportType transportType);
	/**
	 * 删除运输类型
	 * @param id 运输类型的id
	 */
	void deleteTransportType(String id);
	/**
	 * 查询运输类型id
	 * @param id 运输类型的id
	 */
	TransportType getTransportTypeById(String id);
	/**
	 * 查询所有运输类型
	 * @param paramMap
	 */
	List<TransportType> getTransportTypeList(Map<String,Object> paramMap);
}
