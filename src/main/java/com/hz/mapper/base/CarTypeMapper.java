package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.CarType;

public interface CarTypeMapper {
	/**
	 * 添加车辆类型
	 * @param carType
	 */
	void saveCarType(CarType carType);
	/**
	 * 删除车辆类型
	 * @param id 
	 */
	void deleteCarType(String id);
	/**
	 * 更新车辆类型
	 * @param corpseUnit 
	 */
	void updateCarType(CarType carType);
	/**
	 * 获取车辆类型对象
	 * @param id
	 * @return
	 */
	CarType getCarTypeById(String id);
	/**
	 * 根据条件获取车辆类型List
	 * @return
	 */
	List<CarType> getCarTypeList(Map<String, Object>paramMap);
}