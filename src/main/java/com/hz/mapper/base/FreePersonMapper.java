package com.hz.mapper.base;

import com.hz.entity.base.FreePerson;
import java.util.List;
import java.util.Map;

public interface FreePersonMapper {
	/**
	 * 添加免费对象类别
	 * @param freePerson
	 */
	void saveFreePerson(FreePerson freePerson);
	/**
	 * 删除免费对象类别
	 * @param id 
	 */
	void deleteFreePerson(String id);
	/**
	 * 更新免费对象类别
	 * @param corpseUnit 
	 */
	void updateFreePerson(FreePerson freePerson);
	/**
	 * 获取免费对象类别对象
	 * @param id
	 * @return
	 */
	FreePerson getFreePersonById(String id);
	/**
	 * 根据条件获取免费对象类别List
	 * @return
	 */
	List<FreePerson> getFreePersonList(Map<String, Object> paramMap);
}