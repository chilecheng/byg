package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.FarewellType;
/**
 * 告别厅类型
 * @author rgy
 */
public interface FarewellTypeMapper {
	/**
	 * 添加告别厅类型
	 * @param farewellType 告别厅类型
	 */
	void saveFarewellType(FarewellType farewellType);
	/**
	 * 修改告别厅类型
	 * @param farewellType 告别厅类型
	 */
	void updateFarewellType(FarewellType farewellType);
	/**
	 * 删除告别厅类型
	 * @param id 告别厅类型的id
	 */
	void deleteFarewellType(String id);
	/**
	 * 查询告别厅类型id
	 * @param id 告别厅类型id
	 */
	FarewellType getFarewellTypeById(String id);
	/**
	 * 查询所有告别厅类型
	 * @param paramMap
	 */
	List<FarewellType> getFarewellTypeList(Map<String,Object> paramMap);
}
