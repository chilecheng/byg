package com.hz.mapper.base;

import com.hz.entity.base.FreezerType;

import java.util.List;
import java.util.Map;

public interface FreezerTypeMapper {
	/**
	 * 添加冷藏柜类型
	 * @param freezerType
	 */
	void saveFreezerType(FreezerType freezerType);
	/**
	 * 删除冷藏柜类型
	 * @param id 
	 */
	void deleteFreezerType(String id);
	/**
	 * 修改冷藏柜类型
	 * @param corpseUnit 
	 */
	void updateFreezerType(FreezerType freezerType);
	/**
	 * 获取冷藏柜类型对象
	 * @param id
	 * @return
	 */
	FreezerType getFreezerTypeById(String id);
	/**
	 * 根据条件获取冷藏柜类型List
	 * @return
	 */
	List<FreezerType> getFreezerTypeList(Map<String, Object> paramMap);
}