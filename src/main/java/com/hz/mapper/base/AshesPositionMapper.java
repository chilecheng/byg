package com.hz.mapper.base;

import java.util.List;
import com.hz.entity.base.AshesPosition;
/**
 * 骨灰盒位置
 * @author hw
 *
 */
public interface AshesPositionMapper {
	/**
	 * 添加骨灰盒数量
	 * @param ashesPosition
	 */
	void saveAshesPosition(AshesPosition ashesPosition);
	
	/**
	 * 更新骨灰盒数量
	 * @param ashesPostion
	 */
	void updateAshesPosition(AshesPosition ashesPostion);
	
	/**
	 * 获取骨灰盒位置对象
	 * @param id
	 * @return
	 */
	AshesPosition getAshesPositionById(String id);
	/**
	 * 根据条件获取骨灰盒位置List
	 * @return
	 */
	List<AshesPosition> getAshesPositionList();

}
