package com.hz.mapper.base;

import com.hz.entity.base.ItemType;
import java.util.List;
import java.util.Map;

public interface ItemTypeMapper {
	/**
	 * 添加收费项目类别
	 * @param itemType
	 */
	void saveItemType(ItemType itemType);
	/**
	 * 删除收费项目类别
	 * @param id 
	 */
	void deleteItemType(String id);
	/**
	 * 修改收费项目类别
	 * @param corpseUnit 
	 */
	void updateItemType(ItemType itemType);
	/**
	 * 获取收费项目类别对象
	 * @param id
	 * @return
	 */
	ItemType getItemTypeById(String id);
	/**
	 * 根据条件获取收费项目类别List
	 * @return
	 */
	List<ItemType> getItemTypeList(Map<String, Object> paramMap);
	/**
	 * 根据服务项目名字获取ID
	 * @param par
	 * @return
	 */
	String getIdByTypeName(String par);
	
	List<ItemType> getMoreItemTypeList(Map<String, Object> paramMap);
}