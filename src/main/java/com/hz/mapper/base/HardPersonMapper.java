package com.hz.mapper.base;

import com.hz.entity.base.HardPerson;
import java.util.List;
import java.util.Map;

public interface HardPersonMapper {
	/**
	 * 添加重点救助对象类别
	 * @param hardPerson
	 */
	void saveHardPerson(HardPerson hardPerson);
	/**
	 * 删除重点救助对象类别
	 * @param id 
	 */
	void deleteHardPerson(String id);
	/**
	 * 更新重点救助对象类别
	 * @param corpseUnit 
	 */
	void updateHardPerson(HardPerson hardPerson);
	/**
	 * 获取重点救助对象类别对象
	 * @param id
	 * @return
	 */
	HardPerson getHardPersonById(String id);
	/**
	 * 根据条件获取重点救助对象类别List
	 * @return
	 */
	List<HardPerson> getHardPersonList(Map<String, Object> paramMap);
}