package com.hz.mapper.base;

import com.hz.entity.base.FurnaceType;
import java.util.List;
import java.util.Map;

public interface FurnaceTypeMapper {
	/**
	 * 添加火化炉类型
	 * @param furnaceType
	 */
	void saveFurnaceType(FurnaceType furnaceType);
	/**
	 * 删除火化炉类型
	 * @param id 
	 */
	void deleteFurnaceType(String id);
	/**
	 * 修改火化炉类型
	 * @param corpseUnit 
	 */
	void updateFurnaceType(FurnaceType furnaceType);
	/**
	 * 获取火化炉类型对象
	 * @param id
	 * @return
	 */
	FurnaceType getFurnaceTypeById(String id);
	/**
	 * 根据条件获取火化炉类型List
	 * @return
	 */
	List<FurnaceType> getFurnaceTypeList(Map<String,Object> paramMap);
}