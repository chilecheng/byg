package com.hz.mapper.base;
import java.util.List;
import java.util.Map;
import com.hz.entity.base.CorpseAddress;
/**
 * 接尸地址
 * @author jgj
 * */
public interface CorpseAddressMapper {
	/**
	 * 添加接尸地址
	 * @param corpseAddress 
	 */
	void saveCorpseAddress(CorpseAddress corpseAddress);
	/**
	 * 删除接尸地址
	 * @param id 
	 */
	void deleteCorpseAddress(String id);
	/**
	 * 修改接尸地址
	 * @param corpseAddress 
	 */
	void updateCorpseAddress(CorpseAddress corpseAddress);
	/**
	 * 查询接尸地址id
	 * @param id
	 */
	CorpseAddress getCorpseAddressById(String id);
	/**
	 * 查询所有接尸地址
	 * @param paramMap
	 */
	List<CorpseAddress> getCorpseAddressList(Map<String, Object> paramMap);
	
}
