package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;

import com.hz.entity.base.Nation;
/**
 * 民族信息
 * @author rgy
 *
 */
public interface NationMapper {
	/**
	 * 添加民族信息
	 * @param Nation 民族信息
	 */
	void saveNation(Nation nation);
	/**
	 * 修改民族信息
	 * @param nation 民族信息
	 */
	void updateNation(Nation nation);
	/**
	 * 删除民族信息
	 * @param id 民族信息id
	 */
	void deleteNation(String id);
	/**
	 * 查询民族信息Id
	 * @param id 民族信息id
	 */
	Nation getNationById(String id);
	/**
	 * 查询所有的民族信息
	 * @param paramMap
	 */
	List<Nation> getNationList(Map<String, Object> paramMap);
	/**
	 * 通过  民族名  查询id
	 * @param dNation
	 * @return
	 */
	@Select(" select id from ba_nation where name like CONCAT('%',#{0},'%') ")
	String getNationId(String dNation);
	@Select("select name from ba_nation where id=#{0} ")
	String  getNationNameById(String dNationId);
	
}
