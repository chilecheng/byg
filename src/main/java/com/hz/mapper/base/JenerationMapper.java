package com.hz.mapper.base;

import com.hz.entity.base.Jeneration;
import java.util.List;
import java.util.Map;

public interface JenerationMapper {
	/**
	 * 添加编号管理
	 */
	void saveJeneration(Jeneration jeneration);
	/**
	 * 删除编号管理
	 */
	void deleteJeneration(String id);
	/**
	 * 修改编号管理
	 */
	void updateJeneration(Jeneration jeneration);
	/**
	 * 获取编号管理对象
	 */
	Jeneration getJenerationById(String id);
	/**
	 * 根据条件获取编号管理List
	 */
	List<Jeneration> getJenerationList(Map<String,Object> paramMap);
}