package com.hz.mapper.base;
import java.util.List;
import java.util.Map;
import com.hz.entity.base.BillUnit;
/**
 * 挂账单位接口
 * @author jgj
 * */
public interface BillUnitMapper {
	/**
	 * 添加挂账单位
	 * @param billUnit 
	 */
	void saveBillUnit(BillUnit billUnit);
	/**
	 * 删除挂账单位
	 * @param id 
	 */
	void deleteBillUnit(String id);
	/**
	 * 修改挂账单位
	 * @param billUnit 
	 */
	void updateBillUnit(BillUnit billUnit);
	/**
	 * 查询挂账单位id
	 * @param id
	 */
	BillUnit getBillUnitById(String id);
	/**
	 * 查询所有挂账单位
	 * @param paramMap
	 */
	List<BillUnit> getBillUnitList(Map<String,Object> paramMap);
	
}
