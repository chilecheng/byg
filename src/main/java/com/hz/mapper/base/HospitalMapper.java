package com.hz.mapper.base;

import java.util.List;
import java.util.Map;

import com.hz.entity.base.Hospital;

/**
 * 医院类
 * @author rgy
 */
public interface HospitalMapper {
	/**
	 * 添加医院
	 * @param hospital 医院类
	 */
	void saveHospital(Hospital hospital);
	/**
	 * 修改医院
	 * @param hospital 医院类
	 */
	void updateHospital(Hospital hospital);
	/**
	 * 删除医院
	 * @param id 医院的id
	 * @param String id
	 */
	void deleteHospital(String id);
	/**
	 * 查询医院id
	 * @param id 医院类id
	 * @param String id
	 */
	Hospital getHospitalById(String id);
	/**
	 * 查询医院地名
	 * @param paramMap
	 * @param map paramMap
	 */
	List<Hospital> getHospitalList(Map<String,Object> paramMap);
}
