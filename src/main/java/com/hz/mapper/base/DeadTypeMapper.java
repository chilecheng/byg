package com.hz.mapper.base;
import java.util.List;
import java.util.Map;

import com.hz.entity.base.DeadType;
/**
 * 死亡类型
 * @author rgy
 * */
public interface DeadTypeMapper {
	/**
	 * 添加死亡类型
	 * @param deadType 
	 */
	void saveDeadType(DeadType deadType);
	/**
	 * 删除死亡类型
	 * @param id 
	 */
	void deleteDeadType(String id);
	/**
	 * 修改死亡类型
	 * @param deadType 
	 */
	void updateDeadType(DeadType deadType);
	/**
	 * 查询死亡类型id
	 * @param id
	 */
	DeadType getDeadTypeById(String id);
	/**
	 * 查询所有死亡类型
	 * @param paramMap
	 */
	List<DeadType> getDeadTypeList(Map<String,Object> paramMap);
	
}
