package com.hz.mapper.base;

import java.util.List;
import java.util.Map;
import com.hz.entity.base.Register;
/**
 * 户籍类
 * @author rgy
 *
 */
public interface RegisterMapper {
	/**
	 * 添加户籍
	 * @param register 户籍类
	 */
	void saveRegister(Register register);
	/**
	 * 修改户籍
	 * @param register 户籍类
	 */
	void updateRegister(Register register);
	/**
	 * 删除户籍
	 * @param id 户籍类id
	 */
	void deleteRegister(String id);
	/**
	 * 查询户籍Id
	 * @param id 户籍类id
	 */
	Register getRegisterById(String id);
	/**
	 * 查询所有的户籍
	 * @param paramMap
	 */
	List<Register> getRegisterList(Map<String,Object> paramMap);
}
