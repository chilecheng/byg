package com.hz.mapper.base;
import java.util.List;
import java.util.Map;
import com.hz.entity.base.DeadReason;
/**
 * 死亡原因
 * @author rgy
 * */
public interface DeadReasonMapper {
	/**
	 * 添加死亡原因
	 * @param deadReason 
	 */
	void saveDeadReason(DeadReason deadReason);
	/**
	 * 删除死亡原因
	 * @param id 
	 */
	void deleteDeadReason(String id);
	/**
	 * 修改死亡原因
	 * @param deadreason 
	 */
	void updateDeadReason(DeadReason deadreason);
	/**
	 * 查询死亡原因id
	 * @param id
	 */
	DeadReason getDeadReasonById(String id);
	/**
	 * 查询所有死亡原因
	 * @param paramMap
	 */
	List<DeadReason> getDeadReasonList(Map<String,Object> paramMap);
	
}
