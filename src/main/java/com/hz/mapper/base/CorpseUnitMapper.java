package com.hz.mapper.base;
import java.util.List;
import java.util.Map;
import com.hz.entity.base.CorpseUnit;
/**
 * 接尸单位
 * @author rgy
 * */
public interface CorpseUnitMapper {
	/**
	 * 添加接尸单位
	 * @param corpseUnit 
	 */
	void saveCorpseUnit(CorpseUnit corpseUnit);
	/**
	 * 删除接尸单位
	 * @param id 
	 */
	void deleteCorpseUnit(String id);
	/**
	 * 修改接尸单位
	 * @param corpseUnit 
	 */
	void updateCorpseUnit(CorpseUnit corpseUnit);
	/**
	 * 查询接尸单位id
	 * @param id
	 */
	CorpseUnit getCorpseUnitById(String id);
	/**
	 * 查询所有接尸单位
	 * @param paramMap
	 */
	List<CorpseUnit> getCorpseUnitList(Map<String, Object> paramMap);
	
}
