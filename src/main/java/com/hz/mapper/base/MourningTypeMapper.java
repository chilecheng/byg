package com.hz.mapper.base;
import java.util.List;
import java.util.Map;
import com.hz.entity.base.MourningType;
/**
 * 灵堂类型
 * @author rgy
 * */
public interface MourningTypeMapper {
	/**
	 * 添加灵堂类型
	 * @param mourningType 
	 */
	void saveMourningType(MourningType mourningType);
	/**
	 * 删除灵堂类型
	 * @param id 
	 */
	void deleteMourningType(String id);
	/**
	 * 修改灵堂类型
	 * @param mourningType 
	 */
	void updateMourningType(MourningType mourningType);
	/**
	 * 查询灵堂类型id
	 * @param id
	 */
	MourningType getMourningTypeById(String id);
	/**
	 * 查询所有灵堂类型
	 * @param paramMap
	 */
	List<MourningType> getMourningTypeList(Map<String,Object> paramMap);
	
}
