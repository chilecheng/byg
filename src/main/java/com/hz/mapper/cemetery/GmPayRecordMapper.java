package com.hz.mapper.cemetery;

import com.hz.entity.cemetery.PayRecord;

import java.util.List;
import java.util.Map;

/**
 * 墓穴普通收费记录的增删改查
 *
 * @Classname GmPayRecordMapper
 * @Description TODO
 * @Date 2019/7/22 17:45
 * @Created by ZhixiangWang
 */
public interface GmPayRecordMapper {

    /**
     * 添加墓穴普通收费信息
     *
     * @param payRecord
     */
    void saveGmPayRecord(PayRecord payRecord);

    /**
     * 删除墓穴普通收费信息
     *
     * @param id
     */
    void deleteGmPayRecord(String id);

    /**
     * 更新墓穴普通收费信息
     *
     * @param payRecord
     */
    void updateGmPayRecord(PayRecord payRecord);

    /**
     * 获取墓穴普通收费信息
     *
     * @param id
     * @return
     */
    PayRecord getGmPayRecordById(String id);

    /**
     * 根据条件获取墓穴普通收费信息List
     *
     * @return
     */
    List<PayRecord> getGmPayRecordList(Map<String, Object> paramMap);
}
