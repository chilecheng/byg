package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.MaintenancePaymentedRecord;

/**
*Describtion:维护已缴费记录查询

*Creatime:2019年6月25日

*Author:wujiayang

*Comment:

**/
public interface MaintenancePaymentRecordMapper {
	/**
	 * @return 获取所有的 已支付墓穴维护费列表
	 */
	List<MaintenancePaymentedRecord> getListMaintenancePaymentRecord(Map<String, Object> param);
	/**根据条件
	 * @return 获取墓穴维护费记录
	 */
	List<MaintenancePaymentedRecord> getMaintenancePaymentCemeteryList(Map<String, Object> paramMap);
	/**根据条件
	 * @return 获取墓穴维护金额总计
	 */
	Integer getsumvalue(Map<String, Object> maps);
}
