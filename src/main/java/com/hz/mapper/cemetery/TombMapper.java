package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.Tomb;
/**
 * 陵园墓穴的增删改查
 * @author Administrator
 *
 */
public interface TombMapper {
	/**
	 * 添加陵园墓穴信息
	 * @param tomb
	 */
	void saveCemeteryTomb(Tomb tomb);
	/**
	 * 删除陵园墓穴信息
	 * @param id 
	 */
	void deleteCemeteryTomb(String id);
	/**
	 * 更新陵园墓穴信息
	 * @param tomb 
	 */
	void updateCemeteryTomb(Tomb tomb);
	/**
	 * 获取陵园墓穴信息对象
	 * @param id
	 * @return
	 */
	Tomb getCemeteryTombById(String id);
	/**
	 * 根据条件获取陵园墓穴信息List
	 * @return
	 */
	List<Tomb> getCemeteryTombList(Map<String,Object> paramMap);
	Integer getTomb(Map<String, Object> paramMap);
}
