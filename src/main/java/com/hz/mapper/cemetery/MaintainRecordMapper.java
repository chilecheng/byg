package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.MaintainRecord;
/**
 * 墓穴维护记录的增删改查
 * @author Administrator
 *
 */
public interface MaintainRecordMapper {
	/**
	 * 添加墓穴维护信息
	 * @param maintainRecord
	 */
	void saveMaintainRecord(MaintainRecord maintainRecord);
	/**
	 * 删除墓穴维护记录
	 * @param id 
	 */
	void deleteMaintainRecord(String id);
	/**
	 * 更新墓穴维护记录
	 * @param maintainRecord 
	 */
	void updateMaintainRecord(MaintainRecord maintainRecord);
	/**
	 * 获取陵园墓穴维护记录
	 * @param id
	 * @return
	 */
	MaintainRecord getMaintainRecordById(String id);
	/**
	 * 根据条件获取墓穴维护记录List
	 * @return
	 */
	List<MaintainRecord> getMaintainRecordList(Map<String,Object> paramMap);
}
