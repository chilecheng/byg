package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.AppointmentRecord;
/**
 * 墓穴预约记录的增删改查
 * @author Administrator
 *
 */
public interface AppointmentRecordMapper {
	/**
	 * 添加墓穴预约记录
	 * @param appointmentRecord
	 */
	void saveAppointmentRecord(AppointmentRecord appointmentRecord);
	/**
	 * 删除墓穴预约记录
	 * @param id 
	 */
	void deleteAppointmentRecord(String id);
	/**
	 * 更新墓穴预约记录
	 * @param appointmentRecord 
	 */
	void updateAppointmentRecord(AppointmentRecord appointmentRecord);
	
	/**
	 * 通过墓穴id 将预约状态 为预约成功1  修改为预约状态为 预约结束0
	 * @param param
	 */
	void updateAppointmentStatus(String tid);
	/**
	 * 获取墓穴预约记录对象
	 * @param id
	 * @return
	 */
	AppointmentRecord getAppointmentRecordById(String id);
	/**
	 * 根据条件获取墓穴预约记录List
	 * @return
	 */
	List<AppointmentRecord> getAppointmentRecordList(Map<String,Object> paramMap);
	/**
	 * 根据条件获取全部墓穴预约记录List
	 * @return
	 */
	List<AppointmentRecord> getAllAppointmentRecordList(Map<String,Object> paramMap);
}
