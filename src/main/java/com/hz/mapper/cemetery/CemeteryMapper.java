package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.Cemetery;
/**
 * 陵园信息的增删改查
 * @author Administrator
 *
 */
public interface CemeteryMapper {
	/**
	 * 添加陵园信息
	 * @param cemetery
	 */
	void saveCemetery(Cemetery cemetery);
	/**
	 * 删除陵园信息
	 * @param id 
	 */
	void deleteCemetery(String id);
	/**
	 * 更新陵园信息
	 * @param cemetery 
	 */
	void updateCemetery(Cemetery cemetery);
	/**
	 * 获取陵园信息对象
	 * @param id
	 * @return
	 */
	Cemetery getCemeteryById(String id);
	/**
	 * 根据条件获取陵园信息List
	 * @return
	 */
	List<Cemetery> getCemeteryList(Map<String,Object> paramMap);
	void getCemeteryrecord();
}
