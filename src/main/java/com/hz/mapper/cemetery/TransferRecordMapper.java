package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.TransferRecord;
/**
 * 墓穴迁出记录的增删改查
 * @author Administrator
 *
 */
public interface TransferRecordMapper {
	/**
	 * 添加墓穴迁出记录
	 * @param transferRecord
	 */
	void saveTransferRecord(TransferRecord transferRecord);
	/**
	 * 删除墓穴迁出记录
	 * @param id 
	 */
	void deleteTransferRecord(String id);
	/**
	 * 更新墓穴迁出记录
	 * @param transferRecord 
	 */
	void updateTransferRecord(TransferRecord transferRecord);
	/**
	 * 获取墓穴迁出记录对象
	 * @param id
	 * @return
	 */
	TransferRecord getTransferRecordById(String id);
	/**
	 * 根据条件获取墓穴迁出记录List
	 * @return
	 */
	List<TransferRecord> getTransferRecordList(Map<String,Object> paramMap);
}
