package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;
import com.hz.entity.cemetery.PaymentRecord;
/**
 * 墓穴支付记录的增删改查
 * @author Administrator
 *
 */
public interface PaymentRecordMapper {
	/**
	 * 添加墓穴支付记录
	 * @param paymentRecord
	 */
	void savePaymentRecord(PaymentRecord paymentRecord);
	/**
	 * 删除墓穴支付记录
	 * @param id 
	 */
	void deletePaymentRecord(String id);
	/**
	 * 更新墓穴支付记录
	 * @param paymentRecord 
	 */
	void updatePaymentRecord(PaymentRecord paymentRecord);
	/**
	 * 获取陵园墓穴支付记录
	 * @param id
	 * @return
	 */
	PaymentRecord getPaymentRecordById(String id);
	
	/**
	 * 通过墓穴id 获取到对应的 墓穴支付记录
	 * @param id
	 * @return
	 */
	PaymentRecord getPaymentRecordByTombId(String recordId);
	/**
	 * 根据条件获取墓穴支付记录List
	 * @return
	 */
	List<PaymentRecord> getPaymentRecordList(Map<String,Object> paramMap);
	/**
	 * 获取墓穴购买缴费记录
	 */
	void getCemeteryrecord();
	/**
	 * 获取墓穴维护缴费记录
	 */
	void getMaintenancePayment();
}
