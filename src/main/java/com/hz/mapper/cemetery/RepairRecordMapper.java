package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.RepairRecord;
/**
 * ĹѨά�޼�¼����ɾ�Ĳ�
 * @author Administrator
 *
 */
public interface RepairRecordMapper {
	/**
	 * ����ĹѨά�޼�¼
	 * @param repairRecord
	 */
	void saveRepairRecord(RepairRecord repairRecord);
	/**
	 * ɾ��ĹѨά�޼�¼
	 * @param id 
	 */
	void deleteRepairRecord(String id);
	/**
	 * ����ĹѨά�޼�¼
	 * @param repairRecord 
	 */
	void updateRepairRecord(RepairRecord repairRecord);
	/**
	 * ��ȡĹѨά�޼�¼����
	 * @param id
	 * @return
	 */
	RepairRecord getRepairRecordById(String id);
	/**
	 * ����ĹѨID��ά��״̬��δά�ޣ���ѯ��ά�޼�¼
	 * @param tombId
	 * @param repairType
	 * @return
	 */
	RepairRecord getRepairRecordByTombIdAndType(String tombId,byte repairType);
	/**
	 * ����������ȡĹѨά�޼�¼List
	 * @return
	 */
	List<RepairRecord> getRepairRecordList(Map<String,Object> paramMap);
}
