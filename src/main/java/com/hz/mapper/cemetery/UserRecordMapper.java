package com.hz.mapper.cemetery;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.hz.entity.cemetery.UserRecord;
/**
 * 墓穴使用者记录信息的增删改查
 * @author Administrator
 *
 */
public interface UserRecordMapper {
	/**
	 * 添加墓穴使用者记录信息
	 * @param userRecord
	 */
	void saveUserRecord(UserRecord userRecord);
	/**
	 * 删除墓穴使用者记录信息
	 * @param id 
	 */
	void deleteUserRecord(String id);
	/**
	 * 更新墓穴使用者记录信息
	 * @param userRecord 
	 */
	void updateUserRecord(UserRecord userRecord);
	/**
	 * 获取墓穴使用者记录信息
	 * @param id
	 * @return
	 */
	UserRecord getUserRecordById(String id);
	/**
	 * 根据条件获取墓穴使用者记录信息List
	 * @return
	 */
	List<UserRecord> getUserRecordList(Map<String,Object> paramMap);
	/**
	 * 根据条件获取安放量统计
	 * @return
	 */
	Integer getsumvalue(Map<String, Object> paramMap);
	/**
	 * 根据条件获取安放记录List
	 * @return
	 */
	List<UserRecord> getUserCemeteryRecordList(Map<String, Object> paramMap);
	/**
	 * 获取数据库所有安放量统计,不加时间条件
	 * @return
	 */
	Integer getsumvalues(Map<String, Object> paramMap);
}
