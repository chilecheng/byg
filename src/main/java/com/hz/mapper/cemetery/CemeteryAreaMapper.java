package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.CemeteryArea;
/**
 * 陵园区域的增删改查
 * @author Administrator
 *
 */
public interface CemeteryAreaMapper {
	/**
	 * 添加陵园区域信息
	 * @param cemeteryArea
	 */
	void saveCemeteryArea(CemeteryArea cemeteryArea);
	/**
	 * 删除陵园区域信息
	 * @param id 
	 */
	void deleteCemeteryArea(String id);
	/**
	 * 更新陵园区域信息
	 * @param cemeteryArea 
	 */
	void updateCemeteryArea(CemeteryArea cemeteryArea);
	/**
	 * 获取陵园区域信息对象
	 * @param id
	 * @return
	 */
	CemeteryArea getCemeteryAreaById(String id);
	/**
	 * 根据条件获取陵园区域信息List
	 * @return
	 */
	List<CemeteryArea> getCemeteryAreaList(Map<String,Object> paramMap);
}
