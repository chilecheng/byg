package com.hz.mapper.cemetery;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.hz.entity.cemetery.BuyRecord;

/**
 * 墓穴购买信息的增删改查
 *
 * @author Administrator
 */
public interface BuyRecordMapper {
    /**
     * 添加墓穴购买记录信息
     *
     * @param buyRecord
     */
    void saveBuyRecord(BuyRecord buyRecord);

    /**
     * 删除墓穴购买记录
     *
     * @param id
     */
    void deleteBuyRecord(String id);

    /**
     * 更新墓穴购买记录
     *
     * @param buyRecord
     */
    void updateBuyRecord(BuyRecord buyRecord);

    /**
     * 获取墓穴购买记录
     *
     * @param id
     * @return
     */
    BuyRecord getBuyRecordById(String id);

    /**
     * 根据墓穴ID获取购买记录
     *
     * @param tombId
     * @return
     */
    BuyRecord getBuyRecordByTombId(String tombId);

    /**
     * 根据条件获取墓穴购买记录List
     *
     * @return
     */
    List<BuyRecord> getBuyRecordList(Map<String, Object> paramMap);

    /**
     * 根据条件获取墓穴销售数量
     *
     * @return
     */
    Integer getReport(@Param("startTime") Date startTime, @Param("endTime") Date endTime);

    /**
     * 根据条件获取墓穴销售情况
     *
     * @return
     */
    List<BuyRecord> getSaleReport(Map<String, Object> paramMap);

    /**
     * 根据条件获取当前墓穴销售量，销售额，维护费
     *
     * @return
     */
    List<BuyRecord> getsalemoney(Map<String, Object> paramMap);

    /**
     * 根据条件获取 墓穴持证人及使用者信息List
     *
     * @param maps
     * @return java.util.List<com.hz.entity.cemetery.BuyRecord>
     * @Auther: ZhixiangWang on 2019/7/26 13:48
     * @Description: TODO
     */
    List<BuyRecord> getBuyAndUserRecordList(Map<String, Object> maps);
}