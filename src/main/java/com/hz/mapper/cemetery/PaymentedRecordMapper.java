package com.hz.mapper.cemetery;
/**
*Describtion:已支付墓穴 记录查询

*Creatime:2019年6月25日

*Author:wujiayang

*Comment:数据来源 gm_payment_record表

**/

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.PaymentedRecord;

public interface PaymentedRecordMapper {

	/**
	 * @return 获取所有的 已支付墓穴列表
	 */
	List<PaymentedRecord> getListPaymentedRecord(Map<String, Object> param);
	/**
	 * @return 获取所有的 已支付墓穴记录
	 */
	List<PaymentedRecord> getPaymentCemeteryList(Map<String, Object> paramMap);
	/**
	 * @return 获取所有的 已支付墓穴费的总金额
	 */
	Integer getsumvalue(Map<String, Object> maps);
	/**
	 * @return 获取销售墓穴数量,加时间条件
	 */
	Integer getPaymentedTime(Map<String, Object> maps);
	/**
	 * @return 获取销售墓穴数量,不加时间条件
	 */
	Integer PaymentedNumber(Map<String, Object> paramMap);
	/**
	 * @return 获取销售墓穴总金额,不加时间条件
	 */
	Integer PaymentedMoney(Map<String, Object> paramMap);
	/**
	 * @return 获取销售墓穴总金额最小金额
	 */
	Integer PaymentedMinMoney(Map<String, Object> paramMap);
	/**
	 * @return 获取销售墓穴总金额最大金额
	 */
	Integer PaymentedMaxMoney(Map<String, Object> paramMap);
	
}
