package com.hz.mapper.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.entity.cemetery.CemeteryRow;
import com.hz.entity.cemetery.CemeteryRowAndTomb;
/**
 * 陵园区域排的增删改查
 * @author Administrator
 *
 */
public interface CemeteryRowMapper {
	/**
	 * 添加陵园区域排信息
	 * @param cemeteryRow
	 */
	void saveCemeteryRow(CemeteryRow cemeteryRow);
	/**
	 * 删除陵园区域排信息
	 * @param id 
	 */
	void deleteCemeteryRow(String id);
	/**
	 * 更新陵园区域排信息
	 * @param cemeteryRow 
	 */
	void updateCemeteryRow(CemeteryRow cemeteryRow);
	/**
	 * 获取陵园区域排信息对象
	 * @param id
	 * @return
	 */
	CemeteryRow getCemeteryRowById(String id);
	/**
	 * 根据条件获取陵园区域排信息List
	 * @return
	 */
	List<CemeteryRow> getCemeteryRowList(Map<String,Object> paramMap);
	/**
	 * 根据条件获取 排及墓穴信息List
	 * @param maps
	 * @return
	 */
	List<CemeteryRowAndTomb> getRowAndTombList(Map<String,Object> maps);
}
