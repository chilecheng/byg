package com.hz.mapper.cemetery;

import com.hz.entity.cemetery.CemeteryBack;

import java.util.List;
import java.util.Map;

/**
 * 已退墓穴记录的增删改查
 *
 * @Classname CemeteryBackMapper
 * @Description TODO
 * @Date 2019/6/27 17:29
 * @Created by ZhixiangWang
 */
public interface CemeteryBackMapper {

    /**
     * 添加已退墓穴记录
     *
     * @param cemeteryBack
     */
    void saveCemeteryBack(CemeteryBack cemeteryBack);

    /**
     * 删除已退墓穴记录
     *
     * @param id
     */
    void deleteCemeteryBack(String id);

    /**
     * 更新已退墓穴记录
     *
     * @param cemeteryBack
     */
    void updateCemeteryBack(CemeteryBack cemeteryBack);

    /**
     * 获取已退墓穴记录
     *
     * @param id
     * @return
     */
    CemeteryBack getCemeteryBackById(String id);

    /**
     * 根据条件获取已退墓穴记录List
     *
     * @return
     */
    List<CemeteryBack> getCemeteryBackList(Map<String, Object> paramMap);
}
