package com.hz.mapper.cemetery;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.hz.entity.cemetery.PayRecord;
import com.hz.entity.cemetery.PaymentedRecord;

/**
*Describtion:

*Creatime:2019年6月28日

*Author:wujiayang

*Comment:获取普通收费记录表

**/
public interface PayRecordMapper {
	/**
	 * 添加墓穴普通收费信息
	 *
	 * @param payRecord
	/**根据条件
	 * @return 获取所有的收费记录列表
	 */
	void savePayRecord(PayRecord payRecord);

	/**
	 * 删除墓穴普通收费信息
	 *
	 * @param id
	 */
	void deletePayRecord(String id);

	/**
	 * 更新墓穴普通收费信息
	 *
	 * @param payRecord
	 */
	void updatePayRecord(PayRecord payRecord);

	/**
	 * 获取墓穴使用信息
	 *
	 * @param id
	 * @return
	 */
	PayRecord getPayRecordById(String id);

	/**
	 * 根据条件获取墓穴普通收费信息List
	 *
	 * @return
	 */
	List<PayRecord> getPayRecordList(Map<String, Object> paramMap);
	/**
	 * @return 获取雕刻安放费记录
	 */
	List<PayRecord> getpayRecordCemeteryList(Map<String, Object> paramMap);
	/**
	 * @return 获取雕刻安放龙门费记录
	 */
	List<PayRecord> getpayRecordList(Map<String, Object> paramMap);
	/**
	 * @return 获取迁移费记录
	 */
	List<PayRecord> getMovepayRecordList(Map<String, Object> paramMap);
	/**
	 * @return 获取雕刻安放费的总金额
	 */
	Integer getsumvalue(Map<String, Object> maps);
	/**
	 * @return 获取封龙门次数
	 */
	Integer getCloseTimes(Map<String, Object> maps);
	/**
	 * @return 获取迁移费总计
	 */
	Integer getMovesumvalue(Map<String, Object> maps);
}
