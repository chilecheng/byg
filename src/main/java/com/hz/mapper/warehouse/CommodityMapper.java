package com.hz.mapper.warehouse;

import java.util.List;
import java.util.Map;

import com.hz.entity.warehouse.Commodity;

/**
 * 商品信息接口
 * @author 金国军
 *
 */
public interface CommodityMapper {
	/**
	 * 更新商品信息
	 * @param commodity
	 */
	void updateCommodity(Commodity commodity);
	/**
	 * 根据条件获取商品信息
	 * @param map
	 * @return
	 */
	List<Commodity> getCommodityList(Map<String,Object> map);
	/**
	 * 根据ID获取对应的商品信息
	 * @param id
	 * @return
	 */
	Commodity getCommodityById(String id);
	/**
	 * 添加商品信息
	 * @param commodity
	 */
	void saveCommodity(Commodity commodity);
	/**
	 * 删除商品信息
	 * @param id
	 */
	void delCommodity(String id);
}