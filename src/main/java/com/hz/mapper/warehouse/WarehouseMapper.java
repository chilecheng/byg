package com.hz.mapper.warehouse;

import java.util.List;
import java.util.Map;

import com.hz.entity.warehouse.Warehouse;


/**
 * 仓库信息接口
 * @author 金国军
 *
 */
public interface WarehouseMapper {
	/**
	 * 更新仓库信息
	 * @param commodity
	 */
	void updateWarehouse(Warehouse warehouse);
	/**
	 * 根据条件获取仓库信息
	 * @param map
	 * @return
	 */
	List<Warehouse> getWarehouseList(Map<String,Object> map);
	/**
	 * 根据ID获取对应的仓库信息
	 * @param id
	 * @return
	 */
	Warehouse getWarehouseById(String id);
	/**
	 * 添加仓库信息
	 * @param commodity
	 */
	void saveWarehouse(Warehouse warehouse);
	/**
	 * 删除仓库信息
	 * @param id
	 */
	void delWarehouse(String id);
}