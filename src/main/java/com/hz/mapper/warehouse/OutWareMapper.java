package com.hz.mapper.warehouse;

import java.util.List;
import java.util.Map;

import com.hz.entity.warehouse.OutWare;


/**
 * 出库单信息接口
 * @author 金国军
 *
 */
public interface OutWareMapper {
	/**
	 * 根据条件获取出库单信息
	 * @param map
	 * @return
	 */
	List<OutWare> getOutWareList(Map<String,Object> map);
	/**
	 * 根据ID获取对应的出库单信息
	 * @param id
	 * @return
	 */
	OutWare getOutWareById(String id);
	/**
	 * 添加出库单
	 * @param commodity
	 */
	void saveOutWare(OutWare outWare);
}