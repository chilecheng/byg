package com.hz.mapper.warehouse;

import java.util.List;
import java.util.Map;

import com.hz.entity.warehouse.Stock;


/**
 * 库存信息接口
 * @author 金国军
 *
 */
public interface StockMapper {
	/**
	 * 更新库存信息
	 * @param commodity
	 */
	void updateStock(Stock stock);
	/**
	 * 根据条件获取库存信息
	 * @param map
	 * @return
	 */
	List<Stock> getStockList(Map<String,Object> map);
	/**
	 * 根据ID获取对应的库存信息
	 * @param id
	 * @return
	 */
	Stock getStockById(String id);
	/**
	 * 添加库存信息
	 * @param commodity
	 */
	void saveStock(Stock stock);
	/**
	 * 删除库存信息
	 * @param id
	 */
	void delStock(String id);
}