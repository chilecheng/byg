package com.hz.mapper.warehouse;

import java.util.List;
import java.util.Map;

import com.hz.entity.warehouse.Supplier;


/**
 * 供应商信息接口
 * @author 金国军
 *
 */
public interface SupplierMapper {
	/**
	 * 更新供应商信息
	 * @param commodity
	 */
	void updateSupplier(Supplier supplier);
	/**
	 * 根据条件获取供应商信息
	 * @param map
	 * @return
	 */
	List<Supplier> getSupplierList(Map<String,Object> map);
	/**
	 * 根据ID获取对应的供应商信息
	 * @param id
	 * @return
	 */
	Supplier getSupplierById(String id);
	/**
	 * 添加供应商信息
	 * @param commodity
	 */
	void saveSupplier(Supplier supplier);
	/**
	 * 删除供应商信息
	 * @param id
	 */
	void delSupplier(String id);
}