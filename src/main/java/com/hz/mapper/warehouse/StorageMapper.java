package com.hz.mapper.warehouse;

import java.util.List;
import java.util.Map;

import com.hz.entity.warehouse.Storage;


/**
 * 入库信息接口
 * @author 金国军
 *
 */
public interface StorageMapper {
	/**
	 * 根据条件获取入库信息
	 * @param map
	 * @return
	 */
	List<Storage> getStorageList(Map<String,Object> map);
	/**
	 * 根据ID获取对应的入库信息
	 * @param id
	 * @return
	 */
	Storage getStorageById(String id);
	/**
	 * 添加入库信息
	 * @param commodity
	 */
	void saveStorage(Storage storage);
}