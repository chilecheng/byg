package com.hz.mapper.warehouse;

import java.util.List;
import java.util.Map;

import com.hz.entity.warehouse.Bill;

/**
 * 与供应商结算 帐单接口
 * @author 金国军
 *
 */
public interface BillMapper {
	/**
	 * 更新帐单信息
	 * @param commodity
	 */
	void updateBill(Bill bill);
	/**
	 * 根据条件获取帐单信息
	 * @param map
	 * @return
	 */
	List<Bill> getBillList(Map<String,Object> map);
	/**
	 * 根据ID获取对应的帐单信息
	 * @param id
	 * @return
	 */
	Bill getBillById(String id);
	/**
	 * 添加帐单
	 * @param commodity
	 */
	void saveBill(Bill bill);
	/**
	 * 删除帐单
	 * @param id
	 */
	void delBill(String id);
}