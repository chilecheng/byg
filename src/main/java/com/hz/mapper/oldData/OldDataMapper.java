package com.hz.mapper.oldData;

import java.util.List;
import java.util.Map;

import com.hz.entity.oldData.HisData;
import com.hz.entity.oldData.OldData;
import com.hz.entity.oldData.SingleData;
/**
 * 老系统及单机系统数据 查询方法
 * @author jgj
 *
 */
public interface OldDataMapper {
	//按条件查询
	List<OldData> getOldDataList(Map<String,Object> map);
	//按deadID查询
	OldData getOldDataByID(String id);
	
	//按条件查询
	List<SingleData> getSingleDataList(Map<String,Object> map);
	//按corpse_id查询
	SingleData getSingleDataByID(String id);
	
	//按条件查询 6.5系统历史数据（已迁移的数据）
	List<HisData> getHisDataList(Map<String,Object> map);
	//按id查询
	HisData getHisDataByID(String id);
}