package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.BaseReductionDLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;

/**
 * 基本减免申请项目
 * @author rgy
 *
 */
public interface BaseReductionDMapperLog {
	/**
	 * 添加 基本减免申请项目
	 * @param BaseReductionDLog
	 */
	void saveBaseReductionD(BaseReductionDLog baseReductionD);
	/**
	 * 删除 基本减免申请项目
	 * @param id
	 */
	void deleteBaseReductionD(String id);
	/**
	 * 更改 基本减免申请项目
	 * @param BaseReductionDLog
	 */
	void updateBaseReductionD(BaseReductionDLog baseReductionD);
	/**
	 * 获取 基本减免申请项目
	 * @param id
	 * @return
	 */
	BaseReductionDLog getBaseReductionDById(String id);
	/**
	 * 根据条件获取 基本减免申请项目list
	 * @param paramMap
	 * @return
	 */
	List getBaseReductionDList(Map paramMap);
}
