package com.hz.mapper.log;

import java.util.List;
import java.util.Map;
import com.hz.entity.log.FarewellRecordLog;
import com.hz.entity.ye.FarewellRecord;

/**
 * 告别厅记录
 * @author rgy
 *
 */
public interface FarewellRecordMapperLog {
	/**
	 * 添加告别厅记录
	 * @param farewellRecord
	 */
	void saveFarewellRecord(FarewellRecordLog farewellRecord);
	/**
	 * 删除告别厅记录
	 * @param id
	 */
	void deleteFarewellRecord(String id);
	/**
	 * 更改告别厅记录
	 * @param farewellRecord
	 */
	void updateFarewellRecord(FarewellRecordLog farewellRecord);
	/**
	 * 获取告别厅记录对象
	 * @param id
	 * @return
	 */
	FarewellRecordLog getFarewellRecordById(String id);
	/**
	 * 根据条件获取告别厅记录list
	 * @param paramMap
	 * @return
	 */
	List<FarewellRecord> getFarewellRecordList(Map<String, Object> paramMap);
}
