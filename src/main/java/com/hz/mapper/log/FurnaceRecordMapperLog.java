package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.FurnaceRecordLog;


/**
 * 火化炉信息
 * @author tsp
 *
 */
public interface FurnaceRecordMapperLog {
	/**
	 * 添加火化炉信息
	 * @param furnaceRecord
	 */
	void saveFurnaceRecord(FurnaceRecordLog furnaceRecord);
	/**
	 * 删除火化炉信息
	 * @param id 
	 */
	void deleteFurnaceRecord(String id);
	/**
	 * 修改火化炉信息
	 * @param furnaceRecord 
	 */
	void updateFurnaceRecord(FurnaceRecordLog furnaceRecord);

	/**
	 * 获取火化炉信息对象
	 * @param id
	 * @return
	 */
	FurnaceRecordLog getFurnaceRecordById(String id);
	/**
	 * 根据条件获取火化炉信息List
	 * @return
	 */
	List<FurnaceRecordLog> getFurnaceRecordList(Map<String, String> paramMap);
	
}
