package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.HardReductionLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.MourningRecord;

/**
 * ���Ѽ�������
 * @author rgy
 *
 */
public interface HardReductionMapperLog {
	/**
	 * �������Ѽ�������
	 * @param hardReduction
	 */
	void saveHardReduction(HardReductionLog hardReduction);
	/**
	 * ɾ�� ���Ѽ�������
	 * @param id
	 */
	void deleteHardReduction(String id);
	/**
	 * ���� ���Ѽ�������
	 * @param hardReduction
	 */
	void updateHardReduction(HardReductionLog hardReduction);
	/**
	 * ��ȡ���Ѽ�������
	 * @param id
	 * @return
	 */
	HardReductionLog getHardReductionById(String id);
	/**
	 * ����������ȡ ���Ѽ�������list
	 * @param paramMap
	 * @return
	 */
	List getHardReductionList(Map paramMap);
}
