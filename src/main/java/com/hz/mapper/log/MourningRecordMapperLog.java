package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.MourningRecordLog;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;

/**
 * ���ü�¼
 * @author rgy
 *
 */
public interface MourningRecordMapperLog {
	/**
	 * �������ü�¼
	 * @param mourningRecord
	 */
	void saveMourningRecord(MourningRecordLog mourningRecord);
	/**
	 * ɾ�����ü�¼
	 * @param id
	 */
	void deleteMourningRecord(String id);
	/**
	 * �������ü�¼
	 * @param mourningRecord
	 */
	void updateMourningRecord(MourningRecordLog mourningRecord);
	/**
	 * ��ȡ���ü�¼
	 * @param id
	 * @return
	 */
	MourningRecordLog getMourningRecordById(String id);
	/**
	 * ����������ȡ���ü�¼list
	 * @param paramMap
	 * @return
	 */
	List getMourningRecordList(Map paramMap);
}
