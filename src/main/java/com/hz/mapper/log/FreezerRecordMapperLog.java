package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.FreezerRecordLog;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;

/**
 * ��ع��¼
 * @author rgy
 *
 */
public interface FreezerRecordMapperLog {
	/**
	 * ������ع��¼
	 * @param FreezerRecordLog
	 */
	void saveFreezerRecord(FreezerRecordLog freezerRecord);
	/**
	 * ɾ����ع��¼
	 * @param id
	 */
	void deleteFreezerRecord(String id);
	/**
	 * ������ع��¼
	 * @param FreezerRecordLog
	 */
	void updateFreezerRecord(FreezerRecordLog freezerRecord);
	/**
	 * ��ȡ��ع��¼
	 * @param id
	 * @return
	 */
	FreezerRecordLog getFreezerRecordById(String id);
	/**
	 * ����������ȡ��ع��¼list
	 * @param paramMap
	 * @return
	 */
	List getFreezerRecordList(Map paramMap);
}
