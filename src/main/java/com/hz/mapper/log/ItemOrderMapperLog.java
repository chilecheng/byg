package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.ItemOrderLog;
import com.hz.entity.ye.ItemOrder;

public interface ItemOrderMapperLog {
	/**
	 * 添加火化委托单收费项目
	 * @param itemOrder
	 */
	void saveItemOrder(ItemOrderLog itemOrder);
	/**
	 * 删除火化委托单收费项目
	 * @param id 
	 */
	void deleteItemOrder(String id);
	/**
	 * 更新火化委托单收费项目
	 * @param corpseUnit 
	 */
	void updateItemOrder(ItemOrderLog itemOrder);
	/**
	 * 获取火化委托单收费项目对象
	 * @param id
	 * @return
	 */
	ItemOrderLog getItemOrderById(String id);
	/**
	 * 根据条件获取火化委托单收费项目List
	 * @return
	 */
	List getItemOrderList(Map paramMap);
}