package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.BaseReductionLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;

/**
 * 基本减免申请
 * @author rgy
 *
 */
public interface BaseReductionMapperLog {
	/**
	 * 添加 基本减免申请
	 * @param OldData
	 */
	void saveBaseReduction(BaseReductionLog baseReduction);
	/**
	 * 删除 基本减免申请
	 * @param id
	 */
	void deleteBaseReduction(String id);
	/**
	 * 更改 基本减免申请
	 * @param OldData
	 */
	void updateBaseReduction(BaseReductionLog baseReduction);
	/**
	 * 获取 基本减免申请
	 * @param id
	 * @return
	 */
	BaseReductionLog getBaseReductionById(String id);
	/**
	 * 根据条件获取 基本减免申请list
	 * @param paramMap
	 * @return
	 */
	List getBaseReductionList(Map paramMap);
}
