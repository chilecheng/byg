package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.OperationLog;

/**
 * 用户日志
 * @author cjb
 *
 */
public interface LogMapper {
	
	void add(OperationLog log);
	/**
	 * 根据条件查询操作日志
	 * @param map
	 */
	List<OperationLog> getOperationLogList(Map<String,Object> map);
	
}
