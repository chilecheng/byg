package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.CarSchedulRecordLog;
import com.hz.entity.ye.CarSchedulRecord;

public interface CarSchedulRecordMapperLog {
	/**
	 * 添加车辆调度记录
	 * @param carSchedulRecord
	 */
	void saveCarSchedulRecord(CarSchedulRecordLog carSchedulRecord);
	/**
	 * 删除车辆调度记录
	 * @param id 
	 */
	void deleteCarSchedulRecord(String id);
	/**
	 * 更新车辆调度记录
	 * @param corpseUnit 
	 */
	void updateCarSchedulRecord(CarSchedulRecordLog carSchedulRecord);
	/**
	 * 获取车辆调度记录对象
	 * @param id
	 * @return
	 */
	CarSchedulRecordLog getCarSchedulRecordById(String id);
	/**
	 * 根据条件获取车辆调度记录List
	 * @return
	 */
	List getCarSchedulRecordList(Map paramMap);
}