package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.CommissionOrderLog;
import com.hz.entity.ye.CommissionOrder;

public interface CommissionOrderMapperLog {
	/**
	 * 添加火化委托单
	 * @param commissionOrder
	 */
	void saveCommissionOrder(CommissionOrderLog commissionOrder);
	/**
	 * 删除火化委托单
	 * @param id 
	 */
	void deleteCommissionOrder(String id);
	/**
	 * 更新火化委托单
	 * @param corpseUnit 
	 */
	void updateCommissionOrder(CommissionOrderLog commissionOrder);
	/**
	 * 获取火化委托单对象
	 * @param id
	 * @return
	 */
	CommissionOrderLog getCommissionOrderById(String id);
	/**
	 * 根据条件获取火化委托单List
	 * @return
	 */
	List getCommissionOrderList(Map paramMap);
}