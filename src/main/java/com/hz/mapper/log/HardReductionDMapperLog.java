package com.hz.mapper.log;

import java.util.List;
import java.util.Map;

import com.hz.entity.log.HardReductionDLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.MourningRecord;

/**
 * 困难减免申请项目
 * @author rgy
 *
 */
public interface HardReductionDMapperLog {
	/**
	 * 添加困难减免申请项目
	 * @param hardReductionD
	 */
	void saveHardReductionD(HardReductionDLog hardReductionD);
	/**
	 * 删除 困难减免申请项目
	 * @param id
	 */
	void deleteHardReductionD(String id);
	/**
	 * 更改 困难减免申请项目
	 * @param hardReductionD
	 */
	void updateHardReductionD(HardReductionDLog hardReductionD);
	/**
	 * 获取困难减免申请项目
	 * @param id
	 * @return
	 */
	HardReductionDLog getHardReductionDById(String id);
	/**
	 * 根据条件获取 困难减免申请项目list
	 * @param paramMap
	 * @return
	 */
	List getHardReductionDList(Map paramMap);
}
