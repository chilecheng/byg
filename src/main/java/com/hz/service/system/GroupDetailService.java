package com.hz.service.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.Dept;
import com.hz.entity.system.GroupType;
import com.hz.entity.system.User;
import com.hz.mapper.system.GroupUserMapper;
import com.hz.mapper.system.UserMapper;
import com.hz.util.Const;

/**
 * 用户服务
 * @author ljx
 */
@Service
@Transactional
public class GroupDetailService {
	
	@Autowired
	private GroupUserMapper groupUserMapper;
	
	/**
	 * 获取改组用户分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<User> getGroupDetailPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		//设置页码和每页数
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<User> list = groupUserMapper.getGroupDetailList(paramMap);
		PageInfo<User> page=new PageInfo<User>(list);
		return page;
	}
	
	/**
	 * 从分组中删除
	 * @param id
	 */
	public void deleteUserFromGroup(String id){
		String[] ids = id.split(",");
		for(int i =0;i<ids.length;i++){
			groupUserMapper.deleteUserFromGroup(ids[i]);
		}
	}
	
	public List<GroupType> getGroupTypeList() {
		return groupUserMapper.getGroupTypeList();
	}

	public void updateUserType(String userIds, String type) {
		String[] arr = userIds.split(",");
		for (String userId : arr) {
			groupUserMapper.updateUserType(userId,type);
		}
	}

	/**
	 * 查询无分组用户
	 * @return
	 */
	public List<User> getUserNoGroup() {
		List<User> list = groupUserMapper.getUserNoGroup();
		return list;
	}

	public void updateUT(byte type, String string) {
		groupUserMapper.updateUT(type,string);
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	/*public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			User user=getTypeById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		user.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				user.setIsdel(Const.Isdel_Yes);
			}
	    	updateUser(user);
	    }
	}*/
	
	/**
	 * 添加用户
	 * @param user
	 */
/*	public void addType(GroupType type){
		groupUserMapper.saveType(type);
	}*/
	
	/**
	 * 修改用户
	 * @param user
	 */
	/*public void updateType(GroupType type){
		groupUserMapper.updateType(type);;
	}*/
	
	
	/**
	 * 获取用户list
	 * @return
	 */
	/*public List getUserList(Map paramMap){
		return groupUserMapper.getGroupUserList(paramMap);
	}*/
	
	
	/**
	 * 根据登陆账号获取用户
	 * @param userName
	 * @return
	 */
	/*public User getUserByUserName(String userName){
		User user = groupUserMapper.getUserByUserName(userName);
		return user;
	}*/

	/**根据type得到用户组信息
	 * @param type
	 * @return
	 */
	/*public GroupType getGroupByType(String type) {
		GroupType group = groupUserMapper.getGroupByType(type);
		return group;
	}*/

	/**查询所有用户分组
	 * @return
	 */
}
