package com.hz.service.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.Dept;
import com.hz.entity.system.GroupType;
import com.hz.entity.system.User;
import com.hz.mapper.system.GroupUserMapper;
import com.hz.mapper.system.UserMapper;
import com.hz.util.Const;

/**
 * 用户服务
 * @author cjb
 */
@Service
@Transactional
public class GroupUserService {

	@Autowired
	private GroupUserMapper groupUserMapper;

	/**
	 * 根据id获取用户
	 * 
	 * @param id
	 * @return
	 */
	public GroupType getTypeById(String id) {
		return groupUserMapper.getTypeById(id);
	}

	/**
	 * 添加用户
	 * 
	 * @param user
	 */
	public void addType(GroupType type) {
		groupUserMapper.saveType(type);
	}

	/**
	 * 修改用户
	 * 
	 * @param user
	 */
	public void updateType(GroupType type) {
		groupUserMapper.updateType(type);
		;
	}

	/**
	 * 删除用户
	 * 
	 * @param id
	 */
	public void deleteUser(String id) {
		String[] ids = id.split(",");
		for (String typeId : ids) {
			GroupType type = groupUserMapper.getTypeById(typeId);
			groupUserMapper.deleteUserByGroup(type.getType());
			groupUserMapper.deleteType(typeId);
		}
	}

	/**
	 * 获取用户分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<GroupType> getGroupUserPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize,
			String order) {
		// 设置页码和每页数
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<GroupType> list = groupUserMapper.getGroupUserList(paramMap);
		PageInfo<GroupType> page = new PageInfo<GroupType>(list);
		return page;
	}

	public int getTypeCount(Byte bt) {
		return groupUserMapper.getTypeCount(bt.toString());
	}

	public int getNameCount(String name) {
		return groupUserMapper.getNameCount(name);
	}

	public int getTypeByTypeName(String name) {
		return groupUserMapper.getTypeByTypeName(name);
	}
	public GroupType getGroupByType(byte type) {
		return groupUserMapper.getGroupByType(type);
	}
}
