package com.hz.service.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.Notification;
import com.hz.mapper.system.NotificationMapper;
import com.hz.util.UuidUtil;

/**
 * 公开公告管理
 * @author rgy
 */
@Service
@Transactional
public class NotificationService {
	
	@Autowired
	private NotificationMapper notificationMapper;
	
	/**
	 * 根据id获取公开公告
	 * @param id
	 * @return
	 */
	public Notification getNotificationById(String id){
		return notificationMapper.getNotifictionById(id);
		
	}
	
	/**
	 * 获取公开公告浏览数量
	 * @param id
	 * @return
	 */
	public int getNotificationNumber(String id){
		return notificationMapper.getNotifictionByNumber(id);
		
	}
	
	/**
	 * 添加公开公告
	 * @param user
	 */
	public void addNotification(Notification notification){
		notificationMapper.saveNotification(notification);
	}
	
	/**
	 * 修改公开公告
	 * @param user
	 */
	public void updateNotification(Notification notification){
		notificationMapper.updateNotification(notification);
	}
	
	/**
	 * 删除公开公告
	 * @param id
	 */
	public void deleteNotification(String id){
		String[] ids = id.split(",");
		for(int i =0;i<ids.length;i++){
			notificationMapper.deleteNotification(ids[i]);
		}
	}
	
	/**
	 * 获取公开公告list
	 * @return
	 */
	public List<Notification> getNotificationList(Map<String,Object> paramMap){
		List<Notification> list=notificationMapper.getNotificationList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取公开公告的pageinfo
	 * @param paramMap
	 * @param pageSize
	 * @param pageNum
	 * @param order
	 * @return
	 */
	public PageInfo<Notification> getNotificationPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Notification> list=getNotificationList(paramMap);
		PageInfo<Notification> page=new PageInfo<Notification>(list);
		return page;
	}
	/**
	 * 更新公开公告保存
	 * @param id
	 * @param notification
	 */
	public void addUpdate(String id,Notification notification) {
		if(id!=null&&!id.equals("")){
			updateNotification(notification);
		}else{
			notification.setId(UuidUtil.get32UUID());
			addNotification(notification);
		}
	}

	
}
