package com.hz.service.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.Menu;
import com.hz.entity.system.TaskMenu;
import com.hz.mapper.system.MenuMapper;

/**
 * 菜单模块
 * @author tsp
 */
@Service
@Transactional
public class MenuService {
	
	@Autowired
	private MenuMapper menuMapper;
	
	/**
	 * 根据id获取菜单对象
	 * @param id
	 * @return
	 */
	public Menu getMenuById(String id){
		Menu menu = menuMapper.getMenuById(id);
		return menu;
	}
	/**
	 * 添加菜单
	 * @param menu
	 */
	public void addMenu(Menu menu){
		menuMapper.saveMenu(menu);
	}
	/**
	 * 修改菜单
	 * @param menu
	 */
	public void updateMenu(Menu menu){
		menuMapper.updateMenu(menu);
	}
	/**
	 * 删除菜单
	 * @param id
	 */
	public void deleteMenu(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	menuMapper.deleteMenu(ids[i]);
	    }
	}
	/**
	 * 根据条件获取菜单list
	 * @return
	 */
	public List<Menu> getMenuList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return menuMapper.getMenuList(paramMap);
	}
	public List<TaskMenu> getTaskMenuList() {
		return menuMapper.getTaskMenuList();
	}
	/**
	 * 根据条件获取菜单PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Menu> getMenuPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Menu> list=menuMapper.getMenuList(paramMap);
		PageInfo<Menu> page = new PageInfo<Menu>(list);
		return page; 
	}
	
	public List<Menu> getMenuListByUser(String userId,String order){
		PageHelper.orderBy(order);
		return menuMapper.getMenuListByUser(userId);
	}
	public List<Menu> getMenuListByUserDept(String userId, String order) {
		PageHelper.orderBy(order);
		return menuMapper.getMenuListByUserDept(userId);
	}
	/**
	 * <!--根据部门权限获取首页任务菜单  -->
	 */
	public List<TaskMenu> getTaskMenuListByUserDept(String userId) {
		return menuMapper.getTaskMenuListByUserDept(userId);
	}
}
