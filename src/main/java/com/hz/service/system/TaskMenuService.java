package com.hz.service.system;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.TaskMenu;
import com.hz.mapper.system.TaskMenuMapper;

/**
 * 菜单模块
 * @author tsp
 */
@Service
@Transactional
public class TaskMenuService {
	
	@Autowired
	private TaskMenuMapper taskMenuMapper;
	
	/**
	 * 根据id获取菜单对象
	 * @param id
	 * @return
	 */
	public TaskMenu getTaskMenuById(String id){
		TaskMenu taskMenu = taskMenuMapper.getTaskMenuById(id);
		return taskMenu;
	}
	/**
	 * 添加菜单
	 * @param menu
	 */
	public void addTaskMenu(TaskMenu menu){
		taskMenuMapper.saveTaskMenu(menu);
	}
	/**
	 * 修改菜单
	 * @param menu
	 */
	public void updateTaskMenu(TaskMenu menu){
		taskMenuMapper.updateTaskMenu(menu);
	}
	/**
	 * 删除菜单
	 * @param id
	 */
	public void deleteTaskMenu(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	taskMenuMapper.deleteTaskMenu(ids[i]);
	    }
	}
	/**
	 * 根据条件获取菜单list
	 * @return
	 */
	public List<TaskMenu> getTaskMenuList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return taskMenuMapper.getTaskMenuList(paramMap);
	}
	/**
	 * 根据条件获取菜单PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<TaskMenu> getTaskMenuPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<TaskMenu> list=taskMenuMapper.getTaskMenuList(paramMap);
		PageInfo<TaskMenu> page = new PageInfo<TaskMenu>(list);
		return page; 
	}
	
	/*public List<TaskMenu> getTaskMenuListByUser(String userId,String order){
		PageHelper.orderBy(order);
		return taskMenuMapper.getTaskMenuListByUser(userId);
	}*/
	public List<TaskMenu> getTaskMenuListByUserDept(String userId, String order) {
		PageHelper.orderBy(order);
		return taskMenuMapper.getTaskMenuListByUserDept(userId);
	}
}
