package com.hz.service.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.Menu;
import com.hz.entity.system.Role;
import com.hz.entity.system.RoleMenu;
import com.hz.entity.system.UserRole;
import com.hz.mapper.system.RoleMapper;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 职位模块
 * @author tsp
 */
@Service
@Transactional
public class RoleService {
	
	@Autowired
	private RoleMapper roleMapper;
	
	/**
	 * 根据id获取职位对象
	 * @param id
	 * @return
	 */
	public Role getRoleById(String id){
		Role role = roleMapper.getRoleById(id);
		return role;
	}
	/**
	 * 添加职位
	 * @param role
	 */
	public void addRole(Role role){
		roleMapper.saveRole(role);
	}
	/**
	 * 修改职位
	 * @param role
	 */
	public void updateRole(Role role){
		roleMapper.updateRole(role);
	}
	/**
	 * 删除职位
	 * @param id
	 */
	public void deleteRole(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	roleMapper.deleteRole(ids[i]);;
	    }
	}
	/**
	 * 根据条件获取职位list
	 * @return
	 */
	public List<Role> getRoleList(Map<String, Object> paramMap){
		return roleMapper.getRoleList(paramMap);
	}
	/**
	 * 根据条件获取职位PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Role> getRolePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Role> list=roleMapper.getRoleList(paramMap);
		PageInfo<Role> page = new PageInfo<Role>(list);
		return page; 
	}
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Role role=getRoleById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		role.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				role.setIsdel(Const.Isdel_Yes);
			}
	    	updateRole(role);
	    }
	}
	
	//职位菜单功能
	/**
	 * 根据id查找RoleMenu
	 * @param id
	 * @return
	 */
	public RoleMenu getRoleMenuById(String id){
		return roleMapper.getRoleMenuById(id);
	}
	
	/**
	 * 添加修改RoleMenu
	 * @param id
	 * @param roleId
	 * @param menuId
	 */
	public void addOrEditRoleMenu(String roleId,String menuIds){
		List<RoleMenu> list = getRoleMenuList(roleId, "", "");
		//清空所有
		for(int i=0;i<list.size();i++){
			RoleMenu rm = list.get(i);
			roleMapper.deleteRoleMenu(rm.getId());
		}
		for(int i=0;i<menuIds.split(",").length;i++){
			RoleMenu roleMenu = new RoleMenu();
			roleMenu.setId(UuidUtil.get32UUID());
			roleMenu.setMenuId(menuIds.split(",")[i]);
			roleMenu.setRoleId(roleId);
			roleMapper.saveRoleMenu(roleMenu);
			
		}
	}

	/**
	 * 新增职位菜单
	 * @param roleId
	 * @param menuId
	 */
	public void addRoleMenu(String roleId,String menuId){
		List<RoleMenu> list = getRoleMenuList(roleId, menuId, "");
		if(list.size()<=0){
			RoleMenu roleMenu = new RoleMenu();
			roleMenu.setId(UuidUtil.get32UUID());
			roleMenu.setMenuId(menuId);
			roleMenu.setRoleId(roleId);
			roleMapper.saveRoleMenu(roleMenu);
		}
		
	}
	
	/**
	 * 删除职位菜单RoleMenu
	 * @param id
	 */
	public void delRoleMenu(String roleId,String menuId){
		List<RoleMenu> list = getRoleMenuList(roleId, menuId, "");
		//删除
		for(int i=0;i<list.size();i++){
			RoleMenu rm = (RoleMenu)list.get(i);
			roleMapper.deleteRoleMenu(rm.getId());
		}
	}
	
	/**
	 * 删除职位菜单RoleMenu
	 * @param id
	 */
	public void deleteRoleMenu(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	roleMapper.deleteRoleMenu(ids[i]);;
	    }
	}
	
	/**
	 * 查询职位菜单RoleMenu列表
	 * @param roleId
	 * @param menuId
	 * @param order
	 * @return
	 */
	public List<RoleMenu> getRoleMenuList(String roleId,String menuId,String order){
		Map<String, Object> paramMap=new HashMap<String, Object>();
		paramMap.put("roleId", roleId);
		paramMap.put("menuId", menuId);
		PageHelper.orderBy(order);
		return roleMapper.getRoleMenuList(paramMap);
	}
	
	/**
	 * 包含职位菜单选中标志的所有菜单List
	 * @param roleId
	 * @param order
	 * @return
	 */
	public List<Menu> getMenuList(String roleId,String order){
		Map<String, Object> paramMap=new HashMap<String, Object>();
		paramMap.put("roleId", roleId);
		PageHelper.orderBy(order);
		return roleMapper.getMenuList(paramMap);
	}

	/**
	 * 包含用户职位选中标志的所有职位List
	 * @param roleId
	 * @param order
	 * @return
	 */
	public List<Role> getRoleListByUser(String userId,String order){
		Map<String, Object> paramMap=new HashMap<String, Object>();
		paramMap.put("userId", userId);
		PageHelper.orderBy(order);
		return roleMapper.getRoleListByUser(paramMap);
	}
	
	/**
	 * 保存用户职位
	 * @param roleIds
	 * @param userId
	 */
	public void saveUserRole(String[] roleIds,String userId){
		Map<String, Object> paramMap=new HashMap<String, Object>();
		paramMap.put("userId", userId);
		List<UserRole> list = roleMapper.getUserRoleList(paramMap);
		for(int i=0;i<list.size();i++){
			UserRole userRole = list.get(i);
			roleMapper.deleteUserRole(userRole);
		}
		if (roleIds != null && !roleIds.equals("") ) {
			for(int i=0;i<roleIds.length;i++){
				UserRole userRole = new UserRole();
				userRole.setId(UuidUtil.get32UUID());
				userRole.setRoleId(roleIds[i]);
				userRole.setUserId(userId);
				roleMapper.saveUserRole(userRole);
			}
		}
	}
}
