package com.hz.service.system;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Item;
import com.hz.entity.socket.UserNotice;
import com.hz.entity.system.User;
import com.hz.mapper.system.UserMapper;
import com.hz.service.socket.UserNoticeService;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 用户服务
 * @author ljx
 */
@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserMapper userMapper;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	DeptService deptService;
	/**
	 * 根据id获取用户
	 * @param id
	 * @return
	 */
	public User getUserById(String id){
		User user = userMapper.getUserById(id);
		return user;
	}
	
	/**
	 * 添加用户
	 * @param user
	 */
	public void addUser(User user){
		userMapper.saveUser(user);
		addUserNotice(user);
	}
	
	/**
	 * 产生一个用户则根据部门插入对应的
	 * 通知模块
	 * @param user
	 */
	private void addUserNotice(User user) {
		String ykId = deptService.getDeptIdByName("一科");
		String ekId = deptService.getDeptIdByName("二科");
		String admin = deptService.getDeptIdByName("管理");
		if (user.getDeptId().equals(ykId)) {
			String id1 = UuidUtil.get32UUID();
			String id2 = UuidUtil.get32UUID();
			String id3 = UuidUtil.get32UUID();
			UserNotice un = new UserNotice();
			un.setDeptId(ykId);
			un.setUserId(user.getUserId());
			
			un.setId(id1);
			un.setNoticeType(Const.NoticeType_FarewellTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id2);
			un.setNoticeType(Const.NoticeType_MourningTask);
			userNoticeService.saveUserNotice(un);
			un.setId(id3);
			un.setNoticeType(Const.NoticeType_FuneralTask);
			userNoticeService.saveUserNotice(un);
		} else if(user.getDeptId().equals(ekId)) {
			String id = UuidUtil.get32UUID();
			String id2 = UuidUtil.get32UUID();
			String id3 = UuidUtil.get32UUID();
			
			UserNotice un = new UserNotice();
			un.setDeptId(ekId);
			un.setUserId(user.getUserId());
			
			un.setId(id);
			un.setNoticeType(Const.NoticeType_YsjdTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id2);
			un.setNoticeType(Const.NoticeType_CarTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id3);
			un.setNoticeType(Const.NoticeType_FysytjdTask);
			userNoticeService.saveUserNotice(un);
		} else if(user.getDeptId().equals(admin)) {
			String id = UuidUtil.get32UUID();
			String id2 = UuidUtil.get32UUID();
			String id3 = UuidUtil.get32UUID();
			String id4 = UuidUtil.get32UUID();
			String id5 = UuidUtil.get32UUID();
			String id6 = UuidUtil.get32UUID();
			
			UserNotice un = new UserNotice();
			un.setDeptId(admin);
			un.setUserId(user.getUserId());
			
			un.setId(id);
			un.setNoticeType(Const.NoticeType_YsjdTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id2);
			un.setNoticeType(Const.NoticeType_CarTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id3);
			un.setNoticeType(Const.NoticeType_FysytjdTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id4);
			un.setNoticeType(Const.NoticeType_FarewellTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id5);
			un.setNoticeType(Const.NoticeType_MourningTask);
			userNoticeService.saveUserNotice(un);
			
			un.setId(id6);
			un.setNoticeType(Const.NoticeType_FuneralTask);
			userNoticeService.saveUserNotice(un);
		}
	}

	/**
	 * 修改用户
	 * @param user
	 */
	public void updateUser(User user){
		userMapper.updateUser(user);
	}
	public void updateUserAndNotice(String oldDeptId, User user) {
		userMapper.updateUser(user);
		if (!oldDeptId.equals(user.getDeptId())) {
			userNoticeService.deleteUserNotice(user.getUserId());
			addUserNotice(user);
		}
	}
	/**
	 * 删除用户
	 * @param id
	 */
	public void deleteUser(String id){
		String[] ids = id.split(",");
		for(int i =0;i<ids.length;i++){
			userMapper.deleteUser(ids[i]);
			userNoticeService.deleteUserNotice(ids[i]);//用户通知表里对应的用户也要删掉
		}
	}
	/**
	 * 根据条件得用户Option
	 * @param paramMap
	 * @param userId
	 * @param isAll
	 * @return
	 */
	public String getItemOption(Map<String ,Object> paramMap,String userId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<User> userList=userMapper.getUserList(paramMap);
		String option="";
		for (int i = 0; i < userList.size(); i++) {
			User user=(User)userList.get(i);
			list.add(new Object[]{user.getUserId(),user.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getOptionByList(list, userId, false);
		return option;
	}
	/**
	 * 获取一科礼厅工作人员
	 */
	public String getFirstNameOption(Map<String ,Object> paramMap,String userId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<User> userList=userMapper.getFirstUserList(paramMap);
		String option="";
		for (int i = 0; i < userList.size(); i++) {
			User user=(User)userList.get(i);
			list.add(new Object[]{user.getUserId(),user.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getOptionByList(list, userId, false);
		return option;
	}
	/**
	 * 获取一科前台科员
	 */
	public String getFirstBeforeNameOption(Map<String ,Object> paramMap,String userId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<User> userList=userMapper.getFirstBeforeUserList(paramMap);
		String option="";
		for (int i = 0; i < userList.size(); i++) {
			User user=(User)userList.get(i);
			list.add(new Object[]{user.getUserId(),user.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getOptionByList(list, userId, false);
		return option;
	}
	/**
	 * 获取用户list
	 * @return
	 */
	public List<User> getUserList(Map<String,Object> paramMap) {
		return userMapper.getUserList(paramMap);
	}
	
	/**
	 * 获取用户分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<User> getUserPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		//设置页码和每页数
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<User> list = userMapper.getUserList(paramMap);
		PageInfo<User> page=new PageInfo<User>(list);
		return page;
	}
	
	/**
	 * 根据登陆账号获取用户
	 * @param userName
	 * @return
	 */
	public User getUserByUserName(String userName){
		User user = userMapper.getUserByUserName(userName);
		return user;
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			User user=getUserById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		user.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				user.setIsdel(Const.Isdel_Yes);
			}
	    	updateUser(user);
	    }
	}

	/** 查看此编号是否已存在
	 */
	public int getCodeCount(String code) {
		return userMapper.getCodeCount(code);
	}
	/** 查看此用户名是否已存在
	 */
	public int getUserNameCount(String userName) {
		return userMapper.getuserNameCount(userName);
	}
	/**
	 * 根据ID得到name
	 * @param enterId
	 * @return
	 */
	public String getNameById(String enterId) {
		return userMapper.getNameById(enterId);
	}

	/**
	 * 根据职位名称取出用户
	 * @param arranger
	 * @return
	 */
	public List<User> getUserListByRoleName(String arranger) {
		return userMapper.getUserListByRoleName(arranger);
	}

	/**
	 * 根据部门名称取出用户  option
	 * @param id
	 * isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getuserOption(String userName,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<User> userList=userMapper.getDepUser(null);
		//userList.get(index)
		for (int i = 0; i < userList.size(); i++) {
			User user=(User)userList.get(i);
			list.add(new Object[]{ user.getUserId() ,user.getUserName()});
		}
		String option=DataTools.getOptionByList(list, userName, isAll);
		return option;
	}
	
	
	


	/**
	 * 得到对应职位的下拉框
	 * @param roleName
	 * @return
	 */
	public String getRoleOption(String roleName,String id) {
		List<Object[]> list=new ArrayList<Object[]>();
		List<User> listUser = userMapper.getUserListByRoleName(roleName);
		for (User user : listUser) {
			list.add(new Object[]{user.getUserId(),user.getName()});
		}
		String option=DataTools.getOptionByList(list, id, false);
		return option;
	}

	public String getUserNameById(String id) {
		return userMapper.getUserNameById(id);
	}
}
