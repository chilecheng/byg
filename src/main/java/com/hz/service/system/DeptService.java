package com.hz.service.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.Dept;
import com.hz.entity.system.DeptMenu;
import com.hz.entity.system.DeptTaskMenu;
import com.hz.entity.system.Menu;
import com.hz.entity.system.TaskMenu;
import com.hz.mapper.system.DeptMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 部门模块
 * @author tsp
 */
@Service
@Transactional
public class DeptService {
	
	@Autowired
	private DeptMapper deptMapper;
	
	/**
	 * 根据id获取部门对象
	 * @param id
	 * @return
	 */
	public Dept getDeptById(String id){
		Dept dept = deptMapper.getDeptById(id);
		return dept;
	}
	/**
	 * 添加部门
	 * @param dept
	 */
	public void addDept(Dept dept){
		deptMapper.saveDept(dept);
	}
	/**
	 * 修改部门
	 * @param dept
	 */
	public void updateDept(Dept dept){
		deptMapper.updateDept(dept);
	}
	/**
	 * 删除部门
	 * @param id
	 */
	public void deleteDept(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	deptMapper.deleteDept(ids[i]);;
	    }
	}
	/**
	 * 根据条件获取部门list
	 * @return
	 */
	public List<Dept> getDeptList(Map<String, Object> paramMap){
		return deptMapper.getDeptList(paramMap);
	}
	/**
	 * 根据条件获取部门PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Dept> getDeptPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Dept> list=deptMapper.getDeptList(paramMap);
		PageInfo<Dept> page = new PageInfo<Dept>(list);
		return page; 
	}
	/**
	 * 获取部门Option
	 * @param deptId
	 * @return
	 */
	public String getDeptOption(String deptId){
		List<Object[]> list = new ArrayList<Object[]>();
		Map<String, Object> maps=new HashMap<String, Object>();
		maps.put("fatherId","false" );
		PageHelper.orderBy("index_flag");
		List<Dept> deptList=deptMapper.getDeptList(maps);
		for (int i = 0; i < deptList.size(); i++) {
			Dept dept=(Dept)deptList.get(i);
			list.add(new Object[]{dept.getId(),dept.getName()});
		}
		String option = DataTools.getOptionByList(list, deptId, false);
		return option;
	}
	/**
	 * 获取部门Option
	 * @param deptId
	 * @return
	 */
	public String getDeptOption2(String id,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		Map<String, Object> maps=new HashMap<String, Object>();
	/*	maps.put("fatherId","false" );*/
		PageHelper.orderBy("index_flag");
		List<Dept> deptList=deptMapper.getDeptList(maps);
		for (int i = 0; i < deptList.size(); i++) {
			Dept dept=(Dept)deptList.get(i);
			list.add(new Object[]{dept.getId(),dept.getName()});
		}
		String option = DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Dept dept=getDeptById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		dept.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				dept.setIsdel(Const.Isdel_Yes);
			}
	    	updateDept(dept);
	    }
	}
	/**
	 * 包含部门菜单选中标志的所有菜单List
	 */
	public List<Menu> getMenuList(String deptId, String order) {
		Map<String, Object> paramMap=new HashMap<String, Object>();
		paramMap.put("deptId", deptId);
		PageHelper.orderBy(order);
		return deptMapper.getMenuList(paramMap);
	}
	/**
	 * 包含部门菜单选中标志的所有菜单List
	 */
	public List<TaskMenu> getTaskMenuList(String deptId) {
		Map<String, Object> paramMap=new HashMap<String, Object>();
		paramMap.put("deptId", deptId);
		return deptMapper.getTaskMenuList(paramMap);
	}
	/**
	 * 新增部门菜单
	 */
	public void addDeptMenu(String deptId, String menuId) {
		List<DeptMenu> list = getDeptMenuList(deptId, menuId, "");
		if(list.size()<=0){
			DeptMenu deptMenu = new DeptMenu();
			deptMenu.setId(UuidUtil.get32UUID());
			deptMenu.setMenuId(menuId);
			deptMenu.setDeptId(deptId);
			deptMapper.saveDeptMenu(deptMenu);
		}
	}
	/**
	 * 删除部门菜单DeptMenu
	 * @param id
	 */
	public void delDeptMenu(String deptId, String menuId) {
		List<DeptMenu> list = getDeptMenuList(deptId, menuId, "");
		//删除
		for(int i=0;i<list.size();i++){
			DeptMenu rm = (DeptMenu)list.get(i);
			deptMapper.deleteDeptMenu(rm.getId());
		}
	}
	/**
	 * 删除部门菜单DeptMenu
	 * @param id
	 */
	public void deleteDeptMenu(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	deptMapper.deleteDeptMenu(ids[i]);;
	    }
	}
	/**
	 * 查询部门菜单DeptMenu列表
	 */
	public List<DeptMenu> getDeptMenuList(String deptId,String menuId,String order){
		Map<String, Object> paramMap=new HashMap<String, Object>();
		paramMap.put("deptId", deptId);
		paramMap.put("menuId", menuId);
		PageHelper.orderBy(order);
		return deptMapper.getDeptMenuList(paramMap);
	}
	
	public void deleteDeptTaskMenu(String deptId) {
		deptMapper.deleteDeptTaskMenu(deptId);
	}
	public void addDeptTaskMenu(String deptId, String[] ids) {
		for (String menuId : ids) {
			DeptTaskMenu dtm = new DeptTaskMenu();
			dtm.setId(UuidUtil.get32UUID());
			dtm.setDeptId(deptId);
			dtm.setTaskMenuId(menuId);
			deptMapper.saveDeptTaskMenu(dtm);
		}
	}
	public String getDeptIdByName(String deptName) {
		return deptMapper.getDeptIdByName(deptName);
	}
}
