package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.Tomb;
import com.hz.mapper.cemetery.TombMapper;
/**
 * 陵园信息服务层
 * @author jgj
 *
 */
@Service
@Transactional
public class CemeteryTombService {
	@Autowired
	private TombMapper tombMapper;
	
	/**
	 * 添加陵园墓穴信息
	 * @param tomb
	 */
	public void saveCemeteryTomb(Tomb tomb){
		tombMapper.saveCemeteryTomb(tomb);
	}
	/**
	 * 删除陵园墓穴信息
	 * @param id
	 */
	public void deleteCemeteryTomb(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	tombMapper.deleteCemeteryTomb(ids[i]);
	    }
	}
	/**
	 * 更新陵园墓穴信息
	 * @param tomb
	 */
	public void updateCemeteryTomb(Tomb tomb){
		tombMapper.updateCemeteryTomb(tomb);
	}
	/**
	 * 获取陵园墓穴信息对象
	 * @param id
	 * @return
	 */
	public Tomb getCemeteryTombById(String id){
		Tomb tomb=tombMapper.getCemeteryTombById(id);
		return tomb;
	}
	
	/**
	 * 根据条件获取陵园墓穴信息
	 * @param String name
	 * @param byte isdel
	 */
	public List<Tomb> getCemeteryTombList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<Tomb> list= tombMapper.getCemeteryTombList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取陵园墓穴信息PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Tomb> getCemeteryTombPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Tomb> list=tombMapper.getCemeteryTombList(paramMap);
		PageInfo<Tomb> page = new PageInfo<Tomb>(list);
		return page; 
	}
	

}
