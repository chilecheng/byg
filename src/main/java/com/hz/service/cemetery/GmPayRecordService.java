package com.hz.service.cemetery;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.PayRecord;
import com.hz.mapper.cemetery.GmPayRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @Classname GmPayRecordService
 * @Description TODO
 * @Date 2019/7/22 17:50
 * @Created by ZhixiangWang
 */
@Service
@Transactional
public class GmPayRecordService {
    @Autowired
    private GmPayRecordMapper gmPayRecordMapper;

    /**
     * 添加已退墓穴记录
     *
     * @param payRecord
     */
    public void saveGmPayRecord(PayRecord payRecord) {
        gmPayRecordMapper.saveGmPayRecord(payRecord);
    }

    /**
     * 删除已退墓穴记录
     *
     * @param id
     */
    public void deleteGmPayRecord(String id) {
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            gmPayRecordMapper.deleteGmPayRecord(ids[i]);
        }
    }

    /**
     * 更新已退墓穴记录
     *
     * @param payRecord
     */
    public void updateGmPayRecord(PayRecord payRecord) {
        gmPayRecordMapper.updateGmPayRecord(payRecord);
    }

    /**
     * 获取已退墓穴记录 根据 Id
     *
     * @param id
     * @return
     */
    public PayRecord getGmPayRecordById(String id) {
        PayRecord payRecord = gmPayRecordMapper.getGmPayRecordById(id);
        return payRecord;
    }

    /**
     * 根据条件获取已退墓穴记录List
     *
     * @param paramMap
     * @param order
     */
    public List<PayRecord> getGmPayRecordList(Map<String, Object> paramMap, String order) {
        PageHelper.orderBy(order);
        List<PayRecord> list = gmPayRecordMapper.getGmPayRecordList(paramMap);
        return list;
    }

    /**
     * 根据条件获取已退墓穴记录PageInfo
     *
     * @param paramMap 条件
     * @param pageNum  当前页数
     * @param pageSize 每页个数
     * @param order    排序
     * @return
     */
    public PageInfo<PayRecord> getGmPayRecordPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
        PageHelper.startPage(pageNum, pageSize);
        PageHelper.orderBy(order);
        List<PayRecord> list = gmPayRecordMapper.getGmPayRecordList(paramMap);
        PageInfo<PayRecord> page = new PageInfo<PayRecord>(list);
        return page;
    }
}
