package com.hz.service.cemetery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.BuyRecord;
import com.hz.entity.cemetery.MaintenancePaymentedRecord;
import com.hz.entity.cemetery.PayRecord;
import com.hz.entity.cemetery.PaymentedRecord;
import com.hz.entity.cemetery.UserRecord;
import com.hz.mapper.cemetery.BuyRecordMapper;
import com.hz.mapper.cemetery.MaintenancePaymentRecordMapper;
import com.hz.mapper.cemetery.PayRecordMapper;
import com.hz.mapper.cemetery.PaymentedRecordMapper;
import com.hz.mapper.cemetery.TombMapper;
import com.hz.mapper.cemetery.UserRecordMapper;

/**
 * 统计报表
 * 
 * @author wujiayang
 *
 */
@Service
@Transactional
public class ReportService {
	@Autowired
	private PaymentedRecordMapper paymentedRecordMapper;
	@Autowired
	private MaintenancePaymentRecordMapper maintenancePaymentRecordMapper;
	@Autowired 
	private PayRecordMapper payRecordMapper;
	@Autowired 
	private UserRecordMapper userRecordMapper;
	@Autowired 
	private BuyRecordMapper buyRecordMapper;
	@Autowired 
	private TombMapper tombMapper;

	
	/**
	 * 墓穴费统计
	 * @param 开始时间
	 * @param 结束时间
	 */
	public PageInfo<PaymentedRecord> getCemeteryrecord(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<PaymentedRecord> list=paymentedRecordMapper.getPaymentCemeteryList(paramMap);
		PageInfo<PaymentedRecord> page = new PageInfo<PaymentedRecord>(list);
		return page;
	}
	/**
	 * 维护统计
	 * @param 开始时间
	 * @param 结束时间
	 */
	public PageInfo<MaintenancePaymentedRecord> getMaintenancerecord(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<MaintenancePaymentedRecord> list=maintenancePaymentRecordMapper.getMaintenancePaymentCemeteryList(paramMap);
		PageInfo<MaintenancePaymentedRecord> page = new PageInfo<MaintenancePaymentedRecord>(list);
		return page;
	}
	/**
	 * 雕刻安放费统计
	 * @param 开始时间
	 * @param 结束时间
	 */
	public PageInfo<PayRecord> getPaycerecord(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<PayRecord> list=payRecordMapper.getpayRecordCemeteryList(paramMap);
		PageInfo<PayRecord> page = new PageInfo<PayRecord>(list);
		return page;
	}
	/*
	 * 按条件获取墓穴费总计数额
	 * */
	public int getCemeteryrecord(Map<String,Object> paramMap) {
		Integer result=paymentedRecordMapper.getsumvalue(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 按条件获取维护费总计数额
	 * */
	public int getMaintenancerecord(Map<String,Object> paramMap) {
		Integer result=maintenancePaymentRecordMapper.getsumvalue(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 按条件获取掉雕刻安放费总计数额
	 * */
	public int getPaycerecord(Map<String,Object> paramMap) {
		Integer result=payRecordMapper.getsumvalue(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 按条件获取安放量
	 * */
	public PageInfo<UserRecord> getplacementrecord(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<UserRecord> list=userRecordMapper.getUserCemeteryRecordList(paramMap);
		PageInfo<UserRecord> page = new PageInfo<UserRecord>(list);
		return page;
	}
	/*
	 * 按条件获取安放量
	 * */
	public int getplacementrecord(Map<String,Object> paramMap) {
		Integer result=userRecordMapper.getsumvalue(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 按条件获取迁移费总计
	 * */
	public int getMoverecord(Map<String,Object> paramMap) {
		Integer result=payRecordMapper.getMovesumvalue(paramMap);
		result = result==null?0:result;
		return result;
	}
	/**
	 * 墓穴销售统计
	 * @param 开始时间
	 * @param 结束时间
	 */
	public PageInfo<BuyRecord> getsaleRecord(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<BuyRecord> list=buyRecordMapper.getSaleReport(paramMap);
		PageInfo<BuyRecord> page = new PageInfo<BuyRecord>(list);
		return page;
	}
	/**
	 * 墓穴销售
	 * @param 开始时间
	 * @param 结束时间
	 */
	public PageInfo<BuyRecord> getsalemoneyRecord(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<BuyRecord> list=buyRecordMapper.getsalemoney(paramMap);
		PageInfo<BuyRecord> page = new PageInfo<BuyRecord>(list);
		return page;
	}
	/*
	 * 获取封龙门次数
	 * */
	public int getCloserecord(Map<String,Object> paramMap) {
		Integer result=payRecordMapper.getCloseTimes(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 按条件获取墓穴销售数量
	 * */
	public int getPaymentedTime(Map<String,Object> paramMap) {
		Integer result=paymentedRecordMapper.getPaymentedTime(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 查询墓穴建成数量
	 * */
	public int getTomb(Map<String,Object> paramMap) {
		Integer result=tombMapper.getTomb(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 查询墓穴已售出数量不加时间条件
	 * */
	public int PaymentedNumber(Map<String,Object> paramMap) {
		Integer result=paymentedRecordMapper.PaymentedNumber(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 查询墓穴以往已售总金额（2013年前后）
	 * */
	public int PaymentedMoney(Map<String,Object> paramMap) {
		Integer result=paymentedRecordMapper.PaymentedMoney(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 查询墓穴以往已售金额最小值
	 * */
	public int PaymentedMinMoney(Map<String,Object> paramMap) {
		Integer result=paymentedRecordMapper.PaymentedMinMoney(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 查询墓穴以往已售金额最大值
	 * */
	public int PaymentedMaxMoney(Map<String,Object> paramMap) {
		Integer result=paymentedRecordMapper.PaymentedMaxMoney(paramMap);
		result = result==null?0:result;
		return result;
	}
	/*
	 * 获取所有安放量
	 * */
	public int getplacementrecords(Map<String,Object> paramMap) {
		Integer result=userRecordMapper.getsumvalues(paramMap);
		result = result==null?0:result;
		return result;
	}
	
}
