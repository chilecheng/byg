package com.hz.service.cemetery.record;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.MaintenancePaymentedRecord;
import com.hz.mapper.cemetery.MaintenancePaymentRecordMapper;

/**
*Describtion:墓穴维护缴费记录

*Creatime:2019年6月25日

*Author:wujiayang

*Comment:引用MaintenancePaymentedRecord类

**/
@Service
@Transactional
public class MaintenancePaymentRecordService {
	@Autowired
	private  MaintenancePaymentRecordMapper  maintenancePaymentRecordMapper;
	public PageInfo<MaintenancePaymentedRecord> getmaintenancePaymentRecord(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<MaintenancePaymentedRecord> list=maintenancePaymentRecordMapper.getListMaintenancePaymentRecord(paramMap);
		PageInfo<MaintenancePaymentedRecord> page = new PageInfo<MaintenancePaymentedRecord>(list);
		return page;
	}
}
