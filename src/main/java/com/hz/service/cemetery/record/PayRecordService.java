package com.hz.service.cemetery.record;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.PayRecord;
import com.hz.mapper.cemetery.PayRecordMapper;

/**
*Describtion:获取普通收费记录信息

*Creatime:2019年6月28日

*Author:wujiayang

*Comment:

**/
@Service
@Transactional
public class PayRecordService {

	@Autowired
	private  PayRecordMapper  payRecordMapper;

	/**
	 * 添加墓穴普通收费记录信息
	 *
	 * @param payRecord
	 */
	public void savePayRecord(PayRecord payRecord) {
		payRecordMapper.savePayRecord(payRecord);
	}

	/**
	 * 删除墓穴普通收费记录信息
	 *
	 * @param id
	 */
	public void deletePayRecord(String id) {
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			payRecordMapper.deletePayRecord(ids[i]);
		}
	}

	/**
	 * 更新墓穴普通收费记录信息
	 *
	 * @param payRecord
	 */
	public void updatePayRecord(PayRecord payRecord) {
		payRecordMapper.updatePayRecord(payRecord);
	}

	/**
	 * 获取墓穴普通收费记录信息 根据 Id
	 *
	 * @param id
	 * @return
	 */
	public PayRecord getPayRecordById(String id) {
		PayRecord payRecord = payRecordMapper.getPayRecordById(id);
		return payRecord;
	}

	/**
	 * 根据条件获取墓穴普通收费记录信息List
	 *
	 * @param String name
	 */
	public List<PayRecord> getPayRecordList(Map<String, Object> paramMap, String order) {
		PageHelper.orderBy(order);
		List<PayRecord> list = payRecordMapper.getPayRecordList(paramMap);
		return list;
	}

	/**
	 * 根据条件获取墓穴普通收费记录信息PageInfo
	 *
	 * @param paramMap 条件
	 * @param pageNum  当前页数
	 * @param pageSize 每页个数
	 * @param order    排序
	 * @return
	 */
	public PageInfo<PayRecord> getPayRecordPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<PayRecord> list = payRecordMapper.getPayRecordList(paramMap);
		PageInfo<PayRecord> page = new PageInfo<PayRecord>(list);
		return page;
	}
	/**
	 * 获取陵园信息对象
	 * @param id
	 * @return
	 */
public List<PayRecord> getCemeteryList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<PayRecord> list= payRecordMapper.getPayRecordList(paramMap);
		return list;
	}
}
