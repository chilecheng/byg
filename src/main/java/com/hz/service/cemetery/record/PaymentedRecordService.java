package com.hz.service.cemetery.record;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.PaymentedRecord;
import com.hz.mapper.cemetery.PaymentedRecordMapper;

/**
*Describtion:已付款墓穴缴费记录查询

*Creatime:2019年6月25日

*Author:wujiayang

*Comment:

**/
@Service
@Transactional
public class PaymentedRecordService {

	@Autowired
	private  PaymentedRecordMapper  paymentedRecordMapper;
	/**
	 * 根据条件获取墓穴购买缴费记录信息PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<PaymentedRecord> getCemeteryrecord(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<PaymentedRecord> list=paymentedRecordMapper.getListPaymentedRecord(paramMap);
		PageInfo<PaymentedRecord> page = new PageInfo<PaymentedRecord>(list);
		return page;
	}
}
