package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryArea;
import com.hz.mapper.cemetery.CemeteryAreaMapper;
import com.hz.mapper.cemetery.CemeteryMapper;
/**
 * 陵园区域信息服务层
 * @author jgj
 *
 */
@Service
@Transactional
public class CemeteryAreaService {
	@Autowired
	private CemeteryAreaMapper cemeteryAreaMapper;
	
	/**
	 * 添加陵园区域信息
	 * @param cemetery
	 */
	public void saveCemeteryArea(CemeteryArea cemeteryArea){
		cemeteryAreaMapper.saveCemeteryArea(cemeteryArea);
	}
	/**
	 * 删除陵园区域信息
	 * @param id
	 */
	public void deleteCemeteryArea(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	cemeteryAreaMapper.deleteCemeteryArea(ids[i]);
	    }
	}
	/**
	 * 更新陵园区域信息
	 * @param cemeteryArea
	 */
	public void updateCemeteryArea(CemeteryArea cemeteryArea){
		cemeteryAreaMapper.updateCemeteryArea(cemeteryArea);
	}
	/**
	 * 获取陵园区域信息对象
	 * @param id
	 * @return
	 */
	public CemeteryArea getCemeteryAreaById(String id){
		CemeteryArea cemeteryArea=cemeteryAreaMapper.getCemeteryAreaById(id);
		return cemeteryArea;
	}
	
	/**
	 * 根据条件获取陵园区域信息
	 * @param String name
	 * @param byte isdel
	 */
	public List<CemeteryArea> getCemeteryAreaList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<CemeteryArea> list= cemeteryAreaMapper.getCemeteryAreaList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取陵园区域信息PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<CemeteryArea> getCemeteryAreaPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<CemeteryArea> list=cemeteryAreaMapper.getCemeteryAreaList(paramMap);
		PageInfo<CemeteryArea> page = new PageInfo<CemeteryArea>(list);
		return page; 
	}
	

}
