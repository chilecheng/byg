package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.BuyRecord;
import com.hz.entity.cemetery.PaymentRecord;
import com.hz.mapper.cemetery.BuyRecordMapper;
import com.hz.mapper.cemetery.PaymentRecordMapper;

/**
 * 墓穴购买记录信息 服务层
 *
 * @author jgj
 */
@Service
@Transactional
public class BuyRecordService {
    @Autowired
    private BuyRecordMapper buyRecordMapper;
    @Autowired
    private PaymentRecordMapper paymentRecordMapper;

    /**
     * 添加墓穴购买记录
     *
     * @param buyRecord
     */
    public void saveBuyRecord(BuyRecord buyRecord) {
        buyRecordMapper.saveBuyRecord(buyRecord);
    }

    /**
     * 删除墓穴购买记录
     *
     * @param id
     */
    public void deleteBuyRecord(String id) {
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            buyRecordMapper.deleteBuyRecord(ids[i]);
        }
    }

    /**
     * 更新墓穴购买记录
     *
     * @param buyRecord
     */
    public void updateBuyRecord(BuyRecord buyRecord) {
        buyRecordMapper.updateBuyRecord(buyRecord);
    }

    /**
     * 获取墓穴购买记录 根据 Id
     *
     * @param id
     * @return
     */
    public BuyRecord getBuyRecordById(String id) {
        BuyRecord buyRecord = buyRecordMapper.getBuyRecordById(id);
        return buyRecord;
    }

    /**
     * 根据墓穴ID获取购买记录信息
     *
     * @param tombId
     * @return
     */
    public BuyRecord getBuyRecordByTombId(String tombId) {
        BuyRecord buyRecord = buyRecordMapper.getBuyRecordByTombId(tombId);
        return buyRecord;
    }

    /**
     * 根据条件获取墓穴购买记录List
     *
     * @param paramMap
     * @param order
     */
    public List<BuyRecord> getBuyRecordList(Map<String, Object> paramMap, String order) {
        PageHelper.orderBy(order);
        List<BuyRecord> list = buyRecordMapper.getBuyRecordList(paramMap);
        return list;
    }

    /**
     * 根据条件获取墓穴购买记录PageInfo
     *
     * @param paramMap 条件
     * @param pageNum  当前页数
     * @param pageSize 每页个数
     * @param order    排序
     * @return
     */
    public PageInfo<BuyRecord> getBuyRecordPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
        PageHelper.startPage(pageNum, pageSize);
        PageHelper.orderBy(order);
        List<BuyRecord> list = buyRecordMapper.getBuyRecordList(paramMap);
        PageInfo<BuyRecord> page = new PageInfo<BuyRecord>(list);
        return page;
    }

    /**
     * 通过墓穴id 获取对应墓穴支付记录
     *
     * @param tombId
     * @return
     */
    public PaymentRecord getPaymentRecordByTombId(String tombId) {
        BuyRecord buyRecord = buyRecordMapper.getBuyRecordByTombId(tombId);
        PaymentRecord pr = null;
        if (buyRecord != null) {
            pr = paymentRecordMapper.getPaymentRecordByTombId(buyRecord.getId());
            if (pr != null) {
                if (null == pr.getRemark()) {
                    pr.setRemark(buyRecord.getComment());
                }
                if (null == pr.getOperator()) {
                    pr.setOperator(buyRecord.getCreatePeople());
                }
            }
        }
        return pr;
    }

    /**
     * 根据条件获取 墓穴持证人及使用者信息List
     *
     * @param maps  条件
     * @param order 排序
     * @return java.util.List<com.hz.entity.cemetery.BuyRecord>
     * @Auther: ZhixiangWang on 2019/7/26 13:51
     * @Description: TODO
     */
    public List<BuyRecord> getBuyAndUserRecordList(Map<String, Object> maps, String order) {
        PageHelper.orderBy(order);
        List<BuyRecord> list = buyRecordMapper.getBuyAndUserRecordList(maps);
        return list;
    }

    /**
     * 根据条件获取 墓穴持证人及使用者信息PageInfo
     *
     * @param paramMap 条件
     * @param pageNum  当前页数
     * @param pageSize 每页个数
     * @param order    排序
     * @return com.github.pagehelper.PageInfo<com.hz.entity.cemetery.BuyRecord>
     * @Auther: ZhixiangWang on 2019/7/26 13:50
     * @Description: TODO
     */
    public PageInfo<BuyRecord> getBuyAndUserRecordPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
        PageHelper.startPage(pageNum, pageSize);
        PageHelper.orderBy(order);
        List<BuyRecord> list = buyRecordMapper.getBuyAndUserRecordList(paramMap);
        PageInfo<BuyRecord> page = new PageInfo<BuyRecord>(list);
        return page;
    }

}
