package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import com.hz.util.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.Cemetery;
import com.hz.mapper.cemetery.CemeteryMapper;

/**
 * 陵园信息服务层
 *
 * @author jgj
 */
@Service
@Transactional
public class CemeteryService {
    @Autowired
    private CemeteryMapper cemeteryMapper;

    /**
     * 添加陵园信息
     *
     * @param cemetery
     */
    public void saveCemetery(Cemetery cemetery) {
        cemeteryMapper.saveCemetery(cemetery);
    }

    /**
     * 删除陵园信息
     *
     * @param id
     */
    public void deleteCemetery(String id) {
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            Cemetery cemetery = cemeteryMapper.getCemeteryById(ids[i]);
            cemetery.setIsdel(Const.Isdel_Yes);
            cemeteryMapper.updateCemetery(cemetery);
//	    	cemeteryMapper.deleteCemetery(ids[i]);

        }
    }

    /**
     * 更新陵园信息
     *
     * @param cemetery
     */
    public void updateCemetery(Cemetery cemetery) {
        cemeteryMapper.updateCemetery(cemetery);
    }

    /**
     * 获取陵园信息对象
     *
     * @param id
     * @return
     */
    public Cemetery getCemeteryById(String id) {
        Cemetery cemetery = cemeteryMapper.getCemeteryById(id);
        return cemetery;
    }

    /**
     * 根据条件获取陵园信息
     *
     * @param paramMap
     * @param order
     */
    public List<Cemetery> getCemeteryList(Map<String, Object> paramMap, String order) {
        PageHelper.orderBy(order);
        List<Cemetery> list = cemeteryMapper.getCemeteryList(paramMap);
        return list;
    }

    /**
     * 根据条件获取陵园信息PageInfo
     *
     * @param paramMap 条件
     * @param pageNum  当前页数
     * @param pageSize 每页个数
     * @param order    排序
     * @return
     */
    public PageInfo<Cemetery> getCemeteryPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
        PageHelper.startPage(pageNum, pageSize);
        PageHelper.orderBy(order);
        List<Cemetery> list = cemeteryMapper.getCemeteryList(paramMap);
        PageInfo<Cemetery> page = new PageInfo<Cemetery>(list);
        return page;
    }


}
