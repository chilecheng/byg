package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.Cemetery;
import com.hz.entity.cemetery.CemeteryRow;
import com.hz.entity.cemetery.CemeteryRowAndTomb;
import com.hz.mapper.cemetery.CemeteryMapper;
import com.hz.mapper.cemetery.CemeteryRowMapper;
/**
 * 陵园排信息服务层
 * @author jgj
 *
 */
@Service
@Transactional
public class CemeteryRowService {
	@Autowired
	private CemeteryRowMapper cemeteryRowMapper;
	
	/**
	 * 添加陵园排信息
	 * @param cemeteryRow
	 */
	public void saveCemeteryRow(CemeteryRow cemeteryRow){
		cemeteryRowMapper.saveCemeteryRow(cemeteryRow);
	}
	/**
	 * 删除陵园排信息
	 * @param id
	 */
	public void deleteCemeteryRow(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	cemeteryRowMapper.deleteCemeteryRow(ids[i]);
	    }
	}
	/**
	 * 更新陵园排信息
	 * @param cemeteryRow
	 */
	public void updateCemeteryRow(CemeteryRow cemeteryRow){
		cemeteryRowMapper.updateCemeteryRow(cemeteryRow);
	}
	/**
	 * 获取陵园排信息对象
	 * @param id
	 * @return
	 */
	public CemeteryRow getCemeteryRowById(String id){
		CemeteryRow cemeteryRow=cemeteryRowMapper.getCemeteryRowById(id);
		return cemeteryRow;
	}
	
	/**
	 * 根据条件获取陵园排信息
	 * @param String name
	 * @param byte isdel
	 */
	public List<CemeteryRow> getCemeteryRowList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<CemeteryRow> list= cemeteryRowMapper.getCemeteryRowList(paramMap);
		return list;
	}
	/**
	 * 根据条件获取 公墓   排与墓穴信息 List
	 * @param maps
	 * @param order
	 * @return
	 */
	public List<CemeteryRowAndTomb> getRowAndTombList(Map<String,Object> maps,String order){
		PageHelper.orderBy(order);
		List<CemeteryRowAndTomb> list=cemeteryRowMapper.getRowAndTombList(maps);
		return list;
	}
	/**
	 * 根据条件获取陵园排信息PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<CemeteryRow> getCemeteryRowPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<CemeteryRow> list=cemeteryRowMapper.getCemeteryRowList(paramMap);
		PageInfo<CemeteryRow> page = new PageInfo<CemeteryRow>(list);
		return page; 
	}
	

}
