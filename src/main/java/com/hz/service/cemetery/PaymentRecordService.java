package com.hz.service.cemetery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hz.util.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.PaymentRecord;
import com.hz.mapper.cemetery.PaymentRecordMapper;
/**
 * 墓穴支付记录信息 服务层
 * @author jgj
 *
 */
@Service
@Transactional
public class PaymentRecordService {
	@Autowired
	private PaymentRecordMapper paymentRecordMapper;
	/**
	 * 添加墓穴支付记录
	 * @param paymentRecord
	 */
	public void savePaymentRecord(PaymentRecord paymentRecord){
		paymentRecordMapper.savePaymentRecord(paymentRecord);
	}
	/**
	 * 删除墓穴支付记录
	 * @param id
	 */
	public void deletePaymentRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	paymentRecordMapper.deletePaymentRecord(ids[i]);
	    }
	}
	/**
	 * 更新墓穴支付记录
	 * @param paymentRecord
	 */
	public void updatePaymentRecord(PaymentRecord paymentRecord){
		paymentRecordMapper.updatePaymentRecord(paymentRecord);
	}
	/**
	 * 获取墓穴支付记录 根据 Id
	 * @param id
	 * @return
	 */
	public PaymentRecord getPaymentRecordById(String id){
		PaymentRecord paymentRecord=paymentRecordMapper.getPaymentRecordById(id);
		return paymentRecord;
	}
	/**
	 * 获取墓穴缴费记录
	 * @param cemetery
	 * @return
	 */
	public List<PaymentRecord> getCemeteryrecord(){
		paymentRecordMapper.getCemeteryrecord();
		List list= new ArrayList<>();
		return list;
	}
	/**
	 * 获取墓穴维护缴费记录
	 * @param cemetery
	 * @return
	 */
	public List getMaintenancePayment(){
		paymentRecordMapper.getMaintenancePayment();
		List list= new ArrayList<>();
		return list;
	}
	/**
	 * 根据条件获取墓穴支付记录List
	 * @param String name
	 *
	 */
	public List<PaymentRecord> getPaymentRecordList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<PaymentRecord> list= paymentRecordMapper.getPaymentRecordList(paramMap);
		return list;
	}

	/**
	 * 根据条件获取墓穴支付记录PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<PaymentRecord> getPaymentRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<PaymentRecord> list=paymentRecordMapper.getPaymentRecordList(paramMap);
		PageInfo<PaymentRecord> page = new PageInfo<PaymentRecord>(list);
		return page;
	}

	/**
	 * 修改 墓穴的支付记录 如果没有支付记录就插入一条新的记录
	 *
	 * @param pr
	 * @param buyRecordId
	 * @return
	 */
	public void saveTombPaymentRecord(PaymentRecord pr,String buyRecordId) {
		// TODO Auto-generated method stub
        PaymentRecord paymentRecord = paymentRecordMapper.getPaymentRecordByTombId(buyRecordId);
        if (paymentRecord != null) {
            pr.setId(paymentRecord.getId());
            paymentRecordMapper.updatePaymentRecord(pr);
        } else {
            pr.setId(UuidUtil.get32UUID());
            paymentRecordMapper.savePaymentRecord(pr);
        }
	}

}
