package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.RepairRecord;
import com.hz.mapper.cemetery.RepairRecordMapper;
/**
 * ĹѨά�޼�¼ �����
 * @author jgj
 *
 */
@Service
@Transactional
public class RepairRecordService {
	@Autowired
	private RepairRecordMapper repairRecordMapper;
	/**
	 * ����ĹѨά�޼�¼
	 * @param repairRecord
	 */
	public void saveRepairRecord(RepairRecord repairRecord){
		repairRecordMapper.saveRepairRecord(repairRecord);
	}
	/**
	 * ɾ��ĹѨά�޼�¼
	 * @param id
	 */
	public void deleteRepairRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	repairRecordMapper.deleteRepairRecord(ids[i]);
	    }
	}
	/**
	 * ����ĹѨά�޼�¼
	 * @param repairRecord
	 */
	public void updateRepairRecord(RepairRecord repairRecord){
		repairRecordMapper.updateRepairRecord(repairRecord);
	}
	/**
	 * ��ȡĹѨά�޼�¼ ���� Id
	 * @param id
	 * @return
	 */
	public RepairRecord getRepairRecordById(String id){
		RepairRecord transferRecord=repairRecordMapper.getRepairRecordById(id);
		return transferRecord;
	}
	/**
	 * ����ĹѨID��ά��״̬��δά�ޣ���ѯ��ά�޼�¼
	 * @param tombId
	 * @param repairType
	 * @return
	 */
	public RepairRecord getRepairRecordByTombIdAndType(String tombId,byte repairType){
		RepairRecord repairRecord=repairRecordMapper.getRepairRecordByTombIdAndType(tombId,repairType);
		return repairRecord;
	}
	/**
	 * ����������ȡĹѨά�޼�¼List
	 * @param String name
	 * 
	 */
	public List<RepairRecord> getRepairRecordList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<RepairRecord> list= repairRecordMapper.getRepairRecordList(paramMap);
		return list;
	}
	
	/**
	 * ����������ȡĹѨά�޼�¼PageInfo
	 * @param paramMap	����
	 * @param pageNum	��ǰҳ��
	 * @param pageSize	ÿҳ����
	 * @param order		����
	 * @return
	 */
	public PageInfo<RepairRecord> getRepairRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<RepairRecord> list=repairRecordMapper.getRepairRecordList(paramMap);
		PageInfo<RepairRecord> page = new PageInfo<RepairRecord>(list);
		return page; 
	}
	

}
