package com.hz.service.cemetery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.UserRecord;
import com.hz.mapper.cemetery.UserRecordMapper;
import com.hz.util.DataTools;

/**
 * 墓穴使用者记录信息 服务层
 *
 * @author jgj
 */
@Service
@Transactional
public class UserRecordService {
    @Autowired
    private UserRecordMapper userRecordMapper;

    /**
     * 添加墓穴使用者记录
     *
     * @param userRecord
     */
    public void saveUserRecord(UserRecord userRecord) {
        userRecordMapper.saveUserRecord(userRecord);
    }

    /**
     * 删除墓穴使用者记录
     *
     * @param id
     */
    public void deleteUserRecord(String id) {
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            userRecordMapper.deleteUserRecord(ids[i]);
        }
    }

    /**
     * 更新墓穴使用者记录
     *
     * @param userRecord
     */
    public void updateUserRecord(UserRecord userRecord) {
        userRecordMapper.updateUserRecord(userRecord);
    }

    /**
     * 获取墓穴使用者记录 根据 Id
     *
     * @param id
     * @return
     */
    public UserRecord getUserRecordById(String id) {
        UserRecord userRecord = userRecordMapper.getUserRecordById(id);
        return userRecord;
    }

    /**
     * 根据条件获取墓穴使用者记录List
     *
     * @param paramMap
     * @param order
     */
    public List<UserRecord> getUserRecordList(Map<String, Object> paramMap, String order) {
        PageHelper.orderBy(order);
        List<UserRecord> list = userRecordMapper.getUserRecordList(paramMap);
        return list;
    }

    /**
     * 根据墓穴ID和是否迁移来查询出墓穴使用者列表
     *
     * @param checkId    默认选中
     * @param tombId     墓穴Id
     * @param isTransfer 是否迁出
     * @return
     */
    public String getUserNameOption(String checkId, String tombId, byte isTransfer) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tombId", tombId);
//        map.put("isTransfer", isTransfer);
        List<UserRecord> userList = userRecordMapper.getUserRecordList(map);
        List<Object[]> list = new ArrayList<Object[]>();
        for (int i = 0; i < userList.size(); i++) {
            list.add(new Object[]{userList.get(i).getId(), userList.get(i).getUserName()});
        }
        String option = DataTools.getOptionByList(list, checkId, false);
        return option;
    }

    /**
     * 根据条件获取墓穴使用者记录PageInfo
     *
     * @param paramMap 条件
     * @param pageNum  当前页数
     * @param pageSize 每页个数
     * @param order    排序
     * @return
     */
    public PageInfo<UserRecord> getUserRecordPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
        PageHelper.startPage(pageNum, pageSize);
        PageHelper.orderBy(order);
        List<UserRecord> list = userRecordMapper.getUserRecordList(paramMap);
        PageInfo<UserRecord> page = new PageInfo<UserRecord>(list);
        return page;
    }


}
