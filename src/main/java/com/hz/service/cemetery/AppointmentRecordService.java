package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.AppointmentRecord;
import com.hz.mapper.cemetery.AppointmentRecordMapper;
/**
 * 墓穴预约记录 服务层
 * @author jgj
 *
 */
@Service
@Transactional
public class AppointmentRecordService {
	@Autowired
	private AppointmentRecordMapper appointmentRecordMapper;
	/**
	 * 添加墓穴预约记录
	 * @param appointmentRecord
	 */
	public void saveAppointmentRecord(AppointmentRecord appointmentRecord){
		appointmentRecordMapper.saveAppointmentRecord(appointmentRecord);
	}
	/**
	 * 删除墓穴预约记录
	 * @param id
	 */
	public void deleteAppointmentRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	appointmentRecordMapper.deleteAppointmentRecord(ids[i]);
	    }
	}
	/**
	 * 更新墓穴预约记录
	 * @param appointmentRecord
	 */
	public void updateAppointmentRecord(AppointmentRecord appointmentRecord){
		appointmentRecordMapper.updateAppointmentRecord(appointmentRecord);
	}
	/**
	 * 获取墓穴预约记录 根据 Id
	 * @param id
	 * @return
	 */
	public AppointmentRecord getAppointmentRecordById(String id){
		AppointmentRecord appointmentRecord=appointmentRecordMapper.getAppointmentRecordById(id);
		return appointmentRecord;
	}
	
	/**
	 * 根据条件获取墓穴预约记录List
	 * @param String name
	 * 
	 */
	public List<AppointmentRecord> getAppointmentRecordList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<AppointmentRecord> list= appointmentRecordMapper.getAppointmentRecordList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取墓穴预约记录PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<AppointmentRecord> getAppointmentRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<AppointmentRecord> list=appointmentRecordMapper.getAppointmentRecordList(paramMap);
		PageInfo<AppointmentRecord> page = new PageInfo<AppointmentRecord>(list);
		return page; 
	}

	/**
	 * 根据条件获取全部墓穴预约记录PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<AppointmentRecord> getAllAppointmentRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<AppointmentRecord> list=appointmentRecordMapper.getAllAppointmentRecordList(paramMap);
		PageInfo<AppointmentRecord> page = new PageInfo<AppointmentRecord>(list);
		return page;
	}

	/**
	 * 删除 预约 对应的 预约记录
	 * @param tid
	 */
	public void escYuding(String tid) {
		appointmentRecordMapper.updateAppointmentStatus(tid);
	}
	

}
