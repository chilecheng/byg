package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.MaintainRecord;
import com.hz.mapper.cemetery.MaintainRecordMapper;
/**
 * 墓穴维护记录信息 服务层
 * @author jgj
 *
 */
@Service
@Transactional
public class MaintainRecordService {
	@Autowired
	private MaintainRecordMapper maintainRecordMapper;
	/**
	 * 添加墓穴维护记录
	 * @param maintainRecord
	 */
	public void saveMaintainRecord(MaintainRecord maintainRecord){
		maintainRecordMapper.saveMaintainRecord(maintainRecord);
	}
	/**
	 * 删除墓穴维护记录
	 * @param id
	 */
	public void deleteMaintainRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	maintainRecordMapper.deleteMaintainRecord(ids[i]);
	    }
	}
	/**
	 * 更新墓穴维护记录
	 * @param maintainRecord
	 */
	public void updateMaintainRecord(MaintainRecord maintainRecord){
		maintainRecordMapper.updateMaintainRecord(maintainRecord);
	}
	/**
	 * 获取墓穴维护记录 根据 Id
	 * @param id
	 * @return
	 */
	public MaintainRecord getMaintainRecordById(String id){
		MaintainRecord maintainRecord=maintainRecordMapper.getMaintainRecordById(id);
		return maintainRecord;
	}
	
	/**
	 * 根据条件获取墓穴维护记录List
	 * @param String name
	 * 
	 */
	public List<MaintainRecord> getMaintainRecordList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<MaintainRecord> list= maintainRecordMapper.getMaintainRecordList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取墓穴维护记录PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<MaintainRecord> getMaintainRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<MaintainRecord> list=maintainRecordMapper.getMaintainRecordList(paramMap);
		PageInfo<MaintainRecord> page = new PageInfo<MaintainRecord>(list);
		return page; 
	}
}
