package com.hz.service.cemetery;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.CemeteryBack;
import com.hz.mapper.cemetery.CemeteryBackMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 已退墓穴记录 服务层
 * @Classname CemeteryBackService
 * @Description TODO
 * @Date 2019/6/27 19:42
 * @Created by ZhixiangWang
 */
@Service
@Transactional
public class CemeteryBackService {
    @Autowired
    private CemeteryBackMapper cemeteryBackMapper;

    /**
     * 添加已退墓穴记录
     *
     * @param cemeteryBack
     */
    public void saveCemeteryBack(CemeteryBack cemeteryBack) {
        cemeteryBackMapper.saveCemeteryBack(cemeteryBack);
    }

    /**
     * 删除已退墓穴记录
     *
     * @param id
     */
    public void deleteCemeteryBack(String id) {
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            cemeteryBackMapper.deleteCemeteryBack(ids[i]);
        }
    }

    /**
     * 更新已退墓穴记录
     *
     * @param cemeteryBack
     */
    public void updateCemeteryBack(CemeteryBack cemeteryBack) {
        cemeteryBackMapper.updateCemeteryBack(cemeteryBack);
    }

    /**
     * 获取已退墓穴记录 根据 Id
     *
     * @param id
     * @return
     */
    public CemeteryBack getCemeteryBackById(String id) {
        CemeteryBack cemeteryBack = cemeteryBackMapper.getCemeteryBackById(id);
        return cemeteryBack;
    }

    /**
     * 根据条件获取已退墓穴记录List
     *
     * @param String name
     */
    public List<CemeteryBack> getCemeteryBackList(Map<String, Object> paramMap, String order) {
        PageHelper.orderBy(order);
        List<CemeteryBack> list = cemeteryBackMapper.getCemeteryBackList(paramMap);
        return list;
    }

    /**
     * 根据条件获取已退墓穴记录PageInfo
     *
     * @param paramMap 条件
     * @param pageNum  当前页数
     * @param pageSize 每页个数
     * @param order    排序
     * @return
     */
    public PageInfo<CemeteryBack> getCemeteryBackPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
        PageHelper.startPage(pageNum, pageSize);
        PageHelper.orderBy(order);
        List<CemeteryBack> list = cemeteryBackMapper.getCemeteryBackList(paramMap);
        PageInfo<CemeteryBack> page = new PageInfo<CemeteryBack>(list);
        return page;
    }
}
