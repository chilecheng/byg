package com.hz.service.cemetery;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.cemetery.TransferRecord;
import com.hz.mapper.cemetery.TransferRecordMapper;
/**
 * 墓穴迁出记录 服务层
 * @author jgj
 *
 */
@Service
@Transactional
public class TransferRecordService {
	@Autowired
	private TransferRecordMapper transferRecordMapper;
	/**
	 * 添加墓穴迁出记录
	 * @param transferRecord
	 */
	public void saveTransferRecord(TransferRecord transferRecord){
		transferRecordMapper.saveTransferRecord(transferRecord);
	}
	/**
	 * 删除墓穴迁出记录
	 * @param id
	 */
	public void deleteTransferRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	transferRecordMapper.deleteTransferRecord(ids[i]);
	    }
	}
	/**
	 * 更新墓穴迁出记录
	 * @param transferRecord
	 */
	public void updateTransferRecord(TransferRecord transferRecord){
		transferRecordMapper.updateTransferRecord(transferRecord);
	}
	/**
	 * 获取墓穴迁出记录 根据 Id
	 * @param id
	 * @return
	 */
	public TransferRecord getTransferRecordById(String id){
		TransferRecord transferRecord=transferRecordMapper.getTransferRecordById(id);
		return transferRecord;
	}
	
	/**
	 * 根据条件获取墓穴迁出记录List
	 * @param String name
	 * 
	 */
	public List<TransferRecord> getTransferRecordList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<TransferRecord> list= transferRecordMapper.getTransferRecordList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取墓穴迁出记录PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<TransferRecord> getTransferRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<TransferRecord> list=transferRecordMapper.getTransferRecordList(paramMap);
		PageInfo<TransferRecord> page = new PageInfo<TransferRecord>(list);
		return page; 
	}
	

}
