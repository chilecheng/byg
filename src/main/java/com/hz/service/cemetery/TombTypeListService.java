package com.hz.service.cemetery;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.cemetery.AppointmentRecord;
import com.hz.entity.cemetery.BuyRecord;
import com.hz.entity.cemetery.MaintainRecord;
import com.hz.entity.cemetery.PaymentRecord;
import com.hz.entity.cemetery.RepairRecord;
import com.hz.entity.cemetery.Tomb;
import com.hz.entity.cemetery.TransferRecord;
import com.hz.entity.cemetery.UserRecord;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 墓穴购买记录信息 服务层
 *
 * @author jgj
 */
@Service
@Transactional
public class TombTypeListService {
    @Autowired
    private UserRecordService userRecordService;
    @Autowired
    private BuyRecordService buyRecordService;
    @Autowired
    private MaintainRecordService maintainRecordService;
    @Autowired
    private PaymentRecordService paymentRecordService;
    @Autowired
    private CemeteryTombService cemeteryTombService;
    @Autowired
    private AppointmentRecordService appointmentRecordService;
    @Autowired
    private RepairRecordService repairRecordService;
    @Autowired
    private TransferRecordService transferRecordService;

    /**
     * 保存购买记录，并更改相应记录
     *
     * @return
     */
    public void saveAllSaleRecord(String[] userName, String[] sex, String[] deadType,
                                  String[] borthTime, String[] deadTime, String[] releaseTime,
                                  String[] longMenTime, String buyId, BuyRecord buyRecord, PaymentRecord paymentRecord,
                                  String[] beginTime, String[] endTime, String[] longTime, String[] payName,
                                  String[] payTime, String[] payMoney, String[] invoiceNumber, String tombId) {
        //保存墓穴购买基本信息
        buyRecordService.saveBuyRecord(buyRecord);
        //保存墓穴购买支付信息
        paymentRecordService.savePaymentRecord(paymentRecord);
        //保存墓穴购买使用者信息
        int sum = 0;
        if (userName != null && userName.length > 0) {
            for (int i = 0; i < userName.length; i++) {
                UserRecord userRecord = new UserRecord();
                userRecord.setId(UuidUtil.get32UUID());
                userRecord.setBorthTime(DateTools.stringToSqlDate(borthTime[sum], "yyyy-MM-dd"));
                userRecord.setDeadTime(DateTools.stringToSqlDate(deadTime[sum], "yyyy-MM-dd"));
                userRecord.setLongmenTime(DateTools.stringToSqlDate(longMenTime[sum], "yyyy-MM-dd"));
                userRecord.setReleaseTime(DateTools.stringToSqlDate(releaseTime[sum], "yyyy-MM-dd"));
                userRecord.setBuyRecordId(buyId);
                userRecord.setDeadType(DataTools.stringToByte(deadType[sum]));
                userRecord.setTombId(tombId);
                userRecord.setSex(DataTools.stringToByte(sex[sum]));
                userRecord.setUserName(userName[sum]);
                userRecord.setIsTransfer(Const.Is_No);
                userRecordService.saveUserRecord(userRecord);
                sum++;
            }
        }
        //保存墓穴购买维护信息
        int num = 0;
        if (payName != null && payName.length > 0) {
            for (int i = 0; i < payName.length; i++) {
                MaintainRecord maintainRecord = new MaintainRecord();
                maintainRecord.setId(UuidUtil.get32UUID());
                maintainRecord.setRecordId(buyId);
                maintainRecord.setTombId(tombId);
                maintainRecord.setBeginTime(DateTools.stringToSqlDate(beginTime[num], "yyyy-MM-dd"));
                maintainRecord.setEndTime(DateTools.stringToSqlDate(endTime[num], "yyyy-MM-dd"));
                maintainRecord.setPayTime(DateTools.stringToSqlDate(payTime[num], "yyyy-MM-dd"));
                maintainRecord.setLongTime(DataTools.stringToInt(longTime[num]));
                maintainRecord.setPayName(payName[num]);
                maintainRecord.setInvoiceNumber(invoiceNumber[num]);
                maintainRecord.setPayMoney(DataTools.stringToDouble(payMoney[num]));
                maintainRecordService.saveMaintainRecord(maintainRecord);
                num++;
            }
        }
        //保存之后，应更改墓穴的状态为已售
        Tomb tomb = cemeteryTombService.getCemeteryTombById(tombId);
        tomb.setState(Const.GM_SOU);
        cemeteryTombService.updateCemeteryTomb(tomb);
        appointmentRecordService.escYuding(tombId);
    }

    /**
     * 保存预约记录，并更改墓穴状态
     *
     * @param appointmentRecord
     * @param tombId
     */
    public void saveYudingRecord(AppointmentRecord appointmentRecord, String tombId) {
        appointmentRecordService.saveAppointmentRecord(appointmentRecord);
        Tomb tomb = cemeteryTombService.getCemeteryTombById(tombId);
        tomb.setState(Const.GM_YUDING);
        cemeteryTombService.updateCemeteryTomb(tomb);
    }

    /**
     * 取消墓穴预约
     *
     * @param tid
     */
    public void escYuding(String tid) {
        //修改 预约表 预约记录
        appointmentRecordService.escYuding(tid);
        //获取墓穴对象信息  更新对象为 未预定
        Tomb tomb = cemeteryTombService.getCemeteryTombById(tid);
        tomb.setState(Const.GM_KONG);
        cemeteryTombService.updateCemeteryTomb(tomb);
    }


    /**
     * 保存迁出记录，并更改墓穴状态
     *
     * @param transferRecord
     * @param tombId
     */
    public void saveTransferRecord(String userId, TransferRecord transferRecord, String tombId) {
        UserRecord userRecord = userRecordService.getUserRecordById(userId);
        userRecord.setIsTransfer(Const.Is_Yes);
        userRecordService.updateUserRecord(userRecord);
        String userName = userRecord.getUserName();
        transferRecord.setUserName(userName);
        transferRecordService.saveTransferRecord(transferRecord);
        Tomb tomb = cemeteryTombService.getCemeteryTombById(tombId);
        tomb.setState(Const.GM_QIAN);
        cemeteryTombService.updateCemeteryTomb(tomb);
    }

    /**
     * 保存墓穴维修记录，并更改墓穴状态
     *
     * @param repairRecord
     * @param tombId
     */
    public void saveRepairRecord(RepairRecord repairRecord, String tombId) {
        repairRecordService.saveRepairRecord(repairRecord);
        Tomb tomb = cemeteryTombService.getCemeteryTombById(tombId);
        tomb.setState(Const.GM_WEI);
        cemeteryTombService.updateCemeteryTomb(tomb);
    }

    /**
     * 更新墓穴维修记录，并更改墓穴状态为空白
     *
     * @param repairRecord
     * @param tombId
     */
    public void saveCompleteRepair(RepairRecord repairRecord, String tombId) {
        repairRecordService.updateRepairRecord(repairRecord);
        Tomb tomb = cemeteryTombService.getCemeteryTombById(tombId);
        tomb.setState(Const.GM_KONG);
        cemeteryTombService.updateCemeteryTomb(tomb);
    }


}
