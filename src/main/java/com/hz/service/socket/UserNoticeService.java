package com.hz.service.socket;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.socket.Notice;
import com.hz.entity.socket.UserNotice;
import com.hz.mapper.socket.UserNoticeMapper;
import com.hz.util.UuidUtil;

/**
 * @author cjb
 *
 */
@Service
@Transactional
public class UserNoticeService {
	@Autowired
	UserNoticeMapper userNoticeMapper;
	
	public void saveUserNotice(UserNotice un) {
		userNoticeMapper.saveUserNotice(un);
	}
	public int deleteUserNotice(String userId) {
		return userNoticeMapper.deleteUserNotice(userId);
	}
	/**
	 * 点击查看后当天number清零
	 */
	public int updateUserNoticeByUser(String userId,String noticeType, java.sql.Date date) {
		return userNoticeMapper.updateUserNoticeByUser(userId, noticeType, date);
	}
	/**
	 * 对某科的人员  notice 库统一
	 * 加1 或 减1
	 */
	public int updateUserNoticeByNoticeType(String noticeType, java.sql.Date date, int i) {
		return userNoticeMapper.updateUserNoticeByNoticeType(noticeType, date, i);
	}
	/**
	 * 登录时获取通知数量
	 */
	public int getCount(String userId,String noticeType, java.sql.Date date) {
		if (userNoticeMapper.getCount(userId,noticeType,date) == null) {
			return 0;
		} else {
			return Integer.parseInt(userNoticeMapper.getCount(userId,noticeType,date));
		}
	}
	//政法验尸  解冻
	/*public int updateUserNoticeByNoticeTypeNumber(int number, String noticeType) {
		return userNoticeMapper.updateUserNoticeByNoticeTypeNumber(number,noticeType);
	}*/
	/**
	 * 查看是否存在所需通知的日期
	 * @return true:存在
	 */
	public boolean ifDateExit(String noticeType,java.sql.Date date) {
		if (userNoticeMapper.ifDateExit(noticeType, date) > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 插入某模块某天的数据
	 */
	public void addNoticeOfDate(String noticeType, java.sql.Date date) {
		List<String> list =  userNoticeMapper.getSocketIdList(noticeType);
		for (String socketUserId : list) {
			userNoticeMapper.insertSocketNumber(UuidUtil.get32UUID(),socketUserId,date,1);
		}
	}
	/**
	 * 获取变更日当天的  目标科室的所有用户的 的读取状态（socker——number的 number）
	 */
	public   List<Notice> getAllNumberByDep(String noticeType ,java.sql.Date date){
		return userNoticeMapper.getAllDayNotice(noticeType, date);
	}
/**
 * 根据用户名Iid   所在科室 日期三项 对notice 表中的number 数目进行更改
 * @param userId
 * @param notice_type
 * @param notice_date
 */
	public void updateUserNoticeByUserId(String userId,String notice_type,java.sql.Date notice_date){
		userNoticeMapper.updateUserNoticeByUserId(userId, notice_type, notice_date);
	}

}

