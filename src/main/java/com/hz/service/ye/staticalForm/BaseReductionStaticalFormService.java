package com.hz.service.ye.staticalForm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.ye.staticalForm.BaseReductionStaticalForm;
import com.hz.mapper.ye.BaseReductionStaticalFormMapper;

@Service
@Transactional
public class BaseReductionStaticalFormService {
	@Autowired
	private BaseReductionStaticalFormMapper brsfm;
	
	/**
	 * 获取基本殡葬减免列表
	 * @param paramMap
	 * @return
	 */
	public List<BaseReductionStaticalForm> findBaseReductionSF(Map<String,Object> paramMap){
		List<BaseReductionStaticalForm> list = brsfm.findBaseReductionSF(paramMap);
		return list;
	}
	
	/**
	 * 获取基本殡葬减免列表页
	 * @param paramMap
	 * @return
	 */
	public List<BaseReductionStaticalForm> findBaseRSFPageInfo(Map<String,Object> paramMap){
		List<BaseReductionStaticalForm> list = brsfm.findBaseReductionSF(paramMap);
		return list;
	}
}
