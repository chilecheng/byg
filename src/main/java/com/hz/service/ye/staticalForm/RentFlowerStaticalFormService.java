package com.hz.service.ye.staticalForm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.base.Item;
import com.hz.entity.ye.staticalForm.RentFlowerStaticalForm;
import com.hz.mapper.ye.RentFlowerStaticalFormMapper;

@Service
@Transactional
public class RentFlowerStaticalFormService {
	@Autowired
	private RentFlowerStaticalFormMapper rfsfm;
	/**
	 * 获取租售花圈花篮列表
	 * @param paramMap
	 * @return
	 */
	public List<RentFlowerStaticalForm> findRentFlowerSF(Map<String,Object> paramMap){
		List<RentFlowerStaticalForm> list = rfsfm.findRentFlowerSF(paramMap);
		return list;
	}
	/**
	 * 项目列表
	 * @param paramMap
	 * @return
	 */
	public List<Item> findItemNameList(Map<String,Object> paramMap){
		List<Item> list = rfsfm.findItemNameList(paramMap);
		return list;
	}
	/**
	 * 统计数量
	 * @param paraMap
	 * @return
	 */
	public int getItemNameIdCount(Map<String,Object> paraMap){
		return rfsfm.getItemNameIdCount(paraMap);
	}
}
