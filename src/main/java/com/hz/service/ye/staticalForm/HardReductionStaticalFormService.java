package com.hz.service.ye.staticalForm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.ye.staticalForm.HardReductionStaticalForm;
import com.hz.mapper.ye.HardReductionStaticalFormMapper;

@Service
@Transactional
public class HardReductionStaticalFormService {
	@Autowired
	private HardReductionStaticalFormMapper hrsfm;
	
	/**
	 * 获取困难殡葬减免列表
	 * @param paramMap
	 * @return
	 */
	public List<HardReductionStaticalForm> findHardReductionSF(Map<String,Object> paramMap){
		List<HardReductionStaticalForm> list = hrsfm.findHardReductionSF(paramMap);
		return list;
	}
	
	/**
	 * 获取困难殡葬减免列表页
	 * @param paramMap
	 * @return
	 */
	public List<HardReductionStaticalForm> findHardRSFPageInfo(Map<String,Object> paramMap){
		List<HardReductionStaticalForm> list = hrsfm.findHardReductionSF(paramMap);
		return list;
	}
	
}
