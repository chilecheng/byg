package com.hz.service.ye.staticalForm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.PincardRecord;
import com.hz.mapper.ye.staticalForm.CloseCardRecordFormMapper;

/**
 * @author rgy
 * 
 */
@Service
@Transactional
public class CloseCardRecordService {
	@Autowired
	private CloseCardRecordFormMapper ccrf;
	
	/**
	 * 销卡记录列表
	 * @param map
	 * @return
	 */
	public List<PincardRecord> getCloseCardRecord(Map<String, Object> map) {
		List<PincardRecord> list=ccrf.getCloseCardRecordList(map);
		return list;
	}
	
	/**
	 * 根据条件获取pageInfo
	 * @param map
	 * @param pageSize
	 * @param pageNum
	 * @param order
	 * @return
	 */
	public PageInfo<PincardRecord> getCloseCardRecordPageInfo(Map<String, Object> map,int pageSize,int pageNum){
		PageHelper.startPage(pageSize, pageNum);
		List<PincardRecord> list=getCloseCardRecord(map);
		PageInfo<PincardRecord> page=new PageInfo<PincardRecord>(list);
		return page;
	}
	
}
