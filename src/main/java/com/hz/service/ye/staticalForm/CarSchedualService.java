package com.hz.service.ye.staticalForm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.ye.CarSchedulRecord;
import com.hz.mapper.ye.staticalForm.CarSchedualFormMapper;

@Service
@Transactional
public class CarSchedualService {
	@Autowired
	private CarSchedualFormMapper carListMapper;

	/**
	 * 获取车辆列表页
	 * @param paramMap
	 * @return
	 */
	public List<CarSchedulRecord> getCarList(Map<String,Object> paramMap) {
		List<CarSchedulRecord> list = carListMapper.getCarList(paramMap);
		return list;
	}

}
