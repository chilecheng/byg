package com.hz.service.ye.staticalForm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.hz.entity.base.ProveUnit;
import com.hz.mapper.ye.DeadProveUnitFormMapper;

/**
 * @author mbz
 *
 */
@Service
@Transactional
public class DeadProveUnitService {
	@Autowired
	private DeadProveUnitFormMapper dpufm;

	public List<ProveUnit> getDeadProveUnitList(Map<String, Object> map) {
		return dpufm.getDeadProveUnitList(map);
	}
	
	/**
	 * ͳ������
	 * @param map
	 * @return
	 */
	public int getDeadProveUnitIdCount(Map<String, Object> map) {
		return dpufm.getDeadProveUnitIdCount(map);
	}
}
