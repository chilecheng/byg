package com.hz.service.ye.staticalForm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.hz.entity.base.Toponym;
import com.hz.mapper.ye.AreaFuneralStaticalFormMapper;

@Service
@Transactional
public class AreaFuneralStaticalFormService {
	@Autowired
	private AreaFuneralStaticalFormMapper afsfm;
	
	/**
	 * 统计数量
	 * @param map
	 * @return
	 */
	public Toponym getToponymIdCount(Map<String, Object> map) {
		return afsfm.getToponymIdCount(map);
	}
	
	/**
	 * 查询地区列表
	 * @param paramap
	 * @param string
	 * @return
	 */
	public List<Toponym> getToponymList(Map<String, Object> paramap,
			String string) {
		PageHelper.orderBy(string);
		List<Toponym> list = afsfm.getToponymList(paramap);
		return  list;
	}
}
