package com.hz.service.ye.staticalForm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.staticalForm.ChargeStaticForm;
import com.hz.mapper.ye.staticalForm.ChargeStaticFormMapper;


@Service
@Transactional
public class ChargeStaticService {
	
	@Autowired
	private ChargeStaticFormMapper chargeStaticFormMapper;
	
	public List<ChargeStaticForm>  getchargeStaticList(Map<String,Object> paramMap){
		List<ChargeStaticForm> list  = chargeStaticFormMapper.getchargeStaticList(paramMap);
		return list;
	}
	
	/**
	 * h获取骨灰寄存  表 list
	 * @param paramMap
	 * @return
	 */
	
	public List<ChargeStaticForm>  getashchargeStaticList(Map<String,Object> paramMap){
		return chargeStaticFormMapper.getashchargeStaticList(paramMap);
	}
	/**
	 * 
	 * 统计项目中合计  求取 总值
	 */
	
public ChargeStaticForm SumChargeStatic(Map<String,Object> paramMap){	
		return chargeStaticFormMapper.sumStaticChargeRecord(paramMap);
	}
	
	

	/**
	 * 获取冷藏柜记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<ChargeStaticForm> getdelegateRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<ChargeStaticForm> list1 = getchargeStaticList(paramMap);
		List<ChargeStaticForm> list= new ArrayList<ChargeStaticForm>();
		
//		Object[] ob=new Object[list1.size()];
		int j=0;
	
		
		
		for(int i=0;i<list1.size();i++){
//			s = list1.get(i);
			if(list1.get(i) == null){
			/*	ob[j]=list1.get(i);
				j++;*/
				list1.remove(i);
				/*list.add
				list.add(list1.get(i));*/
			}
			
		}
//		list.addAll(ob);
		PageInfo<ChargeStaticForm> page = new PageInfo<ChargeStaticForm>(list1);
		return page;	
	}
	public PageInfo<ChargeStaticForm> getdelegateRecordPageInfo2(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<ChargeStaticForm> list2 = getashchargeStaticList(paramMap);
		PageInfo<ChargeStaticForm> page = new PageInfo<ChargeStaticForm>(list2);
		return page;	
	}
}
