package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.system.Role;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.util.Const;
/**
 * @author cjb
 *
 */
@Service
@Transactional
public class FarewellRecordService {
	@Autowired
	private FarewellRecordMapper farewellRecordMapper;
	@Autowired
	private ItemOrderService itemOrderService;
	/**
	 * 添加告别厅记录
	 * @param farewellRecord
	 */
	public void addFarewellRecord(FarewellRecord farewellRecord){
		farewellRecordMapper.saveFarewellRecord(farewellRecord);
	}
	public void addFarewellRecord2(FarewellRecord farewellRecord){
		farewellRecordMapper.saveFarewellRecord2(farewellRecord);
	}
	/**
	 * 解除锁定
	 * @param userId
	 * @param flag
	 * @param commissionOrderId
	 */
	public void deleteFarewellRecordByLock(String userId,byte flag,String commissionOrderId){
		farewellRecordMapper.deleteFarewellRecordByLock(userId, flag, commissionOrderId);
	}
	/**
	 * 删除告别厅记录
	 * @param id
	 */
	public void deleteFarewellRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			farewellRecordMapper.deleteFarewellRecord(id);
		}
	}
	/**
	 * 修改告别厅记录
	 * @param farewellRecord
	 */
	public void updateFarewellRecord(FarewellRecord farewellRecord){
		farewellRecordMapper.updateFarewellRecord(farewellRecord);;
	}
	/**
	 * 获取告别厅记录对象
	 * @param id
	 * @return
	 */
	public FarewellRecord getFarewellRecordId(String id){
		return farewellRecordMapper.getFarewellRecordById(id);
	}
	/**
	 * 根据业务单ID获取告别厅信息
	 * @param id
	 * @return
	 */
	public FarewellRecord getFarewellRecordByOrderId(String id){
		return farewellRecordMapper.getFarewellRecordByOrderId(id);
	}
	/**
	 * 根据条件获取告别厅记录list
	 * @param paramMap
	 * @return
	 */
	public List<FarewellRecord> getFarewellRecordList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return farewellRecordMapper.getFarewellRecordList(paramMap);
	}
	/**
	 * 获取告别厅记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<FarewellRecord> getFarewellRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<FarewellRecord> list=farewellRecordMapper.getFarewellRecordMapList(paramMap);
		PageInfo<FarewellRecord> page=new PageInfo<FarewellRecord>(list);
		return page;
		
	}
	/**
	 * 布置告别厅
	 * @param orderIds 
	 * @param comments 
	 * @param handlerIds 
	 * @param farewellRecord
	 */
	public void arrangeFarewell(String[] handlerIds, String[] comments, String[] orderIds, FarewellRecord farewellRecord) {
		if (handlerIds != null && comments != null && orderIds != null) {
			for (int i = 0; i < orderIds.length; i++) {
    			ItemOrder io = itemOrderService.findItemOrderById(orderIds[i]);
    			io.setHandlerId(handlerIds[i]);
    			io.setComment(comments[i]);
    			itemOrderService.arrangeItem(io);
			}
		}
		farewellRecordMapper.arrangeFarewell(farewellRecord);
	}
	/**
	 * 获取某天告别厅记录数
	 * @param map
	 * @return
	 */
	public int getFarewellRecordCount(Map<String, Object> map) {
		return farewellRecordMapper.getFarewellRecordCount(map);
	}
	public void deleteFarewellRecordByCoId(String cid) {
		farewellRecordMapper.deleteFarewellRecordByCoId(cid);
	}
	public void updateFarewellRecordIsDeal(String id, byte isdel) {
		//farewellRecordMapper.updateFarewellRecordIsDeal(id, isdel);
		//System.out.println(isdel);
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	FarewellRecord fr = getFarewellRecordId(ids[i]);
	    	if (isdel==Const.Yes_treated) {
	    		fr.setIsdel(Const.Yes_treated);
			}else if(isdel==Const.No_treated){
				fr.setIsdel(Const.No_treated);
			}
	    	updateFarewellRecord(fr);
	    }
	}
}
