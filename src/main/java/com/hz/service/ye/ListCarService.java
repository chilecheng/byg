package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.ye.CarSchedulRecord;
import com.hz.mapper.ye.ListCarMapper;

@Service
@Transactional
public class ListCarService {
	@Autowired
	private ListCarMapper listCarMapper;

	/**
	 * 获取车辆列表
	 */
	public List<CarSchedulRecord> findListCar(Map<String, Object> paramMap) {
		List<CarSchedulRecord> list = listCarMapper.findListCar(paramMap);
		return list;
	}

	/**
	 * 获取车辆列表页
	 */
	public List<CarSchedulRecord> getListCarPageInfo(Map<String, Object> paramMap) {
		List<CarSchedulRecord> list = listCarMapper.findListCar(paramMap);
		return list;
	}
}
