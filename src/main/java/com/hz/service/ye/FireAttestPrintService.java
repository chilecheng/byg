package com.hz.service.ye;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FireAttestPrintInfo;
import com.hz.entity.ye.PincardRecord;
import com.hz.entity.ye.PrintAgain;
import com.hz.entity.ye.PrintRecord;
import com.hz.mapper.ye.CommissionOrderMapper;
import com.hz.mapper.ye.FireAttestPrintMapper;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 三科火化证明打印
 * @author lpp
 *
 */
@Service
@Transactional
public class FireAttestPrintService {
	
	@Autowired
	private FireAttestPrintMapper fireAttestPrintMapper;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private FirePrintAgainService firePrintAgainService;
	@Autowired
	private PincardRecordService pincardRecordService;
	/**
	 * 根据条件获取火化证明打印信息列表
	 * @param map
	 * @return
	 */
	public List<FireAttestPrintInfo> getBasicInfoList(Map<String,Object>map){
		return fireAttestPrintMapper.getBasicInfoList(map);
	}
	/**
	 * 分页
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<FireAttestPrintInfo> getBasicInfoListPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<FireAttestPrintInfo> list=getBasicInfoList(paramMap);
		PageInfo<FireAttestPrintInfo> page = new PageInfo<FireAttestPrintInfo>(list);
		return page; 
	}
	public FireAttestPrintInfo getBasicInfoByCard(String cardCode){
		return fireAttestPrintMapper.getBasicInfoByCard(cardCode);
	}
	
	public void addPrintRecord(PrintRecord pr){
		fireAttestPrintMapper.addPrintRecord(pr);
	}

	public String getActualFireTimeByCId(String cid){
		return fireAttestPrintMapper.getActualFireTimeByCId(cid);
	}
	
	public void destoryCardById(String cid){
		fireAttestPrintMapper.destoryCardById(cid);
	} 
	
	public List<PrintRecord> getPrintRecordByMap(Map<String,Object> map){
		return fireAttestPrintMapper.getPrintRecordByMap(map);
	}
	public void updatePrintRecord(PrintRecord pr){
		fireAttestPrintMapper.updatePrintRecord(pr);
	}
	public PrintRecord getPrintRecordById(String id){
		return fireAttestPrintMapper.getPrintRecordById(id);
	}
	
	/**
	 * 事务控制更新打印状态
	 */
	public void addAndUpdate(PrintRecord printRecord,String cid,
			PrintAgain printAgain,String id){
		
		if(id!=null && !"".equals(id)){
			firePrintAgainService.addPrintAgain(printAgain);
			updatePrintRecord(printRecord);
			commissionOrderService.updateCommissionOrderScheduleFlag(cid, Const.schedule_flag_4);
		}else{
			commissionOrderService.updateCommissionOrderScheduleFlag(cid, Const.schedule_flag_4);
			addPrintRecord(printRecord);
		}
	}
	/**
	 * 事务控制销卡并添加销卡记录
	 * @param pinCard
	 * @param id
	 */
	public void delCard(PincardRecord pinCard,String id){
		destoryCardById(id);//销卡，但保留委托单其他字段
		pincardRecordService.addPincardRecord(pinCard);
	}
	/**
	 * 事务控制保存领取人信息
	 */
	public void addOrUpdate(String commissionOrderId,String printId,
			String takeNameAgain,String takeCodeAgain,Timestamp printTimeAgain,
			String userId){
		
		PrintRecord pr=null;
		if(printId !=null && printId !=""){
			pr=getPrintRecordById(printId);
			pr.setTakeCodeAgain(takeCodeAgain);
			pr.setTakeNameAgain(takeNameAgain);
			pr.setPrintTimeAgain(printTimeAgain);				
			updatePrintRecord(pr);
		}else{
			pr=new PrintRecord();
			pr.setId(UuidUtil.get32UUID());
			pr.setCid(commissionOrderId);
			pr.setTakeCode(takeCodeAgain);
			pr.setTakeName(takeNameAgain);
			pr.setPrintTime(printTimeAgain);
			pr.setUserId(userId);
			pr.setTakeNameAgain(takeNameAgain);
			pr.setTakeCodeAgain(takeCodeAgain);
			pr.setPrintTimeAgain(printTimeAgain);
			addPrintRecord(pr);
//			commissionOrderService.updateCommissionOrderScheduleFlag(commissionOrderId, Const.schedule_flag_4);
		}
		
	}
}
