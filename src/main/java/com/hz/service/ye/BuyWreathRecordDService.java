package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.BuyWreathRecordD;
import com.hz.mapper.ye.BuyWreathRecordDMapper;

/**
 * 花圈花篮订购项目
 * @author hw
 *
 */
@Service
@Transactional
public class BuyWreathRecordDService {
	@Autowired
	private BuyWreathRecordDMapper buyWreathRecordDMapper;
	/**
	 * 添加骨花圈花篮订购
	 * @param BuyWreathRecordD
	 */
	public void addAshesRecordD(BuyWreathRecordD buyWreathRecordD){
		buyWreathRecordDMapper.saveBuyWreathRecordD(buyWreathRecordD);
	}
	/**
	 * 删除花圈花篮订购
	 * @param id
	 */
	public void deleteBuyWreathRecordD(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			buyWreathRecordDMapper.deleteBuyWreathRecordD(ids[i]);
		}
	}
	/**
	 * 修改花圈花篮订购
	 * @param buyWreathRecordD
	 */
	public void updateBuyWreathRecordD(BuyWreathRecordD buyWreathRecordD){
		buyWreathRecordDMapper.updateBuyWreathRecordD(buyWreathRecordD);
	}
	/**
	 * 获取花圈花篮订购
	 * @param id
	 * @return
	 */
	public BuyWreathRecordD getBuyWreathRecordDId(String id){
		return buyWreathRecordDMapper.getBuyWreathRecordDById(id);
	}
	/**
	 * 根据条件获取花圈花篮订购
	 * @param paramMap
	 * @return
	 */
	public List<BuyWreathRecordD> getBuyWreathRecordDList(Map<String, Object> paramMap){
		return buyWreathRecordDMapper.getBuyWreathRecordDList(paramMap);
	}
	
	/**
	 * 用于收费科非委托业务使用
	 * @param paramMap
	 * @return
	 */
	public List<BuyWreathRecordD> getBuyWreathRecordDFee(Map<String ,Object> paramMap){
		return buyWreathRecordDMapper.getBuyWreathRecordDFee(paramMap);
	}
	
	/**
	 * 获取花圈花篮订购列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<BuyWreathRecordD> getBuyWreathRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<BuyWreathRecordD> list=getBuyWreathRecordDList(paramMap);
		PageInfo<BuyWreathRecordD> page=new PageInfo<BuyWreathRecordD>(list);
		return page;
		
	}
}
