package com.hz.service.ye;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.ye.PrintAgain;
import com.hz.mapper.ye.FirePrintAgainMapper;

/**
 * 三科火化证明补打信息查询和增加
 * @author jgj
 *
 */
@Service
@Transactional
public class FirePrintAgainService {
	
	@Autowired
	private FirePrintAgainMapper firePrintAgainMapper;
	
	/**
	 * 根据打印ID获取火化证明补打信息
	 * @param printId
	 * @return
	 */
	public List<PrintAgain> getPrintAgainList(String printId){
		return firePrintAgainMapper.getPrintAgainList(printId);
	}
	/**
	 * 增加补打信息
	 * @param pr
	 */
	public void addPrintAgain(PrintAgain pr){
		firePrintAgainMapper.addPrintAgain(pr);
	}

}
