package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.ye.ItemChange;
import com.hz.mapper.ye.ItemChangeMapper;

/**
 * 丧葬用品更改之前
 * @author jgj
 *
 */
@Service
@Transactional
public class ItemChangeService {
	@Autowired
	private ItemChangeMapper itemChangeMapper;
	
	/**
	 * 添加丧葬用品更改记录
	 * @param itemOrder
	 */
	public void saveItemChange(ItemChange itemChange){
		itemChangeMapper.saveItemChange(itemChange);
	}
	/**
	 * 删除丧葬用品更改记录
	 * @param id
	 */
	public void delItemChange(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	itemChangeMapper.delItemChange(ids[i]);
	    }
	}
	/**
	 * 根据火化单ID删除
	 * @param id
	 */
	public void delItemChangeBycomId(String id){
		itemChangeMapper.delItemChangeBycomId(id);
	}
	/**
	 * 根据购买单号删除
	 * @param id
	 */
	public void delItemChangeByBuyId(String id){
		itemChangeMapper.delItemChangeByBuyId(id);
	}
	/**
	 * 获取丧葬用品更改记录
	 * @param id
	 * @return
	 */
	public ItemChange getItemChangeById(String id){
		ItemChange itemChange=itemChangeMapper.getItemChangeById(id);
		return itemChange;
	}
	
	/**
	 * 根据条件获取丧葬用品更改记录List
	 * 非挂账业务
	 * @param 
	 */
	public List<ItemChange> getItemChangeList(Map<String,Object> paramMap){
		List<ItemChange> list= itemChangeMapper.getItemChangeList(paramMap);
		return list;
	}
}
