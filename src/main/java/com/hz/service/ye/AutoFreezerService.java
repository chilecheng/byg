package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.AutopsyRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.mapper.ye.AutoFreezerMapper;


/**
 * 濡ょ姴鑻鍫ユ偨鐎圭媭鍤�
 * @author non
 *
 */
@Service
@Transactional
public class AutoFreezerService {
	@Autowired
	private AutoFreezerMapper autoFreezerMapper;
	
	/**
	 * 闁哄被鍎叉竟姗�骞嶉敓浠嬪嫉婢跺矈鍎婇柛姘墦閻涙瑧浜搁崨濠冭拫濞寸姴澧庡▓鎴濐潰閺勫骏鎷烽崨顑跨箚闁诡叏鎷�
	 * @param paramMap
	 * @return
	 */
	
	public List<FreezerRecord> findAllAutoFreezer(Map<String ,Object> paramMap){
		List<FreezerRecord> list=autoFreezerMapper.listAutoFreezer(paramMap);
		return list;
	}
	public PageInfo<FreezerRecord> getListAutoFreezer(Map<String ,Object> paramMap,int pageNum,int pageSize,String order) {
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);
		List<FreezerRecord> list=findAllAutoFreezer(paramMap);
		PageInfo<FreezerRecord> page= new PageInfo<FreezerRecord>(list);
		return page;
	
	}
	//婵烇綀顕ф慨鐐测枖閺囩喐鏉哄Δ鐘茶嫰濡楀牓鎮界�圭媭鍤�
	public List findAutoFreezerById(String id)
	{
		return autoFreezerMapper.findAutoFreezerById(id);
	}	
	/**
	 * 闁哄被鍎撮妤呮閿熺晫鎲版笟锟介悰娆戜焊濮濆睗鎺楀礃閼姐倖鐣遍悹浣规緲缂嶏拷 
	 * @param paramMap
	 * @return
	 */
	public List<AutopsyRecord> findAllunAutoFreezer(Map<String,Object> paramMap) {
		List<AutopsyRecord> list=autoFreezerMapper.listunAutoFreezer(paramMap);				
		return list;	
	}	
	
	public PageInfo<AutopsyRecord> getListunAutoFreezer(Map paramMap, int pageNum, int pageSize, String order) {
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);
		List<AutopsyRecord> list=findAllunAutoFreezer(paramMap);
		PageInfo<AutopsyRecord> page=new PageInfo<AutopsyRecord>(list);
		return page;
	}


}
