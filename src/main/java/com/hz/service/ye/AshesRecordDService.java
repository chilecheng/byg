package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.ye.AshesRecordDMapper;
import com.hz.mapper.ye.AshesRecordMapper;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.mapper.ye.FreezerRecordMapper;
import com.hz.mapper.ye.MourningRecordMapper;
/**
 * 骨灰寄存缴费，续费记录
 * @author hw
 *
 */
@Service
@Transactional
public class AshesRecordDService {
	@Autowired
	private AshesRecordDMapper ashesRecordDMapper;
	/**
	 * 添加骨灰寄存缴费，续费记录
	 * @param AshesRecordD
	 */
	public void addAshesRecordD(AshesRecordD ashesRecordD){
		ashesRecordDMapper.saveAshesRecordD(ashesRecordD);
	}
	/**
	 * 删除骨灰寄存缴费，续费记录
	 * @param id
	 */
	public void deleteAshesRecordD(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			ashesRecordDMapper.deleteAshesRecordD(id);
		}
	}
	/**
	 * 修改骨灰寄存缴费，续费记录
	 * @param ashesRecordD
	 */
	public void updateAshesRecordD(AshesRecordD ashesRecordD){
		ashesRecordDMapper.updateAshesRecordD(ashesRecordD);
	}
	/**
	 * 获取骨灰寄存缴费，续费记录对象
	 * @param id
	 * @return
	 */
	public AshesRecordD getAshesRecordDId(String id){
		return ashesRecordDMapper.getAshesRecordDById(id);
	}
	/**
	 * 根据条件获取骨灰寄存缴费，续费记录list
	 * @param paramMap
	 * @return
	 */
	public List<AshesRecordD> getAshesRecordDList(Map<String,Object> paramMap){
		return ashesRecordDMapper.getAshesRecordDList(paramMap);
	}
	/**
	 * 收费科非委托业务使用
	 * @param paramMap
	 * @return
	 */
	public List<AshesRecordD> getAshesRecordDFee(Map<String,Object> paramMap){
		
		return ashesRecordDMapper.getAshesRecordDFee(paramMap);
	}
	/**
	 * 获取骨灰寄存缴费，续费记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo getAshesRecordDPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List list=getAshesRecordDList(paramMap);
		PageInfo page=new PageInfo();
		return page;
		
	}
}
