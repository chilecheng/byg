package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.mapper.ye.FurnaceRecordMapper;

/**火化炉调度
 * @author cjb
 *
 */
@Service
@Transactional
public class FurnaceRecordService {
	@Autowired
	private FurnaceRecordMapper furnaceRecordMapper;
	/**
	 * 添加火化炉信息
	 * @param furnace
	 */
	public void addFurnaceRecord(FurnaceRecord furnace){
		furnaceRecordMapper.saveFurnaceRecord(furnace);
	}
	/**
	 * 删除火化炉信息
	 * @param id
	 */
	public void deleteFurnaceRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	furnaceRecordMapper.deleteFurnaceRecord(ids[i]);
	    }
	}
	/**
	 * 更新火化炉信息
	 * @param furnace
	 */
	public void updateFurnaceRecord(FurnaceRecord furnace){
		furnaceRecordMapper.updateFurnaceRecord(furnace);
	}
	/**
	 * 获取火化炉信息对象
	 * @param id
	 * @return
	 */
	public FurnaceRecord getFurnaceById(String id){
		FurnaceRecord furnace=furnaceRecordMapper.getFurnaceRecordById(id);
		return furnace;
	}
	/**
	 * 根据条件获取火化炉记录list
	 * @param paramMap
	 * @return
	 */
	public List<FurnaceRecord> getFurnaceRecordList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return furnaceRecordMapper.getFurnaceRecordList(paramMap);
	}
	
	/**
	 * 得到所有火化炉的总记录
	 * @param itemId
	 * @param b
	 * @return
	 */
	public int getFurnaceRecordCount(Map<String,Object> map) {
		int furnaceCount=furnaceRecordMapper.getFurnaceRecordCount(map);
		return furnaceCount;
	}
	/**
	 * 首页火化炉查看次数更新
	 * @param furnace
	 */
	public void updateNumber(String id, byte code){
		furnaceRecordMapper.updateNumber(id, code);
	}
	public void deleteFurnaceRecordByCoId(String id) {
		furnaceRecordMapper.deleteFurnaceRecordByCoId(id);
	}
	/**
	 * 解除锁定
	 * @param userId
	 * @param flag
	 * @param commissionOrderId
	 */
	public void deleteFurnaceRecordUnLock(String userId,byte flag,String commissionOrderId){
		furnaceRecordMapper.deleteFurnaceRecordUnLock(userId, flag, commissionOrderId);
	}
}