package com.hz.service.ye;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.ye.FarewellTaskList;
import com.hz.mapper.ye.staticalForm.FarewellTaskMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;

/**
 * @author cjb
 *
 */
@Service
@Transactional
public class FarewellTaskService {
	@Autowired
	FarewellTaskMapper farewellTaskMapper;

	public List<FarewellTaskList> getFarewellTaskList(Map<String, Object> map) {
		return farewellTaskMapper.getFarewellTaskList(map);
	}
	/**
	 * 获取全部的处理状态
	 * @param id
	 * @param isAll
	 * @return
	 */
	public String getDealIselOption0(String id, boolean isAll) {
		List<Object[]> list=new ArrayList<Object[]>();
	    for (int i = 1; i < 4; i++) {
			list.add(new Object[]{i,Const.getDealIsdelType((byte) i)});
		}
	    String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	public void updateIsdel(String id, byte isdel) {
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			FarewellTaskList ftl=getFarewellTaskListById(ids[i]);
			if(isdel==Const.Yes_treated){
				ftl.setDealIsdel(Const.Yes_treated);
			}else if(isdel==Const.No_treated){
				ftl.setDealIsdel(Const.No_treated);
			}else if(isdel==Const.Yes_usered){
				ftl.setDealIsdel(Const.Yes_usered);
			}
			updateFarewellTaskList(ftl);
		}
	}
	/**
	 * 更改
	 * @param ftl
	 */
	private void updateFarewellTaskList(FarewellTaskList ftl) {
		farewellTaskMapper.updateFarewellTaskList(ftl);
		
	}
	/**
	 * 获取
	 * @param id
	 * @return
	 */
	private FarewellTaskList getFarewellTaskListById(String id) {
		FarewellTaskList farewellTaskList = farewellTaskMapper.getFarewellTaskListById(id);
		return farewellTaskList;
	}
	public String getDealIselOptionTwo(String id, boolean isAll) {
		List<Object[]> list=new ArrayList<Object[]>();
	    for (int i = 1; i < 3; i++) {
			list.add(new Object[]{i,Const.getDealIsdelType((byte) i)});
		}
	    String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}

}
