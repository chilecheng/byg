package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.ye.MourningRecordMapper;
import com.hz.util.Const;
/**
 * 灵堂记录信息
 * @author cjb
 *
 */
@Service
@Transactional
public class MourningRecordService {
	@Autowired
	private MourningRecordMapper mourningRecordMapper;
	@Autowired
	private ItemOrderService itemOrderService;
	/**
	 * 添加灵堂记录
	 * @param mourningRecord
	 */
	public void addMourningRecord(MourningRecord mourningRecord){
		mourningRecordMapper.saveMourningRecord(mourningRecord);
	}
	/**
	 * 删除灵堂记录
	 * @param id
	 */
	public void deleteMourningRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			mourningRecordMapper.deleteMourningRecord(id);
		}
	}
	/**
	 * 修改灵堂记录
	 * @param mourningRecord
	 */
	public void updateMourningRecord(MourningRecord mourningRecord){
		mourningRecordMapper.updateMourningRecord(mourningRecord);
	}
	/**
	 * 获取灵堂记录对象
	 * @param 灵堂记录id
	 * @return
	 */
	public MourningRecord getMourningRecordId(String id){
		return mourningRecordMapper.getMourningRecordById(id);
	}
	/**
	 * 根据条件获取灵堂记录list
	 * @param paramMap
	 * @return
	 */
	public List<MourningRecord> getMourningRecordList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return mourningRecordMapper.getMourningRecordList(paramMap);
	}
	
	/**
	 * 获取灵堂记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<MourningRecord> getMourningRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<MourningRecord> list=mourningRecordMapper.getMourningRecordListMap(paramMap);
		PageInfo<MourningRecord> page=new PageInfo<MourningRecord>(list);
		return page;
		
	}
	public MourningRecord getMourningRecordByOId(String id) {
		return mourningRecordMapper.getMourningRecordByOId(id);
	}
	/**
	 * 只查灵堂记录和灵堂信息
	 * @param 火化委托单id
	 * @return
	 */
	public MourningRecord getMourningRecord(String id) {
		return mourningRecordMapper.getMourningRecord(id);
	}
	/**
	 * 更改灵堂状态：如改为清场状态/完成状态
	 * @param commissionOrderId
	 * @param flag
	 */
	public void updateMourningRecordFlag(String commissionOrderId,byte flag){
		mourningRecordMapper.updateMourningRecordFlag(commissionOrderId, flag);
	}
	/**
	 * 布置灵堂信息
	 * @param orderIds 
	 * @param comments 
	 * @param handlerIds 
	 * @param mourningRecord
	 */
	public void arrangeMourning(String[] handlerIds, String[] comments, String[] orderIds, MourningRecord mourningRecord) {
		if (handlerIds != null && orderIds != null) {
			for (int i = 0; i < orderIds.length; i++) {
    			ItemOrder io = itemOrderService.findItemOrderById(orderIds[i]);
    			io.setHandlerId(handlerIds[i]);
    			io.setComment(comments[i]);
    			itemOrderService.arrangeItem(io);
			}
		}
		mourningRecordMapper.arrangeFarewell(mourningRecord);
	}
	
	/**
	 * 获取某天灵堂记录数
	 * @param map
	 * @return
	 */
	public int getMourningRecordCount(Map<String, Object> map) {
		return mourningRecordMapper.getMourningRecordCount(map);
	}
	/**
	 * 根据火化委托单 id 删除对应的灵堂记录
	 * @param coId
	 */
	public void deleteMourningRecordByCoId(String coId) {
		mourningRecordMapper.deleteMourningRecordByCoId(coId);
	}
	/**
	 * 灵堂 解除锁定
	 * @param userId
	 * @param flag
	 * @param commissionOrderId
	 */
	public void deleteMourningRecordUnlock(String userId,byte flag,String commissionOrderId){
		mourningRecordMapper.deleteMourningRecordUnlock(userId, flag, commissionOrderId);
	}
	
	
	
	public void updateMourningRecordIsDeal(String id, byte isdel) {
		//mourningRecordMapper.updateMourningRecordIsDeal(id, isdel);
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	MourningRecord mr = getMourningRecordByOId(ids[i]);
	    	if (isdel==Const.Yes_treated) {
	    		mr.setIsdel(Const.Yes_treated);
			}else if(isdel==Const.No_treated){
				mr.setIsdel(Const.No_treated);
			}
	    	updateMourningRecord(mr);
	    }
	}
}