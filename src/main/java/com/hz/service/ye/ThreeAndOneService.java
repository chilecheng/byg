package com.hz.service.ye;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hz.entity.ye.ThreeAndOne;
import com.hz.mapper.ye.ThreeAndOneMapper;

/**
 * 三合一数据
 * @author Administrator
 *
 */
@Service
public class ThreeAndOneService {
  
	@Autowired
	private ThreeAndOneMapper threeAndOneMapper;
	
	public String getCommissionOrderId(String id){
		Map<String,Object> param =new HashMap<String,Object>();
		param.put("farewellId", id);
		String result="";
		result=threeAndOneMapper.getCommissionOrderId(param);
		return result;
	}
	/**
	 * 通过页面传来的 id 获得需要的礼厅的所有信息 填装进一个对象之中
	 * @param id
	 * @return
	 */
	public ThreeAndOne getAllInformation(String id){
		//1
		ThreeAndOne obj=	threeAndOneMapper.getInformation(id);
		//2
		String[] types={ 
				"507",             //司仪
				"508",             //礼仪出殡
				"581",             //电子遗像
				"584",             //摄像
				"535",              //主联
				};
		String[] result =new String[types.length];
		for(int i=0;i<types.length;i++){
			Map<String ,Object> type=new HashMap<String,Object>();
			type.put("id", id);
		   type.put("helpCode", types[i]);
		  String  price =threeAndOneMapper.getOneItemPrice(type);
		   result[i]=price;
		   System.out.println("################result"+i+price );
		}
		obj.setMaster("司仪");
	    obj.setMasterPrice(result[0]);	
		obj.setFuneral("礼仪出殡");
	    obj.setFuneralPrice(result[1]);	
		obj.setElectronicPortrait("电子遗像");
	    obj.setElectronicPortraitPrice(result[2]);	
		obj.setCamera("摄像");
	    obj.setCameraPrice(result[3]);	
		obj.setContact("主联");
	    obj.setContactPrice(result[4]);
	    
	String[ ] type1={"513","514","515","516","517","589"};  //围花  
	String[] result1 =new String[type1.length];
	for(int i=0;i<type1.length;i++){
		Map<String ,Object> type=new HashMap<String,Object>();
		type.put("id", id);
		 type.put("helpCode", type1[i]);
		 String price= threeAndOneMapper.getOneItemPrice(type);
		    if(price!=null && price!=""){
		    result1[i]=price;
		    }else{	
		    	result1[i]="0";
		    }
	}
	double sum1=Double.parseDouble(result1[0])+Double.parseDouble(result1[1])
    +Double.parseDouble(result1[2])+Double.parseDouble(result1[3])+Double.parseDouble(result1[4])
    		+Double.parseDouble(result1[5]);
	obj.setWaiFlower("围花");
	obj.setWaiFlowerPrice(String.valueOf(sum1));
	
	String[ ] type2={"521","522","523","524"}; //花门  
	String[] result2 =new String[type2.length];
	for(int i=0;i<type2.length;i++){
		Map<String ,Object> type=new HashMap<String,Object>();
		type.put("id", id);
		 type.put("helpCode", type2[i]);
	    String price= threeAndOneMapper.getOneItemPrice(type);
	    if(price!=null && price!=""){
	    	result2[i]=price;
	    }else{	
	    	result2[i]="0";
	    }
	}
	double sum2=Double.parseDouble(result2[0])+Double.parseDouble(result2[1])
			           +Double.parseDouble(result2[2])+Double.parseDouble(result2[3]);
	obj.setFlowerDoor("花门");
    obj.setFlowerDoorPrice(String.valueOf(sum2));
    
	String[ ] type3={"588","587","540"};//背景
	String[] result3 =new String[type3.length];
	for(int i=0;i<type3.length;i++){
		Map<String ,Object> type=new HashMap<String,Object>();
		type.put("id", id);
		 type.put("helpCode", type3[i]);
		 String price= threeAndOneMapper.getOneItemPrice(type);
		    if(price!=null && price!=""){
		    result3[i]=price;
		    }else{	
		    	result3[i]="0";
		    }
	}
	double sum3=Double.parseDouble(result3[0])+Double.parseDouble(result3[1])
    +Double.parseDouble(result3[2]);
	obj.setBackGround("背景");
    obj.setBackGroundPrice(String.valueOf(sum3));
    
	String[ ] type4={"532","586"};//黑纱。白纱
	String[] result4 =new String[type4.length];
	for(int i=0;i<type4.length;i++){
		Map<String ,Object> type=new HashMap<String,Object>();
		type.put("id", id);
		 type.put("helpCode", type4[i]);
	     result4[i]= threeAndOneMapper.getOneItemPrice(type);
	}
	obj.setYarn("纱");
	if(result4[0]!=null && result4[0]!="" && result4[1]!=null && result4[1]!=""){
		obj.setYarnPrice("黑纱，白纱");
	}else if(result4[1]!=null && result4[1]!=""){
		obj.setYarnPrice("白纱");
	}else if(result4[0]!=null && result4[0]!=""){
		obj.setYarnPrice("黑纱");
	}else{
		obj.setYarnPrice("无");
	}
	
	String[ ] type5={"544","545"};//寿球黑白
	String[] result5 =new String[type5.length];
	for(int i=0;i<type5.length;i++){
		Map<String ,Object> type=new HashMap<String,Object>();
		type.put("id", id);
		 type.put("helpCode", type5[i]);
	     result5[i]= threeAndOneMapper.getOneItemPrice(type);
	}
	obj.setBall("寿球");
	if(result5[0]!=null && result5[0]!="" && result5[1]!=null && result5[1]!=""){
		obj.setBallPrice("孝球黑，孝球白");
	}else if(result5[1]!=null && result5[1]!=""){
		obj.setBallPrice("孝球白");
	}else if(result5[0]!=null && result5[0]!=""){
		obj.setBallPrice("孝球黑");
	}else{
		obj.setBallPrice("无");
	}
	
	/**
	 * 花圈
	 */
	String[] type6 = {"667","638","644","642","641"};
	String[] result6 = new String[type6.length];
	for(int i=0; i<type6.length; i++){
		Map<String, Object> type = new HashMap<String, Object>();
		type.put("id", id);
		type.put("helpCode", type6[i]);
		String price= threeAndOneMapper.getOneItemPrice(type);
	    if(price!=null && price!=""){
	    	result6[i]=price;
	    }else{	
	    	result6[i]="0";
	    }
	}
	double sum6=Double.parseDouble(result6[0])+Double.parseDouble(result6[1])+Double.parseDouble(result6[2])
			+Double.parseDouble(result6[3])+Double.parseDouble(result6[4]);
	obj.setFlowerQuan("花圈");
    obj.setFlowerQuanPrice(String.valueOf(sum6));
	
    /**
     * 花柱
     */
    String[] type7 = {"651","652"};
	String[] result7 = new String[type7.length];
	for(int i=0; i<type7.length; i++){
		Map<String, Object> type = new HashMap<String, Object>();
		type.put("id", id);
		type.put("helpCode", type7[i]);
		String price= threeAndOneMapper.getOneItemPrice(type);
	    if(price!=null && price!=""){
	    	result7[i]=price;
	    }else{	
	    	result7[i]="0";
	    }
	}
	double sum7=Double.parseDouble(result7[0])+Double.parseDouble(result7[1]);
	obj.setFlowerZhu("花柱");
	obj.setFlowerZhuPrice(String.valueOf(sum7));
	
	/**
     * 花牌
     */
    String[] type8 = {"653"};
	String[] result8 = new String[type8.length];
	for(int i=0; i<type8.length; i++){
		Map<String, Object> type = new HashMap<String, Object>();
		type.put("id", id);
		type.put("helpCode", type8[i]);
		String price= threeAndOneMapper.getOneItemPrice(type);
	    if(price!=null && price!=""){
	    	result8[i]=price;
	    }else{	
	    	result8[i]="0";
	    }
	}
	double sum8=Double.parseDouble(result8[0]);
	obj.setFlowerPai("花牌");
	obj.setFlowerPaiPrice(String.valueOf(sum8));
	
	/**
     * 绢花篮
     */
    String[] type9 = {"645"};
	String[] result9 = new String[type9.length];
	for(int i=0; i<type9.length; i++){
		Map<String, Object> type = new HashMap<String, Object>();
		type.put("id", id);
		type.put("helpCode", type9[i]);
		String price= threeAndOneMapper.getOneItemPrice(type);
	    if(price!=null && price!=""){
	    	result9[i]=price;
	    }else{	
	    	result9[i]="0";
	    }
	}
	double sum9=Double.parseDouble(result9[0]);
	obj.setJuanFlowerLan("绢花篮");
	obj.setJuanFlowerLanPrice(String.valueOf(sum9));
	
	return obj;
	}
}
