package com.hz.service.ye;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.system.User;
import com.hz.entity.ye.RepairInfo;
import com.hz.mapper.ye.RepairMapper;
import com.hz.service.system.UserService;

/**
 * 维修
 * @author cjb
 *
 */
@Service
@Transactional
public class RepairService {
	
	@Autowired
	private RepairMapper repairMapper;
	@Autowired
	private   UserService userService;
	/**
	 * 报修
	 * @param map
	 */
	public void repair(Map<String, Object> map) {
		repairMapper.repair(map);
	}
	/**
	 * 获取灵堂维修信息
	 * @param id
	 * @return
	 */
	public RepairInfo showMourInfo(String id) {
		return repairMapper.getMourningRecord(id);
	}

	/**
	 * 获取告别厅维修信息
	 * @param id
	 * @return
	 */
	public RepairInfo showFareInfo(String id) {
		return repairMapper.getFarewellRecord(id);
	}
	/**
	 * 获取火化炉维修信息
	 * @param id
	 * @return
	 */
	public RepairInfo showFurnaceInfo(String id) {
		return repairMapper.getFurnaceRecord(id);
	}
	/**
	 * 获取车辆维修信息
	 * @param id
	 * @return
	 */
	public RepairInfo showCarInfo(String id) {
		return repairMapper.getCarRecord(id);
	}
	/**
	 * 获取冰柜维修信息
	 * @param id
	 * return
	 */
	public RepairInfo showFreezerInfo(String id){
		return repairMapper.getFreezerRecord(id);
	}
	
	/**
	 * 维修完成方法
	 * @param map
	 */
	public void completeRepair(Map<String, Object> map) {
		repairMapper.completeRepair(map);
	}
	/**
	 * 更新维修信息
	 * @param map
	 */
	public void changeInfo(Map<String, Object> map) {
		repairMapper.changeInfo(map);
	}
	
	
	
}
