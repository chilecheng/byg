package com.hz.service.ye;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Farewell;
import com.hz.entity.base.Furnace;
import com.hz.entity.base.Jeneration;
import com.hz.entity.base.Mourning;
import com.hz.entity.ye.Appointment;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.ye.AppointmentMapper;
import com.hz.service.base.FarewellService;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.JenerationService;
import com.hz.service.base.MourningService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;
/**
 * 预约登记
 * @author jgj
 *
 */
@Service
@Transactional
public class AppointmentService {
	@Autowired
	private AppointmentMapper appointmentMapper;
	@Autowired
	private JenerationService jenerationService;
	//告别厅
	@Autowired
	private FarewellService farewellService;
	//灵堂信息
	@Autowired
	private MourningService mourningService;
	//告别厅使用记录
	@Autowired
	private FarewellRecordService farewellRecordService;
	//守灵室使用记录
	@Autowired
	private MourningRecordService mourningRecordService;
	//火化炉信息
	@Autowired
	private FurnaceService furnaceService;
	//火化炉记录信息
	@Autowired
	private FurnaceRecordService furnaceRecordService;
	/**
	 * 根据条件查询 预约情况
	 * @param paramap
	 * @return
	 */
	public List<Appointment> getAppointmentList(Map<String, Object> paramap){
		return appointmentMapper.getAppointmentList(paramap);
	}
	/**
	 * 保存预约记录
	 * @param appointment
	 */
	public void saveAppointment(Appointment appointment){
		String serialNumber=jenerationService.getJenerationCode(Jeneration.YYDJ_TYPE);
		appointment.setSerialNumber(serialNumber);
		appointmentMapper.saveAppointment(appointment);
	}
	/**
	 * 更改预约记录
	 * @param appointment
	 */
	public void updateAppointment(Appointment appointment){
		appointmentMapper.updateAppointment(appointment);
	}
	/**
	 * 删除预约记录
	 * @param id
	 */
	public void delAppointment(String id){
		appointmentMapper.delAppointment(id);
	}
	/**
	 * 根据ID查找预约记录
	 * @param id
	 * @return
	 */
	public Appointment getAppointmentById(String id){
		return appointmentMapper.getAppointmentById(id);
	}

	
	public PageInfo getAppointmentListPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		
		
		//没有条件查找的结果
		List<Appointment> list=getAppointmentList(paramMap);		
		PageInfo page = new PageInfo(list);
		page.setPageSize(pageSize);
		return page; 
	}
	/**
	 * 事务控制删除所有预约记录
	 * @param appointment
	 */
	public void delAllRecord(Appointment appointment){
		String farewellId=appointment.getFarewellId();
		String mourningId=appointment.getMourningId();
		String funeralId=appointment.getFuneralId();
		String id=appointment.getId();
		if(!"".equals(farewellId) && farewellId!=null){
			farewellRecordService.deleteFarewellRecord(farewellId);
		}
		if(!"".equals(mourningId) && mourningId!=null){
			mourningRecordService.deleteMourningRecord(mourningId);
		}
		if(!"".equals(funeralId) && funeralId!=null){
			furnaceRecordService.deleteFurnaceRecord(funeralId);
		}
		if(appointment.getIsCar()==Const.Is_Yes){
			appointment.setFarewellCode(null);
			appointment.setFarewellId(null);
			appointment.setFarewellTime(null);
			appointment.setFuneralCode(null);
			appointment.setFuneralId(null);
			appointment.setFuneralTime(null);
			appointment.setMourningBeginTime(null);
			appointment.setMourningCode(null);
			appointment.setMourningEndTime(null);
			appointment.setMourningId(null);
			appointment.setIsAppointment(Const.Zero);
			updateAppointment(appointment);
		}else{
			delAppointment(id);
		}
		
	}
	/**
	 * 修改所有相关的预约记录
	 * @param appointment
	 * @param farewellCode
	 * @param funeralCode
	 * @param mourningCode
	 * @param nowTime
	 * @param userId
	 * @param farewellTime
	 * @param funeralTime
	 * @param mourningBeginTime
	 * @param mourningEndTime
	 */
	public void updateAllRecord(Appointment appointment,String farewellCode,String funeralCode,String mourningCode,
			Timestamp nowTime,String userId,Timestamp farewellTime,Timestamp funeralTime,Timestamp mourningBeginTime,
			Timestamp mourningEndTime){
		
		String farewellId = appointment.getFarewellId();
		String mourningId = appointment.getMourningId();
		String funeralId = appointment.getFuneralId();
		
		//告别厅的记录更新
		if(farewellId !=null &&!"".equals(farewellId)){//之前有告别厅记录  1
			if(farewellCode!=null && !"".equals(farewellCode)){//1-现在改记录
				FarewellRecord farewellRecord=farewellRecordService.getFarewellRecordId(farewellId);	
				Farewell farewell=farewellService.getFarewellByName(farewellCode);
				farewellRecord.setFarewellId(farewell.getId());
				farewellRecord.setCreateUserId(userId);
				farewellRecord.setCreateTime(nowTime);
				farewellRecord.setBeginDate(farewellTime);
				farewellRecord.setFlag(Const.IsFlag_YY);
				farewellRecord.setAppointmentName(appointment.getName());
				farewellRecordService.updateFarewellRecord(farewellRecord);
			}else{//1-现在没记录
				farewellRecordService.deleteFarewellRecord(farewellId);
				appointment.setFarewellId(null);
			}
		}else{//之前没告别厅记录  2
			if(farewellCode!=null && !"".equals(farewellCode)){// 2-现在新加记录
				FarewellRecord farewellRecord=new FarewellRecord();	
				Farewell farewell=farewellService.getFarewellByName(farewellCode);   
				String farewellNewId=UuidUtil.get32UUID();
				farewellRecord.setFarewellId(farewell.getId());
				farewellRecord.setCreateUserId(userId);
				farewellRecord.setCreateTime(nowTime);
				farewellRecord.setBeginDate(farewellTime);
				farewellRecord.setId(farewellNewId);
				farewellRecord.setAppointmentName(appointment.getName());
				farewellRecord.setFlag(Const.IsFlag_YY);
				farewellRecordService.addFarewellRecord(farewellRecord);    
				appointment.setFarewellId(farewellNewId);
			}
		}
		
		//火化炉的记录更新
		if(funeralId !=null &&!"".equals(funeralId)){//之前有火化预定记录 1
			if(funeralCode!=null && !"".equals(funeralCode)){// 1-现在更新记录 
				FurnaceRecord furnaceRecord = furnaceRecordService.getFurnaceById(funeralId);				
				Furnace furnace=furnaceService.getFurnaceByName(funeralCode);
				furnaceRecord.setOrderTime(funeralTime);
				furnaceRecord.setCreateTime(nowTime);
				furnaceRecord.setCreateUserId(userId);
				furnaceRecord.setFurnaceId(furnace.getId());
				furnaceRecord.setFlag(Const.IsFlag_YY);	
				furnaceRecord.setAppointmentName(appointment.getName());
				furnaceRecordService.updateFurnaceRecord(furnaceRecord);
			}else{// 1-现在没有记录了
				furnaceRecordService.deleteFurnaceRecord(funeralId);
				appointment.setFuneralId(null);
			}
		}else{// 之前没有记录 2			
			if(funeralCode!=null && !"".equals(funeralCode)){// 2-现在新加记录
				Furnace furnace=furnaceService.getFurnaceByName(funeralCode);
				FurnaceRecord furnaceRecord = new FurnaceRecord();
				String funeralNewId=UuidUtil.get32UUID();
				furnaceRecord.setOrderTime(funeralTime);
				furnaceRecord.setCreateTime(nowTime);
				furnaceRecord.setCreateUserId(userId);
				furnaceRecord.setFurnaceId(furnace.getId());
				furnaceRecord.setId(funeralNewId);
				furnaceRecord.setAppointmentName(appointment.getName());
				furnaceRecord.setFlag(Const.IsFlag_YY);
				furnaceRecordService.addFurnaceRecord(furnaceRecord);
				appointment.setFuneralId(funeralNewId);
			}			
		}
		
		//灵堂的记录更新
		if(mourningId !=null &&!"".equals(mourningId)){//之前有灵堂记录 1
			if(mourningCode!=null && !"".equals(mourningCode)){//1-现在更改灵堂记录
				Mourning mourning=mourningService.getMourningByName(mourningCode);
				MourningRecord mourningRecord = mourningRecordService.getMourningRecordId(mourningId);
				mourningRecord.setFlag(Const.IsFlag_YY);
				mourningRecord.setBeginTime(mourningBeginTime);
				mourningRecord.setEndTime(mourningEndTime);
				mourningRecord.setCreateTime(nowTime);
				mourningRecord.setCreateUserId(userId);
				mourningRecord.setAppointmentName(appointment.getName());
				mourningRecord.setMourningId(mourning.getId());
				mourningRecordService.updateMourningRecord(mourningRecord);
			}else{//1- 现在没有灵堂记录了
				mourningRecordService.deleteMourningRecord(mourningId);
				appointment.setMourningId(null);
			}
		}else{//之前没有灵堂 记录 2
			if(mourningCode!=null && !"".equals(mourningCode)){//2-现在新加灵堂记录
				Mourning mourning=mourningService.getMourningByName(mourningCode);
				String mourningNewId=UuidUtil.get32UUID();
				MourningRecord mourningRecord = new MourningRecord();
				mourningRecord.setFlag(Const.IsFlag_YY);
				mourningRecord.setBeginTime(mourningBeginTime);
				mourningRecord.setEndTime(mourningEndTime);
				mourningRecord.setCreateTime(nowTime);
				mourningRecord.setCreateUserId(userId);
				mourningRecord.setMourningId(mourning.getId());
				mourningRecord.setId(mourningNewId);
				mourningRecord.setAppointmentName(appointment.getName());
				mourningRecordService.addMourningRecord(mourningRecord);
				appointment.setMourningId(mourningNewId);
			}			
		}
		//最后更新预约记录
		updateAppointment(appointment);
	}
	/**
	 * 事务控制保存相关记录
	 * @param appointment
	 * @param farewellCode
	 * @param funeralCode
	 * @param mourningCode
	 * @param nowTime
	 * @param userId
	 * @param farewellTime
	 * @param funeralTime
	 * @param mourningBeginTime
	 * @param mourningEndTime
	 */
	public void saveAllRecord(String id,Appointment appointment,String farewellCode,String funeralCode,String mourningCode,
			Timestamp nowTime,String userId,Timestamp farewellTime,Timestamp funeralTime,Timestamp mourningBeginTime,
			Timestamp mourningEndTime){
		//如果有告别厅号
		if(farewellCode!=null && !"".equals(farewellCode)){
			FarewellRecord farewellRecord=new FarewellRecord();	
			Map<String,Object> map=new HashMap<>();
			map.put("name", farewellCode);
			List<Farewell> farewellList=farewellService.getFarewellList(map, null);   
			String farewellId=UuidUtil.get32UUID();
			farewellRecord.setFarewellId(farewellList.get(0).getId());
			farewellRecord.setCreateUserId(userId);
			farewellRecord.setCreateTime(nowTime);
			farewellRecord.setBeginDate(farewellTime);
			farewellRecord.setId(farewellId);
			farewellRecord.setAppointmentName(appointment.getName());
			farewellRecord.setFlag(Const.IsFlag_YY);
			farewellRecordService.addFarewellRecord(farewellRecord);    
			appointment.setFarewellId(farewellId);
		}
		//如果有火化炉号
		if(funeralCode!=null && !"".equals(funeralCode)){
			Map<String,Object> map=new HashMap<>();
			map.put("name",funeralCode);
			List<Furnace> furnaceList=furnaceService.getFurnaceList(map, null);
			String funeralId=UuidUtil.get32UUID();
			FurnaceRecord furnaceRecord = new FurnaceRecord();
			furnaceRecord.setFlag(Const.IsFlag_YY);
			furnaceRecord.setOrderTime(funeralTime);
			furnaceRecord.setCreateTime(nowTime);
			furnaceRecord.setCreateUserId(userId);
			furnaceRecord.setFurnaceId(furnaceList.get(0).getId());
			furnaceRecord.setId(funeralId);
			furnaceRecord.setAppointmentName(appointment.getName());
			furnaceRecordService.addFurnaceRecord(furnaceRecord);
			appointment.setFuneralId(funeralId);
		}
		//如果有灵堂号
		if(mourningCode!=null && !"".equals(mourningCode)){
			Map<String,Object> map=new HashMap<>();
			map.put("name", mourningCode);
			List<Mourning> mourningList=mourningService.getMourningList(map, null);
			String mourningId=UuidUtil.get32UUID();
			MourningRecord mourningRecord = new MourningRecord();
			mourningRecord.setFlag(Const.IsFlag_YY);
			mourningRecord.setBeginTime(mourningBeginTime);
			mourningRecord.setEndTime(mourningEndTime);
			mourningRecord.setCreateTime(nowTime);
			mourningRecord.setCreateUserId(userId);
			mourningRecord.setMourningId(mourningList.get(0).getId());
			mourningRecord.setId(mourningId);
			mourningRecord.setAppointmentName(appointment.getName());
			mourningRecordService.addMourningRecord(mourningRecord);
			appointment.setMourningId(mourningId);
		}
		if(id!=null && id!=""){
			updateAppointment(appointment);
		}else{
			saveAppointment(appointment);
		}
		
	}
}
