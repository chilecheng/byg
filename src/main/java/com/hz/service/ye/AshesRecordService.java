package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Jeneration;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.mapper.ye.AshesRecordMapper;
import com.hz.service.base.JenerationService;
/**
 * �ǻҼĴ��¼��Ϣ
 * @author hw
 *
 */
@Service
@Transactional
public class AshesRecordService {
	@Autowired
	private AshesRecordMapper ashesRecordMapper;
	@Autowired
	private AshesRecordDService ashesRecordDService;
	//��Ź���
	@Autowired
	private JenerationService jenerationService;
	/**
	 * ���ӹǻҼĴ��¼
	 * @param asd 
	 * @param AshesRecord
	 */
	public void addAshesRecord(AshesRecord ashesRecord, AshesRecordD asd){
		String serialNumber=jenerationService.getJenerationCode(Jeneration.GHJC_TYPE);//�ǻҼĴ���ˮ��
		ashesRecord.setSerialNumber(serialNumber);
		ashesRecordMapper.saveAshesRecord(ashesRecord);
		ashesRecordDService.addAshesRecordD(asd);
	}
	/**
	 * ɾ���ǻҼĴ��¼
	 * @param id
	 */
	public void deleteAshesRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			ashesRecordMapper.deleteAshesRecord(id);
		}
	}
	/**
	 * �޸ĹǻҼĴ��¼
	 * @param ashesRecord
	 */
	public void updateAshesRecord(AshesRecord ashesRecord){
		ashesRecordMapper.updateAshesRecord(ashesRecord);
	}
	/**
	 * ��ȡ�ǻҼĴ��¼����
	 * @param id
	 * @return
	 */
	public AshesRecord getAshesRecordById(String id){
		return ashesRecordMapper.getAshesRecordById(id);
	}

	/**
	 * ����������ȡ�ǻҼĴ��¼list
	 * @param paramMap
	 * @return
	 */
	public List<AshesRecord> getAshesRecordList(Map<String,Object> paramMap){
		return ashesRecordMapper.getAshesRecordList(paramMap);
	}
	/**
	 * ��ȡ�ǻҼĴ��¼�б�
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<AshesRecord> getAshesRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//���÷�ҳ
		PageHelper.orderBy(order);//����
		List<AshesRecord> list=getAshesRecordList(paramMap);
		PageInfo<AshesRecord> page=new PageInfo<AshesRecord>(list);
		return page;
	}
	/**
	 * �жϸ������Ƿ��Ѿ��Ĵ�
	 */
	public int getIfexit(String coId) {
		return ashesRecordMapper.getIfexit(coId);
	}
}
