package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.ye.ItemOrderMapper;
/**
 * 火化委托单服务项目
 * @author tsp
 *
 */
@Service
@Transactional
public class ItemOrderService {
	@Autowired
	private ItemOrderMapper itemOrderMapper;
	
	
	/**
	 * 根据条件删除收费记录，慎用慎用！！！
	 * @param map
	 * @return
	 */
	public int deleteItemOrderByMap(Map<String,Object> map){
		return itemOrderMapper.deleteItemByMap(map);
	}
	/**
	 * 添加火化委托单收费项目
	 * @param itemOrder
	 */
	public void addItemOrder(ItemOrder itemOrder){
		itemOrderMapper.saveItemOrder(itemOrder);
	}
	/**
	 * 删除火化委托单收费项目
	 * @param id
	 */
	public void deleteItemOrder(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	itemOrderMapper.deleteItemOrder(ids[i]);
	    }
	}
	/**
	 * 更新火化委托单收费项目
	 * @param itemOrder
	 */
	public void updateItemOrder(ItemOrder itemOrder){
		itemOrderMapper.updateItemOrder(itemOrder);
	}


	/**
	 * 获取火化委托单收费项目对象
	 * @param id
	 * @return
	 */
	public ItemOrder getItemOrderById(String id){
		ItemOrder itemOrder=itemOrderMapper.getItemOrderById(id);
		return itemOrder;
	}
	
	/**
	 * 根据条件获取火化委托单收费项目List
	 * 非挂账业务
	 * @param String name
	 */
	public List<ItemOrder> getDelegrateItemOrderList(Map<String,Object> paramMap){
		List<ItemOrder> list= itemOrderMapper.getdelegrateItemOrderList(paramMap);
		return list;
	}
	/**
	 * 委托业务收费使用：根据条件统计金额
	 * @param map
	 * @return
	 */
	public double getTotalByOrderNumber(Map<String,Object> map){
		return itemOrderMapper.getTotalByOrderNumber(map);
	}
	
	
	
	/**
	 * 根据条件获取火化委托单收费项目List
	 * @param String name
	 */
	public List<ItemOrder> getItemOrderList(Map<String,Object> paramMap){
		List<ItemOrder> list= itemOrderMapper.getItemOrderList(paramMap);
		return list;
	}
	/**
	 * 根据条件获取火化委托单收费项目List（新加历史）
	 * @param String name
	 */
	public List<ItemOrder> getItemOrderOldAndNew(Map<String,Object> paramMap){
		List<ItemOrder> list= itemOrderMapper.getItemOrderOldAndNew(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取火化委托单收费项目PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<ItemOrder> getItemOrderPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<ItemOrder> list=getItemOrderList(paramMap);
		PageInfo<ItemOrder> page = new PageInfo<ItemOrder>(list);
		return page; 
	}
	
	
	
	public void updateItemOrderNew(ItemOrder itemOrder){
		itemOrderMapper.updateItemOrderNew(itemOrder);
	}
	public void addItemOrderNew(ItemOrder itemOrder){
		itemOrderMapper.saveItemOrderNew(itemOrder);
	}
	public void deleteItemOrderNew(String id){
		itemOrderMapper.delItemOrderNew(id);
	}
	public ItemOrder findItemOrderById(String id){
		return itemOrderMapper.findItemOrderById(id);
	}
	public List<ItemOrder> getItemOrderListByType(Map<String, Object> paramMap) {
		return itemOrderMapper.getItemOrderListByType(paramMap);
	}
	/**
	 * 给服务项目添加布置信息
	 * @param io
	 */
	public void arrangeItem(ItemOrder io) {
		itemOrderMapper.arrangeItem(io);
	}
	public List<ItemOrder> getItemOrderListForNotice(Map<String, Object> map) {
		return itemOrderMapper.getItemOrderListForNotice(map);
	}
}
