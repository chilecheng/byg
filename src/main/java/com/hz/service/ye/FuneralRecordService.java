package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.ListFueralRecord;
import com.hz.mapper.ye.FuneralRecordMapper;


/*
 *  出殡礼仪 死者记录
 * @author czl
 * 
 */
@Service
//添加事物
@Transactional
public class FuneralRecordService {

	@Autowired
	private FuneralRecordMapper funeralRecordMapper;
	
	/**
	 * 获取所有满足条件的 火化委托单 及纸馆记录
	 * @param paramMap
	 * @param order
	 * @return
	 */
	public List<ListFueralRecord> getFuneralRecord(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);//给Sql增加 排序等 语句
		return funeralRecordMapper.findFuneralRecord(paramMap);
	}
	/**
	 * 获取告别厅记录列表  分页
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<ListFueralRecord> getFarewellRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<ListFueralRecord> list=funeralRecordMapper.findFuneralRecord(paramMap);
		PageInfo<ListFueralRecord> page=new PageInfo<ListFueralRecord>(list);
		return page;
	
	
		
	}
	/**
	 * 首页 查找礼仪出殡
	 * @param map
	 * @return
	 */
	public List<ListFueralRecord> getFuneralRecordList(Map<String,Object> map){
		return funeralRecordMapper.getFuneralRecordList(map);
	}
	public int getNumber(Map<String,Object> map){
		return funeralRecordMapper.getNumber(map);
	}
	public void updateNumber(ListFueralRecord record){
		funeralRecordMapper.updateNumber(record);
	}
	public void updateViewsFuneral(String id,byte one){
		funeralRecordMapper.updateViewsFuneral(id, one);
	}
}
