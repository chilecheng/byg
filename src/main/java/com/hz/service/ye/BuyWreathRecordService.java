package com.hz.service.ye;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Jeneration;
import com.hz.entity.ye.BuyWreathRecord;
import com.hz.entity.ye.BuyWreathRecordD;
import com.hz.mapper.ye.BuyWreathRecordMapper;
import com.hz.service.base.JenerationService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

/**
 * 花圈花篮订购
 * @author hw
 *
 */
@Service
@Transactional
public class BuyWreathRecordService {
	@Autowired
	private BuyWreathRecordMapper buyWreathRecordMapper;
	@Autowired
	private BuyWreathRecordDService buyWreathRecordDService;
	@Autowired
	private JenerationService jenerationService;
	/**
	 * 添加骨花圈花篮订购
	 * @param BuyWreathRecord
	 */
	public void addBuyWreathRecord(BuyWreathRecord buyWreathRecord){
		buyWreathRecordMapper.saveBuyWreathRecord(buyWreathRecord);
	}
	/**
	 * 删除花圈花篮订购
	 * @param id
	 */
	public void deleteBuyWreathRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			buyWreathRecordMapper.deleteBuyWreathRecord(ids[i]);
		}
	}
	/**
	 * 修改花圈花篮订购
	 * @param buyWreathRecord
	 */
	public void updateBuyWreathRecord(BuyWreathRecord buyWreathRecord){
		buyWreathRecordMapper.updateBuyWreathRecord(buyWreathRecord);
	}
	/**
	 * 获取花圈花篮订购
	 * @param id
	 * @return
	 */
	public BuyWreathRecord getBuyWreathRecordId(String id){
		return buyWreathRecordMapper.getBuyWreathRecordById(id);
	}
	/**
	 * 根据条件获取花圈花篮订购
	 * @param paramMap
	 * @return
	 */
	public List<BuyWreathRecord> getBuyWreathRecordList(Map<String, Object> paramMap){
		return buyWreathRecordMapper.getBuyWreathRecordList(paramMap);
	}
	public List<BuyWreathRecord> getListBuyWreathRecord(Map<String, Object> paramMap){
		return buyWreathRecordMapper.getListBuyWreathRecord(paramMap);
	}
	/**
	 * 获取花圈花篮订购列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<BuyWreathRecord> getBuyWreathRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<BuyWreathRecord> list=getBuyWreathRecordList(paramMap);
		PageInfo<BuyWreathRecord> page=new PageInfo<BuyWreathRecord>(list);
		return page;
		
	}
	public void savebuyWreathRecord(String bwrId, String dNameId, String[] tbody, String id,String ordernum, String contact, String createUserId, String buyer, Timestamp buyTime,
			String writer,String assiginUserId, String bname, String comment,String name) {
		BuyWreathRecord buyWreathRecord = new BuyWreathRecord();
		if(bwrId!=null&&!bwrId.equals("")){
			id = bwrId;
			buyWreathRecord = getBuyWreathRecordId(id);
		    Map<String, Object> maps = new HashMap<String, Object>();
		    maps.put("buyWreathRecordId", id);
		    List<BuyWreathRecordD> list = buyWreathRecordDService.getBuyWreathRecordDList(maps);
		    for(int j=0;j<list.size();j++){
		    	buyWreathRecordDService.deleteBuyWreathRecordD(list.get(j).getId());
		    }
		}else{
			
			buyWreathRecord.setId(id);
			buyWreathRecord.setCommissionOrderId(dNameId);
//			String dName = getString("dName");
		}
		for(int i=0;i<tbody.length;i++){
			BuyWreathRecordD buyWreathRecordD = new BuyWreathRecordD();
			String[] tds = tbody[i].split(",");
			buyWreathRecordD.setId(UuidUtil.get32UUID());
			buyWreathRecordD.setBuyWreathRecordId(id);
			buyWreathRecordD.setItemId(tds[1]);
			buyWreathRecordD.setNumber(Integer.parseInt(tds[3]));
			buyWreathRecordD.setTotal(Double.parseDouble(tds[4]));
			if(tds.length>5){
				buyWreathRecordD.setSelfCall(tds[5]);
				if(tds.length>6){
					buyWreathRecordD.setDeadCall(tds[6]);
					if(tds.length>7){
						buyWreathRecordD.setComment(tds[7]);
					}
				}
			}
			buyWreathRecordDService.addAshesRecordD(buyWreathRecordD);
		}
		buyWreathRecord.setdName(name);
		buyWreathRecord.setOrderNumber(ordernum);
		buyWreathRecord.setContact(contact);
		buyWreathRecord.setCreatUserId(createUserId);
		buyWreathRecord.setOrderUser(buyer);
//		String bt = getString("buyTime");
//		Timestamp buyTime = getTime("buyTime");
		buyWreathRecord.setCreatTime(buyTime);
		//书写人员
		buyWreathRecord.setWriteUserId(writer);
		//布置人员
		buyWreathRecord.setAssignUserId(assiginUserId);
		buyWreathRecord.setAgentUser(bname);
		//buyWreathRecord.setUnitBuyFlag(getByte("isComBuy"));
		buyWreathRecord.setComment(comment);
		//修改之后默认都给他 未收费
		buyWreathRecord.setPayFlag(Const.Pay_No);
		if(bwrId!=null&&!bwrId.equals("")){
			updateBuyWreathRecord(buyWreathRecord);
		}else {
			buyWreathRecord.setSerialNumber(jenerationService.getJenerationCode(Jeneration.HQHL_TYPE));
//			buyWreathRecord.setPayFlag(Const.Pay_No);
			addBuyWreathRecord(buyWreathRecord);
		}
		
	}
	/**
	 * 花圈花篮列表
	 * @param maps
	 * @return
	 */
	public List<BuyWreathRecord> getBuyWrethRecord(Map<String, Object> maps) {
		List<BuyWreathRecord> list=getBuyWreathRecordList(maps);
		return list;
	}
}
