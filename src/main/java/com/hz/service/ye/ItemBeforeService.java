package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.ye.ItemBeforeMapper;
import com.hz.mapper.ye.ItemOrderMapper;
/**
 * 丧葬用品更改之前
 * @author jgj
 *
 */
@Service
@Transactional
public class ItemBeforeService {
	@Autowired
	private ItemBeforeMapper itemBeforeMapper;
	
	/**
	 * 添加火化委托单收费项目
	 * @param itemOrder
	 */
	public void saveItemOrderBefore(ItemOrder itemOrder){
		itemBeforeMapper.saveItemOrderBefore(itemOrder);
	}
	/**
	 * 删除火化委托单收费项目
	 * @param id
	 */
	public void delItemOrderBefore(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	itemBeforeMapper.delItemOrderBefore(ids[i]);
	    }
	}
	/**
	 * 根据火化单ID删除
	 * @param id
	 */
	public void delItemOrderBeforeBycomId(String id){
		itemBeforeMapper.delItemOrderBeforeBycomId(id);
	}
	/**
	 * 更新火化委托单收费项目
	 * @param itemOrder
	 */
	public void updateItemOrderBefore(ItemOrder itemOrder){
		itemBeforeMapper.updateItemOrderBefore(itemOrder);
	}


	/**
	 * 获取火化委托单收费项目对象
	 * @param id
	 * @return
	 */
	public ItemOrder getItemOrderBeforeById(String id){
		ItemOrder itemOrder=itemBeforeMapper.getItemOrderBeforeById(id);
		return itemOrder;
	}
	
	/**
	 * 根据条件获取火化委托单收费项目List
	 * 非挂账业务
	 * @param String name
	 */
	public List<ItemOrder> getItemOrderBeforeList(Map<String,Object> paramMap){
		List<ItemOrder> list= itemBeforeMapper.getItemOrderBeforeList(paramMap);
		return list;
	}
}
