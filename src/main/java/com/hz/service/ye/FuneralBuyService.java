package com.hz.service.ye;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Jeneration;
import com.hz.entity.ye.BuyGoodsRecordD;
import com.hz.entity.ye.Buygood;
import com.hz.entity.ye.ItemChange;
import com.hz.entity.ye.ListFuneralOrderRecord;
import com.hz.entity.ye.ListFuneralOrderRecordDetail;
import com.hz.entity.ye.ListOrderRecordDetail;
import com.hz.entity.ye.SadGoods;
import com.hz.mapper.ye.FuneralBuyMapper;
import com.hz.service.base.ItemService;
import com.hz.service.base.JenerationService;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.socket.NoticeHandleService;
import com.hz.socket.SystemWebSocketHandler;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.ItemConst;
import com.hz.util.UuidUtil;

/**
 * 丧葬用品购买
 * 
 * @author lipengpeng
 *
 */
@Service
@Transactional
public class FuneralBuyService {

	@Autowired
	private FuneralBuyMapper funeralbuymapper;
	@Autowired
	JenerationService jenerationService;
	@Autowired
	private ItemService itemservice;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	DeptService deptService;
	@Autowired
	private SystemWebSocketHandler systemWebSocketHandler;
	@Autowired
	private NoticeHandleService noticeHandleService;
	@Autowired
	private ItemChangeService itemChangeService;
	
	

	/**
	 * 查找所有订单记录
	 * 
	 * @param paramMap
	 * @return
	 */
	public List<ListFuneralOrderRecord> findAllOrderlist(Map<String, Object> paramMap) {
		List<ListFuneralOrderRecord> list = funeralbuymapper.listAllOrderRecord(paramMap);
		return list;
	}
	/**
	 * 通过  火化委托单 id  找到 在丧葬用品中购买的 丧葬用品      (丧葬用品任务单)
	 * @gpf
	 */
	public List<Buygood>  findItemOrder(Map<String, Object> paramMap) {
		List<Buygood>  buygood = funeralbuymapper.findItemByOrderId(paramMap);	
		return buygood;
	}
	
	/*public List<Buygood>  findItemOrder(String commissionOrderId) {
		List<Buygood>  buygood = funeralbuymapper.findItemByOrderId(commissionOrderId);	
		return buygood;
	}*/
	
	public List<BuyGoodsRecordD> getGoodsRecordDListByBuyOrderId(String orderId){
		return funeralbuymapper.getGoodsRecordDListByBuyOrderId(orderId);
	}
	
	/**
	 * 收费科非委托业务查询 
	 * @param id
	 * @return
	 */
	public ListFuneralOrderRecord getBuyGoodsRecordFeeById(String id){
		ListFuneralOrderRecord funeralOrder=funeralbuymapper.getBuyGoodsRecordFeeById(id);
		return funeralOrder;
	}

	public PageInfo<ListFuneralOrderRecord> getAllRecordPageInfo(Map<String, Object> paramMap, int pageNum,
			int pageSize, String order) {
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<ListFuneralOrderRecord> list = findAllOrderlist(paramMap);
		PageInfo<ListFuneralOrderRecord> page = new PageInfo<ListFuneralOrderRecord>(list);
		return page;
	}
	public List<ListOrderRecordDetail> getBuyGoodsRecordFee(Map<String,Object> paramMap){
		return funeralbuymapper.getBuyGoodsRecordFee(paramMap);
	}
	/**
	 * 插入订单记录
	 * 
	 * @param listfuneralorderrecord
	 */
	public void addOrderCount(ListFuneralOrderRecord listfuneralorderrecord) {
		funeralbuymapper.insertOrder(listfuneralorderrecord);
	}

	/**
	 * 插入订单记录项
	 * 
	 * @param ld
	 */
	public void addOrderDetail(ListFuneralOrderRecordDetail ld) {
		funeralbuymapper.insertOrderDetail(ld);
	}

/*	public List<ListFuneralOrderRecord> findFareWellAndMourningById(String id) {
		return funeralbuymapper.findFareWellAndMourningById(id);
	}*/

	public List<ListFuneralOrderRecord> findOrderRecordById(String id) {
		return funeralbuymapper.findOrderRecordById(id);
	}

	public void updateOrderRecord(ListFuneralOrderRecord listfuneralorderrecord) {
		funeralbuymapper.updateOrderRecord(listfuneralorderrecord);
	}

	public void deleteOrderRecordById(String id) {
		funeralbuymapper.deleteOrderRecordById(id);
	}

	public List<ListOrderRecordDetail> findOrderDetailById(String id) {
		return funeralbuymapper.findOrderDetailById(id);
	}

	public List<SadGoods> getSadGoodsList(Map<String, Object> paramMap) {
		return funeralbuymapper.getSadGoodsList(paramMap);
	}

	public List<String> getSadGoodsOption(List<String> ids, boolean isAll) {

		List<String> op = new ArrayList<String>();
		for (int j = 0; j < ids.size(); j++) {
			List<Object[]> list = new ArrayList<Object[]>();
			List<SadGoods> listSadGoods = new ArrayList<SadGoods>();
			listSadGoods = funeralbuymapper.getSadGoodsList(null);
			for (int i = 0; i < listSadGoods.size(); i++) {
				SadGoods sg = listSadGoods.get(i);
				list.add(new Object[] { sg.getId(), sg.getName() });
			}

			op.add(DataTools.getOptionByList(list, ids.get(j), isAll));

		}
		return op;

	}

	public void deleteFirstStepById(String id) {
		funeralbuymapper.deleteFirstStepById(id);
	}

		/** 丧葬用品购买保存  */
	/*
	 * 丧葬用品购买保存
	 */
	public void insertOrderCount(ListFuneralOrderRecord listfuneralorderrecord,
			 String[] tbody, String s) {
		listfuneralorderrecord.setFlowId(jenerationService.getJenerationCode(Jeneration.SZYP_TYPE));
		addOrderCount(listfuneralorderrecord);
		byte funeralByte=0;
		for (int i = 0; i < tbody.length; i++) {
			ListFuneralOrderRecordDetail ld = new ListFuneralOrderRecordDetail();
			String[] tds = tbody[i].split(",");
			ld.setId(UuidUtil.get32UUID());
			ld.setGoodsRecordId(s);
			ld.setItemId(tds[1]);
			ld.setNum(tds[3]);
			ld.setTotal(tds[4]);
			ld.setIsCredit(tds[5]);
			ld.setRemarkText(tds[6]);
			addOrderDetail(ld);
			
			byte sort =itemservice.getItemById(tds[1]).getSort();
			if(sort==ItemConst.BaoBuHe_Sort || sort== ItemConst.XiaoHuaQuan_Sort ||
    				sort == ItemConst.BaoHuJi_Sort || sort == ItemConst.GuHuiHe_Sort || sort == ItemConst.HongXiaoDai_Sort ||
    				sort == ItemConst.ShouBei_Sort || sort == ItemConst.ShouYi_Sort ||sort == ItemConst.YuSan_Sort){
    			funeralByte = 1;
			}
			
		}
		if(funeralByte ==1){
			Date date = DateTools.getThisDate();
			 Date takeTime = DateTools.getDate(listfuneralorderrecord.getTakeawayTime()) ;
			 boolean bool	=userNoticeService.ifDateExit(Const.NoticeType_FuneralTask, takeTime);
			 if(bool){
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FuneralTask, takeTime, 1);
			}else{	
					userNoticeService.addNoticeOfDate(Const.NoticeType_FuneralTask,  takeTime);
			}	 
			if(date.equals(takeTime)){
				String deptId = deptService.getDeptIdByName("一科");
				systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("funeralNumber"));
			}
			
		}		
		
	
		
	}
  // 修改 保存
	public void updateOrderCount(ListFuneralOrderRecord listfuneralorderrecord, String buygoodsid, String[] tbody,String ordernum) {
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("id",buygoodsid );
		//获取更新前 是否有满足消息提醒项
		boolean  oldbool = ifexitSort(buygoodsid);
		Date oldTakeawayTime=null;
			//更改前的领用时间
			Timestamp takeawayTime =  funeralbuymapper.getTakeawayTimeById(buygoodsid);
			if(takeawayTime !=null){
				oldTakeawayTime = DateTools.getDate(takeawayTime);
			}
		//先取出之前的记录，用于做判断是否更改丧葬用品记录
		List<BuyGoodsRecordD> oldList= getGoodsRecordDListByBuyOrderId(ordernum);
		
		/* 第一步,删除已有的 */
		deleteFirstStepById(buygoodsid);
		/* 第二步，插入现有的 */
		updateOrderRecord(listfuneralorderrecord);	
		//更改后的领用时间
		//在这里加个判断,要不没有领用时间修改保存的时候 会报错
		Date takeTime =null;
		if(listfuneralorderrecord.getTakeawayTime()!=null && !"".equals(listfuneralorderrecord.getTakeawayTime())){
			takeTime = DateTools.getDate(listfuneralorderrecord.getTakeawayTime()) ;
		}
		// 获取更新后 新的 用品项目是否 需要消息提醒 	
		boolean  newbool = ifexitSort(buygoodsid);
		Date oldday =  DateTools.getDate(takeawayTime);
	
		//如果日期天数发生变更执行下程序
		if(takeTime!=oldday){
				//日期发生变更时，先对旧日期的消息进行变更
			if(oldbool){
				 noticeHandleService.updateNoticeToday(takeawayTime, Const.NoticeType_FuneralTask);
			}
			//存放最新的丧葬记录
			List<ListFuneralOrderRecordDetail> ldList=new ArrayList<ListFuneralOrderRecordDetail>();
				
			byte funeralByte=0;	
			for (int i = 0; i < tbody.length; i++) {
				ListFuneralOrderRecordDetail ld = new ListFuneralOrderRecordDetail();
				String[] tds = tbody[i].split(",");
				ld.setId(UuidUtil.get32UUID());
				ld.setGoodsRecordId(buygoodsid);//
				ld.setItemId(tds[1]);// 1???
				ld.setNum(tds[3]);//
				ld.setTotal(tds[4]);//
				ld.setIsCredit(tds[5]);
				if(tds.length>=7){//保存的时候可能会因为 备注是空的，分割成数组时自动省略，会出错，加个判断
					ld.setRemarkText(tds[6]);
				}
				addOrderDetail(ld);
				
				ldList.add(ld);
				byte sort =itemservice.getItemById(tds[1]).getSort();
				if(sort==ItemConst.BaoBuHe_Sort || sort== ItemConst.XiaoHuaQuan_Sort ||
	    				sort == ItemConst.BaoHuJi_Sort || sort == ItemConst.GuHuiHe_Sort || sort == ItemConst.HongXiaoDai_Sort ||
	    				sort == ItemConst.ShouBei_Sort || sort == ItemConst.ShouYi_Sort ||sort == ItemConst.YuSan_Sort){
	    			funeralByte = 1;
				}
				
			}
			//以下将两个不同时段的记录 按sort统计，便于之后判断	
			Map<String,Double> newmap=new HashMap<String,Double>();
			List<String> sortList=new ArrayList<String>();
			for(int i=0;i<ldList.size();i++){//按sort累加
				ldList.get(i).getItemId();
				String sort=String.valueOf(itemservice.getItemById(ldList.get(i).getItemId()).getSort());//此处无法直接获取item,只能先通过itemID
				double newTotal=Double.valueOf(ldList.get(i).getTotal());
				
				if(sortList.contains(sort)==true){//判断，若之前map有的，则累加
					newmap.put(sort, newmap.get(sort)+newTotal);
				}else{//否则 创建及 添加到list中
					sortList.add(sort);
					newmap.put(sort, newTotal);
				}
			}
			Map<String,Double> beforeMap=new HashMap<String,Double>();
			List<String> sortBeforeList=new ArrayList<String>();
  			for(int j=0;j<oldList.size();j++){//同上
				String sort=String.valueOf(itemservice.getItemById(oldList.get(j).getItemId()).getSort());
				double beforeTotal=oldList.get(j).getTotal();
				if(sortBeforeList.contains(sort)==true){//判断，若之前map有的，则累加
					beforeMap.put(sort, beforeMap.get(sort)+beforeTotal);
				}else{//否则 创建及 添加到list中
					sortBeforeList.add(sort);
					beforeMap.put(sort, beforeTotal);
				}
			}
			
			List<ItemChange> itemChangeList=new ArrayList<ItemChange>();//记录所有更改的地方记录
			if(newmap.size()>=beforeMap.size() && newmap.size()>0){//以多的map来比较少的map
				Set<String> set=newmap.keySet();
				for(String in:set){
					double a=newmap.get(in);
					double b=0;
					if(beforeMap.containsKey(in)){
						b=beforeMap.get(in);
					}
					if(a!=b){//不相同，即改变了 commission_id为id
						ItemChange itemChange=new ItemChange();
						itemChange.setBuyOrderId(ordernum);//order_id
						itemChange.setSort(in);
						itemChange.setId(UuidUtil.get32UUID());
						itemChangeList.add(itemChange);
					}
				}
			}else if(beforeMap.size()>=newmap.size() && beforeMap.size()>0){
				Set<String> set=beforeMap.keySet();				
				for(String in:set){
					double a=beforeMap.get(in);
					double b=0;
					if(newmap.containsKey(in)){
						b=newmap.get(in);
					}
					if(a!=b){//不相同，即改变了 commission_id为id
						ItemChange itemChange=new ItemChange();
						itemChange.setBuyOrderId(ordernum);
						itemChange.setSort(in);
						itemChange.setId(UuidUtil.get32UUID());
						itemChangeList.add(itemChange);
					}
				}
			}
			//同样，改变的记录也是先清空后保存
			itemChangeService.delItemChangeByBuyId(ordernum);
			for(int i=0;i<itemChangeList.size();i++){//将改变的记录 保存到y_item_change表中
				itemChangeService.saveItemChange(itemChangeList.get(i));
			}
			if(itemChangeList.size()>0){
				Date dateTime=null;
				dateTime=takeTime;
				Date today=DateTools.getThisDate();
				
				//以下是消息提醒功能
				if (userNoticeService.ifDateExit(Const.NoticeType_FuneralTask,dateTime)) {
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FuneralTask, dateTime, 1);
				} else if(!userNoticeService.ifDateExit(Const.NoticeType_FuneralTask,dateTime)) {
					userNoticeService.addNoticeOfDate(Const.NoticeType_FuneralTask, dateTime);
				}
				int i=DateTools.compareSqlDate(today, dateTime);
				if (i==0) {//是否需当天 是则发送通知
					String deptId = deptService.getDeptIdByName("一科");
					systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("funeralNumber"));
				}
			}
				
				
				Date date = DateTools.getThisDate();	
				if(funeralByte ==1){	
					 boolean bool	=userNoticeService.ifDateExit(Const.NoticeType_FuneralTask, takeTime);
					 //设计思路  首先进行判断  领用时间是否发生变更， 如果变更 则旧的number 相应的减一 ，新的记录 根据判断进行插入或者更新操作
					 //如果新的领用时间 和当日时间相同  给用户发消息提醒
					 //
					 if(oldTakeawayTime !=null){
							 if(oldTakeawayTime.equals(takeTime)){	 
										if(bool){
												userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FuneralTask, takeTime, 1);	}
										else{	
												userNoticeService.addNoticeOfDate(Const.NoticeType_FuneralTask,  takeTime); }						 	 
										if(date.equals(takeTime)){
											String deptId = deptService.getDeptIdByName("一科");
											systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("funeralNumber"));
										}	 
							 }
					 }	
				}	
		}else{
			//更前前后日期相同 ，且 更改前  sort项有消息  更改后没有消息
			if(oldbool && !newbool){
				 noticeHandleService.updateNoticeToday(takeawayTime, Const.NoticeType_FuneralTask);
			}
			//更改前 没有消息提醒，更改后需要有的
			if(!oldbool && newbool){
				Date date = DateTools.getThisDate();	
				 boolean bool	=userNoticeService.ifDateExit(Const.NoticeType_FuneralTask, takeTime);
								if(bool){
										userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FuneralTask, takeTime, 1);	}
								else{	
										userNoticeService.addNoticeOfDate(Const.NoticeType_FuneralTask,  takeTime); }						 	 
								if(date.equals(takeTime)){
									String deptId = deptService.getDeptIdByName("一科");
									systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("funeralNumber"));
								}	 
					 }
			
			
		}
				/*else{
					if(oldTakeawayTime !=null){
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FuneralTask, oldTakeawayTime, -1); }
				}*/
		
	}
	/**
	 * 修改丧葬用品核对列表状态
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
	String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
		//	CommissionOrder comm=getBuyGoodsRecordFeeById(ids[i]);
			ListFuneralOrderRecord record = getBuyGoodsRecordFeeById(id);
			if(isdel==Const.Yes_treated){
				record.setDealIsdel(Const.Yes_treated);
			}else if(isdel==Const.No_treated){
				record.setDealIsdel(Const.No_treated);
			}else if(isdel==Const.Yes_usered){
				record.setDealIsdel(Const.Yes_usered);
			}
			updateOrderRecord(record);
		}
	}
	
	public boolean ifexitSort(String orderId){
		List<Byte> list= funeralbuymapper.getSortByOrderId(orderId);
		byte funeralByte=0;	
		for(int i=0;i<list.size();i++){
			byte sort = list.get(i);
			if(sort==ItemConst.BaoBuHe_Sort || sort== ItemConst.XiaoHuaQuan_Sort ||
					sort == ItemConst.BaoHuJi_Sort || sort == ItemConst.GuHuiHe_Sort || sort == ItemConst.HongXiaoDai_Sort ||
					sort == ItemConst.ShouBei_Sort || sort == ItemConst.ShouYi_Sort ||sort == ItemConst.YuSan_Sort){
				funeralByte = 1;
				break;
			}
		}
		if(funeralByte==0){
			return false;
		}
		else{
			return true;
		}

	}
	
	
	
	

}
