package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.BaseReductionD;
import com.hz.mapper.ye.BaseReductionDMapper;
import com.hz.util.UuidUtil;
/**
 * 基本减免申请项目
 * @author rgy
 *
 */
@Service
@Transactional
public class BaseReductionDService {
	@Autowired
	private BaseReductionDMapper baseReductionDMapper;
	/**
	 * 添加基本减免申请项目
	 * @param baseReductionD
	 */
	public void addBaseReductionD(BaseReductionD baseReductionD){
		baseReductionDMapper.saveBaseReductionD(baseReductionD);
	}
	/**
	 * 删除基本减免申请项目
	 * @param id
	 */
	public void deleteBaseReductionD(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			baseReductionDMapper.deleteBaseReductionD(id);
		}
	}
	/**
	 * 修改基本减免申请项目
	 * @param baseReduction
	 */
	public void updateBaseReductionD(BaseReductionD baseReductionD){
		baseReductionDMapper.updateBaseReductionD(baseReductionD);
	}
	/**
	 * 获取基本减免申请项目
	 * @param id
	 * @return
	 */
	public BaseReductionD getBaseReductionDId(String id){
		return baseReductionDMapper.getBaseReductionDById(id);
	}
	/**
	 * 根据条件获取基本减免申请项目list
	 * @param paramMap
	 * @return
	 */
	public List<BaseReductionD> getBaseReductionDList(Map<String, Object> paramMap){
		return baseReductionDMapper.getBaseReductionDList(paramMap);
	}
	/**
	 * 收费科
	 * 根据条件获取基本减免申请项目list
	 * @param paramMap
	 * @return
	 */
	public List<BaseReductionD> getBaseReductionDFList(Map<String, Object> paramMap){
		return baseReductionDMapper.getBaseReductionDFeeList(paramMap);
	}
	/**
	 * 根据业务订单流水号 统计金额
	 * @param orderNumber
	 * @return
	 */
	public double getbaseTotalByOrderNumber(String orderNumber){
		return baseReductionDMapper.getbaseTotalByOrderNumber(orderNumber);
	}
	/**
	 * 根据条件 统计基本减免金额
	 * @param map
	 * @return
	 */
	public double getBaseTotal(Map<String,Object> map){
		return baseReductionDMapper.getBaseTotal(map);
	}
	/**
	 * 获取基本减免申请项目列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<BaseReductionD> getBaseReductionDPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<BaseReductionD> list=getBaseReductionDList(paramMap);
		
		PageInfo<BaseReductionD> page=new PageInfo<BaseReductionD>(list);
		return page;
		
	}
	/**
	 * 更新基本减免保存
	 * @param id
	 * @param baseReductionD
	 */
	public void addOrUpdate(String id, BaseReductionD baseReductionD) {
		if (id==null||id.equals("")) {
			baseReductionD.setId(UuidUtil.get32UUID());
			addBaseReductionD(baseReductionD);
		}else {
			updateBaseReductionD(baseReductionD);
		}
	}
	/**
	 * 根据订单流水号 修改pay_time值
	 * @param paramMap
	 */
	public void updateBasePayTime(Map<String,Object> paramMap){
		baseReductionDMapper.updateBasePayTime(paramMap);
	}
}
