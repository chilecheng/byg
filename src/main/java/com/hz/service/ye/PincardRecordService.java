package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.PincardRecord;
import com.hz.mapper.ye.PincardRecordMapper;

/**
 * 销卡记录信息
 * @author hw
 *
 */
@Service
@Transactional
public class PincardRecordService {
	@Autowired
	private PincardRecordMapper pincardRecordMapper;
	/**
	 * 添加销卡记录
	 * @param PincardRecord
	 */
	public void addPincardRecord(PincardRecord pincardRecord){
		pincardRecordMapper.savePincardRecord(pincardRecord);
	}
	/**
	 * 删除销卡记录
	 * @param id
	 */
	public void deletePincardRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			pincardRecordMapper.deletePincardRecord(id);
		}
	}
	/**
	 * 修改销卡记录
	 * @param pincardRecord
	 */
	public void updateAshesRecord(PincardRecord pincardRecord){
		pincardRecordMapper.updatePincardRecord(pincardRecord);
	}
	/**
	 * 获取销卡记录对象
	 * @param id
	 * @return
	 */
	public PincardRecord getPincardRecordById(String id){
		return pincardRecordMapper.getPincardRecordById(id);
	}

	/**
	 * 根据条件获取销卡记录list
	 * @param paramMap
	 * @return
	 */
	public List<PincardRecord> getPincardRecordList(Map<String,Object> paramMap){
		List<PincardRecord> list=pincardRecordMapper.getPincardRecordList(paramMap);
		
		return list;
	}
	/**
	 * 获取销卡记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<PincardRecord> getPincardRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<PincardRecord> list=getPincardRecordList(paramMap);
	
		PageInfo<PincardRecord> page=new PageInfo<PincardRecord>(list);
		return page;
		
	}
}
