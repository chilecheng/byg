package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.HardReductionD;
import com.hz.mapper.ye.HardReductionDMapper;
import com.hz.util.UuidUtil;
/**
 * 困难减免申请项目
 * @author rgy
 *
 */
@Service
@Transactional
public class HardReductionDService {
	@Autowired
	private HardReductionDMapper hardReductionDMapper;
	/**
	 * 添加困难减免申请项目
	 * @param hardReductionD
	 */
	public void addHardReductionD(HardReductionD hardReductionD){
		hardReductionDMapper.saveHardReductionD(hardReductionD);
	}
	/**
	 * 删除困难减免申请项目
	 * @param id
	 */
	public void deleteHardReductionD(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			hardReductionDMapper.deleteHardReductionD(id);
		}
	}
	/**
	 * 修改困难减免申请项目
	 * @param baseReduction
	 */
	public void updateHardReductionD(HardReductionD hardReductionD){
		hardReductionDMapper.updateHardReductionD(hardReductionD);
	}
	/**
	 * 获取困难减免申请项目
	 * @param id
	 * @return
	 */
	public HardReductionD getHardReductionDId(String id){
		return hardReductionDMapper.getHardReductionDById(id);
	}
	/**
	 * 根据条件获取困难减免申请list
	 * @param paramMap
	 * @return
	 */
	public List<HardReductionD> getHardReductionDList(Map<String, Object>paramMap){
		return hardReductionDMapper.getHardReductionDList(paramMap);
	}
	/**
	 * 用于  委托业务收费 困难减免
	 * 根据条件获取困难减免申请list
	 * @param paramMap
	 * @return
	 */
	public List<HardReductionD> getHardReductionDFeeList(Map<String, Object>paramMap){
		return hardReductionDMapper.getHardReductionDFeeList(paramMap);
	}
	/**
	 * 根据业务订单流水号 统计金额
	 * @param orderNumber
	 * @return
	 */
	public double gethardTotalByOrderNumber(String orderNumber){
		return hardReductionDMapper.gethardTotalByOrderNumber(orderNumber);
	}
	/**
	 * 根据条件 统计困难减免金额
	 * @param map
	 * @return
	 */
	public double getHardTotal(Map<String,Object> map){
		return hardReductionDMapper.getHardTotal(map);
	}
	
	/**
	 * 获取困难减免申请列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<HardReductionD> getHardReductionDPageInfo(Map<String, Object>paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<HardReductionD> list=getHardReductionDList(paramMap);
		PageInfo<HardReductionD> page=new PageInfo<HardReductionD>(list);
		return page;
		
	}
	/**
	 * 更新困难减免保存
	 * @param id
	 * @param hardReductionD
	 */
	public void addOrUpdate(String id, HardReductionD hardReductionD) {
		if (id==null||id.equals("")) {
			hardReductionD.setId(UuidUtil.get32UUID());
			addHardReductionD(hardReductionD);
		}else {
			updateHardReductionD(hardReductionD);
		}
	}
	/**
	 * 根据业务订单流水号 修改困难减免扣除时间
	 * @param paramMap
	 */
	public void updateHardPayTime(Map<String,Object> paramMap){
		hardReductionDMapper.updateHardPayTime(paramMap);
	}
}
