package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.FillCardRecord;
import com.hz.entity.ye.ListCardRecord;
import com.hz.mapper.ye.RecardMapper;

/**
 * 
 * @author lipengpeng
 *
 */
@Service
//事务
@Transactional
public class RecardService {
	
	@Autowired
	private RecardMapper recardmapper;
	
	/**
	 * 插入补卡记录
	 * @param fillCardRecord
	 */
	public void addFillCard(FillCardRecord fillCardRecord)
	{
		recardmapper.insertFillCard(fillCardRecord);
	}
	/**
	 * 查找所有有卡死者记录
	 * @return
	 */
	public List<ListCardRecord> findCountlist(Map<String,Object> paramMap)
	{
		List<ListCardRecord> list=recardmapper.listAllCardCount(paramMap);
		return list;
	}
	
//	分页查询
	public PageInfo<ListCardRecord> getAllRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<ListCardRecord> list=findCountlist(paramMap);
		PageInfo<ListCardRecord> page = new PageInfo<ListCardRecord>(list);
		return page; 
	}
	
	public void updateCommissionCard(FillCardRecord fcr)
	{
		recardmapper.updateCommissionCard(fcr);
	}
	public void addFillCardAndUpdateCommissionCard(FillCardRecord fillCardRecord){
		addFillCard(fillCardRecord);
		updateCommissionCard(fillCardRecord);
	}
	public int getCountCardCode(String cardCode){
		return  recardmapper.getBoolCardCode(cardCode);
	}


}
