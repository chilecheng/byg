package com.hz.service.ye;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.BaseReduction;
import com.hz.mapper.ye.BaseReductionMapper;
import com.hz.util.UuidUtil;
/**
 * 基本减免申请
 * @author rgy
 *
 */
@Service
@Transactional
public class BaseReductionService {
	@Autowired
	private BaseReductionMapper baseReductionMapper;
	/**
	 * 添加基本减免申请
	 * @param baseReduction
	 */
	public void addBaseReduction(BaseReduction baseReduction){
		baseReductionMapper.saveBaseReduction(baseReduction);
	}
	/**
	 * 删除基本减免申请
	 * @param id
	 */
	public void deleteBaseReduction(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			baseReductionMapper.deleteBaseReduction(id);
		}
	}
	/**
	 * 修改基本减免申请
	 * @param baseReduction
	 */
	public void updateBaseReduction(BaseReduction baseReduction){
		baseReductionMapper.updateBaseReduction(baseReduction);
	}
	/**
	 * 获取基本减免申请
	 * @param id
	 * @return
	 */
	public BaseReduction getBaseReductionId(String id){
		return baseReductionMapper.getBaseReductionById(id);
	}
	/**
	 * 根据条件获取基本减免申请list
	 * @param paramMap
	 * @return
	 */
	public List<BaseReduction> getBaseReductionList(Map<String, Object>paramMap){
		return baseReductionMapper.getBaseReductionList(paramMap);
	}
	/**
	 * 获取基本减免申请列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<BaseReduction> getBaseReductionPageInfo(Map<String, Object>paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<BaseReduction> list=getBaseReductionList(paramMap);
		PageInfo<BaseReduction> page=new PageInfo<BaseReduction>(list);
		return page;
		
	}
	/**
	 * 更新基本减免申请保存
	 * @param id
	 * @param baseReduction
	 */
	public void addOrUpdate(String id, BaseReduction baseReduction) {
		if (id==null||id.equals("")) {
			baseReduction.setId(UuidUtil.get32UUID());
			addBaseReduction(baseReduction);
		}else {
			updateBaseReduction(baseReduction);
		}
	}
}
