package com.hz.service.ye;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.system.User;
import com.hz.entity.ye.ComOrSpeFuneralRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FireCheckInInfo;
import com.hz.entity.ye.FireFurnaceInfo;
import com.hz.entity.ye.FireWorkerInfo;
import com.hz.entity.ye.PaperInfo;
import com.hz.entity.ye.PolitenessInfo;
import com.hz.mapper.ye.FireCheckInMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;

/**
 * 三科火化登记
 * 
 * @author lipengpeng
 *
 */
@Service
@Transactional
public class FireCheckInService {

	@Autowired
	FireCheckInMapper firecheckinmapper;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private FarewellRecordService farewellRecordService;

	/**
	 * 根据卡号查死者基本信息
	 * 
	 * @param cardCode
	 * @return
	 */
	public FireCheckInInfo getBasicInfoByCard(String cardCode) {
		return firecheckinmapper.getBasicInfoByCard(cardCode);
	}

	/**
	 * 根据卡号查询纸棺和出殡礼仪两项信息
	 * 
	 * @param cardCode
	 * @param PaperOrPoliteness
	 * @return
	 */
	public PaperInfo getPaperInfoByCard(String cardCode, String Paper) {
		return firecheckinmapper.getPaperInfoByCard(cardCode, Paper);
	}

	public PolitenessInfo getPolitenessInfoByCard(String cardCode, int sort) {
		return firecheckinmapper.getPolitenessInfoByCard(cardCode, sort);
	}

	public List<FireWorkerInfo> findAllFireWorker(String firename) {
		return firecheckinmapper.findAllFireWorker(firename);
	}

	public String getFireWorkerOption(String id, boolean isAll) {
		List<Object[]> list = new ArrayList<Object[]>();
		List<FireWorkerInfo> listFireWorker = firecheckinmapper.findAllFireWorker("火化工");
		for (int i = 0; i < listFireWorker.size(); i++) {
			FireWorkerInfo fireworkerinfo = (FireWorkerInfo) listFireWorker.get(i);
			list.add(new Object[] { fireworkerinfo.getUid(), fireworkerinfo.getUname() });
		}
		String option = DataTools.getOptionByList(list, id, isAll);
		return option;

	}

	public List<FireFurnaceInfo> findAllSpecialOrCommonFurnace(String furnacename) {
		return firecheckinmapper.findAllSpecialOrCommonFurnace(furnacename);
	}

	public String getAllSpecialOrCommonFurnaceOption(String id, boolean isAll, String furnacename) {
		List<Object[]> list = new ArrayList<Object[]>();
		List<FireFurnaceInfo> listFireFurnace = firecheckinmapper.findAllSpecialOrCommonFurnace(furnacename);
		for (int i = 0; i < listFireFurnace.size(); i++) {
			FireFurnaceInfo firefurnaceinfo = (FireFurnaceInfo) listFireFurnace.get(i);
			list.add(new Object[] { firefurnaceinfo.getId(), firefurnaceinfo.getName() });
		}
		String option = DataTools.getOptionByList(list, id, isAll);
		return option;
	}

	public String findFurnaceIdBycommissionId(String cid) {
		return firecheckinmapper.findFurnaceIdBycommissionId(cid);
	}

	public void insertCommonOrSpecialFurnaceRecord(ComOrSpeFuneralRecord csfr) {
		firecheckinmapper.insertCommonOrSpecialFurnaceRecord(csfr);
	}

	/*public void updateNotChangeFurnaceRecord(ComOrSpeFuneralRecord csfr) {
		firecheckinmapper.updateNotChangeFurnaceRecord(csfr);
	}*/
	//备选
	public void updateNotChangeFurnaceRecord2(ComOrSpeFuneralRecord cr) {
		firecheckinmapper.updateNotChangeFurnaceRecord2(cr);
	}
	/*public void updateChangeFurnaceRecord(ComOrSpeFuneralRecord csfr) {
		firecheckinmapper.updateChangeFurnaceRecord(csfr);
	}*/
//	/备选
	public void updateChangeFurnaceRecord2(ComOrSpeFuneralRecord cr) {
		firecheckinmapper.updateChangeFurnaceRecord2(cr);
	}
	
	public String findOriginalIdByFurnaceAndCommission(ComOrSpeFuneralRecord csfr) {
		return firecheckinmapper.findOriginalIdByFurnaceAndCommission(csfr);
	}

	public void updateCommissionFlagById(Map<String, Object> map) {
		firecheckinmapper.updateCommissionFlagById(map);
	}

	public ComOrSpeFuneralRecord findFurnaceInfoBycommissionId(String cid) {
		return firecheckinmapper.findFurnaceInfoBycommissionId(cid);
	}

	/**
	 * 特约炉转普通炉
	 */
	public void tyChangePt(String type, ComOrSpeFuneralRecord cr, String cid, String fireid, String funeralNum, String remark,
			String fireworker, Timestamp cremationtime, Timestamp fireStartTime, User u, String recordId) {
		
		cr.setId(recordId);
		cr.setChangeFlag(Const.ChangeFlag_Yes);
		cr.setFlag(Const.IsFlag_No);
		cr.setCommissionOrderId(cid);
//		cr.setFurnaceId(fireid);// 旧的炉号
//		String originalId = findOriginalIdByFurnaceAndCommission(cr);
		updateChangeFurnaceRecord2(cr);
		// **********************以上为update旧的记录********************************
		// **********************以下为insert新的记录********************************
		cr.setId(UuidUtil.get32UUID());
		cr.setFurnaceId(funeralNum);
		cr.setComment(remark);
		cr.setWorkerId(fireworker);
		if ("change".equals(type)) {
			cr.setFlag(Const.IsFlag_Yse);
		} else {
			cr.setFlag(Const.IsFlag_Wc);
		}
		cr.setChangeFlag(Const.ChangeFlag_No);
		cr.setOrderTime(cremationtime);
		cr.setBeginTime(fireStartTime);
		cr.setCreateUserId(u.getUserId());
		cr.setCreateTime(DateTools.dateToTimestamp(new java.util.Date()));
		cr.setOriginalId(recordId);//
		insertCommonOrSpecialFurnaceRecord(cr);
		
	}

	/**
	 * 根据卡号拿到furnace_record中的非转炉记录ID
	 */
	public String getRecordIdByCardcode(Map<String, Object> map) {
		return firecheckinmapper.getRecordIdByCardcode(map);
	}

	public void insertOrUpdate(String spe_or_com_flag, ComOrSpeFuneralRecord cr, String cid, String fireid, String funeralNum, String remark,
			String fireworker, Timestamp cremationtime, Timestamp fireStartTime, User u, String recordId) {
		if (spe_or_com_flag.equals("true")) {
			if (!fireid.equals(funeralNum))// 特约炉转炉，需要update和insert
			{	
				tyChangePt(null,cr,cid,fireid,funeralNum,remark,fireworker,cremationtime,fireStartTime,u,recordId);
			} else// 未转炉,只需要update
			{
				cr.setId(recordId);
				cr.setOrderTime(cremationtime);
				cr.setChangeFlag(Const.ChangeFlag_No);
				cr.setFlag(Const.IsFlag_Wc);
				cr.setWorkerId(fireworker);
				cr.setBeginTime(fireStartTime);
				cr.setComment(remark);
				cr.setCreateUserId(u.getUserId());
				cr.setFurnaceId(funeralNum);// 特约炉入炉这两项可以省略?
				updateNotChangeFurnaceRecord2(cr);
			}
		} else {
			cr.setId(recordId);
			cr.setFurnaceId(funeralNum);
			cr.setComment(remark);
			cr.setOrderTime(cremationtime);
			cr.setWorkerId(fireworker);
			cr.setFlag(Const.IsFlag_Wc);
			cr.setChangeFlag(Const.ChangeFlag_No);
			cr.setBeginTime(fireStartTime);
			cr.setCreateUserId(u.getUserId());
			cr.setOriginalId(null);
			updateNotChangeFurnaceRecord2(cr);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cremationFlag", Const.furnace_flag_Yes);
		map.put("furnaceId", funeralNum);
		map.put("cid", cid);
		//更改告别厅状态为清场状态
		FarewellRecord farewellRecord=farewellRecordService.getFarewellRecordByOrderId(cid);
		if(farewellRecord!=null){
			farewellRecord.setFlag(Const.IsFlag_QC);
			farewellRecordService.updateFarewellRecord(farewellRecord);
		}
		//火化委托单状态改为已火化未打印
		commissionOrderService.updateCommissionOrderScheduleFlag(cid, Const.schedule_flag_3);
		updateCommissionFlagById(map);// 更改火化标志和furnaceId
	}

	/**
	 * 业务三科过来的
	 */
	public void insertOrUpdate2(String ptrulu, String changeto, String type, String spe_or_com_flag, ComOrSpeFuneralRecord cr,
			String cid, String fireid, String funeralNum, String remark, String fireworker, Timestamp cremationtime,
			Timestamp fireStartTime, User u, String recordId) {
		if (spe_or_com_flag.equals("true")) {
			if (!fireid.equals(funeralNum))// 特约炉转炉，需要update和insert
			{
				tyChangePt(type,cr,cid,fireid,funeralNum,remark,fireworker,cremationtime,fireStartTime,u,recordId);
			} else// 未转炉,只需要update
			{
				if (type.equals("change")) {//是转炉那边进来的 直接跳过 
				} else {//入炉那边进来的 则进行入炉操作
					cr.setId(recordId);
					cr.setChangeFlag(Const.ChangeFlag_No);
					cr.setFlag(Const.IsFlag_Wc);
					cr.setWorkerId(fireworker);
					cr.setBeginTime(fireStartTime);
					cr.setComment(remark);
					cr.setOrderTime(cremationtime);
					cr.setCreateUserId(u.getUserId());
//					cr.setCommissionOrderId(cid);
					cr.setFurnaceId(funeralNum);// 特约炉入炉这两项可以省略?
					updateNotChangeFurnaceRecord2(cr);				
				}
			}
		} else {
			if (type.equals("change") && !ptrulu.equals("yes")) {//普通炉转特约炉  因为普通炉入炉也会有type为change这个条件，所以加入后面这个条件加以区分
				tyChangePt(type,cr,cid,null,funeralNum,remark,fireworker,cremationtime,fireStartTime,u,recordId);
			} else {//普通炉入炉
//				cr.setId(UuidUtil.get32UUID());  
				cr.setId(recordId);
				cr.setFurnaceId(funeralNum);
				cr.setComment(remark);
//				cr.setCommissionOrderId(cid);
				cr.setWorkerId(fireworker);
				cr.setFlag(Const.IsFlag_Wc);
				cr.setChangeFlag(Const.ChangeFlag_No);
				cr.setOrderTime(cremationtime);
				cr.setBeginTime(fireStartTime);
				cr.setCreateUserId(u.getUserId());
//				cr.setCreateTime(DateTools.dateToTimestamp(new java.util.Date()));
				cr.setOriginalId(null);
				updateNotChangeFurnaceRecord2(cr);
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cid", cid);
		map.put("furnaceId", funeralNum);
		if (!type.equals("change") || ptrulu.equals("yes")) {//为了普通炉入炉也要走下面的方法，添加后者条件
			map.put("cremationFlag", Const.furnace_flag_Yes);
			//火化委托单状态改为已火化未打印
			commissionOrderService.updateCommissionOrderScheduleFlag(cid, Const.schedule_flag_3);
		} else {
			if (changeto.equals("pt")) {
				map.put("furnaceFlag", Const.furnace_pt);
			} else if(changeto.equals("ty")) {
				map.put("furnaceFlag", Const.furnace_ty);
			}
		}
		updateCommissionFlagById(map);// 更改火化标志和furnaceId
	}
}
