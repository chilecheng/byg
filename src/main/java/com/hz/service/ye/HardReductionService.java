package com.hz.service.ye;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.mapper.ye.HardReductionMapper;
import com.hz.util.UuidUtil;
/**
 * ���Ѽ�������
 * @author rgy
 *
 */
@Service
@Transactional
public class HardReductionService {
	@Autowired
	private HardReductionMapper hardReductionMapper;
	//��ί��
	@Autowired
	private CommissionOrderService commissionOrderService;
	//���Ѽ�����Ŀ
	@Autowired
	private HardReductionDService hardReductionDService;
	/**
	 * �������Ѽ�������
	 * @param hardReduction
	 */
	public void addHardReduction(HardReduction hardReduction){
		hardReductionMapper.saveHardReduction(hardReduction);
	}
	/**
	 * ɾ�����Ѽ�������
	 * @param id
	 */
	public void deleteHardReduction(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			hardReductionMapper.deleteHardReduction(id);
		}
	}
	/**
	 * �޸����Ѽ�������
	 * @param baseReduction
	 */
	public void updateHardReduction(HardReduction hardReduction){
		hardReductionMapper.updateHardReduction(hardReduction);
	}
	/**
	 * ��ȡ���Ѽ�������
	 * @param id
	 * @return
	 */
	public HardReduction getHardReductionId(String id){
		return hardReductionMapper.getHardReductionById(id);
	}
	/**
	 * ����������ȡ���Ѽ�������list
	 * @param paramMap
	 * @return
	 */
	public List<HardReduction> getHardReductionList(Map<String, Object>paramMap){
		return hardReductionMapper.getHardReductionList(paramMap);
	}
	/**
	 * �ı����״̬
	 * @param type
	 * @param userId
	 * @param checkTime
	 * @param allPoseIds
	 */
	public void changeCheckFlag(byte type,String userId,Timestamp checkTime,String[] allPoseIds){
		if(allPoseIds.length>=1){
			for(int i=0;i<allPoseIds.length;i++){
				HardReduction hardReduction=getHardReductionId(allPoseIds[i]);
				hardReduction.setCheckFlag(type);
				hardReduction.setCheckUserId(userId);        		
        		hardReduction.setCheckTime(checkTime);
				updateHardReduction(hardReduction);
			}
		}
	}
	/**
	 * �������Ѽ�������
	 * @param hardReduction
	 * @param comm
	 * @param saveData
	 * @param hardId
	 */
	public void saveHardReduction(HardReduction hardReduction,String[] saveData,String hardId){
		//�������Ѽ���
		addHardReduction(hardReduction);
		//���»�ί�е�
//		commissionOrderService.updateCommissionOrder(comm);
		//���Ӽ�¼
		if(saveData!=null){
			for(int i=0;i<saveData.length;i++){
				String[] strArray = saveData[i].split(","); 
				String id=strArray[0];
				String itemId=strArray[1];
				int number=Integer.parseInt(strArray[3]);
				double total=Double.parseDouble(strArray[4]);
				String comment=strArray[5];
				
				HardReductionD hardReductionD=new HardReductionD();
				hardReductionD.setHardReductionId(hardId);
				hardReductionD.setItemId(itemId);
				hardReductionD.setNumber(number);
				hardReductionD.setTotal(total);
				hardReductionD.setComment(comment);
				//���Ӽ�¼
				id=UuidUtil.get32UUID();
				hardReductionD.setId(id);
				hardReductionDService.addHardReductionD(hardReductionD);
			}
		}
	}
	/**
	 * ��ȡ���Ѽ��������б�
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<HardReduction> getHardReductionPageInfo(Map<String, Object>paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//���÷�ҳ
		PageHelper.orderBy(order);//����
		List<HardReduction> list=getHardReductionList(paramMap);
		PageInfo<HardReduction> page=new PageInfo<HardReduction>(list);
		return page;
		
	}
	/**
	 * ���ݻ�ί�е�id��ѯ�����Ѽ������״̬
	 * @param commissionOrderId
	 * @return
	 */
	public String getHardCheckFlagByComId(String commissionOrderId){
		return hardReductionMapper.getHardCheckFlagByComId(commissionOrderId);
	}
}
