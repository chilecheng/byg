package com.hz.service.ye;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Jeneration;
import com.hz.entity.ye.AutoFreezerDetail;
import com.hz.entity.ye.AutopsyRecord;
import com.hz.mapper.ye.AutopsyRecordMapper;
import com.hz.service.base.JenerationService;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.socket.NoticeHandleService;
import com.hz.socket.SystemWebSocketHandler;
import com.hz.util.Const;
import com.hz.util.DateTools;

/**
 * 法政验尸
 * @author non
 *
 */
@Service
@Transactional
public class AutopsyRecordService {
	@Autowired
	private AutopsyRecordMapper autopsyRecordMapper;
	@Autowired
	DeptService deptService;
	@Autowired
	private  JenerationService jenerationService;
	@Autowired
	private UserNoticeService userNoticeService;
	@Autowired
	private SystemWebSocketHandler systemWebSocketHandler;
	@Autowired
	private NoticeHandleService noticeHandleService;
	
	
	
	/**
	 * 查询所有法政验尸的记录
	 * @param autopsyRecord
	 */
	public List<AutopsyRecord> findAllAutopsyRecord(Map<String, Object> paramMap) {
		List<AutopsyRecord> list=autopsyRecordMapper.listAutopsyRecord(paramMap);
		return list;
	}
	
	

	public PageInfo<AutopsyRecord> getListAutopsyRecord(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);
		List<AutopsyRecord> list=findAllAutopsyRecord(paramMap);
		PageInfo<AutopsyRecord> page=new PageInfo<AutopsyRecord>(list);
		
		return page;
	}
	//验尸解冻

	public List<AutopsyRecord> findAllAutoFreezerRecord(Map<String, Object> paramMap) {
		List<AutopsyRecord> list=autopsyRecordMapper.listAutoFreezerRecord(paramMap);
		return list;
	}	

	public PageInfo<AutopsyRecord> getListAutoFreezerRecord(Map<String, Object> paramMap,int pageNum,int pageSize,String order){

		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);
		List<AutopsyRecord> list=findAllAutoFreezerRecord(paramMap);
		PageInfo<AutopsyRecord> page=new PageInfo<AutopsyRecord>(list);
		return page;
	}
	
	

	//新建验尸申请
	public void addAutopsyRecord(AutopsyRecord apr) {
		String serialNumber=jenerationService.getJenerationCode(Jeneration.FZYS_TYPE);// 验尸流水号
		apr.setSerialNumber(serialNumber);
		autopsyRecordMapper.insertOrder(apr);
		if(apr.getAutoFreezerTime()!=null){
			// leftMenuNotice.autopsyNotice(apr.getAutoFreezerTime());
			java.sql.Date autoDate = DateTools.getDate(apr.getAutoFreezerTime());
			boolean s = userNoticeService.ifDateExit(Const.NoticeType_YsjdTask, (java.sql.Date) autoDate);
			if(s){ 
				userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_YsjdTask, autoDate, 1);
			}
			else{	
				userNoticeService.addNoticeOfDate(Const.NoticeType_YsjdTask, (java.sql.Date) autoDate);
			}
		}	
		String day1 = DateTools.getTimeFormatString("yyyy-MM-dd", apr.getAutoFreezerTime());
		String day2 = DateTools.getTimeString("yyyy-MM-dd");
		if (day1.equals(day2)) {
			String deptId = deptService.getDeptIdByName("二科");
			systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("autoFreezerNumber"));
		}
	}
	public List<AutopsyRecord> getAutopsyRecordById(String id) {
		return autopsyRecordMapper.getAutopsyRecordById(id);
	}
	public void updateAutopsyRecord(AutopsyRecord apr) {
		autopsyRecordMapper.updateOrder(apr);
	}

	public List<AutopsyRecord> checkcid(String cid) {
		List<AutopsyRecord> check=autopsyRecordMapper.checkid(cid);
		return check;
	}

  /**
   * 验尸申请的 更改
   * @param apr
   */
	public void toRevise(AutopsyRecord apr) {
		String serialNumber=jenerationService.getJenerationCode(Jeneration.FZYS_TYPE);// 验尸流水号
		apr.setSerialNumber(serialNumber);
		String orderid= apr.getCommissionOrderId();
		//  获取数据库中旧的autotime 数据
		Timestamp temp1=  autopsyRecordMapper.getAutoTimeById(orderid);
		Date oldDate = new Date( temp1.getTime() );
		Date autoDate = new Date( apr.getAutoFreezerTime().getTime() );
		boolean bool = temp1.equals(apr.getAutoFreezerTime());
		
		autopsyRecordMapper.toRevise(apr);
		if(apr.getAutoFreezerTime()!=null){
			//  autoTime  数据变更时
			if(!bool){

				String noticeType = Const.NoticeType_YsjdTask;
				noticeHandleService.updateNoticeToday(temp1, noticeType);	
				//userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_YsjdTask, oldDate, -1);
				
				boolean s	=userNoticeService.ifDateExit(Const.NoticeType_YsjdTask, autoDate);
				if(s){
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_YsjdTask ,autoDate,1);
				}else{
					userNoticeService.addNoticeOfDate(Const.NoticeType_YsjdTask, autoDate);
				}
				
			}
		}
		String day1 = DateTools.getTimeFormatString("yyyy-MM-dd", apr.getAutoFreezerTime());
		String day2 = DateTools.getTimeString("yyyy-MM-dd");
		if (day1.equals(day2)) {
			String deptId = deptService.getDeptIdByName("二科");
			systemWebSocketHandler.sendMessageToUsers(deptId, new TextMessage("autoFreezerNumber"));
		}		
		
	}
	// 对变更前后日子 进行判断  然后对session 进行设置值
	public int getTypeSession(AutopsyRecord apr){
		Date date = DateTools.getThisDate();
		String orderid= apr.getCommissionOrderId();
		Timestamp temp1=  autopsyRecordMapper.getAutoTimeById(orderid);
		Date oldDate = new Date( temp1.getTime() );
		Date autoDate = new Date( apr.getAutoFreezerTime().getTime() );
	//  变更前的日子刚好跟今天的日子相等  则返回-1  变更前后日子不相同
		//变更后的日子跟今天的日子相同  且  变更前后日子不相同 
		if(date.equals(autoDate) && !autoDate.equals(oldDate)){
			return 1;
		}
		else{
			if(oldDate.equals(date) && !autoDate.equals(oldDate) ){
				return -1;
			}else{
				return 0;
			}
		}	
	}
	// 对新增的  记录进行判断 如果autotime 属于今天则  则返回 消息在  解冻 session上
	public int getTypeIsertSession(AutopsyRecord apr){
		Date date = DateTools.getThisDate();
		Date autoDate = new Date( apr.getAutoFreezerTime().getTime() );
		boolean bool = date.equals(autoDate);
		if(bool){
			return 1;
		}
		else{
			return 0;
		}
		
	}
	
	
	public  AutoFreezerDetail getAutoFD (Map<String,Object> paramMap){
		AutoFreezerDetail detail = autopsyRecordMapper.autoFreezerDetail(paramMap);
		return detail;	
	}
	
/*	//获取 当日 需要解冻的  遗体报数量  和需要验尸的遗体数量 
	public int getAutoFreezerNumber(Timestamp startTime,Timestamp endTime){
		int sum1 = 	 autopsyRecordMapper.getSumAutoFreezer( startTime, endTime);
		return sum1;
	}*/
	public  void deleteAutopsyRecord(String id){
		Timestamp autoTime =  getAutoTimeById(id);
		autopsyRecordMapper.deleteAutopsyRecord(id);
		
		String noticeType = Const.NoticeType_YsjdTask;
		noticeHandleService.updateNoticeToday(autoTime, noticeType);		
		
	}
	
	/**
	 * 获取验尸解冻时间
	 * @param id
	 * @return
	 */
	public Timestamp getAutoTimeById(String id){
		return  autopsyRecordMapper.getAutoTimeById(id);
	}
	
	
	
}
