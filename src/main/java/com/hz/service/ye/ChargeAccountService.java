package com.hz.service.ye;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.ChargeAccount;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.ye.ChargeAccountMapper;
import com.hz.util.Const;
import com.hz.util.UuidUtil;
/**
 * 挂账业务 业务层
 * @author jgj
 *
 */
@Service
@Transactional
public class ChargeAccountService {
	@Autowired
	private ChargeAccountMapper chargeAccountMapper;
	@Autowired
	private CommissionOrderService commissionOrderService;
	//用户收费记录
	@Autowired
	private ItemOrderService itemOrderService;
	/**
	 * 获取挂账list
	 * @return
	 */
	public List<ChargeAccount> loadChargeAccount(){
		List<ChargeAccount> list=chargeAccountMapper.getChargeAccountList();
		return list;
	}
	/**
	 * 
	 */
	public void delChargeAccont(Map<String,Object> paramMap){
		chargeAccountMapper.delChargeAccount(paramMap);
	}
	/**
	 * 获取挂账业务分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<CommissionOrder> getChargeAccountPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		//设置页码和每页数
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<ItemOrder> list = chargeAccountMapper.getProjectFeesByItem(paramMap);
		//根据卡号查找
		if("findByCode".equals(paramMap.get("findBy"))){
			List<ItemOrder> itemList=chargeAccountMapper.getProjectFeesByItem(paramMap);
			List<CommissionOrder> listCode=findBy(itemList);
			PageInfo<CommissionOrder> page = new PageInfo<CommissionOrder>(listCode);
			page.setPageSize(pageSize);
			return page;
		}
		//根据姓名查找
		if("findByName".equals(paramMap.get("findBy"))){
			List<ItemOrder> itemList=chargeAccountMapper.getProjectFeesByItem(paramMap);
			List<CommissionOrder> listName=findBy(itemList);
			PageInfo<CommissionOrder> page = new PageInfo<CommissionOrder>(listName);
			page.setPageSize(pageSize);
			return page;
		}
		//无条件
		List<CommissionOrder> list2=findBy(list);
		PageInfo<CommissionOrder> page=new PageInfo<CommissionOrder>(list2);
		page.setPageSize(pageSize);
		return page;
	}
	
	public ChargeAccount getChargeAccount(String id){
		ChargeAccount chargeAccount=chargeAccountMapper.getChargeAccountById(id);
		return chargeAccount;
	}
	/**
	 * 根据条件获取项目和丧葬用品列表
	 * @param map
	 * @return
	 */
	public List<ItemOrder> getProjectFees(Map<String,Object> map){
		List<ItemOrder> list=chargeAccountMapper.getProjectFeesByItem(map);
		return list;
	}
	public void saveChargeAccount(String[] delNumber,String commissionId,String[] serviceData,String[] articlesData,
			String billUnitId){
		//可能 会有些问题，需要先取记录条再更改
		ItemOrder itemOrder = new ItemOrder();
		boolean payFlag=true;//判断是否全为挂账记录
		if(!"".equals(articlesData) && articlesData!=null){
			for(int i=0;i<articlesData.length;i++){
				String[] strArray = articlesData[i].split(","); 
				String id=strArray[0];
				String itemId=strArray[1];
				/*double pice=Double.parseDouble(strArray[2]);
				String type=strArray[3];*/
				int number=Integer.parseInt(strArray[4]);
				byte flag=Byte.parseByte(strArray[5]);
				double total=Double.parseDouble(strArray[6]);
				//有id更新数据
				if(id!=null&&!id.equals("")&&id.length()>5){
					itemOrder=itemOrderService.getItemOrderById(id);
					//判断记录的收费状态
					if(flag==Const.Is_Yes  && itemOrder.getBillPayFlag()!=Const.Pay_Yes){
						itemOrder.setPayFlag(Const.Pay_Yes);
						itemOrder.setBillPayFlag(Const.Pay_No);
					}else if(flag==Const.Is_No && itemOrder.getTickFlag()==Const.Is_Yes){
						itemOrder.setPayFlag(Const.Pay_No);
						payFlag=false;
					}else if(flag==Const.Is_No && itemOrder.getPayFlag()==Const.Pay_No){
						payFlag=false;
					}
					itemOrder.setItemId(itemId);
					itemOrder.setTotal(total);
					itemOrder.setTickFlag(flag);
					itemOrder.setNumber(number);
					itemOrder.setCommissionOrderId(strArray[8]);
					itemOrder.setComment(strArray[7]);
					itemOrderService.updateItemOrderNew(itemOrder);
				}else{
    				//没有ID则增加记录
					ItemOrder itemOrderNew=new ItemOrder();
    				id=UuidUtil.get32UUID();
    				itemOrderNew.setId(id);
    				itemOrderNew.setItemId(itemId);
    				itemOrderNew.setTotal(total);
    				itemOrderNew.setTickFlag(flag);
    				itemOrderNew.setNumber(number);
    				itemOrderNew.setCommissionOrderId(strArray[8]);
    				itemOrderNew.setComment(strArray[7]);
    				if(flag==Const.Is_Yes){
    					itemOrderNew.setPayFlag(Const.Pay_Yes);//挂账业务默认此处已收费,仅为业务需要
    					itemOrderNew.setBillPayFlag(Const.Pay_No);//挂账业务真实收费情况    					
    				}else if(flag==Const.Is_No){
    					itemOrderNew.setPayFlag(Const.Pay_No);
    					payFlag=false;
    				}
    				itemOrderService.addItemOrderNew(itemOrderNew);
    			}
			}
			
		}		
		
		//有记录的情况 下，不加会报空指针
		if(!"".equals(serviceData) && serviceData!=null){
			for(int i=0;i<serviceData.length;i++){
				String[] strArray = serviceData[i].split(","); 
				String id=strArray[0];
				String itemId=strArray[1];
				/*double pice=Double.parseDouble(strArray[2]);
				String type=strArray[3];*/
				int number=Integer.parseInt(strArray[4]);
				byte flag=Byte.parseByte(strArray[5]);
				double total=Double.parseDouble(strArray[6]);
				
				//有id更新数据
				if(id!=null&&!id.equals("")&&id.length()>5){
					itemOrder=itemOrderService.getItemOrderById(id);
					//判断记录的收费状态
					if(flag==Const.Is_Yes  && itemOrder.getBillPayFlag()!=Const.Pay_Yes){
						itemOrder.setPayFlag(Const.Pay_Yes);
						itemOrder.setBillPayFlag(Const.Pay_No);
					}else if(flag==Const.Is_No && itemOrder.getTickFlag()==Const.Is_Yes){
						itemOrder.setPayFlag(Const.Pay_No);
						payFlag=false;
					}else if(flag==Const.Is_No && itemOrder.getPayFlag()==Const.Pay_No){
						payFlag=false;
					}
					itemOrder.setItemId(itemId);
					itemOrder.setTotal(total);
					itemOrder.setTickFlag(flag);
					itemOrder.setNumber(number);
					itemOrder.setCommissionOrderId(strArray[8]);
					itemOrder.setComment(strArray[7]);
					itemOrderService.updateItemOrderNew(itemOrder);
				}else{
					//没有ID则增加记录
					ItemOrder itemOrderNew=new ItemOrder();
    				id=UuidUtil.get32UUID();
    				itemOrderNew.setId(id);
    				itemOrderNew.setItemId(itemId);
    				itemOrderNew.setTotal(total);
    				itemOrderNew.setTickFlag(flag);
    				itemOrderNew.setNumber(number);
    				itemOrderNew.setCommissionOrderId(strArray[8]);
    				itemOrderNew.setComment(strArray[7]);
    				if(flag==Const.Is_Yes){
    					itemOrderNew.setPayFlag(Const.Pay_Yes);//挂账业务默认此处已收费,仅为业务需要
    					itemOrderNew.setBillPayFlag(Const.Pay_No);//挂账业务真实收费情况    					
    				}else if(flag==Const.Is_No){
    					itemOrderNew.setPayFlag(Const.Pay_No);
    					payFlag=false;
    				}
    				itemOrderService.addItemOrderNew(itemOrderNew);
				}    			
		}
		}
	
	
	
	if(delNumber!=null&&!delNumber.equals("")){
		for(int i=0;i<delNumber.length;i++){
			itemOrderService.deleteItemOrderNew(delNumber[i]);
		}			
	}
	
	CommissionOrder comOrder= commissionOrderService.getCommissionOrderById(commissionId);
	if(payFlag){
		comOrder.setPayFlag(Const.Pay_Yes);
	}else{
		comOrder.setPayFlag(Const.Pay_No);
	}
//	comOrder.setProveUnitContent((proveUnitService.getProveUnitId(proveUnitId).getName()));
	comOrder.setBillUnitId(billUnitId);
	commissionOrderService.updateCommissionOrder(comOrder);
	}
	/**
	 * 封装按条件查询
	 * @param list
	 * @return
	 */
	public List<CommissionOrder> findBy(List<ItemOrder> list){
		List<CommissionOrder> list2=new ArrayList<CommissionOrder>();
		for(int i=0;i<list.size();i++){
			//找到所有火化委托单的ID
			ItemOrder io=(ItemOrder)list.get(i);
//			String id = io.getCommissionOrderId();
			//根据此ID找到相关火化委托单的记录
			CommissionOrder co = commissionOrderService.getCommissionOrderById(io.getCommissionOrderId());			
			list2.add(co);
		}
		return list2;
	}
	
	
	
}
