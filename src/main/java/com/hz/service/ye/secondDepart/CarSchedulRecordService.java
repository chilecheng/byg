package com.hz.service.ye.secondDepart;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Jeneration;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.mapper.ye.CarSchedulRecordMapper;
import com.hz.service.base.JenerationService;

@Service
@Transactional
public class CarSchedulRecordService {
	@Autowired
	private CarSchedulRecordMapper carSchedulRecordMapper;
	@Autowired
	private JenerationService jenerationService;
	
	/**
	 * 派车或者修改派车信息
	 * @param csr
	 */
	public void upOrAdRecorder(CarSchedulRecord csr) {
		csr.setFlowNumber(jenerationService.getJenerationCode(Jeneration.CLDD_TYPE));
		updateCarSchedulRecord(csr);
	}
	/**
	 * 添加车辆调度记录
	 * @param carSchedulRecord
	 */
	public void addCarSchedulRecord(CarSchedulRecord carSchedulRecord){
		carSchedulRecordMapper.saveCarSchedulRecord(carSchedulRecord);
	}
	/**
	 * 删除车辆调度记录
	 * @param id
	 */
	public void deleteCarSchedulRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	carSchedulRecordMapper.deleteCarSchedulRecord(ids[i]);
	    }
	}
	/**
	 * 更新车辆调度记录
	 * @param carSchedulRecord
	 */
	public void updateCarSchedulRecord(CarSchedulRecord carSchedulRecord){
		carSchedulRecordMapper.updateCarSchedulRecord(carSchedulRecord);
	}
	/**
	 * 获取车辆调度记录对象
	 * @param id
	 * @return
	 */
	public CarSchedulRecord getCarSchedulRecordById(String id){
		CarSchedulRecord carSchedulRecord=carSchedulRecordMapper.getCarSchedulRecordById(id);
		return carSchedulRecord;
	}
	
	/**
	 * 根据条件获取车辆调度记录List
	 * @param String name
	 */
	public List<CarSchedulRecord> getCarSchedulRecordList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		return carSchedulRecordMapper.getCarSchedulRecordList(paramMap);
	}
	
	/**
	 * 根据条件获取车辆调度记录PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<CarSchedulRecord> getCarSchedulRecordPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		List<CarSchedulRecord> list=getCarSchedulRecordList(paramMap,order);
		PageInfo<CarSchedulRecord> page = new PageInfo<CarSchedulRecord>(list);
		return page; 
	}
	/**
	 * 车辆调度进入列表
	 * @return
	 */
	public PageInfo<CarSchedulRecord> getCarListPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order) {
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<CarSchedulRecord> list=carSchedulRecordMapper.getCarList(paramMap);
		PageInfo<CarSchedulRecord> page = new PageInfo<CarSchedulRecord>(list);
		return page; 
	}
	/**
	 * 根据车辆调度表ID查询死者信息
	 * @param id
	 * @return
	 */
	public CarSchedulRecord getCommissionOrder(String id) {
		return carSchedulRecordMapper.getCarSchedulRecord(id);
	}
	/**
	 * 根据ID 查找车辆记录,用于首页 显示查看状态使用
	 * @param id
	 * @return
	 */
	public CarSchedulRecord getCarSchedulRecordHome(String id){
		return carSchedulRecordMapper.getCarSchedulRecordHome(id);
	}
	/**
	 * 撤销调度/完成调度
	 */
	public int updateCarSchedulRecordFlag(Map<String,Object> map) {
		return carSchedulRecordMapper.updateCarSchedulRecordFlag(map);
	}
	
	public int getNumber(Map<String, Object> map){
		return carSchedulRecordMapper.getNumber(map);
	}
	public void updateNumber(CarSchedulRecord car){
		carSchedulRecordMapper.updateNumber(car);
	}
	
}
