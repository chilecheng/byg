package com.hz.service.ye;  
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Freezer;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.FreezerRecord;
import com.hz.mapper.ye.FreezerRecordMapper;
import com.hz.service.base.FreezerService;
import com.hz.service.system.DeptService;
import com.hz.util.Const;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;
/**
 * 冷藏柜记录信息
 * @author rgy
 *@author  gpf
 */
@Service
@Transactional
public class FreezerRecordService {
	@Autowired
	private FreezerRecordMapper freezerRecordMapper;
	@Autowired
	 private FreezerService freezerService;
	@Autowired
	DeptService deptService;
	@Autowired
	private CommissionOrderService OrderService;
	@Autowired
	private MourningRecordService mourningRecordService;
	/**
	 * 添加冷藏柜记录
	 * @param freezerRecord
	 */
	public void addFreezerRecord(FreezerRecord freezerRecord){
		freezerRecordMapper.saveFreezerRecord(freezerRecord);
		//当新增 冷藏柜记录时对 新数据进行判断 ，看是否满足 遗体解冻时间的条件 ，如果满足 则 进行消息 提醒到 遗体解冻时间安排
		
	}
	/**
	 * 删除冷藏柜记录
	 * @param id
	 */
	public void deleteFreezerRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			freezerRecordMapper.deleteFreezerRecord(id);
		}
	}
	/**
	 * 修改冷藏柜记录
	 * @param freezerRecord
	 */
	public void updateFreezerRecord(FreezerRecord freezerRecord){
//		String id= freezerRecord.getId();
		//  获取数据库中旧的autotime 数据
		/*Timestamp temp1=  freezerRecordMapper.getAutoTimeById(id);*/
		freezerRecordMapper.updateFreezerRecord(freezerRecord);
	
	}
	/**
	 * 修改冷藏柜记录 入柜 出柜相关操作
	 * @param freezerRecord
	 */
	public void intoOutFreezerRecord(FreezerRecord freezerRecord,Freezer freezer){
		freezerService.updateFreezer(freezer);
		freezerRecordMapper.updateFreezerRecord(freezerRecord);
	}
	/**
	 * 事务控制保存冰柜出柜。连接改变灵堂记录状态为清场
	 * @param id
	 * @param freezerRecord
	 * @param freezer
	 */
	public void saveOutAndUpdateMourning(String id,FreezerRecord freezerRecord,Freezer freezer){
		String commissionOrderId=freezerRecord.getCommissionOrderId();
		mourningRecordService.updateMourningRecordFlag(commissionOrderId, Const.IsFlag_QC);
   		if (id==null||id.equals("")) {
   			freezerRecord.setId(UuidUtil.get32UUID());
   			freezerRecord.setFlag(Const.IsFlag_No);
   			addFreezerRecord(freezerRecord);
   		}else {
   			intoOutFreezerRecord(freezerRecord ,freezer);
   		}
	}
	
	/**
	 * 修改冷藏柜记录  用于  转柜 操作时     转柜相关涉及项的保存 
	 * @param freezerRecord
	 */
	public void updateTransferFreezerRecord(FreezerRecord freezerRecord,Freezer freezer,Freezer newFreezer,CommissionOrder order){
		freezerService.updateFreezer(freezer);
			freezerService.updateFreezer(newFreezer);
			 OrderService.updateCommissionOrder(order);
		freezerRecordMapper.updateFreezerRecord(freezerRecord);
	}
	
	/**
	 * 获取冷藏柜记录对象
	 * @param id
	 * @return
	 */
	public FreezerRecord getFreezerRecordId(String id){
		return freezerRecordMapper.getFreezerRecordById(id);
	}

	/**
	 * 根据条件获取冷藏柜记录     		调用  记录   list
	 * @param paramMap
	 * @return
	 */
	public List<FreezerRecord> getFreeComList(Map<String, Object> paramMap){
		 return freezerRecordMapper.getFreeComList(paramMap);
	}
	
	/*<!--  查询已付费的  的满足解冻时间的    项目  （遗体解冻 消息提醒） -->*/
	public List<FreezerRecord> getFreeComListPayed(Map<String, Object> paramMap){
		 return freezerRecordMapper.getFreezerRecordListPayed(paramMap);
	}
	
	/**
	 * 最新二科冰柜列表查询方法
	 * @param map
	 * @return
	 */
	public List<FreezerRecord> getIceRecondMapper(Map<String,Object> map){
		return freezerRecordMapper.getIceRecondMapper(map);
	}
	
	/**
	 * 根据条件获取冷藏柜记录list
	 * @param paramMap
	 * @return
	 */
	public List<FreezerRecord> getFreezerRecordList(Map<String, Object> paramMap){
		return freezerRecordMapper.getFreezerRecordList(paramMap);
	}
	/**
	 * 根据条件获取冷藏柜记录list
	 * 条件为 未出柜的 数据记录
	 * @param paramMap
	 * @return
	 */
	public List<FreezerRecord> getNotOutFreezerRecordList(Map<String, Object> paramMap){
		return freezerRecordMapper.getNotoutFreezerRecordList(paramMap);
	}
	
	/**
	 * 获取冷藏柜记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */

	public PageInfo<FreezerRecord> getFreezerRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<FreezerRecord> list=getFreezerRecordList(paramMap);
		PageInfo<FreezerRecord> page = new PageInfo<FreezerRecord>(list);
		return page;
		
	}
	


	/**
	 *   显示列表信息  用于list2
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<FreezerRecord> getFreezerRecordPageInfo2(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
//		
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		PageInfo<FreezerRecord> page=new PageInfo<FreezerRecord>(getIceRecondMapper(paramMap));
		return page;	
	}

	/**
	 * 首页使用:查找冰柜调度
	 * @param paramMap
	 * @return
	 */
	public List<FreezerRecord> getFreezerRecordHome(Map<String,Object> paramMap){
		return freezerRecordMapper.getFreezerRecordHome(paramMap);
	}
	public int getNumber(Map<String,Object> paramMap){
		return freezerRecordMapper.getNumber(paramMap);
	}
	public FreezerRecord getFreezerRecordByCoId(String id) {
		return freezerRecordMapper.getFreezerRecordByCoId(id);
	}
	public void deleteFreezerRecordByCoId(String id) {
		freezerRecordMapper.deleteFreezerRecordByCoId(id);
	}
	public Timestamp getAutoTimeByOrderId(String orderId){
		return   freezerRecordMapper.getAutoTimeByOrderId(orderId)  ;
		
	}
	/**
	 * 根据 
	 */
	// 遗体解冻时间
		public Timestamp getautoTime(Timestamp arriveTime,Timestamp mourningTime, Timestamp farewellTime , Timestamp cremationTime){
		    Date date1 = new Date();//1.如果有灵堂时间，就取灵堂时间，2.如果有告别厅时间，就取告别时间，否者就取火化预约时间
		    Timestamp autoTime = null;
		     //return 1 表示第一个时间大于第二个时间 0表示等于  -1表示小于
		     //当到馆时间   
		    if(mourningTime!=null&&!mourningTime.equals("")&& arriveTime!=null&&!arriveTime.equals("") ){
				long day=(mourningTime.getTime()-arriveTime.getTime())/(24*60*60*1000);
					if(day>=7){
						Calendar rightNow = Calendar.getInstance();
				
						rightNow.setTime(mourningTime);
						rightNow.add(Calendar.DAY_OF_YEAR,-2);//当解冻时间大于7天，就提前2天解冻
						date1 =  rightNow.getTime();
					
					}else{
						Calendar rightNow = Calendar.getInstance();
					
						rightNow.setTime(mourningTime);
						rightNow.add(Calendar.DAY_OF_YEAR,-1);//当解冻时间小于7天，就提前1天解冻
						date1 = rightNow.getTime();
					}
			    }else 	if(farewellTime !=null&&	!farewellTime.equals("") &&arriveTime !=null&& !arriveTime.equals("")){			    
			    		long day=(farewellTime.getTime()-arriveTime.getTime())/(24*60*60*1000);
							if(day>=7){
								Calendar rightNow = Calendar.getInstance();
							
								rightNow.setTime(farewellTime);
								rightNow.add(Calendar.DAY_OF_YEAR,-2);//当解冻时间大于7天，就提前2天解冻
								date1 = rightNow.getTime();
							
							}else{
								Calendar rightNow = Calendar.getInstance();
				
								rightNow.setTime(farewellTime);
								rightNow.add(Calendar.DAY_OF_YEAR,-1);//当解冻时间小于7天，就提前1天解冻
								date1 = rightNow.getTime();
							}
					
			    }else  if(cremationTime!=null&&!cremationTime.equals("") && arriveTime !=null&& !arriveTime.equals("") ){ 
						long day=(cremationTime.getTime()-arriveTime.getTime())/(24*60*60*1000);
						if(day>=7){
									Calendar rightNow = Calendar.getInstance(); 
									rightNow.setTime(cremationTime);
									rightNow.add(Calendar.DAY_OF_YEAR,-2);//当解冻时间大于7天，就提前2天解冻
									date1 = rightNow.getTime();
										
						}else{
									Calendar rightNow = Calendar.getInstance();
									rightNow.setTime(cremationTime);
									rightNow.add(Calendar.DAY_OF_YEAR,-1);//当解冻时间小于7天，就提前1天解冻
									date1 = rightNow.getTime();
								}
							
				}	  
			  else {
					  date1 = null;
				  }
							  	       
		    if(date1 != null){
		    /*	if ( arriveTime.getTime() - date1.getTime() >0 )*/
		    //	如果 以上逻辑获取到的日期 为到馆的前一天的日期，造成逻辑上的错误,遇到这样的情况
		    	@SuppressWarnings("deprecation")
				int   day = date1.getDay();
		    	@SuppressWarnings("deprecation")
				int nowDay = DateTools.getThisDate().getDay();
		    	if(nowDay >day){
		    		autoTime = arriveTime;
		    	}else{
	    			autoTime = new Timestamp(date1.getTime()); 
		    	}
		  
		    	
		    }
		 
		return autoTime;
			
		}
	
	
	
}