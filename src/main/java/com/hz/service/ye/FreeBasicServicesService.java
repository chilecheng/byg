package com.hz.service.ye;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CommissionOrder;
import com.hz.mapper.ye.BaseReductionMapper;
import com.hz.service.payDivision.DelegateService;
import com.hz.util.UuidUtil;
/**
/**
 * 基本服务减免申请
 * @author jgj
 *
 */
@Service
@Transactional
public class FreeBasicServicesService {
	@Autowired
	private BaseReductionMapper baseReductionMapper;
	@Autowired
	private CommissionOrderService commissionOrderService;
	//基本减免申请项目
	@Autowired
	private BaseReductionDService baseReduuctionDService;
	
	/**
	 * 根据ID获得基本服务减免申请记录
	 * @param id
	 * @return
	 */
	public BaseReduction  getFreeBasicById(String id){
		BaseReduction baseReduction=baseReductionMapper.getBaseReductionById(id);
		return baseReduction;
	}
	/**
	 * 条件获取基本服务减免申请列表
	 * @param paramMap
	 * @return
	 */
	public List<BaseReduction> getBaseReductionList(Map<String,Object> paramMap){
		List<BaseReduction> list=baseReductionMapper.getBaseReductionList(paramMap);
		return list;
	}	
	/**
	 * 更新基本服务减免申请表
	 * @param baseReduction
	 */
	public void updataBaseReduction(BaseReduction baseReduction){
		baseReductionMapper.updateBaseReduction(baseReduction);
	}
	/**
	 * 添加基本服务减免申请
	 * @param baseReduction
	 */
	public void addBaseReduction(BaseReduction baseReduction){
		baseReductionMapper.saveBaseReduction(baseReduction);
	}
	/**
	 * 获取基本服务减免申请分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */	
	public PageInfo<BaseReduction> getFreeBasicServicesPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<BaseReduction> list=getBaseReductionList(paramMap);
		
		PageInfo<BaseReduction> page = new PageInfo<BaseReduction>(list);
		return page;
	}
	/**
	 * 保存修改
	 * @param baseReduction
	 * @param commission
	 * @param saveData
	 * @param delNumber
	 */
	public void saveFreeBasic(BaseReduction baseReduction,CommissionOrder commission,String[] saveData,String[] delNumber){
		//删除已被清除的记录
		if(delNumber!=null&&!delNumber.equals("")){
			for(int i=0;i<delNumber.length;i++){
				baseReduuctionDService.deleteBaseReductionD(delNumber[i]);
			}			
		}
		//更新基本减免
		updataBaseReduction(baseReduction);
		//更新备注
		commissionOrderService.updateCommissionOrder(commission);
		
		
		//保存减免项目 
		for(int i=0;i<saveData.length;i++){
			String[] strArray = saveData[i].split(","); 
			String id=strArray[0];
			String itemId=strArray[1];
			int number=Integer.parseInt(strArray[3]);
			double total=Double.parseDouble(strArray[4]);
			String comment=strArray[5];
			String baseReductionId=strArray[6];
			BaseReductionD baseReductionD=new BaseReductionD();
			baseReductionD.setBaseReductionId(baseReductionId);
			baseReductionD.setItemId(itemId);
			baseReductionD.setNumber(number);
			baseReductionD.setTotal(total);
			baseReductionD.setComment(comment);
			//有id更新数据
			if(id!=null&&!id.equals("")&&id.length()>5){
				baseReductionD.setId(id);
				baseReduuctionDService.updateBaseReductionD(baseReductionD);
			}else{
			//没有ID则增加记录
				id=UuidUtil.get32UUID();
				baseReductionD.setId(id);
				baseReduuctionDService.addBaseReductionD(baseReductionD);
			}
		}
		
		
	}
	/**
	 * 添加保存功能
	 * @param baseReduction
	 * @param saveData
	 * @param reductionId
	 */
	public void addSaveFreeBasic(BaseReduction baseReduction,String[] saveData,String reductionId){
		//保存减免申请
		addBaseReduction(baseReduction);
		
		for(int i=0;i<saveData.length;i++){
			String[] strArray = saveData[i].split(","); 
			String id=strArray[0];
			String itemId=strArray[1];
			int number=Integer.parseInt(strArray[3]);
			double total=Double.parseDouble(strArray[4]);
			String comment="";
			if(strArray.length>=6){
				comment=strArray[5];    				
			}
			BaseReductionD baseReductionD=new BaseReductionD();
			baseReductionD.setBaseReductionId(reductionId);
			baseReductionD.setItemId(itemId);
			baseReductionD.setNumber(number);
			baseReductionD.setTotal(total);
			baseReductionD.setComment(comment);
			//有id更新数据
			if(id!=null&&!id.equals("")&&id.length()>5){
				baseReduuctionDService.updateBaseReductionD(baseReductionD);
			}else{
			//没有ID则增加记录
				id=UuidUtil.get32UUID();
				baseReductionD.setId(id);
				baseReduuctionDService.addBaseReductionD(baseReductionD);
			}
		}	
	}
}
