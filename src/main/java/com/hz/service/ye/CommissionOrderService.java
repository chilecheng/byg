package com.hz.service.ye;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Freezer;
import com.hz.entity.base.Item;
import com.hz.entity.base.Jeneration;
import com.hz.entity.system.User;
import com.hz.entity.ye.Appointment;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.DelegateRecord;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.ItemChange;
import com.hz.entity.ye.ItemOrder;
import com.hz.entity.ye.MourningRecord;
import com.hz.entity.ye.PincardRecord;
import com.hz.mapper.ye.CommissionOrderMapper;
import com.hz.service.base.FreezerService;
import com.hz.service.base.FurnaceService;
import com.hz.service.base.ItemService;
import com.hz.service.base.JenerationService;
import com.hz.service.payDivision.DelegateService;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.service.ye.secondDepart.CarSchedulRecordService;
import com.hz.socket.NoticeHandleService;
import com.hz.socket.SystemWebSocketHandler;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
import com.hz.util.UuidUtil;



@Service
@Transactional
public class CommissionOrderService {
	@Autowired
	private NoticeHandleService noticeHandleService;
	@Autowired
	private DeptService deptService;
	@Autowired
	private SystemWebSocketHandler socketHandler;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	private CommissionOrderMapper commissionOrderMapper;
	@Autowired
	private ItemChangeService itemChangeService;
	@Autowired
	private ItemService itemService;
	@Autowired
	//丧葬用品更改前记录
	private ItemBeforeService itemBeforeService;
	//预约登记服务
	@Autowired
	private AppointmentService appointmentService;
	//火化委托单服务项目
	@Autowired
	private ItemOrderService itemOrderService;
	//车辆调度
	@Autowired
	private CarSchedulRecordService carSchedulRecordService;
	//告别厅使用记录
	@Autowired
	private FarewellRecordService farewellRecordService;
	//冷藏柜使用记录
	@Autowired
	private FreezerRecordService freezerRecordService;
	//守灵室使用记录
	@Autowired
	private MourningRecordService mourningRecordService;
	//困难减免申请
	@Autowired
	private HardReductionService hardReductionService;
	//困难减免项目
	@Autowired
	private HardReductionDService hardReductionDService;
	//基本减免申请
	@Autowired
	private BaseReductionService baseReductionService;
	//基本减免项目
	@Autowired
	private BaseReductionDService baseReductionDService;
	//火化炉信息
	@Autowired
	private FurnaceService furnaceService;
	//火化炉调度
	@Autowired
	private FurnaceRecordService furnaceRecordService;
	//冷藏柜信息
	@Autowired
	private FreezerService freezerService;
	//编号管理
	@Autowired
	private JenerationService jenerationService;
	@Autowired
	private DelegateService delegrateService;
//----------日志--------------
	//委托单
	/*@Autowired
	private CommissionOrderServiceLog commissionOrderServiceLog;
	//车辆调度
	@Autowired
	private CarSchedulRecordServiceLog carSchedulRecordServiceLog;
	//告别厅使用记录
	@Autowired
	private FarewellRecordServiceLog farewellRecordServiceLog;
	//冷藏柜使用记录
	@Autowired
	private FreezerRecordServiceLog freezerRecordServiceLog;
	//守灵室使用记录
	@Autowired
	private MourningRecordServiceLog mourningRecordServiceLog;
	//困难减免申请
	@Autowired
	private HardReductionServiceLog hardReductionServiceLog;
	//困难减免项目
	@Autowired
	private HardReductionDServiceLog hardReductionDServiceLog;
	//基本减免申请
	@Autowired
	private BaseReductionServiceLog baseReductionServiceLog;
	//基本减免项目
	@Autowired
	private BaseReductionDServiceLog baseReductionDServiceLog;
	//收费项目
	@Autowired
	private ItemOrderServiceLog itemOrderServiceLog;
	//火化炉调度
	@Autowired
	private FurnaceRecordServiceLog furnaceRecordServiceLog;*/
	//销卡记录
	@Autowired
	private PincardRecordService  pincardRecordService;
	/**
	 * 首页未收费列表
	 * @param map
	 * @return
	 */
	public List<CommissionOrder> getUnPayList(Map<String,Object> map){
		return commissionOrderMapper.getUnPayList(map);
	}
	public List<CommissionOrder> getCrematorList(Map<String,Object> map){
		return commissionOrderMapper.getCrematorList(map);
	}
	public int getNumber(Map<String,Object> map){
		return commissionOrderMapper.getNumber(map);
	}
	public void updateNumber(CommissionOrder com){
		commissionOrderMapper.updateNumber(com);
	}
	public void updateNumber2(String id, byte one) {
		commissionOrderMapper.updateNumber2(id,one);
	}	
	public void updateNumber3(String id, byte one) {
		commissionOrderMapper.updateNumber3(id,one);
	}
	/**
	 * 更新收费状态
	 * @param id
	 * @param flag
	 */
	public void updatePayFlag(String id,byte flag){
		commissionOrderMapper.updatePayFlag(id, flag);
	}
	public int getNumberCremator(Map<String,Object> map){
		return commissionOrderMapper.getNumberCremator(map);
	}
	public void updateCommissionOrderScheduleFlag(String id,byte schedule){
		commissionOrderMapper.updateCommissionOrderScheduleFlag( id , schedule);
	}
	public void deleteCardCode(String id ,String cardCode,byte pincardFlag){
		commissionOrderMapper.deleateCardCode(id, cardCode, pincardFlag);
	}
	/**
	 * 根据卡号获取火化委托单对象
	 * @param cardCode
	 * @return
	 */
	public CommissionOrder getCommissionOrderByCardCode(String cardCode){
		return commissionOrderMapper.getCommissionOrderByCardCode(cardCode);
	}
	
	/**
	 * 添加火化委托单
	 * @param commissionOrder
	 */
	public void addCommissionOrder(CommissionOrder commissionOrder){
		commissionOrderMapper.saveCommissionOrder(commissionOrder);
	}
	/**
	 * 删除火化委托单
	 * @param id
	 */
	public void deleteCommissionOrder(String coId){
			Map<String, Object> maps = new HashMap<String, Object>();
			maps.put("commissionOrderId", coId);
			//删除一科对应的通知  只查了某些字段 不够再加
		/*	CommissionOrder co = getCommissionOrderByIdToDeletNotice(coId);*/
			/*List<ItemOrder> itemlistSocket = new ArrayList<ItemOrder>();
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("commissionOrderId", coId);
			itemlistSocket = itemOrderService.getItemOrderList(map);
			noticeHandleService.handleFirstDepart(co, itemlistSocket, -1);*/
			//删除车辆调度及通知
			List<CarSchedulRecord> carList = carSchedulRecordService.getCarSchedulRecordList(maps, "");
			for (int j = 0; j < carList.size(); j++) {
				Timestamp oldPickTime = carList.get(j).getPickTime();
				carSchedulRecordService.deleteCarSchedulRecord(carList.get(j).getId());
				if (userNoticeService.ifDateExit(Const.NoticeType_CarTask,DateTools.getDate(oldPickTime))) {
				/*	userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_CarTask,DateTools.getDate(oldPickTime), -1);*/
					noticeHandleService.updateCarNotice(oldPickTime, Const.NoticeType_CarTask);
				}
			}
			// 冷藏柜记录
			List<FreezerRecord> fList = freezerRecordService.getFreezerRecordList(maps);
			if (fList.size() > 0) {
				Freezer freezer = freezerService.getFreezerById(fList.get(0).getFreezerId());
				if (freezer != null) {
					freezer.setsName("");
					freezer.setFlag(Const.IsFlag_No);
					freezer.setUserName("");
					freezerService.updateFreezer(freezer);
				}
				freezerRecordService.deleteFreezerRecord(fList.get(0).getId());
			}
			// 告别厅记录
			List<FarewellRecord> faList = farewellRecordService.getFarewellRecordList(maps, "");
			if (faList.size() > 0) {
				farewellRecordService.deleteFarewellRecord(faList.get(0).getId());
			}
			// 灵堂记录
			List<MourningRecord> mList = mourningRecordService.getMourningRecordList(maps, "");
			if (mList.size() > 0) {
				mourningRecordService.deleteMourningRecord(mList.get(0).getId());
			}
			// 火化炉调度
			List<FurnaceRecord> fuList = furnaceRecordService.getFurnaceRecordList(maps, "");
			if (fuList.size() > 0) {
				furnaceRecordService.deleteFurnaceRecord(fuList.get(0).getId());
			}
			// 火化委托单收费项目
			List<ItemOrder> iList = itemOrderService.getItemOrderList(maps);
			for (int j = 0; j < iList.size(); j++) {
				itemOrderService.deleteItemOrder(iList.get(j).getId());
			}
			//新加删除基本减免，困难减免
			Map<String,Object> paramMap=new HashMap<String,Object>();
			paramMap.put("commissionOrderId", coId);
			List<BaseReduction> baseList=baseReductionService.getBaseReductionList(paramMap);
			for(int i=0;i<baseList.size();i++){
				Map<String,Object> mapd=new HashMap<String,Object>();
				mapd.put("baseReductionId", baseList.get(i).getId());
				List<BaseReductionD> baseListD=baseReductionDService.getBaseReductionDList(mapd);
				for(int j=0;j<baseListD.size();j++){
					baseReductionDService.deleteBaseReductionD(baseListD.get(j).getId());
				}
				baseReductionService.deleteBaseReduction(baseList.get(i).getId());
			}
			//困难减免
			List<HardReduction> hardList=hardReductionService.getHardReductionList(paramMap);
			for(int i=0;i<hardList.size();i++){
				Map<String,Object> mapd=new HashMap<String,Object>();
				mapd.put("hardReductionId", hardList.get(i).getId());
				List<HardReductionD> hardListD=hardReductionDService.getHardReductionDList(mapd);
				for(int j=0;j<hardListD.size();j++){
					hardReductionDService.deleteHardReductionD(hardListD.get(j).getId());
				}
				hardReductionService.deleteHardReduction(hardList.get(i).getId());
			}
			
			
			commissionOrderMapper.deleteCommissionOrder(coId);
	}
	/*private CommissionOrder getCommissionOrderByIdToDeletNotice(String coId) {
		return commissionOrderMapper.getCommissionOrderByIdToDeletNotice(coId);
	}*/
	public byte getCheckFlag(String coId) {
		return commissionOrderMapper.getCheckFlag(coId);
	}
	/**
	 * 更新火化委托单
	 * @param commissionOrder
	 */
	public void updateCommissionOrder(CommissionOrder commissionOrder){
		commissionOrderMapper.updateCommissionOrder(commissionOrder);
	}
	/**
	 * 获取火化委托单对象
	 * @param id
	 * @return
	 */
	public CommissionOrder getCommissionOrderById(String id){
		CommissionOrder commissionOrder=commissionOrderMapper.getCommissionOrderById(id);
		return commissionOrder;
	}
	/**
	 * 获取火化委托单对象（查询历史加最新数据）
	 * @param id
	 * @return
	 */
	public CommissionOrder getComByIdNewAndOld(String id){
		return commissionOrderMapper.getComByIdNewAndOld(id);
	}
	public CommissionOrder getCommissionOrderByIdChoose(Map<String, Object> map){
		CommissionOrder commissionOrder = null;
		if (map.containsKey("goto")) {
			if ((Integer) map.get("goto") == 1) {//火化转炉入炉时
				commissionOrder = commissionOrderMapper.getCommissionOrderByIdChoose1(map);
			} else if ((Integer) map.get("goto") == 2) {//骨灰寄存保存时
				commissionOrder = commissionOrderMapper.getCommissionOrderByIdChoose2(map);
			}
		}
		return commissionOrder;
	}
	/**
	 * 获取火化委托单对象(审核专用)
	 * @param id
	 * @return
	 */
	public CommissionOrder getCommissionOrderByIdForCheck(String id){
		CommissionOrder commissionOrder=commissionOrderMapper.getCommissionOrderByIdForCheck(id);
		return commissionOrder;
	}
	/**
	 * 根据条件获取火化委托单List
	 * @param String name
	 */
	public List<CommissionOrder> getCommissionOrderList(Map<String, Object> paramMap){
		List<CommissionOrder> list= null;
		if (paramMap != null && paramMap.containsKey("goto")) {
			byte b = (Byte) paramMap.get("goto");
			if (b == Const.GOTO1) {//获取火化委托单List(调度科火化委托单列表用)
				list =  commissionOrderMapper.getCoList1(paramMap);
			} else if(b == Const.GOTO2) {//调度科 骨灰寄存的时候  搜索姓名、id用
				list =  commissionOrderMapper.getCoList2();
			} else if(b == Const.GOTO3) {//花圈花篮购买页面 点姓名搜索的弹出框
				list =  commissionOrderMapper.getCoList3(paramMap);
			} else if(b == Const.GOTO4) {//永嘉登记列表
				list =  commissionOrderMapper.getCoList4(paramMap);
			}  else {
				list = commissionOrderMapper.getCommissionOrderList(paramMap);
			}
		} else {
			list = commissionOrderMapper.getCommissionOrderList(paramMap);
		}
		return list;
	}
	/**
	 * 查询此卡号有没有重复
	 */
	public int getCommissionOrderByCard(String cardCode) {
		return commissionOrderMapper.getCommissionOrderByCard(cardCode);
	}
	/**
	 * 根据条件获取火化委托单PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<CommissionOrder> getCommissionOrderPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<CommissionOrder> list=getCommissionOrderList(paramMap);
		PageInfo<CommissionOrder> page = new PageInfo<CommissionOrder>(list);
		return page; 
	}
	/**
	 * 委托单的销卡处理
	 * @param co
	 * @param user
	 */
	public void pinCardczl(CommissionOrder co,User user){
		if(co.getPincardFlag()==Const.pincard_No){
			String oldCard = co.getCardCode();
    		co.setPincardFlag(Const.pincard_Yes);
    		co.setCardCode(null);
	    	updateCommissionOrder(co);   
	    	String commissionOrderId = co.getId();
	    	java.sql.Date creatTime = DateTools.getThisDate();
	    	String creatUserId = user.getUserId();
	    	PincardRecord pincardRecord = new PincardRecord();
	    	pincardRecord.setCommissionOrderId(commissionOrderId);
	    	pincardRecord.setCreatTime(creatTime);
	    	pincardRecord.setCreatUserId(creatUserId);
	    	pincardRecord.setOldCard(oldCard);
	    	pincardRecord.setId(UuidUtil.get32UUID());
	    	pincardRecordService.addPincardRecord(pincardRecord);
    	}
	}
	
	/**
	 * 获取全部的处理状态
	 * @param id
	 * @param checkFlag
	 */
	public String getDealIselOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<CommissionOrder> listDeal=commissionOrderMapper.getCommissionOrderList(null);
	    for (int i = 0; i < listDeal.size(); i++) {
			CommissionOrder comm=(CommissionOrder) listDeal.get(i);
			list.add(new Object[]{comm.getId(),comm.getDealIsdelName()});
		}
	    String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改丧葬用品核对列表状态
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			CommissionOrder comm=getCommissionOrderById(ids[i]);
			if(isdel==Const.Yes_treated){
				comm.setDealIsdel(Const.Yes_treated);
			}else if(isdel==Const.No_treated){
				comm.setDealIsdel(Const.No_treated);
			}else if(isdel==Const.Yes_usered){
				comm.setDealIsdel(Const.Yes_usered);
			}
			updateCommissionOrder(comm);
		}
	}
	/**
	 * 获取全部的处理状态
	 * @param id
	 * @param checkFlag
	 */
	public String getDealIselOption0(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
	    for (int i = 1; i < 4; i++) {
			list.add(new Object[]{i,Const.getDealIsdelType((byte) i)});
		}
	    String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 事务控制保存委托单后期追加的数据
	 */
	public void saveCremationChanges(String userId,String userName,String id,Timestamp arriveTime,String corpseUnitId,
			String billUnitId,String mourningId,Timestamp mourningBeginTime,Timestamp mourningEndTime,String farewellId,
			Timestamp farewellBeginDate,byte radio,String furnaceTypeId,Timestamp orderTime,String furnaceId,String specialBusinessValue,
			String[] service,String[] articles,String[] itemId,String[] pice,String[] number,
			String[] bill,String[] total,String[] comment,
			String[] transportType,String[] carType,Timestamp[] pickTime,String[] carComment,String[] carSchedulRecordId
			,String[] itemIdR, String[] numberR, String[] totalR, String[] commentR,
			String[] base, String baseReductionId, String[] freePersonId, String[] hardPersonId,byte baseIs,String fidcard,String[] hard,
			String[] proveIds, String hardReductionId, byte hardIs,String hard_proveUnitId,String reason,String hComment,Timestamp createTime,
			String appellationId,String applicant,String phone,byte special,
			String[] hardIdR,String[] hardnumberR,String[] hardpiceR,String[] hardtotalR,
			String[] hardcommentR,String[] piceR){
		//默认 灵堂/告别厅/丧葬用品 为false,如果在此有新加，均表示有变动，将状态变为未处理
		boolean mourFlag=false;
		boolean fareFlag=false;
		boolean artiFlag=false;
		furnaceRecordService.deleteFurnaceRecordUnLock(userId, Const.IsFlag_LSSD, null);
		//新加 订单流水号
		String ywddCode=jenerationService.getJenerationCode(Jeneration.YWDD_TYPE);
		
		CommissionOrder commissionOrder=new CommissionOrder();
		if(id!=null && !"".equals(id)){
			commissionOrder=getCommissionOrderById(id);
		}
		commissionOrder.setArriveTime(arriveTime);
		commissionOrder.setCorpseUnitId(corpseUnitId);
		commissionOrder.setBillUnitId(billUnitId);
		commissionOrder.setCremationTime(orderTime);
		
		//追加记录
		//以下重新添加记录条,并保存
		Map<String,Object> delMap=new HashMap<String,Object>();
		delMap.put("payFlag", Const.Pay_No);
		delMap.put("commissionOrderId", id);
		int x=itemOrderService.deleteItemOrderByMap(delMap);
		if(x>30){//若删除数据过多就特意报空，保存不成功
			System.out.println(x);
			itemOrderService.getItemOrderById(userId).getBillPayFlag();
		}
		//服务项目
		int sum=0;
		String pay="yes";
		byte LiYi = 2;
		Map<String,Object>map =new HashMap<String,Object>();
		byte num=0;
		if (service!=null&&!service.equals("")) {
			for (int i = 0; i < service.length; i++) {
				ItemOrder itemOrder=new ItemOrder();
				itemOrder.setId(UuidUtil.get32UUID());
				itemOrder.setItemId(itemId[sum]);
				itemOrder.setNumber(DataTools.stringToInt(number[sum]));
				itemOrder.setTickFlag(DataTools.stringToByte(bill[sum]));
				itemOrder.setTotal(DataTools.stringToDouble(total[sum]));
				itemOrder.setComment(comment[sum]);
				itemOrder.setPice(DataTools.stringToDouble(pice[sum]));
				//新加，订单流水号
				itemOrder.setOrderNumber(ywddCode);
				//判断服务项目里是否有礼仪出殡，通过助记码查找对应的itemId
				Item item = itemService.getItemByHelpCode("508");
				if(item.getId().equals(itemId[sum])){
					LiYi = 1;
				}
				//若是挂账业务,默认为已收费状态(此状态仅为判断使用,真实收费状态为 bill_pay_flag)
				if(itemOrder.getTickFlag()==Const.Is_Yes){
					itemOrder.setPayFlag(Const.Pay_Yes);
					itemOrder.setBillPayFlag(Const.Pay_No);//表数据已经默认为2了,不知道为什么添加之后还是为0
				}else{
					itemOrder.setPayFlag(Const.Pay_No);
					pay="no";
				}
				if (id!=null && !id.equals("")) {
    				itemOrder.setCommissionOrderId(id);
        		}
				
				map.put("itemId", itemId[sum]);
				num=itemService.getNumberByTypeFlag(map);
				if(num==5){//灵堂收费类
					mourFlag=true;
				}else if(num==4){//告别厅收费类
					fareFlag=true;
				}else if(num==15 || num==12){//丧葬用品类
					artiFlag=true;
				}
				
				itemOrderService.addItemOrder(itemOrder);;	
				sum++;
			}
		}
		//丧葬用品
		if (articles!=null&&!articles.equals("")) {
			for (int i = 0; i < articles.length; i++) {
				ItemOrder itemOrder=new ItemOrder();
				itemOrder.setId(UuidUtil.get32UUID());
				itemOrder.setItemId(itemId[sum]);
				itemOrder.setNumber(DataTools.stringToInt(number[sum]));
				itemOrder.setPice(DataTools.stringToDouble(pice[sum]));
				itemOrder.setTickFlag(DataTools.stringToByte(bill[sum]));
				itemOrder.setTotal(DataTools.stringToDouble(total[sum]));
				itemOrder.setComment(comment[sum]);
				//新加，订单流水号
				itemOrder.setOrderNumber(ywddCode);
				//若是挂账业务,默认为已收费状态(此状态仅为判断使用,真实收费状态为 bill_pay_flag)
				if(itemOrder.getTickFlag()==Const.Is_Yes){
					itemOrder.setPayFlag(Const.Pay_Yes);
					itemOrder.setBillPayFlag(Const.Pay_No);//表数据已经默认为2了,不知道为什么添加之后还是为0
				}else{
					itemOrder.setPayFlag(Const.Pay_No);
					pay="no";
				}
				if (id!=null && !id.equals("")) {
    				itemOrder.setCommissionOrderId(id);
        		}
				map.put("itemId", itemId[sum]);
				num=itemService.getNumberByTypeFlag(map);
				if(num==5){//灵堂收费类
					mourFlag=true;
				}else if(num==4){//告别厅收费类
					fareFlag=true;
				}else if(num==15 || num==12){//丧葬用品类
					artiFlag=true;
				}
				itemOrderService.addItemOrder(itemOrder);
				sum++;
			}
		}
		//新加循环原服务记录，看是否有   非挂账且未收费的	
		Map<String,Object> tickmap=new HashMap<String,Object>();
		tickmap.put("commissionOrderId", id);
		tickmap.put("tickFlag", Const.Is_No);
		tickmap.put("payFlag", Const.Pay_No);
		List<ItemOrder> unTickList=itemOrderService.getItemOrderList(tickmap);
		if(unTickList.size()<=0){//检查已有的服务记录，若没有非挂账的记录，更改火化委托单为已收费
			commissionOrder.setPayFlag(Const.Pay_Yes);
		}
		//表示 有添加过服务项目或者丧葬用品，则改变收费状态为未收费
		//且不是挂账的情况下
		if("no".equals(pay)){
			commissionOrder.setPayFlag(Const.Pay_No);
			commissionOrder.setScheduleFlag(Const.schedule_flag_1);
		}
		Map<String,Object> maps = new HashMap<String, Object>();
		maps.put("commissionOrderId", id);
		
		int oldCarList = 0;
		String oldPickTimeStr = "";
		String beforeOldPickTimeStr = "";
		if (id != null && !id.equals("")) {
			// 同上,先清空,再添加并保存
			List<CarSchedulRecord> cList = carSchedulRecordService.getCarSchedulRecordList(maps, "");
			oldCarList = cList.size();
			for (int j = 0; j < cList.size(); j++) {
				Timestamp oldPickTime = cList.get(j).getPickTime();
				oldPickTimeStr = DateTools.getTimeFormatString("yyyy-MM-dd", oldPickTime);
				beforeOldPickTimeStr = DateTools.getTimeFromatString("yyyy-MM-dd", DateTools.getDayAfterDay(DateTools.getDate(oldPickTime), -1, Calendar.DATE));
				carSchedulRecordService.deleteCarSchedulRecord(cList.get(j).getId());
				if (userNoticeService.ifDateExit(Const.NoticeType_CarTask, DateTools.getDate(oldPickTime))) {
					/*				userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_CarTask, DateTools.getDate(oldPickTime),
											-1);*/
					noticeHandleService.updateCarNotice(oldPickTime, Const.NoticeType_CarTask);
				}
			} 
		}
		//车辆调度
		if (carSchedulRecordId!=null&&!carSchedulRecordId.equals("")) {
			String day2 = DateTools.getTimeString("yyyy-MM-dd");
			String day1 = "";
			for (int i = 0; i < carSchedulRecordId.length; i++) {
				CarSchedulRecord carSchedulRecord=new CarSchedulRecord();
//						String carId=carSchedulRecordId[i];
//						if(carId!=null&&!carId.equals("")){
//							carSchedulRecord=carSchedulRecordService.getCarSchedulRecordById(carId);
//						}
				carSchedulRecord.setCarTypeId(carType[i]);
				carSchedulRecord.setComment(carComment[i]);
				carSchedulRecord.setPickTime(pickTime[i]);
				carSchedulRecord.setTransportTypeId(transportType[i]);
				if (id!=null&&!id.equals("")) {
					carSchedulRecord.setCommissionOrderId(id);
				}
				if (carSchedulRecord.getId()==null||carSchedulRecord.getId().equals("")) {
					carSchedulRecord.setId(UuidUtil.get32UUID());
					carSchedulRecordService.addCarSchedulRecord(carSchedulRecord);
				}else {
					carSchedulRecordService.updateCarSchedulRecord(carSchedulRecord);
				}
				if (userNoticeService.ifDateExit(Const.NoticeType_CarTask,DateTools.getDate(pickTime[i]))) {
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_CarTask, DateTools.getDate(pickTime[i]), 1);
				} else if(!userNoticeService.ifDateExit(Const.NoticeType_CarTask,DateTools.getDate(pickTime[i]))) {
					userNoticeService.addNoticeOfDate(Const.NoticeType_CarTask, DateTools.getDate(pickTime[i]));
				}
				//判断是否同天
				day1 = DateTools.getTimeFormatString("yyyy-MM-dd", pickTime[i]);
				java.sql.Date bed = DateTools.getDayAfterDay(DateTools.getDate(pickTime[i]), -1, Calendar.DATE);
				String beforeday =  DateTools.getTimeFromatString("yyyy-MM-dd", bed);
				if (oldCarList > 0) {
					if (!oldPickTimeStr.equals(day2) && !beforeOldPickTimeStr.equals(day2)
							&& (day1.equals(day2) || beforeday.equals(day2))) {
						String deptId = deptService.getDeptIdByName("二科");
						socketHandler.sendMessageToUsers(deptId, new TextMessage("carNumber"));
					}
				} else {
					if (day1.equals(day2) || beforeday.equals(day2)) {
						String deptId = deptService.getDeptIdByName("二科");
						socketHandler.sendMessageToUsers(deptId, new TextMessage("carNumber"));
					}
				}
			}
		}
		
		Timestamp now=DateTools.getThisDateTimestamp();
		byte checkFlag=commissionOrder.getCheckFlag();
		//灵堂
		commissionOrder.setMourningId(mourningId);
		commissionOrder.setMourningTime(mourningBeginTime);
		commissionOrder.setMourningEndTime(mourningEndTime);
		
		MourningRecord mourningRecord=mourningRecordService.getMourningRecordByOId(id);
		if(mourningRecord !=null){//当原来有灵堂记录时
			if(mourningId !=null && !"".equals(mourningId)){
				mourningRecord.setBeginTime(mourningBeginTime);
				mourningRecord.setEndTime(mourningEndTime);
				mourningRecord.setMourningId(mourningId);
				if(mourFlag){
					mourningRecord.setIsdel(Const.No_treated);
				}
				mourningRecordService.updateMourningRecord(mourningRecord);
			}else if(id!=null && !"".equals(id)){
				mourningRecordService.deleteMourningRecordByCoId(id);
			}
		}else{//当原来没有灵堂 记录时
			if(mourningId !=null && !"".equals(mourningId)){//只有当选择灵堂时才能添加灵堂 记录
				mourningRecord=new MourningRecord();
				if(checkFlag==Const.Check_Yes){
					mourningRecord.setFlag(Const.IsFlag_Yse);
				}else{
					mourningRecord.setFlag(Const.IsFlag_Lock);
				}
				mourningRecord.setId(UuidUtil.get32UUID());
				mourningRecord.setMourningId(mourningId);
				mourningRecord.setBeginTime(mourningBeginTime);
				mourningRecord.setEndTime(mourningEndTime);
				mourningRecord.setCreateTime(now);
				mourningRecord.setCreateUserId(userId);
				mourningRecord.setCommissionOrderId(id);
				mourningRecord.setIsdel(Const.No_treated);
				mourningRecordService.addMourningRecord(mourningRecord);
			}
		}
		//告别厅
		commissionOrder.setFarewellId(farewellId);
		commissionOrder.setFarewellTime(farewellBeginDate);
		
		FarewellRecord farewellRecord=farewellRecordService.getFarewellRecordByOrderId(id);
		if(farewellRecord!=null){//原来有的更新
			if(farewellId !=null && !"".equals(farewellId)){
				farewellRecord.setBeginDate(farewellBeginDate);
				farewellRecord.setFarewellId(farewellId);
				if(fareFlag){
					farewellRecord.setIsdel(Const.No_treated);
				}
				if(LiYi==Const.Is_Yes){
					farewellRecord.setIsLIYI(Const.Is_Yes);
				}
				farewellRecordService.updateFarewellRecord(farewellRecord);
			}else if(id!=null && !"".equals(id)){
				farewellRecordService.deleteFarewellRecordByCoId(id);
			}
			
		}else if(farewellId !=null && !"".equals(farewellId)){//原本没有的且现在有告别厅的
			farewellRecord=new FarewellRecord();
			farewellRecord.setId(UuidUtil.get32UUID());
			farewellRecord.setCommissionOrderId(id);
			farewellRecord.setBeginDate(farewellBeginDate);
			farewellRecord.setFarewellId(farewellId);
			farewellRecord.setCreateUserId(userId);
			farewellRecord.setCreateTime(now);
			farewellRecord.setIsdel(Const.No_treated);
			if(checkFlag==Const.Check_Yes){
				farewellRecord.setFlag(Const.IsFlag_Yse);
			}else{
				farewellRecord.setFlag(Const.IsFlag_Lock);
			}
			if(LiYi==Const.Is_Yes){
				farewellRecord.setIsLIYI(Const.Is_Yes);
			}
			farewellRecordService.addFarewellRecord(farewellRecord);
		}
		//火化炉
		commissionOrder.setFurnaceFlag(radio);
		commissionOrder.setFurnaceTypeId(furnaceTypeId);
		if(radio==Const.furnace_ty){
			commissionOrder.setFurnaceId(furnaceId);
		}else{
			commissionOrder.setFurnaceId(null);
		}
		if(radio==Const.furnace_ts){
			commissionOrder.setFurnaceComment(specialBusinessValue);//火化炉选择无时的备注
		}
		List<FurnaceRecord> furList=furnaceService.getFurnaceRecordList(maps);
		if(furList.size()>0 && radio==Const.furnace_ty){//原本有火化炉记录的更改
			FurnaceRecord furnaceRecord=furList.get(0);
			furnaceRecord.setFurnaceId(furnaceId);
			furnaceRecord.setOrderTime(orderTime);
			furnaceRecordService.updateFurnaceRecord(furnaceRecord);
		}else if(furList.size()<=0 && radio== Const.furnace_ty){
			FurnaceRecord furnaceRecord=new FurnaceRecord();
			furnaceRecord.setId(UuidUtil.get32UUID());
			furnaceRecord.setCommissionOrderId(id);
			furnaceRecord.setFurnaceId(furnaceId);
			furnaceRecord.setOrderTime(orderTime);
			furnaceRecord.setFlag(Const.IsFlag_Yse);
			furnaceRecord.setCreateTime(now);
			furnaceRecord.setCreateUserId(userId);
			furnaceRecordService.addFurnaceRecord(furnaceRecord);
		}
		if(furList.size()>0 && radio==Const.furnace_pt){//原本有火化炉记录的更改
			FurnaceRecord furnaceRecord=furList.get(0);
			furnaceRecord.setOrderTime(orderTime);
			furnaceRecord.setFurnaceId(null);
			furnaceRecordService.updateFurnaceRecord(furnaceRecord);
		}else if(furList.size()<=0 && radio== Const.furnace_pt){
			FurnaceRecord furnaceRecord=new FurnaceRecord();
			furnaceRecord.setId(UuidUtil.get32UUID());
			furnaceRecord.setCommissionOrderId(id);
			furnaceRecord.setOrderTime(orderTime);
			furnaceRecord.setFlag(Const.IsFlag_Yse);
			furnaceRecord.setCreateTime(now);
			furnaceRecord.setCreateUserId(userId);
			furnaceRecordService.addFurnaceRecord(furnaceRecord);
		}
		if(furList.size()>0 && radio==Const.furnace_ts){
			FurnaceRecord furnaceRecord=furList.get(0);
			furnaceRecordService.deleteFurnaceRecord(furnaceRecord.getId());
		}
		if(artiFlag){//改变丧葬用品的处理状态
			commissionOrder.setDealIsdel(Const.No_treated);
		}
		int sumR=0;
		//基本减免
		if (baseIs==Const.Is_Yes) {
			//减免人群
			String freePerson="";
			if (freePersonId != null && !freePersonId.equals("")) {
				for (int i = 0; i < freePersonId.length; i++) {
					if (i != 0) {
						freePerson += ",";
					}
					freePerson += freePersonId[i];
				} 
			}
			//困难证
			String hardPerson="";
			if (hardPersonId != null && !hardPersonId.equals("")) {
				for (int i = 0; i < hardPersonId.length; i++) {
					if (i != 0) {
						hardPerson += ",";
					}
					hardPerson += hardPersonId[i];
				} 
			}
			Map<String,Object> paramMap=new HashMap<String,Object>();
			paramMap.put("commissionOrderId", id);
			List<BaseReduction> baseList=baseReductionService.getBaseReductionList(paramMap);
			BaseReduction baseReduction=new BaseReduction();
			if(baseList.size()>0){//原来是基本减免的
				
				baseReduction=baseList.get(0);
				baseReductionDService.deleteBaseReductionD(id);
				baseReduction.setFidcard(fidcard);
				baseReduction.setFreePersonIds(freePerson);
				baseReduction.setHardPersonIds(hardPerson);
				baseReductionService.updateBaseReduction(baseReduction);
			}else{//原来不是基本减免的
				if (id!=null&&!id.equals("")) {
					baseReduction.setCommissionOrderId(id);
				}
				baseReduction.setCreateTime(DateTools.getThisDate());
				baseReduction.setFidcard(fidcard);
				baseReduction.setFreePersonIds(freePerson);
				baseReduction.setHardPersonIds(hardPerson);
				baseReduction.setId(UuidUtil.get32UUID());
				baseReductionService.addBaseReduction(baseReduction);
			}
			//清除之前的未收费的基本减免项目
			Map<String,Object> baseNotPayMap=new HashMap<String,Object>();
			baseNotPayMap.put("baseReductionId", baseReduction.getId());
			baseNotPayMap.put("payTime", 1);
			List<BaseReductionD> baseNotPayList=baseReductionDService.getBaseReductionDList(baseNotPayMap);
			for(int i=0;i<baseNotPayList.size();i++){
				baseReductionDService.deleteBaseReductionD(baseNotPayList.get(i).getId());
			}
			if (base!=null&&!"".equals(base)) {
				for (int i = 0; i < base.length; i++) {
					BaseReductionD baseReductionD=new BaseReductionD();
					baseReductionD.setItemId(itemIdR[sumR]);
					baseReductionD.setPice(DataTools.stringToDouble(piceR[sumR]));;
					baseReductionD.setNumber(DataTools.stringToInt(numberR[sumR]));
					baseReductionD.setTotal(DataTools.stringToDouble(totalR[sumR]));
					baseReductionD.setComment(commentR[sumR]);
					baseReductionD.setBaseReductionId(baseReduction.getId());
					//新加，订单流水号
					baseReductionD.setOrderNumber(ywddCode);
					baseReductionD.setId(UuidUtil.get32UUID());
					baseReductionDService.addBaseReductionD(baseReductionD);
					sumR++;
				}
			}
		}
		
		//困难减免
		String proveId="";
		if (hardIs==Const.Is_Yes ) {
			//证件类型
			for (int j = 0; j < proveIds.length; j++) {
				if (j!=0) {
					proveId+=",";
				}
				proveId+=proveIds[j];
			}
			HardReduction hardReduction=new HardReduction();
			Map<String,Object> paramMap=new HashMap<String,Object>();
			paramMap.put("commissionOrderId", id);
			List<HardReduction> hardList=hardReductionService.getHardReductionList(paramMap);
			if(hardList.size()>0){//原本有困难减免的
				hardReduction=hardList.get(0);
				hardReduction.setProveIds(proveId);
				hardReduction.setProveComment(hard_proveUnitId);
				hardReduction.setReason(reason);
				hardReduction.setComment(hComment);
				hardReduction.setCreateTime(createTime);
				hardReduction.setAppellationId(appellationId);
				hardReduction.setApplicant(applicant);
				hardReduction.setPhone(phone);
				hardReduction.setAgentUserId(userId);
				hardReduction.setSpecial(special);
				hardReductionService.updateHardReduction(hardReduction);
			}else{//原本没有困难减免的
				if (id!=null&&!id.equals("")) {
					hardReduction.setCommissionOrderId(id);
				}
				hardReduction.setProveIds(proveId);
				hardReduction.setProveComment(hard_proveUnitId);
				hardReduction.setReason(reason);
				hardReduction.setComment(hComment);
				hardReduction.setCreateTime(createTime);
				hardReduction.setAppellationId(appellationId);
				hardReduction.setApplicant(applicant);
				hardReduction.setPhone(phone);
				hardReduction.setAgentUserId(userId);
				hardReduction.setSpecial(special);
				String newHardReductionId=UuidUtil.get32UUID();
				hardReduction.setId(newHardReductionId);
    			hardReductionService.addHardReduction(hardReduction);
			}
			//清除之前的未收费的困难减免项目
			Map<String,Object> hardNotPayMap=new HashMap<String,Object>();
			hardNotPayMap.put("hardReductionId", hardReduction.getId());
			hardNotPayMap.put("payTime", 1);
			List<HardReductionD> hardNotPayList=hardReductionDService.getHardReductionDList(hardNotPayMap);
			for(int i=0;i<hardNotPayList.size();i++){
				hardReductionDService.deleteHardReductionD(hardNotPayList.get(i).getId());
			}
			double sTotal=0;
			int hardSum=0;
			//统计总金额
			if (hard!=null&&!hard.equals("")) {
				for (int i = 0; i < hard.length; i++) {
					HardReductionD hardReductionD=new HardReductionD();
					hardReductionD.setItemId(hardIdR[hardSum]);
					hardReductionD.setNumber(DataTools.stringToInt(hardnumberR[hardSum]));
					hardReductionD.setTotal(DataTools.stringToDouble(hardtotalR[hardSum]));
					hardReductionD.setPice(DataTools.stringToDouble(hardpiceR[hardSum]));
					hardReductionD.setComment(hardcommentR[hardSum]);
					hardReductionD.setHardReductionId(hardReduction.getId());
					//新加，订单流水号
					hardReductionD.setOrderNumber(ywddCode);
					sTotal+=DataTools.stringToDouble(hardtotalR[hardSum]);//统计减免金额
					hardReductionD.setId(UuidUtil.get32UUID());
					hardReductionDService.addHardReductionD(hardReductionD);
					hardSum++;
				}			
			}
//			hardReduction=hardReductionService.getHardReductionId(newHardReductionId);
			//判断金额
			if(sTotal>=1200){
				hardReduction.setCheckFlag(Const.Check_No);
			}else{
				hardReduction.setCheckFlag(Const.Check_Yes);
				hardReduction.setCheckTime(now);
				hardReduction.setCheckUserId(userId);
			}
			hardReductionService.updateHardReduction(hardReduction);
		}
		
		//更新火化委托单
		updateCommissionOrder(commissionOrder);
	}
	
	
	
	
	
	
	
	//添加或更新方法
	public String addOrUpdateMethod(String userId,String userName,String id,int age,Timestamp arriveTime,String cardCode,String certificateCode,
			String certificateId,Timestamp checkTime,String checkUserId,String cComment,String corpseUnitId,
			Timestamp cremationTime,String dAddr,String deadReasonId,String deadTypeId,byte dFlag ,String dIdcardPic,String dNation,
			Timestamp dTime,String eIdcardPic,String fAddr,String fAppellationId,String fName,String fPhone,String fUnit,String fCardCode,String furnaceTypeId,
			String furnaceId,String name,String pickAddr,String proveUnitId,String proveUnitContent,String registerId,String toponymId,byte scheduleFlag,
			byte sex,byte radio,String[] service, String[] articles, String[] itemId,String[] number,String[] bill,String[] total,
			String[] comment,String[] transportType,String[] carType,Timestamp[] pickTime,String[] carComment,String[] carSchedulRecordId,
			Timestamp farewellBeginDate,Timestamp farewellEndDate,String farewellId,String freezerId,Timestamp mourningBeginTime,
			Timestamp mourningEndTime,String mourningId,Timestamp orderTime,String[] itemIdR, String[] numberR, String[] totalR, String[] commentR,
			String[] base, String baseReductionId, String[] freePersonId, String[] hardPersonId,byte baseIs,String fidcard,String[] hard,
			String[] proveIds, String hardReductionId, byte hardIs,String hard_proveUnitId,String reason,String hComment,Timestamp createTime,
			String appellationId,String applicant,String phone,byte special,String billUnitId, String pbookAgentId,String bookAgentId,String assiginUser,byte freezerFlag
			,String[] hardIdR,String[] hardnumberR,String[] hardtotalR,String[] hardcommentR, String province,String city,String area,String specialBusinessValue,Appointment appointment
			,String[] pice,String[] hardpiceR,String[] piceR,String photoPic,String aisle,String isSH){
		
		
		//先处理一下锁定的灵堂/告别厅等
		farewellRecordService.deleteFarewellRecordByLock(userId, Const.IsFlag_LSSD,null);
		mourningRecordService.deleteMourningRecordUnlock(userId, Const.IsFlag_LSSD,null);
		furnaceRecordService.deleteFurnaceRecordUnLock(userId, Const.IsFlag_LSSD, null);
		//新加 订单流水号
		String ywddCode=jenerationService.getJenerationCode(Jeneration.YWDD_TYPE);
		
		String coId=UuidUtil.get32UUID();
		CommissionOrder commissionOrder=new CommissionOrder();
		CommissionOrder oldco = new CommissionOrder();
		List<ItemOrder> ItemlistSocket = null; 
		if (id!=null&&!id.equals("")) {
			commissionOrder=getCommissionOrderById(id);
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("commissionOrderId", id);
			map.put("type", Const.Type_Articles);
			ItemlistSocket = itemOrderService.getItemOrderListForNotice(map);//修改之前的用品收费项目
			oldco.setFarewellId(commissionOrder.getFarewellId());
			oldco.setFarewellTime(commissionOrder.getFarewellTime());
			oldco.setMourningId(commissionOrder.getMourningId());
			oldco.setMourningTime(commissionOrder.getMourningTime());
			oldco.setCreatTime(commissionOrder.getCreatTime());
			oldco.setCremationTime(commissionOrder.getCremationTime());
			oldco.setCheckTime(commissionOrder.getCheckTime());
		}
		commissionOrder.setProvince(province);
		commissionOrder.setCity(city);
		commissionOrder.setArea(area);
		commissionOrder.setAge(age);;
		commissionOrder.setArriveTime(arriveTime);
		commissionOrder.setCardCode(cardCode);
		commissionOrder.setCertificateCode(certificateCode);
		commissionOrder.setCertificateId(certificateId);
		
		
//		commissionOrder.setCheckUserId(checkUserId);
		commissionOrder.setComment(cComment);
		commissionOrder.setCorpseUnitId(corpseUnitId);
		commissionOrder.setCremationFlag(Const.furnace_flag_No);
		if(!"erKe".equals(aisle)){//只有火化委托单与永嘉时生效
			commissionOrder.setCremationTime(cremationTime);
			commissionOrder.setFurnaceFlag(radio);
			commissionOrder.setFurnaceTypeId(furnaceTypeId);
			
			if(radio!=Const.furnace_ty){//不是特约炉，设置炉id为null
				commissionOrder.setFurnaceId(null);
			}else{
				commissionOrder.setFurnaceId(furnaceId);
			}
			commissionOrder.setFurnaceComment(specialBusinessValue);//火化炉选择无时的备注
		}
		if("diaoDuKe".equals(aisle)){//只有是火化委托单修改时更改，限制二科与永嘉
			commissionOrder.setBillUnitId(billUnitId);//挂账单位
			commissionOrder.setPhotoPic(photoPic);//拍照存档
		}
		commissionOrder.setdIdcardPic(dIdcardPic);
		commissionOrder.seteIdcardPic(eIdcardPic);
		commissionOrder.setdAddr(dAddr);
		commissionOrder.setDeadReasonId(deadReasonId);
		commissionOrder.setDeadTypeId(deadTypeId);
		commissionOrder.setdFlag(dFlag);
		commissionOrder.setdNationId(dNation);
		commissionOrder.setdTime(dTime);
		commissionOrder.setfAddr(fAddr);
		commissionOrder.setfAppellationId(fAppellationId);
		commissionOrder.setfName(fName);
		commissionOrder.setfPhone(fPhone);
		commissionOrder.setfUnit(fUnit);
		commissionOrder.setfCardCode(fCardCode);
		commissionOrder.setName(name);
		commissionOrder.setPickAddr(pickAddr);
		commissionOrder.setProveUnitId(proveUnitId);
		commissionOrder.setProveUnitContent(proveUnitContent);
		commissionOrder.setRegisterId(registerId);
		commissionOrder.setToponymId(toponymId);
		commissionOrder.setSex(sex);
		commissionOrder.setPincardFlag(Const.pincard_No);
		commissionOrder.setAgentUserId(userId);
		commissionOrder.setDealIsdel(Const.No_treated);
		//新加个委托单 最新更新时间
		commissionOrder.setUpdateTime(DateTools.getThisDateTimestamp());
		if(id==null || "".equals(id)){
			commissionOrder.setScheduleFlag(Const.schedule_flag_1);
		}
//		commissionOrder.setScheduleFlag(Const.schedule_flag_1);  此处不能这么写，否则会强制改变火化单的状态进度
		/*commissionOrder.setPayFlag(Const.Pay_No);//新加，默认未收费状态
*/		commissionOrder.setDealIsdel(Const.No_treated);
		//  二科 登记 新增    科二登记人  电话预约登记人  到馆登记人
		if(commissionOrder.getPbookAgentId()==null || commissionOrder.getPbookAgentId().equals("")){	
			commissionOrder.setPbookAgentId(pbookAgentId);
		}
		if(commissionOrder.getBookAgentId()==null || commissionOrder.getBookAgentId().equals("")){
			commissionOrder.setBookAgentId(bookAgentId);	
		}
		
		/**
		 * 更新操作时获取 旧的收费记录的 应收金额
		 * 变更之后 收费项的处理
		 */
		// 收费项目  中 实际收的金额   和 找零
		double frontPay=0.0;
		double payAmount=0.0;
		double sumtotal=0.0;
		double odd=0.0;
		if(id!=null & id !=""){	//是否存在付费记录
			int i =delegrateService.getBoolPayed(id);
			if( (int)1 == i ){
				  payAmount   =  delegrateService.getpayAmountByorder(id);
				  odd  =delegrateService.getpayoddByorder(id);
				  sumtotal=payAmount-odd;
				//这里做调整，以免保存第二次收费之后，金额实收金额等发生变动，将对不上金额变成未收费状态
				  frontPay = delegrateService.getFrontPay(id);
			}
		}
		//收费项目   
		int sum=0;
		//保存之前先清空之前的记录条
		Map<String,Object> maps;
		maps = new HashMap<String, Object>();
		maps.put("commissionOrderId", id);
		List<ItemOrder> deleteList= itemOrderService.getItemOrderList(maps);
		//以下这段可能有问题，或多余
		Timestamp payTimeNew=null;
		byte payf=Const.Pay_No;
		for(int i=0;i<deleteList.size();i++){
			payTimeNew=deleteList.get(i).getPayTime();
			payf=deleteList.get(i).getPayFlag();
			if(payf==Const.Pay_Yes){//如果有一条收费过的记录就表示该火化委托单已收费过了，不可保存
				deleteList.get(-8).getComment();//故意报异常 阻止收费之后再次保存
			}
			itemOrderService.deleteItemOrder(deleteList.get(i).getId());
		}
		//以下重新添加记录条,并保存
		//服务项目
		byte LiYi = 2;
		if (service!=null&&!service.equals("")) {
			for (int i = 0; i < service.length; i++) {
				ItemOrder itemOrder=new ItemOrder();
//				if (service[i]!=null&&!service[i].equals("")) {
//					itemOrder=itemOrderService.getItemOrderById(service[i]);
//				}
				itemOrder.setItemId(itemId[sum]);
				itemOrder.setNumber(DataTools.stringToInt(number[sum]));
				itemOrder.setTickFlag(DataTools.stringToByte(bill[sum]));
				itemOrder.setTotal(DataTools.stringToDouble(total[sum]));
				itemOrder.setComment(comment[sum]);
				itemOrder.setPice(DataTools.stringToDouble(pice[sum]));
				itemOrder.setPayTime(payTimeNew);
				//新加，订单流水号
				itemOrder.setOrderNumber(ywddCode);
				//判断服务项目里是否有礼仪出殡，通过助记码查找对应的itemId
				Item item = itemService.getItemByHelpCode("508");
				if(item.getId().equals(itemId[sum])){
					LiYi = 1;
				}
				//若是挂账业务,默认为已收费状态(此状态仅为判断使用,真实收费状态为 bill_pay_flag)
				if(itemOrder.getTickFlag()==Const.Is_Yes){
					itemOrder.setPayFlag(Const.Pay_Yes);
					itemOrder.setBillPayFlag(Const.Pay_No);//表数据已经默认为2了,不知道为什么添加之后还是为0
				}else if(payf == Const.Pay_Yes){//删除前是已付款的，也要跟着变
					itemOrder.setPayFlag(Const.Pay_Yes);
				}else{
					itemOrder.setPayFlag(Const.Pay_No);
				}
				if (id!=null&&!id.equals("")) {
    				itemOrder.setCommissionOrderId(id);
        		}else{
        			itemOrder.setCommissionOrderId(coId);
        		}
    			if (itemOrder.getId()!=null&&!itemOrder.getId().equals("")) {
    				itemOrderService.updateItemOrder(itemOrder);
    			}else {
    				itemOrder.setId(UuidUtil.get32UUID());
    				itemOrderService.addItemOrder(itemOrder);;
    			}
				sum++;
				
			}
		}
		List<ItemOrder> articelsList=new ArrayList<ItemOrder>();//用于存放丧葬用品记录（丧葬用品任务单使用）
		//丧葬用品
		if (articles!=null&&!articles.equals("")) {
			for (int i = 0; i < articles.length; i++) {
				ItemOrder itemOrder=new ItemOrder();
//				if (articles[i]!=null&&!articles[i].equals("")) {
//					itemOrder=itemOrderService.getItemOrderById(articles[i]);
//				}
				itemOrder.setItemId(itemId[sum]);
				itemOrder.setNumber(DataTools.stringToInt(number[sum]));
				itemOrder.setPice(DataTools.stringToDouble(pice[sum]));
				itemOrder.setTickFlag(DataTools.stringToByte(bill[sum]));
				itemOrder.setTotal(DataTools.stringToDouble(total[sum]));
				itemOrder.setComment(comment[sum]);
				itemOrder.setPayTime(payTimeNew);
				//新加，订单流水号
				itemOrder.setOrderNumber(ywddCode);
				//若是挂账业务,默认为已收费状态(此状态仅为判断使用,真实收费状态为 bill_pay_flag)
				if(itemOrder.getTickFlag()==Const.Is_Yes){
					itemOrder.setPayFlag(Const.Pay_Yes);
					itemOrder.setBillPayFlag(Const.Pay_No);//表数据已经默认为2了,不知道为什么添加之后还是为0
				}else if(payf == Const.Pay_Yes){//删除前是已付款的，也要跟着变
					itemOrder.setPayFlag(Const.Pay_Yes);
				}else{
					itemOrder.setPayFlag(Const.Pay_No);
				}
				
				if (id!=null&&!id.equals("")) {
    				itemOrder.setCommissionOrderId(id);
        		}else{
        			itemOrder.setCommissionOrderId(coId);
        		}
    			if (itemOrder.getId()!=null&&!itemOrder.getId().equals("")) {
    				itemOrderService.updateItemOrder(itemOrder);
    				articelsList.add(itemOrder);
    			}else {
    				itemOrder.setId(UuidUtil.get32UUID());
    				itemOrderService.addItemOrder(itemOrder);
    				articelsList.add(itemOrder);
    			}
				sum++;
			}
		}
		//一科丧葬用品任务单标示修改功能
		String newType="";		
		if(id != null && !id.equals("")){
			List<ItemOrder> newList= itemBeforeService.getItemOrderBeforeList(maps);
			if(newList.size()==0){
				newType="before";
				for(int i=0;i<articelsList.size();i++){
					itemBeforeService.saveItemOrderBefore(articelsList.get(i));
				}
			}else{
				newType="newBefore";
			}
		}
		if(newType.equals("newBefore")){//进行对比，然后判断是否  改变了
			List<ItemOrder> beforeList=itemBeforeService.getItemOrderBeforeList(maps);
			Map<String,Double> map=new HashMap<String,Double>();
			List<String> sortList=new ArrayList<String>();
			for(int i=0;i<articelsList.size();i++){//按sort累加
				articelsList.get(i).getItemId();
				String sort=String.valueOf(itemService.getItemById(articelsList.get(i).getItemId()).getSort());//此处无法直接获取item,只能先通过itemID
				double newTotal=articelsList.get(i).getTotal();
				
				if(sortList.contains(sort)==true){//判断，若之前map有的，则累加
					map.put(sort, map.get(sort)+newTotal);
				}else{//否则 创建及 添加到list中
					sortList.add(sort);
					map.put(sort, newTotal);
				}
			}
			Map<String,Double> beforeMap=new HashMap<String,Double>();
			List<String> sortBeforeList=new ArrayList<String>();
			for(int j=0;j<beforeList.size();j++){//同上
				String sort=String.valueOf(itemService.getItemById(beforeList.get(j).getItemId()).getSort());
				double beforeTotal=beforeList.get(j).getTotal();
				if(sortBeforeList.contains(sort)==true){//判断，若之前map有的，则累加
					beforeMap.put(sort, beforeMap.get(sort)+beforeTotal);
				}else{//否则 创建及 添加到list中
					sortBeforeList.add(sort);
					beforeMap.put(sort, beforeTotal);
				}
			}
				
			List<ItemChange> itemChangeList=new ArrayList<ItemChange>();//记录所有更改的地方记录
			if(map.size()>=beforeMap.size() && map.size()>0){//以多的map来比较少的map
				Set<String> set=map.keySet();
				for(String in:set){
					double a=map.get(in); //这里一定要先将value值取出来，然后再做比较，否则永远不相同
					double b=0;
					if(beforeMap.containsKey(in)){
						b=beforeMap.get(in);
					}
					if(a!=b){//不相同，即改变了 commission_id为id
						ItemChange itemChange=new ItemChange();
						itemChange.setCommissionOrderId(id);
						itemChange.setSort(in);
						itemChange.setId(UuidUtil.get32UUID());
						itemChangeList.add(itemChange);
					}
				}
			}else if(beforeMap.size()>=map.size() && beforeMap.size()>0){
				Set<String> set=beforeMap.keySet();
				for(String in:set){
					double a=beforeMap.get(in);
					double b=0;
					if(map.containsKey(in)){
						b=map.get(in);
					}
					if(a!=b){//不相同，即改变了 commission_id为id
						ItemChange itemChange=new ItemChange();
						itemChange.setCommissionOrderId(id);
						itemChange.setSort(in);
						itemChange.setId(UuidUtil.get32UUID());
						itemChangeList.add(itemChange);
					}
				}
			}
			//同样，改变的记录也是先清空后保存
			itemChangeService.delItemChangeBycomId(id);
			for(int i=0;i<itemChangeList.size();i++){//将改变的记录 保存到y_item_change表中
				itemChangeService.saveItemChange(itemChangeList.get(i));
			}
			if(itemChangeList.size()>0){				
				//委托业务的领用时间：按顺序分别为告别时间·火化时间·守灵时间·审核时间
				Date dateTime=null;
				Date today=DateTools.getThisDate();
				if(farewellBeginDate!=null){
					dateTime=DateTools.getDate(farewellBeginDate);
				}else if(commissionOrder.getCremationTime()!=null){
					dateTime=DateTools.getDate(commissionOrder.getCremationTime());
				}else if(commissionOrder.getCheckTime()!=null){
					dateTime=DateTools.getDate(commissionOrder.getCheckTime());
				}
				//以下是消息提醒功能
				if (userNoticeService.ifDateExit(Const.NoticeType_FuneralTask,dateTime)) {
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FuneralTask, dateTime, 1);
				} else if(!userNoticeService.ifDateExit(Const.NoticeType_FuneralTask,dateTime)) {
					userNoticeService.addNoticeOfDate(Const.NoticeType_FuneralTask, dateTime);
				}
				if(dateTime!=null){
					int i=DateTools.compareSqlDate(today, dateTime);
					if (i==0) {//是否需当天 是则发送通知
						String deptId = deptService.getDeptIdByName("一科");
						socketHandler.sendMessageToUsers(deptId, new TextMessage("funeralNumber"));
					}
				}
			}
			//同样，改变前的记录也是先清空后保存
			itemBeforeService.delItemOrderBeforeBycomId(id);
			for(int i=0;i<articelsList.size();i++){
				itemBeforeService.saveItemOrderBefore(articelsList.get(i));
			}
		}
		int oldCarList = 0;
		String oldPickTimeStr = "";
		String beforeOldPickTimeStr = "";
		if (id != null && !id.equals("")) {
			// 同上,先清空,再添加并保存
			List<CarSchedulRecord> cList = carSchedulRecordService.getCarSchedulRecordList(maps, "");
			oldCarList = cList.size();
			for (int j = 0; j < cList.size(); j++) {
				Timestamp oldPickTime = cList.get(j).getPickTime();
				oldPickTimeStr = DateTools.getTimeFormatString("yyyy-MM-dd", oldPickTime);
				beforeOldPickTimeStr = DateTools.getTimeFromatString("yyyy-MM-dd", DateTools.getDayAfterDay(DateTools.getDate(oldPickTime), -1, Calendar.DATE));
				carSchedulRecordService.deleteCarSchedulRecord(cList.get(j).getId());
				if (userNoticeService.ifDateExit(Const.NoticeType_CarTask, DateTools.getDate(oldPickTime))) {
					/*				userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_CarTask, DateTools.getDate(oldPickTime),
											-1);*/
					noticeHandleService.updateCarNotice(oldPickTime, Const.NoticeType_CarTask);
				}
			} 
		}
		//车辆调度
		if (carSchedulRecordId!=null&&!carSchedulRecordId.equals("")) {
			String day2 = DateTools.getTimeString("yyyy-MM-dd");
			String day1 = "";
			for (int i = 0; i < carSchedulRecordId.length; i++) {
				CarSchedulRecord carSchedulRecord=new CarSchedulRecord();
//				String carId=carSchedulRecordId[i];
//				if(carId!=null&&!carId.equals("")){
//					carSchedulRecord=carSchedulRecordService.getCarSchedulRecordById(carId);
//				}
				carSchedulRecord.setCarTypeId(carType[i]);
				carSchedulRecord.setComment(carComment[i]);
				carSchedulRecord.setPickTime(pickTime[i]);
				carSchedulRecord.setTransportTypeId(transportType[i]);
				if (id!=null&&!id.equals("")) {
					carSchedulRecord.setCommissionOrderId(id);
				}else{
					carSchedulRecord.setCommissionOrderId(coId);
				}
				if (carSchedulRecord.getId()==null||carSchedulRecord.getId().equals("")) {
					carSchedulRecord.setId(UuidUtil.get32UUID());
					carSchedulRecordService.addCarSchedulRecord(carSchedulRecord);
				}else {
					carSchedulRecordService.updateCarSchedulRecord(carSchedulRecord);
				}
				if (userNoticeService.ifDateExit(Const.NoticeType_CarTask,DateTools.getDate(pickTime[i]))) {
					userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_CarTask, DateTools.getDate(pickTime[i]), 1);
				} else if(!userNoticeService.ifDateExit(Const.NoticeType_CarTask,DateTools.getDate(pickTime[i]))) {
					userNoticeService.addNoticeOfDate(Const.NoticeType_CarTask, DateTools.getDate(pickTime[i]));
				}
				//判断是否同天
				day1 = DateTools.getTimeFormatString("yyyy-MM-dd", pickTime[i]);
				java.sql.Date bed = DateTools.getDayAfterDay(DateTools.getDate(pickTime[i]), -1, Calendar.DATE);
				String beforeday =  DateTools.getTimeFromatString("yyyy-MM-dd", bed);
				if (oldCarList > 0) {
					if (!oldPickTimeStr.equals(day2) && !beforeOldPickTimeStr.equals(day2)
							&& (day1.equals(day2) || beforeday.equals(day2))) {
						String deptId = deptService.getDeptIdByName("二科");
						socketHandler.sendMessageToUsers(deptId, new TextMessage("carNumber"));
					}
				} else {
					if (day1.equals(day2) || beforeday.equals(day2)) {
						String deptId = deptService.getDeptIdByName("二科");
						socketHandler.sendMessageToUsers(deptId, new TextMessage("carNumber"));
					}
				}
			}
		}
		
		
		if (farewellId != null && !farewellId.equals("")) {//此处做限制，火化委托单中没有就需要删除记录
			//告别厅使用记录
			FarewellRecord farewellRecord = new FarewellRecord();
			farewellRecord.setFlag(Const.IsFlag_Lock);
			if (id != null && !id.equals("")) {
				Map<String, Object> farewellMaps = new HashMap<String, Object>();
				farewellMaps.put("commissionOrderId", id);
				List<FarewellRecord> farewellList = farewellRecordService.getFarewellRecordList(farewellMaps, "");
				if (farewellList.size() > 0) {
					farewellRecord = (FarewellRecord) farewellList.get(0);
				}
				if (commissionOrder.getCheckFlag() == Const.Check_Yes) {
					//如果告别厅状态为布置完成 ，则更改火化委托单后告别厅布置状态不变（暂不考虑更改项是否影响告别厅布置（“”））
					if(farewellRecord.getFlag() != Const.IsFlag_Bzwc){
							farewellRecord.setFlag(Const.IsFlag_Yse);
					}
				} else {
					farewellRecord.setFlag(Const.IsFlag_Lock);
				}
			}
			farewellRecord.setBeginDate(farewellBeginDate);
			farewellRecord.setEndDate(farewellEndDate);
			farewellRecord.setCreateTime(DateTools.getThisDateTimestamp());
			farewellRecord.setCreateUserId(userId);
			farewellRecord.setFarewellId(farewellId);
			farewellRecord.setIsLIYI(LiYi);
			farewellRecord.setIsdel(Const.No_treated);
			if (id != null && !id.equals("")) {
				farewellRecord.setCommissionOrderId(id);
			} else {
				farewellRecord.setCommissionOrderId(coId);
			}
			if (farewellRecord.getId() != null && !farewellRecord.getId().equals("")) {
				farewellRecordService.updateFarewellRecord(farewellRecord);
			} else {
				farewellRecord.setId(UuidUtil.get32UUID());
				farewellRecordService.addFarewellRecord(farewellRecord);
			}
			commissionOrder.setFarewellId(farewellId);
			commissionOrder.setFarewellTime(farewellRecord.getBeginDate());
		}else if("diaoDuKe".equals(aisle) && ("".equals(farewellId) || farewellId == null)){
			if(id !=null && !"".equals(id)){
				farewellRecordService.deleteFarewellRecordByCoId(id);
			}
			commissionOrder.setFarewellId(null);
			commissionOrder.setFarewellTime(null);
		}
		//冷藏柜使用记录
		Timestamp  oldAutoTime =  freezerRecordService.getAutoTimeByOrderId(id);
		if ("erKe".equals(aisle) && freezerId != null && !freezerId.equals("")) {
			FreezerRecord freezerRecord=new FreezerRecord();
			if(id!=null&&!id.equals("")){
				Map<String, Object> freezerMaps=new HashMap<String, Object>();
				freezerMaps.put("commissionOrderId", id);
		
				List<FreezerRecord> freezerList=freezerRecordService.getFreezerRecordList(freezerMaps);
				if (freezerList.size()>0) {
					freezerRecord=(FreezerRecord) freezerList.get(0);
					Freezer freezer=freezerService.getFreezerById(freezerRecord.getFreezerId());
					freezer.setFlag(Const.IsFlag_No);
					
					freezerService.updateFreezer(freezer);
				}
			}
			freezerRecord.setBeginDate(arriveTime);
			freezerRecord.setCreatTime(DateTools.getThisDateTimestamp());
			freezerRecord.setCreateUserId(userId);
			freezerRecord.setFreezerId(freezerId);
			freezerRecord.setIntoOperator(userName);//新加保存二科/调度科有冰柜时 同时将录入员保存
			// 二科登记  时 默认 已入柜   
			//  调度科 对二科登记的内容进行修改  时 进行 判别
			if(freezerFlag==Const.IsFlag_Yse){
				freezerRecord.setFlag(Const.IsFlag_Yse);
			}else{
				if(freezerRecord.getFlag()==Const.IsFlag_Yse){
					freezerRecord.setFlag(Const.IsFlag_Yse);
				}else{
					freezerRecord.setFlag(Const.IsFlag_Lock);
				}
			}
			
			
			if( !assiginUser.equals("") && assiginUser!=null){
				freezerRecord.setIntoStaff(assiginUser);
			}

			if (id!=null&&!id.equals("")) {
				freezerRecord.setCommissionOrderId(id);
			}else{
				freezerRecord.setCommissionOrderId(coId);
			}
			
			//设置解冻时间   根据告别厅 时间 
			//1.如果有灵堂时间，就取灵堂时间，2.如果有告别厅时间，就取告别时间，否者就取火化预约时间   来计算 解冻时间  存放超过七天 取2天  不超过取一天
			Timestamp autoTime =  freezerRecordService.getautoTime(arriveTime, mourningBeginTime, farewellBeginDate , cremationTime);
			freezerRecord.setAutoTime(autoTime);
			
			if (freezerRecord.getId()!=null&&!freezerRecord.getId().equals("")) {
				freezerRecordService.updateFreezerRecord(freezerRecord);
			}else {	
				freezerRecord.setId(UuidUtil.get32UUID());
				freezerRecordService.addFreezerRecord(freezerRecord);
			}
			commissionOrder.setFreezerId(freezerId);
			
			if (freezerId!=null&&!freezerId.equals("")) {
				Freezer freezer=freezerService.getFreezerById(freezerId);
				//以判断来区分   值是来自  二科登记 还是 调度科的火化委托单 新增或更改
				if(freezerFlag == Const.IsFlag_Yse){
					freezer.setFlag(Const.IsFlag_Yse);
				}else{
					if(freezer.getFlag()==Const.IsFlag_Yse){
						freezer.setFlag(Const.IsFlag_Yse);
					}
					else{
					freezer.setFlag(Const.IsFlag_Lock);
					}
				}
				freezer.setUserName(userName);
				freezer.setsName(commissionOrder.getName());
				freezerService.updateFreezer(freezer);
			}		
		}
		
		if (mourningId != null && !mourningId.equals("")) {
			//守灵室使用记录
			MourningRecord mourningRecord = new MourningRecord();
			mourningRecord.setFlag(Const.IsFlag_Lock);
			if (id != null && !id.equals("")) {
				Map<String, Object> mourningMaps = new HashMap<String, Object>();
				mourningMaps.put("commissionOrderId", id);
				List<MourningRecord> mourningList = mourningRecordService.getMourningRecordList(mourningMaps, "");
				if (mourningList.size() > 0) {
					mourningRecord = mourningList.get(0);
				}
				if (commissionOrder.getCheckFlag() == Const.Check_Yes) {
					// 如果守灵室状态为布置完成的话，在更改火化委托单后  （暂不考虑更改项是否涉及到守灵室（守灵室号，守灵时间，灵堂服务项目））布置状态不变
						if(mourningRecord.getFlag() != Const.IsFlag_Bzwc){
							mourningRecord.setFlag(Const.IsFlag_Yse);
						}
				} else {
					mourningRecord.setFlag(Const.IsFlag_Lock);
				}
			}
			mourningRecord.setBeginTime(mourningBeginTime);
			mourningRecord.setEndTime(mourningEndTime);
			mourningRecord.setCreateTime(DateTools.getThisDateTimestamp());
			mourningRecord.setCreateUserId(userId);
			mourningRecord.setMourningId(mourningId);
			mourningRecord.setIsdel(Const.No_treated);
			if (id != null && !id.equals("")) {
				mourningRecord.setCommissionOrderId(id);
			} else {
				mourningRecord.setCommissionOrderId(coId);
			}
			if (mourningRecord.getId() != null && !mourningRecord.getId().equals("")) {
				mourningRecordService.updateMourningRecord(mourningRecord);
			} else {
				mourningRecord.setId(UuidUtil.get32UUID());
				mourningRecordService.addMourningRecord(mourningRecord);
			}
			commissionOrder.setMourningId(mourningId);
			commissionOrder.setMourningTime(mourningRecord.getBeginTime());
			commissionOrder.setMourningEndTime(mourningRecord.getEndTime());
		}else if("diaoDuKe".equals(aisle) && ("".equals(mourningId) || mourningId == null)){
			//当调度科重置时再保存的处理
			if(id !=null && !"".equals(id)){
				mourningRecordService.deleteMourningRecordByCoId(id);
			}
			commissionOrder.setMourningId(null);
			commissionOrder.setMourningTime(null);
			commissionOrder.setMourningEndTime(null);
		}
		if (radio!=Const.furnace_ts && !"erKe".equals(aisle)) {
			//火化炉使用记录
			FurnaceRecord furnaceRecord = new FurnaceRecord();
			furnaceRecord.setFlag(Const.IsFlag_Lock);
			if (id != null && !id.equals("") ) {
				Map<String, Object> furnaceMaps = new HashMap<String, Object>();
				furnaceMaps.put("commissionOrderId", id);
				List<FurnaceRecord> furnaceList = furnaceService.getFurnaceRecordList(furnaceMaps);
				if (furnaceList.size() > 0) {
					furnaceRecord = (FurnaceRecord) furnaceList.get(0);
				} else {
					furnaceRecord.setCommissionOrderId(id);
				}
				if (commissionOrder.getCheckFlag() == Const.Check_Yes) {
					furnaceRecord.setFlag(Const.IsFlag_Yse);
				} else {
					furnaceRecord.setFlag(Const.IsFlag_Lock);
				}
			} else {
				furnaceRecord.setCommissionOrderId(coId);
			}
			furnaceRecord.setOrderTime(orderTime);
			furnaceRecord.setCreateTime(DateTools.getThisDateTimestamp());
			furnaceRecord.setCreateUserId(userId);
			if(radio!=Const.furnace_ty){//普通炉或特殊业务 将炉id设置为空
				furnaceRecord.setFurnaceId(null);
			}else{
				furnaceRecord.setFurnaceId(furnaceId);
			}
			if (id != null && !id.equals("") && !"erKe".equals(aisle)) {
				if (furnaceRecord.getId() != null && !"".equals(furnaceRecord.getId())) {//之前填写过火化炉的
					if (radio == Const.furnace_pt && commissionOrder.getFurnaceFlag() == Const.furnace_ty) {//从特约炉改为普通炉
						furnaceRecord.setFurnaceId(null);
						commissionOrder.setFurnaceId(null);
						furnaceRecordService.updateFurnaceRecord(furnaceRecord);
					} else if (radio == Const.furnace_ts) {
						furnaceRecordService.deleteFurnaceRecord(furnaceRecord.getId());
					} else {
						furnaceRecordService.updateFurnaceRecord(furnaceRecord);
					}
				} else {
					if (radio == Const.furnace_ty) {
						commissionOrder.setFurnaceId(furnaceId);
					}
					furnaceRecord.setId(UuidUtil.get32UUID());
					furnaceRecordService.addFurnaceRecord(furnaceRecord);
				}
			} else if(id == null || "".equals(id)){
				if (radio == Const.furnace_ty) {
					commissionOrder.setFurnaceId(furnaceId);
				}
				furnaceRecord.setId(UuidUtil.get32UUID());
				furnaceRecordService.addFurnaceRecord(furnaceRecord);
			}
			/*if (radio != Const.furnace_ts) {
				commissionOrder.setFurnaceFlag(radio);
			}*/
		}
		//修改火化委托单撤销灵堂、火化炉、告别厅、冰柜的时候，要删除之前的记录。之前没这个操作。
		if (id !=null && !id.equals("") && !"erKe".equals(aisle)) {
			if ("diaoDuke".equals(aisle) && (farewellId==null || farewellId.trim().equals(""))) {
				farewellRecordService.deleteFarewellRecordByCoId(id);
				commissionOrder.setFarewellId(null);
				commissionOrder.setFarewellTime(null);
			}
			if ("diaoDuke".equals(aisle) && (mourningId==null || mourningId.trim().equals(""))) {
				mourningRecordService.deleteMourningRecordByCoId(id);
				commissionOrder.setMourningId(null);
				commissionOrder.setMourningTime(null);
				commissionOrder.setMourningEndTime(null);
			}
			if (radio==Const.furnace_ts) {
				furnaceRecordService.deleteFurnaceRecordByCoId(id);
				commissionOrder.setFurnaceId(null);
				commissionOrder.setFurnaceFlag(Const.furnace_ts);
				commissionOrder.setCremationTime(null);
			}			
		}else if(id !=null && !id.equals("")){
			if (freezerId==null || freezerId.trim().trim().equals("")) {
				FreezerRecord fr = freezerRecordService.getFreezerRecordByCoId(id);
				if (fr != null && fr.getFreezerId()!=null && !fr.getFreezerId().trim().equals("")) {
					freezerService.clearFreezer(fr.getFreezerId(),Const.IsFlag_No);
					freezerRecordService.deleteFreezerRecordByCoId(id);
				}
			}
		}
		int sumR=0;
		
		//基本减免
	
		if (baseIs==Const.Is_Yes && "diaoDuKe".equals(aisle)) {
			//先清空(解决保存按钮重复添加问题)  此处需做修改，否则二科修改一下，火化委托单里的基本减免就没了
			Map<String,Object> paramMap=new HashMap<String,Object>();
			paramMap.put("commissionOrderId", id);
			List<BaseReduction> baseList=baseReductionService.getBaseReductionList(paramMap);
			for(int i=0;i<baseList.size();i++){
				Map<String,Object> mapd=new HashMap<String,Object>();
				mapd.put("baseReductionId", baseList.get(i).getId());
				List<BaseReductionD> baseListD=baseReductionDService.getBaseReductionDList(mapd);
				for(int j=0;j<baseListD.size();j++){
					baseReductionDService.deleteBaseReductionD(baseListD.get(j).getId());
				}
				baseReductionService.deleteBaseReduction(baseList.get(i).getId());
			}
			
			BaseReduction baseReduction=new BaseReduction();
			/*if (baseReductionId!=null&&!baseReductionId.equals("")) {
				baseReduction=baseReductionService.getBaseReductionId(baseReductionId);
			}*/
			if (id!=null&&!id.equals("")) {
				baseReduction.setCommissionOrderId(id);
			}else{
				baseReduction.setCommissionOrderId(coId);
    		}
			baseReduction.setCreateTime(DateTools.getThisDate());
			baseReduction.setFidcard(fidcard);
			String freePerson="";
			if (freePersonId != null && !freePersonId.equals("")) {
				for (int i = 0; i < freePersonId.length; i++) {
					if (i != 0) {
						freePerson += ",";
					}
					freePerson += freePersonId[i];
				} 
			}
			baseReduction.setFreePersonIds(freePerson);
			String hardPerson="";
			if (hardPersonId != null && !hardPersonId.equals("")) {
				for (int i = 0; i < hardPersonId.length; i++) {
					if (i != 0) {
						hardPerson += ",";
					}
					hardPerson += hardPersonId[i];
				} 
			}
			baseReduction.setHardPersonIds(hardPerson);
			if (baseReduction.getId()!=null&&!baseReduction.getId().equals("")) {
				baseReductionService.updateBaseReduction(baseReduction);
			}else {
				baseReduction.setId(UuidUtil.get32UUID());
				baseReductionService.addBaseReduction(baseReduction);
			}
			
			
			if (base!=null&&!"".equals(base)) {
				for (int i = 0; i < base.length; i++) {
					BaseReductionD baseReductionD=new BaseReductionD();
					/*if (base[i]!=null&&!base[i].equals("")) {
						baseReductionD=baseReductionDService.getBaseReductionDId(base[i]);
					}*/
					baseReductionD.setItemId(itemIdR[sumR]);
					baseReductionD.setPice(DataTools.stringToDouble(piceR[sumR]));;
					baseReductionD.setNumber(DataTools.stringToInt(numberR[sumR]));
					baseReductionD.setTotal(DataTools.stringToDouble(totalR[sumR]));
					baseReductionD.setComment(commentR[sumR]);
					baseReductionD.setBaseReductionId(baseReduction.getId());
					//新加，订单流水号
					baseReductionD.setOrderNumber(ywddCode);
					if (baseReductionD.getId()!=null&&!"".equals(baseReductionD.getId())) {
						baseReductionDService.updateBaseReductionD(baseReductionD);
					}else {
						baseReductionD.setId(UuidUtil.get32UUID());
						baseReductionDService.addBaseReductionD(baseReductionD);
					}
					sumR++;
				}
			}
		}else {
			if("diaoDuKe".equals(aisle)){
				Map<String, Object> bMaps=new HashMap<String, Object>();
				bMaps.put("commissionOrderId", id);
				List<BaseReduction> list=baseReductionService.getBaseReductionList(bMaps);
				if (list.size()>0) {
					BaseReduction baseReduction=list.get(0);
					Map<String, Object> b1Maps=new HashMap<String, Object>();
					b1Maps.put("baseReductionId", baseReduction.getId());
					List<BaseReductionD> list1=baseReductionDService.getBaseReductionDList(b1Maps);
					for (int i = 0; i < list1.size(); i++) {
						BaseReductionD baseReductionD=list1.get(i);
						baseReductionDService.deleteBaseReductionD(baseReductionD.getId());
					}
					baseReductionService.deleteBaseReduction(baseReduction.getId());
				}
			}
		}
		
		//困难减免
		String proveId="";
		if (hardIs==Const.Is_Yes && "diaoDuKe".equals(aisle)) {
			//先清空(解决保存按钮重复添加问题)
			Map<String,Object> paramMap=new HashMap<String,Object>();
			paramMap.put("commissionOrderId", id);
			List<HardReduction> hardList=hardReductionService.getHardReductionList(paramMap);
			for(int i=0;i<hardList.size();i++){
				Map<String,Object> mapd=new HashMap<String,Object>();
				mapd.put("hardReductionId", hardList.get(i).getId());
				List<HardReductionD> hardListD=hardReductionDService.getHardReductionDList(mapd);
				for(int j=0;j<hardListD.size();j++){
					hardReductionDService.deleteHardReductionD(hardListD.get(j).getId());
				}
				hardReductionService.deleteHardReduction(hardList.get(i).getId());
			}
			
			HardReduction hardReduction=new HardReduction();
			/*if (hardReductionId!=null&&!hardReductionId.equals("")) {
				hardReduction=hardReductionService.getHardReductionId(hardReductionId);
			}*/
			if (id!=null&&!id.equals("")) {
				hardReduction.setCommissionOrderId(id);
			}else{
				hardReduction.setCommissionOrderId(coId);
    		}
			//证件类型
			for (int i = 0; i < proveIds.length; i++) {
				if (i!=0) {
					proveId+=",";
				}
				proveId+=proveIds[i];
			}
			
			hardReduction.setProveIds(proveId);
			hardReduction.setProveComment(hard_proveUnitId);
			hardReduction.setReason(reason);
			hardReduction.setComment(hComment);
			hardReduction.setCreateTime(createTime);
			hardReduction.setAppellationId(appellationId);
			hardReduction.setApplicant(applicant);
			hardReduction.setPhone(phone);
			hardReduction.setAgentUserId(userId);
			hardReduction.setSpecial(special);
			
			String newHardReductionId="";
			if (hardReduction.getId()!=null&&!hardReduction.getId().equals("")) {
				hardReductionService.updateHardReduction(hardReduction);
			}else {
				newHardReductionId=UuidUtil.get32UUID();
				hardReduction.setId(newHardReductionId);
    			hardReductionService.addHardReduction(hardReduction);
			}			
			double sTotal=0;
			int hardSum=0;
			//统计总金额
			if (hard!=null&&!hard.equals("")) {				
				for (int i = 0; i < hard.length; i++) {
					HardReductionD hardReductionD=new HardReductionD();
					/*if (hard[i]!=null&&!hard[i].equals("")) {
						hardReductionD=hardReductionDService.getHardReductionDId(hard[i]);
					}*/
					hardReductionD.setItemId(hardIdR[hardSum]);
					hardReductionD.setNumber(DataTools.stringToInt(hardnumberR[hardSum]));
					hardReductionD.setTotal(DataTools.stringToDouble(hardtotalR[hardSum]));
					hardReductionD.setPice(DataTools.stringToDouble(hardpiceR[hardSum]));
					hardReductionD.setComment(hardcommentR[hardSum]);
					hardReductionD.setHardReductionId(hardReduction.getId());
					//新加，订单流水号
					hardReductionD.setOrderNumber(ywddCode);
					
					
					sTotal+=DataTools.stringToDouble(hardtotalR[hardSum]);//统计减免金额
					if (hardReductionD.getId()!=null&&!"".equals(hardReductionD.getId())) {
						hardReductionDService.updateHardReductionD(hardReductionD);
					}else {
						hardReductionD.setId(UuidUtil.get32UUID());
						hardReductionDService.addHardReductionD(hardReductionD);
					}
					hardSum++;
				}			
			}
			hardReduction=hardReductionService.getHardReductionId(newHardReductionId);
			//判断金额
			if(sTotal>=1200){
				hardReduction.setCheckFlag(Const.Check_No);
			}else{
				hardReduction.setCheckFlag(Const.Check_Yes);
				hardReduction.setCheckTime(checkTime);
				hardReduction.setCheckUserId(userId);
			}
			hardReductionService.updateHardReduction(hardReduction);
			
		}else {
			if("diaoDuKe".equals(aisle)){
				Map<String, Object> bMaps=new HashMap<String, Object>();
				bMaps.put("commissionOrderId", id);
				List<HardReduction> list=hardReductionService.getHardReductionList(bMaps);
				if (list.size()>0&&id !=null && !"".equals(id)) {
					HardReduction hardReduction= list.get(0);
					Map<String, Object> b1Maps=new HashMap<String, Object>();
					b1Maps.put("hardReductionId", hardReduction.getId());
					List<HardReductionD> list1=hardReductionDService.getHardReductionDList(b1Maps);
					for (int i = 0; i < list1.size(); i++) {
						HardReductionD hardReductionD=list1.get(i);
						hardReductionDService.deleteHardReductionD(hardReductionD.getId());
					}
					hardReductionService.deleteHardReduction(hardReduction.getId());
				}
			}
		}
		
		//遗体处理时间   选取顺序为 1、守灵时间 2、告别时间 3、火化预约时间 
		Timestamp dealTime = null;       
		if(mourningBeginTime!=null  ){
			dealTime = mourningBeginTime;
		}else{
			if( farewellBeginDate !=null ){
				dealTime = farewellBeginDate;
			}
			else{
				if( cremationTime!=null  ){
					dealTime=cremationTime;
				}
			}
		}
		commissionOrder.setDealTime(dealTime);
		//  统计更改后最新项目里面的 应收款项
		//ID为空 为新建火化委托单 收费默认为未收   
		double yingfu=0.0;
		double cha=0.0;
		if(id!= null & id!="" ){
			if(delegrateService.getBoolPayed(id)!=0){
				Map<String, Object> amountMap = new HashMap<String, Object>();
				amountMap.put("commissionOrderId", id);
				amountMap.put("tickFlag", 2);
				amountMap.put("checkFlag", 1);
				List<DelegateRecord>	listdelegate = delegrateService.getdelegateList(amountMap);	
				if(listdelegate.size()>0){
					DelegateRecord delRecord =  listdelegate.get(0);
					yingfu = delRecord.getStotal() - delRecord.getHardTotal()-delRecord.getYbrTotal();
					
				}
				// 当有 支付差产生时 应收金额发生变更  则设pay flag为 2   否则不动
				cha =  yingfu - sumtotal;	
				if(cha !=0.0 && frontPay!=yingfu){
					commissionOrder.setPayFlag(Const.Pay_No);  //数据变更证明需要另外收费 则收费状态改为未收
					commissionOrder.setScheduleFlag(Const.schedule_flag_1);
				} // 当 cha =0 时   payflag 默认
				if(listdelegate.size()<=0){
					commissionOrder.setPayFlag(Const.Pay_No);//加条件，如未进行过收费的委托单肯定为未收费状态
				}
			}
		}else{
			commissionOrder.setPayFlag(Const.Pay_No);
		}
//		commissionOrder.setScheduleFlag(scheduleFlag);
		//尸体解冻时间是否发生变更  通知信息的变更
		if (id!=null&&!id.equals("")) {
			byte payFlag= commissionOrder.getPayFlag();
			Timestamp autoTime =  freezerRecordService.getautoTime(arriveTime, mourningBeginTime, farewellBeginDate , cremationTime);

			if (autoTime != null && oldAutoTime != null) {
				String day1 = DateTools.getTimeFormatString("yyyy-MM-dd", oldAutoTime);
				String day2 = DateTools.getTimeFormatString("yyyy-MM-dd", autoTime);
				if(payFlag == Const.Pay_Yes && !day1.equals(day2) && commissionOrder.getPayFlag()==Const.Pay_Yes ){
					noticeHandleService.updateNoticeRsetAutotTime( autoTime , oldAutoTime);
				}
			}
		}
		
		
		if (id==null||id.equals("")) {
			commissionOrder.setCreatTime(DateTools.getThisDateTimestamp());
			commissionOrder.setId(coId);
			commissionOrder.setCheckFlag(Const.Check_No);
			commissionOrder.setPayFlag(Const.Pay_No);
			String jCode=jenerationService.getJenerationCode(Jeneration.COR_TYPE);
			commissionOrder.setCode(jCode);
			addCommissionOrder(commissionOrder);
		}else {
			noticeHandleService.handleFirstDepart(oldco, ItemlistSocket, -1,null,null);
			if (commissionOrder.getCheckFlag()==Const.Check_Yes) {
				List<ItemOrder> itemList = new ArrayList<ItemOrder>();
				Map<String, Object> itemMap = new HashMap<String, Object>();
				itemMap.put("commissionOrderId", id);
				itemMap.put("type", Const.Type_Articles);
				itemList = itemOrderService.getItemOrderListForNotice(itemMap);
				noticeHandleService.handleFirstDepart(commissionOrder, itemList, 1,oldco,ItemlistSocket);
			}
			updateCommissionOrder(commissionOrder); 
		}
		//保存成功后,,如果有相关的预约登记都清空
		if(appointment !=null ){			
			appointmentService.delAllRecord(appointment);
		}
		
		//新加保存的同时一起审核
		if("yes".equals(isSH) && "diaoDuKe".equals(aisle)){//需要同时将火化委托单审核
			commissionOrder.setCheckFlag(Const.Check_Yes);
			if(commissionOrder.getCheckTime()==null){
				commissionOrder.setCheckTime(checkTime);
			}
			String ids =commissionOrder.getId();
//			if(id!=null&&!"".equals(id)){
//				ids = id;
//			}else{
//				ids = coId;
//			}
	    	byte checkFlag=Const.Check_Yes;
	    	Timestamp nowTime=new Timestamp(System.currentTimeMillis());
	    	Map<String, Object> newmaps = new HashMap<String, Object>();
	    	newmaps.put("commissionOrderId", ids);
			//冷藏柜记录
			List<FreezerRecord> fList=freezerRecordService.getFreezerRecordList(newmaps);
			//告别厅记录
			List<FarewellRecord> faList=farewellRecordService.getFarewellRecordList(newmaps,"");
			//灵堂记录
			List<MourningRecord> mList=mourningRecordService.getMourningRecordList(newmaps,"");
			//火化炉调度
			newmaps.put("changeFlag", Const.ChangeFlag_No);
			List<FurnaceRecord> fuList=furnaceRecordService.getFurnaceRecordList(newmaps, "");
			if (checkFlag==Const.Check_No) {
				newmaps.put("checkFlag", Const.Check_No);
			}else if(checkFlag==Const.Check_Yes){
				newmaps.put("checkFlag", Const.Check_Yes);
			}
			maps.put("checkTime", nowTime);
			
			
			//以下判断 挂账火化委托单里的非挂账业务是否已付款
			Map<String,Object> paramMap=new HashMap<String,Object>();
			paramMap.put("tickFlag", Const.Is_Yes);
			paramMap.put("commissionOrderId", ids);
			List<ItemOrder> listItem=itemOrderService.getItemOrderList(paramMap);
			boolean flag=false;//非挂账都为false
			if(listItem!=null && listItem.size()>0){
				flag=true;//挂账业务默认为true
				paramMap.put("tickFlag", Const.Is_No);
				List<ItemOrder> NoTicklist=itemOrderService.getItemOrderList(paramMap);
				for(int j=0;j<NoTicklist.size();j++){
					byte payFlag=NoTicklist.get(j).getPayFlag();
					if(payFlag==2){//若在挂账业务中有非挂账的,并且未付款
						flag=false;//则此挂账业务也为false
					}
				}
			}
//				CommissionOrder commissionOrder = new CommissionOrder();
//	    		commissionOrder=commissionOrderService.getCommissionOrderById(ids[i]);
    		List<ItemOrder> list = new ArrayList<ItemOrder>();
    		Map<String,Object> map=new HashMap<String,Object>();
    		map.put("commissionOrderId", id);
    		map.put("type", Const.Type_Articles);
    		list = itemOrderService.getItemOrderListForNotice(map);
	    	checkMethod(newmaps, checkFlag,fList,faList,mList,fuList,flag,commissionOrder,list);
		    	
		    }
		return coId;
	}
	public int getProveUnitIdCount(Map<String, Object> map) {
		return commissionOrderMapper.getProveUnitIdCount(map);
	}
	/**
	 * 根据ID获取卡号
	 */
	public String getCardCodeById(String id) {
		return commissionOrderMapper.getCardCodeById(id);
	}
	/**
	 * 审核/取消审核
	 * @param list 
	 * @param commissionOrder 
	 */
	public void checkMethod(Map<String, Object> maps, byte checkFlag, List<FreezerRecord> fList, List<FarewellRecord> faList,
			List<MourningRecord> mList, List<FurnaceRecord> fuList,boolean flag, CommissionOrder commissionOrder, List<ItemOrder> list) {
		//flag 为全部挂账与否的标志
		if(flag){//挂账业务若无特殊情况   此处为已付款状态
			maps.put("payFlag",Const.Pay_Yes);
			commissionOrderMapper.updateCommissionOrderPayFlag(maps);
			if(checkFlag==Const.Check_Yes){
				/** 审核时 在更新操作之后对  notice 变更 */
				commissionOrderMapper.checkOrNoCommissionOrder(maps);
				CommissionOrder order = getCommissionOrderById(commissionOrder.getId());
				noticeHandleService.delegrateNotice(order);
			}
			else{
				//取消审核时在更新操作之前 进行  nottice 的变更然后进行更新操作
				noticeHandleService.cancleDelegateNotice(commissionOrder.getId());
				commissionOrderMapper.checkOrNoCommissionOrder(maps);
			}
		}
		else{
			//对 火化委托单 审核操作
			commissionOrderMapper.checkOrNoCommissionOrder(maps);
		}
	
		CommissionOrder neworder = getCommissionOrderById(commissionOrder.getId());
		if (checkFlag==Const.Check_Yes) {
	    	/*if (fList.size()>0) {
	    		//  freezerRecord   进行审核时  不会对 冰柜的状态造成影响
	    		FreezerRecord freezerRecord=fList.get(0);
	    		// freezerRecord.setFlag(Const.IsFlag_Yse);
	    		freezerRecordService.updateFreezerRecord(freezerRecord);
	    		
	    		Freezer freezer =freezerService.getFreezerById(fList.get(0).getFreezerId());
	    	//	freezer.setFlag(Const.IsFlag_Yse);
	    		freezerService.updateFreezer(freezer);
			}*/
	    	if (faList.size()>0) {
	    		FarewellRecord farewellRecord=faList.get(0);
	    		farewellRecord.setFlag(Const.IsFlag_Yse);
	    		farewellRecordService.updateFarewellRecord(farewellRecord);
			}
	    	if (mList.size()>0) {
	    		MourningRecord mourningRecord=mList.get(0);
	    		mourningRecord.setFlag(Const.IsFlag_Yse);
	    		mourningRecordService.updateMourningRecord(mourningRecord);
			}
	    	if (fuList.size()>0) {
	    		FurnaceRecord furnaceRecord=fuList.get(0);
	    		furnaceRecord.setFlag(Const.IsFlag_Yse);
	    		furnaceRecordService.updateFurnaceRecord(furnaceRecord);
			}
	    	noticeHandleService.handleFirstDepart(neworder, list, 1,null,null);
		}else if (checkFlag==Const.Check_No) {
	    	if (fList.size()>0) {
	    		FreezerRecord freezerRecord=fList.get(0);
	    		// 如果 在  二科登记 已入柜的 话  取消 审核时状态不变      
	    		if(freezerRecord.getFlag() == Const.IsFlag_Yse){
	    			freezerRecord.setFlag(Const.IsFlag_Yse);
	    		}
	    		else{
	    				freezerRecord.setFlag(Const.IsFlag_Lock);
	    		}
	    		freezerRecordService.updateFreezerRecord(freezerRecord);
	    		Freezer freezer =freezerService.getFreezerById(fList.get(0).getFreezerId());
	    		freezer.setFlag(Const.IsFlag_Lock);
	    		freezerService.updateFreezer(freezer);
			}
	    	if (faList.size()>0) {
	    		FarewellRecord farewellRecord=faList.get(0);
	    		farewellRecord.setFlag(Const.IsFlag_Lock);
	    		farewellRecordService.updateFarewellRecord(farewellRecord);
			}
	    	if (mList.size()>0) {
	    		MourningRecord mourningRecord=mList.get(0);
	    		mourningRecord.setFlag(Const.IsFlag_Lock);
	    		mourningRecordService.updateMourningRecord(mourningRecord);
			}
	    	if (fuList.size()>0) {
	    		FurnaceRecord furnaceRecord=fuList.get(0);
	    		furnaceRecord.setFlag(Const.IsFlag_Lock);
	    		furnaceRecordService.updateFurnaceRecord(furnaceRecord);
			}
	    	noticeHandleService.handleFirstDepart(neworder, list, -1,null,null);
	    	Timestamp  oldAutoTime =  freezerRecordService.getAutoTimeByOrderId(commissionOrder.getId());
	    	if(oldAutoTime != null){
	    		noticeHandleService.updateNoticeToday(oldAutoTime,Const.NoticeType_FysytjdTask);
	    	}
		}
	}
	/**
 	 * 保存图片
 	 * @param path
 	 * @param file
 	 * @return
 	 */
// 	public String getSavePath(String path,FormFile file){
// 		Calendar now = Calendar.getInstance();  
//		String n=now.get(Calendar.YEAR)+"";  
//		String y=now.get(Calendar.MONTH) + 1+"";  
//		String r=now.get(Calendar.DAY_OF_MONTH)+""; 
// 		String fileName=null;
// 		String savePath="";
// 		if(file.getFileName()!=null&&!file.getFileName().equals("")){
//			File filePath = new File(path+"/userDataImage/");
//			if(!filePath.exists()){//判断在不在，不在就创建
//				filePath.mkdirs();
//			}
//			//文件保存
//			if(DataTools.getExt(file.getFileName())!=null&&DataTools.getExt(file.getFileName())!=""){
//				if(DataTools.getExt(file.getFileName()).equals("jpg")){
//					fileName =System.currentTimeMillis()+"."+DataTools.getExt(file.getFileName());
//					DataTools.copyFile(path+"/Image/"+n+"/"+y+"/"+r+"/"+fileName, file);
//				}else {
//					throw new Error("格式不正确，请添加jpg格式的图片");
//				}
//			}
//			savePath = "Image/"+n+"/"+y+"/"+r+"/"+fileName;
//		}
//		return savePath;
// 	}
	public String getDealIselOptionTwo(String id, boolean isAll) {
		List<Object[]> list=new ArrayList<Object[]>();
	    for (int i = 1; i < 3; i++) {
			list.add(new Object[]{i,Const.getDealIsdelType((byte) i)});
		}
	    String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 综合业务   打印查询功能
	 * @param id
	 * @return
	 */
	public CommissionOrder getCommissionCheckPrintById(String id){
		return commissionOrderMapper.getCommissionCheckPrintById(id);
	}
	
	
	
	
	
	
}
