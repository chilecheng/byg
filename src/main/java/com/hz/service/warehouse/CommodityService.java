package com.hz.service.warehouse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.warehouse.Commodity;
import com.hz.mapper.warehouse.CommodityMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 商品信息
 * @author 金国军
 */
@Service
@Transactional
public class CommodityService {
	@Autowired
	private CommodityMapper commodityMapper;
	/**
	 * 保存商品信息
	 * @param supplier
	 */
	public void saveCommodity(Commodity commodity){
		commodityMapper.saveCommodity(commodity);
	}
	/**
	 * 删除商品信息
	 * @param id
	 * @param String id
	 */
	public void delCommodity(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			commodityMapper.delCommodity(ids[i]);
		}
		
	}
	/**
	 * 修改商品信息
	 * @param supplier
	 */
	public void updateCommodity(Commodity commodity){
		commodityMapper.updateCommodity(commodity);
	}
	/**
	 * 查找商品信息id
	 * @param id
	 * @param String id
	 */
	public Commodity getCommodityById(String id){
		return commodityMapper.getCommodityById(id);
	}
	/**
	 * 查找所有的商品信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Commodity> getCommodityList(Map<String, Object> paramMap){
		List<Commodity> list = commodityMapper.getCommodityList(paramMap);
		return list;
	}
	/**
	 * 商品信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Commodity> getCommodityPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Commodity> list=getCommodityList(paramMap);
		PageInfo<Commodity> page=new PageInfo<Commodity>(list);
		return page;
		
	}
	/**
	 * 获取商品信息
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getCommodityOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("isDel", Const.Is_Yes);
		List<Commodity> listCommodity=commodityMapper.getCommodityList(paramMap);
		for (int i = 0; i < listCommodity.size(); i++) {
			Commodity commodity=(Commodity) listCommodity.get(i);
			list.add(new Object[]{commodity.getId(),commodity.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改商品是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Commodity commodity=getCommodityById(ids[i]);
			if(isdel==Const.Isdel_No){
				commodity.setIsDel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				commodity.setIsDel(Const.Isdel_Yes);
			}
			updateCommodity(commodity);
		}
	}
	/**
	 * 更新保存商品
	 * @param id
	 * @param nation
	 */
	public void addOrUpdate(String id, Commodity commodity) {
		if(id!=null && !"".equals(id)){
			updateCommodity(commodity);
		}else{
			commodity.setId(UuidUtil.get32UUID());
			saveCommodity(commodity);
		}
	}
	
}
