package com.hz.service.warehouse;

import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.warehouse.Storage;
import com.hz.mapper.warehouse.StorageMapper;

/**
 * 入库记录信息
 * @author 金国军
 */
@Service
@Transactional
public class StorageService {
	@Autowired
	private StorageMapper storageMapper;
	/**
	 * 保存入库记录信息
	 * @param supplier
	 */
	public void saveStorage(Storage storage){
		storageMapper.saveStorage(storage);
	}
	/**
	 * 查找入库记录信息id
	 * @param id
	 * @param String id
	 */
	public Storage getStorageById(String id){
		return storageMapper.getStorageById(id);
	}
	/**
	 * 查找所有的入库记录信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Storage> getStorageList(Map<String, Object> paramMap){
		List<Storage> list = storageMapper.getStorageList(paramMap);
		return list;
	}
	/**
	 * 入库记录信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Storage> getStoragePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Storage> list=getStorageList(paramMap);
		PageInfo<Storage> page=new PageInfo<Storage>(list);
		return page;
		
	}
//	/**
//	 * 更新保存入库记录
//	 * @param id
//	 * @param nation
//	 */
//	public void addOrUpdate(String id, Storage storage) {
//		if(id!=null && !"".equals(id)){
//			updateStorage(storage);
//		}else{
//			storage.setId(UuidUtil.get32UUID());
//			saveStorage(storage);
//		}
//	}
	/**
	 * 删除入库记录信息
	 * @param id
	 * @param String id
	 */
//	public void delStorage(String id){
//		String ids[]=id.split(",");
//		for (int i = 0; i < ids.length; i++) {
//			storageMapper.delStorage(ids[i]);
//		}
//		
//	}
	/**
	 * 修改入库记录信息
	 * @param supplier
	 */
//	public void updateStorage(Storage storage){
//		storageMapper.updateStorage(storage);
//	}
	
}
