package com.hz.service.warehouse;

import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.warehouse.OutWare;
import com.hz.mapper.warehouse.OutWareMapper;

/**
 * 出库记录信息
 * @author 金国军
 */
@Service
@Transactional
public class OutWareService {
	@Autowired
	private OutWareMapper outWareMapper;
	/**
	 * 保存出库记录信息
	 * @param supplier
	 */
	public void saveOutWare(OutWare outWare){
		outWareMapper.saveOutWare(outWare);
	}
	/**
	 * 查找出库记录信息id
	 * @param id
	 * @param String id
	 */
	public OutWare getOutWareById(String id){
		return outWareMapper.getOutWareById(id);
	}
	/**
	 * 查找所有的出库记录信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<OutWare> getOutWareList(Map<String, Object> paramMap){
		List<OutWare> list = outWareMapper.getOutWareList(paramMap);
		return list;
	}
	/**
	 * 出库记录信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<OutWare> getOutWarePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<OutWare> list=getOutWareList(paramMap);
		PageInfo<OutWare> page=new PageInfo<OutWare>(list);
		return page;
		
	}
//	/**
//	 * 更新保存出库记录
//	 * @param id
//	 * @param nation
//	 */
//	public void addOrUpdate(String id, OutWare outWare) {
//		if(id!=null && !"".equals(id)){
//			updateOutWare(outWare);
//		}else{
//			outWare.setId(UuidUtil.get32UUID());
//			saveOutWare(outWare);
//		}
//	}
	/**
	 * 删除出库记录信息
	 * @param id
	 * @param String id
	 */
//	public void delOutWare(String id){
//		String ids[]=id.split(",");
//		for (int i = 0; i < ids.length; i++) {
//			outWareMapper.delOutWare(ids[i]);
//		}
//		
//	}
	/**
	 * 修改出库记录信息
	 * @param supplier
	 */
//	public void updateOutWare(OutWare outWare){
//		outWareMapper.updateOutWare(outWare);
//	}
	
}
