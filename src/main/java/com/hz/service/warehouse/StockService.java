package com.hz.service.warehouse;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.warehouse.Stock;
import com.hz.mapper.warehouse.StockMapper;
import com.hz.util.UuidUtil;

/**
 * 库存记录信息
 * @author 金国军
 */
@Service
@Transactional
public class StockService {
	@Autowired
	private StockMapper stockMapper;
	/**
	 * 保存库存记录信息
	 * @param stock
	 */
	public void saveStock(Stock stock){
		stockMapper.saveStock(stock);
	}
	/**
	 * 查找库存记录信息id
	 * @param id
	 * @param String id
	 */
	public Stock getStockById(String id){
		return stockMapper.getStockById(id);
	}
	/**
	 * 查找所有的库存记录信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Stock> getStockList(Map<String, Object> paramMap){
		List<Stock> list = stockMapper.getStockList(paramMap);
		return list;
	}
	/**
	 * 删除库存记录信息
	 * @param id
	 * @param String id
	 */
	public void delStock(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			stockMapper.delStock(ids[i]);
		}
		
	}
	/**
	 * 修改库存记录信息
	 * @param stock
	 */
	public void updateStock(Stock stock){
		stockMapper.updateStock(stock);
	}
	/**
	 * 库存记录信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Stock> getStockPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Stock> list=getStockList(paramMap);
		PageInfo<Stock> page=new PageInfo<Stock>(list);
		return page;
		
	}
	/**
	 * 更新保存库存记录
	 * @param id
	 * @param stock
	 */
	public void addOrUpdate(String id, Stock stock) {
		if(id!=null && !"".equals(id)){
			updateStock(stock);
		}else{
			stock.setId(UuidUtil.get32UUID());
			saveStock(stock);
		}
	}
	/**
	 * 变动入库与出库时，同时保存库存信息
	 * @param commodityId
	 * @param warehouseId
	 * @param supplierId
	 * @param now
	 * @param number
	 * @param type
	 */
	public void linkedSave(String commodityId,String warehouseId,String supplierId,Timestamp now,int number,String type){
		Map<String,Object> maps=new HashMap<String,Object>();
		maps.put("commodityId", commodityId);
		maps.put("warehouseId",warehouseId);
		List<Stock> stockList=getStockList(maps);
		Stock stock=new Stock();
		if(stockList.size()==1){
			stock=stockList.get(0);
			stock.setRecordTime(now);
			if(type!=null && "add".equals(type)){
				stock.setAmount(stock.getAmount()+number);
			}else if(type!=null && "reduce".equals(type)){
				stock.setAmount(stock.getAmount()-number);
			}
		}else if(stockList.size()>1){//若同时存在两条及以上记录，是否需要将其他的删除？？！！
			stock=stockList.get(0);
			stock.setRecordTime(now);
			if(type!=null && "add".equals(type)){
				stock.setAmount(stock.getAmount()+number);
			}else if(type!=null && "reduce".equals(type)){
				stock.setAmount(stock.getAmount()-number);
			}
		}else if(stockList.size()<=0){
			if(type!=null && "add".equals(type)){
				stock.setAmount(stock.getAmount()+number);
			}else if(type!=null && "reduce".equals(type)){
				stock.setAmount(stock.getAmount()-number);
			}
			stock.setCommodityId(commodityId);
			stock.setSupplierId(supplierId);
			stock.setRecordTime(now);
			stock.setWarehouseId(warehouseId);
		}
		addOrUpdate(stock.getId(), stock);
	}
}
