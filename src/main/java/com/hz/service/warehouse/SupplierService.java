package com.hz.service.warehouse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.warehouse.Supplier;
import com.hz.mapper.warehouse.SupplierMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 供应商信息
 * @author 金国军
 */
@Service
@Transactional
public class SupplierService {
	@Autowired
	private SupplierMapper supplierMapper;
	/**
	 * 保存供应商信息
	 * @param supplier
	 */
	public void saveSupplier(Supplier supplier){
		supplierMapper.saveSupplier(supplier);
	}
	/**
	 * 删除供应商信息
	 * @param id
	 * @param String id
	 */
	public void delSupplier(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			supplierMapper.delSupplier(ids[i]);
		}
		
	}
	/**
	 * 修改供应商信息
	 * @param supplier
	 */
	public void updateSupplier(Supplier supplier){
		supplierMapper.updateSupplier(supplier);
	}
	/**
	 * 查找供应商信息id
	 * @param id
	 * @param String id
	 */
	public Supplier getSupplierById(String id){
		return supplierMapper.getSupplierById(id);
	}
	/**
	 * 查找所有的供应商信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Supplier> getSupplierList(Map<String, Object> paramMap){
		List<Supplier> list = supplierMapper.getSupplierList(paramMap);
		return list;
	}
	/**
	 * 供应商信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Supplier> getSupplierPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Supplier> list=getSupplierList(paramMap);
		PageInfo<Supplier> page=new PageInfo<Supplier>(list);
		return page;
		
	}
	/**
	 * 获取供应商信息
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getSupplierOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("isDel", Const.Is_Yes);
		List<Supplier> listSupplier=supplierMapper.getSupplierList(paramMap);
		for (int i = 0; i < listSupplier.size(); i++) {
			Supplier supplier=(Supplier) listSupplier.get(i);
			list.add(new Object[]{supplier.getId(),supplier.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改供应商是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Supplier supplier=getSupplierById(ids[i]);
			if(isdel==Const.Isdel_No){
				supplier.setIsDel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				supplier.setIsDel(Const.Isdel_Yes);
			}
			updateSupplier(supplier);
		}
	}
	/**
	 * 更新保存供应商
	 * @param id
	 * @param nation
	 */
	public void addOrUpdate(String id, Supplier supplier) {
		if(id!=null && !"".equals(id)){
			updateSupplier(supplier);
		}else{
			supplier.setId(UuidUtil.get32UUID());
			saveSupplier(supplier);
		}
	}
	
}
