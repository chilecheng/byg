package com.hz.service.warehouse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.warehouse.Warehouse;
import com.hz.mapper.warehouse.WarehouseMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 仓库信息
 * @author 金国军
 */
@Service
@Transactional
public class WarehouseService {
	@Autowired
	private WarehouseMapper warehouseMapper;
	/**
	 * 保存仓库信息
	 * @param supplier
	 */
	public void saveWarehouse(Warehouse warehouse){
		warehouseMapper.saveWarehouse(warehouse);
	}
	/**
	 * 删除仓库信息
	 * @param id
	 * @param String id
	 */
	public void delWarehouse(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			warehouseMapper.delWarehouse(ids[i]);
		}
		
	}
	/**
	 * 修改仓库信息
	 * @param supplier
	 */
	public void updateWarehouse(Warehouse warehouse){
		warehouseMapper.updateWarehouse(warehouse);
	}
	/**
	 * 查找仓库信息id
	 * @param id
	 * @param String id
	 */
	public Warehouse getWarehouseById(String id){
		return warehouseMapper.getWarehouseById(id);
	}
	/**
	 * 查找所有的仓库信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Warehouse> getWarehouseList(Map<String, Object> paramMap){
		List<Warehouse> list = warehouseMapper.getWarehouseList(paramMap);
		return list;
	}
	/**
	 * 仓库信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Warehouse> getWarehousePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Warehouse> list=getWarehouseList(paramMap);
		PageInfo<Warehouse> page=new PageInfo<Warehouse>(list);
		return page;
		
	}
	/**
	 * 获取仓库信息
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getWarehouseOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("isDel", Const.Is_Yes);
		List<Warehouse> listWarehouse=warehouseMapper.getWarehouseList(paramMap);
		for (int i = 0; i < listWarehouse.size(); i++) {
			Warehouse warehouse=(Warehouse) listWarehouse.get(i);
			list.add(new Object[]{warehouse.getId(),warehouse.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改仓库是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Warehouse warehouse=getWarehouseById(ids[i]);
			if(isdel==Const.Isdel_No){
				warehouse.setIsDel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				warehouse.setIsDel(Const.Isdel_Yes);
			}
			updateWarehouse(warehouse);
		}
	}
	/**
	 * 更新保存仓库
	 * @param id
	 * @param nation
	 */
	public void addOrUpdate(String id, Warehouse warehouse) {
		if(id!=null && !"".equals(id)){
			updateWarehouse(warehouse);
		}else{
			warehouse.setId(UuidUtil.get32UUID());
			saveWarehouse(warehouse);
		}
	}
	
}
