package com.hz.service.warehouse;

import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.warehouse.Bill;
import com.hz.mapper.warehouse.BillMapper;
import com.hz.util.UuidUtil;

/**
 * 结算记录信息
 * @author 金国军
 */
@Service
@Transactional
public class BillService {
	@Autowired
	private BillMapper billMapper;
	/**
	 * 保存结算记录信息
	 * @param supplier
	 */
	public void saveBill(Bill bill){
		billMapper.saveBill(bill);
	}
	/**
	 * 查找结算记录信息id
	 * @param id
	 * @param String id
	 */
	public Bill getBillById(String id){
		return billMapper.getBillById(id);
	}
	/**
	 * 查找所有的结算记录信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Bill> getBillList(Map<String, Object> paramMap){
		List<Bill> list = billMapper.getBillList(paramMap);
		return list;
	}
	/**
	 * 结算记录信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Bill> getBillPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Bill> list=getBillList(paramMap);
		PageInfo<Bill> page=new PageInfo<Bill>(list);
		return page;
		
	}
	/**
	 * 更新保存结算记录
	 * @param id
	 * @param nation
	 */
	public void addOrUpdate(String id, Bill bill) {
		if(id!=null && !"".equals(id)){
			updateBill(bill);
		}else{
			bill.setId(UuidUtil.get32UUID());
			saveBill(bill);
		}
	}
	/**
	 * 删除结算记录信息
	 * @param id
	 * @param String id
	 */
	public void delBill(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			billMapper.delBill(ids[i]);
		}
		
	}
	/**
	 * 修改结算记录信息
	 * @param supplier
	 */
	public void updateBill(Bill bill){
		billMapper.updateBill(bill);
	}
	
}
