package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.HardReductionLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.log.HardReductionMapperLog;
import com.hz.mapper.ye.AshesRecordDMapper;
import com.hz.mapper.ye.AshesRecordMapper;
import com.hz.mapper.ye.BaseReductionMapper;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.mapper.ye.FreezerRecordMapper;
import com.hz.mapper.ye.HardReductionMapper;
import com.hz.mapper.ye.MourningRecordMapper;
/**
 * ���Ѽ�������
 * @author rgy
 *
 */
@Service
@Transactional
public class HardReductionServiceLog {
	@Autowired
	private HardReductionMapperLog hardReductionMapper;
	/**
	 * �������Ѽ�������
	 * @param hardReduction
	 */
	public void addHardReduction(HardReductionLog hardReduction){
		hardReductionMapper.saveHardReduction(hardReduction);
	}
	/**
	 * ɾ�����Ѽ�������
	 * @param id
	 */
	public void deleteHardReduction(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			hardReductionMapper.deleteHardReduction(id);
		}
	}
	/**
	 * �޸����Ѽ�������
	 * @param baseReduction
	 */
	public void updateHardReduction(HardReductionLog hardReduction){
		hardReductionMapper.updateHardReduction(hardReduction);
	}
	/**
	 * ��ȡ���Ѽ�������
	 * @param id
	 * @return
	 */
	public HardReductionLog getHardReductionId(String id){
		return hardReductionMapper.getHardReductionById(id);
	}
	/**
	 * ����������ȡ���Ѽ�������list
	 * @param paramMap
	 * @return
	 */
	public List getHardReductionList(Map paramMap){
		return hardReductionMapper.getHardReductionList(paramMap);
	}
	/**
	 * ��ȡ���Ѽ��������б�
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo getHardReductionPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//���÷�ҳ
		PageHelper.orderBy(order);//����
		List list=getHardReductionList(paramMap);
		
		PageInfo page=new PageInfo();
		return page;
		
	}
}
