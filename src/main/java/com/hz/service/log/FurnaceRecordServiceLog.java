package com.hz.service.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.log.FurnaceRecordLog;
import com.hz.mapper.log.FurnaceRecordMapperLog;

/**火化炉调度
 * @author tsp
 *
 */
@Service
@Transactional
public class FurnaceRecordServiceLog {
	@Autowired
	private FurnaceRecordMapperLog furnaceRecordMapper;
	/**
	 * 添加火化炉信息
	 * @param furnace
	 */
	public void addFurnaceRecord(FurnaceRecordLog furnace){
		furnaceRecordMapper.saveFurnaceRecord(furnace);
	}
	/**
	 * 删除火化炉信息
	 * @param id
	 */
	public void deleteFurnaceRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	furnaceRecordMapper.deleteFurnaceRecord(ids[i]);
	    }
	}
	/**
	 * 更新火化炉信息
	 * @param furnace
	 */
	public void updateFurnaceRecord(FurnaceRecordLog furnace){
		furnaceRecordMapper.updateFurnaceRecord(furnace);
	}
	/**
	 * 获取火化炉信息对象
	 * @param id
	 * @return
	 */
	public FurnaceRecordLog getFurnaceById(String id){
		FurnaceRecordLog furnace=furnaceRecordMapper.getFurnaceRecordById(id);
		return furnace;
	}
}
