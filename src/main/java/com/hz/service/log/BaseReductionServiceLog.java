package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.BaseReductionLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.log.BaseReductionMapperLog;
import com.hz.mapper.ye.AshesRecordDMapper;
import com.hz.mapper.ye.AshesRecordMapper;
import com.hz.mapper.ye.BaseReductionMapper;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.mapper.ye.FreezerRecordMapper;
import com.hz.mapper.ye.MourningRecordMapper;
/**
 * 基本减免申请
 * @author rgy
 *
 */
@Service
@Transactional
public class BaseReductionServiceLog {
	@Autowired
	private BaseReductionMapperLog baseReductionMapper;
	/**
	 * 添加基本减免申请
	 * @param baseReduction
	 */
	public void addBaseReduction(BaseReductionLog baseReduction){
		baseReductionMapper.saveBaseReduction(baseReduction);
	}
	/**
	 * 删除基本减免申请
	 * @param id
	 */
	public void deleteBaseReduction(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			baseReductionMapper.deleteBaseReduction(id);
		}
	}
	/**
	 * 修改基本减免申请
	 * @param baseReduction
	 */
	public void updateBaseReduction(BaseReductionLog baseReduction){
		baseReductionMapper.updateBaseReduction(baseReduction);
	}
	/**
	 * 获取基本减免申请
	 * @param id
	 * @return
	 */
	public BaseReductionLog getBaseReductionId(String id){
		return baseReductionMapper.getBaseReductionById(id);
	}
	/**
	 * 根据条件获取基本减免申请list
	 * @param paramMap
	 * @return
	 */
	public List getBaseReductionList(Map paramMap){
		return baseReductionMapper.getBaseReductionList(paramMap);
	}
	/**
	 * 获取基本减免申请列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo getBaseReductionPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List list=getBaseReductionList(paramMap);
		
		PageInfo page=new PageInfo();
		return page;
		
	}
}
