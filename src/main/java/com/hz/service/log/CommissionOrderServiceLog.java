package com.hz.service.log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.CarType;
import com.hz.entity.log.CommissionOrderLog;
import com.hz.entity.ye.CommissionOrder;
import com.hz.mapper.log.CommissionOrderMapperLog;
import com.hz.mapper.ye.CommissionOrderMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;

@Service
@Transactional
public class CommissionOrderServiceLog {
	@Autowired
	private CommissionOrderMapperLog commissionOrderMapper;
	
	/**
	 * 添加火化委托单
	 * @param commissionOrder
	 */
	public void addCommissionOrder(CommissionOrderLog commissionOrder){
		commissionOrderMapper.saveCommissionOrder(commissionOrder);
	}
	/**
	 * 删除火化委托单
	 * @param id
	 */
	public void deleteCommissionOrder(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	commissionOrderMapper.deleteCommissionOrder(ids[i]);
	    }
	}
	/**
	 * 更新火化委托单
	 * @param commissionOrder
	 */
	public void updateCommissionOrder(CommissionOrderLog commissionOrder){
		commissionOrderMapper.updateCommissionOrder(commissionOrder);
	}
	/**
	 * 获取火化委托单对象
	 * @param id
	 * @return
	 */
	public CommissionOrderLog getCommissionOrderById(String id){
		CommissionOrderLog commissionOrder=commissionOrderMapper.getCommissionOrderById(id);
		return commissionOrder;
	}
	
	/**
	 * 根据条件获取火化委托单List
	 * @param String name
	 */
	public List getCommissionOrderList(Map paramMap){
		List list= commissionOrderMapper.getCommissionOrderList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取火化委托单PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo getCommissionOrderPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List list=getCommissionOrderList(paramMap);
		PageInfo page = new PageInfo(list);
		return page; 
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param checkFlag
	 */
	public void updateCheckFlag(String id,byte checkFlag){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	CommissionOrderLog commissionOrder=getCommissionOrderById(ids[i]);
	    	if (checkFlag==Const.Check_No) {
	    		commissionOrder.setCheckFlag(Const.Check_No);
			}else if(checkFlag==Const.Check_Yes){
				commissionOrder.setCheckFlag(Const.Check_Yes);
			}
	    	updateCommissionOrder(commissionOrder);
	    }
	}
}
