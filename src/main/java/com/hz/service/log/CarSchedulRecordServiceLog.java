package com.hz.service.log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.CarSchedulRecordLog;
import com.hz.entity.ye.CarSchedulRecord;
import com.hz.mapper.log.CarSchedulRecordMapperLog;
import com.hz.mapper.ye.CarSchedulRecordMapper;

@Service
@Transactional
public class CarSchedulRecordServiceLog {
	@Autowired
	private CarSchedulRecordMapperLog carSchedulRecordMapper;
	
	/**
	 * 添加车辆调度记录
	 * @param carSchedulRecord
	 */
	public void addCarSchedulRecord(CarSchedulRecordLog carSchedulRecord){
		carSchedulRecordMapper.saveCarSchedulRecord(carSchedulRecord);
	}
	/**
	 * 删除车辆调度记录
	 * @param id
	 */
	public void deleteCarSchedulRecord(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	carSchedulRecordMapper.deleteCarSchedulRecord(ids[i]);
	    }
	}
	/**
	 * 更新车辆调度记录
	 * @param carSchedulRecord
	 */
	public void updateCarSchedulRecord(CarSchedulRecordLog carSchedulRecord){
		carSchedulRecordMapper.updateCarSchedulRecord(carSchedulRecord);
	}
	/**
	 * 获取车辆调度记录对象
	 * @param id
	 * @return
	 */
	public CarSchedulRecordLog getCarSchedulRecordById(String id){
		CarSchedulRecordLog carSchedulRecord=carSchedulRecordMapper.getCarSchedulRecordById(id);
		return carSchedulRecord;
	}
	
	/**
	 * 根据条件获取车辆调度记录List
	 * @param String name
	 */
	public List getCarSchedulRecordList(Map paramMap,String order){
		PageHelper.orderBy(order);
		List list= carSchedulRecordMapper.getCarSchedulRecordList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取车辆调度记录PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo getCarSchedulRecordPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List list=getCarSchedulRecordList(paramMap,order);
		PageInfo page = new PageInfo(list);
		return page; 
	}
	
}
