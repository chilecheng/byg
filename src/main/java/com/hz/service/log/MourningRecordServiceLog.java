package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.MourningRecordLog;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.log.MourningRecordMapperLog;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.mapper.ye.FreezerRecordMapper;
import com.hz.mapper.ye.MourningRecordMapper;
/**
 * 灵堂记录信息
 * @author rgy
 *
 */
@Service
@Transactional
public class MourningRecordServiceLog {
	@Autowired
	private MourningRecordMapperLog mourningRecordMapper;
	/**
	 * 添加灵堂记录
	 * @param mourningRecord
	 */
	public void addMourningRecord(MourningRecordLog mourningRecord){
		mourningRecordMapper.saveMourningRecord(mourningRecord);
	}
	/**
	 * 删除灵堂记录
	 * @param id
	 */
	public void deleteMourningRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			mourningRecordMapper.deleteMourningRecord(id);
		}
	}
	/**
	 * 修改灵堂记录
	 * @param mourningRecord
	 */
	public void updateMourningRecord(MourningRecordLog mourningRecord){
		mourningRecordMapper.updateMourningRecord(mourningRecord);
	}
	/**
	 * 获取灵堂记录对象
	 * @param id
	 * @return
	 */
	public MourningRecordLog getMourningRecordId(String id){
		return mourningRecordMapper.getMourningRecordById(id);
	}
	/**
	 * 根据条件获取灵堂记录list
	 * @param paramMap
	 * @return
	 */
	public List getMourningRecordList(Map paramMap,String order){
		PageHelper.orderBy(order);
		return mourningRecordMapper.getMourningRecordList(paramMap);
	}
	/**
	 * 获取灵堂记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo getMourningRecordPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		List list=getMourningRecordList(paramMap,order);
		PageInfo page=new PageInfo();
		return page;
		
	}
}
