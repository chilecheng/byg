package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.BaseReductionDLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.log.BaseReductionDMapperLog;
import com.hz.mapper.ye.AshesRecordDMapper;
import com.hz.mapper.ye.AshesRecordMapper;
import com.hz.mapper.ye.BaseReductionDMapper;
import com.hz.mapper.ye.BaseReductionMapper;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.mapper.ye.FreezerRecordMapper;
import com.hz.mapper.ye.MourningRecordMapper;
/**
 * 基本减免申请项目
 * @author rgy
 *
 */
@Service
@Transactional
public class BaseReductionDServiceLog {
	@Autowired
	private BaseReductionDMapperLog baseReductionDMapper;
	/**
	 * 添加基本减免申请项目
	 * @param baseReductionD
	 */
	public void addBaseReductionD(BaseReductionDLog baseReductionD){
		baseReductionDMapper.saveBaseReductionD(baseReductionD);
	}
	/**
	 * 删除基本减免申请项目
	 * @param id
	 */
	public void deleteBaseReductionD(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			baseReductionDMapper.deleteBaseReductionD(id);
		}
	}
	/**
	 * 修改基本减免申请项目
	 * @param baseReduction
	 */
	public void updateBaseReductionD(BaseReductionDLog baseReductionD){
		baseReductionDMapper.updateBaseReductionD(baseReductionD);
	}
	/**
	 * 获取基本减免申请项目
	 * @param id
	 * @return
	 */
	public BaseReductionDLog getBaseReductionDId(String id){
		return baseReductionDMapper.getBaseReductionDById(id);
	}
	/**
	 * 根据条件获取基本减免申请项目list
	 * @param paramMap
	 * @return
	 */
	public List getBaseReductionDList(Map paramMap){
		return baseReductionDMapper.getBaseReductionDList(paramMap);
	}
	/**
	 * 获取基本减免申请项目列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo getBaseReductionDPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List list=getBaseReductionDList(paramMap);
		
		PageInfo page=new PageInfo();
		return page;
		
	}
}
