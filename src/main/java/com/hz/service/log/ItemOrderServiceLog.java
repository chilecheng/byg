package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.ItemOrderLog;
import com.hz.mapper.log.ItemOrderMapperLog;
/**
 * 火化委托单服务项目
 * @author tsp
 *
 */
@Service
@Transactional
public class ItemOrderServiceLog {
	@Autowired
	private ItemOrderMapperLog itemOrderMapper;
	
	/**
	 * 添加火化委托单收费项目
	 * @param itemOrder
	 */
	public void addItemOrder(ItemOrderLog itemOrder){
		itemOrderMapper.saveItemOrder(itemOrder);
	}
	/**
	 * 删除火化委托单收费项目
	 * @param id
	 */
	public void deleteItemOrder(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	itemOrderMapper.deleteItemOrder(ids[i]);
	    }
	}
	/**
	 * 更新火化委托单收费项目
	 * @param itemOrder
	 */
	public void updateItemOrder(ItemOrderLog itemOrder){
		itemOrderMapper.updateItemOrder(itemOrder);
	}
	/**
	 * 获取火化委托单收费项目对象
	 * @param id
	 * @return
	 */
	public ItemOrderLog getItemOrderById(String id){
		ItemOrderLog itemOrder=itemOrderMapper.getItemOrderById(id);
		return itemOrder;
	}
	
	/**
	 * 根据条件获取火化委托单收费项目List
	 * @param String name
	 */
	public List getItemOrderList(Map paramMap){
		List list= itemOrderMapper.getItemOrderList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取火化委托单收费项目PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo getItemOrderPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List list=getItemOrderList(paramMap);
		PageInfo page = new PageInfo(list);
		return page; 
	}
}
