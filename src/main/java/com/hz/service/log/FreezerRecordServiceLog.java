package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.FreezerRecordLog;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.mapper.log.FreezerRecordMapperLog;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.mapper.ye.FreezerRecordMapper;
/**
 * ��ع��¼��Ϣ
 * @author rgy
 *
 */
@Service
@Transactional
public class FreezerRecordServiceLog {
	@Autowired
	private FreezerRecordMapperLog freezerRecordMapper;
	/**
	 * ������ع��¼
	 * @param freezerRecord
	 */
	public void addFreezerRecord(FreezerRecordLog freezerRecord){
		freezerRecordMapper.saveFreezerRecord(freezerRecord);
	}
	/**
	 * ɾ����ع��¼
	 * @param id
	 */
	public void deleteFreezerRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			freezerRecordMapper.deleteFreezerRecord(id);
		}
	}
	/**
	 * �޸���ع��¼
	 * @param freezerRecord
	 */
	public void updateFreezerRecord(FreezerRecordLog freezerRecord){
		freezerRecordMapper.updateFreezerRecord(freezerRecord);
	}
	/**
	 * ��ȡ��ع��¼����
	 * @param id
	 * @return
	 */
	public FreezerRecordLog getFreezerRecordId(String id){
		return freezerRecordMapper.getFreezerRecordById(id);
	}
	/**
	 * ����������ȡ��ع��¼list
	 * @param paramMap
	 * @return
	 */
	public List getFreezerRecordList(Map paramMap){
		return freezerRecordMapper.getFreezerRecordList(paramMap);
	}
	/**
	 * ��ȡ��ع��¼�б�
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo getFreezerRecordPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//���÷�ҳ
		PageHelper.orderBy(order);//����
		List list=getFreezerRecordList(paramMap);
		PageInfo page=new PageInfo();
		return page;
		
	}
}
