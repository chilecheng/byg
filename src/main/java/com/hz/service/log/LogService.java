package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.OperationLog;
import com.hz.mapper.log.LogMapper;

/**
 * @author cjb
 * 记录用户日志
 */
@Service
public class LogService {
	
	@Autowired
	private LogMapper logMapper;
	
	public void add(OperationLog log) {
		logMapper.add(log);
	}
	public List<OperationLog> getOperationLogList(Map<String,Object> map){
		List<OperationLog> list=logMapper.getOperationLogList(map);
		return list;
	}
	
	
	
	/**
	 * 获取用户操作日志分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */	
	public PageInfo<Object> getOperationLogPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		//按条件查询到所有基本减免申请记录
		List<OperationLog> list=getOperationLogList(paramMap);		
		PageInfo<Object> page = new PageInfo(list);
		page.setPageSize(pageSize);
		return page; 
	}

}

