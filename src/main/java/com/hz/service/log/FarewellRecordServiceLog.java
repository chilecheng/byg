package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.FarewellRecordLog;
import com.hz.entity.ye.FarewellRecord;
import com.hz.mapper.log.FarewellRecordMapperLog;
/**
 * 告别厅记录信息
 * @author rgy
 *
 */
@Service
@Transactional
public class FarewellRecordServiceLog {
	@Autowired
	private FarewellRecordMapperLog farewellRecordMapper;
	/**
	 * 添加告别厅记录
	 * @param farewellRecord
	 */
	public void addFarewellRecord(FarewellRecordLog farewellRecord){
		farewellRecordMapper.saveFarewellRecord(farewellRecord);
	}
	/**
	 * 删除告别厅记录
	 * @param id
	 */
	public void deleteFarewellRecord(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			farewellRecordMapper.deleteFarewellRecord(id);
		}
	}
	/**
	 * 修改告别厅记录
	 * @param farewellRecord
	 */
	public void updateFarewellRecord(FarewellRecordLog farewellRecord){
		farewellRecordMapper.updateFarewellRecord(farewellRecord);;
	}
	/**
	 * 获取告别厅记录对象
	 * @param id
	 * @return
	 */
	public FarewellRecordLog getFarewellRecordId(String id){
		return farewellRecordMapper.getFarewellRecordById(id);
	}
	/**
	 * 根据条件获取告别厅记录list
	 * @param paramMap
	 * @return
	 */
	public List<FarewellRecord> getFarewellRecordList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return farewellRecordMapper.getFarewellRecordList(paramMap);
	}
	/**
	 * 获取告别厅记录列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<FarewellRecord> getFarewellRecordPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<FarewellRecord> list=getFarewellRecordList(paramMap, order);
		PageInfo<FarewellRecord> page=new PageInfo<FarewellRecord>(list);
		return page;
		
	}
}
