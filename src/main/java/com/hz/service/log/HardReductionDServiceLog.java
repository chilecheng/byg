package com.hz.service.log;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.log.HardReductionDLog;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BaseReduction;
import com.hz.entity.ye.FarewellRecord;
import com.hz.entity.ye.FreezerRecord;
import com.hz.entity.ye.HardReduction;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.MourningRecord;
import com.hz.mapper.log.HardReductionDMapperLog;
import com.hz.mapper.ye.AshesRecordDMapper;
import com.hz.mapper.ye.AshesRecordMapper;
import com.hz.mapper.ye.BaseReductionMapper;
import com.hz.mapper.ye.FarewellRecordMapper;
import com.hz.mapper.ye.FreezerRecordMapper;
import com.hz.mapper.ye.HardReductionDMapper;
import com.hz.mapper.ye.HardReductionMapper;
import com.hz.mapper.ye.MourningRecordMapper;
/**
 * ���Ѽ���������Ŀ
 * @author rgy
 *
 */
@Service
@Transactional
public class HardReductionDServiceLog {
	@Autowired
	private HardReductionDMapperLog hardReductionDMapper;
	/**
	 * �������Ѽ���������Ŀ
	 * @param hardReductionD
	 */
	public void addHardReductionD(HardReductionDLog hardReductionD){
		hardReductionDMapper.saveHardReductionD(hardReductionD);
	}
	/**
	 * ɾ�����Ѽ���������Ŀ
	 * @param id
	 */
	public void deleteHardReductionD(String id){
		String []ids=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			hardReductionDMapper.deleteHardReductionD(id);
		}
	}
	/**
	 * �޸����Ѽ���������Ŀ
	 * @param baseReduction
	 */
	public void updateHardReductionD(HardReductionDLog hardReductionD){
		hardReductionDMapper.updateHardReductionD(hardReductionD);
	}
	/**
	 * ��ȡ���Ѽ���������Ŀ
	 * @param id
	 * @return
	 */
	public HardReductionDLog getHardReductionDId(String id){
		return hardReductionDMapper.getHardReductionDById(id);
	}
	/**
	 * ����������ȡ���Ѽ�������list
	 * @param paramMap
	 * @return
	 */
	public List getHardReductionDList(Map paramMap){
		return hardReductionDMapper.getHardReductionDList(paramMap);
	}
	/**
	 * ��ȡ���Ѽ��������б�
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo getHardReductionDPageInfo(Map paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//���÷�ҳ
		PageHelper.orderBy(order);//����
		List list=getHardReductionDList(paramMap);
		
		PageInfo page=new PageInfo();
		return page;
		
	}
}
