package com.hz.service.oldData;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.oldData.HisData;
import com.hz.entity.oldData.OldData;
import com.hz.entity.oldData.SingleData;
import com.hz.mapper.oldData.OldDataMapper;

/**
 * 老数据及单机系统数据 方法
 * @author jgj
 */
@Service
@Transactional
public class OldDataService {
	
	@Autowired
	private OldDataMapper oldDataMapper;
	/**
	 * 6.5系统 根据id查询历史数据（已迁移的数据）
	 * @param id
	 * @return
	 */
	public HisData getHisDataByID(String id){
		return oldDataMapper.getHisDataByID(id);
	}
	/**
	 * 6.5系统 根据条件查询历史数据（已迁移的数据）
	 * @param map
	 * @return
	 */
	public List<HisData> getHisDataList(Map<String,Object> map){
		return oldDataMapper.getHisDataList(map);
	}
	/**
	 * 根据条件获取6.5系统历史数据PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<HisData> getHisDataPageInfoPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<HisData> list=getHisDataList(paramMap);
		PageInfo<HisData> page = new PageInfo<HisData>(list);
		return page; 
	}
	/**
	 * 按deadID查询
	 * @param id
	 * @return
	 */
	public OldData getOldDataByID(String id){
		return oldDataMapper.getOldDataByID(id);
	}
	/**
	 * 根据条件获取 老系统数据
	 * @return
	 */
	public List<OldData> getOldDataList(Map<String, Object> paramMap){
		return oldDataMapper.getOldDataList(paramMap);
	}
	/**
	 * 根据条件获取老系统数据PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<OldData> getgetOldDataPageInfoPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<OldData> list=getOldDataList(paramMap);
		PageInfo<OldData> page = new PageInfo<OldData>(list);
		return page; 
	}
	
	
	/**
	 * 根据条件查询 单机版业务数据
	 * @param map
	 * @return
	 */
	public List<SingleData> getSingleDataList(Map<String,Object> map){
		return oldDataMapper.getSingleDataList(map);
	}
	/**
	 * 根据条件查询 单机版业务数据 分页
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<SingleData> getgetSingleDataPageInfoPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<SingleData> list=getSingleDataList(paramMap);
		PageInfo<SingleData> page = new PageInfo<SingleData>(list);
		return page; 
	}
	/**
	 * 根据业务单号查询单机数据
	 * @param id
	 * @return
	 */
	public SingleData getSingleDataByID(String id){
		return oldDataMapper.getSingleDataByID(id);
	}
	
}
