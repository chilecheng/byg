package com.hz.service.payDivision;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.payDivision.BillRecordD;
import com.hz.mapper.payDivision.BillRecordDMapper;

@Service
@Transactional
public class BillRecordDService {
	@Autowired
	private BillRecordDMapper billRecordDMapper;
	
	/**
	 * ����
	 * @param billRecordD
	 */
	public void saveBillRecordD(BillRecordD billRecordD){
		billRecordDMapper.saveBillRecordD(billRecordD);
	}
}
