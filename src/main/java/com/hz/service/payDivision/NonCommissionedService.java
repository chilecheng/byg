package com.hz.service.payDivision;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Item;
import com.hz.entity.payDivision.BatchLosses;
import com.hz.entity.payDivision.NonCommissioned;
import com.hz.entity.ye.AshesRecord;
import com.hz.entity.ye.AshesRecordD;
import com.hz.entity.ye.BuyGoodD;
import com.hz.entity.ye.BuyWreathRecord;
import com.hz.entity.ye.Buygood;
import com.hz.entity.ye.ListFuneralOrderRecord;
import com.hz.mapper.payDivision.NonCommissionedMapper;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.service.ye.AshesRecordDService;
import com.hz.service.ye.AshesRecordService;
import com.hz.service.ye.BuyWreathRecordDService;
import com.hz.service.ye.BuyWreathRecordService;
import com.hz.service.ye.FuneralBuyService;
import com.hz.socket.SystemWebSocketHandler;
import com.hz.util.Const;
import com.hz.util.ItemConst;
/**
 * 非委托业务收费 业务层
 * @author jgj
 *
 */
@Service
@Transactional
public class NonCommissionedService {
	@Autowired
	private NonCommissionedMapper  nonCommissionedMapper;	
	//骨灰寄存
	@Autowired
	private AshesRecordDService ashesRecordDService;
	//花圈花篮
	@Autowired
	private BuyWreathRecordService buyRecordService;
	//丧葬用品购买
	@Autowired
	private FuneralBuyService funeralBuyService;
	//骨灰寄存
	@Autowired
	private AshesRecordService ashesRecordService;
	@Autowired
	DeptService deptService;
	@Autowired
	UserNoticeService userNoticeService;
	/**
	 * websocket 收费完成后 给各用户推送消息
	 */
	@Autowired
	private SystemWebSocketHandler socketHandler;
	/**
	 * 根据条件查询出非委托业务
	 * @param paramMap
	 * @return
	 */
	public List<NonCommissioned> getNonCommissionedList(Map<String,Object> paramMap){
		List<NonCommissioned> list=nonCommissionedMapper.getNonCommissionedChargeList(paramMap);
		return list;
	}
	/**
	 * 
	 * @param type
	 */
	public void updataRevoke(String type,String id){
		//以下 根据流水号来改不同的数据表
		if("SZYP".equals(type)){//丧葬用品
			List<ListFuneralOrderRecord> list=funeralBuyService.findOrderRecordById(id);
			ListFuneralOrderRecord orderRecord=list.get(0);
			orderRecord.setPayName(null);
			orderRecord.setPayTime(null);
			orderRecord.setActualAmount(0);
			orderRecord.setGiveChange(0);			
			orderRecord.setPayFlag(Const.Pay_No);
			funeralBuyService.updateOrderRecord(orderRecord);
		}else if("HQHL".equals(type)){//花圈花篮
			BuyWreathRecord buyRecord=buyRecordService.getBuyWreathRecordId(id);
			buyRecord.setPayFlag(Const.Pay_No);
			buyRecord.setPayName(null);
			buyRecord.setPayTime(null);
			buyRecord.setActualAmount(0);
			buyRecord.setGiveChange(0);
			buyRecordService.updateBuyWreathRecord(buyRecord);
		}else if("GHJC".equals(type)){//骨灰寄存
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("recordId", id);
			List<AshesRecordD> list=ashesRecordDService.getAshesRecordDList(paramMap);			
			for(int i=0;i<list.size();i++){
				AshesRecordD ard=list.get(i);
				ard.setActualAmount(0);
				ard.setPayFlag(Const.Pay_No);
				ard.setPayName(null);
				ard.setPayTime(null);
				ard.setGiveChange(0);
				ashesRecordDService.updateAshesRecordD(ard);
			}
		}
	}
	
	public void saveNonCommissioned(String id,String payName,Timestamp payTime,String comment,double actualAmount,double 
			giveChange,byte payType,String type){
		if("SZYP".equals(type)){//丧葬用品
			List<ListFuneralOrderRecord> list=funeralBuyService.findOrderRecordById(id);
			ListFuneralOrderRecord orderRecord=list.get(0);
			orderRecord.setPayName(payName);
			orderRecord.setPayTime(payTime);
			orderRecord.setActualAmount(orderRecord.getActualAmount()+actualAmount);
			orderRecord.setGiveChange(orderRecord.getGiveChange()+giveChange);
			orderRecord.setRemarktext(comment);
			//这里添加之前的付款情况 
			double frontPay=orderRecord.getActuallyPaid();
			orderRecord.setActuallyPaid(frontPay+actualAmount-giveChange);
			orderRecord.setPayType(payType);
			orderRecord.setPayFlag(Const.Pay_Yes);
			orderRecord.setDealIsdel(Const.No_treated);
			funeralBuyService.updateOrderRecord(orderRecord);
		}else if("HQHL".equals(type)){//花圈花篮
			BuyWreathRecord buyRecord=buyRecordService.getBuyWreathRecordId(id);
			buyRecord.setPayFlag(Const.Pay_Yes);
			buyRecord.setPayName(payName);
			buyRecord.setPayTime(payTime);
			buyRecord.setPayType(payType);
			buyRecord.setActualAmount(buyRecord.getActualAmount()+actualAmount);
			buyRecord.setGiveChange(buyRecord.getGiveChange()+giveChange);
			buyRecord.setComment(comment);
			buyRecord.setActuallyPaid(buyRecord.getActuallyPaid()+actualAmount-giveChange);
			buyRecordService.updateBuyWreathRecord(buyRecord);
		}else if("GHJC".equals(type)){//骨灰寄存
			Map<String,Object> paramMap=new HashMap<String, Object>();
			paramMap.put("recordId", id);
			List<AshesRecordD> list=ashesRecordDService.getAshesRecordDList(paramMap);	
			//此处可能会有问题???
			for(int i=0;i<list.size();i++){				
				AshesRecordD ard=list.get(i);
				if(ard.getPayFlag()==Const.Pay_No){					
					ard.setActualAmount(ard.getActualAmount()+actualAmount);
					ard.setPayFlag(Const.Pay_Yes);
					ard.setPayName(payName);
					ard.setPayTime(payTime);
					ard.setPayType(payType);
					ard.setGiveChange(ard.getGiveChange()+giveChange);
					ard.setActuallyPaid(ard.getActuallyPaid()+actualAmount-giveChange);
					ashesRecordDService.updateAshesRecordD(ard);
				}
			}
			AshesRecord ar=ashesRecordService.getAshesRecordById(id);
			ar.setComment(comment);
			ashesRecordService.updateAshesRecord(ar);
		}
		
		//   丧葬 用品         left-menu 信息提示
		byte funeralByte =0;// 判断  丧葬用品任务列表中中是否有对应项
		/*if("SZYP".equals(type)){
			Map<String,Object> maps=new HashMap<String,Object>();
			maps.put("id", id);
			List<Buygood> listBuyGood=funeralBuyService.findItemOrder(maps);
			Buygood orderRecord=listBuyGood.get(0);
			List<BuyGoodD> list= orderRecord.getListbuyGoodD();
			for(int j=0;j<list.size();j++){
				Item item =list.get(j).item;
				byte sort=item.getSort();
	    		if(sort==ItemConst.BaoBuHe_Sort || sort== ItemConst.XiaoHuaQuan_Sort ||
	    				sort == ItemConst.BaoHuJi_Sort || sort == ItemConst.GuHuiHe_Sort || sort == ItemConst.HongXiaoDai_Sort ||
	    				sort == ItemConst.ShouBei_Sort || sort == ItemConst.ShouYi_Sort ||sort == ItemConst.YuSan_Sort){
	    			funeralByte = 1;
	    			break;
	    		}		
			}
			if(funeralByte ==1){
				userNoticeService.updateUserNoticeByNoticeType(Const.NoticeType_FarewellTask);
				String deptId = deptService.getDeptIdByName("一科");
				socketHandler.sendMessageToUsers(deptId,new TextMessage("funeralNumber"));
			}		
		}*/
		
		
	}
	
	/**
	 * 获取非委托业务分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<NonCommissioned> getNonCommissionedPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		//按条件查询到所有记录
		List<NonCommissioned> list=getNonCommissionedList(paramMap);
		PageInfo<NonCommissioned> page =  new PageInfo<NonCommissioned>(list);
		return page; 
	}
	
}
