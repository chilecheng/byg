package com.hz.service.payDivision;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.payDivision.CreditCharges;
import com.hz.entity.payDivision.CreditChargesView;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.payDivision.CreditChargesMapper;
import com.hz.mapper.payDivision.CreditChargesViewMapper;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.ItemOrderService;
import com.hz.util.Const;
/**
 * 新建挂账收费    按个人收费
 * @author jgj 
 *
 */
@Service
@Transactional
public class CreditChargesViewService {
	@Autowired
	private CreditChargesViewMapper creditChargesViewMapper;
	@Autowired
	private CreditChargesMapper creditChargesMapper;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	/**
	 * 按条件查询 挂账人员列表
	 * @param map
	 * @return
	 */
	public List<CreditChargesView> getCreditChargesViewList(Map<String,Object> map){
		return creditChargesViewMapper.getCreditChargesViewList(map);
	}
	
	public PageInfo<CreditChargesView> getgetCreditChargesViewPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<CreditChargesView> list=getCreditChargesViewList(paramMap);
		PageInfo<CreditChargesView> page=new PageInfo<CreditChargesView>(list);
		return page;
		
	}
	/**
	 * 保存挂账收费记录
	 * @param creditCharges
	 */
	public void saveCreditCharges(CreditCharges creditCharges){
		creditChargesMapper.saveCreditCharges(creditCharges);
	}
	public CreditCharges getCreditChargesByOrderId(String commissionOrderId){
		return creditChargesMapper.getCreditChargesByOrderId(commissionOrderId);
	}
	/**
	 * 事务控制  更新收费状态
	 * @param credit
	 * @param id
	 */
	public void saveAllPayFlag(CreditCharges credit,String id,Timestamp payTime){
		//1.保存到挂账收费记录表中：y_credit_charges
    	saveCreditCharges(credit);
    	//2.更新y_item_order表中的 bill_pay_flag字段
    	Map<String,Object> paramMap=new HashMap<String,Object>();
    	paramMap.put("commissionOrderId", id);
    	List<ItemOrder> itemList=itemOrderService.getItemOrderList(paramMap);
    	for(int i=0;i<itemList.size();i++){
    		ItemOrder io=itemList.get(i);
    		io.setBillPayFlag(Const.Is_Yes);
    		io.setPayTime(payTime);
    		itemOrderService.updateItemOrder(io);
    	}
    	//3.更新火化委托单中的pay_flag为yes
    	commissionOrderService.updatePayFlag(id, Const.Pay_Yes);
	}
	
}
