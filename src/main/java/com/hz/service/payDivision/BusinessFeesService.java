package com.hz.service.payDivision;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.payDivision.BusinessFees;
import com.hz.entity.ye.BaseReductionD;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.HardReductionD;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.payDivision.BusinessFeesMapper;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.service.ye.BaseReductionDService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.HardReductionDService;
import com.hz.service.ye.ItemOrderService;
import com.hz.socket.NoticeHandleService;
import com.hz.util.Const;

/**
 * 委托业务收费重做后
 * @author jgj
 */
@Service
@Transactional
public class BusinessFeesService {
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	DeptService deptService;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	private NoticeHandleService noticeHandleService;
	@Autowired
	private BusinessFeesMapper businessFeesMapper;
	@Autowired
	private BaseReductionDService baseReductionDService;
	@Autowired
	private HardReductionDService hardReductionDService;
	/**
	 * 保存收费
	 * @param businessFees
	 */
	public void saveBusinessFees(BusinessFees businessFees){
		businessFeesMapper.saveBusinessFees(businessFees);
	}
	/**
	 * 撤消收费
	 * @param payNumber
	 */
	public void deleateBusinessFees(String payNumber){
		businessFeesMapper.deleateBusinessFees(payNumber);
	}
	/**
	 * 根据收费流水号搜索出收费记录
	 * @param payNumber
	 * @return
	 */
	public BusinessFees getBusinessFeesByPayNumber(String payNumber){
		return businessFeesMapper.getBusinessFeesByPayNumber(payNumber);
	}
	/**
	 * 更新收费记录
	 * @param bus
	 */
	public void updateBusinessFees(BusinessFees bus){
		businessFeesMapper.updateBusinessFees(bus);
	}
	/**
	 * 根据火化委托单id查询收费记录（业务全视图用）
	 * @param commissionOrderId
	 * @return
	 */
	public List<BusinessFees> getBusinessFeesByComId(String commissionOrderId){
		return businessFeesMapper.getBusinessFeesByComId(commissionOrderId);
	}
	/**
	 * 根据订单流水号查询收费记录
	 * @param orderNumber
	 * @return
	 */
	public BusinessFees getBusinessFeesByOrderNumber(String orderNumber){
		return businessFeesMapper.getBusinessFeesByOrderNumber(orderNumber);
	}
	/**
	 * 委托业务进行 收费 
	 * 保存到y_order_pay_record记录表及相关
	 */
	@Transactional
	public void saveBusinessFees(BusinessFees businessFees, 
			CommissionOrder commissionOrder,List<ItemOrder> list, 
			Timestamp payTime,List<BaseReductionD> baseList,List<HardReductionD> hardList,
			String orderNumber) {
			// 用于 itemorder 数据表的 分项 更新
		for (int i = 0; i < list.size(); i++) {
			// item 更新操作直接新建表一个 关联项
			ItemOrder itemOrder = list.get(i);
			itemOrder.setPayFlag(Const.Pay_Yes);
			itemOrder.setPayTime(payTime);
			itemOrder.setCommissionOrderId(commissionOrder.getId());
			itemOrderService.updateItemOrderNew(itemOrder);
		}
		//更改基本减免的扣除时间
		if(baseList.size()>0){
			for(int i=0;i<baseList.size();i++){
				BaseReductionD baseReductionD=baseList.get(i);
				baseReductionD.setPayTime(payTime);
				baseReductionDService.updateBaseReductionD(baseReductionD);
			}
		}
		//更改困难减免的扣除时间
		if(hardList.size()>0){
			for(int i=0;i<hardList.size();i++){
				HardReductionD hardReductionD=hardList.get(i);
				hardReductionD.setPayTime(payTime);
				hardReductionDService.updateHardReductionD(hardReductionD);
			}
		}
//		BusinessFees bus=businessFeesMapper.getBusinessFeesByOrderNumber(orderNumber);
//		if(bus !=null){
//			businessFeesMapper.updateBusinessFees(businessFees);
//		}else{
//			
//		}
		businessFeesMapper.saveBusinessFees(businessFees);
		commissionOrderService.updateCommissionOrder(commissionOrder);
//		noticeHandleService.delegrateNotice(commissionOrder);  9.13号收费时发现问题，暂时注释掉
	}


	/**
	 * 委托业务进行 撤销收费
	 * 删除相应的y_order_pay_record记录表及相关修改
	 */
	@Transactional
	public void cancleBusinessFees(String payNumber, CommissionOrder commissionOrder, 
			List<ItemOrder> list,Map<String,Object> paramMap) {
		//撤销收费时  socket——number 相应的发生改变 
		noticeHandleService.cancleDelegateNotice(commissionOrder.getId());
		commissionOrderService.updateCommissionOrder(commissionOrder);
		for (int i = 0; i < list.size(); i++) {
			// item 更新操作直接新建表一个 关联项
			ItemOrder itemOrder = new ItemOrder();
			itemOrder = list.get(i);
			itemOrder.setPayFlag(Const.Pay_No);
			itemOrder.setPayTime(null);
			itemOrderService.updateItemOrderNew(itemOrder);
		}
		
		//更改基本减免的扣除时间为空，订单号为空
		baseReductionDService.updateBasePayTime(paramMap);
		//更改困难减免的扣除时间为空，订单号为空
		hardReductionDService.updateHardPayTime(paramMap);
		//删除收费记录
		businessFeesMapper.deleateBusinessFees(payNumber);

	}
	
	
}
