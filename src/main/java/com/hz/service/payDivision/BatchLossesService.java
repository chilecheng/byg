package com.hz.service.payDivision;


import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.payDivision.BatchLosses;
import com.hz.entity.payDivision.BillRecord;
import com.hz.entity.payDivision.BillRecordD;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.ye.ItemOrderMapper;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.ItemOrderService;
import com.hz.util.Const;
import com.hz.util.UuidUtil;

@Service
@Transactional
public class BatchLossesService {
	@Autowired
	private ItemOrderMapper itemOrderMapper;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private BillRecordService billRecordService;
	@Autowired
	private BillRecordDService billRecordDService;
	
	/**
	 * 根据挂账单位取得相应的挂账信息（收费科）
	 * @return
	 */
	public List<BatchLosses>  getBatchLossesList(Map<String,Object> paramMap){
		List<BatchLosses> list=itemOrderMapper.getBatchLossesList(paramMap);		
		return list;
	}
	/**
	 * 挂账记录详情
	 * @param paramMap
	 * @return
	 */
	public List<ItemOrder> getBatchLossesListByMap(Map<String,Object> paramMap){
		List<ItemOrder> list=itemOrderMapper.getBatchLossesListByMap(paramMap);		
		return list;
	}
	/**
	 * 获取挂账业务分页列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<BatchLosses> getBatchLossesPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		//按条件查询到所有基本减免申请记录
		List<BatchLosses> list=getBatchLossesList(paramMap);
		PageInfo<BatchLosses> page =null;
		if(list.size()>=1 && list.get(0).getSumNumber()!=0){
			page = new PageInfo<BatchLosses>(list);			
			page.setPageSize(pageSize);
		}
		return page; 
	}
	/**
	 * 保存到数据库
	 * @param billUnitId
	 * @param itemIds
	 * @param total
	 * @param payTime
	 * @param id
	 * @param commissionId
	 */
	public void saveBatchLosses(String billUnitId,String [] itemIds,double total,Timestamp payTime,
			String id,String[] commissionId){
		//保存到y_bill_record表
    	BillRecord billRecord=new BillRecord();
    	billRecord.setId(id);
    	billRecord.setBillUnitId(billUnitId);
    	billRecord.setAmount(total);
    	billRecord.setTime(payTime);
    	billRecordService.saveBillRecord(billRecord);
    	
    	//保存到y_bill_record_d表及y_item_order表
    	for(int i=0;i<itemIds.length;i++){   
    		String itemId=itemIds[i];
    		
    		BillRecordD billRecordD=new BillRecordD();
    		billRecordD.setId(UuidUtil.get32UUID());
    		billRecordD.setTime(payTime);
    		billRecordD.setBillRecordId(id);
    		billRecordD.setItemId(itemId);
    		billRecordDService.saveBillRecordD(billRecordD);    		
    		
    		ItemOrder item=itemOrderService.getItemOrderById(itemId);
    		item.setBillPayFlag(Const.IsHave_Yes);
    		item.setPayTime(payTime);
    		itemOrderService.updateItemOrder(item);
    	}
    	//更新火化委托单的状态
    	
    	for(int i=0;i<commissionId.length;i++){
    		if(i==0 || commissionId[i]!=commissionId[i-1]){
    			CommissionOrder commissionOrder=commissionOrderService.getCommissionOrderById(commissionId[i]);
//    			commissionOrder.setPayFlag(Const.IsHave_Yes);
    			commissionOrderService.updateCommissionOrder(commissionOrder);
    		}
    	}    	
	}
	
}
