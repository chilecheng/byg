package com.hz.service.payDivision;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.payDivision.BillRecord;
import com.hz.mapper.payDivision.BillRecordMapper;

@Service
@Transactional
public class BillRecordService {
	@Autowired
	private BillRecordMapper billRecordMapper;	
	
	/**
	 * ����
	 * @param billRecord
	 */
	public void saveBillRecord(BillRecord billRecord){
		billRecordMapper.saveBillRecord(billRecord);
	}
	
}
