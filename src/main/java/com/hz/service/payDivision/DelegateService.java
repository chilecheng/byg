package com.hz.service.payDivision;

import java.sql.Timestamp;
import java.util.List;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.ye.CommissionOrder;
import com.hz.entity.ye.DelegateRecord;
import com.hz.entity.ye.DelegateRecordNew;
import com.hz.entity.ye.ItemOrder;
import com.hz.mapper.payDivision.DelegateMapper;
import com.hz.service.socket.UserNoticeService;
import com.hz.service.system.DeptService;
import com.hz.service.ye.CommissionOrderService;
import com.hz.service.ye.ItemOrderService;
import com.hz.socket.NoticeHandleService;
import com.hz.util.Const;

/**
 * 委托业务收费项
 * @author gpf
 */
@Service
@Transactional
public class DelegateService {
	@Autowired
	private DelegateMapper delegateMapper;
	@Autowired
	private CommissionOrderService commissionOrderService;
	@Autowired
	private ItemOrderService itemOrderService;
	@Autowired
	DeptService deptService;
	@Autowired
	UserNoticeService userNoticeService;
	@Autowired
	private NoticeHandleService noticeHandleService;
	/**
	 * 根据条件获取相应的
	 */
	public List<DelegateRecord> getdelegateList(Map<String, Object> paramMap) {
		return delegateMapper.getdelegateList(paramMap);
	}
	
	/**
	 * 新加：搜索收费列表
	 */
	public List<DelegateRecordNew> getItemOrderPayList(Map<String, Object> paramMap) {
		return delegateMapper.getDelegateListGroupByTime(paramMap);
	}

	/**
	 * 根据条件获取相应的 丧葬用品   任务列表  统计 （统计按审核时间 查询）
	 */
	public List<DelegateRecord> getdelegateListByPayTime(Map<String, Object> paramMap) {
		return delegateMapper.getOrderPayRecordList(paramMap);
	}

	public PageInfo<DelegateRecord> getdelegateRecordPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize,
			String order) {
		PageHelper.startPage(pageNum, pageSize);// 设置分页
		PageHelper.orderBy(order);// 排序
		List<DelegateRecord> list = getdelegateList(paramMap);
		PageInfo<DelegateRecord> page = new PageInfo<DelegateRecord>(list);
		return page;
	}
	/**
	 * 新加收费分组搜索结果  group by pay_time 按收费时间分组，没有就是未收费的
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<DelegateRecordNew> getItemOrderPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize,
			String order) {
		PageHelper.startPage(pageNum, pageSize);// 设置分页
		PageHelper.orderBy(order);// 排序
		List<DelegateRecordNew> list = getItemOrderPayList(paramMap);
		PageInfo<DelegateRecordNew> page = new PageInfo<DelegateRecordNew>(list);
		return page;
	}
	/**
	 * 委托业务进行 收费 保存到相应的delegrate记录表
	 */
	public void saveDelegateRecord(DelegateRecord delegrateRecord, CommissionOrder commissionOrder,
			List<ItemOrder> list, Timestamp payTime, String id) {
		// 用于 itemorder 数据表的 分项 更新
		for (int i = 0; i < list.size(); i++) {
			// item 更新操作直接新建表一个 关联项
			ItemOrder itemOrder = new ItemOrder();
			itemOrder = list.get(i);
			itemOrder.setPayFlag(Const.Pay_Yes);
			itemOrder.setPayTime(payTime);
			itemOrder.setCommissionOrderId(id);
			itemOrderService.updateItemOrderNew(itemOrder);
		}
		commissionOrderService.updateCommissionOrder(commissionOrder);
		delegateMapper.saveDelegateRecord(delegrateRecord);
		noticeHandleService.delegrateNotice(commissionOrder);
	}
	/**
	 * 保存火化委托单更改后的 收费记录
	 * @param delegrateRecord
	 */
	public void updateDelegateRecord(DelegateRecord delegrateRecord, CommissionOrder commissionOrder,
			List<ItemOrder> list, Timestamp payTime, String id) {
		// 用于 itemorder 数据表的 分项 更新
				for (int i = 0; i < list.size(); i++) {
					// item 更新操作直接新建表一个 关联项
					ItemOrder itemOrder = new ItemOrder();
					itemOrder = list.get(i);
					itemOrder.setPayFlag(Const.Pay_Yes);
					itemOrder.setPayTime(payTime);
					itemOrder.setCommissionOrderId(id);
					itemOrderService.updateItemOrderNew(itemOrder);
				}
		commissionOrderService.updateCommissionOrder(commissionOrder);
		delegateMapper.updateDelegateRecord(delegrateRecord);
		noticeHandleService.delegrateNotice(commissionOrder);
	}

	/**
	 * 获取火化委托单对象
	 */
	public DelegateRecord getorderById(String id, byte checkFlag, byte tickFlag) {
		return delegateMapper.getOrderById(id, checkFlag, tickFlag);
	}

	/**
	 * 委托业务进行 收费 撤销收费 时 删除相应的 的delegrate记录表
	 */
	public void cancleDelegateRecord(String id, CommissionOrder commissionOrder, List<ItemOrder> list) {
		//撤销收费时  socket——number 相应的发生改变 
		noticeHandleService.cancleDelegateNotice(id);
		
		commissionOrderService.updateCommissionOrder(commissionOrder);
		for (int i = 0; i < list.size(); i++) {
			// item 更新操作直接新建表一个 关联项
			ItemOrder itemOrder = new ItemOrder();
			itemOrder = list.get(i);
			itemOrder.setPayFlag(Const.Pay_No);
			itemOrder.setPayTime(null);
			itemOrderService.updateItemOrderNew(itemOrder);
		}
		delegateMapper.deleateDelegateRecord(id);

	}
	
	public double getpayAmountByorder(String orderId){
		return  delegateMapper.getpayAmountByorder(orderId);
	}
	public double getpayoddByorder(String orderId){
		return  delegateMapper.getpayOddByorder(orderId);
	}
	public double getFrontPay(String orderId ){
		return  delegateMapper.getFreontPay(orderId);
	}
	//判断是否有付费记录
	public int  getBoolPayed(String orderId){
		return delegateMapper.getBoolPayed(orderId);
	}
	
	/**
	 * //判断是  有第二次的收费记录 
	 * @param orderId
	 * @return
	 */
	public  int getpricrDifferById(String orderId){
		return  (int) delegateMapper.getpricrDifferById(orderId);
	}
	/**
	 * 基本减免申请中用到，取个结算日期
	 * @param orderId
	 * @return
	 */
	public Timestamp getPayTimeByOrderId(String orderId){
		return delegateMapper.getPayTimeByOrderId(orderId);
	}
}
