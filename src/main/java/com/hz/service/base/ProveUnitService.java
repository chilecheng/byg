package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.ProveUnit;
import com.hz.mapper.base.ProveUnitMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 证明单位
 * @author rgy
 */
@Service
@Transactional
public class ProveUnitService {
	@Autowired
	private ProveUnitMapper pum;
	/**
	 * 添加证明单位
	 * @param proveUnit
	 */
	public void addProveUnit(ProveUnit proveUnit){
		pum.saveProveUnit(proveUnit);
	}
	/**
	 *  删除证明单位
	 * @param id
	 * @param String id
	 */
	public void deleteProveUnit(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			pum.deleteProveUnit(ids[i]);
		}
		
	}
	/**
	 * 修改证明单位
	 * @param proveUnit
	 */
	public void updateProveUnit(ProveUnit proveUnit){
		pum.updateProveUnit(proveUnit);
	}
	/**
	 * 
	 * @param id
	 * @return ProveUnit
	 * @param String id
	 */
	public ProveUnit getProveUnitId(String id){
		return pum.getProveUnitById(id);
	}
	/**
	 * 
	 * @param paramMap
	 * @return List
	 * @param Map paramMap
	 * @return
	 */
	public List<ProveUnit> getProveUnitList(Map<String,Object> paramMap){
		return pum.getProveUnitList(paramMap);
		
	}
	/**
	 * 单位证明列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<ProveUnit> getProveUnitPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<ProveUnit> list=getProveUnitList(paramMap);
		PageInfo<ProveUnit> page=new PageInfo<ProveUnit>(list);
		return page;
		
	}
	/**
	 * 获取证明单位option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getProveUnitOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		list.add(new Object[]{"",""});
		PageHelper.orderBy("index_flag");
		List<ProveUnit> listProveUnit=pum.getProveUnitList(null);
		for (int i = 0; i < listProveUnit.size(); i++) {
			ProveUnit proveUnit=listProveUnit.get(i);
			list.add(new Object[]{proveUnit.getProveUnitId(),proveUnit.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取证明单位readOnly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getProveUnits(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");
		List<ProveUnit> listProveUnit=pum.getProveUnitList(null);
		for (int i = 0; i < listProveUnit.size(); i++) {
			ProveUnit proveUnit=(ProveUnit)listProveUnit.get(i);
			list.add(new Object[]{proveUnit.getProveUnitId(),proveUnit.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取证明单位readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getProveUnit(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");
		List<ProveUnit> listProveUnit=pum.getProveUnitList(null);
		for (int i = 0; i < listProveUnit.size(); i++) {
			ProveUnit proveUnit=(ProveUnit)listProveUnit.get(i);
			list.add(new Object[]{proveUnit.getProveUnitId(),proveUnit.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 更改证明单位是否启动标志
	 * @param id
	 * @param isdel
	 * @return
	 */
	public void updateisdelProve(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			ProveUnit proveUnit=pum.getProveUnitById(ids[i]);
			if(isdel==Const.Isdel_No){
				proveUnit.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				proveUnit.setIsdel(Const.Isdel_Yes);
			}
			updateProveUnit(proveUnit);
		}
		
	}
	/**
	 * 更新证明单位保存
	 * @param proveUnitId
	 * @param proveUnit
	 */
	public void addOrUpdate(String proveUnitId, ProveUnit proveUnit) {
		if(proveUnitId!=null&&!proveUnitId.equals("")){
			updateProveUnit(proveUnit);
		}else{
			proveUnit.setProveUnitId(UuidUtil.get32UUID());
			proveUnit.setIsdel(Const.Isdel_No);
			addProveUnit(proveUnit);
		}
	}
}
