package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.TypeFees;
import com.hz.mapper.base.TypeFeesMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 收费项目类型
 * @author jgj
 */
@Service
@Transactional
public class TypeFeesService {
	@Autowired
	private TypeFeesMapper typeFeesMapper;
	/**
	 * 添加收费项目类型
	 * @param typeFees
	 */
	public void addTypeFees(TypeFees typeFees){
		typeFeesMapper.saveTypeFees(typeFees);
	}
	/**
	 * 删除收费项目类型
	 * @param id
	 * @param String id
	 */
	public void deleteTypeFees(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			typeFeesMapper.deleteTypeFees(ids[i]);
		}
		
	}
	/**
	 * 修改收费项目类型
	 * @param typeFrees
	 */
	public void updateTypeFees(TypeFees typeFees){
		typeFeesMapper.updateTypeFees(typeFees);
	}
	/**
	 * 根据ID查找收费项目类型
	 * @param id
	 * @param String id
	 */
	public TypeFees getTypeFeesById(String id){
		
		return typeFeesMapper.getTypeFeesById(id);
	}
	/**
	 * 查找所有的收费项目类型
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<TypeFees> getTypeFeesList(Map<String, Object> paramMap){
		List<TypeFees> list = typeFeesMapper.getTypeFeesList(paramMap);
		return list;
	}
	/**
	 * 收费项目类型信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<TypeFees> getTypeFeesPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<TypeFees> list=getTypeFeesList(paramMap);
		PageInfo<TypeFees> page=new PageInfo<TypeFees>(list);
		return page;
		
	}
	/**
	 * 获取收费项目类型信息option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getTypeFeesOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<TypeFees> listTypeFees=typeFeesMapper.getTypeFeesList(null);
		for (int i = 0; i < listTypeFees.size(); i++) {
			TypeFees typeFees=(TypeFees) listTypeFees.get(i);
			list.add(new Object[]{typeFees.getId(),typeFees.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改收费项目类型是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			TypeFees typeFees=getTypeFeesById(ids[i]);
			if(isdel==Const.Isdel_No){
				typeFees.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				typeFees.setIsdel(Const.Isdel_Yes);
			}
			updateTypeFees(typeFees);
		}
	}
	/**
	 * 更新收费项目类型保存
	 * @param id
	 * @param typeFrees
	 */
	public void addOrUpdate(String id, TypeFees typeFees) {
		if(id!=null&&!id.equals("")){
			updateTypeFees(typeFees);
		}else{
			typeFees.setId(UuidUtil.get32UUID());
			typeFees.setIsdel(Const.Isdel_No);
			addTypeFees(typeFees);
		}
	}
}
