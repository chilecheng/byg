package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.FreezerType;
import com.hz.mapper.base.FreezerTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 冷藏柜类型
 * @author tsp
 *
 */
@Service
@Transactional
public class FreezerTypeService {
	@Autowired
	private FreezerTypeMapper freezerTypeMapper;
	
	/**
	 * 添加冷藏柜类型
	 * @param freezerType
	 */
	public void addFreezerType(FreezerType freezerType){
		freezerTypeMapper.saveFreezerType(freezerType);
	}
	/**
	 * 删除冷藏柜类型
	 * @param id
	 */
	public void deleteFreezerType(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
		freezerTypeMapper.deleteFreezerType(ids[i]);
	    }
	}
	/**
	 * 更新冷藏柜类型
	 * @param freezerType
	 */
	public void updateFreezerType(FreezerType freezerType){
		freezerTypeMapper.updateFreezerType(freezerType);
	}
	/**
	 * 获取冷藏柜类型对象
	 * @param id
	 * @return
	 */
	public FreezerType getFreezerTypeById(String id){
		FreezerType freezerType=freezerTypeMapper.getFreezerTypeById(id);
		return freezerType;
	}
	/**
	 * 根据条件获取冷藏柜类型
	 * @param String name
	 * @param byte isdel
	 */
	public List<FreezerType> getFreezerTypeList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return freezerTypeMapper.getFreezerTypeList(paramMap);
	}
	/**
	 * 根据条件获取冷藏柜类型PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<FreezerType> getFreezerTypePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<FreezerType> list=freezerTypeMapper.getFreezerTypeList(paramMap);
		PageInfo<FreezerType> page = new PageInfo<FreezerType>(list);
		return page; 
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			FreezerType freezerType=getFreezerTypeById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		freezerType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				freezerType.setIsdel(Const.Isdel_Yes);
			}
	    	updateFreezerType(freezerType);
	    }
	}
	/**
	 * 获取冷藏柜类型Option
	 * @param typeId
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getFreezerTypeOption(String typeId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");
		List<FreezerType> freezerTypeList=freezerTypeMapper.getFreezerTypeList(null);
		for (int i = 0; i < freezerTypeList.size(); i++) {
			FreezerType freezerType=(FreezerType)freezerTypeList.get(i);
			list.add(new Object[]{freezerType.getId(),freezerType.getName()});
		}
		String option = DataTools.getOptionByList(list, typeId, isAll);
		return option;
	}
}
