package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.MourningType;
import com.hz.mapper.base.MourningTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 灵堂类型
 * @author rgy
 */
@Service
@Transactional
public class MourningTypeService {
	@Autowired
	private MourningTypeMapper mourningTypeMapper;
	/**
	 * 添加灵堂类型
	 * @param mourningType
	 */
	public void addMourningType(MourningType mourningType){
		mourningTypeMapper.saveMourningType(mourningType);
	}
	/**
	 * 删除灵堂类型
	 * @param id
	 * @param String id
	 */
	public void deleteMourningType(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			mourningTypeMapper.deleteMourningType(ids[i]);
		}
		
	}
	/**
	 * 修改灵堂类型
	 * @param mourningType
	 */
	public void updateMourningType(MourningType mourningType){
		mourningTypeMapper.updateMourningType(mourningType);;
	}
	/**
	 * 查找灵堂类型id
	 * @param id
	 * @param String id
	 */
	public MourningType getMourningTypeId(String id){
		return mourningTypeMapper.getMourningTypeById(id);
	}
	/**
	 * 查找所有灵堂类型
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<MourningType> getMourningTypeList(Map<String,Object> paramMap){
		return mourningTypeMapper.getMourningTypeList(paramMap);
	}
	/**
	 * 灵堂类型列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<MourningType> getMourningType(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<MourningType> list=getMourningTypeList(paramMap);
		PageInfo<MourningType> page=new PageInfo<MourningType>(list);
		return page;
	}
	/**
	 * 获取灵堂类型的option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getMourningTypeOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<MourningType> listMourning=getMourningTypeList(null);
		for (int i = 0; i < listMourning.size(); i++) {
			MourningType mourningType=(MourningType)listMourning.get(i);
			list.add(new Object[]{mourningType.getId(),mourningType.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改灵堂类型是否标志
	 * @param id
	 * @param isdel
	 */
	public void updateMourningIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			MourningType mc=getMourningTypeId(ids[i]);
			if(isdel==Const.Isdel_No){
				mc.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				mc.setIsdel(Const.Isdel_Yes);
			}
			updateMourningType(mc);;
		}
	}
	/**
	 * 更新灵堂类型保存
	 * @param mourningId
	 * @param mourningType
	 */
	public void addOrUpdate(String mourningId, MourningType mourningType) {
		if(mourningId!=null&&!mourningId.equals("")){
			updateMourningType(mourningType);
		}else{
			mourningType.setId(UuidUtil.get32UUID());;
			mourningType.setIsdel(Const.Isdel_No);
			addMourningType(mourningType);;
		}
	}
	
}
