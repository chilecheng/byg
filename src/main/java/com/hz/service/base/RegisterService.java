package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Register;
import com.hz.mapper.base.RegisterMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;
/**
 * 户籍类
 * @author rgy
 */
@Service
@Transactional
public class RegisterService {
	@Autowired
	private RegisterMapper registerMapper;
	/**
	 * 添加户籍
	 * @param register 户籍类
	 */
	public void addRegister(Register register){
		registerMapper.saveRegister(register);
	}
	/**
	 * 删除户籍
	 * @param id 户籍类id
	 * @param String id 
	 */
	public void deleteRegister(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			registerMapper.deleteRegister(ids[i]);
		}
		
	}
	/**
	 * 修改户籍
	 * @param register 户籍类
	 */
	public void updateRegister(Register register){
		registerMapper.updateRegister(register);
	}
	/**
	 * 查询户籍id
	 * @param id 户籍类id
	 * @param String id 
	 */
	public Register getRegisterId(String id){
		return registerMapper.getRegisterById(id);
	}
	/**
	 * 查询所有的户籍
	 * @param paramMap 户籍类的name
	 * @param String name,
	 * @param byte isdel,
	 * @return
	 */
	public List<Register> getRegisterList(Map<String, Object> paramMap){
		return registerMapper.getRegisterList(paramMap);
	}
	/**
	 * 户籍列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Register> getRegisterPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<Register> list=getRegisterList(paramMap);
		PageInfo<Register> page=new PageInfo<Register>(list);
		return page;
		
	}
	/**
	 * 获取户籍option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getRegisterOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Register> listRegister=registerMapper.getRegisterList(null);
		for (int i = 0; i < listRegister.size(); i++) {
			Register register=listRegister.get(i);
			list.add(new Object[]{register.getRegisterId(),register.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取户籍readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getRegister(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Register> listRegister=registerMapper.getRegisterList(null);
		for (int i = 0; i < listRegister.size(); i++) {
			Register register=listRegister.get(i);
			list.add(new Object[]{register.getRegisterId(),register.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 更改户籍是否启动标志
	 * @param id
	 * @param isdel
	 * @return
	 */
	public void updateisdelRegister(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Register register=registerMapper.getRegisterById(id);
			if(isdel==Const.Isdel_No){
				register.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				register.setIsdel(Const.Isdel_Yes);
			}
			updateRegister(register);;
		}
		
	}
	/**
	 * 更新户籍保存
	 * @param id
	 * @param register
	 */
	public void addOrUpdate(String id, Register register) {
		if(id!=null&&!id.equals("")){
			updateRegister(register);;
		}else{
			register.setRegisterId(UuidUtil.get32UUID());;
			register.setIsdel(Const.Isdel_No);
			addRegister(register);;
		}
	}

}
