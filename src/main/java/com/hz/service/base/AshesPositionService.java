package com.hz.service.base;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.base.AshesPosition;
import com.hz.mapper.base.AshesPositionMapper;

/**
 * 骨灰盒位置
 * @author hw
 *
 */
@Service
@Transactional
public class AshesPositionService {
	@Autowired
	private AshesPositionMapper ashesPositionMapper;
	
	/**
	 * 添加骨灰盒数量
	 * @param ashesPosition
	 */
	public void addAshesPosition(AshesPosition ashesPosition){
		ashesPositionMapper.saveAshesPosition(ashesPosition);
	}
	
	/**
	 * 更新骨灰盒数量数量
	 * @param ashesPostion
	 */
	public void updateAshesPosition(AshesPosition ashesPostion){
		ashesPositionMapper.updateAshesPosition(ashesPostion);
	}
	
	/**
	 * 获取骨灰盒位置对象
	 * @param id
	 * @return
	 */
	public AshesPosition getAshesPositionById(String id){
		AshesPosition register=ashesPositionMapper.getAshesPositionById(id);
		return register;
	}
	/**
	 * 获取骨灰盒位置对象
	 * @param parmMap
	 * @return
	 */
	public List<AshesPosition> getAshesPositionList(){
		List<AshesPosition> list=ashesPositionMapper.getAshesPositionList();
		return list;
	}

	public AshesPosition getAshesPosition(){
		List<AshesPosition> list=ashesPositionMapper.getAshesPositionList();
		if(list.size()>0){
			return (AshesPosition)list.get(0);
		}else{
			return  null;
		}
	}
	
}
