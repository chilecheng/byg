package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Item;
import com.hz.mapper.base.ItemMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 收费项目
 * @author tsp
 *
 */
@Service
@Transactional
public class ItemService {
	@Autowired
	private ItemMapper itemMapper;
	/**
	 * 添加收费项目
	 * @param item
	 */
	public void addItem(Item item){
		itemMapper.saveItem(item);
	}
	/**
	 * 删除收费项目
	 * @param id
	 */
	public void deleteItem(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
		itemMapper.deleteItem(ids[i]);
	    }
	}
	/**
	 * 更新收费项目
	 * @param item
	 */
	public void updateItem(Item item){
		itemMapper.updateItem(item);
	}
	/**
	 * 获取收费项目对象
	 * @param id
	 * @return
	 */
	public Item getItemById(String id){
		Item item=itemMapper.getItemById(id);
		return item;
	}
	/**
	 * 根据助记码来获取收费项目对象
	 * @param helpCode
	 * @return
	 */
	public Item getItemByHelpCode(String helpCode){
		Item item=itemMapper.getItemByHelpCode(helpCode);
		return item;
	}
	/**
	 * 根据条件获取收费项目
	 * @param paramMap
	 * @return
	 */
	public List<Item> getItemList(Map<String, Object> paramMap){
		List<Item> list= itemMapper.getItemList(paramMap);
		return list;
	}
	/**
	 * 根据类型获取收费项目
	 * @param paramMap
	 * @return
	 */
	public List<Item> getTypeItemList(Map<String, Object> paramMap){
		List<Item> list= itemMapper.getTypeItemList(paramMap);
		return list;
	}
	/**
	 * 根据条件获取收费项目
	 * @param paramMap	条件
	 * @param order		排序
	 * @return
	 */
	public List<Item> getItemList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<Item> list=itemMapper.getItemList(paramMap);
		return list; 
	}

	/**
	 * 根据条件获取收费项目PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Item> getItemPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Item> list=itemMapper.getItemList(paramMap);
		PageInfo<Item> page = new PageInfo<Item>(list);
		return page; 
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Item item=getItemById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		item.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				item.setIsdel(Const.Isdel_Yes);
			}
	    	updateItem(item);
	    }
	}
	/**
	 * 根据类别获取收费项目Option
	 * @param paramMap 条件
	 * @param itemId 
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemOption(Map<String ,Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getItemList(paramMap);
		String option="";
		String isHave="no";
		String name="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			if(itemId.equals(item.getId())){
				isHave="yes";
			}
			list.add(new Object[]{item.getId(),item.getName()});
		}
		if("no".equals(isHave)&&!"".equals(itemId)){
			name=getItemById(itemId).getName();
			list.add(new Object[]{itemId,name});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getOptionByList(list, itemId, false,isHave);
		return option;
	}
	/**
	 * 根据类别获取收费项目readOnly
	 * @param paramMap 条件
	 * @param itemId 
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItems(Map<String ,Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getItemList(paramMap);
		String option="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			list.add(new Object[]{item.getId(),item.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getNameByList(list, itemId, false);
		return option;
	}
	/**
	 * 根据类别获取收费项目readOnly
	 * @param paramMap 条件
	 * @param itemId 
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItem(Map<String ,Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getItemList(paramMap);
		String option="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			list.add(new Object[]{item.getId(),item.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getNameByList(list, itemId, false);
		return option;
	}
	/**
	 * 根据类别获取收费项目助记码Option
	 * @param paramMap 条件
	 * @param helpCode 
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemHelpCodeOption(Map<String, Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getItemList(paramMap);
		String option="";
		String isHave="no";
		String helpCode="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			if(itemId.equals(item.getId())){
				isHave="yes";
			}
			list.add(new Object[]{item.getId(),item.getHelpCode()});
		}
		if("no".equals(isHave)&&!"".equals(itemId)){
			helpCode=getItemById(itemId).getHelpCode();
			list.add(new Object[]{itemId,helpCode});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getOptionByList(list, itemId, false,isHave);
		return option;
	}
	/**
	 * 根据类别获取收费项目助记码Option
	 * @param paramMap 条件
	 * @param helpCode 
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemHelpCodess(Map<String, Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getItemList(paramMap);
		String option="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			list.add(new Object[]{item.getId(),item.getHelpCode()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getNameByList(list, itemId, false);
		return option;
	}
	/**
	 * 根据类别获取收费项目助记码readOnly 基本
	 * @param paramMap 条件
	 * @param helpCode
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemHelpCodes(Map<String, Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getItemList(paramMap);
		String option="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			list.add(new Object[]{item.getId(),item.getHelpCode()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getNameByList(list, itemId, false);
		return option;
	}
	/**
	 * 根据类别获取收费项目助记码readOnly 困难
	 * @param paramMap 条件
	 * @param helpCode 
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemHelpCode(Map<String, Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getItemList(paramMap);
		String option="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			list.add(new Object[]{item.getId(),item.getHelpCode()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getNameByList(list, itemId, false);
		return option;
	}
	/**
	 * 根据类型获取收费项目Option
	 * @param paramMap 条件（服务类型Id）
	 * @param itemId 
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getTypeItemOption(Map<String, Object> paramMap,String itemId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<Item> itemList=itemMapper.getTypeItemList(paramMap);
		String option="";
		for (int i = 0; i < itemList.size(); i++) {
			Item item=(Item)itemList.get(i);
			list.add(new Object[]{item.getId(),item.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getOptionByList(list, itemId, false);
		return option;
	}
	/**
	 * 判断该类中是否已经存在这个名字的项目
	 * @param name
	 * @param typeId
	 * @return
	 */
	public int ifExit(String name, String typeId) {
		return itemMapper.getCount(name,typeId);
	}
	/**
	 * 检查助记码是否已经存在
	 */
	public int ifHelpCodeExit(String string) {
		return itemMapper.ifHelpCodeExit(string);
	}
	/**
	 * 根据id查找类别
	 * @param map
	 * @return
	 */
	public byte getNumberByTypeFlag(Map<String,Object> map){
		return itemMapper.getNumberByTypeFlag(map);
	}
}
