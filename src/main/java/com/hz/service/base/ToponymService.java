package com.hz.service.base;
/**
 * 地名
 * @author rgy
 *
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Toponym;
import com.hz.mapper.base.ToponymMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;
@Service
@Transactional
public class ToponymService {
	@Autowired
	private ToponymMapper tm;
	
	/**
	 * 添加地名
	 * @param Toponym 地名类
	 */
	public void addToponym(Toponym toponym){
		tm.saveToponym(toponym);;
	}
	/**
	 * 删除地名
	 * @param id 地名类id
	 * @param String id
	 */
	public void deleteToponym(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			tm.deleteToponym(ids[i]);
		}
		
	}
	/**
	 * 修改地名
	 * @param Toponym 地名类
	 */
	public void updateToponym(Toponym toponym){
		tm.updateToponym(toponym);
	}
	/**
	 * 查询地名id
	 * @param id 地名类的id
	 * @param String id
	 */
	public Toponym getToponymById(String id){
		return tm.getToponymById(id);
		
	}
	/**
	 * 查询所有的地名
	 * @param paramMap
	 * @param String name
	 * @param byte isdel
	 * @return
	 */
	public List<Toponym> getToponymList(Map<String,Object> paramMap,String order){
/*		PageHelper.orderBy(order);*/
		return tm.getToponymList(paramMap);
	}
	/**
	 * 获取地名的pageinfo
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Toponym> getToponymPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy("index_flag asc");//排序
		List<Toponym> list=getToponymList(paramMap,"");
		PageInfo<Toponym> page=new PageInfo<Toponym>(list);
		return page;
		
	}
	/**
	 * 获取地名option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getToponymOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Toponym> listToponym=tm.getToponymList(null);
		for (int i = 0; i < listToponym.size(); i++) {
			Toponym toponym=(Toponym)listToponym.get(i);
			list.add(new Object[]{toponym.getToponymId(),toponym.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取地名readOnly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getToponym(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Toponym> listToponym=tm.getToponymList(null);
		for (int i = 0; i < listToponym.size(); i++) {
			Toponym toponym=(Toponym)listToponym.get(i);
			list.add(new Object[]{toponym.getToponymId(),toponym.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 更改地名是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Toponym toponym=getToponymById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		toponym.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				toponym.setIsdel(Const.Isdel_Yes);
			}
	    	updateToponym(toponym);
	    }
	}
	/**
	 * 更新地名保存
	 * @param toponymId
	 * @param toponym
	 */
	public void addOrUpdate(String toponymId, Toponym toponym) {
		if (toponymId!=null&&!toponymId.equals("")){
			updateToponym(toponym);;
		}else {
			toponym.setToponymId(UuidUtil.get32UUID());;
			toponym.setIsdel(Const.Isdel_No);
			addToponym(toponym);
		}
	}
}
