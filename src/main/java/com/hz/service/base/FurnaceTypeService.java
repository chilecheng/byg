package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.FurnaceType;
import com.hz.mapper.base.FurnaceTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 火化炉类型
 * @author tsp
 *
 */
@Service
@Transactional
public class FurnaceTypeService {
	@Autowired
	private FurnaceTypeMapper furnaceTypeMapper;
	
	/**
	 * 添加火化炉类型
	 * @param furnaceType
	 */
	public void addFurnaceType(FurnaceType furnaceType){
		furnaceTypeMapper.saveFurnaceType(furnaceType);
	}
	/**
	 * 删除火化炉类型
	 * @param id
	 */
	public void deleteFurnaceType(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
		furnaceTypeMapper.deleteFurnaceType(ids[i]);
	    }
	}
	/**
	 * 更新火化炉类型
	 * @param furnaceType
	 */
	public void updateFurnaceType(FurnaceType furnaceType){
		furnaceTypeMapper.updateFurnaceType(furnaceType);
	}
	/**
	 * 获取火化炉类型对象
	 * @param id
	 * @return
	 */
	public FurnaceType getFurnaceTypeById(String id){
		FurnaceType furnaceType=furnaceTypeMapper.getFurnaceTypeById(id);
		return furnaceType;
	}
	/**
	 * 根据条件获取火化炉类型
	 * @param String name
	 * @param byte isdel
	 */
	public List<FurnaceType> getFurnaceTypeList(Map<String,Object> paramMap){
		List<FurnaceType> list= furnaceTypeMapper.getFurnaceTypeList(paramMap);
		return list;
	}
	/**
	 * 根据条件获取火化炉类型PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<FurnaceType> getFurnaceTypePageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<FurnaceType> list=furnaceTypeMapper.getFurnaceTypeList(paramMap);
		PageInfo<FurnaceType> page = new PageInfo<FurnaceType>(list);
		return page; 
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			FurnaceType furnaceType=getFurnaceTypeById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		furnaceType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				furnaceType.setIsdel(Const.Isdel_Yes);
			}
	    	updateFurnaceType(furnaceType);
	    }
	}
	/**
	 * 获取火化炉类型Option
	 * @param typeId
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getFurnaceTypeOption(String typeId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<FurnaceType> furnaceTypeList=furnaceTypeMapper.getFurnaceTypeList(null);
		for (int i = 0; i < furnaceTypeList.size(); i++) {
			FurnaceType furnaceType=(FurnaceType)furnaceTypeList.get(i);
			list.add(new Object[]{furnaceType.getId(),furnaceType.getName()});
		}
		String option = DataTools.getOptionByList(list, typeId, isAll);
		return option;
	}
}
