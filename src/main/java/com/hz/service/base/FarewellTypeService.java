package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.FarewellType;
import com.hz.mapper.base.FarewellTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 告别厅类型
 * @author rgy
 */
@Service
@Transactional
public class FarewellTypeService {
	@Autowired 
	private FarewellTypeMapper farewellTypeMapper;
	/**
	 * 添加告别厅类型
	 * @param farewellType
	 */
	public void addFarewellType(FarewellType farewellType){
		farewellTypeMapper.saveFarewellType(farewellType);
	}
	/**
	 * 删除告别厅类型
	 * @param id
	 * @param String id
	 */
	public void deleteFarewellType(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			farewellTypeMapper.deleteFarewellType(ids[i]);;
		}
		
	}
	/**
	 * 修改告别厅类型
	 * @param farewellType
	 */
	public void updateFarewellType(FarewellType farewellType){
		farewellTypeMapper.updateFarewellType(farewellType);;
	}
	/**
	 * 查询告别厅类型id
	 * @param id
	 * @param String id
	 */
	public FarewellType getFarewellTypeById(String id){
		return farewellTypeMapper.getFarewellTypeById(id);
	}
	/**
	 * 查询所有告别厅类型
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<FarewellType> getFarewellTypeList(Map<String,Object> paramMap){
		return farewellTypeMapper.getFarewellTypeList(paramMap);
	}
	/**
	 * 告别厅类型列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<FarewellType> getFarewellTypepageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<FarewellType> list=getFarewellTypeList(paramMap);
		PageInfo<FarewellType> page=new PageInfo<FarewellType>(list);
		return page;
	}
	/**
	 * 获取告别厅类型option
	 * @param id
	 * @param isAll	(true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getFarewellTypeOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<FarewellType> listFarewell=farewellTypeMapper.getFarewellTypeList(null);
		for (int i = 0; i < listFarewell.size(); i++) {
			FarewellType farewellType= listFarewell.get(i);
			list.add(new Object[]{farewellType.getId(),farewellType.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 修改告别厅类型的启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateFarewellIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			FarewellType farewellType=getFarewellTypeById(ids[i]);
			if(isdel==Const.Isdel_No){
				farewellType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				farewellType.setIsdel(Const.Isdel_Yes);
			}
			updateFarewellType(farewellType);
		}
	}
	/**
	 * 更新告别厅类型
	 * @param farewellId
	 * @param farewellType
	 */
	public void addOrUpdate(String farewellId, FarewellType farewellType) {
		if(farewellId!=null&&!farewellId.equals("")){
			updateFarewellType(farewellType);;
		}else{
			farewellType.setId(UuidUtil.get32UUID());;
			farewellType.setIsdel(Const.Isdel_No);
			addFarewellType(farewellType);
		}
	}
	
}
