package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Hospital;
import com.hz.mapper.base.HospitalMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 医院类
 * @author rgy
 * */
@Service
@Transactional
public class HospitalService {

	@Autowired
	private HospitalMapper hospitalMapper;
	/**
	 * 添加医院
	 * @param hospital
	 */
	public void DoAddHospital(Hospital hospital){
		hospitalMapper.saveHospital(hospital);
	}
	/**
	 * 删除医院
	 * @param id
	 * @param String id
	 */
	public void DoDeleteHospital(String id){
		String ids[]=id.split(",");//去空格
		for (int i = 0; i < ids.length; i++) {
			hospitalMapper.deleteHospital(ids[i]);;
		}
		
	}
	/**
	 * 修改医院
	 * @param hospital
	 */
	public void DoUpdateHospital(Hospital hospital){
		hospitalMapper.updateHospital(hospital);;
	}
	/**
	 * 查询医院Id
	 * @param id
	 * @param String id
	 */
	public Hospital getHospitalId(String id){
		return hospitalMapper.getHospitalById(id);
	}
	/**
	 * 查询所有医院
	 * @param paramMap
	 * @param String name
	 * @param byte isdel
	 * @return
	 */
	public List<Hospital> getHospitalList(Map<String,Object> paramMap){
		List<Hospital> list=hospitalMapper.getHospitalList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取医院的pageinfo
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Hospital> getHospitalPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<Hospital> list=getHospitalList(paramMap);
		PageInfo<Hospital> page=new PageInfo<Hospital>(list);
		return page;		
	}
	/**
	 * 获取医院option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getHospitalOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Hospital> listHospital=hospitalMapper.getHospitalList(null);
		for (int i = 0; i < listHospital.size(); i++) {
			Hospital hospital=(Hospital)listHospital.get(i);
			list.add(new Object[]{hospital.getHospitalId(),hospital.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 更改医院是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Hospital hospital=getHospitalId(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		hospital.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				hospital.setIsdel(Const.Isdel_Yes);
			}
	    	DoUpdateHospital(hospital);
	    }
	}
	/**
	 * 更新医院保存
	 * @param hospitalId
	 * @param hospital
	 */
	public void addOrUpdate(String hospitalId, Hospital hospital) {
		//修改
		if (hospitalId!=null&&!hospitalId.equals("")){
			DoUpdateHospital(hospital);
		}else {
			hospital.setHospitalId(UuidUtil.get32UUID());
			hospital.setIsdel(Const.Isdel_No);
			DoAddHospital(hospital);
		}
	}
}
