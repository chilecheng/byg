package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.TransportType;
import com.hz.mapper.base.TransportTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 运输类型
 * @author rgy
 */
@Service
@Transactional
public class TransportTypeService {
	@Autowired
	private TransportTypeMapper transportTypeMapper;
	/**
	 * 添加运输类型
	 * @param transportType
	 */
	public void addTransportType(TransportType transportType){
		transportTypeMapper.saveTransportType(transportType);
	}
	/**
	 * 删除运输类型
	 * @param ID
	 * @param String id
	 */
	public void deleteTransportType(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			transportTypeMapper.deleteTransportType(ids[i]);
		}
		
	}
	/**
	 * 修改运输类型
	 * @param transportType
	 */
	public void updateTransportType(TransportType transportType){
		transportTypeMapper.updateTransportType(transportType);;
	}
	/**
	 * 查找运输类型id
	 * @param ID
	 * @param String id
	 */
	public TransportType getTransportTypeById(String id){
		return transportTypeMapper.getTransportTypeById(id);
	}
	/**
	 * 查找所有的运输类型
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<TransportType> getTransportTypeList(Map<String,Object> paramMap){
		return transportTypeMapper.getTransportTypeList(paramMap);
	}
	/**
	 * 运输类型列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<TransportType> getTransportPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<TransportType> list=getTransportTypeList(paramMap);
		PageInfo<TransportType> page=new PageInfo<TransportType>(list);
		return page;
	}
	/**
	 * 获取运输类型ption
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getTransportTypeOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<TransportType> listTransportType=transportTypeMapper.getTransportTypeList(null);
		for (int i = 0; i < listTransportType.size(); i++) {
			TransportType transportType=(TransportType)listTransportType.get(i);
			list.add(new Object[]{transportType.getTransportId(),transportType.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取运输类型readOnly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getTransportTypes(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<TransportType> listTransportType=transportTypeMapper.getTransportTypeList(null);
		for (int i = 0; i < listTransportType.size(); i++) {
			TransportType transportType=(TransportType)listTransportType.get(i);
			list.add(new Object[]{transportType.getTransportId(),transportType.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取运输类型readOnly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getTransportType(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<TransportType> listTransportType=transportTypeMapper.getTransportTypeList(null);
		for (int i = 0; i < listTransportType.size(); i++) {
			TransportType transportType=(TransportType)listTransportType.get(i);
			list.add(new Object[]{transportType.getTransportId(),transportType.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 修改运输类型是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateTransportIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			TransportType transportType=getTransportTypeById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		transportType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				transportType.setIsdel(Const.Isdel_Yes);
			}
	    	updateTransportType(transportType);;
	    }
	}
	/**
	 * 更新交通类型
	 * @param transportId
	 * @param transportType
	 */
	public void addUpdate(String transportId, TransportType transportType) {
		if(transportId!=null&&!transportId.equals("")){
			updateTransportType(transportType);
		}else{
			transportType.setTransportId(UuidUtil.get32UUID());
			transportType.setIsdel(Const.Isdel_No);
			addTransportType(transportType);
		}
	}
}
