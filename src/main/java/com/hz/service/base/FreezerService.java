package com.hz.service.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Freezer;
import com.hz.mapper.base.FreezerMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 冷藏柜信息
 * @author tsp
 *
 */
@Service
@Transactional
public class FreezerService {
	@Autowired
	private FreezerMapper freezerMapper;
	
	/**
	 * 添加冷藏柜信息
	 * @param freezer
	 */
	public void addFreezer(Freezer freezer){
		freezerMapper.saveFreezer(freezer);
	}
	/**
	 * 删除冷藏柜信息
	 * @param id
	 */
	public void deleteFreezer(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
		freezerMapper.deleteFreezer(ids[i]);
	    }
	}
	/**
	 * 根据条件查询冰柜使用情况 ，返回数量
	 * @param map
	 * @return
	 */
	public int getFreezerNumber(Map<String,Object> map){
		return freezerMapper.getFreezerNumber(map);
	}
	/**
	 * 更新冷藏柜信息
	 * @param freezer
	 */
	public void updateFreezer(Freezer freezer){
		freezerMapper.updateFreezer(freezer);
	}
	/**
	 * 获取冷藏柜信息对象
	 * @param id
	 * @return
	 */
	public Freezer getFreezerById(String id){
		Freezer freezer=freezerMapper.getFreezerById(id);
		return freezer;
	}
	/**
	 * 根据条件获取冷藏柜信息
	 * @param String name
	 * @param byte isdel
	 */
	public List<Freezer> getFreezerList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return freezerMapper.getFreezerList(paramMap);
	}
	/**
	 * 根据条件获取冷藏柜信息PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Freezer> getFreezerPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Freezer> list=freezerMapper.getFreezerList(paramMap);
		PageInfo<Freezer> page = new PageInfo<Freezer>(list);
		return page; 
	}
	
	
	/**
	 * 冷藏柜option
	 * @param id
	 * @param isAll(true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getFreezerOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> maps=new HashMap<String,Object>();	
		maps.put("isdel", Const.Isdel_No);
		PageHelper.orderBy("index_flag");
		List<Freezer> listfreezer=freezerMapper.getFreezerList(maps);
		for (int i = 0; i < listfreezer.size(); i++) {
			Freezer freezer=(Freezer) listfreezer.get(i);
			list.add(new Object[]{freezer.getId(),freezer.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
		
	}
	
	/**
	 * 冷藏柜option  此处选取未被占用的 冰柜 （冰柜调度）
	 * @param id
	 * @param isAll(true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getBlankFreezerOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");
		List<Freezer> listfreezer=freezerMapper.getBlankFreezxer(null);
		for (int i = 0; i < listfreezer.size(); i++) {
			Freezer freezer=(Freezer) listfreezer.get(i);
			list.add(new Object[]{freezer.getId(),freezer.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
		
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Freezer freezer=getFreezerById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		freezer.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				freezer.setIsdel(Const.Isdel_Yes);
			}
	    	updateFreezer(freezer);
	    }
	}
	/**
	 * 修改火化委托单
	 */
	public void clearFreezer(String freezerId, byte isflagNo) {
		freezerMapper.clearFreezer(freezerId, isflagNo);
	}
	

}
