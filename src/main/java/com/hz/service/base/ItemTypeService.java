package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.ItemType;
import com.hz.mapper.base.ItemTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 收费项目类别
 * @author tsp
 *
 */
@Service
@Transactional
public class ItemTypeService {
	@Autowired
	private ItemTypeMapper itemTypeMapper;
	
	/**
	 * 添加收费项目类别
	 * @param itemType
	 */
	public void addItemType(ItemType itemType){
		itemTypeMapper.saveItemType(itemType);
	}
	/**
	 * 删除收费项目类别
	 * @param id
	 */
	public void deleteItemType(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
		itemTypeMapper.deleteItemType(ids[i]);
	    }
	}
	/**
	 * 更新收费项目类别
	 * @param itemType
	 */
	public void updateItemType(ItemType itemType){
		itemTypeMapper.updateItemType(itemType);
	}
	/**
	 * 获取收费项目类别对象
	 * @param id
	 * @return
	 */
	public ItemType getItemTypeById(String id){
		ItemType itemType=itemTypeMapper.getItemTypeById(id);
		return itemType;
	}
	/**
	 * 根据条件获取收费项目类别
	 * @param String name
	 * @param byte isdel
	 */
	public List<ItemType> getItemTypeList(Map<String, Object> paramMap){
		PageHelper.orderBy("index_flag");
		List<ItemType> list= itemTypeMapper.getItemTypeList(paramMap);
		return list;
	}
	/**
	 * 根据条件获取收费项目类别PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<ItemType> getItemTypePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<ItemType> list=itemTypeMapper.getItemTypeList(paramMap);
		PageInfo<ItemType> page = new PageInfo<ItemType>(list);
		return page; 
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			ItemType itemType=getItemTypeById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		itemType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				itemType.setIsdel(Const.Isdel_Yes);
			}
	    	updateItemType(itemType);
	    }
	}
	/**
	 * 根据服务项目名字获取ID
	 * @param par
	 * @return
	 */
	public String getIdByTypeName(String par) {
		return itemTypeMapper.getIdByTypeName(par);
	}
	/**
	 * 获取收费项目类别Option
	 * @param paramMap 条件
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemTypeOption(Map<String, Object> paramMap,String id,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<ItemType> itemTypeList=itemTypeMapper.getItemTypeList(paramMap);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			list.add(new Object[]{itemType.getId(),itemType.getName()});
		}
		String option = DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取收费项目类别readOnly
	 * @param paramMap 条件
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemTypes(Map<String, Object> paramMap,String id,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<ItemType> itemTypeList=itemTypeMapper.getItemTypeList(paramMap);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			list.add(new Object[]{itemType.getId(),itemType.getName()});
		}
		String option = DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取收费项目类别readOnly
	 * @param paramMap 条件
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getItemType(Map<String, Object> paramMap,String id,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		List<ItemType> itemTypeList=itemTypeMapper.getItemTypeList(paramMap);
		for (int i = 0; i < itemTypeList.size(); i++) {
			ItemType itemType=(ItemType)itemTypeList.get(i);
			list.add(new Object[]{itemType.getId(),itemType.getName()});
		}
		String option = DataTools.getNameByList(list, id, isAll);
		return option;
	}
	
	public List<ItemType> getMoreItemTypeList(Map<String, Object> paramMap)
	{
		return itemTypeMapper.getMoreItemTypeList(paramMap);
	}
}
