package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 接尸单位
 * @author rgy
 *
 */
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.CorpseUnit;
import com.hz.mapper.base.CorpseUnitMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

@Service
@Transactional
public class CorpseUnitService {
	@Autowired
	private CorpseUnitMapper corpseUnitMapper;
	/**
	 * 添加接尸单位
	 * @param corpseUnit
	 */
	public void addCorpseUnit(CorpseUnit corpseUnit){
		corpseUnitMapper.saveCorpseUnit(corpseUnit);;
	}
	/**
	 * 删除接尸单位
	 * @param id
	 * @param String id
	 */
	public void deleteCorpseUnit(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			corpseUnitMapper.deleteCorpseUnit(ids[i]);
		}
		
	}
	/**
	 * 修改接尸单位
	 * @param corpseUnit
	 */
	public void updateCorpseUnit(CorpseUnit corpseUnit){
		corpseUnitMapper.updateCorpseUnit(corpseUnit);;
	}
	/**
	 * 查询接尸单位id
	 * @param id
	 * @param String id
	 */
	public CorpseUnit getCorpseUnitById(String id){
		return corpseUnitMapper.getCorpseUnitById(id);
	}
	/**
	 * 查询接尸单位id
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<CorpseUnit> getCorpseUnitList(Map<String, Object> paramMap){
		return corpseUnitMapper.getCorpseUnitList(paramMap);
	}
	/**
	 * 接尸列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<CorpseUnit> getCorpseUnitPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<CorpseUnit> list=getCorpseUnitList(paramMap);
		PageInfo<CorpseUnit> page=new PageInfo<CorpseUnit>(list);
		return page;
		
	}
	/**
	 * 获取接尸单位option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCorpseUnitOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		list.add(new Object[]{"",""});
		PageHelper.orderBy("index_flag");
		List<CorpseUnit> listCorpseUnit=corpseUnitMapper.getCorpseUnitList(null);
		for (int i = 0; i < listCorpseUnit.size(); i++) {
			CorpseUnit corpseUnit=(CorpseUnit)listCorpseUnit.get(i);
			list.add(new Object[]{corpseUnit.getId(),corpseUnit.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取接尸单位readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCorpseUnit(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		list.add(new Object[]{"",""});
		List<CorpseUnit> listCorpseUnit=corpseUnitMapper.getCorpseUnitList(null);
		for (int i = 0; i < listCorpseUnit.size(); i++) {
			CorpseUnit corpseUnit=(CorpseUnit)listCorpseUnit.get(i);
			list.add(new Object[]{corpseUnit.getId(),corpseUnit.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 修改接尸单位的是否启动标志
	 * @param id
	 * @param isdel
	 */
	public void updateConrpseIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			CorpseUnit corpseUnit=getCorpseUnitById(ids[i]);
			if(isdel==Const.Isdel_No){
				corpseUnit.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				corpseUnit.setIsdel(Const.Isdel_Yes);
			}
			updateCorpseUnit(corpseUnit);
		}
	}
	/**
	 * 更新接尸单位保存
	 * @param id
	 * @param corpseUnit
	 */
	public void addOrUpdate(String id, CorpseUnit corpseUnit) {
		if(id!=null&&!id.equals("")){
			updateCorpseUnit(corpseUnit);
		}else{
			corpseUnit.setId(UuidUtil.get32UUID());
			corpseUnit.setIsdel(Const.Isdel_No);
			addCorpseUnit(corpseUnit);
		}
	}
}
