package com.hz.service.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Mourning;
import com.hz.mapper.base.MourningMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 灵堂信息
 * @author rgy
 */
@Service
@Transactional
public class MourningService {
	@Autowired
	private MourningMapper mourningMapper;
	/**
	 * 添加灵堂信息
	 * @param mourning
	 */
	public void addMourning(Mourning mourning){
		mourningMapper.saveMourning(mourning);
	}
	/**
	 * 删除灵堂信息
	 * @param id
	 * @param String id
	 */
	public void deleteMourning(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			mourningMapper.deleteMourning(ids[i]);
		}
		
	}
	/**
	 * 修改灵堂信息
	 * @param mourning
	 */
	public void updateMourning(Mourning mourning){
		mourningMapper.updateMourning(mourning);;
	}
	/**
	 * 查找灵堂信息id
	 * @param id
	 * @param String id
	 */
	public Mourning getMourningById(String id){
		return mourningMapper.getMourningById(id);
	}
	public Mourning getMourningByName(String name){
		return mourningMapper.getMourningByName(name);
	}
	/**
	 * 查找所有的灵堂信息
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<Mourning> getMourningList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		return mourningMapper.getMourningList(paramMap);
	}
	/**
	 * 获取灵堂列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Mourning> getMourningPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		List<Mourning> list=getMourningList(paramMap,order);
		PageInfo<Mourning> page=new PageInfo<Mourning>(list);
		return page;
	}
	/**
	 * 获取灵堂option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getMourningOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isdel", 1);
		List<Mourning> listMourning=mourningMapper.getMourningList(map);
		for (int i = 0; i < listMourning.size(); i++) {
			Mourning mourning=(Mourning) listMourning.get(i);
			list.add(new Object[]{mourning.getId(),mourning.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 修改灵堂信息启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateMourningIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Mourning mourning=getMourningById(ids[i]);
			if(isdel==Const.Isdel_No){
				mourning.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				mourning.setIsdel(Const.Isdel_Yes);
			}
			updateMourning(mourning);
		}
	}
	/**
	 * 更新灵堂保存
	 * @param mourningId
	 * @param mourning
	 */
	public void addOrUpdate(String mourningId, Mourning mourning) {
		if(mourningId!=null&&!mourningId.equals("")){
			updateMourning(mourning);
		}else{
			mourning.setId(UuidUtil.get32UUID());
			mourning.setIsdel(Const.Isdel_No);
			mourning.setFlag(Const.IsFlag_No);
			addMourning(mourning);
		}
	}

}
