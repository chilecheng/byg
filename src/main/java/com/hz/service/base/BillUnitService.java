package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.BillUnit;
import com.hz.mapper.base.BillUnitMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;

/**
 * 挂账单位业务层
 * @author jgj
 */
@Service
@Transactional
public class BillUnitService {
	@Autowired
	private BillUnitMapper billUnitMapper;
	/**
	 * 添加挂账单位
	 * @param billUnit
	 */
	public void addBillUnit(BillUnit billUnit){
		billUnitMapper.saveBillUnit(billUnit);
	}
	/**
	 *  删除挂账单位
	 * @param id
	 * @param String id
	 */
	public void deleteBillUnit(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			billUnitMapper.deleteBillUnit(ids[i]);
		}		
	}
	/**
	 * 修改挂账单位
	 * @param billUnit
	 */
	public void updateBillUnit(BillUnit billUnit){
		billUnitMapper.updateBillUnit(billUnit);
	}
	/**
	 * 根据ID获得挂账单位
	 * @param id
	 * @return BillUnit
	 * @param String id
	 */
	public BillUnit getBillUnitId(String id){
		return billUnitMapper.getBillUnitById(id);		
	}
	/**
	 * 根据条件查询挂账单位
	 * @param paramMap
	 * @return List
	 * @param Map paramMap
	 * @return
	 */
	public List<BillUnit> getBillUnitList(Map<String,Object> paramMap){
		return billUnitMapper.getBillUnitList(paramMap);
	}
	/**
	 * 挂账单位列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<BillUnit> getBillUnitPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<BillUnit> list=getBillUnitList(paramMap);
		PageInfo<BillUnit> page=new PageInfo<BillUnit>(list);
		return page;
		
	}
	/**
	 * 获取挂账单位option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getBillUnitOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		PageHelper.orderBy("index_number");
		List<BillUnit> billUnitList=billUnitMapper.getBillUnitList(null);
		String option="";
		for (int i = 0; i < billUnitList.size(); i++) {
			BillUnit billUnit=(BillUnit)billUnitList.get(i);
			list.add(new Object[]{billUnit.getId(),billUnit.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getOptionByList(list, id, false);
		return option;
	}
	/**
	 * 获取挂账单位readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getBillUnit(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<BillUnit> billUnitList=billUnitMapper.getBillUnitList(null);
		String option="";
		for (int i = 0; i < billUnitList.size(); i++) {
			BillUnit billUnit=(BillUnit)billUnitList.get(i);
			list.add(new Object[]{billUnit.getId(),billUnit.getName()});
		}
		if (isAll) {
			option = "<option value=''></option>";
		}
		option+=DataTools.getNameByList(list, id, false);
		return option;
	}
	/**
	 * 更改挂账单位是否启动标志
	 * @param id
	 * @param isdel
	 * @return
	 */
	public void updateisdelBill(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			BillUnit billUnit=billUnitMapper.getBillUnitById(ids[i]);
			if(isdel==Const.Isdel_No){
				billUnit.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				billUnit.setIsdel(Const.Isdel_Yes);
			}
			updateBillUnit(billUnit);
		}
		
	}
}
