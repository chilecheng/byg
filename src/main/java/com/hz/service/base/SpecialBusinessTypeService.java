package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.SpecialBusinessType;
import com.hz.mapper.base.SpecialBusinessTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 特殊业务类型
 * @author jgj
 */
@Service
@Transactional
public class SpecialBusinessTypeService {
	@Autowired
	private SpecialBusinessTypeMapper specialBusinessTypeMapper;
	/**
	 * 添加特殊业务类型
	 * @param specialBusinessType
	 */
	public void saveSpecialBusinessType(SpecialBusinessType specialBusinessType){
		specialBusinessTypeMapper.saveSpecialBusinessType(specialBusinessType);
	}
	/**
	 * 删除特殊业务类型
	 * @param id
	 * @param String id
	 */
	public void deleteSpecialBusinessType(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			specialBusinessTypeMapper.deleteSpecialBusinessType(ids[i]);
		}
		
	}
	/**
	 * 修改特殊业务类型
	 * @param specialBusinessType
	 */
	public void updateSpecialBusinessType(SpecialBusinessType specialBusinessType){
		specialBusinessTypeMapper.updateSpecialBusinessType(specialBusinessType);
	}
	/**
	 * 查找特殊业务类型id
	 * @param id
	 * @param String id
	 */
	public SpecialBusinessType getSpecialBusinessTypeById(String id){
		
		return specialBusinessTypeMapper.getSpecialBusinessTypeById(id);
	}
	/**
	 * 查找所有的特殊业务类型
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<SpecialBusinessType> getSpecialBusinessTypeList(Map<String, Object> paramMap){
		List<SpecialBusinessType> list = specialBusinessTypeMapper.getSpecialBusinessTypeList(paramMap);
		return list;
	}
	/**
	 * 特殊业务类型列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<SpecialBusinessType> getSpecialBusinessTypePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<SpecialBusinessType> list=getSpecialBusinessTypeList(paramMap);
		PageInfo<SpecialBusinessType> page=new PageInfo<SpecialBusinessType>(list);
		return page;
		
	}
	/**
	 * 获取特殊业务类型option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getSpecialBusinessTypeOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");
		List<SpecialBusinessType> listSpecialBusinessType=specialBusinessTypeMapper.getSpecialBusinessTypeList(null);
		for (int i = 0; i < listSpecialBusinessType.size(); i++) {
			SpecialBusinessType specialBusinessType=(SpecialBusinessType) listSpecialBusinessType.get(i);
 			list.add(new Object[]{specialBusinessType.getId(),specialBusinessType.getName()});

		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取特殊业务类型readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getSpecialBusinessType(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<SpecialBusinessType> listNation=specialBusinessTypeMapper.getSpecialBusinessTypeList(null);
		for (int i = 0; i < listNation.size(); i++) {
			SpecialBusinessType nation=(SpecialBusinessType) listNation.get(i);
			list.add(new Object[]{nation.getId(),nation.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改特殊业务类型是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			SpecialBusinessType specialBusinessType=getSpecialBusinessTypeById(ids[i]);
			if(isdel==Const.Isdel_No){
				specialBusinessType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				specialBusinessType.setIsdel(Const.Isdel_Yes);
			}
			updateSpecialBusinessType(specialBusinessType);
		}
	}
	/**
	 * 更新特殊业务类型保存
	 * @param id
	 * @param specialBusinessType
	 */
	public void addOrUpdate(String id, SpecialBusinessType specialBusinessType) {
		if(id!=null&&!id.equals("")){
			updateSpecialBusinessType(specialBusinessType);
		}else{
			specialBusinessType.setId(UuidUtil.get32UUID());
			specialBusinessType.setIsdel(Const.Isdel_No);
			saveSpecialBusinessType(specialBusinessType);
		}
	}
}
