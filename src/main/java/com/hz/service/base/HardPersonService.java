package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.HardPerson;
import com.hz.mapper.base.HardPersonMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 重点救助对象类别
 * @author tsp
 *
 */
@Service
@Transactional
public class HardPersonService {
	@Autowired
	private HardPersonMapper hardPersonMapper;
	
	/**
	 * 添加重点救助对象类别
	 * @param hardPerson
	 */
	public void addHardPerson(HardPerson hardPerson){
		hardPersonMapper.saveHardPerson(hardPerson);
	}
	/**
	 * 删除重点救助对象类别
	 * @param id
	 */
	public void deleteHardPerson(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	hardPersonMapper.deleteHardPerson(ids[i]);
	    }
	}
	/**
	 * 更新重点救助对象类别
	 * @param hardPerson
	 */
	public void updateHardPerson(HardPerson hardPerson){
		hardPersonMapper.updateHardPerson(hardPerson);
	}
	/**
	 * 获取重点救助对象类别对象
	 * @param id
	 * @return
	 */
	public HardPerson getHardPersonById(String id){
		HardPerson hardPerson=hardPersonMapper.getHardPersonById(id);
		return hardPerson;
	}
	/**
	 * 根据条件获取重点救助对象类别
	 * @param paramMap
	 * @return
	 */
	public List<HardPerson> getHardPersonList(Map<String, Object> paramMap){
		List<HardPerson> list= hardPersonMapper.getHardPersonList(paramMap);
		return list;
	}
	/**
	 * 根据条件获取重点救助对象类别PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<HardPerson> getHardPersonPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<HardPerson> list=hardPersonMapper.getHardPersonList(paramMap);
		PageInfo<HardPerson> page = new PageInfo<HardPerson>(list);
		return page; 
	}
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			HardPerson hardPerson=getHardPersonById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		hardPerson.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				hardPerson.setIsdel(Const.Isdel_Yes);
			}
	    	updateHardPerson(hardPerson);
	    }
	}
	/**
	 * 获取重点救助对象类别ption
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getHardPersonOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<HardPerson> listHardPerson=hardPersonMapper.getHardPersonList(null);
		for (int i = 0; i < listHardPerson.size(); i++) {
			HardPerson hardPerson=(HardPerson)listHardPerson.get(i);
			list.add(new Object[]{hardPerson.getId(),hardPerson.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}

}
