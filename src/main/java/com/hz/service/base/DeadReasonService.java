package com.hz.service.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.DeadReason;
import com.hz.mapper.base.DeadReasonMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;
/**
 * 死亡原因
 * @author rgy
 *
 */
@Service
@Transactional
public class DeadReasonService {
	@Autowired
	private DeadReasonMapper deadReasonMapper;
	
	/**
	 * 添加死亡原因
	 * @param deadReason
	 */
	public void addDeadReason(DeadReason deadReason){
		deadReasonMapper.saveDeadReason(deadReason);;
	}
	/**
	 * 删除死亡原因
	 * @param id
	 * @param String id
	 */
	public void deleteDeadReason(String id){
		deadReasonMapper.deleteDeadReason(id);;
	}
	/**
	 * 修改死亡原因
	 * @param deadReason
	 */
	public void updateDeadReason(DeadReason deadReason){
		deadReasonMapper.updateDeadReason(deadReason);;
	}
	/**
	 * 查找死亡原因Id
	 * @param id
	 * @param String id
	 */
	public DeadReason getDeadReasonById(String id){
		
		return deadReasonMapper.getDeadReasonById(id);
	}
	/**
	 * 查找所有死亡原因
	 * @param paramMap
	 * @param String Map
	 * @return
	 */
	public List<DeadReason> getDeadReasonList(Map<String,Object> paramMap){
		return deadReasonMapper.getDeadReasonList(paramMap);
		
	}
	/**
	 * 死亡原因列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<DeadReason> getDeadReasonPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);//排序
		List<DeadReason> list=getDeadReasonList(paramMap);
		PageInfo<DeadReason> page=new PageInfo<DeadReason>(list);
		return page;
		
	}
	/**
	 * 获取死亡原因option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getDeadReasonOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		list.add(new Object[]{"",""});
		List<DeadReason> listDeadReason=deadReasonMapper.getDeadReasonList(null);
		for (int i = 0; i < listDeadReason.size(); i++) {
			DeadReason deadReason=listDeadReason.get(i);
			list.add(new Object[]{deadReason.getId(),deadReason.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取死亡原因readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getDeadReason(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<DeadReason> listDeadReason=deadReasonMapper.getDeadReasonList(null);
		for (int i = 0; i < listDeadReason.size(); i++) {
			DeadReason deadReason=listDeadReason.get(i);
			list.add(new Object[]{deadReason.getId(),deadReason.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 根据死亡类型获取死亡原因option
	 * @param typeId 死亡类型
	 * @return
	 */
	public String getDeadReasonChange(String typeId,String reasonId){
		List<Object[]> list=new ArrayList<Object[]>();
		list.add(new Object[]{"",""});
		Map<String,Object> maps=new HashMap<String, Object>();
		maps.put("typeId",typeId);
		List<DeadReason> listDeadReason=deadReasonMapper.getDeadReasonList(maps);
		for (int i = 0; i < listDeadReason.size(); i++) {
			DeadReason deadReason=listDeadReason.get(i);
			list.add(new Object[]{deadReason.getId(),deadReason.getName()});
		}
		String option=DataTools.getOptionByList(list,reasonId, false);
		return option;
	}
	
	
	/**
	 * 修改死亡原因启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateDeadIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			DeadReason deadReason=getDeadReasonById(ids[i]);
			if(isdel==Const.Isdel_No){
				deadReason.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				deadReason.setIsdel(Const.Isdel_Yes);
			}
			updateDeadReason(deadReason);
		}
	}
	/**
	 * 更新死亡原因保存
	 * @param id
	 * @param deadReason
	 */
	public void addOrUpdate(String id, DeadReason deadReason) {
		if(id!=null&&!id.equals("")){
			updateDeadReason(deadReason);;
		}else{
			deadReason.setId(UuidUtil.get32UUID());
			deadReason.setIsdel(Const.Isdel_No);
			addDeadReason(deadReason);;
		}
	}

}
