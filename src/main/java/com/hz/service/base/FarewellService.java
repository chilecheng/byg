package com.hz.service.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Farewell;
import com.hz.mapper.base.FarewellMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 告别厅
 * @author rgy
 */
@Service
@Transactional
public class FarewellService {
	@Autowired
	private FarewellMapper farewellMapper;
	/**
	 * 查询所有的告别厅
	 * @param paramMap
	 * @param Map
	 */
	public List<Farewell> getFarewellList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		return farewellMapper.getFarewellList(paramMap);
	}
	/**
	 * 添加告别厅
	 * @param farewell
	 */
	public void addFarewell(Farewell farewell){
		farewellMapper.saveFarewell(farewell);
	}
	/**
	 * 删除告别厅
	 * @param id
	 * @param String id
	 */
	public void deleteFarewell(String id){
		farewellMapper.deleteFarewell(id);;
	}
	/**
	 * 修改告别厅
	 * @param farewell
	 */
	public void updateFarewell(Farewell farewell){
		farewellMapper.updateFarewell(farewell);
	}
	/**
	 * 查询告别厅id
	 * @param id
	 * @param String id
	 * @return
	 */
	public Farewell getFarewellById(String id){
		return farewellMapper.getFarewellById(id);
	}
	/**
	 * 根据名字查找 
	 * @param name
	 * @return
	 */
	public Farewell getFarewellByName(String name){
		return farewellMapper.getFarewellByName(name);
	}
	
	/**
	 * 告别厅列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Farewell> getFarewellpageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<Farewell> list=getFarewellList(paramMap, order);
		PageInfo<Farewell> page=new PageInfo<Farewell>(list);
		return page;
	}
	/**
	 * 获取告别厅option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getFarewellOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isdel", 1);
		List<Farewell> listFarewell=farewellMapper.getFarewellList(map);
		for (int i = 0; i < listFarewell.size(); i++) {
			Farewell farewell=(Farewell)listFarewell.get(i);
			list.add(new Object[]{farewell.getId(),farewell.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 修改告别厅的启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateFarewellIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Farewell farewell=getFarewellById(ids[i]);
			if(isdel==Const.Isdel_No){
				farewell.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				farewell.setIsdel(Const.Isdel_Yes);
			}
			updateFarewell(farewell);
		}
	}
	/**
	 * 更新告别厅保存
	 * @param id
	 * @param farewell
	 */
	public void addOrUpdate(String id, Farewell farewell) {
		if(id!=null&&!id.equals("")){
			updateFarewell(farewell);
		}else{
			farewell.setId(UuidUtil.get32UUID());
			farewell.setIsdel(Const.Isdel_No);
			farewell.setFlag(Const.IsFlag_No);
			addFarewell(farewell);
		}
	}

}
