package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.MourningSpec;
import com.hz.mapper.base.MourningSpecMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 灵堂规格
 * @author rgy
 *
 */
@Service
@Transactional
public class MourningSpecService {
	@Autowired
	private MourningSpecMapper mourningSpecMapper;
	/**
	 * 添加灵堂规格
	 * @param mourningSpec
	 */
	public void addMourningSpec(MourningSpec mourningSpec){
		mourningSpecMapper.saveMourningSpec(mourningSpec);;
	}
	/**
	 * 删除灵堂规格
	 * @param id
	 * @param String id
	 */
	public void deleteMourningSpec(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			mourningSpecMapper.deleteMourningSpec(ids[i]);
		}
		
	}
	/**
	 * 修改灵堂规格
	 * @param mourningSpec
	 */
	public void updateMourningSpec(MourningSpec mourningSpec){
		mourningSpecMapper.updateMourningSpec(mourningSpec);;
	}
	/**
	 * 查找灵堂规格id
	 * @param id
	 * @param String id
	 */
	public MourningSpec getMourningSpecById(String id){
		
		return mourningSpecMapper.getMourningSpecById(id);
	}
	/**
	 * 添加灵堂规格
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<MourningSpec> getMourningSpecList(Map<String,Object> paramMap){
		return mourningSpecMapper.getMourningSpecList(paramMap);
	}
	/**
	 * 灵堂规格列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<MourningSpec> getMourningPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<MourningSpec> list=getMourningSpecList(paramMap);
		PageInfo<MourningSpec> page=new PageInfo<MourningSpec>(list);
		return page;
	}
	/**
	 * 获取灵堂规格option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getMourningSpecOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<MourningSpec> listMourning=mourningSpecMapper.getMourningSpecList(null);
		for (int i = 0; i < listMourning.size(); i++) {
			MourningSpec mourningSpec=(MourningSpec) listMourning.get(i);
			list.add(new Object[]{mourningSpec.getId(),mourningSpec.getName()});
		}
		String option=DataTools.getOptionByList(list, id,isAll);
		return option;
	}
	/**
	 * 修改灵堂规格是否标志
	 * @param id
	 * @param isdel
	 */
	public void updateMourningIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			MourningSpec mc=getMourningSpecById(ids[i]);
			if(isdel==Const.Isdel_No){
				mc.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				mc.setIsdel(Const.Isdel_Yes);
			}
			updateMourningSpec(mc);
		}
	}
	/**
	 * 更新灵堂规格保存
	 * @param mourningId
	 * @param mourningSpec
	 */
	public void addOrUpdate(String mourningId, MourningSpec mourningSpec) {
		if(mourningId!=null&&!mourningId.equals("")){
			updateMourningSpec(mourningSpec);;
		}else{
			mourningSpec.setId(UuidUtil.get32UUID());;
			mourningSpec.setIsdel(Const.Isdel_No);
			addMourningSpec(mourningSpec);
		}
	}
}
