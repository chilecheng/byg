package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.DeadType;
import com.hz.mapper.base.DeadTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;
/**
 * 死亡类型
 * @author rgy
 */
@Service
@Transactional
public class DeadTypeService {
	@Autowired
	private DeadTypeMapper dtm;
	/**
	 * 添加死亡类型
	 * @param deadType
	 */
	public void addDeadType(DeadType deadType){
		dtm.saveDeadType(deadType);
	}
	/**
	 * 删除死亡类型
	 * @param id
	 * @param String id
	 */
	public void deleteDeadType(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			dtm.deleteDeadType(ids[i]);
		}
		
	}
	/**
	 * 修改死亡类型
	 * @param deadType
	 */
	public void updateDeadType(DeadType deadType){
		dtm.updateDeadType(deadType);;
	}
	/**
	 * 查询死亡类型id
	 * @param id
	 * @param String id
	 */
	public DeadType getDeadTypeId(String id){
		return dtm.getDeadTypeById(id);
	}
	/**
	 * 查询所有的死亡类型
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<DeadType> getDeadTypeList(Map<String,Object> paramMap){
		List<DeadType> list=dtm.getDeadTypeList(paramMap);
		return list;
	}
	/**
	 * 死亡类型列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<DeadType> getDeadTypePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy("index_flag");
		List<DeadType> list=getDeadTypeList(paramMap);
		PageInfo<DeadType> page=new PageInfo<DeadType>(list);
		return page;
	}
	/**
	 * 获取死亡类型的option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getDeadTypeOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		list.add(new Object[]{"",""});
		List<DeadType> listDead=dtm.getDeadTypeList(null);
		for (int i = 0; i < listDead.size(); i++) {
			DeadType deadType=(DeadType)listDead.get(i);
			list.add(new Object[]{deadType.getId(),deadType.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取死亡类型readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getDeadType(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<DeadType> listDead=dtm.getDeadTypeList(null);
		for (int i = 0; i < listDead.size(); i++) {
			DeadType deadType=(DeadType)listDead.get(i);
			list.add(new Object[]{deadType.getId(),deadType.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 修改死亡类型的启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateDeadTypeIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			DeadType deadType=getDeadTypeId(ids[i]);
			if(isdel==Const.Isdel_No){
				deadType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				deadType.setIsdel(Const.Isdel_Yes);
			}
			updateDeadType(deadType);
		}
	}
	/**
	 * 更新死亡类型保存
	 * @param id
	 * @param deadType
	 */
	public void addOrUpdate(String id, DeadType deadType) {
		if(id!=null&&!id.equals("")){
			updateDeadType(deadType);
		}else{
			deadType.setId(UuidUtil.get32UUID());;
			deadType.setIsdel(Const.Isdel_No);;
			addDeadType(deadType);
		}
	}
}
