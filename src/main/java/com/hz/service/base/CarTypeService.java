package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.CarType;
import com.hz.mapper.base.CarTypeMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 车辆类型
 * @author tsp
 *
 */
@Service
public class CarTypeService {
	@Autowired
	private CarTypeMapper carTypeMapper;
	
	/**
	 * 添加车辆类型
	 * @param carType
	 */
	public void addCarType(CarType carType){
		carTypeMapper.saveCarType(carType);
	}
	/**
	 * 删除车辆类型
	 * @param id
	 */
	public void deleteCarType(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	carTypeMapper.deleteCarType(ids[i]);
	    }
	}
	/**
	 * 更新车辆类型
	 * @param carType
	 */
	public void updateCarType(CarType carType){
		carTypeMapper.updateCarType(carType);
	}
	/**
	 * 获取车辆类型对象
	 * @param id
	 * @return
	 */
	public CarType getCarTypeById(String id){
		CarType carType=carTypeMapper.getCarTypeById(id);
		return carType;
	}
	
	/**
	 * 根据添加获取车辆类型
	 * @param String name
	 * @param byte isdel
	 */
	public List<CarType> getCarTypeList(Map<String, Object>paramMap){
		List<CarType> list= carTypeMapper.getCarTypeList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取车辆类型PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<CarType> getCarTypePageInfo(Map<String, Object>paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<CarType> list=carTypeMapper.getCarTypeList(paramMap);
		PageInfo<CarType> page = new PageInfo<CarType>(list);
		return page; 
	}
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			CarType carType=getCarTypeById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		carType.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				carType.setIsdel(Const.Isdel_Yes);
			}
	    	updateCarType(carType);
	    }
	}
	/**
	 * 获取车辆类型Option
	 * @param typeId
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCarTypeOption(String typeId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");//排序
		List<CarType> carTypeList=getCarTypeList(null);
		for (int i = 0; i < carTypeList.size(); i++) {
			CarType carType=(CarType)carTypeList.get(i);
			list.add(new Object[]{carType.getId(),carType.getName()});
		}
		String option = DataTools.getOptionByList(list, typeId, isAll);
		return option;
	}
	/**
	 * 获取车辆类型readOnly
	 * @param typeId
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCarTypes(String typeId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");//排序
		List<CarType> carTypeList=getCarTypeList(null);
		for (int i = 0; i < carTypeList.size(); i++) {
			CarType carType=(CarType)carTypeList.get(i);
			list.add(new Object[]{carType.getId(),carType.getName()});
		}
		String option = DataTools.getNameByList(list, typeId, isAll);
		return option;
	}
	/**
	 * 获取车辆类型readOnly
	 * @param typeId
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCarType(String typeId,boolean isAll){
		List<Object[]> list = new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");//排序
		List<CarType> carTypeList=getCarTypeList(null);
		for (int i = 0; i < carTypeList.size(); i++) {
			CarType carType=(CarType)carTypeList.get(i);
			list.add(new Object[]{carType.getId(),carType.getName()});
		}
		String option = DataTools.getNameByList(list, typeId, isAll);
		return option;
	}
}
