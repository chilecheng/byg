package com.hz.service.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Furnace;
import com.hz.entity.ye.FurnaceRecord;
import com.hz.mapper.base.FurnaceMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 火化炉信息
 * @author tsp
 *
 */
@Service
@Transactional
public class FurnaceService {
	@Autowired
	private FurnaceMapper furnaceMapper;
	
	/**
	 * 添加火化炉信息
	 * @param furnace
	 */
	public void addFurnace(Furnace furnace){
		furnaceMapper.saveFurnace(furnace);
	}
	/**
	 * 删除火化炉信息
	 * @param id
	 */
	public void deleteFurnace(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
		furnaceMapper.deleteFurnace(ids[i]);
	    }
	}
	/**
	 * 更新火化炉信息
	 * @param furnace
	 */
	public void updateFurnace(Furnace furnace){
		furnaceMapper.updateFurnace(furnace);
	}
	/**
	 * 获取火化炉信息对象
	 * @param id
	 * @return
	 */
	public Furnace getFurnaceById(String id){
		Furnace furnace=furnaceMapper.getFurnaceById(id);
		return furnace;
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public Furnace getFurnaceByName(String name){
		return furnaceMapper.getFurnaceByName(name);
	}
	/**
	 * 根据条件获取火化炉信息
	 * @param String name
	 * @param byte isdel
	 */
	public List<Furnace> getFurnaceList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<Furnace> list= furnaceMapper.getFurnaceList(paramMap);
		return list;
	}
	/**
	 * 根据条件获取火化炉信息PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Furnace> getFurnacePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Furnace> list=furnaceMapper.getFurnaceList(paramMap);
		PageInfo<Furnace> page = new PageInfo<Furnace>(list);
		return page; 
	}
	
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Furnace furnace=getFurnaceById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    	
	    		furnace.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				
				furnace.setIsdel(Const.Isdel_Yes);
			}
	    	updateFurnace(furnace);
	    }
	}
	public List<FurnaceRecord> getFurnaceRecordList(Map<String, Object> maps) {
		List<FurnaceRecord> list= furnaceMapper.getFurnaceRecordList(maps);
		return list;
	}
	/**
	 * 获取特约炉类型ID
	 * @return
	 */
	public String getSpecialId() {
		String specialId = furnaceMapper.getSpecialId();
		return specialId;
	}
	
	
	public PageInfo<FurnaceRecord> getFurnaceRecordPageInfo(Map<String, Object> paramMap, int pageNum, int pageSize, String order) {
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<FurnaceRecord> list=getFurnaceRecordList(paramMap);
		PageInfo<FurnaceRecord> page = new PageInfo<FurnaceRecord>(list);
		return page; 
	}
	/**
	 * 得到所有火化炉下拉框 
	 * @param itemId
	 * @param b
	 * @return
	 */
	public String getFurnaceOption(String itemId, boolean isAll) {
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("isdel", 1);
		List<Furnace> listFurnace=furnaceMapper.getFurnaceList(map);
		for (int i = 0; i < listFurnace.size(); i++) {
			Furnace mourning=(Furnace) listFurnace.get(i);
			list.add(new Object[]{mourning.getId(),mourning.getName()});
		}
		String option=DataTools.getOptionByList(list, itemId, isAll);
		return option;
	}
	/**
	 * 判断是否存在
	 */
	public FurnaceRecord getFurnaceRecord(Map<String, Object> furnaceRecordMaps) {
		FurnaceRecord fur = furnaceMapper.getFurnaceRecord(furnaceRecordMaps);
		return fur;
	}
	
	

}
