package com.hz.service.base;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Certificate;
import com.hz.mapper.base.CertificateMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

@Service
@Transactional
public class CertificateService {
	@Autowired
	private CertificateMapper cm;
	
	/**
	 * 添加证件
	 * @param certificate证件类
	 */
	public void addCertificate(Certificate certificate){
		cm.saveCertificate(certificate);
	}
	/**
	 * 删除证件
	 * @param id 证件类id
	 * @param String id
	 */
	public void deleteCertificate(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			cm.deleteCertificate(ids[i]);;
		}
		
	}
	/**
	 * 修改证件
	 * @param certificate证件类
	 */
	public void updateCertificate(Certificate certificate){
		cm.updateCertificate(certificate);
	}
	/**
	 * 查询证件id
	 * @param id 证件类id
	 * @param String id
	 */
	public Certificate getCertificateId(String id){
		return cm.getCertificateById(id);
	}
	/**
	 * 查找所有证件
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public List<Certificate> getCertificateList(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);
		List<Certificate> list=cm.getCertificateList(paramMap);
		return list;
	}
	/**
	 * 根据条件找证件
	 * @param paramMap
	 * @return
	 */
	public List<Certificate> getCertificateListBy(Map<String, Object> paramMap){
		List<Certificate> list=cm.getCertificateList(paramMap);
		return list;
	}
	/**
	 * 查找所有证件
	 * @param paramMap 
	 * @return
	 */
	public List<Certificate> getCertificateList(){
		List<Certificate> list=cm.getCertificateList();
		return list;
	}
	/**
	 * 获取证件类型的pageinfo
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Certificate> getCertificatePageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Certificate> list=cm.getCertificateList(paramMap);
		PageInfo<Certificate> page=new PageInfo<Certificate>(list);
		return page;
	} 
	/**
	 * 获取证件option
	 * @param id
	 * @param isAll （true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCertificateOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Certificate> listCertificate=cm.getCertificateList(null);
		for (int i = 0; i < listCertificate.size(); i++) {
			Certificate certificate=(Certificate)listCertificate.get(i);
			list.add(new Object[]{certificate.getCertificateId(),certificate.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取证件只读
	 * @param id
	 * @param isAll （true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCertificate(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Certificate> listCertificate=cm.getCertificateList(null);
		for (int i = 0; i < listCertificate.size(); i++) {
			Certificate certificate=(Certificate)listCertificate.get(i);
			list.add(new Object[]{certificate.getCertificateId(),certificate.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 更改证件是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Certificate certificate=getCertificateId((ids[i]));
	    	if (isdel==Const.Isdel_No) {
	    		certificate.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				certificate.setIsdel(Const.Isdel_Yes);
			}
	    	updateCertificate(certificate);
	    }
	}
	/**
	 * 更新证件保存
	 * @param certificateId
	 * @param certificate
	 */
	public void addOrUpdate(String certificateId, Certificate certificate) {
		if(certificateId!=null&&!certificateId.equals("")){
			updateCertificate(certificate);
		}else{
			certificate.setCertificateId(UuidUtil.get32UUID());
			certificate.setIsdel(Const.Isdel_No);
			addCertificate(certificate);
		}
	}
	
}
