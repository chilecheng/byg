package com.hz.service.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.hz.entity.base.Jeneration;
import com.hz.mapper.base.JenerationMapper;
import com.hz.util.DataTools;
import com.hz.util.DateTools;
/**
 * 编号管理
 * @author tsp
 *
 */
@Service
@Transactional
public class JenerationService {
	@Autowired
	private JenerationMapper jenerationMapper;
	/**
	 * 添加编号管理
	 * @param jeneration
	 */
	public void addJeneration(Jeneration jeneration){
		jenerationMapper.saveJeneration(jeneration);
	}
	/**
	 * 删除编号管理
	 * @param id
	 */
	public void deleteJeneration(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
		jenerationMapper.deleteJeneration(ids[i]);
	    }
	}
	/**
	 * 更新编号管理
	 * @param jeneration
	 */
	public void updateJeneration(Jeneration jeneration){
		jenerationMapper.updateJeneration(jeneration);
	}
	/**
	 * 获取编号管理对象
	 * @param id
	 * @return
	 */
	public Jeneration getJenerationById(String id){
		Jeneration jeneration=jenerationMapper.getJenerationById(id);
		return jeneration;
	}
	/**
	 * 根据条件获取编号管理
	 * @param paramMap	条件
	 * @param order		排序
	 * @return
	 */
	public List<Jeneration> getJenerationList(Map<String, Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<Jeneration> list=jenerationMapper.getJenerationList(paramMap);
		return list; 
	}
	/**
	 * 根据类型获取编号
	 * @param type
	 * @return
	 */
	public String getJenerationCode(byte type){
		//新增设置编号
		Map<String, Object> jMaps=new HashMap<String, Object>();
		jMaps.put("time", DateTools.getThisDate());
		jMaps.put("type",type);
		List<Jeneration> jList=getJenerationList(jMaps, "number desc");
		Jeneration jeneration=new Jeneration();
		
		if (jList.size()>0) {
			jeneration=jList.get(0);
			int num=jeneration.getNumber()+1;
			jeneration.setNumber(num);
			updateJeneration(jeneration);
		}else{
			jeneration.setNumber(1);
			jeneration.setTime(DateTools.getThisDate());
			jeneration.setType(type);
			addJeneration(jeneration);
		}
		String sums=DataTools.addZeroToNumberString(jeneration.getNumber(), 3);
		String jCode=DateTools.getDayString()+sums;
		if (type==Jeneration.GHJC_TYPE) {
			jCode+="GHJC";
		}
		if (type==Jeneration.HQHL_TYPE) {
			jCode+="HQHL";
		}
		if (type==Jeneration.SZYP_TYPE) {
			jCode+="SZYP";
		}
		if (type==Jeneration.CLDD_TYPE) {
			jCode+="CLDD";
		}
		if (type==Jeneration.FZYS_TYPE) {
			jCode+="FZYS";
		}
		if(type==Jeneration.YYDJ_TYPE) {
			jCode+="YYDJ";
		}
		if(type==Jeneration.SFJL_TYPE){
			jCode+="SFJL";
		}
		if(type==Jeneration.YWDD_TYPE){
			jCode+="YWDD";
		}
		return jCode;
	}
	
}
