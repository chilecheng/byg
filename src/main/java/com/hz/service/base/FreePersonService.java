package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.FreePerson;
import com.hz.mapper.base.FreePersonMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 免费对象类别
 * @author tsp
 *
 */
@Service
@Transactional
public class FreePersonService {
	@Autowired
	private FreePersonMapper freePersonMapper;
	
	/**
	 * 添加免费对象类别
	 * @param freePerson
	 */
	public void addFreePerson(FreePerson freePerson){
		freePersonMapper.saveFreePerson(freePerson);
	}
	/**
	 * 删除免费对象类别
	 * @param id
	 */
	public void deleteFreePerson(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	freePersonMapper.deleteFreePerson(ids[i]);
	    }
	}
	/**
	 * 更新免费对象类别
	 * @param freePerson
	 */
	public void updateFreePerson(FreePerson freePerson){
		freePersonMapper.updateFreePerson(freePerson);
	}
	/**
	 * 获取免费对象类别对象
	 * @param id
	 * @return
	 */
	public FreePerson getFreePersonById(String id){
		FreePerson freePerson=freePersonMapper.getFreePersonById(id);
		return freePerson;
	}
	
	/**
	 * 根据条件获取免费对象类别
	 * @param paramMap
	 * @return
	 */
	public List<FreePerson> getFreePersonList(Map<String, Object> paramMap){
		List<FreePerson> list= freePersonMapper.getFreePersonList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取免费对象类别PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<FreePerson> getFreePersonPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<FreePerson> list=freePersonMapper.getFreePersonList(paramMap);
		PageInfo<FreePerson> page = new PageInfo<FreePerson>(list);
		return page; 
	}
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			FreePerson freePerson=getFreePersonById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		freePerson.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				freePerson.setIsdel(Const.Isdel_Yes);
			}
	    	updateFreePerson(freePerson);
	    }
	}
	/**
	 * 获取免费对象类别ption
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getFreePersonOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<FreePerson> listFreePerson=freePersonMapper.getFreePersonList(null);
		for (int i = 0; i < listFreePerson.size(); i++) {
			FreePerson freePerson=(FreePerson)listFreePerson.get(i);
			list.add(new Object[]{freePerson.getId(),freePerson.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}

}
