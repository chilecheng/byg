package com.hz.service.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Car;
import com.hz.mapper.base.CarMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
/**
 * 车辆信息
 * @author tsp
 *
 */
@Service
@Transactional
public class CarService {
	@Autowired
	private CarMapper carMapper;
	
	/**
	 * 添加车辆信息
	 * @param car
	 */
	public void addCar(Car car){
		carMapper.saveCar(car);
	}
	/**
	 * 删除车辆信息
	 * @param id
	 */
	public void deleteCar(String id){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
	    	carMapper.deleteCar(ids[i]);
	    }
	}
	/**
	 * 更新车辆信息
	 * @param car
	 */
	public void updateCar(Car car){
		carMapper.updateCar(car);
	}
	/**
	 * 获取车辆信息对象
	 * @param id
	 * @return
	 */
	public Car getCarById(String id){
		Car car=carMapper.getCarById(id);
		return car;
	}
	
	/**
	 * 根据添加获取车辆信息
	 * @param String name
	 * @param byte isdel
	 */
	public List<Car> getCarList(Map<String,Object> paramMap,String order){
		PageHelper.orderBy(order);
		List<Car> list= carMapper.getCarList(paramMap);
		return list;
	}
	
	/**
	 * 根据条件获取车辆信息PageInfo
	 * @param paramMap	条件
	 * @param pageNum	当前页数
	 * @param pageSize	每页个数
	 * @param order		排序
	 * @return
	 */
	public PageInfo<Car> getCarPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.orderBy(order);
		List<Car> list=carMapper.getCarList(paramMap);
		PageInfo<Car> page = new PageInfo<Car>(list);
		return page; 
	}
	/**
	 * 更新是否启用
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String[] ids = id.split(",");
	    for(int i=0;i<ids.length;i++){
			Car car=getCarById(ids[i]);
	    	if (isdel==Const.Isdel_No) {
	    		car.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				car.setIsdel(Const.Isdel_Yes);
			}
	    	updateCar(car);
	    }
	}
	
	public String getCarOption(String id) {
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isdel", 1);
		PageHelper.orderBy("index_flag");//排序
		List<Car> carList = carMapper.getCarList(map);
		for (Car car : carList) {
			list.add(new Object[]{car.getId(),car.getCarNumber()});
		}
		String option=DataTools.getOptionByList(list, id, false);
		return option;
	}
	

}
