package com.hz.service.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 接尸地址
 * @author jgj
 *
 */
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.CorpseAddress;
import com.hz.mapper.base.CorpseAddressMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

@Service
@Transactional
public class CorpseAddressService {
	@Autowired
	private CorpseAddressMapper corpseAddressMapper;
	/**
	 * 添加接尸地址
	 * @param corpseAddress
	 */
	public void addCorpseAddress(CorpseAddress corpseAddress){
		corpseAddressMapper.saveCorpseAddress(corpseAddress);;
	}
	/**
	 * 删除接尸地址
	 * @param id
	 * @param String id
	 */
	public void deleteCorpseAddress(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			corpseAddressMapper.deleteCorpseAddress(ids[i]);
		}
		
	}
	/**
	 * 修改接尸地址
	 * @param corpseAddress
	 */
	public void updateCorpseAddress(CorpseAddress corpseAddress){
		corpseAddressMapper.updateCorpseAddress(corpseAddress);;
	}
	/**
	 * 查询接尸地址id
	 * @param id
	 * @param String id
	 */
	public CorpseAddress getCorpseAddressById(String id){
		return corpseAddressMapper.getCorpseAddressById(id);
	}
	/**
	 * 查询接尸地址id
	 * @param paramMap
	 * @param Map paramMap
	 * @return
	 */
	public List<CorpseAddress> getCorpseAddressList(Map<String, Object> paramMap){
		return corpseAddressMapper.getCorpseAddressList(paramMap);
	}
	/**
	 * 接尸列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<CorpseAddress> getCorpseAddressPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		PageHelper.startPage(pageNum, pageSize);//设置分页
		PageHelper.orderBy(order);//排序
		List<CorpseAddress> list=getCorpseAddressList(paramMap);
		PageInfo<CorpseAddress> page=new PageInfo<CorpseAddress>(list);
		return page;
		
	}
	/**
	 * 获取接尸地址option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return
	 */
	public String getCorpseAddressOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("isdel", Const.Isdel_No);//启用
		PageHelper.orderBy("index_flag");
		List<CorpseAddress> listCorpseAddress=corpseAddressMapper.getCorpseAddressList(map);
		for (int i = 0; i < listCorpseAddress.size(); i++) {
			CorpseAddress corpseAddress=(CorpseAddress)listCorpseAddress.get(i);
			list.add(new Object[]{corpseAddress.getId(),corpseAddress.getName()});
		}
		String option=DataTools.getOption(list, id, isAll);
		return option;
	}
	/**
	 * 修改接尸地址的是否启动标志
	 * @param id
	 * @param isdel
	 */
	public void updateConrpseIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			CorpseAddress corpseAddress=getCorpseAddressById(ids[i]);
			if(isdel==Const.Isdel_No){
				corpseAddress.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				corpseAddress.setIsdel(Const.Isdel_Yes);
			}
			updateCorpseAddress(corpseAddress);
		}
	}
	/**
	 * 更新接尸地址保存
	 * @param id
	 * @param corpseAddress
	 */
	public void addOrUpdate(String id, CorpseAddress corpseAddress) {
		if(id!=null&&!id.equals("")){
			updateCorpseAddress(corpseAddress);
		}else{
			corpseAddress.setId(UuidUtil.get32UUID());
			corpseAddress.setIsdel(Const.Isdel_No);
			addCorpseAddress(corpseAddress);
		}
	}
}
