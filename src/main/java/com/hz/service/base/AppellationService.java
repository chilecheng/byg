package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.Appellation;
import com.hz.mapper.base.AppellationMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 称谓关系
 * @author rgy
 */
@Service
@Transactional
public class AppellationService {
	@Autowired
	private AppellationMapper appellationMapper;
	/**
	 * 添加称谓关系
	 * @param appellation
	 */
	public void addAppellation(Appellation appellation){
		appellationMapper.saveAppellation(appellation);
	}
	/**
	 * 删除称谓关系
	 * @param id
	 * @param String id
	 */
	public void deleteAppellation(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			appellationMapper.deleteAppellation(ids[i]);
		}
		
	}
	/**
	 * 修改称谓关系
	 * @param appellation
	 */
	public void updateAppellation(Appellation appellation){
		appellationMapper.updateAppellation(appellation);;
	}
	/**
	 * 查找称谓关系id
	 * @param id
	 * @param String id
	 */
	public Appellation getAppellationById(String id){
		
		return appellationMapper.getAppellationById(id);
	}
	/**
	 * 查找所有的称谓关系
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Appellation> getAppellationList(Map<String,Object> paramMap){
		List<Appellation> list = appellationMapper.getAppellationList(paramMap);
		return list;
	}
	/**
	 * 称谓关系列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Appellation> getAppellationPageInfo(Map<String,Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Appellation> list=getAppellationList(null);
		PageInfo<Appellation> page=new PageInfo<Appellation>(list);
		return page;
		
	}
	/**
	 * 获取称谓关系option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getAppellationOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
//		list.add(new Object[]{"",""});
		PageHelper.orderBy("index_flag");
		List<Appellation> listAppellation=appellationMapper.getAppellationList(null);
		for (int i = 0; i < listAppellation.size(); i++) {
			Appellation appellation=listAppellation.get(i);
			list.add(new Object[]{appellation.getAppellationId(),appellation.getName()});
		}
		String option=DataTools.getOptionByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取称谓关系readOnly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getAppellations(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Appellation> listAppellation=appellationMapper.getAppellationList(null);
		for (int i = 0; i < listAppellation.size(); i++) {
			Appellation appellation=listAppellation.get(i);
			list.add(new Object[]{appellation.getAppellationId(),appellation.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	/**
	 * 获取称谓关系readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getAppellation(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Appellation> listAppellation=appellationMapper.getAppellationList(null);
		for (int i = 0; i < listAppellation.size(); i++) {
			Appellation appellation=listAppellation.get(i);
			list.add(new Object[]{appellation.getAppellationId(),appellation.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改称谓关系是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Appellation appellation=getAppellationById(ids[i]);
			if(isdel==Const.Isdel_No){
				appellation.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				appellation.setIsdel(Const.Isdel_Yes);
			}
			updateAppellation(appellation);
		}
	}
	/**
	 * 更新保存方法
	 * @param appellationId
	 * @param appellation 
	 */
	public void addOrUpdate(String appellationId, Appellation appellation) {
		if(appellationId!=null&&!appellationId.equals("")){
			updateAppellation(appellation);
		}else{
			appellation.setAppellationId(UuidUtil.get32UUID());
			appellation.setIsdel(Const.Isdel_No);
			addAppellation(appellation);
		}
	}
}
