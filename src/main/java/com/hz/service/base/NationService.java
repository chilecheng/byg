package com.hz.service.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hz.entity.base.CorpseAddress;
import com.hz.entity.base.Nation;
import com.hz.mapper.base.NationMapper;
import com.hz.util.Const;
import com.hz.util.DataTools;
import com.hz.util.UuidUtil;

/**
 * 民族信息
 * @author rgy
 */
@Service
@Transactional
public class NationService {
	@Autowired
	private NationMapper nationMapper;
	/**
	 * 添加民族信息
	 * @param nation
	 */
	public void addNation(Nation nation){
		nationMapper.saveNation(nation);
	}
	/**
	 * 删除民族信息
	 * @param id
	 * @param String id
	 */
	public void deleteNation(String id){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			nationMapper.deleteNation(ids[i]);
		}
		
	}
	/**
	 * 修改民族信息
	 * @param nation
	 */
	public void updateNation(Nation nation){
		nationMapper.updateNation(nation);
	}
	/**
	 * 查找民族信息id
	 * @param id
	 * @param String id
	 */
	public Nation getNationById(String id){
		
		return nationMapper.getNationById(id);
	}
	/**
	 * 查找所有的民族信息
	 * @param paramMap
	 * @param Map paramMap
	 */
	public List<Nation> getNationList(Map<String, Object> paramMap){
		List<Nation> list = nationMapper.getNationList(paramMap);
		return list;
	}
	/**
	 * 民族信息列表
	 * @param paramMap
	 * @param pageNum
	 * @param pageSize
	 * @param order
	 * @return
	 */
	public PageInfo<Nation> getNationPageInfo(Map<String, Object> paramMap,int pageNum,int pageSize,String order){
		//设置分页
		PageHelper.startPage(pageNum,pageSize);
		PageHelper.orderBy(order);//排序
		List<Nation> list=getNationList(paramMap);
		PageInfo<Nation> page=new PageInfo<Nation>(list);
		return page;
		
	}
	/**
	 * 获取民族信息option
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getNationOption(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		PageHelper.orderBy("index_flag");
		List<Nation> listNation=nationMapper.getNationList(null);
		for (int i = 0; i < listNation.size(); i++) {
			Nation nation=(Nation) listNation.get(i);
			list.add(new Object[]{nation.getId(),nation.getName()});

		}
/*		String option=DataTools.getOptionByList(list, id, isAll);
*/				String option=DataTools.getOption(list, id, isAll);
		return option;
	}
	/**
	 * 获取民族信息readonly
	 * @param id
	 * @param isAll (true--"全部"　false--"没有全部")
	 * @return 
	 */
	public String getNation(String id,boolean isAll){
		List<Object[]> list=new ArrayList<Object[]>();
		List<Nation> listNation=nationMapper.getNationList(null);
		for (int i = 0; i < listNation.size(); i++) {
			Nation nation=(Nation) listNation.get(i);
			list.add(new Object[]{nation.getId(),nation.getName()});
		}
		String option=DataTools.getNameByList(list, id, isAll);
		return option;
	}
	
	/**
	 * 修改民族信息是否启用标志
	 * @param id
	 * @param isdel
	 */
	public void updateIsdel(String id,byte isdel){
		String ids[]=id.split(",");
		for (int i = 0; i < ids.length; i++) {
			Nation nation=getNationById(ids[i]);
			if(isdel==Const.Isdel_No){
				nation.setIsdel(Const.Isdel_No);
			}else if(isdel==Const.Isdel_Yes){
				nation.setIsdel(Const.Isdel_Yes);
			}
			updateNation(nation);
		}
	}
	/**
	 * 更新民族保存
	 * @param id
	 * @param nation
	 */
	public void addOrUpdate(String id, Nation nation) {
		if(id!=null&&!id.equals("")){
			updateNation(nation);
		}else{
			nation.setId(UuidUtil.get32UUID());
			nation.setIsdel(Const.Isdel_No);
			addNation(nation);
		}
	}
	/**
	 * 通过  nation  名 查询 nationid
	 * @param dNation
	 * @return
	 */
	public String getdnationId(String dNation){
		return  nationMapper.getNationId(dNation);
	}
	/**
	 * 通过nationid   获取 nation   name
	 * @param dNationId
	 * @return
	 */
	public String getNationNameById(String dNationId){
		return  nationMapper.getNationNameById(dNationId);
	}
	
	
	
	
	
	
	
}
