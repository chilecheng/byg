package com.hz.service.outInterface;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hz.entity.outInterface.FareInterface;
import com.hz.entity.outInterface.MourningInterface;
import com.hz.mapper.outInterface.OutInterfaceMapper;

/**
 * 对外接口
 * @author jgj
 */
@Service
@Transactional
public class OutInterfaceService {
	
	@Autowired
	private OutInterfaceMapper outInterfaceMapper;
	
	/**
	 * 获取告别信息
	 * @param id
	 * @return
	 */
	public List<FareInterface> getFarewellList(String beginTime){
		return outInterfaceMapper.getFarewellList(beginTime);
	}
	/**
	 * 获取灵堂信息
	 * @return
	 */
	public List<MourningInterface> getMourningList(String beginTime){
		return outInterfaceMapper.getMourningList(beginTime);
	}
	
}
