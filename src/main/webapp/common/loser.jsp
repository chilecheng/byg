<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
	<style>
		.wk-btns .btn{
			padding:3px 25px;
			border-radius:8px;
		}
		.wk-btns{
			text-align:center;
			margin-top:0px;
			padding:10px 0px;
			border-top:1px solid #ccc;
		}
		.nomargin{
			margin:0px;
		}
	</style>
</head>
<body>	
	<div class="modal-dialog" role="document" style="margin-top: 103.5px;width:800px;">
		<div class="modal-content">
			<div class='row nomargin'>
				<div class='col-md-12'>
					<section class="content-header">
						<i class="title-line"></i>
						<div class="title">查询结果</div>
					</section>	
				</div>	
				<div class='col-md-12 Hscroll' style="overflow-y: scroll; max-height:270px;">
					<table class='table table-bordered table-hover'>
						<thead>
							<tr>
								<th><input type="checkbox" hidden group="id"></th>
								<th>死者姓名</th>
								<th>到馆时间</th>
								<th>身份证号</th>
								<th>家庭地址</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${pageSearch}" var="u">
						<tr>
						<td>
							<input data-nowDate='${nowDate}' 
							name='id' 
							data-name='${u[1]}' 
							data-age='${u[5]}' 
							data-sex='${u[6]}' 
							data-mourningPlace='${u[7]}' 
								data-beginDate='<fmt:formatDate value="${u[8]}" pattern="yyyy-MM-dd hh:mm:ss"/>' 
								data-endDate='<fmt:formatDate value="${u[9]}" pattern="yyyy-MM-dd hh:mm:ss"/>'
								data-farewellPlace='${u[10]}' data-farewellDate='<fmt:formatDate value="${u[11]}" pattern="yyyy-MM-dd hh:mm:ss"/>' 
								data-certificateCode='${u[12]}' data-cremationTime='${u[13]}' value='${u[0]}' class="checkBoxCtrl" type='radio'/>
						</td>
						<td>${u[1]}</td>
						<td>${u[2]}</td>
						<td>${u[3]}</td>
						<td>${u[4]}</td>	
						</tr>
						</c:forEach>
						
						</tbody>
					</table>
					<form action="${url}?method=dNameSearch&dName=${dName}" id="pageForm" onsubmit="return changeThis(this);">
							<input type="hidden" name="method" value="dNameList">
							<input type="hidden" name="searchName" value="${dName }">
<%-- 							<%@ include file="/common/pageHead2.jsp"%> --%>
<%-- 							<%@ include file="/common/pageFoodSearch.jsp"%> --%>
					</form>
				</div>
			</div>
			<div class='row nomargin'>
				<div class='col-md-12 wk-btns'>
					<button type="submit" data-sure='yes'  class="btn btn-info">确定</button>
					<a checkone="true" checkname="id" data-check='one' class='hide' target="dialog" rel="myModal" role="button"></a>
					<button type="submit" hidden data-miss='miss' data-dismiss="modal"></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
				</div>
			</div>
				
		</div>
	</div>
<script>
	(function(){
		$('[data-sure="yes"]').click(function(){
			var size=$('[name="id"]:checked').size();
			if(size){
				var input=$('[name="id"]:checked');
				$('#dName').val(input.attr('data-name'));
				$("#dNameId").val(input.val());
				$("#dAge").val(input.attr('data-age'));
				$("#mourningPlace").val(input.attr("data-mourningPlace"));
				$('#dSex').val(input.attr('data-sex'));
				$('#beginDate').val(input.attr('data-beginDate'));
				$('#endDate').val(input.attr('data-endDate'));
				$('#farewellPlace').val(input.attr('data-farewellPlace'));
				$('#farewellDate').val(input.attr('data-farewellDate'));
				$('#certificateCode').val(input.attr('data-certificateCode'));
				$('#cremationTime').val(input.attr('data-cremationTime'));
				$('#nowDate').val(input.attr('data-nowDate'));
				$('[data-miss="miss"]').click();
			}else{
				$('[data-check="one"]').click();
			}
		})
	})();
	function changeThis(form) {
		$("button").prop("disabled", "disabled");
		var $form = $(form);
		var href=$form.attr("action");
		$.ajax({
			type : 'POST',
			url : $form.attr("action"),
			data : $form.serializeArray(),
			dataType : "html",
			cache : false,
			success : function(json){
				$("#myModal").html(json);
				$("#"+rel).modal("show");
				initTab();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				toastr["error"](XMLHttpRequest.status);
				$("button").removeAttr("disabled");
			}
		});
		return false;
	};
	
	 
</script>	
</body>