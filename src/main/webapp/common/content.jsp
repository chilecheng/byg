<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<style>
* {
	padding: 0;
	margin: 0;
}

.index_ul {
	list-style: none;
}

a {
	text-decoration: none;
}

body {
	font-family: Arial, "微软雅黑";
	background-color: #f0f0f2;
}

.col-md-6 {
	font-family: Arial, "微软雅黑";
}

.col-md-4 h3, .col-md-8 h3, .col-md-12 h3 {
	font-family: Arial, "微软雅黑";
	font-size: 20px;
	color: #5a5a5a;
}

.box {
	margin-bottom: 0;
}

.box-body .boxone {
	width: 120px;
	height: 120px;
	border-radius: 8px;
	position: relative;
	opacity: 0.8;
	float: left;
	margin-right: 10px;
	margin-bottom: 10px;
}

.box-body .boxone:hover {
	opacity: 1;
}

.boxone .fa {
	color: #fff;
	font-size: 50px;
	position: absolute;
	top: 25px;
	left: 50%;
	transform: translate(-50%);
}

.box-header>.fa {
	color: #5a5a5a;
}

.boxone .fa-credit-card {
	position: absolute;
	top: 25px;
	left: 50%;
	transform: translate(-50%);
}

.boxone .fa-car {
	position: absolute;
	top: 25px;
	left: 50%;
	transform: translate(-50%);
}

.boxone p {
	color: #fff;
	font-size: 14px;
	text-align: center;
	padding-top: 90px;
}

.web1 {
	float: left;
	width: 120px;
}

.web1 a:hover {
	color: #fff;
	background-color: #0098e4;
}

.web {
	display: inline-block;
	width: 120px;
	height: 40px;
	border-radius: 8px;
	background-color: #29b6f6;
	color: #fff;
	font-size: 16px;
	text-align: center;
	line-height: 40px;
	margin-bottom: 10px;
}
.web:hover{
	color:#fff;
	background-color:#367fa9;
}

#chaxun1 {
	padding-bottom: 3px;
}

.chaxun {
	width: 100%;
	height: 34px;
	line-height: 34px;
	background-color: #29b6f6;
	border: 1px solid #0098e4;
	color: #fff;
	border-radius: 6px;
}

.chaxun:hover {
	background-color: #0098e4;
}

.form-control {
	border-radius: 6px;
}

.nav-tabs-custom li {
	position: relative;
}

#remind {
	font-size: 14px;
	padding: 0px 8px;
	border-radius: 4px;
	background-color: #ffa016;
	color: #fff;
	z-index: 100;
	text-align: center;
	line-height: 24px;
	box-shadow: 1px 1px 1px #7e7e7e;
}

.remind1 {
	position: absolute;
	top: -14px;
	right: 0px;
}

.table-responsive a {
	color: #28b5f4;
}

#tonggao {
	padding-bottom: 0;
}

.main:visited {
	color: #a7a7a7;
}

.news {
	display: block;
	margin: 8px 8px 8px 24px;
	position: relative;
}

.main {
	color: #1c1c1c;
}

.main:hover {
	color: #29b6f6;
}

.time {
	color: #1c1c1c;
	float: right;
}

.circle {
	width: 10px;
	height: 10px;
	background-color: #28b5f4;
	border-radius: 20px;
	position: absolute;
	top: 5px;
	left: -20px;
}

.news:hover {
	color: #29b6f6;
}

.more {
	height: 26px;
	border-top: 1px solid #dddddd;
	margin-top: 10px;
}

.more a {
	color: #444444;
	float: right;
	line-height: 26px;
	padding: 5px;
	margin-right: 10px;
}

.more a:hover {
	color: #29b6f6
}

a {
	text-decoration: none;
}

.piece {
	margin: 0 auto;
}

.piece ul {
	float: left;
	background-color: #29b6f6;
	padding: 0px;
}

.table-responsive {
	padding-left: 30px;
}

.piece ul li {
	height: 40px;
	line-height: 40px;
	font-size: 14px;
	font-family: "微软雅黑";
	position: relative;
}

.hint {
	width: 26px;
	height: 26px;
	border-radius: 26px;
	background-color: #ffa726;
	color: #fff;
	position: absolute;
	right: 7px;
	top: 7px;
	text-align: center;
	line-height: 24px;
	font-size: 14px;
	border: 1px solid #eb9b25;
}

.piece ul li a {
	color: #fff;
	display: block;
	height: 40px;
	border-top: 1px solid #0095e1;
	border-right: 1px solid #0095e1;
	background-color: #29b6f6;
	width: 100%;
	text-align: left;
	padding-left: 10px;
	line-height: 40px;
}

.piece ul li .active {
	background: #fff;
	border-right: 1px solid #fff;
	color: #29b6f6;
	border-top: none;
}

.body-box {
	border: 1px solid #cbcbcb;
}

.tab-content {
	height: 402px;
	border: 1px solid #cbcbcb;
	padding: 0 20px 20px;
	float: left;
	width: 100%;
	padding-left: 0;
}

.table-responsive {
	height: 392px;
	overflow: auto;
	border: none;
}

.tab-content h3 {
	line-height: 46px;
	font-size: 22px;
	font-family: arial;
	font-weight: normal;
	color: #43434f;
	border-bottom: 1px dashed #dadada;
	margin-bottom: 10px;
}

.img {
	float: left;
	border: 1px solid #dddddd;
	padding: 5px;
	margin-right: 20px;
	margin-bottom: 32px;
}

.tab-content p {
	line-height: 15px;
	font-size: 12px;
	font-family: arial;
}

/* select {
	color: red;
} */

.tab-content h4 a {
	color: #274285;
	text-decoration: underline;
	font-size: 12px;
	font-family: arial;
}

.table-responsive #weichuli {
	width: 70px;
	height: 24px;
	background-color: red;
	color: white;
}

</style>
<script type="text/javascript">
var clickWhich="${sessionScope.refresh}";//从session中取出上次点击选中的任务列表，在自动刷新的时候保持选中它的状态。

$(function(){
	if(!clickWhich) {
		$("a[target=taskMenu]").eq(0).click();
	}
});
function changeNumber(ele){
	$(ele).parent().siblings().eq(0).find('b').hide();
	var showNumber=$('.active[target="taskMenu"]').next().text()-1;
	
	if(showNumber<=0){
		$('.active[target="taskMenu"]').next().hide();
	}else{
		$('.active[target="taskMenu"]').next().html(showNumber);
	}
}
	/* $(document).ready(function(e) {
		$('#select').change(function() {
			var mySelect = document.getElementById('select');
			var selectedId = mySelect.selectedIndex;
			if (parseInt(selectedId) < 0) {
				return;
			}
			var color = mySelect.options[selectedId].style.color;
			mySelect.style.color = color;
		})
	}); */
timer = setTimeout(refreshHome,20000);
function refreshHome() {
	if($(".modal-backdrop.fade.in").size()!==0){
		$(".modal-backdrop.fade.in:first").remove();
		if($('.modal.fade.in').size()!==0)$('.modal.fade.in').removeClass('in').css('display','none');
		$('body').removeClass('modal-open');
		$('body').css('padding-right','0px');
		timer = setTimeout(refreshHome,20000);
		return false;
		};
	$.ajax({
		type: "POST",
		url: "login.do",
		data:{method : "content"},
		dataType:"html",
		success:function(json){
			clearTimeout(timer);//如果不加的话就会又开启一个新的定时器然后你上面的那个停止dsq定时器的函数是无法停止这个新的定时器的，当你鼠标不断移入移出都会开启一个新的定时器，多个定时器同时执行相同的动作，这样相当于你的定时器里面的时间成倍的减少，效果就是你的定时器越来越快的执行，你可以试一下看是什么效果，不要试多了，小心电脑死机。
			timer=null;
			$("#page-content").html(json);
			//alert(GetCookie("scroll"));
			if(GetCookie("scroll")!=null){
				//alert(GetCookie("scroll"));
				document.body.scrollTop=GetCookie("scroll")
				
			};//保持上次页面的位置
			initTab();//
			//alert("点击"+clickWhich);
			if(clickWhich) {
				//alert("点击"+clickWhich);
				$('a[href="'+clickWhich+'"]').click();
			}
		},
		error:function(XMLHttpRequest, textStatus, errorThrown) {  
			if(XMLHttpRequest.status==500){
				toastr["error"]("程序内部出现错误"); 
			}else if(XMLHttpRequest.status==404){
				toastr["error"]("页面不存在"); 
			}else{
				toastr["error"](XMLHttpRequest.status); 
			}
		}
	})
}
//下面的  用于刷新后回滚到上次的位置	
function  Trim(strValue) {     
	return strValue.replace(/^s*|s*$/g,"");     
}
        
function GetCookie(sName) {     
	var aCookie = document.cookie.split(";");     
	for(var i=0;i<aCookie.length;i++){     
	 	var aCrumb = aCookie[i].split("=");     
		if(sName==Trim(aCrumb[0])){     
			return unescape(aCrumb[1]);     
		}     
	}     
	return null;     
} 
//浏览器会默认保存上次滚动位置，所以不需要下面这段去手动设置cookie了      
/* function SetCookie(sName,sValue) {     
	document.cookie = sName + "=" + escape(sValue);     
}    */
</script>
</head>
<body>
	<section class="content">
		<div class="row">
			<div class="col-md-12 nomargin-T">
<!-- 				<div class="box box-default"> -->
<!-- 					<div class="box-header with-border"> -->
<!-- 						<i class="fa fa-road"></i> -->
<!-- 						<h3 class="box-title">快捷通道</h3> -->
<!-- 					</div> -->
					<!-- /.box-header -->
<!-- 					<div class="box-body"> -->
<!-- 						<a class="boxone" href="#" style="background-color: #eb3a2a"> -->
<!-- 							<div class="fa fa-search"></div> -->
<!-- 							<p>委托业务综合查询</p> -->
<!-- 						</a> <a class="boxone" href="#" style="background-color: #1ba7e8"> -->
<!-- 							<div class="fa fa-car"></div> -->
<!-- 							<p>礼仪出殡查询</p> -->
<!-- 						</a> <a class="boxone" href="#" style="background-color: #40d50c"> -->
<!-- 							<div class="fa fa-credit-card"></div> -->
<!-- 							<p>补卡业务</p> -->
<!-- 						</a> <a class="boxone" href="#" style="background-color: #525252"> -->
<!-- 							<div class="fa fa-plus"></div> -->
<!-- 							<p>添加</p> -->
<!-- 						</a> -->
<!-- 						<div class="col-md-12"> -->
							<a href="http://www.wzbyw.com.cn/RED/" target='_blank' class="web">温州殡仪网>></a> <a href="http://www.wzmz.gov.cn/" target='_blank' class="web">温州民政网>></a>
							
<!-- 						</div> -->
<!-- 					</div> -->

					<!-- /.box-body -->
<!-- 				</div> -->
				<!-- /.box -->
			</div>

			<%@ include file='../WEB-INF/jsp/taskList/taskList.jsp'%>


			<div class="col-md-4">
				<div class="box box-default">
					<div class="box-header with-border">
						<i class="fa fa-list-alt"></i>
						<h3 class="box-title">业务全视图查询通道</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body" id="chaxun1">
						<form action="commissionOrderCheck.do?method=show" id=""
							onsubmit="return homeSearchNotNull(this);">
							<div class="form-group">
								<input type="text" class="form-control" id="findCode"
									name="findCode" placeholder="输入卡号">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="findName"
									name="findName" placeholder="输入姓名">
							</div>
							<div class="form-group">
								<input type="submit" class="chaxun" target="homeTab" value="查询"
									placeholder="查询">
								<!--                   <button type='button' class="chaxun" target="homeTab">查询 </button> -->
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-8" id="search">
				<div class="box box-default">
					<div class="box-header with-border">
						<i class="fa fa-feed"></i>
						<h3 class="box-title">公开通告</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body" id="tonggao" style="padding-top: 0">
						<c:choose>
							<c:when test="${fn:length(nList)==0 }">
								<tr>
									<td colspan="6"><font color="Red">还没有数据</font></td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${nList }" var="u">
									<a href="notification.do?method=listedit&position=home&id=${u.id}"
										target="homeTab" class="news">
										<div class="circle"></div> <span class="main">${u.title }</span>
										<span class="time">${u.createTime }</span>
									</a>
								</c:forEach>
							</c:otherwise>
						</c:choose>
						<div class="more">
							<a href="notification.do?method=list" target="homeTab">更多>></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>