<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<div class="row">
	<div class="col-sm-5">
		<label>共${page4.total }条</label> 
		<select name="numPerPage" style="padding: 4px 6px" onchange="changePageSize4(this.value)">			
			<c:choose>
				<c:when test="${page4.pageSize== 10}">
					<option value="10" selected="selected">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="50">50</option>
				</c:when>
				<c:when test="${page4.pageSize== 20}">
					<option value="10">10</option>
					<option value="20" selected="selected">20</option>
					<option value="30">30</option>
					<option value="50">50</option>
				</c:when>
				<c:when test="${page4.pageSize== 30}">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30" selected="selected">30</option>
					<option value="50">50</option>
				</c:when>
				<c:when test="${page4.pageSize== 50}">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="50" selected="selected">50</option>
				</c:when>
			</c:choose>
		</select>
	</div>
	<div class="col-sm-7">
		<div class="dataTables_paginate">
			<ul class="pagination">
			
				<li class="paginate_button previous <c:if test="${page4.pageNum == 1 }">disabled</c:if>" id="example2_previous">
					<a href="JavaScript:changePageNum4('1');" >首页</a>
				</li>
<%-- 					<c:if test="${page.pageNum > 5}"> --%>
<!-- 						<li class="paginate_button"><a href="#">...</a></li> -->
<%-- 					</c:if> --%>
				<c:forEach begin="1" end="${page4.pages }" var="p">
					<c:if test="${p<(page4.pageNum+3) && p>(page4.pageNum-3)}">
						<li class="paginate_button <c:if test="${p == page4.pageNum}">active</c:if>">
							<a href="JavaScript:changePageNum4('${p}');">${p}</a>
						</li>
					</c:if>
				</c:forEach>
					<c:if test="${page4.pages - page4.pageNum > 3}">
						<li class="paginate_button"><a href="JavaScript:changePageNum4('${page4.pageNum+3}');">...</a></li>
					</c:if>
				<li class="paginate_button next <c:if test="${page4.pageNum == page4.pages }">disabled</c:if>">
					<a href="JavaScript:changePageNum4('${page4.pages}');" >末页</a>
				</li>
			</ul>
		</div>
	</div>
</div>