<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
	
</head>
<body>
	<!-- -查看丧葬用品- -->
	
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
				<div class="modal-content">
					<div class='row'>
						<div class='col-md-6 border-R height-500'>
							<section class="content-header content-header-S">
								<i class="title-line"></i>
								<div class="title">选择</div>
							</section>
							<div class='panel-group panel-group-S' role="tablist" id='accordion3'>
								<div class="box box-warning">
						            <div class="box-header with-border">
						              <h3 class="box-title">列表</h3>
						              <div class="box-tools">
						                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						                </button>
						              </div>
						            </div>
						            <div class="box-body no-padding">
						              <ul class="nav nav-pills nav-stacked">
<!-- 						                <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li> -->
 										<c:forEach items="${list}" var="u">
										<li class='checkbox checkbox-S'><label><input type='checkbox' name='namager' value='${u.userId }'/>${u.name}</label></li>
										</c:forEach>
						              </ul>
						            </div>
						            <!-- /.box-body -->
						          </div>
<!-- 								<div class='panel box box-warning'> -->
<!-- 									<div class='panel-heading' role='tab'> -->
<!-- 										<p class='panel-title'> -->
<%-- 											<a role='button' data-toggle='collapse' data-parent='#accordion' href='#manager'>${groupName}列表</a> --%>
<!-- 										</p> -->
										
<!-- 									</div> -->
<!-- 									<div id='manager' class='panel-collapse collapse in'> -->
<!-- 										<div class='panel-body'> -->
<%-- 										<c:forEach items="${list}" var="u"> --%>
<%-- 										<div class='checkbox'><label><input type='checkbox' name='namager' value='${u[0]}'/>${u[1]}</label></div> --%>
<%-- 										</c:forEach> --%>
											
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
							</div>
						</div>
						<div class='col-md-6 border-R height-500  margin-L-S'>
							<section class="content-header content-header-S">
								<i class="title-line"></i>
								<div class="title">已选</div>
							</section>
							<div class='chosed-item panel-group-S '>
								<div class='box box-warning' id='chosed'>
<!-- 									<p class='item-txt'>111111111<span class='item-del'>x</span></p> -->
								</div>
							</div>
						</div>
					</div>
					<div class='row nomargin'>
						<div class='col-md-12 wk-btns'>
							<button type="submit"  class="btn btn-info" data-dismiss="modal">确定</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
						</div>
					</div>
				
				</div>
			</div>
				
	
<script>
	(function(){
		$('[type="submit"]').click(function(){
			var names='';
			var ids='';
			console.log($('.item-txt')[0]);
			$('.item-txt').each(function(i,p){
				var name=$(this).children('a').html();
				var id=$(this).attr('data-value');
				i===0?(names+=name,ids+=id):
				  i>0?(names+=(','+name),ids+=(','+id)):
				i===$('.item-txt').size()?(names+=name,ids+=id):(names+='',ids+='');
			})
			$('[data-name="'+data_group.name+'"]').val(names);
			$('[data-id="'+data_group.id+'"]').val(ids);
			var href=$('[data-dept="dept"]').attr('href');
			var num=href.lastIndexOf('=');
			href=href.slice(0,num+1)+ids;
			$('[data-dept="dept"]').attr('href',href);
		})
		$('.panel-group input:checkbox').click(function(){
			if($(this).is(':checked')){
				$('#chosed').append("<p class='item-txt' data-value='"+$(this).val()+"'><a>"+$(this).parent().text()+"</a><span class='item-del'>x</span></p>")
			}else{
				$("#chosed p[data-value='"+$(this).val()+"']").remove();
			}
		});
		$('#chosed').on('click','.item-del',function(){
			$(".panel-group input[value='"+$(this).parent().attr('data-value')+"']").removeAttr('checked');
			$(this).parent().remove();
		});
	})();
</script>	
</body>