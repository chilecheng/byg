<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<div class="row">
	<div class="col-sm-5">
		<label>共${page2.total }条</label> 
		<select name="numPerPage" style="padding: 4px 6px" onchange="changePageSize2(this.value)">			
			<c:choose>
				<c:when test="${page2.pageSize== 10}">
					<option value="10" selected="selected">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="50">50</option>
				</c:when>
				<c:when test="${page2.pageSize== 20}">
					<option value="10">10</option>
					<option value="20" selected="selected">20</option>
					<option value="30">30</option>
					<option value="50">50</option>
				</c:when>
				<c:when test="${page2.pageSize== 30}">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30" selected="selected">30</option>
					<option value="50">50</option>
				</c:when>
				<c:when test="${page2.pageSize== 50}">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="50" selected="selected">50</option>
				</c:when>
			</c:choose>
		</select>
	</div>
	<div class="col-sm-7">
		<div class="dataTables_paginate">
			<ul class="pagination">
			
				<li class="paginate_button previous <c:if test="${page2.pageNum == 1 }">disabled</c:if>" id="example2_previous">
					<a href="JavaScript:changePageNum2('1');" >首页</a>
				</li>
<%-- 					<c:if test="${page.pageNum > 5}"> --%>
<!-- 						<li class="paginate_button"><a href="#">...</a></li> -->
<%-- 					</c:if> --%>
				<c:forEach begin="1" end="${page2.pages }" var="p">
					<c:if test="${p<(page2.pageNum+3) && p>(page2.pageNum-3)}">
						<li class="paginate_button <c:if test="${p == page2.pageNum}">active</c:if>">
							<a href="JavaScript:changePageNum2('${p}');">${p}</a>
						</li>
					</c:if>
				</c:forEach>
					<c:if test="${page2.pages - page2.pageNum > 3}">
						<li class="paginate_button"><a href="JavaScript:changePageNum2('${page2.pageNum+3}');">...</a></li>
					</c:if>
				<li class="paginate_button next <c:if test="${page2.pageNum == page2.pages }">disabled</c:if>">
					<a href="JavaScript:changePageNum2('${page2.pages}');" >末页</a>
				</li>
			</ul>
		</div>
	</div>
</div>