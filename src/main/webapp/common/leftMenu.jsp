<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>  
<script type="text/javascript">

$(document).ready(function(){
<c:forEach items="${menuList }" var="m">

	addMenu("${m.menuId}","${m.id}","${m.url}","${m.icon}","${m.name}");
	
</c:forEach>

function addMenu(fid,id,href,icon,name){
	if(fid==""){
		var hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span></a></li>";
		$("#menubar").append(hl);
	}else{
		if(href=='farewellWordOrder.do'){
			hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span><small id='farewellNumber' class='label pull-right bg-yellow'>${farewellNumber==0?'': farewellNumber}</small></a></li>";
		}else if(href=='mourningWordOrder.do'){
			hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span><small id='mourningNumber' class='label pull-right bg-yellow'>${mourningNumber==0?'': mourningNumber}</small></a></li>";
		}else if(href=='taskFuneral.do'){
			hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span><small id='funeralNumber'  class='label pull-right bg-yellow'>${funeralNumber==0?'': funeralNumber}</small></a></li>";
		} 
		/* 新增 法政验尸解冻 */
		 else if(href=='autopsyRecord.do'){ 
			hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span><small id='autoFreezerNumber'  class='label pull-right bg-yellow'>${autoFreezerNumber==0?'': autoFreezerNumber}</small></a></li>";
		} /**遗体解冻时间安排   */
		else if(href=='freezerThawTime.do'){
			hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span><small id='freezerThawNumber'  class='label pull-right bg-yellow'>${freezerThawNumber==0?'': freezerThawNumber}</small></a></li>";
		} 
		 else if(href=='ListCar.do'){
			hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span><small id='carNumber'  class='label pull-right bg-yellow'>${carNumber==0?'': carNumber}</small></a></li>";
		}  
		else{
			var hl = "<li id='id_"+id+"' ><a id='a_"+id+"' href='"+href+"' target='homeTab'><i class='fa "+icon+"'></i><span>"+name+"</span></a></li>";
		}
		if($("#ul_"+fid).length<=0){
			hl="<ul id='ul_"+fid+"' class='treeview-menu' >"+hl+"</ul>";
			$("#id_"+fid).attr("class","treeview");
			$("#id_"+fid).append(hl);
		}else{
			$("#ul_"+fid).append(hl);
		}
		$("#a_"+fid).removeAttr("target");
		if($("#i_"+fid).length<=0){
			$("#a_"+fid).append("<i id='i_"+fid+"' class='fa fa-angle-left pull-right'>");
		}
		
		
	}
}
initTab();
});
</script>
<section class="sidebar">
 <!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu" id="menubar">
	 	 <li class="header" style="font-size: 18px;color: #b3b3b3;">&nbsp;&nbsp;殡仪业务</li>
	</ul>
</section>
