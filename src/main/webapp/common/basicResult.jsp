<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
	<style>
		.wk-btns .btn{
			padding:3px 25px;
			border-radius:8px;
		}
		.wk-btns{
			text-align:center;
			margin-top:0px;
			padding:10px 0px;
			border-top:1px solid #ccc;
		}
		.nomargin{
			margin:0px;
		}
		.modal-content{
			width:800px;
			margin-left:-100px;
		}
	</style>
</head>
<body>	
	<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
<%-- 	<%@ include file="/common/pageHead.jsp"%> --%>
		<div class="modal-content">
			<div class='row nomargin'>
				<div class='col-md-12'>
					<section class="content-header">
						<i class="title-line"></i>
						<div class="title">查询结果</div>
					</section>	
				</div>	
				<div class='col-md-12' style="overflow-y: scroll; max-height:270px;">
					<table class='table table-bordered table-hover'>
						<thead>
							<tr>
								<th><input type="checkbox" hidden group="id"></th>
								<th>死者姓名</th>
								<th>年龄</th>
								<th>性别</th>
								<th>身份证号</th>
								<th>家庭地址</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${page}" var="u">
						<tr>
						<td><input name='id' dName='${u[1]}' age='${u[2]}' sex='${u[3]}' dieId='${u[4]}' cardCode='${u[6]}' fName='${u[7]}'
						fPhone='${u[8]}' fAppellationOption="${u[9]}"  dieTime="${u[10]}" cremationTime="${u[11]}" addr="${u[5] }"
						fCardCode="${u[12]}" value='${u[0]}' class="checkBoxCtrl" type='radio'/></td>
						<td>${u[1]}</td>
						<td>${u[2]}</td>
						<td>${u[3]}</td>
						<td>${u[4]}</td>
						<td>${u[5]}</td>
						</tr>
						</c:forEach>
						
						</tbody>
					</table>
<%-- 					<%@ include file="/common/pageFood.jsp"%> --%>
				</div>
			</div>
			<div class='row nomargin'>
				<div class='col-md-12 wk-btns'>
					<button type="submit" data-sure='yes'  class="btn btn-info">确定</button>
					<a checkone="true" checkname="id" data-check='one' class='hide' target="dialog" rel="myModal" role="button"></a>
					<button type="submit" hidden data-miss='miss' data-dismiss="modal"></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
				</div>
			</div>
				
		</div>
	</div>
<script>
	(function(){
		$('[data-sure="yes"]').click(function(){
			var size=$('[name="id"]:checked').size();
			if(size){
				var input=$('[name="id"]:checked');
				$('#dName').val(input.attr('dName'));
				$("#dNameId").val(input.val());
				$("#age").val(input.attr('age'));
				$("#code").val(input.attr("cardCode"));
				$('#sex').val(input.attr('sex'));
				$('#fName').val(input.attr('fName'));
				$('#fPhone').val(input.attr('fPhone'));				
				$("#address").val(input.attr('addr'));
 				$("#fCardCode").val(input.attr('fCardCode')); 				
				$('#fAppellationId').html(input.attr('fAppellationOption'));
				//基本信息		
       			$("#cardcode").val(input.attr('dieId'));
       			$("#dieTime").val(input.attr("dieTime").slice(0,input.attr("dieTime").lastIndexOf('.')));
       			$("#cremationTime").val(input.attr('cremationTime').slice(0,input.attr('cremationTime').lastIndexOf('.')));
       			
				$('[data-miss="miss"]').click();
			}else{
				$('[data-check="one"]').click();
			}
		})
	})();
</script>	
</body>