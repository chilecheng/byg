(function () {
    $.extend({
        loadShow: function () {
            $("#bonfire-pageloader")
                .removeClass('bonfire-pageloader-hide bonfire-pageloader-fade');
            document.addEventListener('mousewheel', stopMouse, {passive: false});
        },
        loadHide: function () {
            /* fade out the loading div */
            $("#bonfire-pageloader")
                .addClass('bonfire-pageloader-fade')
                .on('transitionend', function () {
                    $(this).addClass('bonfire-pageloader-hide');
                    $(this).unbind('transitionend');
                    document.removeEventListener('mousewheel', stopMouse);
                });
        }
    });

    function stopMouse(e) {
        if (navigator.userAgent.toLowerCase().indexOf('msie') >= 0) {
            event.returnValue = false;
        } else {
            e.preventDefault();
        }
    }
}());

