/**
 * 跳转js
 */
function initTab(){
	//$("a[target=homeTab]").each(function(){
		//$(this).unbind("click");
		timeRange();
		$('body').off("click.homeTab").on('click.homeTab',"a[target=homeTab],area[target=homeTab]",function(event){
			//
			if(timer!==null){
				clearTimeout(timer);
				timer=null;
			}
			if($('.modal-backdrop.fade.in').size()){
				$('.modal-backdrop.fade.in').removeClass('in').removeClass('modal-backdrop').addClass('modal');
				if($('body').has('modal-open'))$('body').removeClass('modal-open').css('padding-right','0px');
			}
			var $this = $(this);
			var href=$this.attr("href");
			var checkName = $(this).attr("checkName");
			var checkOne = $(this).attr("checkOne");
			var warm = $(this).attr("warm");
			var code=$(this).attr('data-code');
			var checkpay=$(this).attr('data-checkpay');
			var add=$(this).attr('data-add');
			var addData=$(this).attr('data-addData');
			
			//新加：委托业务收费区别
			var payTime=$(this).attr('data-paytime');
			
			if(add!==undefined){
				href+=$this.parent().siblings(':eq(0)').find('[name="checkboxId"]').val().trim();
			}
			if(addData!==undefined){
				href+='&appointment='+JSON.stringify(data);
			}
			if(checkName!=undefined){
				var ids = "";
				var num = 0;
				$("input[name='"+checkName+"']:checked").each(function(){
					var id = $(this).val();
					if(ids!=""){
						ids+=",";
					}
					ids+=id;
					num++;
				});
				if(checkOne){					
					if(num!=1){
						toastr["warning"]("请选择一条记录");
						return false;
					}					
				}else{
					if(num<1){
						toastr["warning"]("请选择记录");
						return false;
					}
				}
				href+=ids;
				if(code!==undefined){
					var codeArr=code.split('-');
					for(var key in codeArr){
						var codes=$("input[name='"+checkName+"']:checked").parent().siblings().eq(codeArr[key]-1).text();
						href+=('&code'+(codeArr[key])+'='+codes);
					}
					if(code=='2-4-8'){href+='&fid='+$('[name="fid"]').val()}
				}
				if(checkpay){
					var checkFlag = $(this).attr('data-checkFlag');
					if (checkFlag) {//因为火化委托单列表最后面加了一列 所以要区分开来
						var txt=$("input[name='"+checkName+"']:checked").parent().siblings().last().prev().text().trim();
					} else {
						var txt=$("input[name='"+checkName+"']:checked").parent().siblings().last().text().trim();
					}
					if(txt==='已收费'){
						toastr["warning"]("该记录已收费");
						return false;
					}else if(txt === '已审核'&&checkFlag==1){
						toastr["warning"]("该记录已审核");
						return false;
					}else if(txt === '未审核'&&checkFlag==2){
						toastr["warning"]("该记录已处于未审核状态");
						return false;
					}
				}
				
				//委托业务收费
				if(payTime){
					var payTime2=$("input[name='"+checkName+"']:checked").parent().siblings().last().text().trim();
					href+='&payTime='+payTime2;
					var total=$("input[name='"+checkName+"']:checked").parent().siblings().last().prev().text().trim();
					href+='&total='+total;
					var orderNumber=$("input[name='"+checkName+"']:checked").parent().siblings().eq(1).text().trim();
					href+='&orderNumber='+orderNumber;
				}
			}
			//根据条件限制修改
//			if($('#contentID').length > 0){
//				var tr = $('.table input:checked').parents('tr').find('.tdID font');
//				if( tr.text() == '1'){
//					toastr["warning"]("已限制,无法修改");
//				     return false;
//				  }
//			}
			
			if(href==""){
				return false;
			}
			
			
			
			
			
			$.ajax({
				url:href,
				async:false,
				type:"POST",
				dataType:"html",
				success:function(json){
					$("#page-content").html(json);

					$('body').animate({scrollTop: '0px'}, 100 ); 
					initTab();    
					$('#pageForm:not(".outerform") input, #pageForm:not(".outerform") select').each(function(){
						var name = $(this).attr('name');
						if( searchData[name] ){
							$(this).val(searchData[name]);
						}
					});
					if(window.isBack){
						$('.btn-search').trigger("click");
						window.isBack = false;
					}
					
					//根据条件限制修改
//					if($('#contentID').length > 0){
//						$('.table .tdID font').each(function(){
//							  if( $(this).text() == '1'){
//							     var tr = $(this).parents('tr');
//							     tr.find('.light.middle').removeAttr('target');
//								 tr.find('.light.middle').removeAttr('href');
//								 tr.find('.light.middle').css('background','#aaa');
//							  }
//							});
//					}

					
					
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					if(XMLHttpRequest.status==500){
						toastr["error"]("程序内部出现错误"); 
					}else if(XMLHttpRequest.status==404){
						toastr["error"]("页面不存在"); 
					}else{
						toastr["error"](XMLHttpRequest.status); 
					}
				}
			});
			
			
			event.preventDefault();
			
			 
		});
	//});
	
	$("a[target=dialog]").each(function(){
		$(this).unbind("click");
		$(this).click(function(event){
			$("#page-content").unbind('click');
			if(timer!==null){
				clearTimeout(timer);
				timer=null;
			}
			var $this = $(this);
			var href=$this.attr("href");
			var checkName = $(this).attr("checkName");
			var checkOne = $(this).attr("checkOne");
			var check = $(this).attr("check");
			var code=$(this).attr('data-code');
			var startInterval=$(this).attr('data-startInterval')
			if(checkName!=undefined){
				var checkVal=$(this).attr('data-check');
				if (checkVal) {
					var result;
					if (checkVal=='checkCar') {//派送车辆
						result=$("input[name='"+checkName+"']:checked").parent().siblings().eq(8).text();
						if (result=='已派车') {
							toastr["warning"]("已派车"); 
							return false;
						}
					}
					if (checkVal=='check') {//火化入炉
						result=$("input[name='"+checkName+"']:checked").parent().siblings().eq(9).text();
						if (result=='未收费') {
							toastr["warning"]("还未收费，不能进行此操作"); 
							return false;
						}
						result=$("input[name='"+checkName+"']:checked").parent().siblings().eq(8).text();
						if (result=='已火化') {
							toastr["warning"]("该死者已入炉"); 
							return false;
						}
					}
					if (checkVal=='checkChangePt') {
						result=$("input[name='"+checkName+"']:checked").parent().siblings().eq(5).text();
						if (result=='普通炉') {
							toastr["warning"]("已经是普通炉"); 
							return false;
						}
					}
					if (checkVal=='checkChangePt' || checkVal=='checkChangeTy') {
						result=$("input[name='"+checkName+"']:checked").parent().siblings().eq(8).text();
						if (result=='已火化') {
							toastr["warning"]("该死者已入炉"); 
							return false;
						}
					}
					if (checkVal=='ifCheckBody') {
						result=$("input[name='"+checkName+"']:checked").parent().siblings().eq(4).text();
						if (result!=='') {
							toastr["warning"]("该尸体已验尸"); 
							return false;
						}
					}
					
				}
				/*===*/
				if(check==undefined){
					check=true;
				}
				var ids = "";
				var num = 0;
				$("input[name='"+checkName+"']:checked").each(function(){
					var id = $(this).val();
					if(ids!=""){
						ids+=",";
					}
					ids+=id;
					num++;
				});
				if(check==true){
					if(checkOne){
						if(num!=1){
							toastr["warning"]("请选择一条记录");
							return false;
						}
					}else{
						if(num<1){
							toastr["warning"]("请选择记录");
							return false;
						}
					}
				}
				href+=ids;
				if(code!==undefined){
					var codeArr=code.split('-');
					for(var key in codeArr){
						var codes=$("input[name='"+checkName+"']:checked").parent().siblings().eq(codeArr[key]-1).text();
						href+=('&code'+(codeArr[key])+'='+codes);
					}
				}
			}
			var rel=$this.attr("rel");
			if($("#"+rel,$("#page-content")).length<=0){
				var dialogDiv = "<div class='modal fade' id='"+rel+"' tabindex='-1' role='dialog'></div>";
				$("#page-content").append(dialogDiv);
				if(startInterval){
					$("#page-content").on('click','[data-dismiss="modal"]',function(){
						if (!$(this).attr('data-noRefresh')) {
							refreshHome();
						}
					})
					$('#myModal').unbind('click');
				}
			}
			if(href==""){
				return false;
			}
			$.ajax({
				url:href,
				async:false,
				type:"POST",
				dataType:"html",
				success:function(json){
					$("#"+rel).html(json);
					$("#"+rel).modal("show");
					initTab();
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {  
					if(XMLHttpRequest.status==500){
						toastr["error"]("程序内部出现错误"); 
					}else if(XMLHttpRequest.status==404){
						toastr["error"]("页面不存在"); 
					}else{
						toastr["error"](XMLHttpRequest.status);
					}
				}
			});
			event.preventDefault();
		});
	});
	
	$("a[target=ajaxTodo]").each(function(){
		$(this).unbind("click");
		$(this).click(function(event){
			var $this = $(this);
			var href=$this.attr("href");
			var checkName = $(this).attr("checkName");
			var checkOne = $(this).attr("checkOne");
			var warm = $(this).attr("warm");
			var code=$(this).attr('data-code');
			var canDelete = $(this).attr('data-delete');
			var busnessCancel=$(this).attr('busnessCancel');
			if(canDelete) {
				var txt=$("input[name='"+checkName+"']:checked").parent().siblings().last().prev().text().trim();
				if(txt==='已审核'){
					toastr["warning"]("请先取消审核");
					return false;
				}
			}
			if(checkName!=undefined){
				var ids = "";
				var num = 0;
				$("input[name='"+checkName+"']:checked").each(function(){
					var id = $(this).val();
					if(ids!=""){
						ids+=",";
					}
					ids+=id;
					num++;
				});
				if(checkOne){
					if(num!=1){
						toastr["warning"]("请选择一条记录");
						return false;
					}
				}else{
					if(num<1){
						toastr["warning"]("请选择记录");
						return false;
					}
				}
				href+=ids;
				if(code!==undefined){
					var codeArr=code.split('-');
					for(var key in codeArr){
						var codes=$("input[name='"+checkName+"']:checked").parent().siblings().eq(codeArr[key]-1).text();
						href+=('&code'+(codeArr[key])+'='+codes);
					}
				}
				//委托业务收费中 撤消收费使用
				if(busnessCancel){
					var payNumber=$("input[name='"+checkName+"']:checked").parent().siblings().eq(0).text();
					var orderNumber=$("input[name='"+checkName+"']:checked").parent().siblings().eq(1).text();
					href+="&payNumber="+payNumber;
					href+="&orderNumber="+orderNumber;
				}
			}
			var rel=$this.attr("rel");
			if(href==""){
				return false;
			}
		    bootbox.setLocale("zh_CN"); 
			bootbox.confirm({
				title:"操作确认",
			    size: 'small',
			    message: warm, 
			    callback: function(result){ 
			    	if(result){
			    		$.ajax({
			    			url:href,
			    			async:false,
			    			type:"POST",
			    			dataType:"json",
			    			success:dialogAjaxDone,
							error:function(XMLHttpRequest, textStatus, errorThrown) {  
								if(XMLHttpRequest.status==500){
									toastr["error"]("程序内部出现错误"); 
								}else if(XMLHttpRequest.status==404){
									toastr["error"]("页面不存在"); 
								}else{
									toastr["error"](XMLHttpRequest.status); 
								}
							}
			    		});
			    	}
			    }
			});
			event.preventDefault();
		});
	});

 	$(".checkBoxCtrl").click(function(){
 		var $this = $(this);
 		var group = $this.attr("group");
 	    if($this.is(':checked')){
 	    	$("input[name='"+group+"']").prop("checked",true);   
 	    }else{    
 	    	$("input[name='"+group+"']").prop("checked",false); 
 	    }    
 	});
 	
 	$("a[target=taskMenu]").each(function(){
		$(this).unbind("click");
		$(this).click(function(event){
			var $this = $(this);
			$this.addClass('active').parent().siblings().find('a').removeClass('active');
			var href=$this.attr("href");
			$.ajax({
				url : href,
				async : false,
				data : {
					"method" : "showList",
				},
				type : "POST",
				dataType : "html",
				success : function(json) {
					$('#tabs').html(json);
//					$('body').animate({
//						scrollTop : '0px'
//					}, 100);
					initTab();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					if (XMLHttpRequest.status == 500) {
						toastr["error"]("程序内部出现错误");
					} else if (XMLHttpRequest.status == 404) {
						toastr["error"]("页面不存在");
					} else {
						toastr["error"](XMLHttpRequest.status);
					}
				}
			});
			event.preventDefault();
		});
	});
}

function openHomeTab(href){
	if(href==""){
		return false;
	}
	$.ajax({
		url:href,
		async:false,
		type:"POST",
		dataType:"html",
		success:function(json){
			$("#page-content").html(json);
			$('body').animate( 
					{ scrollTop: '0px' }, 100 
					);
			initTab();
		},
		error:function(XMLHttpRequest, textStatus, errorThrown) {  
			if(XMLHttpRequest.status==500){
				toastr["error"]("程序内部出现错误"); 
			}else if(XMLHttpRequest.status==404){
				toastr["error"]("页面不存在"); 
			}else{
				toastr["error"](XMLHttpRequest.status); 
			}
		}
	});
}

function openDialogTab(dialogId,href){
	var rel=dialogId;
	if($("#"+rel,$("#page-content")).length<=0){
		var dialogDiv = "<div class='modal fade' id='"+rel+"' tabindex='-1' role='dialog'></div>";
		$("#page-content").append(dialogDiv);
	}
	if(href==""){
		return false;
	}
    $.ajax({
        url:href,
        async:false,
        type:"POST",
        dataType:"html",
        success:function(json){
            $("#"+rel).html(json);
            $("#"+rel).modal("show");
            initTab();
        },
        error:function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status==500){
                toastr["error"]("程序内部出现错误");
            }else if(XMLHttpRequest.status==404){
                toastr["error"]("页面不存在");
            }else{
                toastr["error"](XMLHttpRequest.status);
            }
        }
    });
}

$(function(){
    initTab();
});