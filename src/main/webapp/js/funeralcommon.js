function changeDate(){
	$("#pageForm").submit();
}

/*var sum=0;//标识
var aTotal;//合计总金额
*/
//项目名称改变
function itemName(id){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
	    	
	    	aTotal=aTotal-$("#total_"+id).val();
			aTotal+=json.pice;
		    $("#articlesFont").html(aTotal+"元");
		    $("#total_"+id).val(json.pice);
	
    		$("#pice_"+id).val(json.pice);
	    	$("#number_"+id).val(1);
	    	$("#remarks_"+id).val("");
	    	$("#iscredit_"+id).val(0);//
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 

//数目改变
function number(id){
	
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	if(a!=number){//表示是不是数字
		toastr["warning"]("请输入正确的数量！");
	}else{
			var pice=$("#pice_"+id).val();
	  		
    		aTotal=aTotal-$("#total_"+id).val();
    		
			aTotal+=pice*number;
		
	    	$("#articlesFont").html(aTotal+"元");
	
	    	$("#total_"+id).val(pice*number)

		
	}
} 

//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value_index")
		aTotal=aTotal-$("#total_"+id).val();
		$("#articlesFont").html(aTotal+"元");

		$(this).parent().parent().remove();
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
function readData(form,callback) {
	$("button").prop("disabled", "disabled");
	var tbody=[];
	$("#articles").find("tr").each(function(i,tr){	
		var trs='';	
		$(tr).find("td").each(function(j,td){	
			if(j>0){			
					trs+=',';
			}
			trs+=$(td.children[0]).val();				
		})	
		tbody[i]=trs;			
	})
	$("button").prop("disabled", "disabled");
	var $form = $(form);
	var va = validHomeForm($form);
	if (va == false) {
		$("button").removeAttr("disabled");
		toastr["warning"]("请确保填写完整并且格式正确");
		return false;
	}
	rel = $form.attr("rel");
	var datas= $form.serializeArray();
	var obj={};
	for(var key in datas){
		obj[datas[key].name]=datas[key].value;
	}
	obj.table=tbody;
	console.log(obj)
	//datas.push({name:"table",value:tbody});
	//console.log(JSON.stringify(datas));
	$.ajax({
		type : 'POST',
		traditional: true,
		url : $form.attr("action"),
		data : obj,
		dataType : "json",
		cache : false,
		success : callback,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			toastr["error"](XMLHttpRequest.status);
			$("button").removeAttr("disabled");
		}
	});
	
	return false;
	
}
