function update(tag,beginDate0,endDate0,pice,flag) {
	var months = $(tag).val();
	var add = parseInt(months);
	if(flag==true){
		var total = add*pice;//当前骨灰寄存费用为0元每月
		$("#total").val(total);
	}
	var beginDate = $(beginDate0).val();
	var year = beginDate.split("-")[0];
	var month = beginDate.split("-")[1];
	var day = beginDate.split("-")[2];
	// 这段主要是把月和日中的像"01"这样的改成"1"
	if (month.substr(0, 1) == 0)
		month = month.substr(1, 1);
	if (day.substr(0, 1) == 0)
		day = day.substr(1, 1);
	// 字符转换成数字
	year = parseInt(year);
	month = parseInt(month);
	day = parseInt(day);
	// 计算新的年和月
	var newmonth = month + add; // 直接把起始的月和间隔相加
	year += parseInt(newmonth / 12); // 总月除以12，商加在起始年上,就是终点的年
	if (newmonth >= 12) { // 大于12就说明是新的一年
		if (newmonth % 12 == 0) { // 这里很有意思,举个例子吧:2011-2-23在10个月后的日期是2011-12-23，就是为了这种情况
			year = year - 1;
			month = 12;
		}
		else
			month = parseInt(newmonth % 12);
	}
	else
		month += add;
	// 计算day
	if (month == 2 && day >= 28) { // 新日期是2月28日以后的情况
		if (year % 4 == 0 && year % 100 != 0) // 闰年
			day = 29;
		else
			day = 28; // 平年
	}
	else { // 新日期是除了2月,日是30日以后的情况
		if (day >= 31) {
			switch (month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				day = 31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				day = 30;
				break;
			}
		}
	}
	var endDate = year + "-"
			+ (month.toString().length == 1 ? "0" + month : month) + "-"
			+ (day.toString().length == 1 ? "0" + day : day); // 构造yyyy-mm-dd的格式
	$(endDate0).val(endDate);
}