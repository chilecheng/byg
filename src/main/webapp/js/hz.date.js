//开始和结束时间控件初始化
	function timeRange(){

		//保证 只执行一次
		$('[data-id="beginDate"], [data-provide="datetimepicker"]').datepicker({  
             todayBtn : "linked",  
             autoclose : true,  
             todayHighlight : true,  
        }).on('changeDate',function(e){  
             var startTime = e.date;  
             $('[data-id="endDate"]').datepicker('setStartDate',startTime);  
        });
 		
         //结束时间：  
 		$('[data-id="endDate"]').datepicker({  
             todayBtn : "linked",  
             autoclose : true,  
             todayHighlight : true,
         }).on('changeDate',function(e){  
             var endTime = e.date;  
             $('[data-id="beginDate"]').datepicker('setEndDate',endTime);  
         })
	}

	        //带有分的时间控件
	function timeMunite(){
	 		if($('.daterangepicker.dropdown-menu.show-calendar.opensright').length!==0){
				$('.daterangepicker.dropdown-menu.show-calendar.opensright').remove();
			}
			$('[data-id="reservation"]').daterangepicker({timePicker: true, timePickerIncrement: 5, format: 'YYYY-MM-DD hh:mm:ss A'});
			$('[data-id="reservation"]').change(function(){
				var time=$(this).val();
				var arr=time.split(' ');
				if(arr.length===3){
					var hour=parseInt(arr[1].slice(0,2));
					arr.pop();
					//var second=new Date().getSeconds();
					//second<10&&(second='0'+second);
					if(time.lastIndexOf('AM')!==-1&&hour==12){
						arr[1]='0'+(hour-12)+arr[1].slice(2);
					}
					if(time.lastIndexOf('PM')!==-1){
						arr[1]=hour<12?(hour+12+arr[1].slice(2)):(arr[1].slice(0));
						
					}
					arr[1]=arr[1].slice(0,arr[1].lastIndexOf(':'));
					 console.log(arr);
					$(this).val(arr.join(' '));
				}
			});
		}
	//$(document).on('focus.datepicker.data-api click.datepicker.data-api','[data-id="reservation"]',timeMunite);