(function(){
    var select=$('#dName');
    var input=$('#show');
    var show=$('#prompt-content');
    var index=-1;
    select.change(function(){
        input.val($('#dName option:selected').html());
        var dNameId=$(this).val();
        var $form = $(pageForm);
        $.ajax({
       		type : 'POST',
       		url : $form.attr("action"),
       		data : {dNameId:dNameId,method:"dNameChange"},
       		dataType : "json",
       		cache : false,
       		success : function(json) {
       			$("#certificateCode").val(typeof(json.certificateCode)!="undefined"?json.certificateCode:'');
       			$("#dSex").val(typeof(json.dSex)!="undefined"?json.dSex:'');
       			$("#dAge").val(typeof(json.dAge)!="undefined"?json.dAge:'');
       			$("#mourningPlace").val(typeof(json.mourningPlace)!="undefined"?json.mourningPlace:'');
       			$("#beginDate").val(typeof(json.beginDate)!="undefined"?json.beginDate:'');
       			$("#endDate").val(typeof(json.endDate)!="undefined"?json.endDate:'');
       			$("#farewellPlace").val(typeof(json.farewellPlace)!="undefined"?json.farewellPlace:'');
       			$("#farewellDate").val(typeof(json.farewellDate)!="undefined"?json.farewellDate:'');
       			$("#cremationTime").val(typeof(json.cremationTime)!="undefined"?json.cremationTime:'');
       			
       		
       		}
       	});
    });
    select.click(function(){
       show.css('display','none');
    });
    input.keyup(function(e){
    	 e=e||window.event;
        var str=$(this).val();
        show.css('display','none');
        if(str.trim()!==''){
            var options=$('#dName option:gt(0)');
            var arr=[];
            options.each(function(i,opts){
                var obj={};
                var inn=opts.innerHTML;
                var val=opts.value;
                obj.inn=inn;
                obj.val=val;
                inn.indexOf(str)!==-1&&(arr.push(obj));
            });
            if(arr.length){
                var content='';
                show.css('display','block');
                for(var key in arr){
                   content+='<p data-value="'+arr[key].val+'">'+arr[key].inn+'</p>';
                }
                (e.keyCode!==38&& e.keyCode!==40&& e.keyCode!==13)&& show.html(content);
                var ps=$('#prompt-content p') ;
                function getVal(){
                	
                    input.val($(this).html());
                    var $form = $(pageForm);
                    $(this).parent().css('display','none');
                    var dNameId=$(this).attr('data-value');
                    select.val(dNameId);
                    $.ajax({
                		type : 'POST',
                		url : $form.attr("action"),
                		data : {dNameId:dNameId,method:"dNameChange"},
                		dataType : "json",
                		cache : false,
                		success : function(json) {
                			$("#certificateCode").val(typeof(json.certificateCode)!="undefined"?json.certificateCode:'');
                   			$("#dSex").val(typeof(json.dSex)!="undefined"?json.dSex:'');
                   			$("#dAge").val(typeof(json.dAge)!="undefined"?json.dAge:'');
                   			$("#mourningPlace").val(typeof(json.mourningPlace)!="undefined"?json.mourningPlace:'');
                   			$("#beginDate").val(typeof(json.beginDate)!="undefined"?json.beginDate:'');
                   			$("#endDate").val(typeof(json.endDate)!="undefined"?json.endDate:'');
                   			$("#farewellPlace").val(typeof(json.farewellPlace)!="undefined"?json.farewellPlace:'');
                   			$("#farewellDate").val(typeof(json.farewellDate)!="undefined"?json.farewellDate:'');
                   			$("#cremationTime").val(typeof(json.cremationTime)!="undefined"?json.cremationTime:'');
                			
                		
                		}
                	});
                    
                }
                ps.each(function(i,p){
                   $(p).click( getVal);
                });
            }
        }
        switch(e.keyCode){
        	case 13:
        		$('.mark')[0].enter=getVal;
        		$('.mark')[0].enter();
        		break;
	        case 38:
	            if(index>0){
	                index--;
	                $(ps[index]).addClass('mark').siblings().removeClass('mark');
	            }
	            break;
	        case 40:
	            if(index<arr.length-1){
	                index++;
	                $(ps[index]).addClass('mark').siblings().removeClass('mark');
	            }
	            break;
	        default :
	            index=-1;
	            break;
	    }
    });
})();