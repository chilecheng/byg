/**
 * form表单
 */
var rel;
toastr.options = {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-top-center",
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "2000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function uploadCallback(form, callback) {
    $("button").prop("disabled", "disabled");
    var $form = $(form);
    var formobj = document.getElementById("uploadForm");
    var formData = new FormData(formobj);
    // if(!$form.valid()){
    // return false;
    // }
    var va = validDialogForm($form);
    if (va == false) {
        $("button").removeAttr("disabled");
        toastr["warning"]("请确保填写完整并且格式正确");
        return false;
    }
    rel = $form.attr("rel");
    $.ajax({
        type: 'POST',
        url: $form.attr("action"),
        data: formData,
        dataType: 'json',
        traditional: true,
        cache: false,
        processData: false,
        contentType: false,
        success: callback,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr["error"](XMLHttpRequest.status);
            $("button").removeAttr("disabled");
        }
    });

    return false;
}

function validateCallback(form, callback) {
    $("button").prop("disabled", "disabled");

    var $form = $(form);
    // if(!$form.valid()){
    // return false;
    // }
    var va = validDialogForm($form);
    if (va == false) {
        $("button").removeAttr("disabled");
        toastr["warning"]("请确保填写完整并且格式正确");
        return false;
    }
    rel = $form.attr("rel");
    $.ajax({
        type: 'POST',
        url: $form.attr("action"),
        data: $form.serializeArray(),
        traditional: true,
        dataType: "json",
        cache: false,
        success: callback,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr["error"](XMLHttpRequest.status);
            $("button").removeAttr("disabled");
        }
    });

    return false;
}

function validateHomeCallback(form, callback) {
    $("button").prop("disabled", "disabled");
    var $form = $(form);
    // if(!$form.valid()){
    // return false;
    // }
    var va = validHomeForm($form);
    if (va == false) {
        $("button").removeAttr("disabled");
        toastr["warning"]("请确保填写完整并且格式正确");
        return false;
    }
    rel = $form.attr("rel");

    $.ajax({
        type: 'POST',
        url: $form.attr("action"),
        data: $form.serializeArray(),
        traditional: true,
        dataType: "json",
        cache: false,
        success: callback,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr["error"](XMLHttpRequest.status);
            $("button").removeAttr("disabled");
        }
    });

    return false;
}

function homeSearchNotNull(form) {
    var $form = $(form);
//	var findCode=x[0].val();
    var findName = $("#findName").val();
    var findCode = $("#findCode").val();
    if (findName == "" && findCode == "") {
        toastr["warning"]("查询值不能为空");
        return false;
    }
    $.ajax({
        type: 'POST',
        url: $form.attr("action"),
        data: $form.serializeArray(),
        traditional: true,
        dataType: "html",
        cache: false,
        success: function (json) {
            $("#page-content").html(json);
            initTab();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr["error"](XMLHttpRequest.status);
        }
    });
    return false;
}

//遍历 form 里面的  input  将input的值存在本地存储。
window.searchData = {};
window.activeNmae = '';

function aa(from) {
    var json = {};
    $('#pageForm:not(".outerform") input, #pageForm:not(".outerform") select').each(function () {
        //$('#pageForm input, #pageForm select').each(function(){
        var name = $(this).attr('name');
        var inputval = $(this).val();
        json[name] = inputval;
    });
    return json;
}

$(function () {
    $('body').on('click', '#typeDiv button', function () {
        var name = $(this).attr('name');
        window.activeNmae = name;
        $(this).addClass("btn-all").siblings("button").removeClass("btn-all");
    });
});


function homeSearch(form, type) {
    var $form = $(form);
    //将 form的值 保存起来。
    searchData = aa($form); //组装成键值对
    var formData = $form.serializeArray();
    if (type) {
        formData.push({"name": "type", "value": type});
    }
    $.loadShow();
    $.ajax({
        type: 'POST',
        url: $form.attr("action"),
        data: formData,
        traditional: true,
        dataType: "html",
        cache: false,
        success: function (json) {
            $("#page-content").html(json);
            initTab();
            if (activeNmae && $('button[name="' + activeNmae + '"]').length > 0) {
                $('button[name="' + activeNmae + '"]').addClass("btn-all").siblings("button").removeClass("btn-all");
            }
            $.loadHide();
            return false;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr["error"](XMLHttpRequest.status);
            $.loadHide();
        }
    });
    return false;
}

function homeSearch2(form) {
    var $form = $(form);
    searchData = aa($form); //组装成键值对


    var dataIs = $form.attr("data-is");
    if (!dataIs) {
        clearTimeout(fresh);
        return false;
    }
    $.ajax({
        type: 'POST',
        url: $form.attr("action"),
        data: $form.serializeArray(),
        traditional: true,
        dataType: "html",
        cache: false,
        success: function (json) {
            $("#page-content").html(json);
            initTab();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr["error"](XMLHttpRequest.status);
        }
    });
    return false;
}

function dialogAjaxDone(json) {
    if (json.statusCode == 200) {
        toastr["success"](json.message);
        $("#" + rel).modal('hide');
        $("#" + json.rel).modal('hide');
        $('.modal-backdrop').size() !== 0 && $('.modal-backdrop').remove();
        if (json.isRefresh) {
            setTimeout('homeRefreshNull()', 500);
        }
        if (json.refresh && !json.noRefresh) {
            refreshHome();
        }
    } else if (json.statusCode == 302) {
        toastr["warning"](json.message);
    } else {
        toastr["error"](json.message);
    }

    $("button").removeAttr("disabled");
}

function homeAjaxDone(json) {

    if (json.statusCode == 200) {
        toastr["success"](json.message);
        if ($('#id').attr('data-changeId')) {
            $('#id').val(json.coId);
        }
        if (json.isRefresh) {

            if (json.href != "" && json.href != null && json.href != undefined) {
                setTimeout("homeRefresh('" + json.href + "')", 500);
            }
        }
    } else if (json.statusCode == 302) {
        toastr["warning"](json.message);
    } else {
        toastr["error"](json.message);
    }
    $("button").removeAttr("disabled");
}

/**
 * 保存成功，但保存按钮不再可用
 * @param json
 * @returns
 */
function homeAjaxDoneNo(json) {

    if (json.statusCode == 200) {
        toastr["success"](json.message);
        if ($('#id').attr('data-changeId')) {
            $('#id').val(json.coId);
        }
        if (json.isRefresh) {

            if (json.href != "" && json.href != null && json.href != undefined) {
                setTimeout("homeRefresh('" + json.href + "')", 500);
            }
        }
    } else if (json.statusCode == 302) {
        toastr["warning"](json.message);
    } else {
        toastr["error"](json.message);
    }
}


/**
 * 上传文件后返回
 * @param json
 */
function afterUpload(json) {
    if (json.statusCode == 200) {
        toastr["success"](json.message);
        if (json.isRefresh) {
            setTimeout('homeRefreshNull()', 500);
        }
    } else {
        toastr["error"](json.message);
    }
    $("button").removeAttr("disabled");
}

/*新增*/
function dialogAjaxDone1(json) {
    if (json.statusCode == 200) {
        toastr["success"](json.message);
        if (json.isRefresh) {
            setTimeout('homeRefreshNull()', 500);
        }
        $('#fh').click();
    } else if (json.statusCode == 302) {
        toastr["warning"](json.message);
    } else {
        toastr["error"](json.message);
    }
    $("button").removeAttr("disabled");
}

/**
 * form表单验证(dialog)
 *
 * @param ob
 * @returns {Boolean}
 */
function validDialogForm(ob) {
    var e = 0;
    // 必填
    $(".error-label", ob).remove();
    $("[errorFlag]", ob).removeAttr("errorFlag");
    $(".error-input", ob).removeClass("error-input");

    $(".required", ob)
        .each(
            function () {
                var value = $(this).val();
                if (value == "") {
                    var flag = $(this).attr("errorFlag");
                    if (flag == undefined) {
                        $(this).attr("errorFlag", "true");
                        $(this).addClass("error-input");
                        var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
                        $(this).before(err);
                    }
                    e++;
                }
            });

    // 最小最大值
    $("[max],[min]", ob)
        .each(
            function () {
                var reg = /^-?\d+\.?\d*$/;
                var value = $(this).val();
                if (value != "") {
                    var flag = $(this).attr("errorFlag");
                    var min = $(this).attr("min");
                    var max = $(this).attr("max");
                    var le = "";
                    if (min != undefined) {
                        if (reg.test(min)) {
                            le += "大于" + min;
                        } else {
                            min = undefined;
                        }
                    }
                    if (max != undefined) {
                        if (reg.test(max)) {
                            if (le != "") {
                                le += ",";
                            }
                            le += "小于" + max
                        } else {
                            max = undefined;
                        }
                    }
                    if (flag == undefined) {
                        if (!reg.test(value)
                            || (min != undefined && parseInt(value) < parseInt(min))
                            || (max != undefined && parseInt(value) > parseInt(max))) {
                            $(this).attr("errorFlag", "true");
                            $(this).addClass("error-input");
                            var err = "<label class='control-label error-label' style='color: #dd4b39;'>"
                                + le + "</label>";
                            $(this).before(err);
                            e++;
                        }
                    }
                }
            });

    // 整数
    $(".integer", ob)
        .each(
            function () {
                var reg = /^-?\d+$/;
                var value = $(this).val();
                if (value != "") {
                    if (!reg.test(value)) {
                        var flag = $(this).attr("errorFlag");
                        if (flag == undefined) {
                            $(this).attr("errorFlag", "true");
                            $(this).addClass("error-input");
                            var err = "<label class='control-label error-label' style='color: #dd4b39;'>请填整数</label>";
                            $(this).before(err);
                        }
                        e++;
                    }
                }
            });

    // 实数
    $(".number", ob)
        .each(
            function () {
                var reg = /^-?\d+\.?\d*$/;
                var value = $(this).val();
                if (value != "") {
                    if (!reg.test(value)) {
                        var flag = $(this).attr("errorFlag");
                        if (flag == undefined) {
                            $(this).attr("errorFlag", "true");
                            $(this).addClass("error-input");
                            var err = "<label class='control-label error-label' style='color: #dd4b39;'>请填数字</label>";
                            $(this).before(err);
                        }
                        e++;
                    }
                }
            });

    if (e > 0) {
        return false;
    } else {
        return true;
    }

}

/**
 * form表单验证(dialog)
 *
 * @param ob
 * @returns {Boolean}
 */
function validHomeForm(ob) {
    var e = 0;
    // 必填
    $(".error-label", ob).remove();
    $("[errorFlag]", ob).removeAttr("errorFlag");
    $(".error-input", ob).removeClass("error-input");

    $(".required", ob)
        .each(
            function () {
                var value = $(this).val();
                if (value == "") {
                    var flag = $(this).attr("errorFlag");
                    if (flag == undefined) {
                        $(this).attr("errorFlag", "true");
                        $(this).addClass("error-input");
                        var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
                        $(this).parent().append(err);
                    }
                    e++;
                }
            });

    // 最小最大值
    $("[max],[min]", ob)
        .each(
            function () {
                var reg = /^-?\d+\.?\d*$/;
                var value = $(this).val();
                if (value != "") {
                    var flag = $(this).attr("errorFlag");
                    var min = $(this).attr("min");
                    var max = $(this).attr("max");
                    var le = "";
                    if (min != undefined) {
                        if (reg.test(min)) {
                            le += "大于" + min;
                        } else {
                            min = undefined;
                        }
                    }
                    if (max != undefined) {
                        if (reg.test(max)) {
                            if (le != "") {
                                le += ",";
                            }
                            le += "小于" + max
                        } else {
                            max = undefined;
                        }
                    }
                    if (flag == undefined) {
                        if (!reg.test(value)
                            || (min != undefined && parseInt(value) < parseInt(min))
                            || (max != undefined && parseInt(value) > parseInt(max))) {
                            $(this).attr("errorFlag", "true");
                            $(this).addClass("error-input");
                            var err = "<label class='control-label error-label' style='color: #dd4b39;'>"
                                + le + "</label>";
                            $(this).parent().append(err);
                            e++;
                        }
                    }
                }
            });

    // 整数
    $(".integer", ob)
        .each(
            function () {
                var reg = /^-?\d+$/;
                var value = $(this).val();
                if (value != "") {
                    if (!reg.test(value)) {
                        var flag = $(this).attr("errorFlag");
                        if (flag == undefined) {
                            $(this).attr("errorFlag", "true");
                            $(this).addClass("error-input");
                            var err = "<label class='control-label error-label' style='color: #dd4b39;'>请填整数</label>";
                            $(this).parent().append(err);
                        }
                        e++;
                    }
                }
            });

    // 实数
    $(".number", ob)
        .each(
            function () {
                var reg = /^-?\d+\.?\d*$/;
                var value = $(this).val();
                if (value != "") {
                    if (!reg.test(value)) {
                        var flag = $(this).attr("errorFlag");
                        if (flag == undefined) {
                            $(this).attr("errorFlag", "true");
                            $(this).addClass("error-input");
                            var err = "<label class='control-label error-label' style='color: #dd4b39;'>请填数字</label>";
                            $(this).parent().append(err);
                        }
                        e++;
                    }
                }
            });

    if (e > 0) {
        return false;
    } else {
        return true;
    }

}

function homeRefreshNull() {
    $("#pageForm").submit();
    $('body').css('padding-right', '0px');
    initTab();
}

function homeRefresh(href) {
    $.ajax({
        url: href,
        async: false,
        type: "POST",
        dataType: "html",
        traditional: true,
        success: function (json) {
            $("#page-content").html(json);
            $('body').animate({
                scrollTop: '0px'
            }, 100);
            initTab();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (XMLHttpRequest.status == 500) {
                toastr["error"]("程序内部出现错误");
            } else if (XMLHttpRequest.status == 404) {
                toastr["error"]("页面不存在");
            } else {
                toastr["error"](XMLHttpRequest.status);
            }
        }
    });

}

/**弹出确认框的调用这个方法
 * @param m
 */
function click_change(m) {
    bootbox.setLocale("zh_CN");
    bootbox.confirm({
        title: "操作确认",
        size: 'small',
        message: m,
        callback: function (result) {
            if (result) {
                $("#detail").submit();
            }
        }
    });
}

$(function () {
    $('body').on('click', '.btn.btn-info.btn-search', function () {
        $('#pageForm #pageNum').val("1");
    });

    $('body').on('click', '.btn.btn-default', function () {
        window.isBack = true;
    });

    $('body').on('click', '.sidebar a[target="homeTab"]', function () {
        window.searchData = {};
    });
})

