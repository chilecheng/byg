<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
<%@ include file="/common/common.jsp" %> 
<!-- 必要加载 -->
<!-- jQuery 2.2.0 -->
<script src="lte/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="lte/bootstrap/js/bootstrap.min.js"></script>
<!-- Font Awesome -->
<link rel="stylesheet" href="lte/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="lte/ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="lte/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="lte/dist/css/skins/_all-skins.min.css">
<!-- AdminLTE App -->
<script src="lte/dist/js/app.min.js"></script>

<!-- 选择加载 -->
<!-- jvectormap -->
<link rel="stylesheet" href="lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- FastClick -->
<script src="lte/plugins/fastclick/fastclick.js"></script>
<!-- Sparkline -->
<script src="lte/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<!-- <script src="lte/plugins/slimScroll/jquery.slimscroll.min.js"></script> -->
<!-- ChartJS 1.0.1 -->
<script src="lte/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="lte/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="lte/dist/js/demo.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
  	<!-- 头部 -->
	<%@ include file="/common/head.jsp" %> 
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- 左侧菜单栏 -->
	<%@ include file="/common/leftMenu.jsp" %> 
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- 中心内容 -->
    
	<%@ include file="/WEB-INF/jsp/home/hhwtd1.jsp" %> 


  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <!-- 底部内容 -->
	<%@ include file="/common/foot.jsp" %> 
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- 右侧弹出菜单 -->
	<%@ include file="/common/rightMenu.jsp" %> 
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
</body>
</html>
