<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化委托单</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="">
					<div class="box-body">
						<div class="col-md-4">
							<label>类型：</label> <select class="list_select ">
								<option>已审核</option>
								<option>未审核</option>
							</select>
						</div>
						<div class="col-md-8">
							<label>备注：</label> <input type="text" class="list_select"
								value="">
						</div>
						<div class="col-md-4">
							<label>时间：</label> <input type="text" class="list_select"
								value="">
						</div>

						<div class="col-md-4">
							<label>常用：</label> <a href="#" class="btn btn-primary btn-sm"
								role="button">未洽谈</a> <a href="#" class="btn btn-default btn-sm"
								role="button">已洽谈</a> <a href="#" class="btn btn-default btn-sm"
								role="button">未审核</a> <a href="#" class="btn btn-default btn-sm"
								role="button">已审核</a>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-info">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<button type="button" class="btn btn-primary">基本减免</button>
					<button type="button" class="btn btn-primary">全视图</button>
					<button type="button" class="btn btn-primary">信息修改</button>
					<button type="button" class="btn btn-primary">业务变更</button>
					<button type="button" class="btn btn-primary">业务变更</button>
					<button type="button" class="btn btn-primary">挂账</button>
					<button type="button" class="btn btn-primary">添加</button>
					<button type="button" class="btn btn-success pull-right">删除</button>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th><input type="checkbox"></th>
											<th>死者姓名</th>
											<th>年龄</th>
											<th>死者性别</th>
											<th>冰柜号</th>
											<th>火化时间</th>
											<th>死亡证明</th>
											<th>是否审核</th>
										</tr>
									</thead>
									<tbody>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
										<tr role="row">
											<td><input type="checkbox"></td>
											<td><font color="blue">张三</font></td>
											<td>88</td>
											<td>男</td>
											<td>普通8号</td>
											<td>2016.6.12-12:00</td>
											<td><font color="#b2ed6e">已收</font></td>
											<td><font color="red">未审核</font></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>

</section>