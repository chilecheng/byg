<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String wsPath = "ws://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String reportPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="edge"/>
    <title>温州殡仪馆系统</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <%@ include file="/common/common.jsp" %>
    <script type='text/javascript'>
        var _vds = _vds || [];
        window._vds = _vds;
        (function () {
            _vds.push(['setAccountId', '8a8fbef7c889e8f6']);
            (function () {
                var vds = document.createElement('script');
                vds.type = 'text/javascript';
                vds.async = true;
                vds.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'dn-growing.qbox.me/vds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(vds, s);
            })();
        })();
    </script>
    <!-- 必要加载 -->
    <!-- jQuery 2.2.0 -->
    <script src="js/jquery-2.0.3.min.js"></script>
    <!-- fileinput -->
    <link type="text/css" rel="stylesheet" href="css/fileinput.min.css"/>
    <script type="text/javascript" src="js/fileinput.min.js"></script>
    <script type="text/javascript" src="js/fileinput_locale_zh.js"></script>
    <!-- <script src="lte/plugins/jQuery/jQuery-2.2.0.min.js"></script> -->
    <script src="lte/plugins/json/json.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="lte/bootstrap/js/bootstrap.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="lte/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="lte/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="lte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="lte/dist/css/skins/_all-skins.min.css">
    <!-- AdminLTE App -->
    <script src="lte/dist/js/app.min.js"></script>

    <!-- 选择加载 -->
    <!-- jvectormap -->
    <link rel="stylesheet" href="lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- FastClick -->
    <script src="lte/plugins/fastclick/fastclick.js"></script>
    <!-- Sparkline -->
    <script src="lte/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- 日期控件 -->
    <link rel="stylesheet" href="lte/plugins/datetimepicker/bootstrap-datetimepicker.css">
    <script src="lte/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="lte/plugins/datepicker/datepicker3.css">
    <script src="lte/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- 日期时刻控件 -->
    <link rel="stylesheet" href="lte/plugins/daterangepicker/daterangepicker-bs3.css">
    <script src="lte/plugins/daterangepicker/moment.min.js"></script>
    <script src="lte/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/laydate/laydate.js"></script>
    <!-- 下拉多选框控件 -->
    <link href="js/select/multiple-select.css" rel="stylesheet"/>
    <script src="js/select/multiple-select.js"></script>
    <!-- toastr -->
    <link href="plug/toastr/toastr.min.css" rel="stylesheet"/>
    <script src="plug/toastr/toastr.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <!-- <script src="lte/plugins/slimScroll/jquery.slimscroll.min.js"></script> -->
    <!-- ChartJS 1.0.1 -->
    <script src="lte/plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="lte/dist/js/pages/dashboard2.js"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script src="lte/dist/js/demo.js"></script>
    <script src="js/hz.form.js"></script>
    <script src="js/hz.tab.js"></script>
    <script src="js/hz.ui.js"></script>
    <!-- <script src="js/sockjs-0.3.min.js"></script> -->
    <!--打印功能-->
    <script src="js/print.js"></script>
    <script src="js/showprint.js"></script>
    <script src="js/payprint.js"></script>
    <script src="js/showprint2.js"></script>
    <script src='js/hz.date.js'></script>
    <script src='js/loading.js'></script>
    <link rel="stylesheet" href="css/hz.list.css">
    <link rel='stylesheet' href="css/hz.common.css">
    <link rel='stylesheet' href='css/hz.selectItem.css'>
    <link rel='stylesheet' href='css/areasale.css'>
    <link rel="stylesheet" type="text/css" media="print" href="css/print.css"/>
    <link rel="stylesheet" type="text/css" href="css/loading.css">
    <!-- 清除缓存 -->
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="5s"/>
    <style>
        .header-top {
            position: fixed;
            width: 100%;
        }

        .content-wrapper {
            padding-top: 65px;
        }

        .main-sidebar {
            position: fixed;
            overflow-y: auto;
        }

        .padding-t {
            margin-top: 10px;
        }

        .sidebar-left {
            display: none;
            width: 215px;
            height: 100%;
            position: fixed;
            overflow-y: auto;
            z-index: 1000;
            margin: 0 1px;
            background: #2c3b41;
            top: 50px;
            left: 49px;
        }

        .sidebar-sroll > a {
            padding: 12px 5px 12px 15px;
            display: block;
            color: #fff;
            background: #1e282c;
            border-left-color: #3c8dbc;
        }

        .sidebar-sroll > ul > li > a {
            padding: 5px 5px 5px 15px;
            display: block;
            font-size: 14px;
        }

        .sidebar-sroll > ul > li > a > .fa, .sidebar-sroll > a > .fa {
            width: 20px;
        }


        .sidebar-sroll ul {
            padding: 0;
            display: block !important;
        }

        .arrow {
            display: block;
            position: fixed;
        }

        .arrow {
            width: 0;
            height: 0;
            border-top: 7px solid transparent;
            border-left: 10px solid #fff;
            border-bottom: 7px solid transparent;
        }

    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header header-top" id="head_hidden">
        <!-- 头部 -->
        <%@ include file="/common/head.jsp" %>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <div class="scroll-wrapper">
            <!-- 左侧菜单栏 -->
            <%@ include file="/common/leftMenu.jsp" %>
        </div>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="page-content">
        <!-- 中心内容 -->
        <%@ include file="/common/content.jsp" %>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer" id="f_hidden">
        <!-- 底部内容 -->
        <%@ include file="/common/foot.jsp" %>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- 右侧弹出菜单 -->
        <%@ include file="/common/rightMenu.jsp" %>
    </aside>
    <div class="control-sidebar-bg"></div>
    <div class="sidebar-left">
        <span class="arrow"></span>
        <div class="sidebar-sroll"></div>
    </div>
</div>

<%--加载层--%>
<div id="bonfire-pageloader" class="bonfire-pageloader-hide">
    <div class="bonfire-pageloader-icon">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="512px" height="512px" viewBox="0 0 512 512" enable-background="new 0 0 512 512"
             xml:space="preserve">
		<path id="loading-12-icon" d="M291,82.219c0,16.568-13.432,30-30,30s-30-13.432-30-30s13.432-30,30-30S291,65.65,291,82.219z
			 M261,404.781c-15.188,0-27.5,12.312-27.5,27.5s12.312,27.5,27.5,27.5s27.5-12.312,27.5-27.5S276.188,404.781,261,404.781z
			 M361.504,113.167c-4.142,7.173-13.314,9.631-20.487,5.489c-7.173-4.141-9.631-13.313-5.49-20.487
			c4.142-7.173,13.314-9.631,20.488-5.489C363.188,96.821,365.645,105.994,361.504,113.167z M188.484,382.851
			c-14.348-8.284-32.697-3.368-40.98,10.98c-8.285,14.349-3.367,32.696,10.98,40.981c14.35,8.283,32.697,3.367,40.98-10.981
			C207.75,409.482,202.834,391.135,188.484,382.851z M421.33,184.888c-8.368,4.831-19.07,1.965-23.901-6.404
			c-4.832-8.368-1.965-19.07,6.404-23.902c8.368-4.831,19.069-1.964,23.9,6.405C432.566,169.354,429.699,180.056,421.33,184.888z
			 M135.399,329.767c-8.285-14.35-26.633-19.266-40.982-10.982c-14.348,8.285-19.264,26.633-10.979,40.982
			c8.284,14.348,26.632,19.264,40.981,10.98C138.767,362.462,143.683,344.114,135.399,329.767z M436.031,277.249
			c-11.044,0-20-8.953-20-19.999c0-11.045,8.955-20.001,20.001-20.001c11.044,0,19.999,8.955,19.999,20.002
			C456.031,268.295,447.078,277.249,436.031,277.249z M115.97,257.251c-0.001-16.57-13.433-30.001-30.001-30.002
			c-16.568,0.001-29.999,13.432-30,30.002c0.001,16.566,13.433,29.998,30.001,30C102.538,287.249,115.969,273.817,115.97,257.251z
			 M401.333,364.248c-10.759-6.212-14.446-19.97-8.234-30.73c6.212-10.759,19.971-14.446,30.731-8.233
			c10.759,6.211,14.445,19.971,8.232,30.73C425.852,366.774,412.094,370.46,401.333,364.248z M135.398,184.736
			c8.285-14.352,3.368-32.698-10.98-40.983c-14.349-8.283-32.695-3.367-40.981,10.982c-8.282,14.348-3.366,32.696,10.981,40.981
			C108.768,204,127.115,199.082,135.398,184.736z M326.869,421.328c-6.902-11.953-2.807-27.242,9.148-34.145
			s27.243-2.806,34.146,9.149c6.902,11.954,2.806,27.243-9.15,34.145C349.059,437.381,333.771,433.284,326.869,421.328z
			 M188.482,131.649c14.352-8.286,19.266-26.633,10.982-40.982c-8.285-14.348-26.631-19.264-40.982-10.98
			c-14.346,8.285-19.264,26.633-10.98,40.982C155.787,135.017,174.137,139.932,188.482,131.649z"/>
		</svg>
    </div>
</div>
<script>
    /******全局变量*****/
    var active = {
        parsent: 'qicao',
        parsent2: '#a'

    };
    var data_group = {};
    $(function () {
        //建立socket连接
        var sock;
        if ('WebSocket' in window) {
            sock = new WebSocket("<%=wsPath%>serverSocke1t.do");
        } else if ('MozWebSocket' in window) {
            sock = new MozWebSocket("<%=wsPath%>serverSocket.do");
        } else {
            sock = new SockJS("<%=basePath%>sockjs/serverSocket.do");
        }
        sock.onopen = function (e) {
            /* console.log(e); */
        };
        sock.onmessage = function (e) {
            /* console.log(e) ; */

            if (e.data == 'farewellNumber') {
                var farewellNumber = Number($('#farewellNumber').text()) + 1;
                $('#farewellNumber').text(farewellNumber);
            } else if (e.data == 'mourningNumber') {
                var mourningNumber = Number($('#mourningNumber').text()) + 1;
                $('#mourningNumber').text(mourningNumber);
            } else if (e.data == 'funeralNumber') {
                var funeralNumber = Number($('#funeralNumber').text()) + 1;
                $('#funeralNumber').text(funeralNumber);
            } else if (e.data == 'autoFreezerNumber') {
                var autoFreezerNumber = Number($('#autoFreezerNumber').text()) + 1;
                $('#autoFreezerNumber').text(autoFreezerNumber);
            } else if (e.data == 'freezerThawNumber') {
                var freezerThawNumber = Number($('#freezerThawNumber').text()) + 1;
                $('#freezerThawNumber').text(freezerThawNumber);
            } else if (e.data == 'carNumber') {
                var carNumber = Number($('#carNumber').text()) + 1;
                $('#carNumber').text(carNumber);
            }
        };
        sock.onerror = function (e) {
            console.log(e);
        };
        sock.onclose = function (e) {
            console.log(e);
        }
    });
</script>

<script>
    $(function () {
        var clientHeight = document.documentElement.clientHeight;
        $(".main-sidebar").css("height", clientHeight);
        $(".sidebar-left").css("height", clientHeight);
        $(".sidebar-menu li").mouseenter(function () {
            var html = $(this).html();
            if ($("body").hasClass("sidebar-collapse")) {
                $(".sidebar-left").css("display", "block");
            }
            $(".sidebar-sroll").html(html);
            var ofset = $(this).offset();
            var scrollTop = $(window).scrollTop();
            $(".arrow").css("top", ofset.top + 15 - scrollTop).css("left", 45);
        });

        $(".sidebar-left").mouseleave(function () {
            $(".sidebar-left").css("display", "none");
        });

    });


    $('.content-wrapper').css({
        'minHeight': $(window).height() - $('.main-footer').outerHeight()
    })
</script>

<script>
    $('.scroll-wrapper').scroll(function (e) {
        var ev = e || event
        ev.stopPropagation()
    })
</script>
</body>
</html>