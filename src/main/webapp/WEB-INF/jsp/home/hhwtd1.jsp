<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style type="text/css">
	.title{
		height: 30px;
		float: left;
		font-size: 16px;
		color: #29b6f6;
		font-family: Serif ;
		padding-left: 10px;
	}
	.btn-info{
		width: 10%;
		float:right;	
		text-align: right;
	}
	.form-control{
	border-radius:6px;
	width: 60%;
	}
	.base1{
		width: 80px;
		height: 80px;
		background: #e84e40;
		border-radius:6px;
		float: left;
		margin-left: 35%;
	}
	.base2{
		width: 80px;
		height: 80px;
		background: #ab47bc;
		border-radius:6px;
		float: left;
		margin-left:2%;
	}
	.base3{
		width: 80px;
		height: 80px;
		background: #ff7043;
		border-radius:6px;
		float: left;
		margin-left: 2%;
	}
	.base4{
		width: 80px;
		height: 80px;
		background: #8ed938;
		border-radius:6px;
		float: left;
		margin-left: 2%;
	}
	.base5{
		width: 80px;
		height: 80px;
		background: #29b6f6;
		border-radius:6px;
		float: left;
		margin-left: 2%;
	}
	.base6{
		width: 80px;
		height: 80px;
		background: #5c6bc0;
		border-radius:6px;
		float: left;
		margin-left:2%;
	}
	.base7{
		width: 80px;
		height: 80px;
		background: #07bd5f;
		border-radius:6px;
		float: left;
		margin-left: 2%;
	}
	.glyphicon. glyphicon-search{
	   width: 50px;
	   height: 50px;
	}
	.row1{
		text-align: center;;
	}
</style>
</head>

<body>
<div>
<div class="row" style="padding: 15px 25px 0 30px;">
	<div style="height: 20px;width: 6px;background-color: #29b6f6;float: left;">
	</div>
	<div class="title">火化委托单</div>
</div>
<section class="content">
<div class="row" style="margin-top:-1%">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
            	
		<div class="base1">
                    <li>
                    <span class="glyphicon glyphicon-search"></span>
                    <span class="glyphicon-class">查看</span>
                  </li>
         </div>      
		<div class="base2"></div>
		<div class="base3"></div>
		<div class="base4"></div>
		<div class="base5"></div>
		<div class="base6"></div>
		<div class="base7"></div>
         	 
            </div>
          </div>
     	</div>
</div>
</section>

<section class="content">
      <div class="row" style="margin-top: -14%">
        <div class="col-md-12">
        </div>
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class=""><a href="#activity" data-toggle="tab" aria-expanded="false">基础信息</a></li>
              <li class=""><a href="#deadinfo" data-toggle="tab" aria-expanded="false">业务信息</a></li>
              <li class=""><a href="#settings" data-toggle="tab" aria-expanded="true">减免信息</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="activity">
              		<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px;font-size: 18px">死者信息</div>
              		<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px"></div>
	              	<div class="box box-primary">
	           				<div class="box-body">	    
               					<div class="form-inline">
               						<div class="form-group">
								    <label for="exampleInputName2">死者姓名&nbsp;:&nbsp;</label>
								    <input type="text" class="form-control" id="exampleInputName2" placeholder="死者姓名">
								  </div>
						   <div class="form-group" style="margin-left: 28%">
						    <label for="exampleInputEmail2">死者地址&nbsp;:&nbsp;</label>
						    <input style="width: 60%" type="email" class="form-control" id="exampleInputEmail2" placeholder="死者地址">
  						 </div>
                  </div>
          </div>
         <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2">证件类型&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 50%">
							  <option>1</option>
							  <option>2</option>
							  <option>3</option>
							  <option>4</option>
							  <option>5</option>
							</select>
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 20%">
						    <label for="exampleInputName2">接尸地址&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 75%">
							  <option>1</option>
							  <option>2</option>
							  <option>3</option>
							  <option>4</option>
							  <option>5</option>
							</select>
						  </div>		  
                  </div>
          </div>
          <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2">证件号码&nbsp;:&nbsp;</label>
						   <input type="text" class="form-control" id="exampleInputName2" placeholder="证件号码">
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 20%">
						    <label for="exampleInputName2">死亡类型&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 50%">
							  <option>1</option>
							  <option>2</option>
							  <option>3</option>
							  <option>4</option>
							  <option>5</option>
							</select>
						  </div>		  
                  </div>
          </div>
          <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2">死者年龄&nbsp;:&nbsp;</label>
						   <input type="text" class="form-control" id="exampleInputName2" placeholder="死者年龄">
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 20%">
						    <label for="exampleInputName2">死亡原因&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 50%">
							  <option>1</option>
							  <option>2</option>
							  <option>3</option>
							  <option>4</option>
							  <option>5</option>
							</select>
						  </div>		  
                  </div>
          </div>
           <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2">死亡性别&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 50%">
							  <option>男</option>
							  <option>女</option>
							</select>
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 20%">
						    <label for="exampleInputName2">死亡日期&nbsp;:&nbsp;</label>
						   <input style="width: 50%" type="email" class="form-control" id="exampleInputEmail2" placeholder="死者地址">
						  </div>		  
                  </div>
          </div>
          <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2">死者户籍&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 50%">
							  <option>杭州</option>
							  <option>温州</option>
							</select>
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 20%">
						    <label for="exampleInputName2">证明单位&nbsp;:&nbsp;</label>
						   <input style="width: 30%" type="email" class="form-control" id="exampleInputEmail2" placeholder="证明单位">
						   <input style="width: 45%" type="email" class="form-control" id="exampleInputEmail2" placeholder="证明单位">
						  </div>		  
                  </div>
          </div>
          <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2">户籍街道&nbsp;:&nbsp;</label>
						   <input type="text" class="form-control" id="exampleInputName2" placeholder="证件号码">
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 20%">
						    <label for="exampleInputName2">死亡证明&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 50%">
							  <option>1</option>
							  <option>2</option>
							  <option>3</option>
							  <option>4</option>
							  <option>5</option>
							</select>
						  </div>		  
                  </div>
          </div> 
          <div class="box box-info">
          	<div style="color: #29b6f6;margin-left: 10px;margin-top: 20px;margin-bottom: 10px;font-size: 18px">家属/经办人信息</div>
          	<br>
          </div>  
          <div class="box-body" >	    
               		<div class="form-inline" >
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2" style="margin-left: 10%">姓名&nbsp;:&nbsp;</label>
						   <input type="text" class="form-control" id="exampleInputName2" placeholder="证件号码">
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 18%">
						    <label for="exampleInputName2">与死者关系&nbsp;:&nbsp;</label>
						    <select class="form-control" style="width: 50%">
							  <option>1</option>
							  <option>2</option>
							  <option>3</option>
							  <option>4</option>
							  <option>5</option>
							</select>
						  </div>		  
                  </div> 
          </div> 
          <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2" style="margin-left: 0px">联系电话&nbsp;:&nbsp;</label>
						   <input type="text" class="form-control" id="exampleInputName2" placeholder="联系电话">
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 22%">
						    <label for="exampleInputName2">住址&nbsp;:&nbsp;</label>
						    <input type="text" class="form-control" id="exampleInputName2" placeholder="住址">
						  </div>		  
                  </div> 
          </div>  
         <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2" style="margin-left: 10%">单位&nbsp;:&nbsp;</label>
						   <input type="text" class="form-control" id="exampleInputName2" placeholder="单位">
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 17%">
						    <label for="exampleInputName2">业务审核状态&nbsp;:&nbsp;</label>
						   <font style="color: red">未审核</font>
						  </div>		  
                  </div> 
          </div>       			
	    </div>        
     </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane" id="deadinfo">
               		<div class="form-inline">
               			<div class="box box-primary">
	           				<div class="box-body">	    
	               				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px; font-size: 18px">业务调度信息</div>
	              				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px"></div>	
                            </div>		  
                      	</div>
                      	<div class="box-body" style="margin-top: -2%">	    
	               				  <div class="form-group" >
								    <label for="exampleInputName2">到馆时间&nbsp;:&nbsp;</label>
								    <input type="text" class="form-control" id="exampleInputName2" placeholder="到馆时间">
								  </div>
								  <div class="form-group" style="margin-left: 28%">
								    <label for="exampleInputEmail2">告别厅号&nbsp;:&nbsp;</label>
								    <input style="width: 60%" type="email" class="form-control" id="exampleInputEmail2" placeholder="告别厅号">
								    <label for="exampleInputEmail2"><font color="red">选择</font></label>
		  						 </div>	
                       </div>
                       <div class="box-body">	    
	               				  <div class="form-group" style="width: 30%">
								    <label for="exampleInputName2">接尸单位&nbsp;:&nbsp;</label>
								    <select class="form-control" style="width: 50%">
									  <option>1</option>
									  <option>2</option>
									</select>
								  </div>
								  <div class="form-group" style="width:30%;margin-left: 20%">
								    <label for="exampleInputName2">告别时间&nbsp;:&nbsp;</label>
								   <input style="width: 50%" type="email" class="form-control" id="exampleInputEmail2" placeholder="告别时间">
								  </div>
                       </div>
                       <div class="box-body">
                       			<div class="form-group" style="width: 30%">
								    <label for="exampleInputName2" style="margin-left: 0%">冷藏柜号&nbsp;:&nbsp;</label>
								   <input type="text" class="form-control" id="exampleInputName2" placeholder="冷藏柜号">
								   <label for="exampleInputEmail2"><font color="red">选择</font></label>
								</div>
							  <div class="form-group" style="width:30%;margin-left: 18.5%">
							    <label for="exampleInputName2">火化炉类型&nbsp;:&nbsp;</label>
							    <select class="form-control" style="width: 50%">
								  <option>1</option>
								  <option>2</option>
								  <option>3</option>
								  <option>4</option>
								  <option>5</option>
								</select>
							  </div>	
	                  </div>
	                  <div class="box-body">
                       			<div class="form-group" style="width: 30%">
								    <label for="exampleInputName2" style="margin-left: 0%">守灵室号&nbsp;:&nbsp;</label>
								   <input type="text" class="form-control" id="exampleInputName2" placeholder="守灵室号">
								   <label for="exampleInputEmail2"><font color="red">选择</font></label>
								</div>
							  <div class="form-group" style="width:30%;margin-left: 17%">
							     <label for="exampleInputName2">火化预约时间&nbsp;:&nbsp;</label>
								 <input style="width: 50%" type="email" class="form-control" id="exampleInputEmail2" placeholder="火化预约时间">
							  </div>	
	                  </div>
	                   <div class="box-body">
                       			<div class="form-group" style="width: 30%">
								    <label for="exampleInputName2" style="margin-left: 0%">守灵时间&nbsp;:&nbsp;</label>
								   <input type="text" class="form-control" id="exampleInputName2" placeholder="开始守灵时间">
								</div>
							  <div class="form-group" style="width:30%;margin-left:-7%">
							     <label for="exampleInputName2">至&nbsp;&nbsp;</label>
								 <input style="width: 50%" type="email" class="form-control" id="exampleInputEmail2" placeholder="结束守灵时间">
							  </div>	
	                  </div>
	                  <div class="box box-info">
	           				<div class="box-body">	    
	               				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px; font-size: 18px">车辆调度信息</div>
	              				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px"></div>	
                            </div>		  
                      </div>
                      <div class="box-body" style="margin-left: 80%;margin-top: -2%">
                       		 <button type="button" class="btn btn-primary">添加</button>
						     <button type="button" class="btn btn-primary">删除</button>		
	                  </div>
			                  <div class="row"><div class="col-sm-12">
		              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
		                <thead>
		                <tr role="row" style="background:#ededed;text-align: center;">
		                <th class="row1">全选<input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></th>
		                <th class="row1">运输类型</th>
		                <th class="row1">车辆类型</th>
		                <th class="row1">死者性别</th>
		                <th class="row1">备注</th>
		                </tr>
		                </thead>
		                <tbody>   
		                <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1">
		                  		<select class="form-control" style="width: 100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1">
		                  		<select class="form-control" style="width: 100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1">
		                  	<input style="width: 80%" type="text" class="form-control" id="exampleInputEmail2" placeholder="运送时间">
		                  </td>
		                  <td class="row1">
		                  	<input style="width: 100%" type="email" class="form-control" id="exampleInputEmail2" placeholder="单行输入">
		                  </td>
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		               </tr>
		               </tbody>
		              </table>
		              </div></div>
		              
		              
		             <div class="box box-primary">
	           				<div class="box-body">	    
	               				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px; font-size: 18px">挂账信息</div>
	              				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px"></div>	
                            </div>		  
                     </div>
                     <div class="box-body" style="margin-top: -2%;text-align: right;">
                       		 <button type="button" class="btn btn-primary">撤销</button>	
	                  </div>
	                  
	                   <div class="row"><div class="col-sm-12">
		              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
		                <thead>
		                <tr role="row" style="background:#ededed;text-align: center;">
		                <th class="row1">全选<input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></th>
		                <th class="row1">名称</th>
		                <th class="row1">单位(元)</th>
		                <th class="row1">单位</th>
		                <th class="row1">数量</th>
		                <th class="row1">方向</th>
		                <th class="row1">小计</th>
		                <th class="row1">备注</th>
		                </tr>
		                </thead>
		                <tbody>   
		                <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1" style="width: 30%"></td>
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1" style="width: 30%"></td>
		               </tr>
		               </tbody>
		              </table>
		              </div></div>
	                  
	                  
	                  <div class="box box-info">
	           				<div class="box-body">	    
	               				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px; font-size: 18px">服务项目信息</div>
	              				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px"></div>	
                            </div>		  
                      </div>
	                  <div class="box-body" style="margin-left: 80%;margin-top: -2%">
                       		 <button type="button" class="btn btn-primary">添加</button>
						     <button type="button" class="btn btn-primary">删除</button>		
	                  </div>
			                  <div class="row"><div class="col-sm-12">
		              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
		                <thead>
		                <tr role="row" style="background:#ededed;text-align: center;">
		                <th class="row1">全选<input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></th>
		                <th class="row1">名称</th>
		                <th class="row1">单价(元)</th>
		                <th class="row1">类型</th>
		                <th class="row1">数量</th>
		                <th class="row1">挂账</th>
		                <th class="row1">小计</th>
		                <th class="row1">备注</th>
		                
		                </tr>
		                </thead>
		                <tbody>   
		                <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1">
		                  		<select class="form-control" style="width: 100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1" style="width: 15%">
		                  		<input style="width:100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="单价">
		                  </td>
		                  
		                  <td class="row1">
		                  	<select class="form-control" style="width:100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1" style="width: 12%">
		                  	<input style="width: 100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="数量">
		                  </td>
		                  <td class="row1">
		                  		<select class="form-control" style="width:100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1" style="width: 8%">
		                  	<input style="width: 100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="小计">
		                  </td>
		                  <td class="row1">
		                  	<input style="width: 100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="但行输入">
		                  </td>
		              
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		             
		               </tr>
		               </tbody>
		              </table>
		              </div></div>
		              <!-- 丧葬用品的信息 -->
	                   <div class="box box-info">
	           				<div class="box-body">	    
	               				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px; font-size: 18px">服务项目信息</div>
	              				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px"></div>	
                            </div>		  
                      </div>
	                  <div class="box-body" style="margin-left: 80%;margin-top: -2%">
                       		 <button type="button" class="btn btn-primary">添加</button>
						     <button type="button" class="btn btn-primary">删除</button>		
	                  </div>
			                  <div class="row"><div class="col-sm-12">
		              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
		                <thead>
		                <tr role="row" style="background:#ededed;text-align: center;">
		                <th class="row1">全选<input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></th>
		                <th class="row1">名称</th>
		                <th class="row1">单价(元)</th>
		                <th class="row1">类型</th>
		                <th class="row1">数量</th>
		                <th class="row1">挂账</th>
		                <th class="row1">小计</th>
		                <th class="row1">备注</th>
		                
		                </tr>
		                </thead>
		                <tbody>   
		                <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1">
		                  		<select class="form-control" style="width: 100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1" style="width: 15%">
		                  		<input style="width:100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="单价">
		                  </td>
		                  
		                  <td class="row1">
		                  	<select class="form-control" style="width:100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1" style="width: 12%">
		                  	<input style="width: 100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="数量">
		                  </td>
		                  <td class="row1">
		                  		<select class="form-control" style="width:100%">
										  <option>1</option>
										  <option>2</option>
										  <option>3</option>
										  <option>4</option>
										  <option>5</option>
								</select>
		                  </td>
		                  <td class="row1" style="width: 8%">
		                  	<input style="width: 100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="小计">
		                  </td>
		                  <td class="row1">
		                  	<input style="width: 100%" type="text" class="form-control" id="exampleInputEmail2" placeholder="但行输入">
		                  </td>
		              
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		               </tr>
		               </tbody>
		              </table>
		              </div></div>
	                  <div class="box-body">	    
               		<div class="form-inline">
               			<label>备注:</label>
               			<div style="width: 100px;height: 100px;border: 1px solid red;"></div>
                  </div>
          </div> 
         		</div>  	              	
             </div>
          
              
              
              
              
              <div class="tab-pane active" id="settings">
                <div class="form-line">
                  <div class="form-group">
                     <div class="box box-info">
	           				<div class="box-body">	    
	               				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px; font-size: 18px">减免项目申免</div>
	              				<div style="color: #29b6f6;margin-left: 10px;margin-top: 10px"></div>	
                            </div>		  
                      </div>
                  </div>
                  <div class="form-group">
	               		<label>免费对象类别&nbsp;：</label>
	               		<input type="checkbox" style="margin-left: 5px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">市区市民</label>
                     	<input type="checkbox" style="margin-left: 15px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">在温院校学生</label>
                     	<input type="checkbox" style="margin-left: 15px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">驻温部队现役军人</label>
                     	<input type="checkbox" style="margin-left: 15px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">外来务工人员</label>	
                  </div>
                  <div class="form-group">
	               		<label>重点救助对象类别&nbsp;：</label>
	               		<input type="checkbox" style="margin-left: 5px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">城乡低保，困难家庭成员</label>
                     	<input type="checkbox" style="margin-left: 15px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">特困难残疾人</label>
                     	<input type="checkbox" style="margin-left: 15px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">重点优抚对象</label>
                     	<input type="checkbox" style="margin-left: 15px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">"三老"人员</label>	
                     	<input type="checkbox" style="margin-left: 15px;margin-bottom: 5px ">	
                     	<label style="margin-left: 15px">市级以上劳模</label>	
                  </div>
                 <div class="box-body" style="margin-top: -2%;text-align: right;">
                       		 <button type="button" class="btn btn-primary">撤销</button>	
	                  </div>
	                  
	                   <div class="row"><div class="col-sm-12">
		              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
		                <thead>
		                <tr role="row" style="background:#ededed;text-align: center;">
		                <th class="row1">全选<input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></th>
		                <th class="row1">名称</th>
		                <th class="row1">单位(元)</th>
		                <th class="row1">单位</th>
		                <th class="row1">数量</th>
		                <th class="row1">方向</th>
		                <th class="row1">小计</th>
		                <th class="row1">备注</th>
		                </tr>
		                </thead>
		                <tbody>   
		                <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1" style="width: 30%"></td>
		               </tr>
		               <tr role="row" class="odd" >
		                  <td class="row1"><input type="checkbox" style="margin-left: 5px;margin-bottom: 5px "></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1"></td>
		                  <td class="row1" style="width: 30%"></td>
		               </tr>
		               </tbody>
		              </table>
		              </div></div>
                 
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<section class="content">
<div class="row" style="margin-top:-3.8%">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
            	<div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2" style="margin-left: 10%">引导员&nbsp;:&nbsp;</label>
						   <font>张三</font>
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 5%">
						    <label for="exampleInputName2">代办员&nbsp;:&nbsp;</label>
						  </div>		  
                  </div> 
         	 </div>  
         	 <div class="box-body">	    
               		<div class="form-inline">
               			<div class="form-group" style="width: 30%">
						    <label for="exampleInputName2" style="margin-left:10%">编号&nbsp;:&nbsp;</label>
						   <input type="text" class="form-control" id="exampleInputName2" placeholder="编号">
						  </div>
						  <div class="form-group" style="width:30%;margin-left: 21%">
						    <label for="exampleInputName2">必须填&nbsp;:&nbsp;</label>
						     <button type="button" class="btn btn-primary">保存</button>
						     <button type="button" class="btn btn-primary">重置</button>
						   	 <button type="button" class="btn btn-primary">返回</button>
						  </div>		  
                  </div> 
          </div>   
            </div>
          </div>
     	</div>
</div>
</section>
</div>
</body>
</html>