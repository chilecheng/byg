<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method}" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					出车预约登记
				</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">姓名:</td>
						<td><input type="text" class="required integer form-control" id="name" name="name" value=""></td>
						<td>&nbsp;</td>
						<td width="100px">运输类型:</td>
						<td><select class='list_select' name='type' style="width:150px"></select></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">联系方式:</td>
						<td><input type="text" name="userName" class="required form-control" id="phone" value=""></td>
						<td>&nbsp;</td>
						<td width="100px">车辆类型:</td>
						<td><select class='list_select' name='carType'  style="width:150px"></select></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">出车时间:</td>
						<td><input type="text" data-provide="datetimepicker" onchange="" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="beginDate" name="date" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i></td>
					
					</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					<tr>
						<td width="100px">接尸地址:</td>
						<td>
							<select class="list_select" style="width:150px" name="addr"  >
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">备注:</td>
						<td colspan='4'><textarea type="text" name="more" rows='4' class=" form-control" value=""></textarea></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>	