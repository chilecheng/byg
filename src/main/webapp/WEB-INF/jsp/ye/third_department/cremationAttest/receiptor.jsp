
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化证明打印</div>
</section>
<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input name="cid" value="${id}" type="hidden">
	<input name="cardCode" value="${cardCode}" type="hidden" />
	<section class="content">
	
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
						<div class="box-body" style='padding:0px;'>
						<div style='opacity:0;'>
									<object id="plugin0" type="application/lagen-plugin" width="0" height="0">
									<param name="onload" value="pluginLoaded" />
									</object>
									<object id="plugin1" type="application/x-lathumbplugin" width="0" height="0">
 										    <param name="onload" value="pluginLoaded" />
 									</object> 
									<br />
									</div>
									<div class='hide'>
										当前设备：<select id="curDev" style="width: 90px" name="selDev"
																			onchange="changedev()"></select>
										当前分辨率：<select id="curRes" style="width: 90px" name="curRes"
																			onchange="changeres()"></select>
										颜色：<select id="curColor" style="width: 90px" name="curRes"
																			onchange="changeclr()"></select>
										拍照模式：<select id="capMode" style="width: 90px" name="curRes"
																			onchange="changemode()"></select>
									
										<input id="rotatecrop" checked type="checkbox" value="" onclick="RotateCrop(this)" />纠偏裁边
										<input id="drawrect" type="checkbox" value="" onclick="setmousemode(this)" />框选
										<br><br>    
									<input   TYPE="button"   VALUE="开始预览"   onClick="start_preview()"> 
									<input   TYPE="button"   VALUE="停止预览"   onClick="stop_preview()">
									<input   TYPE="button"   VALUE="左转90度"   onClick="rotleft()">
									<input   TYPE="button"   VALUE="右转90度"   onClick="rotright()">
									<input   TYPE="button"   VALUE="视频属性"   onClick="showprop()">
									<input   TYPE="button"   VALUE="条码识别"   onClick="readbarcode()">
									<input   TYPE="button"   VALUE="画面恢复"   onClick="resetvideo()">
									<input   TYPE="button"   VALUE="生成PDF"   onClick="makepdf()">
									<input   TYPE="button"   id=recvideo VALUE="开始录像"   onClick="startrecord()">
									<input   TYPE="button"   VALUE="拍照"   onClick="capture()">
									<input   TYPE="button"   VALUE="拍照为Base64"   onClick="capturebase64()"> <br><br>
									<input   TYPE="button"   id=autocap VALUE="开始智能连拍"   onClick="startautocap()">
									<input   TYPE="button"   id=tmcap VALUE="开始定时连拍"   onClick="starttmcap()">
								</div>
							<P class='col-md-12 p border-B'>领取人信息</P>
							<div class='col-md-12 border-B padding-B nomargin-T'>
								<div class='row'>
									<div class='col-md-12'>
										<label style='margin-left:30px'>读取身份证信息:</label>
										<button type='button' class='btn btn-default' onclick='readidcardL()'>读取</button>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>姓名：</label>
										<input class='list_input input-dialog' id="receiptor" name="receiptor" value="${printRecord.takeName }" >
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>身份证号：</label>
										<input class='list_input input-dialog' id="receiptorId" name="receiptorId" value="${printRecord.takeCode }" >
									</div>
								</div>
							</div>
							
							<p class='col-md-12 p border-B'>死者信息</p>
							<div class='col-md-12 border-B padding-B nomargin-T'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>姓名：</label>
										<span>${dname}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>身份证号：</label>
										<span>${certificateCode}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>性别：</label>
										<span>${sex}</span>
									</div>
								</div>
							</div>
						</div>
						
				</div>
				<!-- /.col -->
				<div class='box' style='padding:10px;'>
					<div class='row'>
						<div class='col-md-12 btns-dialog'>
						    <a data-name='sure' class='btn btn-info btn-margin'>保存</a>
							<a href="fireAttestPrint.do?method=save&id=${id}&cardCode=${cardCode}&printId=${printRecord.id}" target="homeTab" rel="myModal" id="sure"  class="hide" role="button">保存</a>
							<button type='reset' class='btn btn-margin btn-color-9E8273'>重置</button>
							<a href="fireAttestPrint.do?method=back&pageFlag=${pageFlag}&cardCode=${cardCode}" target="homeTab" rel="myModal" id="return" class="btn btn-default btn-margin" role="button">返回</a>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
</form>

<script>
	(function(){
		$('[data-name="sure"]').click(function (){
			var href=$('#sure').attr('href')+'&'+$('#receiptor').attr('id')+'='+$('#receiptor').val()
			         +'&'+$('#receiptorId').attr('id')+'='+$('#receiptorId').val();	         
			//alert(href);
			$('#sure').attr('href',href);
			$('#sure').click();
		});

	})()
</script>
<script type="text/javascript" src='js/carId.js'></script>