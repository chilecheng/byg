<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化证明打印</div>
</section>
<link rel="stylesheet" type="text/css" media="print" href="css/print2.css"/> 
<script type="text/javascript">
function loginfo(ele)
{
	//alert(1);
	$('[data-even="change"]').click();
}

function changeStateAndDestoryCard(ele)
{	
	
	$("#isornot").html("已打印确认单").css('color','#9CCC65');
	$('#print').attr('disabled','disabled');
	$('#receiveLogin').attr('disabled','disabled');
	/* $('#print').attr('readonly','readonly');
	$('#receiveLogin').attr('readonly','readonly'); */
	var href=$(ele).attr('data-href');
	$.ajax({
		url:href,
		async:false,
		type:"POST",
		dataType:'json',
		success:function(json){
			toastr["success"](json.info);
			initTab();
		},
		error:function(XMLHttpRequest, textStatus, errorThrown) {  
			if(XMLHttpRequest.status==500){
				toastr["error"]("程序内部出现错误"); 
			}else if(XMLHttpRequest.status==404){
				toastr["error"]("页面不存在"); 
			}else{
				toastr["error"](XMLHttpRequest.status); 
			}
		}
	});
}
</script>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<small class="btns-buy">
					<button data-href="fireAttestPrint.do?method=destoryCard&id=${id}" onclick='changeStateAndDestoryCard(this);' class="btn btn-success" target="homeTab" rel="myModal"  id="print">火化验证销卡</button>
					<button onclick="loginfo(this);" target="homeTab" rel="myModal" checkOne="true"  class="btn btn-warning" id="receiveLogin">领取人录入</button>
					<a href="javascript:fireprint()"  class="btn btn-info">火化证明打印</a>
					<a class='hide' data-even='change' target="homeTab" href='fireAttestPrint.do?method=receiveLogin&id=${id}&sex=${sex}&dname=${dname}&certificateCode=${certificateCode}&cardCode=${cardCode}&pageFlag=2'></a>
					</small>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="" type="hidden" />
					<input type="hidden" name="id1" class="id1"  id="id1" value="${id}">
					<input name="certificateCode" id="certificateCode" value="${certificateCode}" type="hidden" />
					<div class="box-body">
						
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="id" /></th>
											
											<th>死者姓名</th>
											<th>死者性别</th>
											<th>死者年龄</th>
											<th>火化时间</th>
											<th>业务状态</th>
											<th>打印状态</th>
										</tr>
									</thead>
									<tbody>

											<tr role="row" width="10">
												<td><input type="checkbox" name="id" class="id"  id="id" value="${id}"></td>
						    					<td>
						    						<a href='fireAttestPrint.do?method=showInfo&sex=${sex}&age=${age}&certificateCode=${certificateCode}&dname=${dname}&id=${id}' target='dialog' rel="myModal" id="read">
						    							<font color="blue">${dname}</font>
						    						</a>
						    					</td>
						    					<td>${sex}</td>
						    					<td>${age}</td>
						    					<td><fmt:formatDate value="${begin_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
						    					<td class='changeColor state1'>${business_state}</td>
						    							<c:if test="${schedulFlag==1 }">
																<td style='color:#e84e40;' class='state1'>已到馆未收费</td>
													  		 </c:if>
									    					 <c:if test="${schedulFlag==2 }">
																<td style='color:#fea625;' class='state1'>收费结清</td>
													  		 </c:if>
													  		 <c:if test="${schedulFlag==3 }">
																<td style='color:#29b6f6;' class='state1'>已火化未打印</td>
													  		 </c:if>
													  		 <c:if test="${schedulFlag==4 }">
																<td style='color:#9ccc65;' class='state1'>已打印火化证明</td>
													  		 </c:if>	
						    					</tr>
                                           </tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- 火化证明书打印 -->
						<div class='hide'>
							<!--startprint0--> 
							<p class='first_line'>  <span class='dName_1'>${dname }</span>
								<span class='dsex_1'>${sex }</span>
								<span class='dID_1'>${certificateCode }</span>
							</p>
							<p class='date_line_1'><span class='year_1'><fmt:formatDate value="${begin_time}" pattern="yyyy"/></span>
							<span class='month_1'><fmt:formatDate value="${begin_time}" pattern="MM"/></span>
							<span class='day_1'><fmt:formatDate value="${begin_time}" pattern="dd"/></span></p>
							<p class='second_line'><span class='username_1'>${userName }</span></p>
							<p><span class='nowmonth_1'><fmt:formatDate value="${now}" pattern="MM"/></span>
							<span class='nowday_1'><fmt:formatDate value="${now}" pattern="dd"/></span></p>
							<p><span class='idcode'>${receiptorId }</span></p>
							<p class='third_line'>  <span class='dName_2'>${dname }</span>
								<span class='dsex_2'>${sex }</span>
								<span class='dID_2'>${certificateCode }</span>
							</p>
							<p class='date_line_2'><span class='year_2'><fmt:formatDate value="${begin_time}" pattern="yyyy"/></span>
							<span class='month_2'><fmt:formatDate value="${begin_time}" pattern="MM"/></span>
							<span class='day_2'><fmt:formatDate value="${begin_time}" pattern="dd"/></span></p>
							<p class='forth_line'><span class='username_2'>${userName }</span></p>
							<p class='last_line'><span class='nowmonth_2'><fmt:formatDate value="${now}" pattern="MM"/></span>
							<span class='nowday_2'><fmt:formatDate value="${now}" pattern="dd"/></span></p>
							<!--endprint0-->
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>

</section>
<script>
    
	(function(){
		$('.changeColor').each(function(){
			var txt =$(this).text();
			switch(txt){
				case '火化完成':
					$(this).css('color','#9CCC65');
					break;
				case '已打印确认单':
					$(this).css('color','#9CCC65');
					break;
				case '未打印确认单':
					$(this).css('color','#e84e40');
					break;
				case '火化未完成':
					$(this).css('color','#e84e40');
					break;
			}
		})
	})()
	<!--打印-->
	function fireprint(){
	var bdhtml = window.document.body.innerHTML;
	var beginprint0 = "<!--startprint0-->";
	var endprint0 = "<!--endprint0-->";
    var beginprint1 = "<!--startprint1-->";
    var endprint1 = "<!--endprint1-->";    
    
    
    var prnhtml0 = bdhtml.substr(bdhtml.indexOf(beginprint0) + 19);
    var prnhtml1 = bdhtml.substr(bdhtml.indexOf(beginprint1) + 19);
   
    
     prnhtml0 = prnhtml0.substring(0, prnhtml0.indexOf(endprint0));
     prnhtml1 = prnhtml1.substring(0, prnhtml1.indexOf(endprint1));
     
     
     window.document.body.innerHTML = prnhtml0;
     
     
     window.print();
     window.document.body.innerHTML = bdhtml;
     
		$.ajax({
			type : 'POST',
			data: {id:$("#id").val() },
			url : "fireAttestPrint.do?method=upSchedual",
			dataType : "json", 
		
		 	success : function(json) {
					 initTab();		
	 		}, 
		 
		})
     
     
     
     initTab();
}

	
	
	
	
	
</script>