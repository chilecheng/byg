
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="" rel="myModal">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
							<P class='col-md-12 p border-B'>领取人信息</P>
							<div class='col-md-12 border-B padding-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>姓名：</label>
										<span>${printRecord.takeName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>身份证号：</label>
										<span>${printRecord.takeCode}</span>
									</div>
								</div>
							</div>
							<p class='col-md-12 p border-B height-align'>死者信息</p>
							<div class='col-md-12 border-B padding-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>姓名：</label>
										<span>${dname}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>身份证号：</label>
										<span>${certificateCode}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>性别：</label>
										<span>${sex}</span>
									</div>
								</div>
							</div>
						<div class="col-md-12 btns-dialog">
							
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>