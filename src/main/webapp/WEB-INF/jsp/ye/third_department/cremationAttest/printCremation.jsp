<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化证明打印</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<small class="btns-buy">
					
					<a href="fireAttestPrint.do?method=receiveLogin&id=${id}&sex=${sex}&dname=${dname}&certificateCode=${certificateCode}&cardCode=${cardCode}&pageFlag=1" target="homeTab" rel="myModal" checkOne="true"  class="btn btn-warning" role="button">领取人录入</a>
					</small>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="" type="hidden" />
					<input name="cardCode" value="${cardCode}" type="hidden" />
					<input name="certificateCode" id="certificateCode" value="${certificateCode}" type="hidden" />
					<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="id" readidcardD('name')/></th>
											
											<th>死者姓名</th>
											<th>死者性别</th>
											<th>死者年龄</th>
											<th>火化时间</th>
											<th>业务状态</th>
											<th>打印状态</th>
										</tr>
									</thead>
									<tbody>
											<tr role="row" width="10">
												<td><input type="checkbox" name="id" value="${id}"></td>
						    					<td><font color="blue">${dname}</font></td>
						    					<td>${sex}</td>
						    					<td id="age">${age}</td>
						    					<td><fmt:formatDate value="${begin_time}" pattern="yyyy-MM-dd HH:mm"/></td>
						    					<td class='changeColor state1'>${business_state}</td>
						    								<c:if test="${schedulFlag==1 }">
																<td style='color:#e84e40;' class='state1'>已到馆未收费</td>
													  		 </c:if>
									    					 <c:if test="${schedulFlag==2 }">
																<td style='color:#fea625;' class='state1'>收费结清</td>
													  		 </c:if>
													  		 <c:if test="${schedulFlag==3 }">
																<td style='color:#29b6f6;' class='state1'>已火化未打印</td>
													  		 </c:if>
													  		 <c:if test="${schedulFlag==4 }">
																<td style='color:#9ccc65;' class='state1'>已打印火化证明</td>
													  		 </c:if>	
						    					</tr>
										
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>

</section>
<script>
	(function(){
		$('.changeColor').each(function(){
			var txt =$(this).text();
			switch(txt){
				case '火化完成':
					$(this).css('color','#9CCC65');
					break;
				case '已打印确认单':
					$(this).css('color','#9CCC65');
					break;
				case '未打印确认单':
					$(this).css('color','#e84e40');
					break;
				case '火化未完成':
					$(this).css('color','#e84e40');
					break;
			}
		})
	})()
</script>