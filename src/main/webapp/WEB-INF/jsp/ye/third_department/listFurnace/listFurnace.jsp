<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 
<style type="text/css">
</style>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化炉列表</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>类型：</label> 
							<select class="list_input nopadding-R" name="searchType" style="width:52%;">
								<option value="1">姓名</option>
								<option value="2">卡号</option>
							</select>
						</div>
						<div class="col-md-8">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" name="searchVal" value="${searchVal }" placeholder="查询值">
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<label>日期：</label> 
							<input data-id='beginDate' class="list_select" id="dTime" name="beginDate" value=""><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						
							<span class='timerange'>--</span> 
							<input data-id='endDate' class="list_select" id="dTime" name="endDate" value=""><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<button type="submit" class="btn btn-info btn-search" >搜索</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border btns-buy btns-print">
					<small class='btns-buy'>
						<a href="" target="dialog" rel="myModal" checkOne="true" checkName="id" class="btn btn-warning" role="button">火化确认</a>
					<!-- 加链接 --><a href="" target="dialog" rel="myModal" checkOne="true" checkName="id" class="btn btn-info" role="button">火化入炉</a>
					
					</small>
					<small class="pull-right btns-buy btns-print">
<%-- 					<a href="${url}?method=exportExcel" target="dialog" rel="myModal" class="btn btn-success" role="button">导出EXCEL</a> --%>
<!-- 					<a href="javascript:myprint()" target="dialog"  rel="myModal" class="btn btn-color-ab47bc" role="button" id="d">打印</a> -->
						
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"></th>
											<th>IC卡号</th>
											<th>死者姓名</th>
											<th>死者性别</th>
											<th>年龄</th>
											<th>火化炉号</th>
											<th>火化开始时间</th>
											<th>预约火化时间</th>
											<th>业务状态</th>
											<th>是否收费</th>
											<th>打印状态</th>
										</tr>
									</thead>
									<tbody>
<%-- 									 <c:choose> --%>
<%-- 									    <c:when test="${fn:length(page.list)==0 }"> --%>
<!-- 										    <tr width="20"> -->
<!-- 								    			<td colspan="6"> -->
<!-- 											  		<font color="Red">还没有数据</font> -->
<!-- 										 		</td> -->
<!-- 										    </tr> -->
<%-- 									    </c:when> --%>
<%-- 									    <c:otherwise> --%>
<%-- 										    <c:forEach items="${page.list }" var="u"> --%>
											<tr role="row" width="10">
												<td><input type="radio" name="id" value="${u.id}"></td>
						    					<td>${u.cardCode }</td>
						    					<td><a href='dispatcher.do?list=third_department-listFurnace-charRegration' target='hometab' rel="myModal" id="read"><font color="blue">小明</font></a></td>
						    					<td>${u.sexName}</td>
						    					<td>${u.age}</td>
						    					<td>火化炉号</td>
						    					<td>火化开始时间</td>
						    					<td><fmt:formatDate value="${u.cremationTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
						    					<td class='state1'>业务状态</td>
						    					<td class='state1'>是否收费</td>
						    					<td class='state1'>打印状态</td>
<%-- 											</c:forEach> --%>
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
<script>

$(function(){
	timeRange();
});
</script>