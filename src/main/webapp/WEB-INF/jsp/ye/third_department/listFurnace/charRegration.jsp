<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
							<P class='col-md-12 p border-B'>死者信息</P>
							<div class='col-md-12 border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6'>
										<label class='lab-5'>死者姓名：</label>
										<span>1111111111</span>
									</div>
									<div class='col-md-6'>
										<label class='lab-5'>联系人：</label>
										<span>1111111111</span>
									</div>
									<div class='col-md-6'>
										<label class='lab-5'>死者编号：</label>
										<span>1111111111</span>
									</div>
									<div class='col-md-6'>
										<label class='lab-5'>联系人电话：</label>
										<span>1111111111</span>
									</div>
									<div class='col-md-12'>
										<label class='lab-5'>死者年龄：</label>
										<span>1111111111</span>
									</div>
									<div class='col-md-12'>
										<label class='lab-5'>死者性别：</label>
										<span>1111111111</span>
									</div>
								</div>
							</div>
							<p class='col-md-12 p border-B'>调度信息</p>
							<div class='col-md-12  border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6'>
										<label>火化炉号：</label>
										<span>111111111111111</span>
									</div>
									<div class='col-md-6 height-align'>
										<label style='width:130px;margin-left:-40px;'>火化预约开始时间：</label>
										<span>111111111111111</span>
									</div>
									<div class='col-md-6'>
										<label>火化工：</label>
										<span>111111111111111</span>
									</div>
									<div class='col-md-6 height-align'>
										<label>火化开始时间：</label>
										<input type="text"   data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="fireStartTime" name="fireStartTime" value="<fmt:formatDate value="" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
									<div class='col-md-12'>
										<label>录入员：</label>
										<span>111111111111111</span>
									</div>
									<div class='col-md-12'>
										<label>火化状态：</label>
										<span class='changeColor'>未火化</span>
									</div>
									<div class='col-md-12'>
										<label class='lab-5'>备注：</label>
										<textarea class='list_input textarea-dialog'></textarea>
									</div>
								</div>
							</div>
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit" class="btn btn-info btn-margin" >保存</button>
							
							<button type='reset' class="btn btn-margin btn-color-9E8273" >重置</button>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
							
						</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>




<script>
	(function(){
		$('.changeColor').each(function(){
			var txt=$(this).html();
			txt.indexOf('未')===0?($(this).css('color','#e84e40')):($(this).css('color','#9CCC65'));
		})
	})()
</script>