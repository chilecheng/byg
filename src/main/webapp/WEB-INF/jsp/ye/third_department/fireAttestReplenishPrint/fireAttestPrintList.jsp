<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化证明补打</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title" style="color:#4EC2F6">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
				</div>
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input nopadding-R" id="findBy" name="findBy">
									${searchOption }
								</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" id='searchVal' name='searchVal' value='${searchVal}' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>火化时间：</label> 
								<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span>
								<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							</div>
							<!-- 控制常用搜索状态（收费等） -->
<!-- 							<input type="hidden" id="checkType" name="checkType" value=""> -->
<!-- 							<div class="col-md-12" id="typeDiv"> -->
<!-- 								<label>常用：</label> -->
<!-- 								<button type="submit" class="btn btn-normally" name="payYes">已收费</button> -->
<!-- 								<button type="submit" class="btn btn-normally" name="payNo">未收费</button> -->
<!-- 								<button type="submit" class="btn btn-normally" name="payAll">全部</button> -->
<!-- 							</div> -->
							<div class="col-md-12">
								<button type="submit" id="timeRefresh" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
				</div>
				<!-- /.box-header -->
				 <div class="box-body">
					<div id="example2_wrapper"	class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
<!-- 											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId" /></th> -->
											<th>死者姓名</th>
											<th>死者性别</th>
											<th>年龄</th>
											<th>火化时间</th>
											<th>打印时间</th>
											<th>打印状态</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="8">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
<%-- 												<td><input type="checkbox" name="roleId" value="${u.cid}"></td> --%>
						    					<td><a href="${url }?method=show&printId=${u.printId}&orderId=${u.cid}" target='dialog' rel='myModal'>${u.dname}</td>
							    				<td>${u.sexName }</td>
						    					<td>${u.age}</td>
						    					<td><fmt:formatDate value="${u.beginTime}" pattern="yyyy-MM-dd HH:mm"/></td>
						    					<td><fmt:formatDate value="${u.printTime}" pattern="yyyy-MM-dd HH:mm"/></td>
					    						<c:if test="${u.scheduleFlag==1 }">
													<td style='color:#e84e40;' class='state1'>已到馆未收费</td>
										  		</c:if>
						    					<c:if test="${u.scheduleFlag==2 }">
													<td style='color:#fea625;' class='state1'>收费结清</td>
										  		</c:if>
										  		<c:if test="${u.scheduleFlag==3 }">
													<td style='color:#29b6f6;' class='state1'>已火化未打印</td>
										  		</c:if>
										  		<c:if test="${u.scheduleFlag==4 }">
													<td style='color:#9ccc65;' class='state1'>已打印火化证明</td>
										  		</c:if>
							    				<td>
								    				<a href="${url}?method=insert&dSex=${u.sexName }&id=${u.printId}&cid=${u.cid}&dName=${u.dname}&age=${u.age }&dCode=${u.certificateCode}" target="dialog" rel="myModal" class="btn btn-info" style="height:30px; width:98px;">领取人录入</a>
								    				<a href="${url}?method=print&dSex=${u.sexName }&id=${u.printId}&cid=${u.cid}&dName=${u.dname}&age=${u.age }&dCode=${u.certificateCode}"  target="dialog" rel="myModal" class="btn btn-info" style="height:30px; width:108px;background-color:#ab47bc;border-color:#ab47bc" >火化证明补打</a>  
								    			</td>
											</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
							
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div> 
			</div>
		</div>
	</div>
</section>
<script>

</script>
