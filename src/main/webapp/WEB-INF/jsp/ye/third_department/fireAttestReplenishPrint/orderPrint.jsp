<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form id="Form" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input name="method" value="${method}" type="hidden" />
			<input name="id" id="id"  type="hidden"  value="${printId }"/>
			<input name="commissionOrderId" id="commissionOrderId"  type="hidden"  value="${commissionOrderId }"/>
 			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">补打领取人录入</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-12  height-align">
									<label class="lab-6">领取人姓名：</label>
									<input type="text" readonly class="list_input input-hometab" id="takeName" name="takeName" value="${printRecord.takeNameAgain }">
<!-- 									<input type="text" readonly class="list_input" id="dNameId" data-id="dNameId" name="dNameId" hidden="" value=""> -->
<!-- 									<span class="nowarning" data-warning="warning"></span> -->
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>领取人证件：</label>
									<input type="text" readonly class="list_input input-hometab" id="takeCode" name="takeCode" value="${printRecord.takeCodeAgain }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者姓名：</label>
									<input readonly name='dName' class="list_input input-hometab" value="${dName }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者证件：</label>
									<input readonly name='dCode' class="list_input input-hometab" value="${dCode }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者性别：</label>
									<input readonly name='dSex' class="list_input input-hometab" value="${dSex }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者年龄：</label>
									<input readonly name='age' class="list_input input-hometab" value="${age }">
								</div>
							</div>
							<!-- 火化证明书打印 -->
							<div class='hide' id='space'>
								<!--startprint0--> 
								<p class='first_line'>  
									<span class='dName_1' >${dName }</span>
									<span class='dsex_1' >${dSex }</span>
									<span class='dID_1'  >${dCode }</span>
								</p>
								<p class='date_line_1'><span class='year_1'><fmt:formatDate value="${beginTime}" pattern="yyyy"/></span>
								<span class='month_1'><fmt:formatDate value="${beginTime}" pattern="MM"/></span>
								<span class='day_1'><fmt:formatDate value="${beginTime}" pattern="dd"/></span></p>
								<p class='second_line'><span class='username_1'>${userName }</span></p>
								<p><span class='nowmonth_1'><fmt:formatDate value="${now}" pattern="MM"/></span>
								<span class='nowday_1'><fmt:formatDate value="${now}" pattern="dd"/></span></p>
								<p><span class='idcode'>${printRecord.takeCodeAgain }</span></p>
								<p class='third_line'>  <span class='dName_2'>${dName}</span>
									<span class='dsex_2'>${dSex }</span>
									<span class='dID_2'>${dCode }</span>
								</p>
								<p class='date_line_2'><span class='year_2'><fmt:formatDate value="${beginTime}" pattern="yyyy"/></span>
								<span class='month_2'><fmt:formatDate value="${beginTime}" pattern="MM"/></span>
								<span class='day_2'><fmt:formatDate value="${beginTime}" pattern="dd"/></span></p>
								<p class='forth_line'><span class='username_2'>${userName }</span></p>
								<p class='last_line'><span class='nowmonth_2'><fmt:formatDate value="${now}" pattern="MM"/></span>
								<span class='nowday_2'><fmt:formatDate value="${now}" pattern="dd"/></span></p>
								<!--endprint0-->
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<a href="javascript:fireprint()" class='btn btn-info btn-margin'>打印</a>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
				
	</form>
</body>
<script>
function fireprint(){
	var bdhtml = window.document.body.innerHTML;
	var beginprint0 = "<!--startprint0-->";
	var endprint0 = "<!--endprint0-->";
	var beginprint1 = "<!--startprint1-->";
	var endprint1 = "<!--endprint1-->"; 
	
	var prnhtml0 = bdhtml.substr(bdhtml.indexOf(beginprint0) + 19);
	var prnhtml1 = bdhtml.substr(bdhtml.indexOf(beginprint1) + 19);
	
	prnhtml0 = prnhtml0.substring(0, prnhtml0.indexOf(endprint0));
	prnhtml1 = prnhtml1.substring(0, prnhtml1.indexOf(endprint1));
	 
	window.document.body.innerHTML = prnhtml0;
	 
	window.print();
	window.document.body.innerHTML = bdhtml;
	$.ajax({
		type : 'POST',
		data: {"id":$("#id").val(),"takeName":$("#takeName").val(),"takeCode":$("#takeCode").val()},
		url : "fireAttestReplenishPrint.do?method=upSchedual",
		dataType : "json", 
	 	success : function(json) {
				 initTab();		
 		}, 
 		
	})
	initTab();
	$('[data-dismiss="modal"]').click(function(){
    	 $('#myModal').removeClass('in').css('display','none');
    	 $('body').removeClass('modal-open');
 		 $('body').css('padding-right','0px');
    	 $('.modal-backdrop.fade.in').remove();
     })
}
</script>