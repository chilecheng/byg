<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form id="Form" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input name="method" value="${method}" type="hidden" />
			<input name="id" id="id"  type="hidden"  value="${printId }"/>
			<input name="commissionOrderId" id="commissionOrderId"  type="hidden"  value="${commissionOrderId }"/>
 			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">补打领取人录入</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-12  height-align">
									<label class="lab-6">领取人姓名：</label>
									<input type="text" class="list_input input-hometab" id="takeName" name="takeName" value="${printRecord.takeNameAgain }">
<!-- 									<input type="text" readonly class="list_input" id="dNameId" data-id="dNameId" name="dNameId" hidden="" value=""> -->
<!-- 									<span class="nowarning" data-warning="warning"></span> -->
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>领取人证件：</label>
									<input type="text" class="list_input input-hometab" id="takeCode" name="takeCode" value="${printRecord.takeCodeAgain }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者姓名：</label>
									<input readonly name='dName' class="list_input input-hometab" value="${dName }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者证件：</label>
									<input readonly name='dCode' class="list_input input-hometab" value="${dCode }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者性别：</label>
									<input readonly name='dSex' class="list_input input-hometab" value="${dSex }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>死者年龄：</label>
									<input readonly name='age' class="list_input input-hometab" value="${age }">
								</div>
								
								
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<button type='submit' class='btn btn-info btn-margin'>保存</button>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
</body>
<script>
</script>