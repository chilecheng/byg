<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form id="Form" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input name="method" value="${method}" type="hidden" />
<%-- 			<input name="id" id="id"  type="hidden"  value="${printId }"/> --%>
 			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content" style="width:700px">
				<div class="modal-body nopadding-B nopadding-T" style="width:700px">
					<div class="row">
						<p class="col-md-12 p border-B">死者信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者姓名：</label>
									<input readonly name='dName' class="list_input input-hometab" value="${commissionOrder.name }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者证件：</label>
									<input readonly name='dCode' class="list_input " value="${commissionOrder.certificateCode }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者性别：</label>
									<input readonly name='dSex' class="list_input input-hometab" value="${commissionOrder.sexName }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者年龄：</label>
									<input readonly name='age' class="list_input input-hometab" value="${commissionOrder.age }">
								</div>
							</div>
							<p class="col-md-12 p border-B">火化证明领取信息</p>
							<div class="row">
								<div class="col-md-6  height-align">
									<label class="lab-6">领取人姓名：</label>
									<input type="text" class="list_input input-hometab" id="takeName" name="takeName" value="${printRecord.takeName }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>领取人证件：</label>
									<input type="text" class="list_input " id="takeCode" name="takeCode" value="${printRecord.takeCode }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>操作人员：</label>
									<input type="text" class="list_input input-hometab" id="takeCode" name="takeCode" value="${printRecord.user.name }">
								</div>
								<div class="col-md-6  height-align">
									<label class="lab-6">打印时间：</label>
									<input type="text" class="list_input " id="takeName" name="takeName" value='<fmt:formatDate value="${printRecord.printTime }" pattern="yyyy-MM-dd HH:mm"/>'>
								</div>
							</div>
							<p class="col-md-12 p border-B">火化证明补打信息</p>
							<div class="row">
							<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th>领取人姓名</th>
											<th>领取人证件</th>
											<th>操作人员</th>
											<th>打印时间</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
									    <c:when test="${fn:length(listAgain)==0 }">
										    <tr>
								    			<td colspan="4">
											  		<font color="Red">没有补打信息</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${listAgain }" var="u">
												<tr role="row">
						    					<td>${u.takeNameAgain}</td>
							    				<td>${u.takeCodeAgain }</td>
						    					<td>${u.user.name}</td>
						    					<td><fmt:formatDate value="${u.printTimeAgain}" pattern="yyyy-MM-dd HH:mm"/></td>
											</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
</body>
<script>
</script>