<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.sql.*,java.util.*,java.text.*"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<style>
.control-label.error-label{
display:none;
}
</style>
	<input name="method" value="list" type="hidden">
	<input name="cardCode" value="${cardCode}" type="hidden">
	<div class="modal-dialog" role="document">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body" style='padding:0px;'>
						<P class='col-md-12 p border-B'>死者信息</P>
						<div class='col-md-12 border-B padding-B nomargin-T'>
							<div class='row'>
								<div class='col-md-6 height-align'>
									<label class='lab-1'>死者姓名：</label>
									<span>${dname}</span>
								</div>
								<div class='col-md-6 height-align'>
									<label class='lab-2'>联系人：</label>
									<span>${fname }</span>
								</div>
								<div class='col-md-6 height-align'>
									<label class='lab-1'>死者编号：</label>
									<span>${code }</span>
								</div>
								<div class='col-md-6 height-align'>
									<label class='lab-5'>联系人电话：</label>
									<span>${fphone}</span>
								</div>
								<div class='col-md-12'>
									<label class='lab-1'>死者年龄：</label>
									<span>${age }</span>
								</div>
								<div class='col-md-12'>
									<label class='lab-1'>死者性别：</label>
									<span>${sex }</span>
								</div>
							</div>
						</div>
						<p class='col-md-12 p border-B'>调度信息</p>
						<div class='col-md-12  border-B padding-B nomargin-T'>
							<div class='row'>
								<div class='col-md-6 height-align'>
									<label class='lab-2'>火化炉号：</label>
									<input type="hidden" name="furnaceId" id="furnaceId" />
									<select <c:if test="${select eq 'true' }">onclick="changeFur('${recordId }')"</c:if> class="list_input input-hometab required" id="funeralNum" name="funeralNum" <c:if test="${changeto ne 'pt' && furnaceType ne 'pt' }">disabled="disabled"</c:if>  >
										${FurnaceOption }
									</select>
									<c:if test="${changeto ne 'pt' && furnaceType ne 'pt' }">
										<a href="commissionOrder.do?method=editFurnace&where=changeFur&current_time=${cremationtime}" target="dialog" id="chooseFur" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">更改</a>
									</c:if>
								</div>
								
								<%-- <c:if test="${type ne 'change' }"> --%>
									<div class='col-md-6 height-align'>
										<label  class='lab-6'>火化开始时间：</label>
										<%-- <input type="text"   data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="fireStartTime" name="fireStartTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
										<input type="text" style='min-width:160px;width:61%;' class="list_input input-hometab" readOnly='readOnly' data-id="reservation" id="fireStartTime" name='FireStartTime' value='${startTime}'><!-- <i style="margin-left: -20px;" class="fa fa-calendar"></i> -->
	
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-3'>火化工：</label>
										<select class="list_input input-hometab required" id="fireworker" name="fireworker">
											${fireworkerOption}
										</select>
										<i class="fa fa-circle text-red"></i>
									</div>
								<%-- </c:if> --%>
								<div class='col-md-6 height-align'>
									<label  class='lab-6' >火化预约时间：</label>
									<%-- <span id="cremationTime2"><fmt:formatDate value="${cremationtime}" pattern="yyyy-MM-dd HH:mm"/></span> --%>
									<input type="text" style='min-width:160px;width:61%;' class="list_input input-hometab" readOnly='readOnly' id="orderTime" name='FireStartTime' value="${cremationtime}"><!-- <i style="margin-left: -20px;" class="fa fa-calendar"></i> -->
								</div>
								<div class='col-md-12'>
									<label  class='lab-2'>火化状态：</label>
									<span class='changeColor' id="cremationFlag">${cremationFlag}</span>
								</div>
								
								<div class='col-md-6 height-align'>
									<label  class='lab-3'>录入员：</label>
									<span>${uname}</span>
								</div>
								
								<div class='col-md-12'>
									<label  class='label-4'>备注：</label>
									<textarea class='list_input textarea-hometab' id="remark" name="remark">
									   ${comment}
									</textarea>
								</div>
								<div class='col-md-12 btns-dialog'>
									<a data-name='sure' class='btn btn-info btn-margin' >确认</a>
									<a href='${url }?method=insertOrUpdate&noRef=${noRef }&<c:if test="${type=='change' }">type=change&</c:if><c:if test="${ptrulu=='yes' }">ptrulu=yes&</c:if><c:if test="${changeto!='' }">changeto=${changeto }&</c:if>spe_or_com_flag=${spe_or_com_flag }&fireid=${fireid }&cid=${cid }&cremationFlag=${cremationFlag }&recordId=${recordId }' 
									rel="model" target="ajaxTodo" warm=<c:if test="${type=='change' && ptrulu != 'yes' }">"确认转炉"</c:if><c:if test="${type!='change' || ptrulu == 'yes' }">"确认入炉"</c:if> id="sure" class="hide" role="button">确认</a>
									 
									<button type='reset' class='btn btn-margin btn-color-9E8273'>重置</button>
									<button class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
<script>
	(function(){
		$('#cremationFlag').html().trim()==='已火化'&&($('#remark').attr('disabled','disabled'),
				 $('#fireworker').attr('disabled','disabled'),$('#funeralNum').attr('disabled','disabled'),
				 $('#fireStartTime').attr('disabled','disabled'));
		$('[data-name="sure"]').click(function (){
			var e=0;
			$(".required").each(function() {
					var value = $(this).val();
					if (!value) {
						var flag = $(this).attr("errorFlag");
						if (flag == undefined) {
							$(this).attr("errorFlag", "true");
							$(this).addClass("error-input");
							var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
							$(this).parent().append(err);
						}
						e++;
					}
				});
			if(e){toastr["warning"]("请确保填写完整");return false;}
			var href=$('#sure').attr('href')+'&'+$('#remark').attr('id')+'='+$('#remark').val()
			         +'&'+$('#funeralNum').attr('id')+'='+$('#funeralNum').val()
			         +'&'+$('#fireworker').attr('id')+'='+$('#fireworker').val()
			         +'&'+$('#fireStartTime').attr('id')+'='+$('#fireStartTime').val()
			         +'&'+'orderTime='+$('#orderTime').val()+'noRef=${noRef }';
			$('#sure').attr('href',href);
			$('#sure').click();
		});
		$('.changeColor').each(function(){
			var txt=$(this).html().trim();
			txt.indexOf('已')===0?($(this).css('color','#9CCC65')):($(this).css('color','#e84e40'));
		})
		
        //带有分的时间控件
 		if($('.daterangepicker.dropdown-menu.show-calendar.opensright').length!==0){
			$('.daterangepicker.dropdown-menu.show-calendar.opensright').remove();
		}
		$('[data-id="reservation"]').daterangepicker({timePicker: true, timePickerIncrement: 5, format: 'YYYY-MM-DD hh:mm:ss A'});
		$('[data-id="reservation"]').change(function(){
			var time=$(this).val();
			var arr=time.split(' ');
			var hour=parseInt(arr[1].slice(0,2));
			arr.pop();
			if(time.lastIndexOf('AM')!==-1&&hour==12){
				arr[1]=hour-12+arr[1].slice(2);
			}
			if(time.lastIndexOf('PM')!==-1){
				arr[1]=hour<12?(hour+12+arr[1].slice(2)):(arr[1]);
			}
			$(this).val(arr.join(' '));
		});
	})()
	function changeFur(recordId) {
		$('#chooseFur').click();
	}
</script>