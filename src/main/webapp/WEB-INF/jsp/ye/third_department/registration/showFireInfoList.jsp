
<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.sql.*,java.util.*,java.text.*"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" role="document">
	<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
		<%@ include file="/common/pageHead.jsp"%>
		<input name="method" value="list" type="hidden">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-warning">
						<div class="box-body" style='padding:0px;'>
							<P   class='col-md-12 border-B p'>业务信息</P>
							<div class='col-md-12 border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者姓名：</label>
										<span>${listfc.dname}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>联系人：</label>
										<span>${listfc.fname}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者编号：</label>
										<span>${listfc.code}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-5'>联系人电话：</label>
										<span>${listfc.fphone}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者年龄：</label>
										<span>${listfc.age}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>告别厅号：</label>
										<span>${listfc.fareWellName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者性别：</label>
										<span>${listfc_sex}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>出殡礼仪：</label>
										<span>${poinfo}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者住址：</label>
										<span>${listfc.address}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>收费状态：</label>
										<span class='changeColor'>${listfc.payFlagName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-3'>纸馆：</label>
										<span>
										<c:if test="${fn:length(pinfo)==0}"> 
										     &nbsp;无
										</c:if>
										<c:forEach items="${pinfo}" var="u" varStatus="sta">
										     <c:if test="${sta.last==true}">
										        ${u.paperName}
										     </c:if>
										     <c:if test="${sta.last==false}">
										        ${u.paperName}、
										     </c:if>
										</c:forEach>			
										</span>
									</div>
								</div>
							</div>
							<p class='col-md-12 border-B p'>调度信息</p>
							<div class='col-md-12 border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>火化状态：</label>
										<span class='changeColor'>
										   ${cremation_flag}
										</span>
									</div>
									
									<div class='col-md-6  height-align'>
										<label  class='lab-1'>确认时间：</label>
										<span><%=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new java.util.Date())%></span>
									</div>
									<div class='col-md-12 height-align'>
										<label  class='lab-5'>火化预约开始时间：</label>
										<span><fmt:formatDate value="${listfc.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></span>
									</div>
									<div class='col-md-12 btns-dialog'>
											<button type="button" id="return" class="btn btn-info btn-margin" data-dismiss="modal">确认</button>
									  
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</form>
</div>
<script>
	(function(){
		$('.changeColor').each(function(){
			var txt=$(this).html().trim();
			txt.indexOf('已')===0?($(this).css('color','#9CCC65')):($(this).css('color','#e84e40'));// e84e40  9CCC65
		})
	})()
</script>