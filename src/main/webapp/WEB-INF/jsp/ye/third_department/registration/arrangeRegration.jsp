<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.sql.*,java.util.*,java.text.*"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化安排</div>
</section>
	<%-- <input name="method" value="list" type="hidden">
	<input name="cardCode" value="${cardCode}" type="hidden"> --%>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
						<div class="box-body" style='padding:0px;'>
							<P class='col-md-12 p border-B'>死者信息</P>
							<div class='col-md-12 border-B padding-B nomargin-T'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者姓名：</label>
										<span>${dname}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>联系人：</label>
										<span>${fname }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者编号：</label>
										<span>${code }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-5'>联系人电话：</label>
										<span>${fphone}</span>
									</div>
									<div class='col-md-12'>
										<label class='lab-1'>死者年龄：</label>
										<span>${age }</span>
									</div>
									<div class='col-md-12'>
										<label class='lab-1'>死者性别：</label>
										<span>${sex }</span>
									</div>
								</div>
							</div>
							<p class='col-md-12 p border-B'>调度信息</p>
							<div class='col-md-12  border-B padding-B nomargin-T'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>火化炉号：</label>
										<select class="list_input input-hometab" id="funeralNum" name="funeralNum" <c:if test="${spe_or_com_flag }"> disabled="disabled"</c:if>>
											${FurnaceOption}
										</select>
										<c:if test="${spe_or_com_flag }">
											<a href="commissionOrder.do?method=editFurnace&where=changeFur&current_time=${cremationtime}" target="dialog" id="chooseFur" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">更改</a>
										</c:if>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-2' style='width:160px;margin-left:-30px;'>火化预约开始时间：</label>
										<input type="text" style='min-width:40%;width:40%;' class="list_input input-hometab" readOnly='readOnly' id="orderTime" value="${cremationtime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-3'>火化工：</label>
										<select class="list_input input-hometab" id="fireworker" name="fireworker">
											${fireworkerOption}
										</select>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-6'>火化开始时间：</label>
										<%-- <input type="text"   data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="fireStartTime" name="fireStartTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
										<input style='min-width:40%;width:40%;' type="text" class="list_input input-hometab" readOnly='readOnly' data-id="reservation" id="FireStartTime" name='FireStartTime' value='<fmt:formatDate value="${startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'><i style="margin-left: -20px;" class="fa fa-calendar"></i>

									</div>
									<div class='col-md-12'>
										<label  class='lab-3'>录入员：</label>
										<span>${uname}</span>
									</div>
									<div class='col-md-12'>
										<label  class='lab-2'>火化状态：</label>
										<span class='changeColor' id="cremationFlag">${cremationFlag}</span>
									</div>
									<div class='col-md-12'>
										<label  class='label-4'>备注：</label>
										<textarea class='list_input textarea-hometab' id="remark" >
										   ${comment}
										</textarea>
									</div>
								</div>
							</div>
						</div>
						
				</div>
				<!-- /.col -->
				<div class='box' style='padding:10px;'>
					<div class='row'>
						<div class='col-md-12 btns-dialog'>
							<a data-name='sure' class='btn btn-info btn-margin'>确认</a>
							<a href="fireCheckIn.do?method=insertOrUpdate&spe_or_com_flag=${spe_or_com_flag}&fireid=${fireid}&cid=${cid}&cremationtime=${cremationtime}&cremationFlag=${cremationFlag}&recordId=${recordId }&cardCode=${cardCode }" 
							 target="homeTab" rel="myModal" id="sure" class="hide" role="button">确认</a>
							<button type='reset' class='btn btn-margin btn-color-9E8273'>重置</button>
							<a href="fireCheckIn.do?method=list&cardCode=${cardCode}"  target="homeTab" rel="myModal" id="return" class="btn btn-default btn-margin" role="button">返回</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<script>
(function(){
	$('#cremationFlag').html().trim()==='已火化'&&($('#remark').attr('disabled','disabled'),
			 $('#fireworker').attr('disabled','disabled'),$('#funeralNum').attr('disabled','disabled'),
			 $('#FireStartTime').attr('disabled','disabled'));
	$('[data-name="sure"]').click(function (){
		
		var href=$('#sure').attr('href')+'&'+$('#remark').attr('id')+'='+$('#remark').val()
		         +'&'+$('#funeralNum').attr('id')+'='+$('#funeralNum').val()
		         +'&'+$('#fireworker').attr('id')+'='+$('#fireworker').val()
		         +'&'+$('#FireStartTime').attr('id')+'='+$('#FireStartTime').val()
		         +'&'+'orderTime='+$('#orderTime').val();
		$('#sure').attr('href',href);
		$('#sure').click();
	});
	$('.changeColor').each(function(){
		var txt=$(this).html().trim();
		txt.indexOf('已')===0?($(this).css('color','#9CCC65')):($(this).css('color','#e84e40'));
	})
       //带有分的时间控件
		if($('.daterangepicker.dropdown-menu.show-calendar.opensright').length!==0){
		$('.daterangepicker.dropdown-menu.show-calendar.opensright').remove();
	}
	$('[data-id="reservation"]').daterangepicker({timePicker: true, timePickerIncrement: 5, format: 'YYYY-MM-DD hh:mm:ss A'});
	$('[data-id="reservation"]').change(function(){
		var time=$(this).val();
		var arr=time.split(' ');
		var hour=parseInt(arr[1].slice(0,2));
		arr.pop();
		if(time.lastIndexOf('AM')!==-1&&hour==12){
			arr[1]=hour-12+arr[1].slice(2);
		}
		if(time.lastIndexOf('PM')!==-1){
			arr[1]=hour<12?(hour+12+arr[1].slice(2)):(arr[1]);
		}
		$(this).val(arr.join(' '));
	});
})()
</script>