<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<script type="text/javascript">
$(function(){
	$('#mourningNumber').text('');
});

	timeRange();
	function changeDate(){
		$('#pageForm').submit();
	}

	(function() {
		function changeColor() {
			var value = $(this).val();
			var txt = $(this).find("option[value='" + value + "']").text();
			switch (txt) {
			case '已处理':
				$(this).css('color', '#4DA2FD');
				break;
			case '未处理':
				$(this).css('color', '#D73925');
				break;
			/* case '已领用':
				$(this).css('color', '#00A65A');
				break; */
			}
		}
		$(".state").change(changeColor);
		$(".state").each(changeColor);
		$(".state").each(function() {
			$(this).find('option').each(function() {
				var txt = $(this).text();
				switch (txt) {
				case '已处理':
					$(this).css('color', '#4DA2FD');
					break;
				case '未处理':
					$(this).css('color', '#D73925');
					break;
				/* case '已领用':
					$(this).css('color', '#00A65A');
					break; */
				}
			})
		});
	})();


	function GaiBian(v) {
		var value = $(v).val();
		var cvalue = $(v).parent().parent().find('input:checkbox').val();
	
		$.ajax({
			type : 'POST',
			url : "mourningWordOrder.do?method=isdel&isdel=" + value + "&id=" + cvalue,
			dataType : "json",
			success : dialogAjaxDone,
		});
	}
</script>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">灵堂安排任务单</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
					<%-- <iframe class='frame' id="reportFrame" width="100%" src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=mouringWordOrder.cpt&op=view"></iframe> --%>
						<div class="col-md-4">
							<label>类型：</label>
							<select class="list_select nopadding-R" name="searchType" id="searchType" style="width: 52%;">
								${searchOption }
							</select>
						</div>
						<div class="col-md-8" style="margin-left: -94px">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" name="searchVal" id="searchVal" value="${searchVal}" placeholder="查询值">
						</div>
						<div class="col-md-12">
							<label>设灵时间：</label> 
							<input type="text"  data-id='beginDate' class="list_select"  name="beginDate" value="<fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<span class='timerange'>--</span> 
							<input type="text"  data-id='endDate' class="list_select"  name="endDate" value="<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search btn-search-left">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	 <div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					${beginDate }——<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />
					<small class="pull-right btns-print">
						<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认处理吗" class="btn btn-info" role="button">批量处理</a>
						<a href="${url}?method=isdel&isdel=${Isdel_No }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认未处理吗" class="btn btn-danger" role="button">批量未处理</a>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12 overx">
								<table class="table table-bordered table-hover table-width">
									<thead>
										<tr role="row"><!-- style='display:none;' -->
											<th ><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>
											<th>灵堂</th>
											<th>设灵时间</th>
											<th>结束时间</th>
											<th>告别时间</th>
											<th>死者姓名</th>
											<th>性别</th>
											<th>年龄</th>
											<th>对联</th>
											<th>围花</th>
											<th>黑纱</th>
											<th>白纱</th>
											<th>花门</th>
											<th>背景</th>
											<th>告别厅</th>
											<th>状态</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="16">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											     <c:forEach items="${page.list}" var="u">
													<tr role="row">
														<c:forEach items="${u}" var="k" varStatus="vs">
															<c:choose>
																<c:when test="${vs.index==0}">
																	<td ><input type="checkbox" name="checkboxId" value="${k}"></td>
																</c:when>
																<c:when test="${vs.index==2 }">
										    						<td><fmt:formatDate value="${k}" pattern="yyyy-MM-dd"/></td>
										    					</c:when>
										    					<c:when test="${vs.index==3 }">
										    						<td><fmt:formatDate value="${k}" pattern="yyyy-MM-dd"/></td>
										    					</c:when>
										    					<c:when test="${vs.index==4 }">
										    						<td><fmt:formatDate value="${k}" pattern="yyyy-MM-dd HH:mm"/></td>
										    					</c:when>
										    					<c:when test="${vs.index==15}">
																	<td><select name="state" class="state"
																		onchange="GaiBian(this)"> ${k }
																	</select></td>
																</c:when>
																<c:otherwise>
										    					<td>${k}</td>
										    					</c:otherwise>
									    					</c:choose>
								    					</c:forEach>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
						<%@ include file="/common/pageFood.jsp"%>
			</div>
		</div>
	</div> 
</section>
