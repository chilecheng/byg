<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">丧葬用品购买</div>
</section>

<script type="text/javascript">
var sum=10000;//用于区分查出来的，和新添加的，10000条对于查出来的应该足够
var aTotal=0;//合计总金额
$(function(){
	//timeRange();
	timeMunite();
});
</script>
<script src="js/funeralcommon.js"></script>

<form action="${url}" id="pageForm" onsubmit="return readData(this, homeAjaxDone);">
	<!-- -修改丧葬用品- -->
	<section class="content nopadding-B">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<%@ include file="/common/pageHead.jsp"%>
						<input name="method" value="${method}" type="hidden">
						<input name="buygoodsid" value="${buygoodsid}" type="hidden">
						<div class="box-body">
							<div class="row padding-B border-B"  style='margin-right: -10px; margin-left: -10px;'>
<!-- 								<div class="col-md-6"> -->
<!-- 									<label class='lab-6'>卡号：</label>  -->
<%-- 									<span>${listrec.cardCode}</span> --%>
<!-- 								</div> -->
								
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者姓名：</label> 
									<span>${listrec.dName}</span>
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-2'>购买人：</label> 
									<input type="text" class="list_input input-hometab" id='buyer' name='buyer' value='${listrec.takeName}'/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>守灵时间：</label> 
									<span><fmt:formatDate value="${listrec.mourningTime}" pattern="yyyy-MM-dd HH:mm"/></span><!--  -->
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>与死者关系：</label> 
									<!-- <input type="text" class="list_input" id='relactive' name='relactive' value=''/> -->
									<select  class="input-content list_input nopadding-R input-hometab" id="relactive" name="relactive">
										${fOption}
									</select>
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者年龄：</label> 
									<span>${listrec.dAge}</span>
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-1'>购买时间：</label> 
									<input type="text" data-id="reservation" data-min-view="2" data-date-format="yyyy-mm-dd HH:mm" class="list_select" id="buyTime" name="buyTime" value="<fmt:formatDate value='${buyTime}' pattern='yyyy-MM-dd HH:mm'/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>告别时间：</label> 
									<span><fmt:formatDate value="${listrec.farewellTime}" pattern="yyyy-MM-dd HH:mm"/></span><!--  -->
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-1'>领用时间：</label> <!-- data-provide="datetimepicker" -->
									<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="recTime" name="recTime" value="<fmt:formatDate value="${takeawayTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-1'>告别厅号：</label> 
									<span>${listrec.farewellName}</span>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-1'>死者性别：</label> 
									<span>${dSex}</span>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-2'>灵堂号：</label> 
									<span>${listrec.mourningName}</span>
								</div>
							</div>
							<div class="row padding-B">
								<div class="col-md-12">
									<small class='btns-buy pull-right'>
										<a href="funeralBuy.do?method=selectItems" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<a onclick="delHtml('id')"  class="btn btn-danger" role="button">删除</a>
									</small>
								</div>
							</div>
							<div class="row padding-B">
								<div class="col-md-12" style="padding-right:15px;">
									<table class="table table-bordered">
									    <tbody>
										<thead>
											<tr>
												<th>
													  <div class="form-group nomargin-B">
													      <div class="checkbox nomargin-B nomargin-T">
													        <label>
													         	<input type="checkbox" group='id' class='checkBoxCtrl'>全选 
													        </label>
													      </div>
													  </div>
												</th>
												<th>名称</th>
												<th>单价（元）</th>
												<th>数量</th>
												<th>小计</th>
												<th>挂账</th>
												<th>备注</th>
											</tr>
										</thead>
										</tbody>
										<tbody>
										    <tbody id="articles">
										     <c:set var="index" value="0"></c:set>
										     <c:set var="total" value="0"></c:set>
										     <c:forEach items="${listdetail}" var="u">								        
										        <tr role="row">
										           <td style='text-align: center; vertical-align: middle;'>
										               <input type="checkbox" id="checkbox_${index}" name="id" class="checkBoxCtrl"  value="${u.id}" value_index="${index}"/>
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <select  class="list_table" id="itemId_${index}" name='itemId' onchange="itemName(${index})">
										                  ${lsOption.get(index)}
										               </select>
										               
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <input type='text' readonly='readonly' class='list_table' id="pice_${index}" value="<fmt:formatNumber value='${u.pice}' pattern='#' type='number'/>">
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <input type='text' class='list_table' id="number_${index}" value="${u.number}" onchange="number(${index})">
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <input type='text' readonly='readonly' id="total_${index}" class='list_table' value="<fmt:formatNumber value='${u.total}' pattern='#' type='number'/>">
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <select  class='list_table'  id="iscredit_${index}" value=''>
										                   <c:choose>
										                      <c:when test="${u.iscredit=='0'}">
										                          <option value='0' selected="selected">否</option>
										                          <option value='1'>是</option>
										                      </c:when>
										                      <c:otherwise>
										                          <option value='0'>否</option>
										                          <option value='1' selected="selected">是</option>
										                      </c:otherwise>
										                   </c:choose>								                   
										               </select>
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <input type='text' class='list_input input-hometab'  id="remarks_${index}" value='${u.remarks}' style='width: 80%'>									               
										           </td>
										        </tr>
										        <c:set var="index" value="${index+1}"></c:set>
										        <c:set var="total" value="${total+u.total}"></c:set>										      
										     </c:forEach>
									       </tbody>
									       <tbody>
											<tr>
												<td>合计</td>										
												<td colspan='6' style="text-align:left;">
													<font color='red' id="articlesFont">
											        <fmt:formatNumber value='${total}' pattern='#' type='number'/>元
											       </font>
											  	</td>
											</tr>											
										  </tbody>
									</table>
									
								</div>
							</div>
							<div class='row padding-B'>
								<div class="col-md-12" style="padding-left:15px;">
									<div class="row">
										<span class="col-md-1">备注：</span> 
										<textarea id="remarktext" name="remarktext" class="col-md-11" style="vertical-align:top;" rows="5" cols="150" value="">${listrec.remarktext}</textarea>
									</div>
									
								</div>
							</div>
						</div>
					
					
				</div>
				<!-- /.col -->
				
			</div>
		</div>
	</section>
	<section class="content nopadding-B" id='main-content'>
		<div class='box padding-B padding-L nomargin-B'>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-5 height-align">
							<label class='lab-1'>经办人：</label>
							<span>${u.name}</span>
						</div>
						<div class="col-md-7 height-align">
							<label class='lab-1'>代办人：</label>
							<input type="text" class="list_input" id='bname' name='bname' value='${listrec.replaceName}'/>
						</div>
						<div class="col-md-5 height-align">
							<label class='lab-1'>流水号：</label>
							<span>${listrec.flowId}</span>
						</div>
						<div class="col-md-7 height-align">
							<label class='lab-4'>购买单号：</label>
							<input type="text" class="list_input" id='ordernum' name='ordernum' value='${listrec.buyOrderId}'/><!--  -->
						</div>		
					</div>
				</div>
				<div class="col-md-6">
				   <small class='btns-hometab pull-right'>
				   		<button type="submit" class="btn btn-info btn-margin" >保存</button>
				   		<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
				   		<a href="${url}" target='homeTab' class="btn btn-default btn-margin">返回</a>
				   </small>
				</div>
			</div>
		</div>
	</section>
</form>


<script>
aTotal=${total};
var date=new Date();
var Y=date.getFullYear();
var M=date.getMonth()+1;M<10&&(M='0'+M);
var D=date.getDate();D<10&&(D='0'+D);
var H=date.getHours();H<10&&(H='0'+H);
var Mu=date.getMinutes();Mu<10&&(Mu='0'+Mu);
var S=date.getSeconds();S<10&&(S='0'+S);
$('#buyTime').val(Y+'-'+M+'-'+D+' '+H+':'+Mu);
</script> 