<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<head>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">丧葬用品购买</div>
</section>


<script type="text/javascript">
var sum=0;//这里是新添加，所以可以从0开始
var aTotal=0;//合计总金额
$(function(){
	timeRange();
	timeMunite();
});
</script>
<script src="js/funeralcommon.js"></script>


</head>
<body>
<form action="${url}" id="Form" onsubmit="return readData(this, homeAjaxDone);">

<!-- -购买丧葬用品- -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<input name="method" value="${method}" type="hidden">
						<div class="box-body">
							<div class="row padding-B border-B">
								<div class="col-md-6  height-align">
									<label class='lab-6'>死者姓名：</label> 
									<input type="text" class="list_input input-hometab" id='dName' name='dName' value=''/>
									<input  type="hidden" name='hideName' id="hideName" value=''/>
									<button class='btn-psearch btn btn-info' type='button' data-name='search'>搜索</button>
									<input type="text" readonly class="list_input" id='dNameId' data-id="dNameId" name='dNameId' hidden value=''/>
									<a data-dialog='dialog' class='hide' target='dialog' rel="myModal" href='${url}?method=dNameList'>选择</a>
									<span class='nowarning' data-warning='warning'></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>购买时间：</label> 
									<input type="text" data-id="reservation"   data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="buyTime" name="buyTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者年龄：</label> 
									<input type="text"  class="list_input input-hometab" id='dAge' readonly="readonly" name='dAge' value=''/>
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-6'>领用时间：</label> <!-- data-provide="datetimepicker" -->
									<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="required list_select input-hometab" id="recTime" name="recTime" value="${recTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>			
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者性别：</label> 
									<input type="text"  class="list_input input-hometab" id='dSex' readonly="readonly" name='dSex' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>购买单号：</label>
									<input type="text" class="required list_input input-hometab" id='ordernum' name='ordernum' value=''/>&nbsp;<i class="fa fa-circle text-red"></i>
								</div>								
								<div class="col-md-6 height-align">
									<label class='lab-1'>灵堂号：</label> 
									<input type="text" class="list_input input-hometab" readonly="readonly" id='hallName' name='hallName' value=''/>
								</div>
								<div class="col-md-6 height-align">
<!-- 									<label class='lab-1'>购买人：</label>  -->
<!-- 									<input type="text" class="required list_input input-hometab " id='buyer' name='buyer'/>&nbsp;<i class="fa fa-circle text-red"></i> -->
									<label class='lab-1'>购买人：</label> 
									<input list="buyer" autocomplete='off' class="required list_select"  style='min-width:40%' name='buyer' />&nbsp;<i class="fa fa-circle text-red"></i>
									<datalist  class="list_select"  id="buyer">
										<option value="亲友"></option>	
										<option value="朋友"></option>
									</datalist>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>与死者关系：</label> 
									<!-- <input type="text" class="list_input" id='relactive' name='relactive' value=''/> -->
									<select  class="list_input input-hometab nopadding-R" id="relactive" name="relactive">
										${fAppellationOption}
									</select>
								</div>
								<div class="col-md-7 height-align">
								
									<label class='lab-6'>守灵时间：</label> 
									<input type="text"  class="list_select input-hometab" readonly="readonly" id="hallTime" name="hallTime"/><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								
								<div class="col-md-7 height-align">
									<label class='lab-6'>告别时间：</label> 
									<input type="text" class="list_select input-hometab" readonly="readonly" id="fareTime" name="fareTime" ><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								
								<div class="col-md-7 height-align">
									<label class='lab-6'>告别厅号：</label> 
									<input type="text" class="list_input input-hometab" readonly="readonly" id='fareName' name='fareName' value=''/>
									
								</div>
								
								
								
							</div>
							<div class="row padding-B">
								<div class="col-md-12">
									<small class='btns-buy'>
										<a href="funeralBuy.do?method=selectItems" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<!-- <button type="button" onclick="delHtml('id')" class="btn btn-danger ">删除</button> -->
										<a onclick="delHtml('id')"  class="btn btn-danger" role="button">删除</a>
									</small>
								</div>
							</div>
							<div class="row padding-B">
								<div class="col-md-12" style="padding-right:15px;">
									<table class="table table-bordered">
									    <tbody>
										<thead>
											<tr>
												<th width='70px'>
													  <div class="form-group nomargin-B">
													      <div class="checkbox nomargin-B nomargin-T">
													        <label>
													         	<input type="checkbox" group='id' class='checkBoxCtrl'>全选 
													        </label>
													      </div>
													  </div>
												</th>
												<th>名称</th>
												<th>单价（元）</th>
												<th>数量</th>
												<th>小计</th>
												<th>挂账</th>
												<th width='180px'> 备注</th>
											</tr>
										</thead>
										</tbody>
										<tbody id="articles">
										
									    </tbody>									
										<tbody>	
											<tr>
												<td>合计</td>
												<td colspan='6' style="text-align:left;">
													<font color='red' id="articlesFont">
											         0元
											       </font>
											  	</td>
											</tr>
											
										</tbody>
									</table>
									
								</div>
							</div>
							<div class='row padding-B'>
								<div class="col-md-12" style="padding-left:15px;">
									<div class="row">
										<span class="col-md-1">备注：</span> 
										<textarea id="remarktext" name="remarktext" class="col-md-11" style="margin-left:-15px;" rows="5" cols="150" placeholder="" ></textarea>
									</div>
									
								</div>
							</div>
						</div>
					
					
				</div>
				<!-- /.col -->
				
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box padding-L margin-R padding-B'>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-5 height-align">
							<label class='lab-1'>经办人：</label>
							<span>${u.name}</span>
						</div>
						<div class="col-md-7 height-align">
							<label class='lab-1'>代办人：</label>
							<input type="text" style='min-width:60%;' class="list_input" id='bname' name='bname' value=''/>
						</div>
						<div class="col-md-5 height-align">
							<label class='lab-1'>流水号：</label>
							<span>由系统自动生成</span>
						</div>	
							
<!-- 						<div class="col-md-5">
							<label>流水号：</label>
							<span></span>
						</div>	 -->
						<!-- <input type="hidden"  id='flowId' name='flowId' value=''/> -->
					</div>
				</div>
				<div class="col-md-6">
					<small class='btns-hometab pull-right'>
						<button type="submit" class="btn btn-info btn-margin" >保存</button>
						<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
						<a href="${url}" target='homeTab' class="btn btn-default btn-margin">返回</a>
					<!-- <button type='button' class="btn btn-default">返回</button> -->
					</small>
				</div>
			</div>
		</div>
	</section>
</form>
<script>
	(function(){
		var date=new Date();
		var Y=date.getFullYear();
		var M=date.getMonth()+1;M<10&&(M='0'+M);
		var D=date.getDate();D<10&&(D='0'+D);
		var H=date.getHours();H<10&&(H='0'+H);
		var Mu=date.getMinutes();Mu<10&&(Mu='0'+Mu);
		var S=date.getSeconds();S<10&&(S='0'+S);
		$('#buyTime').val(Y+'-'+M+'-'+D+' '+H+':'+Mu);
		var href=$('[data-dialog="dialog"]').attr('href');
 		$('[data-name="search"]').click(function(){
 			var dName = $("#dName").val().trim();
 			if(dName==""){
 				$('[data-warning="warning"]').html('请输入死者姓名').removeClass('nowarning').addClass('warning');
 				return;
 			}else{
 				var $form = $("#Form");
 				$.ajax({
 				    url: $form.attr("action"),
 				    dataType:'json',
 				    data:{method:'dNameSearch',dName:dName},
 				    success: function (json) {
 				    	if (json.statusCode==200) {
	 				    	var size = json.size;
		 				   	if(size===0){
		 		 				$('[data-warning="warning"]').html('无死者信息').removeClass('nowarning').addClass('warning');
		 		 			}else if(size===1){
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				//$("#certificateCode").val(typeof(json.certificateCode)!="undefined"?json.certificateCode:'');
		 		 				$("#dName").val(typeof(json.dName)!="undefined"?json.dName:'');
		 		 				$("#hideName").val(typeof(json.hideName)!="undefined"?json.hideName:'');
		 		 				$("#dNameId").val(typeof(json.dNameId)!="undefined"?json.dNameId:'');
	                   			$("#dSex").val(typeof(json.dSex)!="undefined"?json.dSex:'');
	                   			$("#dAge").val(typeof(json.dAge)!="undefined"?json.dAge:'');
	                   			$("#hallName").val(typeof(json.hallName)!="undefined"?json.hallName:'');
	                   			$("#hallTime").val(typeof(json.hallTime)!="undefined"?json.hallTime:'');
	                   			//$("#beginDate").val(typeof(json.beginDate)!="undefined"?json.beginDate:'');
	                   			//$("#endDate").val(typeof(json.endDate)!="undefined"?json.endDate:'');
	                   			$("#fareName").val(typeof(json.fareName)!="undefined"?json.fareName:'');
	                   			$("#fareTime").val(typeof(json.fareTime)!="undefined"?json.fareTime:'');
	                   			//$("#cremationTime").val(typeof(json.cremationTime)!="undefined"?json.cremationTime:'');
		 		 			}else{
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$('[data-dialog="dialog"]').attr('href',href+"&dName="+json.hideName);
		 		 				$('[data-dialog="dialog"]').click();
		 		 			}
 				    	} else {
	 				   		toastr["error"](json.message);
	 				   	}
	 				},
	 				error : function(XMLHttpRequest, textStatus, errorThrown) {
	 					toastr["error"](XMLHttpRequest.status);
	 					$("button").removeAttr("disabled");
	 				}
 				});
 			}
 		});
		
	})()
</script>
</body>
