<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
	
<script type="text/javascript">
var sum=0;//用于区分查出来的，和新添加的，10000条对于查出来的应该足够
var aTotal=0;//合计总金额
</script>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail"   action="" rel="myModal" ><!-- onsubmit="return validateHomeCallback(this,homeAjaxDone);" -->
			
			<div class="modal-dialog" role="document">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content"  style="border-radius:6px;">
				<div class="modal-body nopadding-T nopadding-B" >
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-2'>卡号：</label>
									<span>${lr.cardCode}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者性别：</label>
									<span>${dSex}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者姓名：</label>
									<span>${lr.dName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者年龄：</label>
									<span>${lr.dAge}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>灵堂号：</label>
									<span>${lr.mourningName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>守灵时间：</label>
									<span><fmt:formatDate value="${lr.mourningTime}" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>告别厅号：</label>
									<span>${lr.farewellName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>告别时间：</label>
									<span><fmt:formatDate value="${lr.farewellTime}" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>购买人：</label>
									<span>${lr.takeName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>购买时间：</label>
									<span><fmt:formatDate value="${buyTime}" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>与死者关系：</label>
									<%-- <span>${fOption}</span> --%>
								    <select class="input-content list_input input-dialog" id="relactive" name="relactive">
										${fOption}
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>领用时间：</label>
									<span><fmt:formatDate value="${takeawayTime}" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-12">
									<table class="table table-bordered">
									   <tbody>
										<thead>
											<tr>
												<th>名称</th>
												<th>单价</th>
												<th>数量</th>
												<th>小计</th>
												<th>挂账</th>
												<th>备注</th>
											</tr>
										</thead>
										</tbody>
										<tbody id="articles">
										     <c:set var="total" value="0"></c:set>
										     
										     <c:forEach items="${ld}" var="u">								        
										        <tr role="row">
										           <%-- <td style='text-align: center; vertical-align: middle;'>
										               <input type="checkbox"  class="checkBoxCtrl" value="${u.id}" />
										           </td> --%>
										           <td style='text-align: center; vertical-align: middle;'>									               
										               ${u.itemName}										               
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <fmt:formatNumber value='${u.pice}' pattern='#' type='number'/>
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               ${u.number}
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               <fmt:formatNumber value='${u.total}' pattern='#' type='number'/>
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>					               
										                   <c:choose>
										                      <c:when test="${u.iscredit=='0'}">
										                      &nbsp;否										                         
										                      </c:when>
										                      <c:otherwise>	
										                      &nbsp;是
										                      </c:otherwise>
										                   </c:choose>								                   
										               
										           </td>
										           <td style='text-align: center; vertical-align: middle;'>
										               ${u.remarks}								               
										           </td>
										        </tr>
										        <c:set var="total" value="${total+u.total}"></c:set>										      
										     </c:forEach>
									       </tbody>
										<tbody>
											
											<tr>
												<td>合计</td>
												<td colspan="5" style="text-align:left">
													<font color='red' id="articlesFont">
											        <fmt:formatNumber value='${total}' pattern='#' type='number'/>元
											       </font>
												</td>
											</tr>
										</tbody>
									</table>
									<div>
										<label>备注：</label>
										<span style='color:red;'>${lr.remarktext}</span>
									</div>
								</div>
								
							</div>
							
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-1'>经办人：</label>
									<span>${u.name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>代办员：</label>
									<span>${lr.replaceName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>流水号：</label>
									<span>${lr.flowId}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>购买单号：</label>
									<span>${lr.buyOrderId}</span>
								</div>
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
				
	</form>
	
</body>

<script>
aTotal=${total};
</script> 