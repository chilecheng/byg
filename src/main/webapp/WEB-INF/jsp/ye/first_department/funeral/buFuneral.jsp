<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">丧葬用品购买</div>
</section>

<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
//将点击的常用搜索按钮对应值改变，用于后台判断
var checkType='';
$("#typeDiv").on('click','button',function(){	
	checkType=$(this).attr('name');
	$("#checkType").val(checkType);
})
</script>
<form action="${url}?method=list&ids=" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<section class="content">
	<!-- -购买丧葬用品（主）- -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input nopadding-R" id="select_kind" name="select_kind">
<!-- 									<option value='k1'>购买单号</option> -->
<!-- 									<option value='k2'>购买人</option> -->
<!-- 									<option value='k3'>死者卡号</option> -->
<!-- 									<option value='k4'>死者姓名</option> -->
									${searchOption }
								</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" id='search_val' name='search_val' value='${search_val }' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>购买时间：</label> 
								<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i> <!-- ${cremationTime} -->
            					<span class='timerange'>-</span>  
								<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}" ><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							</div>
							<!-- 控制常用搜索状态（收费等） -->
							<input type="hidden" id="checkType" name="checkType" value="${checkType }">
							<div class="col-md-12" id="typeDiv">
								<label >常用：</label> 
								<button type="submit" class="btn btn-normally btn-all"  name='payAll'>全部</button>
								<button type="submit" class="btn btn-normally"  name='payYes'>已收费</button>
								<button type="submit" class="btn btn-normally"  name='payNo'>未收费</button>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content nopadding-T" id='main-content'>
		<div class='box'>
		
			<div class="row row-width">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 btns">
							<small class='btns-buy '>
								<a href="funeralBuy.do?method=buy" class="btn btn-warning" target='homeTab'>购买</a>
								<a href="funeralBuy.do?method=edit&id=" checkone="true" checkname="id" rel="myModal" class="btn btn-color-ff7043" target='homeTab'>修改</a>
								<a href="funeralBuy.do?method=delete&id=" target="ajaxTodo" checkname="id" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
							</small>
						</div>
					</div>
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th>
									  <div class="form-group nomargin-B">
									      <div class="checkbox nomargin-B nomargin-T">
									        <label>
									         	<input type="checkbox" group='id' class='checkBoxCtrl'>全选 
									        </label>
									      </div>
									  </div>
								</th>
								<th>购买单号</th>
								<th>死者姓名</th>
								<th>购买时间</th>
								<th>购买人</th>
								<th>灵堂号</th>
								<th>告别厅号</th>
								<th>告别时间</th>
								<th>火化时间</th>
								<th>是否收费</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${fn:length(page.list)==0 }">
							    <tr width="20">
					    			<td colspan="10">
								  		<font color="Red">还没有数据</font>
							 		</td>
							    </tr>
							</c:if>
						    <c:forEach items="${page.list}" var="u">
							<tr role="row">
								<td><input type='checkbox' class='checkBoxCtrl' name='id' value='${u.id}'/></td>
								<td>${u.buyOrderId}</td>
								<td>${u.dName}</td>
								<td><fmt:formatDate value="${u.buyTime}" pattern="yyyy-MM-dd HH:mm"/></td>
								<td><a href="funeralBuy.do?method=showOrderDialog&id=${u.id}" class="loser" target="dialog" rel="myModal">${u.takeName}</a></td>
								<td>${u.mourningName}</td>
								<td>${u.farewellName}</td>
								<td><fmt:formatDate value="${u.farewellTime}" pattern="yyyy-MM-dd HH:mm"/></td>
								<td><fmt:formatDate value="${u.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
								<td class="payFlag state1">${u.ifPay}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>
<script>
$(function(){
	timeRange();
	$('.payFlag').each(function(){
		//console.log(this);
		var data=$(this).text();
		switch (data){
			case '已收费':$(this).css({'color':'green'});
				break;
			case '未收费':
				$(this).css({'color':'red'});
				break;
		}
	})
});
</script>
