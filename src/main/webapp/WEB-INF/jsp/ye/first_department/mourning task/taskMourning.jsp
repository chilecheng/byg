<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">告别厅安排任务单</div>
</section>
<style type="text/css">
	.box.box-warning .box-body>div.col-md-12{
		padding-bottom:10px;
		border-bottom:1px solid #ddd;
	}
	p.col-md-2{
		color:#5AC6F7;
		line-height:30px;
	}
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	#main-content .list_input{
		min-width:35%;
	}
	select.list_input{
		color:#aaa;
	}
	#main-content .pagination{
		float:right;
	}
	#main-content table.table.table-bordered{
		margin-top:10px;
	}
	table.table.table-bordered thead div{
		margin:0px;
	}
	table.table.table-bordered tbody a.loser{
		display:block;
		width:100%;
		height:100%;
		color:#58C6F7;
	}
	table.table.table-bordered tbody td{
		height:30px;
	}
	#pageForm .content:last-child{
		padding-top:0px;
	}
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
</script>
<form action="first_department.do" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<section class="content">
	<!-- -告别厅安排任务单- -->
	
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
						<div class="box-body">
							
							<div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<label style="margin:8px;">至</label> 
								<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<button type="submit" class="btn btn-info btn-search btn-search-left">搜索</button>
							</div>
							<div class="row"  id='main-content'>
								<div class="col-md-12">
									<div class="row">
										<p class="col-md-3">2016-5-14 -2016-6-6</p>
										<div class="col-md-9">
											<small class='pull-right btns-print'>
<!-- 												<a href="" class="btn btn-color-ab47bc" role="button">打印</a> -->
<!-- 												<a href="" class="btn btn-success" role="button">导出EXCEL</a> -->
<!-- 												<a href="" class="btn btn-primary" role="button">导出WORD</a> -->
											</small>
										</div>
									</div>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>
													时间
												</th>
												<th>姓名</th>
												<th>性别</th>
												<th>年龄</th>
												<th>围花</th>
												<th>黑纱</th>
												<th>白纱</th>
												<th>孝球</th>
												<th>花门</th>
												<th>礼仪</th>
												<th>司仪</th>
												<th>地毯</th>
												<th>背景绢花台</th>
												<th>电子遗像</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
									<%@ include file="/common/pageFood.jsp"%>
								</div>
							</div>
						</div>
						
				</div>
				<!-- /.col -->
			</div>
			
		</div>
	</section>
	
</form>