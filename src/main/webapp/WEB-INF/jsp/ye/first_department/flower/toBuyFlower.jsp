<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<script type="text/javascript">
$(function(){
	timeRange();
	timeMunite();
});
var sum=0;//标识
var aTotal=0;//合计总金额
//项目名称改变
function itemName(id,type){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
	    	
	    	aTotal=aTotal-$("#total_"+id).val();
			aTotal+=json.pice;
		    $("#articlesFont").html(aTotal+"元");
		    $("#total_"+id).val(json.pice)
	    	
	    	
	    	
    		$("#pice_"+id).val(json.pice)
	    	$("#number_"+id).val(1)
	    	$("#comment_"+id).val("")
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 

//数目改变
function number(id,type){
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	if(a!=number){//表示是不是数字
		toastr["warning"]("请输入正确的数量！");
	}else{
			var pice=$("#pice_"+id).val();
	  		
    		aTotal=aTotal-$("#total_"+id).val();
    		
			aTotal+=pice*number;
		
	    	$("#articlesFont").html(aTotal+"元");
	
	    	$("#total_"+id).val(pice*number)
	}
} 
//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		aTotal=aTotal-$("#total_"+id).val();
		$("#articlesFont").html(aTotal+"元");

		$(this).parent().parent().remove();
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
function readData(form,callback) {
	var tbody=[];
	$("#articles").find("tr").each(function(i,tr){
		var trs='';
		$(tr).find("td").each(function(j,td){
			if(j>0){
					trs+=',';
			}
			trs+=$(td.children[0]).val();
		})
		tbody[i]=trs;
	})
	$("button").prop("disabled", "disabled");
	var $form = $(form);
	var va = validHomeForm($form);
	if (va == false) {
		$("button").removeAttr("disabled");
		toastr["warning"]("请确保填写完整并且格式正确");
		return false;
	}
	rel = $form.attr("rel");
	var datas= $form.serializeArray();
	var obj={};
	for(var key in datas){
		obj[datas[key].name]=datas[key].value;
	}
	obj.table=tbody;
	//datas.push({name:"table",value:tbody});
	//console.log(JSON.stringify(datas));
	$.ajax({
		type : 'POST',
		traditional: true,
		url : $form.attr("action"),
		data : obj,
		dataType : "json",
		cache : false,
		success : callback,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			toastr["error"](XMLHttpRequest.status);
			$("button").removeAttr("disabled");
		}
	});
	return false;
}
</script>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">花圈花篮订购</div>
</section>

<form action="${url}" id="Form" onsubmit="return readData(this,homeAjaxDone);">
<!-- -购买丧葬用品- -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<input name="method" value="${method}" type="hidden">
						<div class="cl border-B">
							<p class="col-md-12 p">基本信息</p>
						</div>
						<div class="box-body border-B">
							<div class="row padding-B">
								
								<div class="col-md-6  height-align">
									<label class='lab-6'>死者姓名：</label>
									<input type="text" class="list_input input-hometab" id='dName' name='dName' value=''/>
									<button class='btn-psearch btn btn-info' type='button' data-name='search'>搜索</button>
									<input type="text" readonly class="list_input" id='dNameId' data-id="dNameId" name='dNameId' hidden value=''/>
									<a data-dialog='dialog' class='hide' target='dialog' rel="myModal" href='${url}?method=dNameList'>选择</a>
									<span class='nowarning' data-warning='warning'></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>购买单号：</label>
									<input type="text" class="required list_input " id='ordernum' style='min-width:40%' name='ordernum' value=''/>&nbsp;<i class="fa fa-circle text-red"></i>
								</div>								
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者年龄：</label> 
									<input type="text" readOnly='readOnly' class="list_input input-hometab" id='dAge' name='dAge' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>购买人：</label> 
									<input list="buyer" autocomplete='off' class="required list_select"  style='min-width:40%' name='buyer' />&nbsp;<i class="fa fa-circle text-red"></i>
									<datalist  class="list_select"  id="buyer">
										<option value="亲友"></option>	
										<option value="朋友"></option>
									</datalist>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者性别：</label> 
									<input type="text" readOnly='readOnly' class="list_input input-hometab" id='dSex' name='dSex' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>联系电话：</label> 
									<input type="text" class="list_input" id='contact' style='min-width:40%' name='contact' value=''/>
								</div>
								
								
								<div class="col-md-6 height-align">
									<label class='lab-1'>灵堂号：</label> 
									<input type="text" readOnly='readOnly' class="list_input input-hometab" id='mourningPlace' name='mourningPlace' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>购买时间：</label> 
									<input type="text" class="required list_input" data-id="reservation" style='min-width:40%' id="buyTime" name='buyTime' value=''><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
<!-- 								<div class="col-md-6"> -->
<!-- 									<label class='lab-6'>是否单位购买：</label>  -->
<!-- 									<select class="list_input" id="isComBuy" name="isComBuy"> -->
<!-- 										<option value='1'>是</option> -->
<!-- 										<option value='2'>否</option> -->
<!-- 									</select> -->
<!-- 								</div> -->
								
								
								<div class="col-md-6 height-align">
									<label class='lab-6'>告别厅号：</label> 
									<input type="text" readOnly='readOnly' class="list_input" id='farewellPlace' style='min-width:40%' name='farewellPlace' value=''/>
								</div>
								<div class="col-md-6 wk-out height-align">
									<label class='lab-6'>布置人员：</label> 
									<input type="text" readonly class="list_input input-hometab" id='assiginUser' data-name="workerName" name='assiginUser' value=''/>
									<input type="text" readonly class="list_input" id='assiginUserId' data-id="workerId" name='assiginUserId' hidden value=''/>
									<a class='wk-choose btn btn-default' data-choose='get'>选择</a>
									<a class='hide' data-fun="wk"  target='dialog' href='${url}?method=choose&groupType=1'>选择</a>
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-6'>告别时间：</label> 
									<input type="text"  readOnly='readOnly' class="list_input" style='min-width:40%' id="farewellDate" name="farewellDate" value="<fmt:formatDate value="${farewellDate}" pattern="yyyy-MM-dd hh:mm"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>书写人：</label> 
									<select class="list_input input-hometab noppading-R" id="writer" name="writer">
										<option>请选择</option>
										${writer}
									</select>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-6'>守灵时间：</label> 
            						<input type="text" readonly="readonly" class="list_input input-hometab" name="beginDate" id="beginDate" /> 
            						<span class='timerange'>--</span>  
           							<input type="text" readonly="readonly" class="list_input input-hometab" name="endDate" id="endDate" />
								</div>
							</div>
							
						</div>
						<div class="cl border-B">
							<p class="col-md-12 p">用品购买</p>
						</div>
						<div class="box-body border-B ">
							<div class="row padding-B">
								<div class="col-md-12">
									<small class='btns-buy'>
										<a href="${url}?method=insert" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<button type="button" onclick="delHtml('id')" class="btn btn-danger ">删除</button>
									</small>
								</div>
							</div>
							<div class="row padding-B">
								<div class="col-md-12" style="padding-right:15px;">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th width='70px'>
													  <div class="form-group nomargin-B">
													      <div class="checkbox nomargin-B na-margin-T">
													        <label>
													         	<input type="checkbox" group='id' class='checkBoxCtrl'>全选 
													        </label>
													      </div>
													  </div>
												</th>
												<th>名称</th>
												<th>单价（元）</th>
												<th>数量</th>
												<th>小计</th>
												<th>敬献者自称</th>
												<th>对死者称呼</th>
												<th width='180px'>备注</th>
											</tr>
										</thead>
									<tbody>
									<tbody id="articles">
										
									</tbody>
									<tbody>
										<tr>
											<td >合计：</td>
											<td colspan='7' style='text-align: left;' >
											<font color='red' id="articlesFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
									</table>
									
								</div>
							</div>
							<div class='row padding-B'>
								<div class="col-md-12" style="padding-left:15px;">
									<div class="row">
										<span class="col-md-1">备注：</span> 
										<textarea class="col-md-11" style="margin-left:-15px;" rows="5" cols="150" placeholder="多行输入" id="comment" name="comment"></textarea>
									</div>
									
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>
			<div class="row padding-B">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6 height-align">
							<label class='lab-1'>经办人：</label>
							<span>${agentUser}</span>
						</div>
						<div class="col-md-6 height-align">
							<label class='lab-1'>代办人：</label>
							<input type="text" class="list_input input-dialog" id='bname' name='bname' value=''/>
						</div>
						<div class="col-md-6 height-align">
							<label class='lab-1'>流水号：</label>
							<span>由系统自动生成</span>
						</div>								
					</div>
				</div>
				<div class="col-md-6">
					<small class='btns-hometab pull-right'>
						<button type="submit" class="btn btn-info btn-margin" >保存</button>
						<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
						<a href="${url}?method=list" target="homeTab" rel="myModal" class="btn btn-default btn-margin">返回</a>
					</small>
				</div>
			</div>
		</div>
	</section>
	 <script>
	 	(function(){
	 		var date=new Date();
	 		var Y=date.getFullYear();
	 		var M=date.getMonth()+1;M<10&&(M='0'+M);
	 		var D=date.getDate();D<10&&(D='0'+D);
	 		var H=date.getHours();H<10&&(H='0'+H);
	 		var Mu=date.getMinutes();Mu<10&&(Mu='0'+Mu);
	 		var S=date.getSeconds();S<10&&(S='0'+S);
	 		$('#buyTime').val(Y+'-'+M+'-'+D+' '+H+':'+Mu);
	 		/*选择按钮初始化*/
	 		$('[data-choose="get"]').click(function(){
	 			data_group.name=$(this).prev().prev().attr('data-name');
	 			data_group.id=$(this).prev().attr('data-id');
	 			$(this).next().click();
	 		});
	 		
			var href=$('[data-dialog="dialog"]').attr('href');
	 		$('[data-name="search"]').click(function(){
	 			var dName = $("#dName").val().trim();
	 			if(dName==""){
	 				$('[data-warning="warning"]').html('请输入死者姓名').removeClass('nowarning').addClass('warning');
	 				return;
	 			}else{
	 				var $form = $("#Form");
	 				$.ajax({
	 				    url: $form.attr("action"),
	 				    dataType:'json',
	 				    cache: false,
	 				    data:{method:'dNameSearch',dName:dName},
	 				    success: function (json) {
	 				    	var size = json.size;
		 				   	if(size===0){
		 		 				$('[data-warning="warning"]').html('无死者信息').removeClass('nowarning').addClass('warning');
		 		 			}else if(size===1){
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$("#certificateCode").val(typeof(json.certificateCode)!="undefined"?json.certificateCode:'');
		 		 				$("#dName").val(typeof(json.name)!="undefined"?json.name:'');
		 		 				$("#dNameId").val(typeof(json.name)!="undefined"?json.dNameId:'');
	                   			$("#dSex").val(typeof(json.dSex)!="undefined"?json.dSex:'');
	                   			$("#dAge").val(typeof(json.dAge)!="undefined"?json.dAge:'');
	                   			$("#mourningPlace").val(typeof(json.mourningPlace)!="undefined"?json.mourningPlace:'');
	                   			$("#beginDate").val(typeof(json.beginDate)!="undefined"? json.beginDate : '');
	                   			$("#endDate").val(typeof(json.endDate)!="undefined"? json.endDate : '');
	                   			$("#farewellPlace").val(typeof(json.farewellPlace)!="undefined"?json.farewellPlace:'');
	                   			$("#farewellDate").val(typeof(json.farewellDate)!="undefined" ? json.farewellDate : '');
	                   			$("#cremationTime").val(typeof(json.cremationTime)!="undefined"?json.cremationTime:'');
		 		 				
		 		 			}else{
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$('[data-dialog="dialog"]').attr('href',href+"&dName="+dName);
		 		 				$('[data-dialog="dialog"]').click();
		 		 			}
		 				}
	 				});
	 			}
	 		});
	 	})()
	 </script>
</form>
