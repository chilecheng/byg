<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">基本信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者姓名：</label>
									<span>${deadName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>购买人：</label>
									<span>${orderUser }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者年龄：</label>
									<span>${dAge }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>联系电话：</label>
									<span>${contact }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者性别：</label>
									<span>${dSex }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>购买时间：</label>
									<span>${buyTime }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>灵堂号：</label>
									<span>${mourningPlace }</span>
								</div>
<!-- 								<div class="col-md-6"> -->
<!-- 									<label class='lab-6'>是否单位购买：</label> -->
<%-- 									<span>${unitBuyFlag }</span> --%>
<!-- 								</div> -->
								<div class="col-md-6 height-align">
									<label class='lab-6'>守灵时间：</label>
									<span>${mourningTime }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>书写人：</label>
									<span>${writeUser }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>告别厅号：</label>
									<span>${farewellPlace }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>布置人员：</label>
									<span>${assignUser }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>告别时间：</label>
									<span><fmt:formatDate value="${farewellTime}" pattern="yyyy-MM-dd hh:mm"/></span>
								</div>
								
								
							</div>
							
						</div>
						<p class="col-md-12 p border-B">用品购买</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>名称</th>
												<th>单价（元）</th>
												<th>数量</th>
												<th>小计</th>
												<th>敬献者自称</th>
												<th>对死者称呼</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody>
										<c:set var="total" value="0"></c:set>
										<c:forEach var="u" items="${list}">
										<tr>
										<td>${u.item.name}</td>
										<td><fmt:formatNumber value='${u.item.pice}' pattern='#' type='number'/></td>
										<td>${u.number}</td>
										<td><fmt:formatNumber value='${u.total}' pattern='#' type='number'/></td>
										<td>${u.selfCall}</td>
										<td>${u.deadCall}</td>
										<td>${u.comment}</td>
										<c:set var="total" value="${total+u.total}"></c:set>
										</tr>
										</c:forEach>
											<tr>
												<td>小计</td>
												<td colspan="6" style="text-align:left">
													<span><fmt:formatNumber value='${total}' pattern='#' type='number'/></span>
												</td>
											</tr>
										</tbody>
									</table>
									<div>
										<label>备注：</label>
										<span>${comment}</span>
									</div>
								</div>
							</div>
						</div>
						<p class="col-md-12">办理信息</p>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-1'>经办人：</label>
									<span>${creatUser}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>代办员：</label>
									<span>${agentUser}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>流水号：</label>
									<span>${serialNumber }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>购买单号：</label>
									<span>${orderNumber }</span>
								</div>
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
				
	</form>
</body>
