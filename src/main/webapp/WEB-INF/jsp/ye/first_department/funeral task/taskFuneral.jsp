<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">丧葬用品核对表</div>
</section>
<style type="text/css">
	.deal{
		color:#4DA2FD;
	}
	.receive{
		color:#00A65A;
	}
	.nodeal{
		color:#D73925;
	}
	p.col-md-2{
		color:#5AC6F7;
		line-height:30px;
	}
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	#main-content .list_input{
		min-width:35%;
	}
	select.list_input{
		color:#aaa;
	}
	#main-content .pagination{
		float:right;
	}
	.content:last-child{
		min-height:0px;
	}
	.content:last-child p{
		padding:10px;
		margin:0px;
		border-bottom:1px solid #ccc;
	}
	#main-content table.table.table-bordered{
		margin-top:10px;
	}
	table.table.table-bordered thead div{
		margin:0px;
	}
	table.table.table-bordered tbody a.loser{
		display:block;
		width:100%;
		height:100%;
		color:#58C6F7;
	}
	table.table.table-bordered tbody td{
		height:30px;
	}
	#pageForm .content:last-child{
		padding-top:0px;
	}
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
function selectVlaue(){
	var names=$('#names').val();
	$('#get').val(names);
}
</script>
<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input type="hidden" name="itemId" value="${itemOrder.id }" >
	<section class="content">
	<!-- -丧葬用品核对表- -->
	
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input" id="names" name="name" onChange="seclectValue()">
									<option selected="selected">请选择</option>
									<option value="">卡号</option>
									<option value="">死者名字</option>
								</select>
							</div>
							<div class="col-md-6">
								<label>查询值：</label> 
								<input type="text" class="list_input" id='get' name='get' value='' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<label style="margin:8px;">至</label> 
								<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<button type="button" class="btn btn-normally btn-all">今日领用</button>
								<button type="button" class="btn btn-normally">明日领用</button>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>
		
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<p class="col-md-3">2016-5-14 -2016-6-6</p>
						<div class="col-md-9">
							<small class='pull-right btns-print'>
<!-- 								<a href="" class="btn btn-color-ab47bc" role="button">打印</a> -->
<!-- 								<a href="" class="btn btn-success" role="button">导出EXCEL</a> -->
<!-- 								<a href="" class="btn btn-primary" role="button">导出WORD</a> -->
							</small>
						</div>
					</div>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>
									死者姓名
								</th>
								<th>年龄</th>
								<th>性别</th>
								<th>厅</th>
								<th>告别时间</th>
								<th>备注</th>
								<th>状态</th>
								<th>包布盒</th>
								<th>小花圈</th>
								<th>保护剂</th>
								<th>骨灰盒</th>
								<th>红孝带</th>
								<th>寿被</th>
								<th>寿衣</th>
								<th>雨伞</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><a href="dispatcher.do?list=first_department-funeral-charFuneral" class="loser" target="dialog" rel="myModal">小明</a></td>
								<td>85</td>
								<td>男</td>
								<td>3</td>
								<td>2016.6.16-11:11</td>
								<td></td>
								<td>
									<select name="state" id="state" class="deal">
										<option value="deal" class="deal">已处理</option>
										<option value='nodeal' class='nodeal'>未处理</option>
										<option value='receive' class="receive">已领取</option>
									</select>
								</td>
								<td>3333</td>
								<td>3333</td>
								<td>3333</td>
								<td>3333</td>
								<td>3333</td>
								<td>3333</td>
								<td>3333</td>
								<td>3333</td>
							</tr>
							
						</tbody>
					</table>
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="box">
				<p>
					<label>统计时间</label>
					<span>2016-6-6 12:12:12</span>
				</p>
			<div class="box-body">
				<div class="row">
					<span class="col-md-2">圣经：860*1</span>
					<span class="col-md-2">圣经：860*1</span>
					<span class="col-md-2">圣经：860*1</span>
					<span class="col-md-2">圣经：860*1</span>
					<span class="col-md-2">圣经：860*1</span>
					<span class="col-md-2">圣经：860*1</span>
				</div>
			</div>
			
		</div>
	</section>
</form>
<script>
	(function(){
		$("#state").change(function(){
			var value=$(this).val();
			var className=$("#state option[value='"+value+"']").attr('class');
			$(this).attr('class',"").addClass(className);
		})
	})()
</script>