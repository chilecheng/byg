
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" role="document">
	<div class='modal-content'>
		<div class='modal-body'>
			<div class='row'>
				<div class='col-md-6'>
					<span>死者姓名：</span>
					<label>${bwr.commissionOrder.name }</label>
				</div>
				<div class='col-md-6'>
					<span class='lab-2'>卡号：</span>
					<label>${commissionOrder.cardCode }</label>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-6'>
					<span>灵堂号：</span>
					<label>${commissionOrder.mourning.name}</label>
				</div>
				<div class='col-md-6'>
					<span>设灵时间：</span>
					<label><fmt:formatDate value="${commissionOrder.mourningTime}" pattern="yyyy-MM-dd hh:mm"/></label>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-6'>
					<span>告别厅号：</span>
					<label>${commissionOrder.farewell.name}</label>
				</div>
				<div class='col-md-6'>
					<span>告别时间：</span>
					<label><fmt:formatDate value="${commissionOrder.farewellTime}" pattern="yyyy-MM-dd hh:mm"/></label>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-12'>
					<table class='table table-bordered' style='margin-bottom:0px;'>
						<thead>
							<tr>
								<th>花圈花篮名称</th>
								<th>单价</th>
								<th>数量</th>
								<th>小计</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>合计</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<#if list ?? && (list?size > 0)>
						    	<#list list as u>
									<tr role="row">
				    					<td>${(u.fishingManage.gone)! }</td>
				    					<td>
				    						<@util.state value="${(u.fishingManage.isArrive)!}" color1="green" color2="red" name1="已到港" name2="未到港" />  
			    						</td>
			    						<td>
				    						<@util.state value="${(u.fishingManage.isAbroad)!}" color1="green" color2="red" name1="是" name2="否" />  
			    						</td> 
			    						<td>${(u.fishingManage.time?string("yyyy-MM-dd"))! }</td>			    						
			    						<td>${(u.fishingManage.comment)! }</td>
									</tr>
								</#list>
								<#else>
								 	<tr>
						    			<td colspan="5">
									  		<font color="Red">还没有数据</font>
								 		</td>
								    </tr>
							</#if>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer btns-dialog btns-print">
			<button type="submit"  class="btn btn-info btn-margin btn-color-ab47bc">打印</button>
			<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
		</div>
	</div>
</div>	