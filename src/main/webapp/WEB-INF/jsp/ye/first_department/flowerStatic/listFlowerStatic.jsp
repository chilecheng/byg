
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">花圈花篮统计单</div>
</section>
<script>
$(function(){
	timeRange();
})
</script>
<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="" type="hidden">
	<section class="content">
	<!-- -购买丧葬用品（主）- -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
					</div>
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input nopadding-R" id="type" name="type">
							</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" id='search' name='search' value='' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-id='beginDate' class="list_select" id="begin" name="begin" value="${begin }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>-</span> 
								<input type="text" data-id='endDate' class="list_select" id="end" name="end" value="${end }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search" >搜索</button>
							</div>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content onpadding-T" id='main-content'>
		<div class='box'>
		
			<div class="row row-width">
				<div class="col-md-12">
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th>卡号</th>
								<th>死者姓名</th>
								<th>家属姓名</th>
								<th>灵堂号</th>
								<th>设灵开始时间</th>
								<th>告别厅号</th>
								<th>告别时间</th>
								<th>花圈花篮数量</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
							    <c:when test="${fn:length(page.list)==0 }">
								    <tr width="20">
						    			<td colspan="10">
									  		<font color="Red">还没有数据</font>
								 		</td>
								    </tr>
							    </c:when>
							    	<c:otherwise>
							    	<c:forEach items="${page.list}" var="u">
										<tr>
											<!-- <td><input type='checkbox' class='checkBoxCtrl' name='id' value=""/></td> -->
											<td>${u[0]}</td>
											<td>${u[1]}</td>
											<td>${u[2]}</td>
											<td>${u[3]}</td>
											<td>
												<!-- <a href="buyWreathRecord.do?method=show&id=" class="loser" target="dialog" rel="myModal"></a> -->
												<fmt:formatDate value="${u[4]}" pattern="yyyy-MM-dd hh:mm"/>
											</td>
											<td>${u[5]}</td>
											<td><fmt:formatDate value="${u[6]}" pattern="yyyy-MM-dd hh:mm"/></td>
											<td></td>
											<td><a href="buyWreathRecord.do?method=readOnly&id=${bwr.id }" style='width:auto;height:auto;' class="btn btn-success" target="dialog" rel="myModal">花圈花篮详情</a></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>

