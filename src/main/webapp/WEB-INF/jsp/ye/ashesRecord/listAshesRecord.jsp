<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<script>
function changePage(pageNum){
	$("#pageNum").val(pageNum);
	$("#Form").submit();
}
function changePageHere(form) {
	var $form = $(form);
	$.ajax({
		type : 'POST',
		url : $form.attr("action"),
		data : $form.serializeArray(),
		traditional: true,
		dataType : "html",
		cache : false,
		success : function(json) {
			$("#tck").html(json);
			initTab();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			toastr["error"](XMLHttpRequest.status);
		}
	});
	return false;
}
//单元格单击
function td_click(i){
	$("#placeCode").val(i);
   	$("#choosePosition").modal("hide");
}
</script>
<!-- type=choose是骨灰寄存添加页面跳出选择框的情况 -->
<c:if test="${type=='choose' }">
	<div class="modal-dialog" role="document" id="tck" style="width:1200px;">
</c:if>
<c:if test="${type=='' }">
	<section class="content-header">
		<i class="title-line"></i>
		<div class="title">骨灰寄存</div>
	</section>
	<section class="content">
</c:if>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<c:if test="${type=='' }">
						<form action="${url }" id="Form" onsubmit="return homeSearch(this);">
					</c:if>
					<c:if test="${type=='choose' }">
						<form action="${url }" id="Form" onsubmit="return changePageHere(this);">
						<input name="type" value="choose" type="hidden" />
					</c:if>
						<input name="method" value="${method}" type="hidden" />
						<%@ include file="/common/pageHead.jsp"%>
						<div class="box-body">
							<div class='row'>
								<c:if test="${type=='' }">
									<div class="col-md-3 btns-list">
										<a href="${url}?method=ashesRecordList" target="homeTab" rel="myModal" id="list" class="btn btn-info" role="button">进入列表</a>
									</div>
								</c:if>
								<div class="col-md-9">
									<ul class="la_ul">
										<li><div>空闲</div></li>
										<li style="background:#29b6f6;"><div>占用</div></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="box-body">
							<div id="example2_wrapper"
								class="dataTables_wrapper form-inline dt-bootstrap">
								<div class="row">
									<div class="col-sm-12">
										<table class="table table-bordered table-hover1" style="border:none">
											<c:forEach items="${page.list }" var="u">
												<tr class="row">
													<c:forEach var="j" begin="0" end="9" step="1">
														
														<c:if test="${u[j]==null }">
															<td></td>
														</c:if>
														<c:if test="${type=='' }">
															<c:if test="${u[j]!=null }">
																<c:choose>
									   								<c:when test="${fn:contains(u[j], ':')}">   
									   									<td style="background:#29b6f6;" title="${u[j]}"><a href="${url}?method=show&placeName=${u[j]}" style='color:#fff;' target="dialog" rel="myModal">${fn:substringBefore(u[j], ":")}</a></td>
									   								</c:when>
																	<c:otherwise> 
									   									<td><a href="${url}?method=edit&placeCode=${u[j]}&pageNum=${pageNum}" target="homeTab" rel="myModal">${u[j]}</a></td> 
									  								</c:otherwise>
								  								</c:choose>
															</c:if>
														</c:if>
														<c:if test="${type=='choose' }">
															<c:if test="${u[j]!=null }">
																<c:choose>
									   								<c:when test="${fn:contains(u[j], ':')}">   
									   									<td style="background:#29b6f6;"><a>${u[j]}</a></td>
									   								</c:when>
																	<c:otherwise> 
									   									<td onclick="td_click('${u[j]}')"><a>${u[j]}</a></td> 
									  								</c:otherwise>
								  								</c:choose>
															</c:if>
														</c:if>
													</c:forEach>
												</tr>
											</c:forEach>	
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="dataTables_paginate">
											<ul class="pagination">
		
												<li
													class="paginate_button previous <c:if test="${page.pageNum == 1 }">disabled</c:if>"
													id="example2_previous"><a
													href="JavaScript:changePage('1');">首页</a></li>
												<c:forEach begin="1" end="${page.pages }" var="p">
													<c:if test="${p<(page.pageNum+3) && p>(page.pageNum-3)}">
														<li
															class="paginate_button <c:if test="${p == page.pageNum}">active</c:if>">
															<a href="JavaScript:changePage('${p}');">${p}</a>
														</li>
													</c:if>
												</c:forEach>
												<c:if test="${page.pages - page.pageNum > 3}">
													<li class="paginate_button"><a
														href="JavaScript:changePage('${page.pageNum+3}');">...</a></li>
												</c:if>
												<li
													class="paginate_button next <c:if test="${page.pageNum == page.pages }">disabled</c:if>">
													<a href="JavaScript:changePage('${page.pages}');">末页</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
					</div>
						</div>
					</form>
				</div>
			</div>
		</div>
<c:if test="${type=='' }">
	</section>
</c:if>
<c:if test="${type=='choose' }">
	</div>
</c:if>