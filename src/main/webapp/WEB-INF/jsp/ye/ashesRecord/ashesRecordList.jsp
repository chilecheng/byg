<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">骨灰寄存列表</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title" style="color:#4EC2F6">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>类型：</label> 
							<select class="list_select nopadding-R" name="type" style="width:52%;" ">
<!-- 								<option value="code" name="code">寄存编号</option> -->
<!-- 								<option value="dName" name="dName">火化人姓名</option> -->
<!-- 								<option value="sName" name="sName" >寄存人姓名</option> -->
								${searchOption }
							</select>
						</div>
						<div class="col-md-8" style="margin-left:-94px">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" name="name" value="${name}" placeholder="查询值">
						</div>
						<div class="col-md-12">
							<label>到期日期：</label> 
							<input data-id='beginDate' class="list_select" id="dTime" name="beginDate" value="${beginDate}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<span class='timerange'>--</span>
							<input data-id='endDate' class="list_select" id="dTime" name="endDate" value="${endDate}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
						<!-- 控制常用搜索状态（收费等） -->
						<input type="hidden" id="checkType" name="checkType" value="${checkType }">
						<div class="col-md-12" id="typeDiv">
							<label >常用：</label> 
							<button type="submit" class="btn btn-normally btn-all"  name='flagAll'>全部</button>
							<button type="submit" class="btn btn-normally"  name='flagOut'>已领出</button>
							<button type="submit" class="btn btn-normally"  name='flagIn'>寄存中</button>
						</div>
						<div class="box-body" style="margin-top: -10px">
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-buy  btns-charge'>
						<a href="${url}?method=edit" target="homeTab" rel="myModal"  class="btn btn-warning" role="button">添加</a>
						<a href="${url}?method=renew&ashesRecordId=" target="dialog" checkName="ashesRecordId" checkOne="true" rel="myModal"  class="btn btn-success" role="button">续费</a>
						<button type='button'  class='btn btn-danger' checkone="true" checkname="ashesRecordId" data-fun='freezer' >领出</button>
						<a href="${url}?method=out&ashesRecordId=" target="dialog" checkName="ashesRecordId"  checkOne="true" rel="myModal"  class="btn btn-danger hide" role="button" >领出</a>
						<a href="${url}?method=list" target="homeTab" rel="myModal" class="btn btn-default">返回</a>
					</small>
					<small class="pull-right">
					
					
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"></th>
											<th>业务流水号</th>
											<th>死者编号</th>
											<th>火化人姓名</th>
											<th>寄存证编号</th>
											<th>性别</th>
											<th>年龄</th>
											<th>存放位置</th>
											<th>存入时间</th>
											<th>到期时间</th>
											<th>寄存状态</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr width="20">
								    			<td colspan="11">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
									     	<c:set var="index" value="0"></c:set>
										    <c:forEach items="${page.list}" var="u">
										   		
										    	<tr role="row" width="10">
													<td><input type="radio" name="ashesRecordId" value="${u.id}"></td>
							    					<td>${u.serialNumber}</td>
							    					<td>${u.dieCode}</td>
							    					<td><a href="${url}?method=showMessage&id=${u.id}" target="dialog"  rel="myModal"  role="button">${u.dName}</a></td>
							    					<td>${u.code}</td>
							    					<td>${u.sexName}</td>
							    					<td>${u.dAge}</td>
							    					<td>${u.ashesPositionId}</td>
							    					<td>${u.beginDate}</td>
							    					<td>${u.endDate}</td>
							    					<c:if test="${u.statusFlag==1 }">
														<td style='color:#29b6f6;' class='state1'>寄存中</td>
											  		 </c:if>
							    					 <c:if test="${u.statusFlag==2 }">
														<td style='color:#e84e40;' class='state1'>已领出</td>
											  		 </c:if>
						    					</tr>
<%-- 						    					<c:set var="index" value="${index+1}"></c:set> --%>
						    					
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
<script>

timeRange();
//将点击的常用搜索按钮对应值改变，用于后台判断
var checkType='';
$("#typeDiv").on('click','button',function(){	
	checkType=$(this).attr('name');
	$("#checkType").val(checkType);
})


$('[data-fun="freezer"]').click(function(){
	debugger;
	var checkname=$(this).attr('checkname');
	var value=$('[name="'+checkname+'"]:checked').parent().siblings().eq(9).text().trim();
	if(value==='已领出'&&$('[name="'+checkname+'"]:checked').size()!==0){
		toastr["warning"]("此骨灰已领出");
	}else{
		$(this).next().click();
	}
})
</script>