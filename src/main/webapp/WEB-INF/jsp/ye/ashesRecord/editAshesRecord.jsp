<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
 <script src="js/updateDate.js"></script>
 <script>
 $(function(){
		timeMunite();
	});
 </script>
</head>
<body>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">骨灰寄存</div>
</section>
<section class="content">
<form action="${url}" id="pageForm" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		<input type="hidden" name="method" value="save" >
		<input type="hidden" name="currentTime" value="${currentTime}" >
		<input type="hidden" name="pageNum" value="${pageNum}" >
			<div class='box box-warning'>
			<!-- 基本信息 -->
				<div class="box-header with-border nopadding-T">
					<div class='row'>	
						<p class='col-md-12 p border-B'>死者信息</p>
						<div class='col-md-12 border-B padding-B'>
							<div class='row'>
								<div class="col-md-6 height-align">
										<label class='lab-5'>死者姓名：</label>
<!-- 										<select style="width: 174px;position:absolute;" class="input-content" name="dName" id="dName"> -->
<%-- 											${dName} --%>
<!-- 										</select> -->
<!-- 										<input  type="text" autocomplete="off" name="input-box" class="input-show" id="show" onkeydown="if(event.keyCode == 13) return false"/> -->
<!-- 										<div id="prompt-content" class="show-menu"></div> -->
										<input type="text" class="list_select" id='dName' name='dName' value=''/>
										<button class='btn-psearch btn btn-info' type='button' data-name='search'>搜索</button>
										<input type="text" readonly class="list_input" id='dNameId' data-id="dNameId" name='dNameId' hidden value=''/>
										<a data-dialog='dialog' class='hide' target='dialog' href='buyWreathRecord.do?method=dNameList'>选择</a>
										<span class='nowarning' data-warning='warning'></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>证件号码：</label> 
									<input type="text" class="list_select" name="certificateCode" id="certificateCode" value=""/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>死者性别：</label> 
									<input type="text" class="list_select" name="dSex" id="dSex" value=""/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>死者年龄：</label>
									<input type="text"  class="list_select" name="dAge" id="dAge" value="">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>火化日期：</label> 
									<input type="text" data-id='beginDate' data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="cremationTime" name="cremationTime" value=""><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>存入时间：</label> <!-- data-provide="datetimepicker" -->
									<input type="text" data-id='beginDate' data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="nowDate" name="beginDate"   value=""	>
									<i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>存放月数：</label> 
									<input type="text" class="required list_select" name="depositLong" id="depositLong" onkeyup="update(this,'#nowDate','#asendDate',${pice},true)">
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>到期时间：</label> <!-- data-provide="datetimepicker" -->
									<input type="text" data-id='beginDate' data-min-view="2" data-date-format="yyyy-mm-dd" class="required list_select" id="asendDate" name="endDate" value="<fmt:formatDate value="${endTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>存放位置：</label> 
									<input style="width: 169px" readonly="readonly" class="required list_select" name="placeCode" id="placeCode" value="${placeCode}">
									<a href="${url}?method=list&type=choose" target="dialog" rel="choosePosition" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>寄存金额：</label> 
									<input type="text"  class="list_select" id="total" name="total" value="">
									
								</div>
							</div>
						</div>	
						<p class='col-md-12 p border-B'>寄存人信息</p>	
						<!-- 寄存人信息 -->
								<div class="col-md-6 height-align">
									<label  class='lab-5'>寄存人姓名：</label> 
									<input type="text" class="list_select" name="sName" value="">
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-5'>寄存证编号：</label> 
									<input type="text" class="list_select" name="sCode" value="">
	
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-5'>与死者关系：</label> 
									<select style="width: 169px" class="list_select" name="fAppellationId" id="fAppellationId">
										${fAppellationOption}
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-5'>寄存人电话：</label>
									<input type="text" class="list_select" name="sPhone" value="">
								</div>
								<div class="col-md-10">
									<label  class='lab-5'>寄存人住址：</label> 
									<input type="text" class="list_select" name="sAddr" value="">
								</div>
								<div class="col-md-12">
									<label  class='lab-5'>备注：</label> 
									<textarea type="text" class="list_input textarea-hometab" name="comment" value=""></textarea>
								</div>
						</div>
					</div>
				</div>	
				<div class='box'>	
					<div class="box-body">
						<div  class="container-fluid">
							<div class='col-md-12'>
								<div class='row'>
									<div class="col-md-6 height-align">
									<label class='lab-5'>经办人：</label>
										${agentUser}
									</div>
									<div class="col-md-6 height-align">
									<label class='lab-5'>办理时间：</label>
										${currentTime}
									</div>
									<div class="col-md-6 height-align">
									<label  class='lab-5'>单据号：</label> 
									<input type="text" readonly="readonly" class="list_select" name="code" value="由系统自动生成">
									</div>
									<div class="col-md-6 height-align">
							<small class="pull-right btns-hometab"> 
								<button type="submit"  class="btn btn-info">保存</button>
								<input type="reset" class="btn btn-color-9E8273" value="重置" />
								<c:if test="${flag==1}">
								<a href="${url}?method=list&pageNum=${pageNum}" target="homeTab"  class="btn btn-default">返回</a>
								</c:if>
								<c:if test="${flag==2}">
								<a href="${url}?method=ashesRecordList&pageNum=${pageNum}" target="homeTab"  class="btn btn-default">返回</a>
								</c:if>
							</small>
							</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
					</form>
</section>
<!--  <script src="js/select.js"></script> -->
<script>
	 	(function(){
			var href=$('[data-dialog="dialog"]').attr('href');
	 		$('[data-name="search"]').click(function(){
	 			var dName = $("#dName").val().trim();
	 			if(dName==""){
	 				$('[data-warning="warning"]').html('请输入死者姓名').removeClass('nowarning').addClass('warning');
	 				return;
	 			}else{
	 				var $form = $(pageForm);
	 				$.ajax({
	 				    url: 'buyWreathRecord.do',
	 				    dataType:'json',
	 				    cache: false,
	 				    data:{method:'dNameSearch',dName:dName},
	 				    success: function (json) {
	 				    	console.log(json);
	 				    	var size = json.size;
		 				   	if(size===0){
		 		 				$('[data-warning="warning"]').html('无死者信息').removeClass('nowarning').addClass('warning');
		 		 			}else if(size===1){
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$("#certificateCode").val(typeof(json.certificateCode)!="undefined"?json.certificateCode:'');
		 		 				$("#dName").val(typeof(json.name)!="undefined"?json.name:'');
		 		 				$("#dNameId").val(typeof(json.name)!="undefined"?json.dNameId:'');
	                   			$("#dSex").val(typeof(json.dSex)!="undefined"?json.dSex:'');
	                   			$("#dAge").val(typeof(json.dAge)!="undefined"?json.dAge:'');
	                   			$("#mourningPlace").val(typeof(json.mourningPlace)!="undefined"?json.mourningPlace:'');
	                   			$("#nowDate").val(typeof(json.nowDate)!="undefined"?json.nowDate:'');
	                   			/* $("#asendDate").val(typeof(json.endDate)!="undefined"?json.endDate:''); */
	                   			$("#farewellPlace").val(typeof(json.farewellPlace)!="undefined"?json.farewellPlace:'');
	                   			$("#farewellDate").val(typeof(json.farewellDate)!="undefined"?json.farewellDate:'');
	                   			$("#cremationTime").val(typeof(json.cremationTime)!="undefined"?json.cremationTime:'');
		 		 				
		 		 			}else{
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$('[data-dialog="dialog"]').attr('href',href+"&dName="+dName);
		 		 				$('[data-dialog="dialog"]').click();
		 		 			}
		 				}
	 				});
	 			}
	 		});
	 	})()
</script>
</body>