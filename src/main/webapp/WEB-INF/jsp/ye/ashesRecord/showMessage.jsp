<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>


<div class="modal-dialog nav-tabs-custom" role="document">
<!-- 基本信息 -->
						
<p class='p border-B'>死者基本信息</p>
	<div class="box-body border-B padding-B">
		<div class="col-md-5 height-align">
			<label class='lab-6 height-align'>死者姓名：</label>
			${ashesRecord.dName}
		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>死者性别：</label> 
			${ashesRecord.sexName}
		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>死者年龄：</label>
			${ashesRecord.dAge}
		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>火化日期：</label> 
			${ashesRecord.cremationTime}
			
		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>存入时间：</label> 
			${ashesRecord.beginDate}
			
		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>存放位置：</label> 
			${ashesRecord.ashesPositionId}
				
		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>到期时间：</label> 
			${ashesRecord.endDate}
			
		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>存放月数：</label> 
			${ashesRecord.depositLong}
			
		</div>

	</div>

<!-- 寄存人信息 -->
<p class='border-B  p'>寄存人信息</p>
	<div class="box-body border-B padding-B">
		<div class="col-md-5 height-align">
			<label class='lab-6'>
				寄存人姓名：
			</label>
			${ashesRecord.sName}

		</div>
		<div class="col-md-7 height-align">
			<label class='lab-6'>
				寄存证编号：
			</label>
			${ashesRecord.code}

		</div>
		<div class="col-md-5 height-align">
			<label class='lab-6'>
				与死者关系：
			</label>
			${appellation}

		</div>
		<div class="col-md-7 height-align">
			<label class='lab-6'>
				寄存人电话：
			</label>
			${ashesRecord.sPhone}
		</div>
		<div class="col-md-12">
			<label class='lab-6'>
				寄存人住址：
			</label>
			${ashesRecord.sAddr}
		</div>
		<div class="col-md-12">
			<label class='lab-6'>记录备注：</label>
			${ashesRecord.comment}
		</div>
	</div>
	
<!-- 领取人信息 -->
<c:choose>
	<c:when test="${ashesRecord.statusFlag ==2 }">
		<p class='border-B  p'>领取人信息</p>
		<div class="box-body border-B padding-B">
			<div class="col-md-5 height-align">
				<label class='lab-6'>
					领取人姓名：
				</label>
				${ashesRecord.takeName}
			</div>
			<div class="col-md-5 height-align">
				<label class='lab-6'>
					联系电话：
				</label>
				${ashesRecord.takePhone}
			</div>
			<div class="col-md-5 height-align">
				<label class='lab-6'>
					领取时间：
				</label>
				${ashesRecord.takeTime}
			</div>
			<div class="col-md-5 height-align">
				<label class='lab-6'>操作员：</label>
				${userName}
			</div>
		</div>
	</c:when>
</c:choose>


<div class="modal-footer btns-dialog">
<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
</div>
				
</div>

