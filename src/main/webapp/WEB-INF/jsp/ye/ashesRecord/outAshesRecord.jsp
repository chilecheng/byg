<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<script type="text/javascript">


//死者基本信息
</script>

</head>
<body>


<section class="content">
<form action="${url}" id="form" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="ashesRecordId" value="${ashesRecordId}" >
		<input type="hidden" name="currentTime" value="${currentTime}" >
		
			<div class="modal-dialog  nav-tabs-custom" role="document">
				
				
					
			<!-- 基本信息 -->
						
						<p class='p border-B'>业务信息</p>
							<div class="box-body border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
								
									<label class='lab-6'>火化人姓名：</label>
									${dName}
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-3'>性别：</label> 
									${dSex}
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-3'>年龄：</label>
									${dAge}
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>到期时间：</label> 
									${endDate}
									
								</div>
								<div class="col-md-6">
									<label class='lab-1'>火化日期：</label> 
									
									${cremationTime}
								</div>
								<div class="col-md-6">
									<label class='lab-1'>存入时间：</label> 
									${beginDate}
								</div>
							<div class="col-md-6">
									<label class='lab-1'>存放位置：</label> 
									${placeCode}
									
								</div>
								<div class="col-md-6">
									<label class='lab-6'>寄存人姓名：</label> 
									${sName}
									
								</div>
								<div class="col-md-6">
									<label class='lab-1'>到期时间：</label> 
									${endDate}
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>与死者关系：</label> 
									${appellation}
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>寄存证编号：</label> 
									${code}
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>寄存人住址：</label> 
									${sAddr}
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>寄存人电话：</label> 
									${sPhone}
									
								</div>
								<div class="col-md-6 height-align">
								<label class='lab-1'>领出日期：</label> 
								<input  type="text" data-provide="datetimepicker" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select input-dialog" id="takeTime" name="takeTime" value="<fmt:formatDate value="${takeTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>领出人：</label>
									<input type="text"  class="list_select input-dialog" name="takeName" id="takeName" value="">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>联系电话：</label>
									<input type="text"  class="list_select input-dialog" name="takePhone" id="takePhone" value="">
								</div>
								<div class="col-md-12">
									<label  class='lab-3'>备注：</label> 
									<textarea type="text" class="form-control textarea-dialog" name="comment" >${comment }</textarea>
								</div>
								</div>
								</div>
							<p class='p border-B'>办理信息</p>	
								
							<div class="box-body border-B padding-B">
							<div class="col-md-5 height-align">
									<label class='lab-6'>经办人：</label>
									${agentUser}
							</div>
					
					
					
							<div class="col-md-7 height-align">
							<label class='lab-6'>办理时间：</label>
								${currentTime}
							</div>
					      <div class="col-md-12 height-align">
					    	<label  class='lab-8'>流水号：</label> 
					     	<input style="width: 160px" type="text" class="list_select" name="code" value="${serialNumber}">
						  </div>
								
						</div>
					
					
					
						<div class="modal-footer btns-dialog">
							<button type="submit"  class="btn  btn-info btn-margin ">保存</button>
							   	<input type="reset" class="btn btn-color-9E8273 btn-margin" value="重置" />
							   
								<button type="button" class="btn btn-defualt btn-margin" data-dismiss="modal">返回</button>
							
						</div>
					</form>
</section>

</body>