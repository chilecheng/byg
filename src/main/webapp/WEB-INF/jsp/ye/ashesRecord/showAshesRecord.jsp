<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>


<div class="modal-dialog nav-tabs-custom" role="document">
<!-- 基本信息 -->
						
						<p class='p border-B'>死者基本信息</p>
					
							<div class="box-body border-B padding-B">
								<div class="col-md-5 height-align">
									<label class='lab-6 height-align'>死者姓名：</label>
									${dName}
								</div>
								<div class="col-md-5 height-align">
										<label class='lab-6'>死者性别：</label> 
										<span>${dSex }</span>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>死者年龄：</label>
									${dAge}
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>火化日期：</label> 
									${cremationTime}
									
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>存入时间：</label> 
									${beginDate}
									
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>存放位置：</label> 
									${placeCode}
										
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>到期时间：</label> 
									${endDate}
									
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>存放月数：</label> 
									${depositLong}
									
								</div>
						
							</div>
							
							
						
						
						<!-- 寄存人信息 -->
						<p class='border-B  p'>寄存人信息</p>
					
							<div class="box-body border-B padding-B">
								<div class="col-md-5 height-align">
									<label class='lab-6'>
										寄存人姓名：
									</label>
									${sName}

								</div>
								<div class="col-md-7 height-align">
									<label class='lab-6'>
										寄存证编号：
									</label>
									${code}

								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>
										与死者关系：
									</label>
									${appellation}

								</div>
								<div class="col-md-7 height-align">
									<label class='lab-6'>
										寄存人电话：
									</label>
									${sPhone}
								</div>
								<div class="col-md-12">
									<label class='lab-6'>
										寄存人住址：
									</label>
									${sAddr}
								</div>
								<div class="col-md-12">
									<label class='lab-6'>备注：</label>
									${comment}
								</div>

							</div>
					
						
				
					
					<div class="modal-footer btns-dialog">
					<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
					</div>
				
					
				
</div>

