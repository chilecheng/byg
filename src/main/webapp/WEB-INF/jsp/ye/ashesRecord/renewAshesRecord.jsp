<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<script src="js/updateDate.js"></script>
<section class="content">
<form action="${url}" id="form" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="ashesRecordId" value="${ashesRecordId}" >
		<input type="hidden" name="currentTime" value="${currentTime}" >
		<input type="hidden" id="endDate" value="${endDate}" >
			<div class="modal-dialog nav-tabs-custom" role="document">
						<!-- 基本信息 -->
						<div class="modal-body nopadding-T nopadding-B">
						<div class='row'>
							<p class='col-md-12 p border-B'>业务信息</p>
							<div class="col-md-12 border-B padding-B">
								<div class='row'>
									<div class="col-md-6 height-align">
										<label class='lab-1'>死者编号：</label>
										${dcode}
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-6'>死者姓名：</label> 
										${dName}
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>续期月数：</label>
										<input  type="text"  class="list_select input-dialog" name="longTime" id="longTime" value="" onkeyup="update(this,'#endDate','#newEndDate',${pice},true)">
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-6'>到期时间：</label> 
										${endDate}
										
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-6'>新到期时间：</label> 
										<input  type="text" data-provide="datetimepicker" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select input-dialog" id="newEndDate" name="newEndDate" value="<fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
										
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-6'>金额：</label> 
										<input  class="list_select input-dialog" name="total" id="total">
									</div>
								</div>
							</div>
							<p class='col-md-12 p border-B'>办理信息</p>
							<div class="col-md-12 border-B padding-B">
								<div class='row'>
									<div class="col-md-6 height-align">
										<label class='lab-6'>经办人：</label>
										${agentUser}
									</div>
									<div class="col-md-6 height-align">
									<label class='lab-6'>办理时间：</label>
										${currentTime}
									</div>
								     <div class="col-md-7 height-align">
								    	<label  class='lab-7'>流水号：</label> 
								     	<input  type="text" class="list_select input-dialog" name="code" value="${serialNumber }">
									 </div>
								</div>	
							</div>
						<div class="col-md-12 btns-dialog"> 
							<button type="submit"  class="btn btn-info btn-margin">保存</button>
							<input type="reset" class="btn btn-color-9E8273 btn-margin" value="重置" />
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</form>
</section>