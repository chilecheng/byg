<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -查看丧葬用品- -->
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<input type="hidden" name="method" value="${method }" >
			<input type="hidden" name="id" value="${id }">
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p broder-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-5'>卡号：</label>
									<span>${cs.cardCode}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>死者性别：</label>
									<span>${cs.sexName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>死者姓名：</label>
									<span>${cs.name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>死者年龄：</label>
									<span>${cs.age}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>灵堂号：</label>
									<span>${cs.mourning.name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>守灵时间：</label>
									<span>${cs.mourningTime}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>告别厅号：</label>
									<span>${cs.farewell.name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>告别时间：</label>
									<span>${cs.farewellTime}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>购买人：</label>
									<span></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>购买时间：</label>
									<span></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>与死者关系：</label>
									<span>${cs.appellation.name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>领用时间：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-12">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>名称</th>
												<th>单价</th>
												<th>数量</th>
												<th>挂账</th>
												<th>小计</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>${itemOrder.item.name }</td>
												<td>${itemOrder.item.pice }</td>
												<td>${itemOrder.number}</td>
												<td>${itemOrder.isdelTickName }</td>
												<td>${itemOrder.total }</td>
												<td>${itemOrder.item.comment }</td>
												
											</tr>
											<tr>
												<td>小计</td>
												<td colspan="5" style="text-align:left">
													<span>${itemOrder.total }</span>
												</td>
											</tr>
										</tbody>
									</table>
									<div>
										<label>备注：</label>
										<span>${itemOrder.comment}</span>
									</div>
								</div>
								
							</div>
							
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-5'>经办人：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>代办员：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>流水号：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>购买单号：</label>
									<span>111111111111111</span>
								</div>
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
</div>	
</body>