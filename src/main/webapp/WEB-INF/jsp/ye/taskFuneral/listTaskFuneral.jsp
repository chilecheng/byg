<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style type="text/css">
	 td a.a{
	 display:inline;
	}
	
	
</style>


<section class="content-header">
	<i class="title-line"></i>
	<div class="title">丧葬用品核对表</div>
</section>

<script type="text/javascript">
$(function(){
	timeRange();
	$('#funeralNumber').text('');
});
var checkType='3';
$("#typeDiv").on('click','button',function(){	
	checkType=$(this).attr('name');
	$("#checkType").val(checkType);
});
	(function() {
		function changeColor() {
			var value = $(this).val();
			var txt = $(this).find("option[value='" + value + "']").text();
			switch (txt) {
			case '已处理':
				$(this).css('color', '#4DA2FD');
				break;
			case '未处理':
				$(this).css('color', '#D73925');
				break;
			case '已领用':
				$(this).css('color', '#00A65A');
				break;
			}
		}
		$(".state").change(changeColor);
		$(".state").each(changeColor);
		$(".state").each(function() {
			$(this).find('option').each(function() {
				var txt = $(this).text();
				switch (txt) {
				case '已处理':
					$(this).css('color', '#4DA2FD');
					break;
				case '未处理':
					$(this).css('color', '#D73925');
					break;
				case '已领用':
					$(this).css('color', '#00A65A');
					break;
				}
			})
		});
	})();


	function GaiBian(v) {
		var value = $(v).val();
		var cvalue = $(v).parent().parent().find('input:checkbox').val();
	
		$.ajax({
			type : 'POST',
			url : "taskFuneral.do?method=isdel&isdel=" + value + "&id=" + cvalue,
			dataType : "json",
			success : dialogAjaxDone,
		});
	}
	/*  改变费委托业务状态 */
	function GaiBianNodelegate(v) {
		var value = $(v).val();
		
		var cvalue = $(v).parent().parent().find('input:checkbox').val();
		$.ajax({
			type : 'POST',
			url : "taskFuneral.do?method=isdelNodelegate&isdel=" + value + "&id=" + cvalue,
			dataType : "json",
			success : dialogAjaxDone,
		});
	}
	
	
	
	

</script>
<section class="content">
	<!-- -丧葬用品核对表- -->
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title" style="color: #4EC2F6">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<form action="${url }" id="pageForm"
						onsubmit="return homeSearch(this);">
						<input name="method" value="${method }" type="hidden" /> <input
							type="hidden" name="id" value="${id }">
						<div class="box-body">
						<c:if test="${tongji == 'yes'}">
							<iframe id="reportFrame" width="100%" height="1400" src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taskFuneral.cpt&op=view"></iframe>
						</c:if>
							<div class="col-md-4">
								<label>类型：</label> <select class="list_input nopadding-R" name="searchType">
									${searchOption }
								</select>
							</div>

							<div class="col-md-8" style="margin-left: -50px">
								<label>查询值：</label> <input type="text" class="list_input input-hometab"
									name="searchVal" value="" placeholder="查询值">
							</div>
						
						<div class="col-md-12" >
							<label>领用时间：</label> <input type="text"
								data-id='beginDate'
								class="list_select" id='beginDate' name="beginDate"
								value="<fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd" />"><i style="margin-left: -20px;"
								class="fa fa-calendar"></i> <span class='timerange'>--</span> <input type="text"
								data-id='endDate'
								class="list_select" id='endDate' name="endDate"
								value="<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />"><i style="margin-left: -20px;"
								class="fa fa-calendar"></i>
<%-- 			<a href="${url}?method=list&type=1"	target="homeTab" id="default" data-btn='time' data-time="today"	class="btn btn-normally margin-L" style='margin-top:-3px;' role="button">今日领用</a>  --%>
<%-- 			<a href="${url}?method=list&type=2" target="homeTab" style='margin-top:-3px;' id="default"	data-btn='time' data-time='tomorrow' class="btn btn-normally " role="button">明日领用</a> --%>
			
						</div>	
						<input type="hidden" id="checkType" name="checkType" value="${checkType }">
						<div class="col-md-12" id="typeDiv">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
							<button type="submit" class="btn btn-normally btn-all margin-L"  name="3" >全部</button>
							<button type="submit" class="btn   btn-normally " name="1">今日领用</button>
							<button type="submit" class="btn btn-normally"  name="2" >明日领用</button>
						</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
	<div class="row">
	<div class='col-md-12'>
		<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active" style="width: 150px; text-align: center;">
				<a href='#delegate'  data-toggle="tab" aria-expanded="true">委托业务领用</a>
			</li>
			<li style="width: 150px; text-align: center;">
				<a href='#undelegate'  data-toggle="tab" aria-expanded="true" id="undel" name="undel">非委托业务领用</a>
			</li>
		</ul>
		<div class='tab-content'>
			<div class="tab-pane active" id='delegate'>
			<div class="box nomargin-B" style="border: 0;box-shadow:none;">
				<div class="box-header">
				<!--startprint0-->
				 <span>领用日期：</span>  ${beginDate }——<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />
				<!--endprint0-->
					<small class="pull-right btns-print">
						<a href="${url}?method=isdel&isdel=${Yes_treated }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认处理吗" class="btn btn-info" role="button">批量处理</a>
						<a href="${url}?method=isdel&isdel=${No_treated }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认未处理吗" class="btn btn-danger" role="button">批量未处理</a>
						<a href="javascript:showprint()" class="btn btn-color-ab47bc" role="button">打印</a> 
					  	<a href="${url}?method=exportExcel"class="btn btn-success" role="button">导出EXCEL</a> 
<%-- 						<a href="${url}?method=exportWord"class="btn btn-primary" role="button">导出WORD</a> --%>
					</small>
				</div>
				<!-- /.box-header -->
				<!--startprint1-->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row  margin-B">
							<div class="col-sm-12 overx">
								<table class="table table-bordered table-hover table-width"
									id='funeral_detail'>
									<thead>
										<tr role="row">
											<th ><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th><!-- style='display:none;' -->
											<th>序号</th> 
											<th>死者姓名</th>
											<th>年龄</th>
											<th>性别</th>
											<th>告别厅</th>
											<th>告别时间</th>
											<th data-detail='detail'>骨灰盒</th>
											<th data-detail='detail'>小花圈</th>
											<th data-detail='detail'>保护剂</th>
											<th data-detail='detail'>包布盒</th>
											<th data-detail='detail'>红孝带</th>
											<th data-detail='detail'>寿被</th>
											<th data-detail='detail'>寿衣</th>
											<th data-detail='detail'>雨伞</th>
											<th>备注</th>
											<th>状态</th>
											<th>操作 </th>
										</tr>
									</thead>
									<tbody >
										<c:choose>
											<c:when test="${fn:length(list)==0}">
												<tr>
													<td colspan="20"><font color="Red">还没有数据</font></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach items="${list}" var="u" varStatus="status">
													<c:set value="0" var="flag"></c:set>
													<tr role="row" class="listtask">
<%-- 														<c:set value="0" var="typeFlag"></c:set> --%>
														<c:forEach items="${u}" var="k">
															<c:choose>
																<c:when test="${flag==0}">
																	<td ><input type="checkbox" name="checkboxId" value="${k}"></td>
																</c:when>
																<c:when test="${flag==1}">
																	 <td>${k}</td> 
																</c:when>
																
																<c:when test="${flag==2}">
																	<td width='30px' style="text-decoration: none;text-align:center">
																		<a id="href_comorder" class="a" target="homeTab">${k }</a>
																	</td>																	 
																</c:when>
																
																<c:when test="${flag==6}">
																	<td><fmt:formatDate value="${k}"
																			pattern="yyyy-MM-dd HH:mm" /></td>
																</c:when>
																<c:when test="${flag==16}">
																	<td><select name="state" class="state"
																		onchange="GaiBian(this)"> ${k }
																	</select></td>
																</c:when>
																<c:when test="${flag==17}">
																	<td style="text-decoration: none;text-align:center">
																		<a href="taskFuneral.do?method=editComment"  class="btn a" rel="myModel" target="dialog" >备注</a>
																	</td>
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${changeFlagCommission[status.count-1][flag]=='yes'}">
																			<td style="color:red">${k}</td>
	<%-- 																		<c:set value="${typeFlag+1 }" var="typeFlag"></c:set> --%>
																		</c:when>
																		<c:otherwise>
																			<td>${k}</td>
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose>
															<c:set value="${flag+1}" var="flag"></c:set>
														</c:forEach>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="box">
							<p class='padding-T padding-L border-B'>
								<label>统计时间:</label> <span><font color="red"><fmt:formatDate
											value="${calculateTime}" pattern="yyyy/MM/dd HH:mm" /></font></span>
							</p>
							<div class="box-body">
								<div class="row" data-show="thing">
									<c:forEach items="${namePrice}" var="m">
										<span class="col-md-2"> ${m.key}:<font color="red">${m.value}</font> </span>
									</c:forEach>
					
								</div>
							</div>
						</div>
						
						
					</div>
				</div>
				<!--endprint1-->
		
			</div>
		
		</div>
		<div class="tab-pane" id='undelegate'>
			<div class="box nomargin-B" style="border: 0;box-shadow:none;">
				<div class="box-header">
					<!--startprint4-->	
					 <span>领用日期：</span>${beginDate }——<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />
					 <!--endprint4-->
					<small class="pull-right btns-print">
						<a href="${url}?method=isdel&isdel=${Yes_treated }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认处理吗" class="btn btn-info" role="button">批量处理</a>
						<a href="${url}?method=isdel&isdel=${No_treated }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认未处理吗" class="btn btn-danger" role="button">批量未处理</a>
						<a href="javascript:showprint2()" class="btn btn-color-ab47bc" role="button">打印</a>
<%-- 					  	<a href="${url}?method=exportExcelNoDel"class="btn btn-success" role="button">导出EXCEL</a>  --%>
<%-- 						<a href="${url}?method=exportWordNoDel"class="btn btn-primary" role="button">导出WORD</a> --%>
					</small>
				</div>
				<!-- /.box-header -->
				<!--startprint5-->
				<div class="box-body">
					<div
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row margin-B">
							<div class="col-sm-12 overx">
								<table class="table table-bordered table-hover table-width">
									<thead>
										<tr role="row">
											<th ><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>
											<th>序号</th>
											<th>死者姓名</th>
											<th>年龄</th>
											<th>性别</th>
											<th>告别厅</th>
											<th>告别时间</th>
											<th>购买人</th>
											<!-- <th>备注</th>
											<th>状态</th> -->
											<th data-detail='detail'>骨灰盒</th>
											<th data-detail='detail'>小花圈</th>
											<th data-detail='detail'>保护剂</th>
											<th data-detail='detail'>包布盒</th>
											<th data-detail='detail'>红孝带</th>
											<th data-detail='detail'>寿被</th>
											<th data-detail='detail'>寿衣</th>
											<th data-detail='detail'>雨伞</th>
											<th>备注</th>
											<th>状态</th>
											<th> 操作 </th>
										</tr>
									</thead>
									<tbody >
										<c:choose>
											<c:when test="${fn:length(list2)==0}">
												<tr>
													<td colspan="20"><font color="Red">还没有数据</font></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach items="${list2}" var="u" varStatus="status2">
													<c:set value="0" var="flag"></c:set>
													<tr role="row"  class="listtask2">
														<c:forEach items="${u}" var="k">
															<c:choose>
															<c:when test="${flag==0}">
																<td ><input type="checkbox" name="checkboxId"
																 value="${k}"></td>
															</c:when>
															<c:when test="${flag==2}">
																<td style='display:none;'><input type="hidden" name="orderId"
																 value="${k}"></td>
															</c:when>
															<c:when test="${flag==1}">
																<td>${k}</td>
															</c:when>
															<c:when test="${flag==3}">
																<td style="text-decoration: none;text-align:center"> <a id="href_comorder" class="a" target="homeTab"> ${k } </td>
															</c:when>
															<c:when test="${flag==7}">
																<td><fmt:formatDate value="${k}"
																		pattern="HH:mm" /></td>
															</c:when>
															<c:when test="${flag==18}">
																<td><select name="state" class="state"
																	onchange="GaiBianNodelegate(this)"> ${k }
																</select></td>
															</c:when>
															<c:when test="${flag==19}">
																	<td align='center' style="text-decoration: none;text-align:center">
																		<a href="#" class="btn a" rel="myModel" target="dialog" >备注</a>
																	</td>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when test="${changeFlagFuneral[status2.count-1][flag]=='yes'}">
																		<td style="color:red">${k}</td>
																	</c:when>
																	<c:otherwise>
																		<td>${k}</td>
																	</c:otherwise>
																</c:choose>
															</c:otherwise>
																
															</c:choose>
															<c:set value="${flag+1}" var="flag"></c:set>
														</c:forEach>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<div class="box">
					<p class='padding-T padding-L border-B'>
						<label>统计时间:</label> <span><font color="red"><fmt:formatDate
									value="${calculateTime}" pattern="yyyy/MM/dd HH:mm" /></font></span>
					</p>
					<div class="box-body">
						<div class="row" data-show="thing">
							<c:forEach items="${namePrice2}" var="m">
								<span class="col-md-2"> ${m.key}:${m.value} </span>
							</c:forEach>
			
						</div>
					</div>
				</div>
					</div>
				</div>
				<!--endprint5-->
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	
</section>
<script>
 $(function(){
 	$(".listtask").each(function(i){
 		var id= $(this).children("td").eq(0).find('input:checkbox').val();
 		var href="commissionOrder.do?method=readOnly&id="+id+"&adr=E";
 		$(this).children("td").eq(2).find('a').attr("href",href);
 		var commentHref = "taskFuneral.do?method=editComment&id="+id+"&src=delegat";
 		$(this).children("td:last-child").find('a').attr("href",commentHref);
	
 	});
 	$(".listtask2").each(function(i){
 		var id= $(this).children("td").eq(2).find('input:hidden').val();
 		var href="commissionOrder.do?method=readOnly&id="+id+"&adr=E";
 		$(this).children("td").eq(3).find('a').attr("href",href);
 		var buyId = $(this).children("td").eq(0).find('input:checkbox').val();
 		var commentHref = "taskFuneral.do?method=editComment&id="+buyId+"&src=noDelegat";
 		$(this).children("td:last-child").find('a').attr("href",commentHref);
 	});
})
 
 	</script>
 
 
 
 

