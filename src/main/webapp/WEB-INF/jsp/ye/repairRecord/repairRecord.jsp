<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<script>
	$(function(){
		timeMunite();
	});
</script>
<body>
		<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<form  action="${url }" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
							<input type="hidden" name="type" value="${type }" />
							<input type="hidden" name="method" value="${method }" />
							<p class="col-md-12 p border-B">业务信息</p>
							<div class="col-md-12 nomargin-T border-B padding-B">
								<div class="row">
									<div class="col-md-12">
										<label  class='lab-4'>
											<c:if test="${type == 'mour' }">
												灵堂号：
											</c:if>
											<c:if test="${type == 'fare' }">
												告别厅号：
											</c:if>
											<c:if test="${type == 'furnace' }">
												火化炉号：
											</c:if>
											<c:if test="${type == 'freezer' }">
												冰柜号：
											</c:if>
										</label> 						
												<select class="required list_input nopddigng-R"  name="itemId" style='min-width:30%;'>
													 	${itemOption } 
												</select>
									</div>
									<div class="col-md-12">
										<label class='lab-4'>维修时间：</label> 
										<input data-id="reservation" readOnly='readOnly' class="required list_select input_a" name="startTime" "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
										<span class='timerange'>--</span>   
										<input data-id="reservation" readOnly='readOnly' class="required list_select input_a"  name="endTime" "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
										
									</div>
									<div class="col-md-12">
										<label class='lab-4'>维修费用：</label> 
										<input type="text" class="number required list_input" id='fix' name='fee' style='min-width:30%;'/>
									</div>
									
									<div class="col-md-12">
										<label class='lab-4'>维修原因：</label> 
										<textarea class='textarea-dialog' id='reason' name='reason'></textarea>
									</div>
								</div>
								
							</div>
							<p class="col-md-12 p border-B">办理信息</p>
							<div class="col-md-12 nomargin-T border-B paddng-B">
								<div class="row">
									<div class="col-md-6 height-align">
										<label class='lab-4'>报修日期：</label>
										<span>系统自动取当前时间</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-4'>报修人员：</label>
										<span>${user.name }</span>
										<input type="hidden" name="reportId" value="${user.userId }" />
									</div>
								</div>
							</div>
							<div class="col-md-12 btns-dialog">
								<button type="submit" class="btn btn-info btn-margin" >保存</button>
								<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
								<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</body>