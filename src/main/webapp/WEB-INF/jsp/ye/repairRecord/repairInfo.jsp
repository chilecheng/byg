<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<script>
	$(function(){
		timeMunite();
	});
</script>
<body>
		<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<form  action="repairRecord.do?method=change" target="dialog" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
							<input type="hidden" name=id value="${repairInfo.id }" />
							<input type="hidden" name="type" value="${type }" />
							<input type="hidden" name="method" value="change" />
						<div class="row">
							<p class="col-md-12 p border-B">业务信息</p>
							<div class="col-md-12 border-B padding-B">
								<div class="row">
									<div class="col-md-12">
										<label  class='lab-4'>
											<c:if test="${type == 'mour' }">
												灵堂号：
											</c:if>
											<c:if test="${type == 'fare' }">
												告别厅号：
											</c:if>
											<c:if test="${type == 'furnace' }">
												火化炉号：
											</c:if>
											<c:if test="${type == 'freezer' }">
												冰柜号：
											</c:if>
										</label> 
										
												<select class="required list_input nopddigng-R"  name="itemId" style='min-width:30%;'>
													 	${itemOption } 
												</select>
									</div>
									<div class="col-md-12">
										<label  class='lab-4'>维修时间：</label> 
										<input data-id="reservation" readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii" class="required list_select input_dialog" name="startTime" value="<fmt:formatDate value="${repairInfo.beginTime}" pattern="yyyy-MM-dd HH:mm"/>">&nbsp;<i style="margin-left: -20px;" class="fa fa-calendar"></i>
										<span class='timerange'>--</span> 
										<input data-id="reservation" readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii" class="required list_select input_dialog"  name="endTime" value="<fmt:formatDate value="${repairInfo.endTime}" pattern="yyyy-MM-dd HH:mm"/>"/>&nbsp;<i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
									<div class="col-md-12">
										<label  class='lab-4'>维修费用：</label> 
										<input type="text" class="number required list_input input-dialog" id='fix' name='fee' style='min-width:30%;' value="${repairInfo.repairFee}"/>
									</div>
									
									<div class="col-md-12">
										<label  class='lab-4'>维修原因：</label> 
										<span>${repairInfo.comment} </span>
									</div>
								</div>
								
							</div>
							<p class="col-md-12 p border-B">办理信息</p>
							<div class="col-md-12 border-B padding-B">
								<div class="row">
									<div class="col-md-6 height-align">
										<label  class='lab-4'>报修日期：</label>
										<span><fmt:formatDate value="${repairInfo.createTime}" pattern="yyyy/MM/dd HH:mm"/></span>
									</div>
									<div class="col-md-6 height-align">
										<label  class='lab-4'>报修人员：</label>
										<span>${userName}</span>
									</div>
									<div class="col-md-6 height-align">
										<label  class='lab-4'>修改人员：</label>
										<select name="reportId" class='list_input input-dialog'>
											<option value="${currentUser.userId}">${currentUser.name}</option>
										</select>
									</div>
								</div>
							</div>
								<div class="col-md-12 btns-dialog">
									<%-- <a href="${url}?method=complete&type=${type }&reportId=${repairInfo.id }"  class="btn btn-primary" role="button">维修结束</a> --%>
									<button type="submit" class="btn btn-info btn-margin" >更改信息</button>
									<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
								</div>
						</div>
					</form>
				</div>
			</div>
		</div>
				
	
	
</body>