<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">销卡记录查询</div>
</section>
<style type="text/css">
	#pageForm .btn.btn-info{
		padding:6px 50px;
		border-radius:10px;
	}
	div.col-md-12.btns a.btn{
		float:right;
		margin-left:10px;
		padding:4px 35px;
		border-radius:6px;
	}
	div.col-md-12.btns a.btn:nth-child(2){
		background-color:#FF7043;
		color:#fff;
	}
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	#main-content .list_input{
		min-width:35%;
	}
	select.list_input{
		color:#aaa;
	}
	#main-content .pagination{
		float:right;
	}
	#main-content table.table.table-bordered{
		margin-top:10px;
	}
	table.table.table-bordered thead div{
		margin:0px;
	}
	table.table.table-bordered tbody a.loser{
		display:block;
		width:100%;
		height:100%;
		color:#58C6F7;
	}
	table.table.table-bordered tbody td{
		height:30px;
	}
	#pageForm .content:last-child{
		padding-top:0px;
	}
	.box-body label{
		width:60px;
		display:inline-block;
		text-align:right;
	}
</style>

<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="${method}" type="hidden">
	<section class="content">
	<!-- -销卡记录- -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							
							<div class="col-md-6">
								<label>查询值：</label> 
								<input type="text" class="list_input" id='search' name='search' value='${search}' placeholder='死者姓名'/>
							</div>
							<div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="${startTime}">
								<span style="margin:8px;">至</span> 
								<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="${endTime}">
								
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>
		
			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>死者姓名</th>
								<th>年龄</th>
								<th>死者性别</th>
								<th>旧卡号</th>
								<th>销卡人</th>
								<th>销卡时间</th>
							</tr>
						</thead>
						<tbody>
							 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="6">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td>${u.commissionOrder.name}</td>
						    					<td>${u.commissionOrder.age}</td>
						    					<td>${u.commissionOrder.sex}</td>
						    					<td>${u.oldCard}</td>
						    					<td>${u.creatUser.name}</td>
						    					<td>${u.creatTime}</td>
											</c:forEach>
										</c:otherwise>
									</c:choose>
							
						</tbody>
					</table>
				
				</div>
			</div>
			<%@ include file="/common/pageFood.jsp"%>
		</div>
	</section>
</form>