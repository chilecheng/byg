<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form id="detail" action="appointment.do" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input name="method" value="${method}" type="hidden" />
			<input name="appointmentId" id="appointmentId" value="${id }" type="hidden" />
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">出车登记</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-12 height-align">
<!-- 									<input type='radio' name='radio'  checked value='trunCode'> &nbsp; -->
									<label >数据转至卡号：</label>
									<input name='commissionCode' id='commissionCode' onChange='CardCodeChange(this)'  class="list_input input-hometab" placeholder="卡号,不填则新增委托单">
									<label id="message"></label>
								</div>
<!-- 								<div class="col-md-12 height-align"> -->
<!-- 									<input type='radio' name='radio'   value='newCode'> &nbsp; -->
<!-- 									<label class='lab-6'>生成新委托单：</label> -->
<!-- 								</div> -->
							</div>
						</div>
						<div class="col-md-12 btns-dialog">
<!-- 							<button type='submit' class='btn btn-info btn-margin'>确定</button> -->
							<a href="appointment.do?method=jumpPage&appointmentId=${id }&commissionCode=" target="homeTab" id='turnTo'  class="btn btn-info btn-margin" role="button">确定</a>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
</body>
<script>
function CardCodeChange(obj){
	$.ajax({
	    url: "appointment.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'isExist',code:$("#commissionCode").val()},
	    success: function (json) {
	    	if(json.statusCode==302){
		    	toastr["error"](json.message);
	    	}
	    	if(json.statusCode==200){
		    	toastr["success"](json.message);
		    	var  id=$(obj).val();
		    	var href=$('#turnTo').attr('href');
		    	$('#turnTo').attr('href',href+id);
	    	}
	    	
	    	
// 	    	$("#deadReasonId").html(json.str);
// 	    	if(json.deadFlag==2){
// 	    		$("#ifNormal").removeClass('hide');
// 	    		$("#proveUnitId").addClass('required');
// 	    	}else{
// 	    		$("#ifNormal").addClass('hide');
// 	    		$("#proveUnitId").removeClass('required');
// 	    	}
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 
</script>