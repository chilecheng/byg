<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">出车登记列表</div>
</section>
<style type="text/css">
	.table td a.light{
		width:50px;
		height:20px;
		/* background-color:lightblue; */
		text-align:center;
		line-height:20px;
		color:#fff;
		border-radius:4px;
	}
	.table td a.light.right{
		
		background-color:#ab47bc;
	}
</style>
<script type="text/javascript">
// function changeDate(){
// 	$("#pageForm").submit();
// }
// $('#search').change(function(){
// 	if($("#find").find("option:selected").text()=="卡号"){
// 		homeSearch(pageForm);		
// 	}
// })
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div>
						<div class="box-header with-border">
							<h3 class="box-title" style="color:#4EC2F6">查询</h3>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-chevron-down"></i>
								</button>
							</div>
							<!-- /.box-tools -->
						</div>
					</div>
					<div class="box-body border-B">
						
						
						<div class="col-md-3">
							<label>类型：</label> 
							<select class="list_input nopadding-R" id="find" name="find">
								<option value='findByName'>姓名</option>
							</select>
						</div>
						<div class="col-md-8">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" id='search' name='search' value='${search }' placeholder='单行输入'/>
						</div>
						<div class="col-md-12">
							<label>创建时间：</label> 
							<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${beginDate}">
							<span class='timerange'>--</span>  
							<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endDate}">
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</div>
			<!-- /.col -->
		</div>
	</div>
</section>
<section  class="content">
					<div class="row">
					<div class="col-xs-12">
						<div class="box" style="border: 0;margin-bottom:0">
							
							<div class="box-body">
								<div class='cl'>
									<small class=' btns-buy'>
										<a href="appointmentCar.do?method=carOrder" class='btn btn-warning' role="button"  target="dialog" rel="myModal" >出车登记</a>
									</small>
								</div>
								<div id="example2_wrapper"
									class="dataTables_wrapper form-inline dt-bootstrap margin-T">
									<div class="row">
										<div class="col-sm-12">
											<table class="table table-bordered table-hover">
												<thead>
													<tr role="row">
														<th>预约流水号</th>
														<th>姓名</th>
														<th>联系方式</th>
														<th>出车时间</th>
														<th>接尸地址</th>
														<th>操作</th>
													</tr>
												</thead>
												<tbody>
												 <c:choose>
												    <c:when test="${fn:length(page.list)==0 }">
													    <tr>
											    			<td colspan="6">
														  		<font color="Red">没有预约出车记录</font>
													 		</td>
													    </tr>
												    </c:when>
												    <c:otherwise>
													    <c:forEach items="${page.list }" var="u">
														<tr role="row">
															<td>${u.serialNumber}</td>															
									    					<td id="name_Click"><a href="${url}?method=show&id=${u.id}"  target="dialog" rel="myModal" ><font color="blue">${u.name }</font></a></td>
									    					<td>${u.phone}</td>
									    					<td><fmt:formatDate value="${u.departureTime}" pattern="yyyy-MM-dd HH:mm"/></td>
									    					<td>${u.pickAddress }</td>
									    					<td>
									    					<a href="${url}?method=choose&id=${u.id}" class='btn btn-warning' style='width:auto;height:auto;' target="dialog" rel="myModal">转为火化委托单</a>
									    					<a href="${url}?method=edit&id=${u.id}" class='btn btn-info' target="dialog" rel="myModal" style='background-color:#ff7043;border:0px;width:auto;height:auto;'>修改</a>
									    					<a href="${url}?method=deleteMessage&id=${u.id }" target="ajaxTodo" data-delete="true" rel="myModal" warm="确认删除吗" class="btn btn-danger" style='width:auto;height:auto;' role="button">删除</a>
									    					</td>
														</c:forEach>
													</c:otherwise>
												</c:choose>
												</tbody>
											</table>
										</div>
									</div>
									<%@ include file="/common/pageFood.jsp"%>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>
				<!-- <div class="box-footer clearfix btns-print" style="border-top:none;">
	              <a href="javascript:void(0)" class="btn btn-color-ab47bc pull-right">打印</a>
	            </div> -->
				</form>
				</section>
			
<script>
$(function(){
	timeRange();
})
</script>