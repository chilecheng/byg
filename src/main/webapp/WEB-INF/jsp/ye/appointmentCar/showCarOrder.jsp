<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form  action="appointmentCar.do" id="Form" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input name="method" value="${method}" type="hidden" />
			<input name="id" id="id" type="hidden" value="${id }"/>
 			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">出车查看</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-12  height-align">
									<label class="lab-2">姓名：</label>
									<input type="text" class="list_input input-hometab" id="dName" name="dName" value="${name }">
<!-- 									<button class="btn-psearch btn btn-info" type="button" data-name="search">搜索</button> -->
<!-- 									<input type="text" readonly="" class="list_input" id="dNameId" data-id="dNameId" name="dNameId" hidden="" value=""> -->
<%-- 									<a data-dialog="dialog" class="hide" target="dialog" rel="myModal2" href='${url}?method=NameList'>选择</a> --%>
<!-- 									<span class="nowarning" data-warning="warning"></span> -->
								</div>
								<div class="col-md-12 height-align">
									<label >家属电话：</label>
									<input type="text" class="list_input input-hometab" id="fphone" name="fphone" value="${phone }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>出车时间：</label>
<!-- 									<input readonly name='farewellTime' class="list_input input-hometab"> -->
									<input   readOnly  class="list_select" value='<fmt:formatDate value="${departureTime }" pattern="yyyy-MM-dd HH:mm"/>' >
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>运输类型：</label>
									<select name="transport" id="transport" style="width: 169px"  class="list_select nopadding-R">
										${transportOption }
									</select>
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>车辆类型：</label>
									<select name="carType" id="carType" style="width: 169px" class="list_select nopadding-R">
										${carTypeOption }
									</select>
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>接尸地址：</label>
									<input list="pickAddr1" autocomplete='off' class="input-hometab  list_select" value="${pickAdd }" name="pickAddr" id="pickAddr" >
									<datalist  class="list_select"  id="pickAddr1">
										${corpseAddOption}
									</datalist>
								</div>
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
<!-- 							<button type='submit' class='btn btn-info btn-margin'>保存</button> -->
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
				
	</form>
</body>
<script>
	$(function(){
		timeMunite();	

	})
</script>