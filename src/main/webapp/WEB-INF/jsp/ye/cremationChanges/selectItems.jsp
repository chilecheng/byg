<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<script src="lte/plugins/treeview/bootstrap-treeview.js"></script>
<script type="text/javascript">

$(function(){ 
	var tree = [${itemTree}]
	var html='';
	$(tree).each(function(i,val){
		var indexFlag=val.indexFlag;
	html+='<div class="box box-warning collapsed-box">';
      html+=  '<div class="box-header with-border">';
       html+=  (' <h3 class="box-title">'+val.text+'</h3>');
       html+=  ' <div class="box-tools">';
         html+='<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i> </button>';
           
         html+=' </div>';
        html+='</div>';
       html+=' <div class="box-body no-padding">';
         html+='<ul class="nav nav-pills nav-stacked">';
         	$(val.nodes).each(function(i,v){
         		var id=v.tags[0].split('_');
         		var num=Number(id[1]);
         		html+=("<li class='checkbox checkbox-S'><label><input type='checkbox' name='namager' data-indexFlag='"+indexFlag+v.indexFlag+"' data-num='"+num+"' value='"+id[0]+"'/>"+v.text+"</label></li>");
         	})
          html+='</ul>';
        html+='</div>';
      html+='</div>';
	});
	$('#accordion').append(html);
});

function save(id){
	var arr=[];
	$('.item-txt').each(function(){
		var index=Number($(this).attr('data-number'));
		arr[index]=$(this).attr('data-value');
	})
	$.ajax({
	    url: "commissionOrder.do?method=addItems&ids="+arr+"&type="+'${type}',
	    dataType:'json',
	    cache: false,
	    success: function (list) {
	    	var str='';
	    	if('${id}'=='hard'){
	    		for(var i=0;i<list.length;i++){
		    		var ob=list[i];
		    		hardSum++;
		    		str+="<tr><td><input type='hidden' name="+id+"_id ><input type='checkbox' data-findexFlag='"+ob[7]+"' data-indexFlag='"+ob[8]+"' id=checkbox_"+hardSum+" name="+id+" value="+hardSum+"></td>";
		    		str+="<td><select  class='list_table' onchange=itemHelpCode("+hardSum+",'${id}') name=hard_helpCode id='hard_helpCode_"+hardSum+"'>"+ob[6]+"</select></td>";
		    		str+="<td><select  class='list_table' onchange=itemName("+hardSum+",'${id}') name=hard_itemId id='hard_itemId_"+hardSum+"'>"+ob[0]+"</select></td>";
		    		str+="<td><input type='text' readonly='readonly' class='list_table' name=hard_pice id=hard_pice_"+hardSum+"  value="+ob[1].toFixed(0)+"></td>";
		    		str+="<td><input type='text' class='list_table' onchange=numbers("+hardSum+",'${id}') name=hard_number id=hard_number_"+hardSum+"  value="+ob[3]+"></td>";
		    		str+="<td><input type='text' readonly='readonly' class='list_table' name=hard_total id=hard_total_"+hardSum+" value="+ob[5].toFixed(0)+"></td>";
		    		str+="<td><textarea type='text' class='list_table' name=hard_comment id=hard_comment_"+hardSum+" value='' ></textarea></td></tr>";
		    		hardTotal+=ob[5]
		    		if(i==list.length-1){
		    		$("#hard").append(str);
	    			
	    			$("#hardFont").html(hardTotal+"元");
	    			sortItem($("#hard"))
	    			}
		    	}
	    	}else{
		    	for(var i=0;i<list.length;i++){
		    		var ob=list[i];
		    		sum++;
		    		str+="<tr><td style='text-align: center; vertical-align: middle;'>";
		    		str+="<input type='hidden' name="+id+"_id >";
		    		str+="<input type='checkbox'  data-findexFlag='"+ob[7]+"' data-indexFlag='"+ob[8]+"'  id=checkbox_"+sum+" name="+id+" value="+sum+"></td>";
		    		str+="<td style='text-align: center; vertical-align: middle;'>";
		    		str+="<select  class='list_table' onchange=itemHelpCode("+sum+",'${id}') name='helpCode' id='helpCode_"+sum+"'> "+ob[6]+"</select></td>";
		    		str+="<td style='text-align: center; vertical-align: middle;'>";
		    		str+="<select  class='list_table' onchange=itemName("+sum+",'${id}') name='itemId' id='itemId_"+sum+"'> "+ob[0]+"</select></td>";
		    		str+="<td style='text-align: center; vertical-align: middle; '>";
		    		str+="<input type='text' onchange=priceChange("+sum+",'${id}') class='list_table' name=pice id='pice_"+sum+"' value="+ob[1].toFixed(0)+"></td>";
		    		str+="<td style='text-align: center; vertical-align: middle;'>";
		    		str+="<select  class='list_table' onchange=itemType("+sum+",'${id}') name='typeId' id='typeId_"+sum+"'> "+ob[2]+"</select></td>";
		    		str+="<td style='text-align: center; vertical-align: middle;'>";
		    		str+="<input type='text' class='list_table' onchange=numbers("+sum+",'${id}') name=number id='number_"+sum+"' value="+ob[3]+"></td>";
		    		str+="<td style='text-align: center; vertical-align: middle;'>";
		    		str+="<select  class='list_table' name=bill id=bill_"+sum+"> "+ob[4]+"</select></td>";
		    		str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name=total id=total_"+sum+" class='list_table' value="+ob[5].toFixed(0)+"></td>";
		    		str+="<td><textarea type='text' class='list_table' name=comment id='comment_"+sum+"' value='' style='width: 100%'></textarea></td></tr>";
		    		if('${type}'=='${Type_Service}'){
		    			sTotal+=ob[5];
		    			if(i==list.length-1){
		    			$("#service").append(str);
		    			sortItem($("#service"));
		    			if(state==""){
			    			$('[name="helpCode"] option:selected:contains("535")').each(function(){
			    				$(this).parent().parent().siblings(':last').find('textarea').addClass('required')
			    			})
			    			$('[name="helpCode"] option:selected:contains("538")').each(function(){
			    				$(this).parent().parent().siblings(':last').find('textarea').addClass('required')
			    			})
			    			}
		    			}
		    		}
		    		if('${type}'=='${Type_Articles}'){
			    		aTotal+=ob[5];
			    		if(i==list.length-1){
			    		$("#articles").append(str);
			    		sortItem($("#articles"))
			    		}
			    		
		    		}
		    	}
		    	
	    	}
	    	if('${type}'=='${Type_Service}'){
	    		var jg=sTotal.toFixed(1);
	    		$("#sFont").html(parseFloat(jg)+"元");
	    		var nn=parseFloat($("#serviceNumber").text());	
    			$("#serviceNumber").html(nn+list.length);
	    	}
    		if('${type}'=='${Type_Articles}'){
    			var jg=aTotal.toFixed(1);
    			$("#aFont").html(parseFloat(jg)+"元");
    			var nn=parseFloat($("#articlesNumber").text());	
    			$("#articlesNumber").html(nn+list.length);
	    	}
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
	$('.item-del').click();
}
</script>
<style>
/* #treeContent td{ */
/* 	height: 20px; */
/* 	background-color: #DBDFDE; */
/* } */
/* #treeContent tr{ */
/* 	display:block;  */
/* 	margin:2px 0;   */
/* } */
</style>

<div class="modal-dialog" role="document">
	<form action="${url}" id="form1" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="roleId" name="roleId" value="${role.id}">
		<input type="hidden" id="ids" name="ids" value="">
		<div class="modal-content ">
					<div class='row'>
						<div class='col-md-6 border-R height-500'>
							<section class="content-header content-header-S">
								<i class="title-line"></i>
								<div class="title">请选择服务项目</div>
							</section>
							<div class='panel-group panel-group-S' role="tablist" id='accordion'>
								<div class="box box-warning ">
									<div class="box-header with-border">
										<input id='checkall' type='button' value='全选'>
									</div>
								</div>
							</div>
						</div>
						<div class='col-md-6 height-500 margin-L-S'>
							<section class="content-header content-header-S">
								<i class="title-line"></i>
								<div class="title">已选服务项目
<!-- 									<span style="color: black;font-size: 12px;vertical-align: bottom;" id="chooseNumber">已选择 0 条</span> -->
								
								</div>
							</section>
							<div class='chosed-item panel-group-S'>
								<div class='box box-warning' id='chosed'>
<!-- 									<p class='item-txt'>111111111<span class='item-del'>x</span></p> -->
								</div>
							</div>
						</div>
					</div>
					<div class='row nomargin'>
						<div class='col-md-12 wk-btns'>
							<button type="submit" onclick="save('${id}')"  class="btn btn-info" data-dismiss="modal">确定</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
							<span style="color: black;font-size: 16px;vertical-align: bottom; float: right; margin-right: 40px;" id="chooseNumber">已选择 0 条</span>
						</div>
					</div>
				
				</div>
	</form>
</div>
<script>
	(function(){
		var str=$('#chooseNumber').html().split(" ");
		var sum=parseInt(str[1]);
		$('.panel-group input:checkbox').click(function(){
			if($(this).is(':checked')){
				$('#chosed').append("<p class='item-txt' data-indexFlag='"+$(this).attr('data-indexFlag')+"' data-number='"+$(this).attr('data-num')+"' data-value='"+$(this).val()+"'><a>"+$(this).parent().text()+"</a><span class='item-del'>x</span></p>")
				sum+=1;
				$('#chooseNumber').html("已选择 "+sum+" 条");
			}else{
				$("#chosed p[data-value='"+$(this).val()+"']").remove();
				sum=sum-1;
				$('#chooseNumber').html("已选择 "+sum+" 条");
			}
		});
		$('#chosed').on('click','.item-del',function(){
			$(".panel-group input[value='"+$(this).parent().attr('data-value')+"']").removeAttr('checked');
			$(this).parent().remove();
			sum=sum-1;
			$('#chooseNumber').html("已选择 "+sum+" 条");
		});
		$('#checkall').click(function(){
			$('.panel-group input:checkbox').click();
		})
	})();
</script>	