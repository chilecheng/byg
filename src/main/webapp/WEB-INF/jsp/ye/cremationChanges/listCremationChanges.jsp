<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">委托单追加更改</div>
</section>
<style>
	.table td a.light{
		width:50px;
		height:20px;
		/* background-color:lightblue; */
		text-align:center;
		line-height:20px;
		color:#fff;
		border-radius:4px;
	}
	.table td a.light.left{
	background-color:#00c0ef;
	}
	.table td a.light.middle{
	background-color:#ff7043;
	}
	.table td a.light.right{
		background-color:#ab47bc;
	}
</style>
<script>
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>类型：</label>
							<select class="list_select nopadding-R" name="searchType" id="searchType" style="width: 52%;">
								${searchOption }
							</select>
						</div>
						<div class="col-md-8" style="margin-left: -94px">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" name="searchVal" id="searchVal" value="${searchVal}" placeholder="查询值">
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-print btns-buy'>
						<a href="${url}?method=editBase&menu=com&id=" target="homeTab" checkName="checkboxId" checkOne="true" class="btn btn-color-green" role="button">更改信息</a>
						<a href="${url}?method=edit&id=" target="homeTab" checkName="checkboxId" checkOne="true" class="btn btn-color-ff7043" role="button">项目更改</a>
					</small>
					
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>
											<th>死者姓名</th>
											<th>年龄</th>
											<th>死者性别</th>
											<th>冰柜号</th>
											<th>火化时间</th>
											<th>死亡证明</th>
											<th>是否审核</th>
<!-- 											<th>操作</th> -->
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="8">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td><input type="checkbox" name="checkboxId" value="${u.id}"></td>
							    					<td><a href="commissionOrder.do?method=readOnly&id=${u.id}&adr=F" target='homeTab'>${u.name }</a></td>
							    					<td>
							    					${u.age }
							    					</td>
							    					<td>
							    					${u.sexName }
							    					</td>
							    					<td>
							    					${u.freezer.name }
							    					</td>
							    					<td>
							    					<fmt:formatDate value="${u.cremationTime }" pattern="yyyy-MM-dd HH:mm"/>
							    					</td>
							    					<td>
								    					<c:if test="${u.dFlag==IsHave_No }">
								    						<font color="red" class='state1'>${u.flagName }</font>
								    					</c:if>
								    					<c:if test="${u.dFlag==IsHave_Yes }">
								    						<font color="green" class='state1'>${u.flagName }</font>
								    					</c:if>
							    					</td>
							    					<td>
								    					<c:if test="${u.checkFlag==Check_No }">
								    						<font color="red" class='state1'>${u.checkFlagName }</font>
								    					</c:if>
								    					<c:if test="${u.checkFlag==Check_Yes }">
								    						<font color="green" class='state1'>${u.checkFlagName }</font>
								    					</c:if>
							    					</td>
<!-- 							    					<td> -->
<%-- 								    					<a href="${url }?method=readOnly&checkFlag=${Check_Yes }&adr=A&ifCheck=checkYes&id=${u.id}" class='light left' target="homeTab" data-checkpay='true left' data-checkFlag=1>审核</a> --%>
								    					
<%-- 								    					<a href="${url }?method=readOnly&adr=A&styleType=print&id=${u.id}" class='light right' target="homeTab">打印</a> --%>
<%-- 								    					<a href="${url }?method=edit&id=${u.id}" class='light middle' target="homeTab">修改</a> --%>
<!-- 							    					</td> -->
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$('#searchVal').change(function(){
	if($("#searchType").find("option:selected").text()=="卡号"){
		homeSearch(pageForm);		
	}
})
</script>