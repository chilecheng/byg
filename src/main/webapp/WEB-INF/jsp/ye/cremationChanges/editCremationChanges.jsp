<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style>
	.ID-pic.ID-pic1 img {
    //position: absolute;
    height: auto !important;}
</style>
<script type="text/javascript">
var preConfigList = new Array(); var previewJson = new Array(); 
var sum=0;//标识
var hardSum=0;//困难减免标识
var sTotal=0;//服务项目合计总金额	
var aTotal=0;//丧葬用品合计总金额
var baseTotal=0;//基本减免合计总金额
var hardTotal=0;//困难减免合计总金额
var iceType="";//用于记录冷藏冰柜判断依据

var furnaceOrderTime="";
var fareOrderTime="";

var iceHave="no";
var state='${id}';
var before='';
var mourningBefore='';
var farewellBefore='';
var funBefore='';
var mourningBefore2='';
$(function(){
	timeRange();
	timeMunite();
	if("${specialBusinessOption}"=="" || "${specialBusinessOption}"==null){
		$("#specialBusinessType").hide();
	}
	//火化炉
	$("#furnaceType_div").hide();
	//是否基本或困难减免
	$("#baseLi").hide();
	$("#hardLi").hide();
	if(${base_Is==1}){
		$("#baseLi").show();
	}
	if(${hard_Is==1}){
		$("#hardLi").show();
	}
	//修改
	if('${id}'!="" && '${id}'!=null){
		//车辆调度
		<c:forEach var="u" items='${car_List}'>
			sum++;
			car(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}");
		</c:forEach>
		//服务项目
		<c:forEach var="u" items='${service_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			
			sTotal+=parseFloat('${u[8]}');
			$("#sFont").html(sTotal+"元");
			
		</c:forEach>
		//葬用品项目
		<c:forEach var="u" items='${articles_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			aTotal+=parseFloat('${u[8]}');
			$("#aFont").html(aTotal+"元");
		</c:forEach>
		//基本减免
		//免费对象类别
		var fStr="<label>免费对象类别：</label>";
		if(${isFree==false}){
			<c:forEach var="u" items='${freePerson_list}'>
				fStr+="<input type='checkbox'  name='${u[0]}' value='${u[2] }'>";
				fStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
				$("#freePerson").html(fStr);
			</c:forEach>
		}else{
			<c:forEach var="u" items='${freePerson_list}'>
			fStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			fStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#freePerson").html(fStr);
		</c:forEach>
		}
		//重点救助对象类别
		var hStr="<label>重点救助对象类别：</label>";
		<c:forEach var="u" items='${hardPerson_list}'>
			hStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			hStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#hardPerson").html(hStr);
		</c:forEach>
		//基本减免项目
		if(${base_Is==Is_Yes}){
			<c:forEach var="u" items='${base_list}'>
				sum++;
				var pice=parseFloat('${u[5]}');
				reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[9]}","${u[6]}","${u[5]}","${u[7]}","${u[8]}")
				baseTotal+=pice;
				$("#baseFont").html(baseTotal+"元");
			</c:forEach>
		} else {
			<c:forEach var="u" items='${baselist}'>
				sum++;
				var pice=parseFloat('${u[5]}');
				reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[9]}","${u[6]}","${u[5]}","${u[7]}","${u[8]}")
				baseTotal+=pice;
				$("#baseFont").html(baseTotal+"元");
			</c:forEach>
		}
	//困难减免
		//证件类型
		var pStr="<label class='lab-6'>证件类型：</label>";
		<c:forEach var="u" items='${prove_list}'>
			pStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			pStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#prove").html(pStr);
		</c:forEach>
		//困难减免项目
		if(${hard_Is==Is_Yes}){
			<c:forEach var="u" items='${hard_list}'>
				hardSum++;
				var pice=parseFloat('${u[5]}');
				reductionHard(hardSum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[9]}","${u[6]}","${u[5]}","${u[7]}","${u[8]}")
				hardTotal+=pice;
				$("#hardFont").html(hardTotal+"元");
			</c:forEach>
		}
		//先控制除了水晶棺的,然后再判断水晶棺与灵堂的天数不同
		
		//火化炉按钮
		var fun="<label class=lab-6>火化炉类型：</label> "
		var flagType="x";
		<c:forEach var="u" items='${furnace_list}'>
			fun+="<input ${u[2]} type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
			if("${u[2]}"!=""){
				flagType="y";
			}
			if("${u[2]}"!=""&& ${u[0]==furnace_ty}){
				$("#furnaceType_div").show();
				$("#specialBusinessType").hide();
			}else if("${u[2]}"!=""&& ${u[0]=="1"}){
				$("#specialBusinessType").hide();
			}
		</c:forEach>
		$("#furnace_div").html(fun);
		if(flagType=="x"){
			$('[name="radio"][value="1"]').attr('checked','checked');
			$("#specialBusinessType").hide();
		}
		if('${funeralCode2}'!=null && '${funeralCode2}'!=''){
			$('[name="radio"][value="2"]').attr('checked','checked');
			radio_click('${furnace_ty}');
			$('#furnaceName').val('${funeralCode2}');
		}
		
	}
	sortItem($('#articles'));
	sortItem($('#service'));
	sortItem($('#hard'));
	sortItem($('#base'));
});
//收费项目方法
function item(sum,name,id,itemId,helpCode,pice,typeId,number,bill,total,comment,findexFlag,indexFlag){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name="+name+"_id value="+id+">";
	str+="<input type='checkbox' data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"' id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select readOnly='readOnly' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name='helpCode' id='helpCode_"+sum+"'> "+helpCode+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemName("+sum+",'"+name+"') name='itemId' id='itemId_"+sum+"'> "+itemId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' onchange=priceChange("+sum+",'"+name+"') class='list_table' name=pice id='pice_"+sum+"' value="+parseFloat(pice)+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemType("+sum+",'"+name+"') name='typeId' id='typeId_"+sum+"'>"+typeId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=number id='number_"+sum+"' value="+number+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name=bill id=bill_"+sum+"> "+bill+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name=total id=total_"+sum+" class='list_table' value="+parseFloat(total)+"></td>";
	if(helpCode==="538"||helpCode==="535"){
		str+="<td><textarea type='text' class='list_table required' name=comment id='comment_"+sum+"' >"+comment+"</textarea></td></tr>";
	}else{
		str+="<td><textarea type='text' class='list_table' name=comment id='comment_"+sum+"' >"+comment+"</textarea></td></tr>";
	}
	$("#"+name).append(str);
	var nn=parseFloat($("#"+name+"Number").text());	
	$("#"+name+"Number").html(nn+1);
}

//减免项目方法(原方法,在更改中)
function reduction(sum,name,id,item,helpCode,number,pice,comment,total,findexFlag,indexFlag){
	var str="<tr><td><input type='hidden' name="+name+"_id value="+id+"><input type='checkbox'  data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"'  id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td><select  readOnly='readOnly' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name=reduction_helpCode id='helpCode_"+sum+"'>"+helpCode+"</select></td>";
	str+="<td><select  class='list_table' onchange=itemName("+sum+",'"+name+"') name=reduction_itemId id='itemId_"+sum+"'>"+item+"</select></td>";
	str+="<td><input type='text' onchange=priceChange("+sum+",'"+name+"') class='list_table' name=reduction_pice id=pice_"+sum+"  value="+parseFloat(pice)+"></td>";
	str+="<td><input type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=reduction_number id=number_"+sum+"  value="+number+"></td>";
	str+="<td><input type='text' readonly='readonly' class='list_table' name=reduction_total id=total_"+sum+" value="+parseFloat(total)+"></td>";
	str+="<td><textarea type='text' class='list_table' name=reduction_comment id=comment_"+sum+" value='' >"+comment+"</textarea></td></tr>";
	$("#"+name).append(str);
}
//困难减免(新加,以区别基本减免)
function reductionHard(sum,name,id,item,helpCode,number,pice,comment,total,findexFlag,indexFlag){
	var str="<tr><td><input type='hidden' name="+name+"_id value="+id+"><input type='checkbox'  data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"'  id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td><select  readOnly='readOnly' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name=hard_helpCode id='hard_helpCode_"+sum+"'>"+helpCode+"</select></td>";
	str+="<td><select  class='list_table' onchange=itemName("+sum+",'"+name+"') name=hard_itemId id='hard_itemId_"+sum+"'>"+item+"</select></td>";
	str+="<td><input type='text' onchange=priceChange("+sum+",'"+name+"') class='list_table' name=hard_pice id=hard_pice_"+sum+"  value="+parseFloat(pice)+"></td>";
	str+="<td><input type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=hard_number id=hard_number_"+sum+"  value="+number+"></td>";
	str+="<td><input type='text' readonly='readonly' class='list_table' name=hard_total id=hard_total_"+sum+" value="+parseFloat(total)+"></td>";
	str+="<td><textarea type='text' class='list_table' name=hard_comment id=hard_comment_"+sum+" value='' >"+comment+"</textarea></td></tr>";
	$("#"+name).append(str);
}

//车辆调度方法
function car(sum,id,transportTypeOption,carTypeOption,pickTime,comment){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name='csr_id' value="+id+" >";
	str+="<input type='checkbox' name='carSchedulRecordId' ></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select class='list_table' name=transportTypeId  id=transportTypeId_"+sum+"> "+transportTypeOption+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name=carTypeId id=typeId_"+sum+" >"+carTypeOption+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input data-id='reservation' readOnly='readOnly'  style='width: 180px' data-date-format='yyyy-mm-dd hh:ii' class='required list_table' id=dTime_"+sum+" name='pickTime' ><i style='margin-left: -20px;' class='fa fa-calendar'></i></td>";
	str+="<td><textarea type='text' class='list_table' name=carComment id=comment_"+sum+"  >"+comment+"</textarea></td></tr>";
	$("#car").append(str);
	$("#dTime_"+sum).val(pickTime);
	timeMunite();
}
//添加车辆信息调度
function addCar() {
	sum++;
	car(sum,"","${transportTypeOption}","${carTypeOption}","","")
}
//死亡原因是 枪决的情况
function deathCauseChange(){
	var name=$('#deadReasonId').find("option:selected").text();
	if(name==="枪决"){
		$('#dAddr').val('不详');
		$('#pickAddr').val('不详');
		$('#certificateCode').val('不详');
		$('#age').val('不详');
		$('#fName').val('不详');
		$('#fPhone').val('不详');
		$('#fCardCode').val('不详');
	}
}

//项目类别改变
function itemType(id,type){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemTypeChange',id:$("#typeId_"+id).val()},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	$('#checkbox_'+id).attr('data-findexFlag',json.faIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
				sortItem($("#service"));
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
				sortItem($("#articles"));
    		}
    		$("#pice_"+id).val(json.pice);
	    	$("#total_"+id).val(json.pice);
	    	$("#number_"+id).val(1);
	    	$("#helpCode_"+id).html(json.helpOption);
	    	$("#itemId_"+id).html(json.str);
    		$("#comment_"+id).val("")
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 
//项目名称改变
function itemName(id,type){
	var itemId;
	if(type=="hard"){
		itemId=$("#hard_itemId_"+id).val();
	}else{
		itemId=$("#itemId_"+id).val()
	}
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:itemId},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#service"))
		    	
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#articles"));
    		}
    		if(type=="base"){
    			baseTotal=baseTotal-$("#total_"+id).val();
    			baseTotal+=json.pice;
    			var b=baseTotal.toFixed(2);
				$("#baseFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#base"));
    		}
    		$("#pice_"+id).val(json.pice);
    		$("#helpCode_"+id).html(json.str);
    		var txt=$("#helpCode_"+id).find('option:selected').text();
    		var text=$("#helpCode_"+id).parent().siblings(':last').find('textarea');
    		if('${id}'==""){
    			if(txt=='535'||txt=='538'){
    				if(text.hasClass('required')){return;}
    				text.addClass('required');
    			}else{
    				if(text.hasClass('required')){text.removeClass('required');}
    			}
    		}
	    	$("#number_"+id).val(1);
	    	$("#comment_"+id).val("");
	    	
    		if(type=="hard"){
    			hardTotal=hardTotal-$("#hard_total_"+id).val();
    			hardTotal+=json.pice;
    			var b=hardTotal.toFixed(2);
				$("#hardFont").html(parseFloat(b)+"元");
		    	$("#hard_total_"+id).val(json.pice);
		    	
		    	$("#hard_pice_"+id).val(json.pice);
	    		$("#hard_helpCode_"+id).html(json.str);
		    	$("#hard_number_"+id).val(1);
		    	$("#hard_comment_"+id).val("");
		    	sortItem($("#hard"));
    		}
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	})
} 


//助记码改变
function itemHelpCode(id,type){
	var helpCode;
	if(type=="hard"){
		helpCode=$("#hard_helpCode_"+id).val();
	}else{
		helpCode=$("#helpCode_"+id).val()
	}
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemHelpCodeChange',id:helpCode},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#service"))
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#articles"));
    		}
    		if(type=="base"){
    			baseTotal=baseTotal-$("#total_"+id).val();
    			baseTotal+=json.pice;
    			var b=baseTotal.toFixed(2);
				$("#baseFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#base"));
    		}
    		$("#pice_"+id).val(json.pice);
    		$("#itemId_"+id).html(json.str);
	    	$("#number_"+id).val(1);
	    	$("#comment_"+id).val("");
	    	
    		if(type=="hard"){
    			hardTotal=hardTotal-$("#hard_total_"+id).val();
    			hardTotal+=json.pice;
    			var b=hardTotal.toFixed(2);
				$("#hardFont").html(parseFloat(b)+"元");
		    	$("#hard_total_"+id).val(json.pice)
		    	$("#hard_pice_"+id).val(json.pice);
	    		$("#hard_itemId_"+id).html(json.str);
		    	$("#hard_number_"+id).val(1);
		    	$("#hard_comment_"+id).val("");
		    	sortItem($("#hard"));
    		}
    		
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 
//单价改变
function priceChange(id,type){
	var price=1;
	if(type=="hard"){
		price=$("#hard_pice_"+id).val();
	}else{
		price=$("#pice_"+id).val();
	}
	var a=parseFloat(price);
	if(a!=price){//表示不是数字
		toastr["warning"]("请输入正确的价格!");
	}else{
		var number=1;
		if(type=="hard"){
			number=$("#hard_number_"+id).val();
		}else{
			number=$("#number_"+id).val();
		}
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=price*number;
			var jg=sTotal.toFixed(2);
			$("#sFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=price*number;
			var jg=aTotal.toFixed(2);
			$("#aFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='base'){
			baseTotal=baseTotal-$("#total_"+id).val();
			baseTotal+=price*number;
			var jg=baseTotal.toFixed(2);
			$("#baseFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='hard'){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			hardTotal+=price*number;
			var jg=hardTotal.toFixed(2);
			$("#hardFont").html(parseFloat(jg)+"元");
			$("#hard_total_"+id).val((price*number).toFixed(0));
		}
	}
}
//数目改变
function numbers(id,type){
	var number=1;
	if(type=="hard"){
		number=$("#hard_number_"+id).val();
	}else{		
		number=$("#number_"+id).val();
	}
	var a=parseFloat(number);
	if(a!=number){//表示是不是数字
		toastr["warning"]("请输入正确的数量！");
	}else{
		var pice;
		if(type=="hard"){
			pice=$("#hard_pice_"+id).val();
		}else{
			pice=$("#pice_"+id).val();
		}
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			var jg=sTotal.toFixed(2);
			$("#sFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((pice*number).toFixed(0));
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=pice*number;
			var jg=aTotal.toFixed(2);
			$("#aFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((pice*number).toFixed(0));
		}
		if(type=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			baseTotal+=pice*number;
			var jg=baseTotal.toFixed(2);
			$("#baseFont").html(parseFloat(jg)+"元");
	    	$("#total_"+id).val((pice*number).toFixed(0))
		}
		if(type=="hard"){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			hardTotal+=pice*number;
			var jg=hardTotal.toFixed(2);
			$("#hardFont").html(parseFloat(jg)+"元");
	    	$("#hard_total_"+id).val((pice*number).toFixed(0))
		}
		
	}
} 



//添加基本减免
function addBase() {
	sum++;
	var pice=parseFloat('${baseItem.pice}');
	reduction(sum,"base","","${baseItemOption}","${baseItemHelpCodeOption}",1,pice,"","${baselist[7]}","${baselist[8]}")
	baseTotal+=pice;
	$("#baseFont").html(baseTotal+"元");
}


//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		if(str=="articles"){
			aTotal=aTotal-$("#total_"+id).val();
			$("#aFont").html(aTotal+"元");
		}
		if(str=="service"){
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		}
		if(str=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			$("#baseFont").html(baseTotal+"元");
		}
		if(str=="hard"){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			$("#hardFont").html(hardTotal+"元");
		}
		$(this).parent().parent().remove();
		num++;
		var nn=parseFloat($("#"+str+"Number").text());	
		$("#"+str+"Number").html(nn-1);
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}

}	

//是否申免项目
function is_change(id) {
	if(id=="baseIs"){
		if($("#baseIs").val()=='1'){
			$("#baseLi").show();
		}else{
			$("#baseLi").hide();
		}
	}
	if(id=="hardIs"){
		if($("#hardIs").val()=='1'){
			$("#hardLi").show();
		}else{
			$("#hardLi").hide();
		}
	}
}

//火化炉类型单击
function radio_click(type) {
	if (type==${furnace_ty}) {
		$("#furnaceType_div").show();
		$("#specialBusinessType").hide();
	}else{
		$("#furnaceType_div").hide();
		if(type!=='3'){
			$("#specialBusinessType").hide();
		}else{
			$("#specialBusinessType").show();
		}
		
	}
}
//创建时间
function ndatet(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]+' '+arrTime[1]);
	return checkDate;
}
function ndate(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]);
	return checkDate;
}
$('#orderTime').change(function(){
	if($('#mourningEndTime').val()!==""){
		var checkTime=ndate($('#mourningEndTime').val());
		var now=ndate($(this).val());
		if(now-checkTime<0){
			toastr["error"]("火化预约时间必须大于守灵结束时间！");
			$(this).val(' ');
		}
	}else if($('#farewellBeginDate').val()!==""){
		var checkTime=ndatet($('#farewellBeginDate').val());
		var now=ndatet($(this).val());
		if(now-checkTime<0){
			toastr["error"]("火化预约时间必须大于告别时间！");
			$(this).val(' ');
		}
	}
})
//排序项目
function sortItem(target){
	var trs=target.find('tr');
	
	//trs=Array.prototype.slice.call(trs);
	trs.sort(function(a,b){
		var type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		var type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		if(type1==type2){
			type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
			type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
		}
		return type2>type1?-1:type2<type1?1:0;
	})
	target.html(trs);
}
//$('#service').on('change','tr select[name="typeId"]',sortItem.bind(this,$('#service')));
//$('#articles').on('change','tr select[name="typeId"]',sortItem.bind(this,$('#articles')));
$('#service').on('change','tr select[name="helpCode"]',function(){
	var txt=$(this).find('option:selected').text();
	var text=$(this).parent().siblings(':last').find('textarea');
	if('${id}'==""){
	if(txt=='535'||txt=='538'){
		if(text.hasClass('required')){return;}
		text.addClass('required');
	}else{
		if(text.hasClass('required')){text.removeClass('required');}
	}
	}
})
//告别厅时间改变
function farewellTime_change() { 
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'farewellTime_change',time:$("#farewellBeginDate").val()},
	    success: function (json) {
	    	if (json.statusCode == 200) {
		    	$("#orderTime").val(json.time);
	    	} else {
	    		toastr["warning"]("时间格式有误");
	    	}
		},
	   error: function (e) {
		   toastr["error"]("系统异常");
	   }
	} )
}


//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   { //author: meizz   
	var o = {   
	 "M+" : this.getMonth()+1,                 //月份   
	 "d+" : this.getDate(),                    //日   
	 "h+" : this.getHours(),                   //小时   
	 "m+" : this.getMinutes(),                 //分   
	 "s+" : this.getSeconds(),                 //秒   
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	 "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
	 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
	 if(new RegExp("("+ k +")").test(fmt))   
	fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
}  
/* 保存时候的方法 */ 
function validateSub(form, callback) {
	$("button").prop("disabled", "disabled");
	var $form = $(form);
	/* var va = validHomeForm($form); */
	$(".error-label", form).remove();
	$("[errorFlag]", form).removeAttr("errorFlag");
	$(".error-input", form).removeClass("error-input");
	var errArr=[];
	$(".required", form)
	.each(
			function(i,ele) {
				var value = $(this).val();
				if (value == "") {
					var flag = $(this).attr("errorFlag");
					if (flag == undefined) {
						$(this).attr("errorFlag", "true");
						$(this).addClass("error-input");
						var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
						$(this).parent().append(err);
						errArr.push(i)
					}
				}
			});
	if(errArr.length!==0){
		var errNum=Math.min.apply(Math,errArr);
		var ele=$(".required", form).eq(errNum);
		var errName=$(ele).attr('name');
		var errMsg=errName=='pickTime'?'运送时间':errName=='comment'?'挽联备注':$(ele).prev().text().slice(0,-1);
		if($('#a [name="'+errName+'"]').size()!==0){$('[href="#a"]').click()}
		if($('#b [name="'+errName+'"]').size()!==0){$('[href="#b"]').click()}
		var height=$('.required[name="'+errName+'"]').eq(0).offset().top;
		window.scrollTo(0,height);
		toastr["warning"]("请填写"+errMsg);
		$("button").removeAttr("disabled");
		return false;
	}
	
// 	if (va == false) {
// 		$("button").removeAttr("disabled");
// 		toastr["warning"]("请确保填写完整并且格式正确");
// 		return false;
// 	}
	rel = $form.attr("rel");
	/*保存时发出请求*/
	$.ajax({
   		type : 'POST',
   		url : $form.attr("action"),
   		data : $form.serializeArray(),
   		traditional: true,
   		dataType : "json",
   		cache : false,
   		success : callback,
   		error : function(XMLHttpRequest, textStatus, errorThrown) {
   			toastr["error"](XMLHttpRequest.status);
   			$("button").removeAttr("disabled");
   		}
   	});
	
	return false;
}


</script>
 
<style>
.container-fluid { 
	background: #fff;
} 
</style>
	<section class="content">
	
	
	<div style='opacity:0; position: absolute;'>
			<object id="plugin0" type="application/lagen-plugin" width="0" height="0">
				<param name="onload" value="pluginLoaded" />
			</object>
			<object id="plugin1" type="application/x-lathumbplugin" width="0" height="0">
 				 <param name="onload" value="pluginLoaded" />
 			</object>
	</div>
									
									
									
									
		<form action="${url}" id="homeForm" rel="myModal" onsubmit="return validateSub(this,homeAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" data-changeId="true" value="${commissionOrder.id}">
		<input type="hidden" id="appointmentId" name="appointmentId" value="${appointmentId }" >
		<input type="hidden" id="name" value="${commissionOrder.name }">
		<input type="hidden" id="isSH" name="isSH" value="">
			<div class="box-body">
				<div class="nav-tabs-custom" style="overflow:hidden">
					<ul class="nav nav-tabs">
<!-- 						<li class="active" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab" aria-expanded="true">基本信息</a></li> -->
						<li class="active" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" aria-expanded="true">业务信息</a></li>
<!-- 						<li id='yeInfo' class="active" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" aria-expanded="true">业务信息</a></li> -->
						<li id="baseLi" class="" style="width: 150px; text-align: center;"><a href="#c" data-toggle="tab" aria-expanded="false">基本项目申免</a></li>
						<li id="hardLi" class="" style="width: 150px; text-align: center;"><a href="#d" data-toggle="tab" aria-expanded="false">困难救助减免</a></li>
<!-- 						<li id="" class="" style="width: 150px; text-align: center;"><a href="#e" data-toggle="tab" aria-expanded="false">拍照存档</a></li> -->
					</ul>
					<div class="tab-content" style='padding-left:0px;padding-right:0px;'>
			
			    <!-- 基本信息 -->
				<!-- 业务信息 -->
						<div class="tab-pane active" id="b">
							<div class="box-body border-B">
								<div class="col-md-4 height-align">
									<label class='lab-6'>是否基本减免：</label>
									<select id="baseIs" name="baseIs" onchange="is_change('baseIs')"  class='list_select nopadding-R' >${baseIsOption}</select>
								</div>
								<div class="col-md-4 height-align">
									<label class='lab-6'>是否困难减免：</label>
									<select  id="hardIs" name="hardIs" onchange="is_change('hardIs')" class='list_select nopadding-R' >${hardIsOption}</select>
								</div>
							</div>
						<p class='p border-B'>业务调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label  class='lab-6'>到馆时间：</label> 
									<c:choose>
										<c:when test="${commissionOrder.arriveTime  !=null}">
											<input data-id="reservation" readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="arriveTime" name="arriveTime" 
											value='<fmt:formatDate value="${commissionOrder.arriveTime }" pattern="yyyy-MM-dd HH:mm"/>'>
										</c:when>
										<c:otherwise>
											<input data-id="reservation" data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="arriveTime" name="arriveTime" value="${now }">
										</c:otherwise>
									</c:choose>
									
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>接尸单位：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="corpseUnitId" id="corpseUnitId">
										${corpseUnitOption}
									</select>
									<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>冷藏柜号：</label> 
									<input type="hidden" name="freezerId" id="freezerId"  value="${freezerRecord.freezerId }">
									<input name="freezerName" id="freezerName" class=" list_select" readonly="readonly" value="${freezerRecord.freezer.name }">
								</div>
								<div class="col-md-7 height-align">
									<label  class='lab-6'>挂账单位：</label> 
									<select style="width: 169px" class="nopadding-R list_select nopadding-R" name="billUnitId" id="billUnitId">
										${billUnitOption}
									</select>	
								</div>
								<div class="col-md-5 height-align"> 
									<label  class='lab-6'>守灵室号：</label> 
									<c:choose>									
										<c:when test="${mourningCode2!=null }">
											<input type="hidden" name="mourningId" id="mourningId"  value="${mourningId2 }">
											<input name="mourningName" id="mourningName" class=" list_select" readonly="readonly" value="${mourningCode2}">
										</c:when>
										<c:when test="${mourningCode!=null }">
											<input type="hidden" name="mourningId" id="mourningId"  value="${mourningId1 }">
											<input name="mourningName" id="mourningName" class=" list_select" readonly="readonly" value="${mourningCode}">
										</c:when>
										<c:otherwise>
											<input type="hidden" name="mourningId" id="mourningId"  value="${mourningRecord.mourningId }">
											<input name="mourningName" id="mourningName" class=" list_select" readonly="readonly" value="${mourningRecord.mourning.name}">
										</c:otherwise>
									</c:choose>
									
									<a href="${url}?method=editMourning&current_time=${mourningRecord.beginTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								</div>
								<div class="col-md-7 height-align">
									<label  class='lab-6'>守灵时间：</label>
									<c:choose>									
										<c:when test="${mourningBeginTime2!=null }">
											<input  readonly="readonly" class="list_select" id="mourningBeginTime" name="mourningBeginTime" value='<fmt:formatDate value="${mourningBeginTime2}" pattern="yyyy-MM-dd HH:mm"/>'>
											<span class='timerange'>--</span>
											<input  readonly="readonly" class="list_select" id="mourningEndTime" name="mourningEndTime" value='<fmt:formatDate value="${mourningEndTime2}" pattern="yyyy-MM-dd HH:mm"/>'>
										</c:when>
										<c:when test="${mourningBeginTime!=null }">
											<input  readonly="readonly" class="list_select" id="mourningBeginTime" name="mourningBeginTime" value="${mourningBeginTime}">
											<span class='timerange'>--</span>
											<c:if test="${mourningEndTime !=null }">
											<input  readonly="readonly" class="list_select" id="mourningEndTime" name="mourningEndTime" value="${mourningEndTime}">
											</c:if>
											<c:if test="${mourningEndTime ==null }">
											<input  readonly="readonly" class="list_select" id="mourningEndTime" name="mourningEndTime" value="${mourningBeginTime}">
											</c:if>
										</c:when>
										<c:otherwise>
											<input  readonly="readonly" class="list_select" id="mourningBeginTime" name="mourningBeginTime" value='<fmt:formatDate value="${mourningRecord.beginTime }" pattern="yyyy-MM-dd HH:mm"/>'>
											<span class='timerange'>--</span>
											<input  readonly="readonly" class="list_select" id="mourningEndTime" name="mourningEndTime" value="<fmt:formatDate value="${mourningRecord.endTime }" pattern="yyyy-MM-dd HH:mm"/>">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>告别厅号：</label>
									<c:choose>					
										<c:when test="${farewellCode2!=null }">
											<input type="hidden" name="farewellId" id="farewellId"  value="${farewellId2}">
											<input name="farewellName" id="farewellName" class=" list_select" readonly="readonly" value="${farewellCode2}">
										</c:when>				
										<c:when test="${farewellCode!=null }">
											<input type="hidden" name="farewellId" id="farewellId"  value="${farewellId1 }">
											<input name="farewellName" id="farewellName" class=" list_select" readonly="readonly" value="${farewellCode}">
										</c:when>
										<c:otherwise>
											<input type="hidden" name="farewellId" id="farewellId"  value="${ farewellRecord.farewellId}">
											<input name="farewellName" id="farewellName" class=" list_select" readonly="readonly" value="${ farewellRecord.farewell.name}">
										</c:otherwise>
									</c:choose>
									<a href="${url }?method=editFarewell&current_time=${farewellRecord.beginDate }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								</div>
								<div class="col-md-7 height-align">
									<label  class='lab-6'>告别时间：</label>
									<%-- <input data-id="reservation" readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="<fmt:formatDate value="${farewellRecord.beginDate }" pattern="yyyy-MM-dd HH:mm:ss"/>"> --%>
									<c:choose>									
										<c:when test="${farewellTime2!=null }">
											<input  readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="<fmt:formatDate value="${farewellTime2}" pattern="yyyy-MM-dd HH:mm"/>">
										</c:when>
										<c:when test="${farewellTime!=null }">
											<input  readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="${farewellTime}">
										</c:when>
										<c:otherwise>
											<input  readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="<fmt:formatDate value="${farewellRecord.beginDate }" pattern="yyyy-MM-dd HH:mm"/>">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-md-5" id="furnace_div">
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>火化预约时间：</label> 
									<c:choose>		
										<c:when test="${funeralTime2!=null }">
											<input data-id="reservation"  class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${funeralTime2 }" pattern="yyyy-MM-dd HH:mm"/>">
										</c:when>							
										<c:when test="${funeralTime!=null }">
											<input data-id="reservation"   class="list_select" id="orderTime" name="orderTime" value="${funeralTime }">
										</c:when>
										<c:when test="${furnaceRecord.orderTime!=null }">
											<input data-id="reservation"   class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm"/>">
										</c:when>
										<c:otherwise>
											<input data-id="reservation"  class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${fireTime }" pattern="yyyy-MM-dd HH:mm"/>">
										</c:otherwise>
									</c:choose>
									
								</div>
								<div class="col-md-6 height-align" id="furnaceType_div">
									<label  class='lab-6'>火化炉号：</label>
									<c:choose>
										<c:when test="${funeralCode2!=null}">
											<input type="hidden" name="furnaceId" id="furnaceId"  value="${funeralId2 }">
											<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${funeralCode2 }">
											<a href="${url}?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
										</c:when>
										<c:when test="${funeralCode!=null }">
											<input type="hidden" name="furnaceId" id="furnaceId"  value="${funeralId1 }">
											<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${funeralCode }">
											<a href="${url}?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
										</c:when>
										<c:otherwise>
											<input type="hidden" name="furnaceId" id="furnaceId"  value="${furnaceRecord.furnaceId }">
											<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${furnaceRecord.furnace.name }">
											<a href="${url}?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-md-6 height-align" id="specialBusinessType">
									<label class='lab-6'>特殊业务类型:</label>
									<select style="width: 169px" class="nopadding-R list_select " name="specialBusinessValue" id="specialBusinessValue">
										${specialBusinessOption}
									</select>	
								</div>
							</div>
							<p class='border-B p'>车辆调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-12"  style='padding-left:0px;'>
									<small class="btns-buy"> 
										<button type="button" onclick="addCar()" class="btn btn-warning" >添加</button>
										<button type="button" onclick="delHtml('carSchedulRecordId')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="carSchedulRecordId" /></th>
											<th width="200px">运输类型</th>
											<th width="200px">车辆类型</th>
											<th width="200px">运送时间</th>
											<th>备注</th>
										</tr>
									</thead>
									<!-- 车辆调度信息 -->
									<tbody id="car">
										
									</tbody>
								</table>
							</div>
							<p class='p border-B'>服务项目信息</p>
							<div class="box-body border-B">
									<!-- 这里显示已经收费的具体项目信息 -->
									<div class="col-md-12" style="padding-left:10px;">
										<table class="table table-bordered" > 
											<thead>
												<tr>
													<th>助记码</th>
													<th>名称</th>
													<th>单价(元)</th>
													<th>数量</th>
													<th>挂账</th>
													<th>小计</th>
													<th>付款时间</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												    <c:when test="${fn:length(serviceList)==0 }">
													    <tr>
											    			<td colspan="7">
														  		<font color="Red">没有收费数据</font>
													 		</td>
													    </tr>
												    </c:when>
												    <c:otherwise>
													    <c:forEach items="${serviceList }" var="u">
														<tr>
															<td>${u.item.helpCode }</td>
									    					<td>${u.item.name }</td>
									    					<td>${u.pice }</td>
									    					<td>${u.number }</td>
									    					<td>
										    					<c:if test="${u.tickFlag==IsHave_No }">
										    						<font color="red" class='state1'>否</font>
										    					</c:if>
										    					<c:if test="${u.tickFlag==IsHave_Yes }">
										    						<font color="green" class='state1'>是</font>
										    					</c:if>
									    					</td>
									    					<td>${u.total }</td>
									    					<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														</tr>
														</c:forEach>
															<tr>
																<td>合计：</td>
																<td colspan='6' style='text-align: left;' >
																<font color='red'>${serTotal } 元</font>
																<div style="float: right; margin-right: 50%;">
																	<font color='black' >合计：共</font>
																	<font color='red'>${fn:length(serviceList) }</font>
																	<font color='black' >条记录</font>
																</div>
																</td>
															</tr>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
									<div class="col-md-12"  style='padding-left:0px; padding-bottom: 10px;'>
										<small class="btns-buy"> 
											<a href="${url}?method=selectItems&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
											<button type="button" onclick="delHtml('service')" class="btn btn-danger " >删除</button>
										</small>
									</div>
									<div class="box-body">										
										<table class="table table-bordered" style="margin-top: 60px;" >
											<thead>
												<tr>
													<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="service" /></th>
													<th width="90px">助记码</th>
													<th width="250px">名称</th>
											        <th width="90px">单价（元）</th>
											        <th width="135px">类别</th>
											        <th width="60px">数量</th>
											        <th width="70px">挂账</th>
											        <th width="90px">小计</th>
											        <th>备注</th>
												</tr>
											</thead>
											<tbody id="service">
												
											</tbody>
											<tbody>
												<tr>
													<td style='width: 60px'>合计：</td>
													<td colspan='8' style='text-align: left;' >
													<font color='red' id="sFont">
													0元
													</font>
													<div style="float: right; margin-right: 50%;">
														<font color='black' >合计：共</font>
														<font color='red' id='serviceNumber' >0</font>
														<font color='black' >条记录</font>
													</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
<!-- 								</div> -->
							</div>
							<p class='p border-B'>丧葬用品信息</p>
							<div class="box-body">
									<!-- 这里显示已经收费的具体项目信息 -->
									<div class="col-md-12" style="padding-left:10px;">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>助记码</th>
													<th>名称</th>
													<th>单价(元)</th>
													<th>数量</th>
													<th>挂账</th>
													<th>小计</th>
													<th>付款时间</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												    <c:when test="${fn:length(articlesList)==0 }">
													    <tr>
											    			<td colspan="7">
														  		<font color="Red">没有收费数据</font>
													 		</td>
													    </tr>
												    </c:when>
												    <c:otherwise>
													    <c:forEach items="${articlesList }" var="u">
														<tr>
															<td>${u.item.helpCode }</td>
									    					<td>${u.item.name }</td>
									    					<td>${u.pice }</td>
									    					<td>${u.number }</td>
									    					<td>
										    					<c:if test="${u.tickFlag==IsHave_No }">
										    						<font color="red" class='state1'>否</font>
										    					</c:if>
										    					<c:if test="${u.tickFlag==IsHave_Yes }">
										    						<font color="green" class='state1'>是</font>
										    					</c:if>
									    					</td>
									    					<td>${u.total }</td>
									    					<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														</tr>
														</c:forEach>
															<tr>
																<td>合计：</td>
																<td colspan='6' style='text-align: left;' >
																<font color='red'>${artTotal } 元</font>
																<div style="float: right; margin-right: 50%;">
																	<font color='black' >合计：共</font>
																	<font color='red'>${fn:length(articlesList) }</font>
																	<font color='black' >条记录</font>
																</div>
																</td>
															</tr>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
									<div class="col-md-12" style='padding-left:0px; padding-bottom: 10px;'>
										<small class="btns-buy" >
											<a href="${url}?method=selectItems&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
											<button type="button" onclick="delHtml('articles')" class="btn btn-danger " >删除</button>
										</small>
									</div>
								<div class="box-body">
								<table class="table table-bordered" style="margin-top: 60px;" >
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="articles" /></th>
											<th width="90px">助记码</th>
											<th width="250px">名称</th>
									        <th width="90px">单价（元）</th>
									        <th width="135px">类别</th>
									        <th width="60px">数量</th>
									        <th width="70px">挂账</th>
									        <th width="90px">小计</th>
									         <th>备注</th>
										</tr>
									</thead>
									<tbody id="articles">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="aFont">
											0元
											</font>
											<div style="float: right; margin-right: 50%;">
												<font color='black' >合计：共</font>
												<font color='red' id='articlesNumber' >0</font>
												<font color='black' >条记录</font>
											</div>
											</td>
										</tr>
									</tbody>
								</table>
								</div>
							</div>
						</div>
				<!-- 基本项目申免 -->
						<div class="tab-pane" id="c">
							<div class="box-body">
								<input type="hidden" name="baseReductionId" value="${baseReduction.id }">
								<div class="col-md-12" id="freePerson"></div>
								<div class="col-md-12" id="hardPerson"></div>
							</div>
							<div class="box-body">
								<!-- 这里显示已经收费的基本减免项目信息 -->
								<div class="col-md-12" style="padding-left:10px;">
									<table class="table table-bordered">
										<thead>
											<tr>
<!-- 												<th>助记码</th> -->
												<th>名称</th>
												<th>单价(元)</th>
												<th>数量</th>
												<th>小计</th>
												<th>付款时间</th>
											</tr>
										</thead>
										<tbody>
											<c:choose>
											    <c:when test="${fn:length(baseBeforeList)==0 }">
												    <tr>
										    			<td colspan="6">
													  		<font color="Red">没有基本减免</font>
												 		</td>
												    </tr>
											    </c:when>
											    <c:otherwise>
												    <c:forEach items="${baseBeforeList }" var="u">
													<tr>
<%-- 														<td>${u.item.helpCode }</td> --%>
								    					<td>${u.name }</td>
								    					<td>${u.pice }</td>
								    					<td>${u.number }</td>
								    					<td>${u.total }</td>
								    					<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
													</tr>
													</c:forEach>
														<tr>
															<td>合计：</td>
															<td colspan='4' style='text-align: left;' >
															<font color='red'>${baseTotal } 元</font>
															<div style="float: right; margin-right: 50%;">
																<font color='black' >合计：共</font>
																<font color='red'>${fn:length(baseBeforeList) }</font>
																<font color='black' >条记录</font>
															</div>
															</td>
														</tr>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</div>
								<!-- 新加的基本减免 -->
								<div class="col-md-12" style='padding-left:0px; padding-bottom: 10px;'>
									<small class="btns-buy"  style='padding-left:0px;'> 
										<button type="button" onclick="addBase()" class="btn btn-warning" >添加</button>
										<button type="button" onclick="delHtml('base')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="60px"><input type="checkbox" class="checkBoxCtrl" group="base" /></th>
											<th width="100px">助记码</th>
											<th width="260px">名称</th>
									        <th width="120px">单价（元）</th>
									        <th width="100px">数量</th>
									        <th width="120px">总价</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="base">
										
									</tbody>
									<tbody>
										<tr>
											<td >合计：</td>
											<td colspan='7' style='text-align: left;' >
											<font color='red' id="baseFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								
								<div class="col-md-12">
									<label>备注</label> 
									<textarea type="text" class="form-control" name="bComment" value="${baseReduction.comment }" style="width: 100%;"></textarea>
								</div>
							</div>
						</div>
					<!-- 困难救助减免 -->
					<div class="tab-pane" id="d">
						<div class="box-body border-B">
								<div class="col-md-12" id="prove"></div>
								
								<div class="col-md-5 height-align">
									<label  class='lab-6'>申请日期：</label>
									<input data-id="reservation" readOnly='readOnly'  data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="createTime" name="createTime" value='<fmt:formatDate value="${hardReduction.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>申请人姓名：</label>
									<input type="text" class="list_select" name="applicant" value="${hardReduction.applicant }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>申请原因：</label>
									<input type="text" class="list_select" name="reason" value="${hardReduction.reason }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>联系电话：</label>
									<input type="text" class="list_select" name="phone" value="${hardReduction.phone }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>证明单位：</label>
									<select class="list_select nopadding-R" name="hard_proveUnitId" id="hard_proveUnitId" >
										${hard_proveUnitOption}
									</select>&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-6'>与死者关系：</label>
									<select style="width: 169px" class="list_select nopadding-R" name="appellationId" id="appellationId"> 
									    ${hard_appellationOption}
									</select>
								</div>
								<div class="col-md-6 height-align">
								<input type="hidden" name="hardReductionId" value="${hardReduction.id }">
								<label   class='lab-6'>是否特殊减免：</label>
								<select  class='list_select nopadding-R' name="special" id="special">${specialOption}</select>
								</div>
							</div>
							<div class="box-body">
								<!-- 已收费的困难减免项 -->
								<div class="col-md-12" style="padding-left:10px;">
									<table class="table table-bordered">
										<thead>
											<tr>
<!-- 												<th>助记码</th> -->
												<th>名称</th>
												<th>单价(元)</th>
												<th>数量</th>
												<th>小计</th>
												<th>付款时间</th>
											</tr>
										</thead>
										<tbody>
											<c:choose>
											    <c:when test="${fn:length(hardBeforeList)==0 }">
												    <tr>
										    			<td colspan="5">
													  		<font color="Red">没有困难减免</font>
												 		</td>
												    </tr>
											    </c:when>
											    <c:otherwise>
												    <c:forEach items="${hardBeforeList }" var="u">
													<tr>
<%-- 														<td>${u.item.helpCode }</td> --%>
								    					<td>${u.name }</td>
								    					<td>${u.pice }</td>
								    					<td>${u.number }</td>
								    					<td>${u.total }</td>
								    					<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
													</tr>
													</c:forEach>
														<tr>
															<td>合计：</td>
															<td colspan='4' style='text-align: left;' >
															<font color='red'>${hardTotal } 元</font>
															<div style="float: right; margin-right: 50%;">
																<font color='black' >合计：共</font>
																<font color='red'>${fn:length(hardBeforeList) }</font>
																<font color='black' >条记录</font>
															</div>
															</td>
														</tr>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</div>
								
								<div class="col-md-12" style='padding-left:0px; padding-bottom: 10px;'>
									<small class="btns-buy"  style='padding-left:0px;'> 
										<a href="${url}?method=selectItems&id=hard" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<button type="button" onclick="delHtml('hard')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="60px"><input type="checkbox" class="checkBoxCtrl" group="hard" /></th>
											<th width="100px">助记码</th>
											<th width="260px">名称</th>
									        <th width="120px">单价（元）</th>
									        <th width="100px">数量</th>
									        <th width="120px">总价</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="hard">
										
									</tbody>
									<tbody>
										<tr>
											<td >合计：</td>
											<td colspan='6' style='text-align: left;' >
											<font color='red' id="hardFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="col-md-12">
									<label>备注</label> 
									<textarea type="text" class="form-control" name="hComment" value="${hardReduction.comment }" style="width: 100%;"></textarea>
								</div>
							</div>
					</div>
					<!-- 拍照 -->			
					</div>
				</div>
			</div>
			<div class="box-body">
			<div  class="container-fluid">
				<div class='row padding-B'>
					<div class='col-md-6'>
						<div class='row'>
							<div class="col-md-5 height-align">
								<label>业务登记员：</label>
								<c:choose>
								    <c:when test="${commissionOrder.agentUser.name!=null}">
								    	${commissionOrder.agentUser.name}
								    </c:when>
								    <c:otherwise>
								   		${agentUser}
									</c:otherwise>
								</c:choose> 
								
							</div>
							<div class="col-md-6 height-align">
								<label>预约登记人：</label>
								<span>${commissionOrder.pbookAgentId }</span>
							</div>
							
							<div class="col-md-5 height-align">
								<label>到馆登记人：</label>
								<span>${commissionOrder.bookAgentId }</span>
							</div>
							<div class="col-md-7 height-align">
								<label class='lab-3'>编号：</label> 
								<input type="text" class="list_select" readonly="readonly"  value="${commissionOrder.code == null ? '由系统自动生成' : commissionOrder.code}">
							</div>
							<div class="col-md-6 height-align">
								<label >办理时间：</label> 
								${time }
							</div>
						</div>
					</div>
					<div class='col-md-6 height-align'>
						<div class='row'>
							<div class="col-md-12">
								<small class="pull-right btns-hometab">
								<c:if test="${checkFlag!=Check_Yse }">  <!-- 这个判断条件也不知道是啥意义，后台传过来应该是 Check_Yes?? -->
									<button type="button" onclick="checkSelect()" class="btn btn-info" style='margin-top:30px;'>保存</button>
			   					</c:if>
								<a href="${url}?method=list" target="homeTab" style='margin-top:30px;' class="btn btn-default" role="button">返回</a>
								</small>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</form>
		
	</section>
<script type="text/javascript">
	
	
	
//若有基本减免，则必须勾选对象类别

function checkSelect() {
	if(!$('#corpseUnitId').val()){
		toastr["warning"]("请填写接尸单位");
		$('#yeInfo').find('a').click();
		var top=$('#corpseUnitId').offset().top;
		window.scrollTo(0,top);
		return false;
	}
	
	if($("#baseIs").val()=='${Is_Yes}'){
		if($("input[name='freePersonId']:checked").size()===0 && $("input[name='hardPersonId']:checked").size()===0){
			toastr["warning"]("请选择免费对象类别或重点类别");
			$('#baseLi').find('a').click();
			var top=$('#baseLi').offset().top;
			window.scrollTo(0,top);
			return false;
		} 
	}
	if ($("#hardIs").val()=='${Is_Yes}') {
		if($("input[name='proveIds']:checked").size()===0){
			toastr["warning"]("请选择证件类型");
			$('#hardLi').find('a').click();
			var top=$('#hardLi').offset().top;
			window.scrollTo(0,top);
			return false;
		}
		if(!$('#hard_proveUnitId').val()){
			toastr["warning"]("请选择证明单位");
			$('#hardLi').find('a').click();
			var top=$('#hardLi').offset().top;
			window.scrollTo(0,top);
			return false;
		}
	}
	if($('input[type="radio"]:checked').val()==1&&$('#orderTime').val().trim()==''){
		toastr["warning"]("请填写火化预约时间");
		var top=$('#orderTime').offset().top;
		window.scrollTo(0,top);
		return false;
	}
	if($('input[type="radio"]:checked').val()==2&&$('#furnaceId').val().trim()==''){
		toastr["warning"]("请选择特约炉号");
		var top=$('#furnaceName').offset().top;
		window.scrollTo(0,top);
		return false;
	}
	//本页面另有一个检测方法，但不完善，新加一个
	//检测灵堂挽联和告别厅挽联的备注情况
	var isReturn=false;
	var x=0;
	$("input[name='service']").each(function(){
		var id = $(this).attr("value");
		var check=$("#helpCode_"+id).find("option:selected").text();
		if("538"===check||"535"===check){//礼厅挽联或者灵堂挽联
			var comment=$("#comment_"+id).val();
			if(comment===""){
				x=id;
				isReturn=true;
			}
		}
	});
	//假如条件成立，说明挽联没写备注
	if(isReturn){
		toastr["warning"]("请填写挽联备注");
		var top=$('#helpCode_'+x).offset().top;
		window.scrollTo(0,top);
		return false;
	}
	$("#homeForm").submit();
};

</script>



