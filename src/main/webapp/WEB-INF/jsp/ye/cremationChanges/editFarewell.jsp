<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style>
#example2_wrapper td:hover{
	background-color:#f39c12;
	cursor:pointer;
}
.table.table-bordered td{ width:100px;}

</style>
<script>
		(function(){
			$('#example2_wrapper table tbody>tr td[data-num]').each(function(){
				var data=$(this).attr('data-num');
				switch (data){   
					case '${IsFlag_Yse }':$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '${IsFlag_Bzwc }':$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
						break;
					case '${IsFlag_Lock }':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
					case '${IsFlag_Decrate }':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
					case '${IsFlag_YY}':
						$(this).css({'backgroundColor':'#673ab7','color':'#fff'});
						break;
					case '11':
						$(this).css({'backgroundColor':'#aaa','color':'#fff'});
						break;
					/* case '2':
						$(this).html('进行').css({'backgroundColor':'#738ffe','color':'#fff'});
						break; */
					/* case '5':
						$(this).html('维修完').css({'backgroundColor':'#9CCC65','color':'#fff'});
						break; */
				}
			})
		})();
</script>
<script type="text/javascript">
//解除锁定
$("#unLock").click(function(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'html',
	    cache: false,
	    data:{method:'unLock'},
	    success: function (json) {
			alert("解锁成功！");
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
});
//重置
$("#reset").click(function(){
	$("#farewellId").val("");
	$("#farewellName").val("");
	$("#farewellBeginDate").val("");
// 	$("#orderTime").val("");
});





//当前时间改变
function changeDate(){
	var date=$("#current_time").val();
	$.ajax({
	    url: "cremationChanges.do",
	    dataType:'html',
	    cache: false,
	    data:{method:'editFarewell',current_time:date},
	    success: function (json) {
	    	$("#myModal").html(json);
			$("#myModal").modal("show");
			initTab();
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
}


//单元格单击
function td_click(i,a){
	var date=$("#current_time").val();
	$.ajax({
	    url: "cremationChanges.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'farewell_click',col:a,row:i,date:date},
	    success: function (json) {
	    	$("#farewellId").val(json.id);
	    	$("#farewellName").val(json.name);
	    	$("#farewellBeginDate").val(json.time);
	    	
	    	if($('#mourningEndTime').val()!==''){
	    		var checkTime=ndate($('#mourningEndTime').val());
	    		var now=ndate($('#farewellBeginDate').val());
	    		if(now-checkTime<0){
	    			 toastr["error"]("告别时间必须大于等于守灵结束时间！");
	    			$("#farewellBeginDate").val(' ');
	    			return false;
	    		}
	    	}
	    	if($('#orderTime').val()!==''){
	    		var checkTime=ndatet($('#orderTime').val());
	    		var now=ndatet($('#farewellBeginDate').val());
	    		if(now-checkTime>0){
	    			 toastr["error"]("告别时间必须小于火化预约时间！");
	    			$("#farewellBeginDate").val(' ');
	    			return false;
	    		}
	    	}
	    	if($('#orderTime').val()===''){
    			$("#orderTime").val(json.fTime);    			
    		}
    		$("#myModal").modal("hide");
    		//有冰柜项目，且没有灵堂项目，以告别厅为准
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
// 	$('#orderTime').change();
}

</script>
	<div class="modal-dialog" role="document" style="width:1200px;">
		<form action="${url}" id="form1" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				 <div style="text-align: center;">
					<h4 class="modal-title" id="myModalLabel">告别厅使用情况</h4>
				 </div>
			</div>
			<div class="box-body">
				<div class="col-md-12" style="text-align: center;">
					<a href="${url}?method=editFarewell&time=-1&current_time=${date}"  target="dialog" rel="myModal" id="front">前一天</a> 
					<label > ${date }</label> 
					<a href="${url}?method=editFarewell&time=1&current_time=${date}"  target="dialog" rel="myModal" id="back">后一天</a> 
					<input type="text" data-provide="datetimepicker" id="current_time" name="current_time" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  value="${date }">
					<i style="margin-left: -20px;" class="fa fa-calendar"></i>
				</div>
				<div class="col-md-12">
					<ul class="la_ul">
						<li style="background: #9ccc65;"><div>布置完成</div></li>
						<li style="background: #fea625;"><div>锁定</div></li>
						<li style="background: #673ab7;"><div>预定</div></li>
						<li style="background: #29b6f6;"><div>占用</div></li>
						<li style="background: #e84e40;"><div>装修</div></li>
					</ul>
				</div>
			</div>
			<div class="box-body" style='padding:20px;  overflow-x: scroll;'>
					<div id="example2_wrapper" style="width:1500px;"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover1" >
									<c:forEach items="${list }" var="u" varStatus="i">
										<c:choose>
											<c:when test="${i.index==0 }">
												<thead>
													<tr role="row">
														<th>${u[0]}</th>
														<c:forEach begin="1" end="13" var="a">
														<th colspan="2"><fmt:formatDate value="${u[a]}" pattern="HH:mm"/></th>
														</c:forEach>
													</tr>
												</thead>
											</c:when>
											<c:otherwise>
												<tr role="row">
													<td><font color="blue">${u[0].name }</font></td>
													<c:forEach begin="1" end="26" var="a">
														<c:if test="${u[a]==null }">
															<td  style="width: 44px;padding: 1px" onclick="td_click(${i.index},${a })" id="td_${i.index}_${a }"></td>
														</c:if>
														<c:if test="${u[a]!=null }">
															<c:choose>
																<c:when test="${u[a].flag == IsFlag_Decrate }">
																	<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																		<a style="color:#fff" href="repairRecord.do?method=info&type=fare&id=${u[a].id}&&itemId=${u[0].id }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Lock }">
																	<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
																		${u[a].order.name}
																	</td>
																</c:when>
																<c:when test="${u[a].flag == 11 }">
																	<td  data-num='${u[a].flag}' title='此处由${u[a].user.name}临时锁定,<fmt:formatDate value="${u[a].beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
																		临时锁定
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_YY}">
																	<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
																		${u[a].appointmentName }
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Bzwc}">
																	<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
																		${u[a].order.name}
																 	</td>
																</c:when>
																<c:otherwise>
																	<td data-num='${u[a].flag}'></td>
																</c:otherwise>
															</c:choose>
														</c:if>
													</c:forEach>
												</tr>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</table>
								
							</div>
						</div>
					</div>
					<div class='btns-dialog'>
						<input type="button" class='btn btn-margin btn-color-9E8273' data-dismiss="modal" id="reset" value="重置" />
						<input type="button" class="btn btn-margin btn-info" data-dismiss="modal" id="unLock" value="解除锁定" />
					</div>
				</div>
			</div>
		</form>
	</div>