<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style>
	.ID-pic.ID-pic1 img {
    //position: absolute;
    height: auto !important;}
</style>
<script type="text/javascript">
var preConfigList = new Array(); var previewJson = new Array(); 

var furnaceOrderTime="";
var fareOrderTime="";

var state='${id}';
var before='';
var mourningBefore='';
var farewellBefore='';
var funBefore='';
var mourningBefore2='';
$(function(){
	timeRange();
	timeMunite();
	deadTypeChange();
	if("${specialBusinessOption}"=="" || "${specialBusinessOption}"==null){
		$("#specialBusinessType").hide();
	}
});
//死亡原因是 枪决的情况
function deathCauseChange(){
	var name=$('#deadReasonId').find("option:selected").text();
	if(name==="枪决"){
		$('#dAddr').val('不详');
		$('#pickAddr').val('不详');
		$('#certificateCode').val('不详');
		$('#age').val('不详');
		$('#fName').val('不详');
		$('#fPhone').val('不详');
		$('#fCardCode').val('不详');
	}
}


//死亡类型改变
function deadTypeChange(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'deadTypeChange',id:$("#deadTypeId").val(),reasonId:$("#deadReasonId").val()},
	    success: function (json) {
	    	$("#deadReasonId").html(json.str);
	    	if(json.deadFlag==2){
	    		$("#ifNormal").removeClass('hide');
	    		$("#proveUnitId").addClass('required');
	    	}else{
	    		$("#ifNormal").addClass('hide');
	    		$("#proveUnitId").removeClass('required');
	    	}
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 






//创建时间
function ndatet(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]+' '+arrTime[1]);
	return checkDate;
}
function ndate(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]);
	return checkDate;
}


//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   { //author: meizz   
	var o = {   
	 "M+" : this.getMonth()+1,                 //月份   
	 "d+" : this.getDate(),                    //日   
	 "h+" : this.getHours(),                   //小时   
	 "m+" : this.getMinutes(),                 //分   
	 "s+" : this.getSeconds(),                 //秒   
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	 "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
	 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
	 if(new RegExp("("+ k +")").test(fmt))   
	fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
}  
/* 保存时候的方法 */ 
function validateSub(form, callback) {
	$("button").prop("disabled", "disabled");
	var $form = $(form);
	/* var va = validHomeForm($form); */
	$(".error-label", form).remove();
	$("[errorFlag]", form).removeAttr("errorFlag");
	$(".error-input", form).removeClass("error-input");
	var errArr=[];
	$(".required", form)
	.each(
			function(i,ele) {
				var value = $(this).val();
				if (value == "") {
					var flag = $(this).attr("errorFlag");
					if (flag == undefined) {
						$(this).attr("errorFlag", "true");
						$(this).addClass("error-input");
						var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
						$(this).parent().append(err);
						errArr.push(i)
					}
				}
			});
	if(errArr.length!==0){
		var errNum=Math.min.apply(Math,errArr);
		var ele=$(".required", form).eq(errNum);
		var errName=$(ele).attr('name');
		var errMsg=errName=='pickTime'?'运送时间':errName=='comment'?'挽联备注':$(ele).prev().text().slice(0,-1);
		if($('#a [name="'+errName+'"]').size()!==0){$('[href="#a"]').click()}
		if($('#b [name="'+errName+'"]').size()!==0){$('[href="#b"]').click()}
		var height=$('.required[name="'+errName+'"]').eq(0).offset().top;
		window.scrollTo(0,height);
		toastr["warning"]("请填写"+errMsg);
		$("button").removeAttr("disabled");
		return false;
	}
	
// 	if (va == false) {
// 		$("button").removeAttr("disabled");
// 		toastr["warning"]("请确保填写完整并且格式正确");
// 		return false;
// 	}
	rel = $form.attr("rel");
	$.ajax({
		type : 'POST',
		url : $form.attr("action"),
		data : $form.serializeArray(),
		traditional: true,
		dataType : "json",
		cache : false,
		success : callback,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			toastr["error"](XMLHttpRequest.status);
			$("button").removeAttr("disabled");
		}
	});
	
	return false;
}


</script>
 
<style>
.container-fluid { 
	background: #fff;
} 
</style>
	<section class="content">
	
	<div style='opacity:0; position: absolute;'>
			<object id="plugin0" type="application/lagen-plugin" width="0" height="0">
				<param name="onload" value="pluginLoaded" />
			</object>
			<object id="plugin1" type="application/x-lathumbplugin" width="0" height="0">
 				 <param name="onload" value="pluginLoaded" />
 			</object>
			</div>
									
									
									
									
		<form action="${url}" id="homeForm" rel="myModal" onsubmit="return validateSub(this,homeAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" data-changeId="true" value="${commissionOrder.id}">
		<input type="hidden" id="name" value="${commissionOrder.name }">
		<input type="hidden" id="menu" name="menu" value="${menu }">
			<div class="box-body">
				<div class="nav-tabs-custom" style="overflow:hidden">
					<ul class="nav nav-tabs">
						<li class="active" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab" aria-expanded="true">基本信息</a></li>
						<li id="" class="" style="width: 150px; text-align: center;"><a href="#e" data-toggle="tab" aria-expanded="false">拍照存档</a></li>
					</ul>
					<div class="tab-content" style='padding-left:0px;padding-right:0px;'>
			
			    <!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						<!-- 选择申免项目 -->
						<P class='p border-B'>死者信息</P>
							<div class="box-body broder-B">
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者姓名：</label>
									<input type="text" style='width:20%' autocomplete="off" class="required list_select" name="name" value="${commissionOrder.name }">&nbsp;<i class="fa fa-circle text-red"></i>
									<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardD('name')">
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死者地址：</label> 
									<input type="text" autocomplete='off' style="width:330px;" class="required list_select" name="dAddr" id="dAddr" value="${commissionOrder.dAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件类型：</label>
									<select style="width: 169px" class="list_select" name="certificateId" id="certificateId">
										${certificateOption}
									</select>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>接尸地址：</label> 
									<input list="pickAddr1" autocomplete='off' style="width:330px;" class="required  list_select" name="pickAddr" id="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
									<datalist  class="list_select"  id="pickAddr1">
										${corpseAddOption}
									</datalist>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件号码：</label> 
									<input type="text" class="required list_select" autocomplete="off" name="certificateCode" id="certificateCode" value="${commissionOrder.certificateCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡类型：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="deadTypeId" id="deadTypeId" onchange="deadTypeChange()">
										${deadTypeOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者年龄：</label> 
									<input type="text" class="required  number list_select" onchange="changeAge(this)" name="age"  id="age" autocomplete="off" value="${commissionOrder.age==0 ? '' : commissionOrder.age }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡原因：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" onchange="deathCauseChange()" name="deadReasonId" id="deadReasonId">
										${deadReasonOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者性别：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="sex" id="sex">
										${sexOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡日期：</label> 
									<input  data-id="reservation" readOnly='readOnly' data-min-view="2" data-date-format="yyyy-mm-dd" class="required list_select" id="dTime" name="dTime" value='<fmt:formatDate value="${commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/>'><i style="margin-left: -20px;" class="fa fa-calendar"></i>&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								
								<div class="col-md-5 height-align">
									<label class='lab-2'>民族：</label> 
									<input list="dNation" autocomplete='off' class="list_select required" name="dNation" id="nation" value="${commissionOrder.dNationId }">
									<datalist  class="list_select"  id="dNation">
												${nationOption}
									</datalist>	

									</select>
								</div>
								<div class="col-md-7 cl" style='line-height:30px;'>
									<label style='float:left;'>死者地区：</label> 
										<div class=' cl' style='float:left;'>
											<!-- <span style='float:left;'>省：</span> -->
											<div  style='float:left;margin-right:10px;'>
												<select  class="list_input" name="province" id="province">
											
												</select>
											</div>
											<div  style='float:left;margin-right:10px;'>
												<select  class="list_input" name="city" id="city">
											
												</select>
											</div>
											<div  style='float:left;'>
												<select  class="required list_input" name="area" id="area" onchange="areaChange()">
											
												</select>
												&nbsp;<i class="fa fa-circle text-red"></i>
											</div>
										</div>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-2'>卡号：</label> 
									<input type="text" class=" list_select" name="cardCode" autocomplete="off" value="${commissionOrder.cardCode }">
								</div>
								<div class="col-md-7 height-align hide" id="ifNormal">
									<label class='lab-4'>证明单位：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="proveUnitId" id="proveUnitId">
										${proveUnitOption}
									</select>&nbsp;<i class="fa fa-circle text-red"></i>
									<input style="width: 200px" type="text" name="proveUnitContent" class="list_select" value="${commissionOrder.proveUnitContent }">
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡证明：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="dFlag" id="dFlag">
										${dFlagOption}
									</select>
								</div>
								<div class="col-md-12">
									<label>死者身份证图片</label> 
<!-- 									<div class='cl'> -->
									<input  type='file' class='hide' id='dFile' name="dfile">
									<div class='ID-pic' data-name='name'>
									<span>点击选择图片</span>
										<c:choose>
				    						<c:when test="${commissionOrder.id != null}">
				    							<img width=325px;  src="${commissionOrder.dIdcardPic }" alt="images">
				    						</c:when>
				    						<c:otherwise>
				    						</c:otherwise>
			    						</c:choose>
<!-- 										<object id="plugin1" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
									</div>
									
									<input type="hidden" name="filename" value='${commissionOrder.dIdcardPic }'>
									<button type='button'  style='margin-top:10px;' class='btn btn-default' data-a='upload'>确认上传</button>
<!-- 									<div class='ID-pic' onClick="capture(this,'F')"> -->
<!-- 										<span>点击上传反面</span> -->
<!-- 										<object id="plugin2" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
<!-- 									</div> -->
<!-- 									</div> -->
<%-- 									<textarea type="text" class="form-control" name="dIdcardPic" value="${commissionOrder.name }" style="width: 180px;height: 200px"></textarea> --%>
								</div>
							</div>
							<p class='p border-B'>家属/经办人信息</p>
							<div class="box-body">
								<div class="col-md-6 height-align">
									<label  class='lab-6'>家属姓名：</label> 
									<input type="text" autocomplete="off" class="required list_select" name="fName" id="fName" value="${commissionOrder.fName }">&nbsp;<i class="fa fa-circle text-red"></i>
									<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardDF('fName')">
								</div>
								
								
								<div class='hide'>
										当前设备：<select id="curDev" style="width: 90px" name="selDev"
																			onchange="changedev()"></select>
										当前分辨率：<select id="curRes" style="width: 90px" name="curRes"
																			onchange="changeres()"></select>
										颜色：<select id="curColor" style="width: 90px" name="curRes"
																			onchange="changeclr()"></select>
										拍照模式：<select id="capMode" style="width: 90px" name="curRes"
																			onchange="changemode()"></select>
									
										<input id="rotatecrop" checked type="checkbox" value="" onclick="RotateCrop(this)" />纠偏裁边
										<input id="drawrect" type="checkbox" value="" onclick="setmousemode(this)" />框选
										<br><br>    
									<input   TYPE="button"   VALUE="开始预览"   onClick="start_preview()"> 
									<input   TYPE="button"   VALUE="停止预览"   onClick="stop_preview()">
									<input   TYPE="button"   VALUE="左转90度"   onClick="rotleft()">
									<input   TYPE="button"   VALUE="右转90度"   onClick="rotright()">
									<input   TYPE="button"   VALUE="视频属性"   onClick="showprop()">
									<input   TYPE="button"   VALUE="条码识别"   onClick="readbarcode()">
									<input   TYPE="button"   VALUE="画面恢复"   onClick="resetvideo()">
									<input   TYPE="button"   VALUE="生成PDF"   onClick="makepdf()">
									<input   TYPE="button"   id=recvideo VALUE="开始录像"   onClick="startrecord()">
									<input   TYPE="button"   VALUE="拍照"   onClick="capture()">
									<input   TYPE="button"   VALUE="拍照为Base64"   onClick="capturebase64()"> <br><br>
									<input   TYPE="button"   id=autocap VALUE="开始智能连拍"   onClick="startautocap()">
									<input   TYPE="button"   id=tmcap VALUE="开始定时连拍"   onClick="starttmcap()">
								</div>
								
								
								<div class="col-md-5 height-align">
									<label  class='lab-6'>与死者关系：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="fAppellationId" id="fAppellationId"> 
									    ${fAppellationOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-6'>联系号码：</label> 
									<input type="text" autocomplete="off" class="required list_select" name="fPhone" id="fPhone" value="${commissionOrder.fPhone}">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-3'>住址：</label>
									<input type="text" autocomplete="off" class="list_select" name="fAddr" value="${commissionOrder.fAddr }">
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-2'>单位：</label> 
									<input type="text" autocomplete="off" class="list_select" name="fUnit" value="${ommissionOrder.fUnit }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>身份证号码：</label> 
									<input type="text" autocomplete="off" class="list_select" name="fCardCode" id="fCardCode" value="${commissionOrder.fCardCode }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>业务审核状态：</label>
			    					<c:choose>
			    						<c:when test="${checkFlag==Check_Yes }">
			    							<font color="green">${commissionOrder.checkFlagName}</font>
			    						</c:when>
			    						<c:otherwise>
			    							<font color="red">未审核</font>
			    						</c:otherwise>
			    					</c:choose>
								</div>
								<div class="col-md-12">
									<label>家属身份证图片</label>
<!-- 									<div class='cl'> -->
									<input class='hide' class='hide' type='file' id='fFile'>
									<div class='ID-pic' data-name='fName'>
									<span>点击选择图片</span>
										<c:choose>
			    						<c:when test="${commissionOrder.id != null}">
			    							<img width=325px;  src="${ commissionOrder.eIdcardPic }" data-status='1'  alt="images"/>
			    						</c:when>
			    						<c:otherwise>
			    						</c:otherwise>
			    						</c:choose>
<!-- 										<object id="plugin3" type="application/x-lathumbplugin" width="0" height="0"> -->
<!-- 										    <param name="onload" valurequired Loaded" /> -->
<!-- 										</object> -->
									</div>
									<input type="hidden" name="filenameF" value='${commissionOrder.eIdcardPic }'/>
									<button type='button' style='margin-top:10px;' class='btn btn-default' data-a='upload'>确认上传</button>
<!-- 									<div class='ID-pic' onClick="capture(this,'F')"> -->
<!-- 										<span>点击上传反面</span> -->
<!-- 										<object id="plugin4" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
<!-- 									</div> -->
<!-- 									</div> -->
<%-- 									<textarea type="text" class="form-control" name="eIdcardPic“ value="${commissionOrder.name }" style="width: 180px;height: 200px"></textarea> --%>
								</div>
							</div>
						</div>
					<!-- 拍照 -->			
						<div class="tab-pane" id="e">
						   <p class="p border-B">拍照存档</p>
							<div class="box-body border-B">
							
								<div class="col-md-12">
									<input class="hide" type="file" id="pFile">
									<div class="ID-pic ID-pic1" data-name="pName" style="width:329px; height:249px">
									<span>点击选择图片</span>
										<c:choose>
				    						<c:when test="${commissionOrder.id != null}">
				    							<img width=325px; height=249px; src="${commissionOrder.photoPic }" alt="images">
				    						</c:when>
			    						</c:choose>
									</div>
									<iframe width="290" style="border:0;" height="60px" scrolling="yes" src='demo.html'></iframe></br>
									<input type="hidden" name="filenameP" value="${commissionOrder.photoPic }">
									<button type="button" style="margin-top:10px;" class="btn btn-default" data-a="upload">确认上传</button>
								</div>
							</div>
						</div>
	 
						
						
					</div>
				</div>
			</div>
			<div class="box-body">
			<div  class="container-fluid">
				<div class='row padding-B'>
					<div class='col-md-6'>
						<div class='row'>
							<div class="col-md-5 height-align">
								<label>业务登记员：</label>
								<c:choose>
								    <c:when test="${commissionOrder.agentUser.name!=null}">
								    	${commissionOrder.agentUser.name}
								    </c:when>
								    <c:otherwise>
								   		${agentUser}
									</c:otherwise>
								</c:choose> 
								
							</div>
							<div class="col-md-6 height-align">
								<label>预约登记人：</label>
								<span>${commissionOrder.pbookAgentId }</span>
							</div>
							
							<div class="col-md-5 height-align">
								<label>到馆登记人：</label>
								<span>${commissionOrder.bookAgentId }</span>
							</div>
							<div class="col-md-7 height-align">
								<label class='lab-3'>编号：</label> 
								<input type="text" class="list_select" readonly="readonly"  value="${commissionOrder.code == null ? '由系统自动生成' : commissionOrder.code}">
							</div>
							<div class="col-md-6 height-align">
								<label >办理时间：</label> 
								${time }
							</div>
						</div>
					</div>
					<div class='col-md-6 height-align'>
						<div class='row'>
							<div class="col-md-12">
								<small class="pull-right btns-hometab">
									<c:if test="${checkFlag!=Check_Yse }">  <!-- 这个判断条件也不知道是啥意义，后台传过来应该是 Check_Yes?? -->
										<button type="button" onclick="checkSelect()" class="btn btn-info" style='margin-top:30px;'>保存</button>
				   					</c:if>
									<a href="${url}?method=list" target="homeTab" style='margin-top:30px;' class="btn btn-default" role="button">返回</a>
								</small>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</form>
		
	</section>
<script type="text/javascript">
// 	$('#btn_exit').click(function(){
// 		window.history.back(); 
// 	})
 	$('.ID-pic').click(function(){
		$(this).prev().click();
	})
	$('[type="file"]').change(function(){
		$(this).next().next().next().attr('disabled')&&$('[data-a="upload"]').removeAttr('disabled');
		$this=$(this);
		var file=this.files[0];
		if(file!==undefined){
			var reader= new FileReader();
			reader.readAsDataURL(file);
			$(reader).load(function(){
				$this.next().find('img').remove();
				var img=document.createElement('img');
				img.src=reader.result;
				img.width=325;
				img.height=415;
				$this.next().append(img);
			})
		}
		
	}) 
	
	
	$('#pFile').change(function(){
		$('button[data-a="upload"]').removeAttr('disabled');
	});
	
	$('[data-a="upload"]').click(function(){
		event.preventDefault();
		$this=$(this);
		//获取  input file对象
		var inputFiles = $this.siblings('[type="file"]');
		if( inputFiles.val() !== '' ){
			var id = inputFiles.attr('id');
			var fileObj = document.getElementById(id).files[0]; 
			var form = new FormData();
			form.append("file", fileObj); 
			var xhr = new XMLHttpRequest();
	        xhr.open("post", 'commissionOrder.do?method=fileupload', true);
	        xhr.onload = function () {
				if((xhr.readyState == 4) && (xhr.status == 200)){
					$this.prev().val(xhr.responseText);
					toastr["success"]("上传成功");
					$this.attr('disabled','disabled');
				}else{
					toastr["danger"]("上传失败");
				}
	        };
	        xhr.send(form);
		}else{
			toastr["warning"]("请选择图片");
		} 
	});
	/* $('[data-a="upload"]').click(function(){
		event.preventDefault();
		$this=$(this);
		if($this.prev().prev().prev().val()!==''){
			var id=$this.prev().prev().prev().attr('id');
			var fileObj = document.getElementById(id).files[0]; 
			var form = new FormData();
			form.append("file", fileObj); 
			var xhr = new XMLHttpRequest();
	        xhr.open("post", 'commissionOrder.do?method=fileupload', true);
	        xhr.onload = function () {
				if((xhr.readyState == 4) && (xhr.status == 200)){
					$this.prev().val(xhr.responseText);
					toastr["success"]("上传成功");
					$this.attr('disabled','disabled');
				}else{
					toastr["danger"]("上传失败");
				}
	        };
	        xhr.send(form);
		}else{
			toastr["warning"]("请选择图片");
		} 
	}); */
//若有基本减免，则必须勾选对象类别

		if('${id}'==""){
	 			$("#nation").val("汉族");
	 		} 
function checkSelect() {
	
	//本页面另有一个检测方法，但不完善，新加一个
	//检测灵堂挽联和告别厅挽联的备注情况
	$("#homeForm").submit();
};

</script>
<script type="text/javascript" src='js/carId.js'></script>
<script type="text/javascript" src='js/jsAddress.js'></script>
<script>
	addressInit('province', 'city', 'area', '${province==null?"浙江省":province }', '${city==null?"温州市":city }', '${area==null?"":area }');
</script>
<inuput type="file" value="" />



