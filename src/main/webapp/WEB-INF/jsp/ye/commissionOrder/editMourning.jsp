<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style type="text/css">
#dayin {
	background: #7e57c2;
}
#example2_wrapper td:hover{
	background-color:#f39c12;
	cursor:pointer;
}
/*****进入列表按钮****/
#liebiao {
	background: #29b6f6;
}

.box-body>div.col-md-3 a.btn {
	padding: 5px 20px;
	border-radius: 10px;
	margin-top: -3px;
}

.la_ul {
	list-style: none;
	float: right;
}

.la_ul li {
	width: 20px;
	height: 20px;
	border-radius: 6px;
	margin: 0 0 0 20px;
	float: left;
}

.la_ul li>div {
	width: 30px;
	font-size: 5px;
	margin-top: 23px;
	margin-left: -1px;
	position: absolute;
}
</style>
<script>
		(function(){
			$('#example2_wrapper table tr td[data-num]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-num');
				switch (data){
					case '${IsFlag_Yse }':
						$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '${IsFlag_Bzwc }':
						$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
						break;
					case '${IsFlag_Lock }':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
					case '${IsFlag_YY}':
						$(this).css({'backgroundColor':'#673ab7','color':'#fff'});
						break;
					case '${IsFlag_Decrate }':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
					case '${IsFlag_QC }':
						$(this).css({'backgroundColor':'#336868','color':'#fff'});
						break;
					case '11':
						$(this).css({'backgroundColor':'#aaa','color':'#fff'});
						break;
				}
			})
		})();
</script>
<script type="text/javascript">
//解除锁定
$("#unLock").click(function(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'html',
	    cache: false,
	    data:{method:'mourningUnLock'},
	    success: function (json) {
			alert("解锁成功！");
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
});
//重置
$("#reset").click(function(){
	$("#mourningId").val("");
	$("#mourningBeginTime").val("");
	$("#mourningName").val("");
	$("#mourningEndTime").val("");
	$("#mourning_num").val("");
	var types="no";
	//这里添加 删除对应的 灵堂 服务项目记录 501,502,503,402
	for(var i=1;i<=sum;i++){
		var text=$("#helpCode_"+i).find("option:selected").text();
		if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="501" ||text==="502"||text==="503")){
			$("#checkbox_"+i).attr('checked','checked');
			types="yes";
		}
	}
	for(var i=sum;i>=1;i--){//以下判断为了区别与冷藏的水晶棺一样里,只从后面开始删一条记录(可能有问题)
		var text=$("#helpCode_"+i).find("option:selected").text();
		if(($("#helpCode_"+i).attr('name')==='helpCode') && text==="402"){
			$("#checkbox_"+i).attr('checked','checked');
			types="yes";
			break;
		}
	}
	if(types==="yes"){
		delHtml("service");
	}
	mourningBefore="";
	mourningBefore2="";
	//有冰柜项目，以预约火化时间为准
	if($("#freezerName").val()!=""){
		iceHave="yes";
	}
	if(iceHave==="yes"){
		if($("#farewellBeginDate").val()!==""){//有告别时间
			var beforeTime=ndate($('#arriveTime').val());
		 	var nextTime=ndate($('#farewellBeginDate').val());
		 	var longTime=(nextTime-beforeTime)/(24*60*60*1000);
		 	if(longTime<=0){
				longTime=1;
			}
			for(var i=1;i<=sum;i++){
				var text=$("#helpCode_"+i).find("option:selected").text();
				if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="405"||text==="403"||text==="402")){//先判断排除水晶棺之后冷藏记录
					ice="yes";
					$("#number_"+i).val(longTime)
					numbers(i,"service");
					break;
				}
			}
		}else if($("#orderTime").val()!==""){//有预约火化时间
			var beforeTime=ndate($('#arriveTime').val());
		 	var nextTime=ndate($('#orderTime').val());
		 	var longTime=(nextTime-beforeTime)/(24*60*60*1000);
		 	if(longTime<=0){
				longTime=1;
			}
			for(var i=1;i<=sum;i++){
				var text=$("#helpCode_"+i).find("option:selected").text();
				if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="405"||text==="403"||text==="402")){//先判断排除水晶棺之后冷藏记录
					ice="yes";
					$("#number_"+i).val(longTime)
					numbers(i,"service");
					break;
				}
			}
		}else{//啥都没有
			var longTime=1;
			for(var i=1;i<=sum;i++){
				var text=$("#helpCode_"+i).find("option:selected").text();
				if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="405"||text==="403"||text==="402")){
					ice="yes";
					$("#number_"+i).val(longTime)
					numbers(i,"service");
					break;
				}
			}
		}
	}
});

//当前时间改变
function changeDate(){
	var date=$("#current_time").val();
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'html',
	    cache: false,
	    data:{method:'editMourning',current_time:date},
	    success: function (json) {
	    	$("#myModal").html(json);
			$("#myModal").modal("show");
			initTab();
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
}

//单元格单击
function td_click(i,a,ele){
	var tDate = $("#tDate").text();
	$this=$(ele);
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'selectedTime',col:a,row:i,tDate:tDate},
	    success: function (json) {
	    	var start= $('input:radio[id="start_radio"]:checked').val();
	    	var end= $('input:radio[id="end_radio"]:checked').val();
	    	if(end!=null){
	    		$("#end_time").val(json.time);
		    	saveMourning(json.mourningList,json.mourningList2);
		    	sortItem($('#service'));
	    	}
	    	if(start!=null){
	    		$("#start_time").val(json.time);
	    		if($('#arriveTime').val()!==''){
		    		var checkTime=ndate($('#arriveTime').val());
		    		var now=ndate($('#start_time').val());
		    		
		    		if(now-checkTime<0){
		    			toastr["error"]("开始时间必须大于等于到馆时间！");
		    			$('#start_time').val('');
		    			return false;
		    		}
	    		}
	    		if($('#arriveTime').val()!==''){
	    			var checkTime=ndate($('#farewellBeginDate').val());
	        		var now=ndate($('#start_time').val());
	        		if(now-checkTime>0){
	        			toastr["error"]("结束时间必须小于等于告别时间！");
	        			$('#end_time').val('');
	        			return false;
	        		}
	    		}
	    		$this.css('background-color','#f39c12');
	    			
 	    		$("#end_radio").attr("checked","checked");
	    		
	    	}
	    	$("#mourning_num").val(json.name);
	    	$("#mourning_id").val(json.id);
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	})
}
//验证时间
$('#start_time').change(function(){
	var checkTime=ndate($('#arriveTime').val());
	var now=ndate($('#start_time').val());
	if(now-checkTime<0){
		toastr["error"]("开始时间必须大于到馆时间！");
		$('#start_time').val('');
	}
})
//保存灵堂调度
function saveMourning(mourningList,mourningList2){
	var start=$("#start_time").val();
	var end=$("#end_time").val();
	if(start>end){
		toastr["error"]("开始日期不能大于结束日期！");
	}else{
		if($('#arriveTime').val()!==''){
			var checkTime=ndate($('#farewellBeginDate').val());
    		var now=ndate($('#end_time').val());
    		if(now-checkTime>0){
    			toastr["error"]("结束时间必须小于等于告别时间！");
    			$('#end_time').val('');
    			return false;
    		}else{
    			var endTime=ndate($('#end_time').val());
		    	var startTime=ndate($('#start_time').val());
		    	var arrTime=ndate($('#arriveTime').val());
		    	var num=(endTime-startTime)/(24*60*60*1000);
		    	var num2=(startTime-arrTime)/(24*60*60*1000);
		    	if(num<=0){
		    		num=1;
		    	}
		    	if(num2<=0){
		    		num2=1;
		    	}
		    	
		    	
		    	if($("#freezerName").val()!==""){//有冰柜的情况下
		    		var types="no";//记录是否应该触发删除功能
		    		var isPT="not";//记录是否是普通的冷藏服务（除水晶棺冷藏之外）
		    		for(var i=0;i<=sum;i++){
		    			var text=$("#helpCode_"+i).find("option:selected").text();
		    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="403" ||text==="405")){
		    				$("#number_"+i).val(num2);
		    				numbers(i,"service");
		    				isPT="yes";
		    				break;
		    			}
		    		}
		    		for(var i=0;i<=sum;i++){//将水晶棺与灵堂项目删除
		    			var text=$("#helpCode_"+i).find("option:selected").text();
		    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="402"||text==="503"||text==="501" ||text==="502")){
		    				$("#checkbox_"+i).attr('checked','checked');
		    				types="yes";
		    			}
		    		}
		    		if(types==="yes"){
		    			delHtml("service");
		    		}
		    		if(isPT==="not"){//若冰柜为水晶棺冷藏，则重新添加
		    			sum++;
						item(sum,mourningList2[0],"",mourningList2[1],mourningList2[2],mourningList2[3],mourningList2[4],num2,mourningList2[6],num2*mourningList2[3],"",mourningList2[9],mourningList2[10])
						sTotal+=parseFloat(num2*mourningList2[3]);
						$("#sFont").html(sTotal+"元");
		    		}
		    		//重新添加 灵堂租用项目
		    		sum++;
					mourningBefore='checkbox_'+sum;
					item(sum,mourningList[0],"",mourningList[1],mourningList[2],mourningList[3],mourningList[4],num,mourningList[6],num*mourningList[3],"",mourningList[9],mourningList[10])
					sTotal+=parseFloat(num*mourningList[3]);
					$("#sFont").html(sTotal+"元");
					//重新添加 灵堂租用中的水晶棺冷藏
					sum++;
					mourningBefore2='checkbox_'+sum;
					item(sum,mourningList2[0],"",mourningList2[1],mourningList2[2],mourningList2[3],mourningList2[4],num,mourningList2[6],num*mourningList2[3],"",mourningList2[9],mourningList2[10])
					sTotal+=parseFloat(num*mourningList2[3]);
					$("#sFont").html(sTotal+"元");
		    	}else{//没有冰柜的情况下，只考虑 灵堂租用和水晶棺冷藏
		    		var types="no";//记录是否应该触发删除功能
		    		for(var i=0;i<=sum;i++){//将水晶棺与灵堂项目删除
		    			var text=$("#helpCode_"+i).find("option:selected").text();
		    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="402"||text==="503"||text==="501" ||text==="502")){
		    				$("#checkbox_"+i).attr('checked','checked');
		    				types="yes";
		    			}
		    		}
		    		if(types==="yes"){
		    			delHtml("service");
		    		}
		    		//重新添加 灵堂租用项目
		    		sum++;
					mourningBefore='checkbox_'+sum;
					item(sum,mourningList[0],"",mourningList[1],mourningList[2],mourningList[3],mourningList[4],num,mourningList[6],num*mourningList[3],"",mourningList[9],mourningList[10])
					sTotal+=parseFloat(num*mourningList[3]);
					$("#sFont").html(sTotal+"元");
					//重新添加 灵堂租用中的水晶棺冷藏
					sum++;
					mourningBefore2='checkbox_'+sum;
					item(sum,mourningList2[0],"",mourningList2[1],mourningList2[2],mourningList2[3],mourningList2[4],num,mourningList2[6],num*mourningList2[3],"",mourningList2[9],mourningList2[10])
					sTotal+=parseFloat(num*mourningList2[3]);
					$("#sFont").html(sTotal+"元");
		    	}
    		}
    		
		}
		var id=$("#mourning_id").val();
		$("#mourningId").val(id);
		var name=$("#mourning_num").val();
		$("#mourningName").val(name);
		var startTime=$("#start_time").val();
		$("#mourningBeginTime").val(startTime);
		var endTime=$("#end_time").val();
		$("#mourningEndTime").val(endTime);
		$("#myModal").modal("hide");
		
		$.ajax({
		    url: "commissionOrder.do",
		    dataType:'json',
		    cache: false,
		    data:{method:'mourningLock',startTime:startTime,endTime:endTime,mourningId:id},
		    success: function (json) {
			},
		   error: function (e) {
		      alert("错误"+e);
		   }
		})
		
		
	}
}
timeMunite();
</script>
	<div class="modal-dialog" role="document" style="width:1200px;">
		<form action="${url}" id="form1" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				 <div style="text-align: center;">
					<h4 class="modal-title" id="myModalLabel">灵堂使用情况</h4>
				 </div>
			</div>
			<div class="box-body">
				<div class="col-md-12" style="text-align: center;">
					<a href="${url}?method=editMourning&time=-1&current_time=${date}"  target="dialog" rel="myModal" id="front">前一天</a> 
					<label id="tDate">${date }</label> 
					<a href="${url}?method=editMourning&time=1&current_time=${date}"  target="dialog" rel="myModal" id="back">后一天</a> 
					<input type="text" data-provide="datetimepicker" id="current_time" name="current_time" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  value="${date }">
					<i style="margin-left: -20px;" class="fa fa-calendar"></i>
				</div>
				
				<div class="col-md-12" style='text-align:center'>
					<input id="mourning_id"  type="hidden" value=""> 
					<label>灵堂号</label> <input id="mourning_num" readonly="readonly" type="text" value=""> 
					<label>开始</label>
					<input id="start_radio" name="radio" type="radio" checked="checked"  value="" /> 
					<input data-id="reservation"  readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii" id="start_time" class="required list_select"> 
					<label>结束</label> 
					<input id="end_radio" name="radio" type="radio"  value="" />
					<input data-id="reservation" readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii" id="end_time" class="required list_select"> 
					
				</div>
				<div class="col-md-12">
					<ul class="la_ul">
						<li style="background: #336868;"><div>清场</div></li>
						<li style="background: #9ccc65;"><div>布置完成</div></li>
						<li style="background: #fea625;"><div>锁定</div></li>
						<li style="background: #673ab7;"><div>预定</div></li>
						<li style="background: #29b6f6;"><div>占用</div></lFronti>
						<li style="background: #e84e40;"><div>装修</div></li>
					</ul>
				</div>
			</div>
			<div class="box-body nomargin-B" style='padding:20px;'>
					<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover1">
									<c:forEach items="${list }" var="u" varStatus="i">
										<c:choose>
											<c:when test="${i.index==0 }">
												<thead>
													<tr role="row">
														<th>灵堂</th>
														<c:forEach begin="1" end="7" var="a">
															<th colspan="2"><fmt:formatDate value="${u[a]}" pattern="MM.dd"/></th>
														</c:forEach>
													</tr>
												</thead>
											</c:when>
											<c:otherwise>
												<tr role="row">
													<td><font color="blue">${u[0].name }</font></td>
													<c:forEach begin="1" end="14" var="a">
														<c:if test="${u[a]==null }">
															<td width="6%" onclick="td_click( ${i.index},${a },this)" id="td_${i.index}_${a }"  style=""></td>
														</c:if>
														<c:if test="${u[a]!=null }">
															<c:choose>
																	<c:when test="${u[a].flag == IsFlag_Decrate }">
																	<td width="6%" data-num='${u[a].flag}' >
																		维修
																	</td>
																</c:when>
																<c:when test="${u[a].flag == 11 }">
																	<td  data-num='${u[a].flag}' title='此处由${u[a].user.name}临时锁定,<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																		临时锁定
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Lock }">
																	<td width="6%" data-num='${u[a].flag}' title="${u[a].order.name},<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		${u[a].order.name}
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_YY}">
																	<td width="6%" data-num='${u[a].flag}' title="${u[a].appointmentName},<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		${u[a].appointmentName}
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Yse  || u[a].flag == IsFlag_Bzwc || u[a].flag == IsFlag_QC }">
																	<td width="6%" data-num='${u[a].flag}' title="${u[a].order.name},<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		${u[a].order.name}
																	</td>
																</c:when>
																<c:otherwise>
																	<td width="6%" ></td>
																</c:otherwise>
															</c:choose>
														</c:if>
													</c:forEach>
												</tr>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
					<div class='btns-dialog'>
						<input type="button" class='btn btn-color-9E8273 btn-margin' data-dismiss="modal" id="reset" value="重置" />
						<input type="button" class="btn btn-margin btn-info" data-dismiss="modal" id="unLock" value="解除锁定" />
					</div>
				</div>
			</div>
		</form>
	</div>