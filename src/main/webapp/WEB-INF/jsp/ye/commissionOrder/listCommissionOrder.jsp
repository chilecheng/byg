<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化委托单</div>
</section>
<style>
	.table td a.light{
		width:50px;
		height:20px;
		/* background-color:lightblue; */
		text-align:center;
		line-height:20px;
		color:#fff;
		border-radius:4px;
	}
	.table td a.light.left{
	background-color:#00c0ef;
	}
	.table td a.light.middle{
	background-color:#ff7043;
	}
	.table td a.light.right{
		background-color:#ab47bc;
	}
</style>
<section class="content" id='contentID'>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-2">
							<label>类型：</label>
							<select class="list_select nopadding-R" name="searchType" id="searchType" style="width: 52%;">
								${searchOption }
							</select>
						</div>
						<div class="col-md-5" >
							<label>查询值：</label> 
							<input type="text" class="list_input" name="searchVal" id="searchVal" value="${searchVal}" placeholder="查询值">
						</div>
						<div class="col-md-2" >
							<input type="checkbox" name="isLock" value="unlock">取消锁定数据
						</div>
						<div class="col-md-2" >
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-print btns-buy'>
						<a href="${url}?method=edit" target="homeTab" rel="myModal" class="btn btn-warning" role="button">添加</a>
						<a href="${url}?method=delete&ids=" target="ajaxTodo" data-delete='true' checkName="checkboxId" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
						<a href="${url}?method=pincard&ids=" target="ajaxTodo" checkName="checkboxId" rel="myModal" warm="确认销卡吗" class="btn btn-color-ab47bc"  role="button">销卡</a>
					</small>
					<small class="btns-buy pull-right">
						<a href="${url }?method=readOnly&checkFlag=${Check_Yes }&adr=A&ifCheck=checkYes&id=" target="homeTab" checkName="checkboxId" checkone="true"  data-checkpay='true' data-checkFlag=1 class="btn btn-info" role="button">审核</a>
						<a href="${url }?method=readOnly&checkFlag=${Check_No }&adr=A&ifCheck=checkNo&id=" target="homeTab" checkName="checkboxId"  checkone="true" data-checkpay='true' data-checkFlag=2 class="btn btn-danger " role="button">取消审核</a>
					</small>
					
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>
											<th>死者姓名</th>
											<th>年龄</th>
											<th>死者性别</th>
											<th>冰柜号</th>
											<th>火化时间</th>
											<th>死亡证明</th>
											<th>是否审核</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="20">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td><input type="checkbox" name="checkboxId" value="${u.id}"></td>
							    					<td><a href="${url }?method=readOnly&id=${u.id}&adr=A" target='homeTab'>${u.name }</a></td>
							    					<td>
							    					${u.age }
							    					</td>
							    					<td>
							    					${u.sexName }
							    					</td>
							    					<td>
							    					${u.freezer.name }
							    					</td>
							    					<td>
							    					<fmt:formatDate value="${u.cremationTime }" pattern="yyyy-MM-dd HH:mm"/>
							    					</td>
							    					<td>
								    					<c:if test="${u.dFlag==IsHave_No }">
								    						<font color="red" class='state1'>${u.flagName }</font>
								    					</c:if>
								    					<c:if test="${u.dFlag==IsHave_Yes }">
								    						<font color="green" class='state1'>${u.flagName }</font>
								    					</c:if>
							    					</td>
							    					<td>
								    					<c:if test="${u.checkFlag==Check_No }">
								    						<font color="red" class='state1'>${u.checkFlagName }</font>
								    					</c:if>
								    					<c:if test="${u.checkFlag==Check_Yes }">
								    						<font color="green" class='state1'>${u.checkFlagName }</font>
								    					</c:if>
							    					</td>
							    					<c:if test="${u.payNumber >'0'}">
							    						<td>
									    					<a href="${url }?method=readOnly&checkFlag=${Check_Yes }&adr=A&ifCheck=checkYes&id=${u.id}" class='light left' target="homeTab" data-checkpay='true left' data-checkFlag=1>审核</a>
									    					<a href="${url }?method=readOnly&adr=A&styleType=print&id=${u.id}" class='light right' target="homeTab">打印</a>
									    					<a href="javasrcipt:;" class='light middle' style='background:#aaa; cursor:text'>修改</a>
									    					<a href="${url}?method=businessView&id=${u.id}" target="dialog" rel="myModal" style='background:green;'  class="light middle">视图</a>
							    						</td>
							    					</c:if>
							    					<c:if test="${u.payNumber <='0' }">
							    						<td>
									    					<a href="${url }?method=readOnly&checkFlag=${Check_Yes }&adr=A&ifCheck=checkYes&id=${u.id}" class='light left' target="homeTab" data-checkpay='true left' data-checkFlag=1>审核</a>
									    					<a href="${url }?method=readOnly&adr=A&styleType=print&id=${u.id}" class='light right' target="homeTab">打印</a>
									    					<a href="${url }?method=edit&id=${u.id}" class='light middle' target="homeTab">修改</a>
									    					<a href="${url}?method=businessView&id=${u.id}" target="dialog" rel="myModal" style='background:green;'  class="light middle">视图</a>
							    						</td>
							    					</c:if>
							    					
							    					<td class='tdID hidden'>
							    						<font>${u.payFlag }</font>
							    					</td>
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$('#searchVal').change(function(){
	if($("#searchType").find("option:selected").text()=="卡号"){
		homeSearch(pageForm);		
	}
})
</script>