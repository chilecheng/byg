<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style>
	.ID-pic.ID-pic1 img {
/*     position: absolute; */
    height: auto !important;
    }
</style>
<script type="text/javascript">
var preConfigList = new Array();
var previewJson = new Array();
var sum=0;//标识
var hardSum=0;//困难减免标识
var sTotal=0;//服务项目合计总金额	
var aTotal=0;//丧葬用品合计总金额
var baseTotal=0;//基本减免合计总金额
var hardTotal=0;//困难减免合计总金额
var iceType="";//用于记录冷藏冰柜判断依据

var furnaceOrderTime="";
var fareOrderTime="";

var iceHave="no";
var state='${id}';
var before='';
var mourningBefore='';
var farewellBefore='';
var funBefore='';
var mourningBefore2='';
$(function(){
	timeRange();
	timeMunite();
	deadTypeChange();
// 	areaChange();
	if("${specialBusinessOption}"=="" || "${specialBusinessOption}"==null){
		$("#specialBusinessType").hide();
	}
	//火化炉
	$("#furnaceType_div").hide();
	//是否基本或困难减免
	$("#baseLi").hide();
	$("#hardLi").hide();
	if(${base_Is==Is_Yes}){
		$("#baseLi").show();
	}else if(${base_Is==Is_No}){
		$("#baseLi").hide();
	}
	if(${hard_Is==Is_Yes}){
		$("#hardLi").show();
	}
	//添加或修改
	if('${id}'==""){
// 		$("#baseIs").val(1);
		is_change('baseIs');
		
		//初始化服务项目
		<c:forEach var="u" items='${serviceList}'>
			sum++;
			item(sum,"${u[0]}","","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","","${u[9]}","${u[10]}")
			sTotal+=parseFloat("${u[7]}")
			$("#sFont").html(sTotal+"元");
		</c:forEach>
		//初始丧葬用品项目
		<c:forEach var="u" items='${articlesList}'>
			sum++;
			item(sum,"${u[0]}","","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","","${u[9]}","${u[10]}")
			aTotal+=parseFloat("${u[7]}")
			$("#aFont").html(aTotal+"元");
		</c:forEach>
		//初始免费对象类别
		var fStr="<label>免费对象类别：</label>";
		<c:forEach var="u" items='${freePersonList}'>
			<c:choose>
				<c:when test="${u.name=='市区居民'}">
					fStr+="<input type='checkbox'  name='freePersonId' checked='checked' value='${u.id }'>";
					fStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
					$("#freePerson").html(fStr);
				</c:when>
				<c:otherwise>
					fStr+="<input type='checkbox'  name='freePersonId' value='${u.id }'>";
					fStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
					$("#freePerson").html(fStr);
				</c:otherwise>
			</c:choose>
		</c:forEach>
		//初始重点救助对象类别
		var hStr="<label>重点救助对象类别：</label>";
		<c:forEach var="u" items='${hardPersonList}'>
			hStr+="<input type='checkbox'  name='hardPersonId' value='${u.id }'>";
			hStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#hardPerson").html(hStr);
		</c:forEach>
		//初始证件类型
		var pStr="<label class='lab-6'>证件类型：</label>";
		<c:forEach var="u" items='${certificateList}'>
			pStr+="<input type='checkbox'  name='proveIds' value='${u.certificateId }'>";
			pStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#prove").html(pStr);
// 			pStr+="<input type='checkbox'  name='proveIds' value='${u.certificateId }'>";
// 			pStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
// 			$("#prove").html(pStr);
		</c:forEach>
		
		//初始化火化炉按钮
		var fun="<label class=lab-6>火化炉类型：</label> "
		<c:forEach var="u" items='${furnaceList}'>
			<c:choose>
				<c:when test="${u[0]==1}">//默认选择 普通炉
					fun+="<input type=radio name=radio onclick=radio_click('${u[0]}') checked value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
					$("#specialBusinessType").hide();
				</c:when>
				<c:otherwise>
					fun+="<input type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
				</c:otherwise>
			</c:choose>
		</c:forEach>
		
		$("#furnace_div").html(fun);		
		if('${funeralCode}'!=null && '${funeralCode}'!=''){
			$('[name="radio"][value="2"]').attr('checked','checked');
			radio_click('${furnace_ty}');
			$('#furnaceName').val('${funeralCode}');
		}
		
		if('${funeralCode2}'!=null && '${funeralCode2}'!=''){
			$('[name="radio"][value="2"]').attr('checked','checked');
			radio_click('${furnace_ty}');
			$('#furnaceName').val('${funeralCode2}');
		}
		

		//初始化基本减免项目
		<c:forEach var="u" items='${baselist}'>
			sum++;
			var pice=parseFloat('${u[5]}');
			
			reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]/u[4]}","${u[6]}","${u[7]}","${u[8]}")
			baseTotal+=pice;
			$("#baseFont").html(baseTotal+"元");
		</c:forEach>
		//初始化困难减免项目
		<c:forEach var="u" items='${hardlist}'>
			hardSum++;
			var pice=parseFloat('${u[5]}');
			reductionHard(hardSum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]/u[4]}","${u[6]}","${u[7]}","${u[8]}")
			hardTotal+=pice;
			$("#hardFont").html(hardTotal+"元");
		</c:forEach>	
	}else{
	
		//车辆调度
		<c:forEach var="u" items='${car_List}'>
			sum++;
			car(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}");
		</c:forEach>
		//服务项目
		<c:forEach var="u" items='${service_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			
			sTotal+=parseFloat('${u[8]}');
			$("#sFont").html(sTotal+"元");
			
		</c:forEach>
		//葬用品项目
		<c:forEach var="u" items='${articles_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			aTotal+=parseFloat('${u[8]}');
			$("#aFont").html(aTotal+"元");
		</c:forEach>
	//基本减免
		//免费对象类别
		var fStr="<label>免费对象类别：</label>";
		if(${isFree==false}){
			<c:forEach var="u" items='${freePerson_list}'>
				<c:choose>
					<c:when test="${u[3]=='市区居民'}">
						fStr+="<input type='checkbox'  name='${u[0]}' checked='checked' value='${u[2] }'>";
						fStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
						$("#freePerson").html(fStr);
					</c:when>
					<c:otherwise>
						fStr+="<input type='checkbox'  name='${u[0]}' value='${u[2] }'>";
						fStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
						$("#freePerson").html(fStr);
					</c:otherwise>
				</c:choose>
			</c:forEach>
		}else{
			<c:forEach var="u" items='${freePerson_list}'>
			fStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			fStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#freePerson").html(fStr);
		</c:forEach>
		}
		//重点救助对象类别
		var hStr="<label>重点救助对象类别：</label>";
		<c:forEach var="u" items='${hardPerson_list}'>
			hStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			hStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#hardPerson").html(hStr);
		</c:forEach>
		//基本减免项目
		if(${base_Is==Is_Yes}){
			<c:forEach var="u" items='${base_list}'>
				sum++;
				var pice=parseFloat('${u[5]}');
				reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]/u[4]}","${u[6]}","${u[7]}","${u[8]}")
				baseTotal+=pice;
				$("#baseFont").html(baseTotal+"元");
			</c:forEach>
		} else {
			<c:forEach var="u" items='${baselist}'>
				sum++;
				var pice=parseFloat('${u[5]}');
				reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]/u[4]}","${u[6]}","${u[7]}","${u[8]}")
				baseTotal+=pice;
				$("#baseFont").html(baseTotal+"元");
			</c:forEach>
		}
	//困难减免
		//证件类型
		var pStr="<label class='lab-6'>证件类型：</label>";
		<c:forEach var="u" items='${prove_list}'>
			pStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			pStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#prove").html(pStr);
		</c:forEach>
		//困难减免项目
		if(${hard_Is==Is_Yes}){
			<c:forEach var="u" items='${hard_list}'>
				hardSum++;
				var pice=parseFloat('${u[5]}');
				reductionHard(hardSum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]/u[4]}","${u[6]}","${u[7]}","${u[8]}")
				hardTotal+=pice;
				$("#hardFont").html(hardTotal+"元");
			</c:forEach>
		} else {
			<c:forEach var="u" items='${hardlist}'>
			hardSum++;
			var pice=parseFloat('${u[5]}');
			reductionHard(hardSum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]/u[4]}","${u[6]}","${u[7]}","${u[8]}")
			hardTotal+=pice;
			$("#hardFont").html(hardTotal+"元");
		</c:forEach>	
		}
		//先控制除了水晶棺的,然后再判断水晶棺与灵堂的天数不同
		if($('#freezerName').val()!=null&&$('#freezerName').val()!=""){
			var st=ndate($('#arriveTime').val());
			if($('#mourningBeginTime').val()!=null && $('#mourningBeginTime').val()!=""){
				var et=ndate($('#mourningBeginTime').val());
				var endt=ndate($('#mourningEndTime').val());
				var longTime=(et-st)/(24*60*60*1000);
				var iNum=(endt-et)/(24*60*60*1000);
				if(longTime<=0){
					longTime=1;
				}
				if(iNum<=0){
					iNum=1;
				}
				var ice="no";
				for(var i=1;i<=sum;i++){
					var text=$("#helpCode_"+i).find("option:selected").text();
					if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="405"||text==="403")){//先判断排除水晶棺之后冷藏记录
						ice="yes";
						$("#number_"+i).val(longTime)
						numbers(i,"service");
						break;
					}
				}
				if(ice==="no"){//若以上没有冷藏记录，则在水晶棺记录中删除    !!!条件不是很充足!!!
					var o=1000;
					var p=1000;
					for(var i=1;i<=sum;i++){
		    			var text=$("#helpCode_"+i).find("option:selected").text();
		    			if(($("#helpCode_"+i).attr('name')==='helpCode') && (text==="402")){
		    				if($('#number_'+i).val() != iNum){
		    					o=i;
		    				}else{
		    					p=i;
		    				}
		    			}
		    		}
					if(o==1000){
						o=p;
					}
					$("#number_"+o).val(longTime)
    				numbers(o,"service");
				}
				
			}else if($('#farewellBeginDate').val()!=null && $('#farewellBeginDate').val()!=""){//没有灵堂以告别厅时间为准
				var et=ndate($('#farewellBeginDate').val());
				var longTime=(et-st)/(24*60*60*1000);
				if(longTime<=0){
					longTime=1;
				}
				for(var i=1;i<=sum;i++){
					var text=$("#helpCode_"+i).find("option:selected").text();
					if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="405"||text==="403"||text==="402")){
						$("#number_"+i).val(longTime)
						numbers(i,"service");
						break;
					}
				}
			}
		}
		
		//火化炉按钮
		var fun="<label class=lab-6>火化炉类型：</label> "
		var flagType="x";
		<c:forEach var="u" items='${furnace_list}'>
			fun+="<input ${u[2]} type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
			if("${u[2]}"!=""){
				flagType="y";
			}
			if("${u[2]}"!=""&& ${u[0]==furnace_ty}){
				$("#furnaceType_div").show();
				$("#specialBusinessType").hide();
			}else if("${u[2]}"!=""&& ${u[0]=="1"}){
				$("#specialBusinessType").hide();
			}
		</c:forEach>
		$("#furnace_div").html(fun);
		if(flagType=="x"){
			$('[name="radio"][value="1"]').attr('checked','checked');
			$("#specialBusinessType").hide();
		}
		if('${funeralCode2}'!=null && '${funeralCode2}'!=''){
			$('[name="radio"][value="2"]').attr('checked','checked');
			radio_click('${furnace_ty}');
			$('#furnaceName').val('${funeralCode2}');
		}
		
	}
	sortItem($('#articles'));
	sortItem($('#service'));
	sortItem($('#hard'));
	sortItem($('#base'));
});
//收费项目方法
function item(sum,name,id,itemId,helpCode,pice,typeId,number,bill,total,comment,findexFlag,indexFlag){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name="+name+"_id value="+id+">";
	str+="<input type='checkbox' data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"' id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select readOnly='readOnly' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name='helpCode' id='helpCode_"+sum+"'> "+helpCode+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemName("+sum+",'"+name+"') name='itemId' id='itemId_"+sum+"'> "+itemId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' onchange=priceChange("+sum+",'"+name+"') class='list_table' name=pice id='pice_"+sum+"' value="+parseFloat(pice)+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemType("+sum+",'"+name+"') name='typeId' id='typeId_"+sum+"'>"+typeId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=number id='number_"+sum+"' value="+number+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name=bill id=bill_"+sum+"> "+bill+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name=total id=total_"+sum+" class='list_table' value="+parseFloat(total)+"></td>";
	if(helpCode==="538"||helpCode==="535"){
		str+="<td><textarea type='text' class='list_table required' name=comment id='comment_"+sum+"' >"+comment+"</textarea></td></tr>";
	}else{
		str+="<td><textarea type='text' class='list_table' name=comment id='comment_"+sum+"' >"+comment+"</textarea></td></tr>";
	}
	$("#"+name).append(str);
	var nn=parseFloat($("#"+name+"Number").text());	
	$("#"+name+"Number").html(nn+1);
}

//减免项目方法(原方法,在更改中)
function reduction(sum,name,id,item,helpCode,number,pice,comment,findexFlag,indexFlag){
	var str="<tr><td><input type='hidden' name="+name+"_id value="+id+"><input type='checkbox'  data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"'  id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td><select  readOnly='readOnly' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name=reduction_helpCode id='helpCode_"+sum+"'>"+helpCode+"</select></td>";
	str+="<td><select  class='list_table' onchange=itemName("+sum+",'"+name+"') name=reduction_itemId id='itemId_"+sum+"'>"+item+"</select></td>";
	str+="<td><input type='text' onchange=priceChange("+sum+",'"+name+"') class='list_table' name=reduction_pice id=pice_"+sum+"  value="+parseFloat(pice)+"></td>";
	str+="<td><input type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=reduction_number id=number_"+sum+"  value="+number+"></td>";
	str+="<td><input type='text' readonly='readonly' class='list_table' name=reduction_total id=total_"+sum+" value="+parseFloat(pice*number)+"></td>";
	str+="<td><textarea type='text' class='list_table' name=reduction_comment id=comment_"+sum+" value='' >"+comment+"</textarea></td></tr>";
	$("#"+name).append(str);
}
//困难减免(新加,以区别基本减免)
function reductionHard(sum,name,id,item,helpCode,number,pice,comment,findexFlag,indexFlag){
	var str="<tr><td><input type='hidden' name="+name+"_id value="+id+"><input type='checkbox'  data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"'  id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td><select  readOnly='readOnly' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name=hard_helpCode id='hard_helpCode_"+sum+"'>"+helpCode+"</select></td>";
	str+="<td><select  class='list_table' onchange=itemName("+sum+",'"+name+"') name=hard_itemId id='hard_itemId_"+sum+"'>"+item+"</select></td>";
	str+="<td><input type='text' onchange=priceChange("+sum+",'"+name+"') class='list_table' name=hard_pice id=hard_pice_"+sum+"  value="+parseFloat(pice)+"></td>";
	str+="<td><input type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=hard_number id=hard_number_"+sum+"  value="+number+"></td>";
	str+="<td><input type='text' readonly='readonly' class='list_table' name=hard_total id=hard_total_"+sum+" value="+parseFloat(pice*number)+"></td>";
	str+="<td><textarea type='text' class='list_table' name=hard_comment id=hard_comment_"+sum+" value='' >"+comment+"</textarea></td></tr>";
	$("#"+name).append(str);
}

//车辆调度方法
function car(sum,id,transportTypeOption,carTypeOption,pickTime,comment){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name='csr_id' value="+id+" >";
	str+="<input type='checkbox' name='carSchedulRecordId' ></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select class='list_table' name=transportTypeId  id=transportTypeId_"+sum+"> "+transportTypeOption+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name=carTypeId id=typeId_"+sum+" >"+carTypeOption+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input data-id='reservation' readOnly='readOnly'  style='width: 180px' data-date-format='yyyy-mm-dd hh:ii' class='required list_table' id=dTime_"+sum+" name='pickTime' ><i style='margin-left: -20px;' class='fa fa-calendar'></i></td>";
	str+="<td><textarea type='text' class='list_table' name=carComment id=comment_"+sum+"  >"+comment+"</textarea></td></tr>";
	$("#car").append(str);
	$("#dTime_"+sum).val(pickTime);
	timeMunite();
}
//根据年龄来判断火化收费
function changeAge(age){
	var ageNumber=$(age).val();
	//儿童遗体火化费
	if(ageNumber<14){
		for(var i=1;i<=sum;i++){
			var text=$("#helpCode_"+i).find("option:selected").text();
			if(($("#checkbox_"+i).attr('name')==='service') && text==="301"){
				$("#helpCode_"+i+" option:selected").removeAttr('selected').siblings(':contains("302")').attr('selected','true');
// 				$("#helpCode_"+i+" option:selected").parent().change();
				itemHelpCode(i,"service");
			}
		}
	}else{//成人遗体火化费
		for(var i=1;i<=sum;i++){
			var text=$("#helpCode_"+i).find("option:selected").text();
			if(($("#checkbox_"+i).attr('name')==='service') && text==="302"){
				$("#helpCode_"+i+" option:selected").removeAttr('selected').siblings(':contains("301")').attr('selected','true');
// 				$("#helpCode_"+i+" option:selected").parent().change();
				itemHelpCode(i,"service");
			}
		}
// 		$('option:selected:contains("302")').removeAttr('selected').siblings(':contains("301")').attr('selected','selected');
// 		$('option:selected:contains("301")').parent().change();
	}
}
//死者地区更改(判断是否默认基本减免)
function areaChange(){
	var name=$('#area').val();
	if(name==="龙湾区" || name==="鹿城区" || name=="瓯海区"){
		$("#baseIs").val(1);
		is_change('baseIs');
	}else{
		$("#baseIs").val(2);
		is_change('baseIs');
	}
}
//死亡原因是 枪决的情况
function deathCauseChange(){
	var name=$('#deadReasonId').find("option:selected").text();
	if(name==="枪决"){
		$('#dAddr').val('不详');
		$('#pickAddr').val('不详');
		$('#certificateCode').val('不详');
		$('#age').val('不详');
		$('#fName').val('不详');
		$('#fPhone').val('不详');
		$('#fCardCode').val('不详');
	}
}

//新加  预约登记中 洽谈与 转化为火化委托单时视情况添加服务项目
if("${appointmentFarewell}"!=null && "${appointmentFarewell}"!=''){//若有告别厅
	sum++;
	farewellBefore='checkbox_'+sum;
	item(sum,"${appointmentFarewell[0]}","","${appointmentFarewell[1]}","${appointmentFarewell[2]}","${appointmentFarewell[3]}","${appointmentFarewell[4]}","${appointmentFarewell[5]}","${appointmentFarewell[6]}","${appointmentFarewell[7]}","","${appointmentFarewell[9]}","${appointmentFarewell[10]}")
	sortItem($('#service'));
	sTotal+=parseFloat("${appointmentFarewell[7]}");
	$("#sFont").html(sTotal+"元");	
}
if("${appointmentMourning}"!=null && "${appointmentMourning}"!=''){//若有灵堂
	var endTime;
	var startTime;
	var arrTime;
	if('${mourningBeginTime2}'!=null && "${mourningBeginTime2}"!=''){//转火化委托单
		endTime=ndate('${mourningEndTime2}');
		startTime=ndate('${mourningBeginTime2}');
		arrTime=ndate('${now}');		
	}else if('${mourningBeginTime}'!=null && "${mourningBeginTime}"!=''){//洽谈
		if('${mourningEndTime}' !=null && '${mourningEndTime}' !=''){
			endTime=ndate('${mourningEndTime}');
		}else{
			endTime=ndate('${mourningBeginTime}');
		}
		startTime=ndate('${mourningBeginTime}');
		arrTime=ndate('${now}');	
		
	}
	
	var num=(endTime-startTime)/(24*60*60*1000);
	var num2=(startTime-arrTime)/(24*60*60*1000);
	if(num<=0){
		num=1; 
	}
	if(num2<=0){
		num2=1;
	}
	sum++;
	mourningBefore='checkbox_'+sum;
	item(sum,"${appointmentMourning[0]}","","${appointmentMourning[1]}","${appointmentMourning[2]}","${appointmentMourning[3]}","${appointmentMourning[4]}",num,"${appointmentMourning[6]}",num*parseFloat('${appointmentMourning[3]}'),"","${appointmentMourning[9]}","${appointmentMourning[10]}")
	sTotal+=parseFloat(num*parseFloat('${appointmentMourning[3]}'));
	$("#sFont").html(sTotal+"元");
	sum++;
	mourningBefore2='checkbox_'+sum;
	item(sum,"${iceList[0]}","","${iceList[1]}","${iceList[2]}","${iceList[3]}","${iceList[4]}",num,"${iceList[6]}",num*parseFloat('${iceList[3]}'),"","${iceList[9]}","${iceList[10]}")
	sTotal+=parseFloat(num*parseFloat('${iceList[3]}'));
	$("#sFont").html(sTotal+"元");
}
//车辆登记转火化委托单生效
if("${carPick}"!=null && "${carPick}"!=''){
	sum++;
	car(sum,"","${appTransportOption}","${appCarTypeOption}","${appDepartureTime}","");
	$('#pickAddr').val('${pickAddr}');
	
}

//死亡类型改变
function deadTypeChange(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'deadTypeChange',id:$("#deadTypeId").val(),reasonId:$("#deadReasonId").val()},
	    success: function (json) {
	    	$("#deadReasonId").html(json.str);
	    	if(json.deadFlag==2){
	    		$("#ifNormal").removeClass('hide');
	    		$("#proveUnitId").addClass('required');
	    	}else{
	    		$("#ifNormal").addClass('hide');
	    		$("#proveUnitId").removeClass('required');
	    	}
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 


//项目类别改变
function itemType(id,type){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemTypeChange',id:$("#typeId_"+id).val()},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	$('#checkbox_'+id).attr('data-findexFlag',json.faIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
				sortItem($("#service"));
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
				sortItem($("#articles"));
    		}
    		$("#pice_"+id).val(json.pice);
	    	$("#total_"+id).val(json.pice);
	    	$("#number_"+id).val(1);
	    	$("#helpCode_"+id).html(json.helpOption);
	    	$("#itemId_"+id).html(json.str);
    		$("#comment_"+id).val("")
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 
//项目名称改变
function itemName(id,type){
	var itemId;
	if(type=="hard"){
		itemId=$("#hard_itemId_"+id).val();
	}else{
		itemId=$("#itemId_"+id).val()
	}
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:itemId},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	
		    	$("#pice_"+id).val(json.pice);
	    		$("#helpCode_"+id).html(json.str);
	    		
		    	sortItem($("#service"))
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	
		    	$("#pice_"+id).val(json.pice);
	    		$("#helpCode_"+id).html(json.str);
	    		
		    	sortItem($("#articles"));
    		}
    		if(type=="base"){
    			baseTotal=baseTotal-$("#total_"+id).val();
    			baseTotal+=json.pice;
    			var b=baseTotal.toFixed(2);
				$("#baseFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	
		    	$("#pice_"+id).val(json.pice);
	    		$("#helpCode_"+id).html(json.str);
	    		
		    	sortItem($("#base"));
    		}
//     		$("#pice_"+id).val(json.pice);
//     		$("#helpCode_"+id).html(json.str);
    		var txt=$("#helpCode_"+id).find('option:selected').text();
    		var text=$("#helpCode_"+id).parent().siblings(':last').find('textarea');
    		if('${id}'==""){
    			if(txt=='535'||txt=='538'){
    				if(text.hasClass('required')){return;}
    				text.addClass('required');
    			}else{
    				if(text.hasClass('required')){text.removeClass('required');}
    			}
    		}
	    	$("#number_"+id).val(1);
	    	$("#comment_"+id).val("");
	    	
    		if(type=="hard"){
    			hardTotal=hardTotal-$("#hard_total_"+id).val();
    			hardTotal+=json.pice;
    			var b=hardTotal.toFixed(2);
				$("#hardFont").html(parseFloat(b)+"元");
		    	$("#hard_total_"+id).val(json.pice);
		    	
		    	$("#hard_pice_"+id).val(json.pice);
	    		$("#hard_helpCode_"+id).html(json.str);
		    	$("#hard_number_"+id).val(1);
		    	$("#hard_comment_"+id).val("");
		    	sortItem($("#hard"));
    		}
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	})
} 


//助记码改变
function itemHelpCode(id,type){
	var helpCode;
	if(type=="hard"){
		helpCode=$("#hard_helpCode_"+id).val();
	}else{
		helpCode=$("#helpCode_"+id).val()
	}
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemHelpCodeChange',id:helpCode},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#service"))
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#articles"));
    		}
    		if(type=="base"){
    			baseTotal=baseTotal-$("#total_"+id).val();
    			baseTotal+=json.pice;
    			var b=baseTotal.toFixed(2);
				$("#baseFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#base"));
    		}
    		$("#pice_"+id).val(json.pice);
    		$("#itemId_"+id).html(json.str);
	    	$("#number_"+id).val(1);
	    	$("#comment_"+id).val("");
	    	
    		if(type=="hard"){
    			hardTotal=hardTotal-$("#hard_total_"+id).val();
    			hardTotal+=json.pice;
    			var b=hardTotal.toFixed(2);
				$("#hardFont").html(parseFloat(b)+"元");
		    	$("#hard_total_"+id).val(json.pice)
		    	$("#hard_pice_"+id).val(json.pice);
	    		$("#hard_itemId_"+id).html(json.str);
		    	$("#hard_number_"+id).val(1);
		    	$("#hard_comment_"+id).val("");
		    	sortItem($("#hard"));
    		}
    		
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 
//单价改变
function priceChange(id,type){
	var price=1;
	if(type=="hard"){
		price=$("#hard_pice_"+id).val();
	}else{
		price=$("#pice_"+id).val();
	}
	var a=parseFloat(price);
	if(a!=price){//表示不是数字
		toastr["warning"]("请输入正确的价格!");
	}else{
		var number=1;
		if(type=="hard"){
			number=$("#hard_number_"+id).val();
		}else{
			number=$("#number_"+id).val();
		}
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=price*number;
			var jg=sTotal.toFixed(2);
			$("#sFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=price*number;
			var jg=aTotal.toFixed(2);
			$("#aFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='base'){
			baseTotal=baseTotal-$("#total_"+id).val();
			baseTotal+=price*number;
			var jg=baseTotal.toFixed(2);
			$("#baseFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='hard'){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			hardTotal+=price*number;
			var jg=hardTotal.toFixed(2);
			$("#hardFont").html(parseFloat(jg)+"元");
			$("#hard_total_"+id).val((price*number).toFixed(0));
		}
	}
}
//数目改变
function numbers(id,type){
	var number=1;
	if(type=="hard"){
		number=$("#hard_number_"+id).val();
	}else{		
		number=$("#number_"+id).val();
	}
	var a=parseFloat(number);
	if(a!=number){//表示是不是数字
		alert(a+"---"+id);
		toastr["warning"]("请输入正确的数量！");
	}else{
		var pice;
		if(type=="hard"){
			pice=$("#hard_pice_"+id).val();
		}else{
			pice=$("#pice_"+id).val();
		}
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			var jg=sTotal.toFixed(2);
			$("#sFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((pice*number).toFixed(0));
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=pice*number;
			var jg=aTotal.toFixed(2);
			$("#aFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((pice*number).toFixed(0));
		}
		if(type=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			baseTotal+=pice*number;
			var jg=baseTotal.toFixed(2);
			$("#baseFont").html(parseFloat(jg)+"元");
	    	$("#total_"+id).val((pice*number).toFixed(0))
		}
		if(type=="hard"){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			hardTotal+=pice*number;
			var jg=hardTotal.toFixed(2);
			$("#hardFont").html(parseFloat(jg)+"元");
	    	$("#hard_total_"+id).val((pice*number).toFixed(0))
		}
		
	}
} 


//添加车辆信息调度
function addCar() {
	sum++;
	car(sum,"","${transportTypeOption}","${carTypeOption}","","")
}

//添加基本减免
function addBase() {
	sum++;
	var pice=parseFloat('${baseItem.pice}');
	reduction(sum,"base","","${baseItemOption}","${baseItemHelpCodeOption}",1,pice,"","${baselist[7]}","${baselist[8]}")
	baseTotal+=pice;
	$("#baseFont").html(baseTotal+"元");
}

//添加困难减免
function addHard() {
	hardSum++;
	var pice=parseFloat('${hardItem.pice}');
	reduction(hardFont,"hard","","${hardItemOption}","${hardItemHelpCodeOption}",1,pice,"")
	hardTotal+=pice;
	$("#hardFont").html(hardTotal+"元");
}

//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		if(str=="articles"){
			aTotal=aTotal-$("#total_"+id).val();
			$("#aFont").html(aTotal+"元");
		}
		if(str=="service"){
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		}
		if(str=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			$("#baseFont").html(baseTotal+"元");
		}
		if(str=="hard"){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			$("#hardFont").html(hardTotal+"元");
		}
		$(this).parent().parent().remove();
		num++;
		var nn=parseFloat($("#"+str+"Number").text());	
		$("#"+str+"Number").html(nn-1);
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}

}	

//是否申免项目
function is_change(id) {
	if(id=="baseIs"){
		if($("#baseIs").val()=='${Is_Yes}'){
			$("#baseLi").show();
		}else{
			$("#baseLi").hide();
		}
	}
	if(id=="hardIs"){
		if($("#hardIs").val()=='${Is_Yes}'){
			$("#hardLi").show();
		}else{
			$("#hardLi").hide();
		}
	}
}

//火化炉类型单击
function radio_click(type) {
	if (type==${furnace_ty}) {
		$("#furnaceType_div").show();
		$("#specialBusinessType").hide();
   		var types="no";
   		for(var i=1;i<=sum;i++){
   			var text=$("#helpCode_"+i).find("option:selected").text();
   			if(($("#helpCode_"+i).attr('name')==='helpCode') && (text==="301" ||text==="302"||text==="420")){
   				$("#checkbox_"+i).attr('checked','checked');
   				types="yes";
   			}
   		}
   		if(types==="yes"){
   			delHtml("service");
   		}
		sum++;
		item(sum,"${funListTY[0]}","","${funListTY[1]}","${funListTY[2]}","${funListTY[3]}","${funListTY[4]}","${funListTY[5]}","${funListTY[6]}","${funListTY[7]}","","${funListTY[9]}","${funListTY[10]}")
		sortItem($("#service"));
		sTotal+=parseFloat("${funListTY[7]}");
		$("#sFont").html(sTotal+"元");
// 		funBefore='checkbox_'+sum;
		sum++;
		item(sum,"${funList[0]}","","${funList[1]}","${funList[2]}","${funList[3]}","${funList[4]}","${funList[5]}","${funList[6]}","${funList[7]}","","${funListTY[9]}","${funListTY[10]}")
		sortItem($("#service"));
		sTotal+=parseFloat("${funList[7]}");
		$("#sFont").html(sTotal+"元");
		var age=$("#age").val();
		var ageNumber=Number(age);
		if(ageNumber<14){
			$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("302")').attr('selected','selected');
			itemHelpCode(sum,"service");
		}else{
			$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("301")').attr('selected','selected');
			itemHelpCode(sum,"service");
		}
	}else{
		$("#furnaceType_div").hide();
		if(type!=='3'){
			$("#specialBusinessType").hide();
    		var types="no";
    		for(var i=1;i<=sum;i++){
    			var text=$("#helpCode_"+i).find("option:selected").text();
    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="301" ||text==="302"||text==="420")){
    				$("#checkbox_"+i).attr('checked','checked');
    				types="yes";
    			}
    		}
    		if(types==="yes"){
    			delHtml("service");
    		}
			sum++;
			funBefore='checkbox_'+sum;
			item(sum,"${funList[0]}","","${funList[1]}","${funList[2]}","${funList[3]}","${funList[4]}","${funList[5]}","${funList[6]}","${funList[7]}","","${funListTY[9]}","${funListTY[10]}")
			sortItem($("#service"));
			sTotal+=parseFloat("${funList[7]}");
			$("#sFont").html(sTotal+"元");
			var age=$("#age").val();
			var ageNumber=Number(age);
			if(ageNumber<14){
				$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("302")').attr('selected','selected');
				itemHelpCode(sum,"service");
			}else{
				$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("301")').attr('selected','selected');
				itemHelpCode(sum,"service");
			}
		}else{
			$("#specialBusinessType").show();
			var types="no";
    		for(var i=1;i<=sum;i++){
    			var text=$("#helpCode_"+i).find("option:selected").text();
    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="301" ||text==="302"||text==="420")){
    				$("#checkbox_"+i).attr('checked','checked');
    				types="yes";
    			}
    		}
    		if(types==="yes"){
    			delHtml("service");
    		}
		}
		
	}
}
//创建时间
function ndatet(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]+' '+arrTime[1]);
	return checkDate;
}
function ndate(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]);
	return checkDate;
}
$('#orderTime').change(function(){
	if($('#mourningEndTime').val()!==""){
		var checkTime=ndate($('#mourningEndTime').val());
		var now=ndate($(this).val());
		if(now-checkTime<0){
			toastr["error"]("火化预约时间必须大于守灵结束时间！");
			$(this).val(' ');
		}
	}else if($('#farewellBeginDate').val()!==""){
		var checkTime=ndatet($('#farewellBeginDate').val());
		var now=ndatet($(this).val());
		if(now-checkTime<0){
			toastr["error"]("火化预约时间必须大于告别时间！");
			$(this).val(' ');
		}
	}
// 	var beforeTime=ndate($('#arriveTime').val());
// 	var nextTime=ndate($('#orderTime').val());
// 	var longTime=(nextTime-beforeTime)/(24*60*60*1000);
// 	if(longTime<=0){
// 		longTime=1;
// 	}
// 	if(before!==''){
// 		$("#"+before).parent().siblings().eq(4).find('input').val(longTime);
// 		var sum1=before.slice(before.indexOf('_')+1);
// 		numbers(sum1,"service");
// 	}
	//有冰柜项目，且没有灵堂项目和告别厅 ，以预约火化时间为准
	if($("#freezerName").val()!=""){
		iceHave="yes";
	}
	if(iceHave==="yes" && $("#mourningName").val()===""&& $("#farewellName").val()===""){
		var beforeTime=ndate($('#arriveTime').val());
		var nextTime=ndate($('#orderTime').val());	
// 		if(furnaceOrderTime!=''){
// 			nextTime=ndate(furnaceOrderTime);
// 			alert("1--"+furnaceOrderTime);
// 			alert("3--"+nextTime);
// 		}
		var longTime=(nextTime-beforeTime)/(24*60*60*1000);
// 		alert(longTime);
		if(longTime<=0){
			longTime=1;
		}
		var ice="no";
		for(var i=1;i<=sum;i++){
			var text=$("#helpCode_"+i).find("option:selected").text();
			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="405"||text==="403"||text==="402")){//先判断排除水晶棺之后冷藏记录
				ice="yes";
				$("#number_"+i).val(longTime)
				numbers(i,"service");
// 				furnaceOrderTime="";
				break;
			}
		}
// 		if(ice==="no"){//若以上没有冷藏记录，则在水晶棺记录中删除    !!!条件不是很充足!!!
// 			for(var i=1;i<=sum;i++){
//     			var text=$("#helpCode_"+i).find("option:selected").text();
//     			if(($("#helpCode_"+i).attr('name')==='helpCode') && (text==="402")){
//     				$("#number_"+i).val(longTime)
//     				numbers(i,"service");
//     				break;
//     			}
//     		}
// 		}
	}
})
//检查运送时间     最新需求,不再限制运送时间必须小于到馆时间
// $('#car').on('change','tr input[name="pickTime"]',function(){
// 	if($('#arriveTime').val()!==''){
// 		var checkTime=ndatet($('#arriveTime').val());
// 		var now=ndatet($(this).val());
// 		if(now-checkTime>=0){
// 			$(this).val('');
// 			toastr["error"]("运送时间必须小于到馆时间！");
// 		}
// 	}
// });
//排序项目
function sortItem(target){
	var trs=target.find('tr');
	
	//trs=Array.prototype.slice.call(trs);
	trs.sort(function(a,b){
		var type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		var type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		if(type1==type2){
			type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
			type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
		}
		return type2>type1?-1:type2<type1?1:0;
	})
	target.html(trs);
}
//$('#service').on('change','tr select[name="typeId"]',sortItem.bind(this,$('#service')));
//$('#articles').on('change','tr select[name="typeId"]',sortItem.bind(this,$('#articles')));
$('#service').on('change','tr select[name="helpCode"]',function(){
	var txt=$(this).find('option:selected').text();
	var text=$(this).parent().siblings(':last').find('textarea');
	if('${id}'==""){
	if(txt=='535'||txt=='538'){
		if(text.hasClass('required')){return;}
		text.addClass('required');
	}else{
		if(text.hasClass('required')){text.removeClass('required');}
	}
	}
})
//告别厅时间改变
function farewellTime_change() { 
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'farewellTime_change',time:$("#farewellBeginDate").val()},
	    success: function (json) {
	    	if (json.statusCode == 200) {
		    	$("#orderTime").val(json.time);
	    	} else {
	    		toastr["warning"]("时间格式有误");
	    	}
		},
	   error: function (e) {
		   toastr["error"]("系统异常");
	   }
	} )
}


//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   { //author: meizz   
	var o = {   
	 "M+" : this.getMonth()+1,                 //月份   
	 "d+" : this.getDate(),                    //日   
	 "h+" : this.getHours(),                   //小时   
	 "m+" : this.getMinutes(),                 //分   
	 "s+" : this.getSeconds(),                 //秒   
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	 "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
	 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
	 if(new RegExp("("+ k +")").test(fmt))   
	fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
}  
/* 保存时候的方法 */ 
function validateSub(form, callback) {
	$("button").prop("disabled", "disabled");
	var $form = $(form);
	/* var va = validHomeForm($form); */
	$(".error-label", form).remove();
	$("[errorFlag]", form).removeAttr("errorFlag");
	$(".error-input", form).removeClass("error-input");
	var errArr=[];
	$(".required", form)
	.each(
			function(i,ele) {
				var value = $(this).val();
				if (value == "") {
					var flag = $(this).attr("errorFlag");
					if (flag == undefined) {
						$(this).attr("errorFlag", "true");
						$(this).addClass("error-input");
						var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
						$(this).parent().append(err);
						errArr.push(i)
					}
				}
			});
	if(errArr.length!==0){
		var errNum=Math.min.apply(Math,errArr);
		var ele=$(".required", form).eq(errNum);
		var errName=$(ele).attr('name');
		var errMsg=errName=='pickTime'?'运送时间':errName=='comment'?'挽联备注':$(ele).prev().text().slice(0,-1);
		if($('#a [name="'+errName+'"]').size()!==0){$('[href="#a"]').click()}
		if($('#b [name="'+errName+'"]').size()!==0){$('[href="#b"]').click()}
		var height=$('.required[name="'+errName+'"]').eq(0).offset().top;
		window.scrollTo(0,height);
		toastr["warning"]("请填写"+errMsg);
		$("button").removeAttr("disabled");
		return false;
	}
	
// 	if (va == false) {
// 		$("button").removeAttr("disabled");
// 		toastr["warning"]("请确保填写完整并且格式正确");
// 		return false;
// 	}
	rel = $form.attr("rel");
	/*保存时发出请求*/
	var ajax = function(){
		$.ajax({
    		type : 'POST',
    		url : $form.attr("action"),
    		data : $form.serializeArray(),
    		traditional: true,
    		dataType : "json",
    		cache : false,
    		success : callback,
    		error : function(XMLHttpRequest, textStatus, errorThrown) {
    			toastr["error"](XMLHttpRequest.status);
    			$("button").removeAttr("disabled");
    		}
    	});
	}
	/*调用弹窗的地方*/
	pop.show({
        title:"操作确认",
        content:'保存并且同时审核吗？',
        ok:'是',
        cancel:"否",
        width:"300px",
        okFn:function(){
        	$("#isSH").val('yes');
        	ajax();
        },
        cancelFn:function(){
        	  $("#isSH").val('not');
        	  ajax();
        }
    });
	
	return false;
// 	$.ajax({
// 		type : 'POST',
// 		url : $form.attr("action"),
// 		data : $form.serializeArray(),
// 		traditional: true,
// 		dataType : "json",
// 		cache : false,
// 		success : callback,
// 		error : function(XMLHttpRequest, textStatus, errorThrown) {
// 			toastr["error"](XMLHttpRequest.status);
// 			$("button").removeAttr("disabled");
// 		}
// 	});
	
// 	return false;
}
/*弹窗的方法*/
pop ={
        show:function(html){
            var _this = this;
            $(".tanchuang").html(_this.gethtml(html)).css("width",html.width);
            _this.postion();
            $(window).resize(function(){
                _this.postion();
            });
            $(".tanchuang").show();
            $(".tanchuang").off('click.cancelFn').on('click.cancelFn','.pop-footer .cancelFn, .close',function(){
            	 if(html.cancelFn){
                     html.cancelFn();
                     $(".tanchuang").hide();
                 }
            });
            $(".tanchuang").off('click.okFn').on('click.okFn','.pop-footer .okFn',function(){
                if(html.okFn){
                    html.okFn();
                    $(".tanchuang").hide();
                }
            });

        },
        gethtml:function(text){
        	html ="<div style='background:#fff;border-radius: 6px;'>";
            html += "<div class='pop-title'><button type='button' class='close'>×</button><h4 style='color:#00c0ef'>"+text.title+"</h4></div>";
            html += "<div class='pop-content'>"+text.content+"</div>";
            html += "<div class='pop-footer'>" +
                    "<button class='okFn'>"+( text.ok || "保存" )+"</button>" +
                    "<button class='cancelFn'>"+( text.cancel || "关闭" )+"</button>";
            "</div>";
            html +="</div>";
            return html;
        },
        postion:function(){
            var width = $(".tanchuang").outerWidth();
            var heigth = $(".tanchuang").outerHeight();
            var winwidth = $(window).width();
            var winheight = $(window).height();
            $(".tanchuang").css({
                top: (winheight-heigth)/2,
                left:(winwidth-width)/2
            });
        }

    }


</script>
 
<style>
.container-fluid { 
	background: #fff;
} 
.pop{
     position: absolute;
     border: 1px solid #ddd;
 } 
.tanchuang{
  display:none;
      border: 1px solid #ccc;
      font-size: 14px;
      box-shadow: 0px 0px 5px #ddd;
      position: fixed;
      background: #fff;
      z-index:900;
}
.tanchuang .pop-title{
    border-bottom: 1px solid #ddd;
    padding: 15px;
     
}
.pop-title .close{ margin-top:-2px;}
.pop-title h4{ margin:0;}
.tanchuang .pop-content{
    padding: 15px;
}
.tanchuang .pop-footer{
    border-top: 1px solid #ddd;
    padding: 15px;
    text-align: right;
}
.pop-footer button{
    padding: 6px 20px;
    border: 1px solid transparent;
    border-radius: 3px;
    margin-left: 10px;
    outline: none;
    cursor: pointer;
    border-radius: 6px;
}
.pop-footer .okFn {
    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
}
.pop-footer .okFn:hover {
    background-color: #286090;
    border-color: #286090;
}
.pop-footer .cancelFn {
    color: #333;
    background-color: #f4f4f4;
    border-color: #ccc;
}
.pop-footer .cancelFn:hover {
    color: #333;
    background-color: #e6e6e6;
    border-color: #adadad;
}
.tanchuang:after{
    content: '';
	position: fixed;
	background-color: #000;
	z-index: -1;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0px; opacity: 0.6;
} 
</style>
	<div class="tanchuang"></div>
	<section class="content">
	
	
	<div style='opacity:0; position: absolute;'>
			<object id="plugin0" type="application/lagen-plugin" width="0" height="0">
				<param name="onload" value="pluginLoaded" />
			</object>
			<object id="plugin1" type="application/x-lathumbplugin" width="0" height="0">
 				 <param name="onload" value="pluginLoaded" />
 			</object>
			</div>
									
									
									
									
		<form action="${url}" id="homeForm" rel="myModal" onsubmit="return validateSub(this,homeAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" data-changeId="true" value="${commissionOrder.id}">
		<input type="hidden" id="appointmentId" name="appointmentId" value="${appointmentId }" >
		<input type="hidden" id="name" value="${commissionOrder.name }">
		<input type="hidden" id="isSH" name="isSH" value="">
			<div class="box-body">
				<div class="nav-tabs-custom" style="overflow:hidden">
					<ul class="nav nav-tabs">
						<li class="active" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab" aria-expanded="true">基本信息</a></li>
						<li id='yeInfo' class="" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" aria-expanded="false">业务信息</a></li>
						<li id="baseLi" class="" style="width: 150px; text-align: center;"><a href="#c" data-toggle="tab" aria-expanded="false">基本项目申免</a></li>
						<li id="hardLi" class="" style="width: 150px; text-align: center;"><a href="#d" data-toggle="tab" aria-expanded="false">困难救助减免</a></li>
						<li id="" class="" style="width: 150px; text-align: center;"><a href="#e" data-toggle="tab" aria-expanded="false">拍照存档</a></li>
					</ul>
					<div class="tab-content" style='padding-left:0px;padding-right:0px;'>
			
			    <!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						<!-- 选择申免项目 -->
						<div class="box-body border-B">
							<div class="col-md-4 height-align">
								<label class='lab-6'>是否基本减免：</label>
								<select id="baseIs" name="baseIs" onchange="is_change('baseIs')"  class='list_select nopadding-R' >${baseIsOption}</select>
							</div>
							<div class="col-md-4 height-align">
								<label class='lab-6'>是否困难减免：</label>
								<select  id="hardIs" name="hardIs" onchange="is_change('hardIs')" class='list_select nopadding-R' >${hardIsOption}</select>
							</div>
						</div>
						<P class='p border-B'>死者信息</P>
							<div class="box-body broder-B">
									
									
								<c:choose>
									<c:when test="${commissionOrder.name !=null }">
										<div class="col-md-5 height-align">
											<label class='lab-4'>死者姓名：</label>
											<input type="text" style='width:20%' autocomplete="off" class="required list_select" name="name" value="${commissionOrder.name }">&nbsp;<i class="fa fa-circle text-red"></i>
											<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardD('name')">
										</div>
									</c:when>
									<c:otherwise>
										<div class="col-md-5 height-align">
											<label class='lab-4'>死者姓名：</label>
											<input type="text" style='width:20%' autocomplete="off" class="required list_select" name="name" value="${appointmentName }">&nbsp;<i class="fa fa-circle text-red"></i>
											<input type="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardD('name')">
										</div>
									</c:otherwise>
								</c:choose>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死者地址：</label> 
									<input type="text" autocomplete='off' style="width:330px;" class="required list_select" name="dAddr" id="dAddr" value="${commissionOrder.dAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件类型：</label>
									<select style="width: 169px" class="list_select" name="certificateId" id="certificateId">
										${certificateOption}
									</select>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>接尸地址：</label> 
<%-- 									<input type="text" class="required list_select" name="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;<i class="fa fa-circle text-red"></i> --%>
									<input list="pickAddr1" autocomplete='off' style="width:330px;" class="required  list_select" name="pickAddr" id="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
									<datalist  class="list_select"  id="pickAddr1">
										${corpseAddOption}
									</datalist>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件号码：</label> 
									<input type="text" class="required list_select" autocomplete="off" name="certificateCode" id="certificateCode" value="${commissionOrder.certificateCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡类型：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="deadTypeId" id="deadTypeId" onchange="deadTypeChange()">
										${deadTypeOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者年龄：</label> 
									<input type="text" class="required  number list_select" onchange="changeAge(this)" name="age"  id="age" autocomplete="off" value="${commissionOrder.age==0 ? '' : commissionOrder.age }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡原因：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" onchange="deathCauseChange()" name="deadReasonId" id="deadReasonId">
										${deadReasonOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者性别：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="sex" id="sex">
										${sexOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡日期：</label> 
									<input  data-id="reservation" readOnly='readOnly' data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select required" id="dTime" name="dTime" value='<fmt:formatDate value="${commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/>'><i style="margin-left: -20px;" class="fa fa-calendar"></i>&nbsp; &nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								
								<div class="col-md-5 height-align">
									<label class='lab-2'>民族：</label> 
										<input list="dNation" autocomplete='off' class="list_select required" name="dNation" id="nation" value="${commissionOrder.dNationId }">
									<datalist  class="list_select"  id="dNation">
												${nationOption}
									</datalist>	

									</select>
								</div>
								<div class="col-md-7 cl" style='line-height:30px;'>
									<label style='float:left;'>死者地区：</label> 
										<div class='cl' style='float:left;'>
											<div  style='float:left;margin-right:10px;'>
												<select  class="list_input" name="province" id="province">
											
												</select>
											</div>
											<div  style='float:left;margin-right:10px;'>
												<select  class="list_input" name="city" id="city">
											
												</select>
											</div>
											<div  style='float:left;'>
												<select  class="required list_input" name="area" id="area" onchange="areaChange()">
											
												</select>
												&nbsp;<i class="fa fa-circle text-red"></i>
											</div>
										</div>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-2'>卡号：</label> 
									<input type="text" class="required list_select" name="cardCode" autocomplete="off" value="${commissionOrder.cardCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-7 height-align hide" id="ifNormal">
									<label class='lab-4'>证明单位：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="proveUnitId" id="proveUnitId">
										${proveUnitOption}
									</select>&nbsp;<i class="fa fa-circle text-red"></i>
									<input style="width: 200px" type="text" name="proveUnitContent" class="list_select" value="${commissionOrder.proveUnitContent }">
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡证明：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="dFlag" id="dFlag">
										${dFlagOption}
									</select>
								</div>
								<div class="col-md-12">
									<label>死者身份证图片</label> 
<!-- 									<div class='cl'> -->
									<input  type='file' class='hide' id='dFile' name="dfile">
									<div class='ID-pic' data-name='name'>
									<span>点击选择图片</span>
										<c:choose>
				    						<c:when test="${commissionOrder.id != null}">
				    							<img width=325px;  src="${commissionOrder.dIdcardPic }" alt="images">
				    						</c:when>
				    						<c:otherwise>
				    						</c:otherwise>
			    						</c:choose>
<!-- 										<object id="plugin1" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
									</div>
									
									<input type="hidden" name="filename" value='${commissionOrder.dIdcardPic }'>
									<button type='button'  style='margin-top:10px;' class='btn btn-default' data-a='upload'>确认上传</button>
<!-- 									<div class='ID-pic' onClick="capture(this,'F')"> -->
<!-- 										<span>点击上传反面</span> -->
<!-- 										<object id="plugin2" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
<!-- 									</div> -->
<!-- 									</div> -->
<%-- 									<textarea type="text" class="form-control" name="dIdcardPic" value="${commissionOrder.name }" style="width: 180px;height: 200px"></textarea> --%>
								</div>
							</div>
							<p class='p border-B'>家属/经办人信息</p>
							<div class="box-body">
								<div class="col-md-6 height-align">
									<label  class='lab-6'>家属姓名：</label> 
									<input type="text" autocomplete="off" class="required list_select" name="fName" id="fName" value="${commissionOrder.fName }">&nbsp;<i class="fa fa-circle text-red"></i>
									<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardDF('fName')">
								</div>
								
								
								<div class='hide'>
										当前设备：<select id="curDev" style="width: 90px" name="selDev"
																			onchange="changedev()"></select>
										当前分辨率：<select id="curRes" style="width: 90px" name="curRes"
																			onchange="changeres()"></select>
										颜色：<select id="curColor" style="width: 90px" name="curRes"
																			onchange="changeclr()"></select>
										拍照模式：<select id="capMode" style="width: 90px" name="curRes"
																			onchange="changemode()"></select>
									
										<input id="rotatecrop" checked type="checkbox" value="" onclick="RotateCrop(this)" />纠偏裁边
										<input id="drawrect" type="checkbox" value="" onclick="setmousemode(this)" />框选
										<br><br>    
									<input   TYPE="button"   VALUE="开始预览"   onClick="start_preview()"> 
									<input   TYPE="button"   VALUE="停止预览"   onClick="stop_preview()">
									<input   TYPE="button"   VALUE="左转90度"   onClick="rotleft()">
									<input   TYPE="button"   VALUE="右转90度"   onClick="rotright()">
									<input   TYPE="button"   VALUE="视频属性"   onClick="showprop()">
									<input   TYPE="button"   VALUE="条码识别"   onClick="readbarcode()">
									<input   TYPE="button"   VALUE="画面恢复"   onClick="resetvideo()">
									<input   TYPE="button"   VALUE="生成PDF"   onClick="makepdf()">
									<input   TYPE="button"   id=recvideo VALUE="开始录像"   onClick="startrecord()">
									<input   TYPE="button"   VALUE="拍照"   onClick="capture()">
									<input   TYPE="button"   VALUE="拍照为Base64"   onClick="capturebase64()"> <br><br>
									<input   TYPE="button"   id=autocap VALUE="开始智能连拍"   onClick="startautocap()">
									<input   TYPE="button"   id=tmcap VALUE="开始定时连拍"   onClick="starttmcap()">
								</div>
								
								
								<div class="col-md-5 height-align">
									<label  class='lab-6'>与死者关系：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="fAppellationId" id="fAppellationId"> 
									    ${fAppellationOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<c:choose>
									<c:when test="${commissionOrder.fPhone !=null}">
										<div class="col-md-6 height-align">
											<label  class='lab-6'>联系号码：</label> 
											<input type="text" autocomplete="off" class="required list_select" name="fPhone" id="fPhone" value="${commissionOrder.fPhone}">&nbsp;<i class="fa fa-circle text-red"></i>
										</div>
									</c:when>
									<c:otherwise>
										<div class="col-md-6 height-align">
											<label  class='lab-6'>联系号码：</label> 
											<input type="text" autocomplete="off" class="required list_select" name="fPhone" id="fPhone" value="${appointmentPhone }">&nbsp;<i class="fa fa-circle text-red"></i>
										</div>
									</c:otherwise>
								</c:choose>								
								<div class="col-md-5 height-align">
									<label  class='lab-3'>住址：</label>
									<input type="text" autocomplete="off" class="list_select" name="fAddr" value="${commissionOrder.fAddr }">
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-2'>单位：</label> 
									<input type="text" autocomplete="off" class="list_select" name="fUnit" value="${ommissionOrder.fUnit }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>身份证号码：</label> 
									<input type="text" autocomplete="off" class="list_select" name="fCardCode" id="fCardCode" value="${commissionOrder.fCardCode }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>业务审核状态：</label>
			    					<c:choose>
			    						<c:when test="${checkFlag==Check_Yes }">
			    							<font color="green">${commissionOrder.checkFlagName}</font>
			    						</c:when>
			    						<c:otherwise>
			    							<font color="red">未审核</font>
			    						</c:otherwise>
			    					</c:choose>
								</div>
								<div class="col-md-12">
									<label>家属身份证图片</label>
<!-- 									<div class='cl'> -->
									<input class='hide' class='hide' type='file' id='fFile'>
									<div class='ID-pic' data-name='fName'>
									<span>点击选择图片</span>
										<c:choose>
			    						<c:when test="${commissionOrder.id != null}">
			    							<img width=325px;  src="${ commissionOrder.eIdcardPic }" data-status='1'  alt="images"/>
			    						</c:when>
			    						<c:otherwise>
			    						</c:otherwise>
			    						</c:choose>
<!-- 										<object id="plugin3" type="application/x-lathumbplugin" width="0" height="0"> -->
<!-- 										    <param name="onload" valurequired Loaded" /> -->
<!-- 										</object> -->
									</div>
									<input type="hidden" name="filenameF" value='${commissionOrder.eIdcardPic }'/>
									<button type='button' style='margin-top:10px;' class='btn btn-default' data-a='upload'>确认上传</button>
<!-- 									<div class='ID-pic' onClick="capture(this,'F')"> -->
<!-- 										<span>点击上传反面</span> -->
<!-- 										<object id="plugin4" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
<!-- 									</div> -->
<!-- 									</div> -->
<%-- 									<textarea type="text" class="form-control" name="eIdcardPic“ value="${commissionOrder.name }" style="width: 180px;height: 200px"></textarea> --%>
								</div>
							</div>
						</div>
				<!-- 业务信息 -->
						<div class="tab-pane" id="b">
						<p class='p border-B'>业务调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label  class='lab-6'>到馆时间：</label> 
									<c:choose>
										<c:when test="${commissionOrder.arriveTime  !=null}">
											<input data-id="reservation" readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="arriveTime" name="arriveTime" 
											value='<fmt:formatDate value="${commissionOrder.arriveTime }" pattern="yyyy-MM-dd HH:mm"/>'>
										</c:when>
										<c:otherwise>
											<input data-id="reservation" data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="arriveTime" name="arriveTime" value="${now }">
										</c:otherwise>
									</c:choose>
									
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>接尸单位：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="corpseUnitId" id="corpseUnitId">
										${corpseUnitOption}
									</select>
									<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>冷藏柜号：</label> 
									<input type="hidden" name="freezerId" id="freezerId"  value="${freezerRecord.freezerId }">
									<input name="freezerName" id="freezerName" class=" list_select" readonly="readonly" value="${freezerRecord.freezer.name }">
<!-- 									<select style="width: 169px" class="list_select" name="freezerId" id="freezerId"> -->
<%-- 										${freezerOption} --%>
<!-- 									</select> -->
<%-- 									<a href="${url}?method=editFreezer" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a> --%>
								</div>
								<div class="col-md-7 height-align">
									<label  class='lab-6'>挂账单位：</label> 
									<select style="width: 169px" class="nopadding-R list_select nopadding-R" name="billUnitId" id="billUnitId">
										${billUnitOption}
									</select>	
								</div>
<!-- 								<div class="col-md-7"> -->
<!-- 									<label class="lable_c">冷藏时间：</label>  -->
<%-- 									<input data-provide="datetimepicker" data-date-format="yyyy-mm-dd hh:ii:ss" class="required list_select" id="freezerBeginDate" name="freezerBeginDate" value="<fmt:formatDate value="${freezerRecord.beginDate}" pattern="yyyy-MM-dd"/>"> --%>
<!-- 									<label >--</label> -->
<%-- 									<input data-provide="datetimepicker" data-date-format="yyyy-mm-dd hh:ii:ss" class="required list_select" id="freezerEndDate" name="freezerEndDate" value="${freezerRecord.endDate }">&nbsp;<i class="fa fa-circle text-red"></i> --%>
<!-- 								</div> -->
								<div class="col-md-5 height-align"> 
									<label  class='lab-6'>守灵室号：</label> 
									<c:choose>									
										<c:when test="${mourningCode2!=null }">
											<input type="hidden" name="mourningId" id="mourningId"  value="${mourningId2 }">
											<input name="mourningName" id="mourningName" class=" list_select" readonly="readonly" value="${mourningCode2}">
										</c:when>
										<c:when test="${mourningCode!=null }">
											<input type="hidden" name="mourningId" id="mourningId"  value="${mourningId1 }">
											<input name="mourningName" id="mourningName" class=" list_select" readonly="readonly" value="${mourningCode}">
										</c:when>
										<c:otherwise>
											<input type="hidden" name="mourningId" id="mourningId"  value="${mourningRecord.mourningId }">
											<input name="mourningName" id="mourningName" class=" list_select" readonly="readonly" value="${mourningRecord.mourning.name}">
										</c:otherwise>
									</c:choose>
									
<!-- 									<select style="width: 169px" class="list_select" >  -->
<%-- 										${mourningOption} --%>
<!-- 									</select> -->
									<a href="${url}?method=editMourning&current_time=${mourningRecord.beginTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								</div>
								<div class="col-md-7 height-align">
									<label  class='lab-6'>守灵时间：</label>
									<c:choose>									
										<c:when test="${mourningBeginTime2!=null }">
											<input   class="list_select" id="mourningBeginTime" name="mourningBeginTime" value='<fmt:formatDate value="${mourningBeginTime2}" pattern="yyyy-MM-dd HH:mm"/>'>
											<span class='timerange'>--</span>
											<input   class="list_select" id="mourningEndTime" name="mourningEndTime" value='<fmt:formatDate value="${mourningEndTime2}" pattern="yyyy-MM-dd HH:mm"/>'>
										</c:when>
										<c:when test="${mourningBeginTime!=null }">
											<input   class="list_select" id="mourningBeginTime" name="mourningBeginTime" value="${mourningBeginTime}">
											<span class='timerange'>--</span>
											<c:if test="${mourningEndTime !=null }">
											<input  " class="list_select" id="mourningEndTime" name="mourningEndTime" value="${mourningEndTime}">
											</c:if>
											<c:if test="${mourningEndTime ==null }">
											<input   class="list_select" id="mourningEndTime" name="mourningEndTime" value="${mourningBeginTime}">
											</c:if>
										</c:when>
										<c:otherwise>
											<input   class="list_select" id="mourningBeginTime" name="mourningBeginTime" value='<fmt:formatDate value="${mourningRecord.beginTime }" pattern="yyyy-MM-dd HH:mm"/>'>
											<span class='timerange'>--</span>
											<input   class="list_select" id="mourningEndTime" name="mourningEndTime" value="<fmt:formatDate value="${mourningRecord.endTime }" pattern="yyyy-MM-dd HH:mm"/>">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>告别厅号：</label>
									<c:choose>					
										<c:when test="${farewellCode2!=null }">
											<input type="hidden" name="farewellId" id="farewellId"  value="${farewellId2}">
											<input name="farewellName" id="farewellName" class=" list_select" readonly="readonly" value="${farewellCode2}">
										</c:when>				
										<c:when test="${farewellCode!=null }">
											<input type="hidden" name="farewellId" id="farewellId"  value="${farewellId1 }">
											<input name="farewellName" id="farewellName" class=" list_select" readonly="readonly" value="${farewellCode}">
										</c:when>
										<c:otherwise>
											<input type="hidden" name="farewellId" id="farewellId"  value="${ farewellRecord.farewellId}">
											<input name="farewellName" id="farewellName" class=" list_select" readonly="readonly" value="${ farewellRecord.farewell.name}">
										</c:otherwise>
									</c:choose>
<!-- 									<select style="width: 169px" class="list_select" name="farewellId" id="farewellId"> -->
<%-- 										${farewellOption} --%>
<!-- 									</select> -->
									<a href="${url}?method=editFarewell&current_time=${farewellRecord.beginDate }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								</div>
								<div class="col-md-7 height-align">
									<label  class='lab-6'>告别时间：</label>
									<%-- <input data-id="reservation" readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="<fmt:formatDate value="${farewellRecord.beginDate }" pattern="yyyy-MM-dd HH:mm:ss"/>"> --%>
									<c:choose>									
										<c:when test="${farewellTime2!=null }">
											<input  readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="<fmt:formatDate value="${farewellTime2}" pattern="yyyy-MM-dd HH:mm"/>">
										</c:when>
										<c:when test="${farewellTime!=null }">
											<input  readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="${farewellTime}">
										</c:when>
										<c:otherwise>
											<input  readonly="readonly" class="list_select"  onchange="farewellTime_change()" id="farewellBeginDate" name="farewellBeginDate" value="<fmt:formatDate value="${farewellRecord.beginDate }" pattern="yyyy-MM-dd HH:mm"/>">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-md-5" id="furnace_div">
<!-- 									<select onclick="furnaceType_click()" style="width: 169px" class="list_select" name="furnaceTypeId" id="furnaceTypeId">  -->
<%-- 										${furnaceTypeOption} --%>
<!-- 									</select> -->
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>火化预约时间：</label> 
									<c:choose>		
										<c:when test="${funeralTime2!=null }">
											<input data-id="reservation"  class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${funeralTime2 }" pattern="yyyy-MM-dd HH:mm"/>">&nbsp;<i class="fa fa-circle text-red"></i>
										</c:when>							
										<c:when test="${funeralTime!=null }">
											<input data-id="reservation"   class="list_select" id="orderTime" name="orderTime" value="${funeralTime }">&nbsp;<i class="fa fa-circle text-red"></i>
										</c:when>
										<c:when test="${furnaceRecord.orderTime!=null }">
											<input data-id="reservation"   class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm"/>">&nbsp;<i class="fa fa-circle text-red"></i>
										</c:when>
										<c:otherwise>
											<input data-id="reservation"  class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${fireTime }" pattern="yyyy-MM-dd HH:mm"/>">&nbsp;<i class="fa fa-circle text-red"></i>
										</c:otherwise>
									</c:choose>
									
									<%-- <input data-id="reservation" class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm:ss"/>"> --%>
								</div>
								<div class="col-md-6 height-align" id="furnaceType_div">
									<label  class='lab-6'>火化炉号：</label>
									<c:choose>
										<c:when test="${funeralCode2!=null}">
											<input type="hidden" name="furnaceId" id="furnaceId"  value="${funeralId2 }">
											<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${funeralCode2 }">
											<a href="${url}?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
										</c:when>
										<c:when test="${funeralCode!=null }">
											<input type="hidden" name="furnaceId" id="furnaceId"  value="${funeralId1 }">
											<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${funeralCode }">
											<a href="${url}?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
										</c:when>
										<c:otherwise>
											<input type="hidden" name="furnaceId" id="furnaceId"  value="${furnaceRecord.furnaceId }">
											<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${furnaceRecord.furnace.name }">
											<a href="${url}?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-md-6 height-align" id="specialBusinessType">
									<label class='lab-6'>特殊业务类型:</label>
									<select style="width: 169px" class="nopadding-R list_select " name="specialBusinessValue" id="specialBusinessValue">
										${specialBusinessOption}
									</select>	
								</div>
							</div>
							<p class='border-B p'>车辆调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-12"  style='padding-left:0px;'>
									<small class="btns-buy"> 
										<button type="button" onclick="addCar()" class="btn btn-warning" >添加</button>
										<button type="button" onclick="delHtml('carSchedulRecordId')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="carSchedulRecordId" /></th>
											<th width="200px">运输类型</th>
											<th width="200px">车辆类型</th>
											<th width="200px">运送时间</th>
											<th>备注</th>
										</tr>
									</thead>
									<!-- 车辆调度信息 -->
									<tbody id="car">
										
									</tbody>
								</table>
							</div>
							<p class='p border-B'>服务项目信息</p>
							<div class="box-body border-B">
<!-- 								<div class="box  collapsed-box"> -->
<!-- 									<div class="box-header "> -->
										<div class="col-md-12"  style='padding-left:0px;'>
											<small class="btns-buy"> 
		<%-- 										<a href="${url}?method=selectItem&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
												<a href="${url}?method=selectItems&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
												<button type="button" onclick="delHtml('service')" class="btn btn-danger " >删除</button>
											</small>
										</div>
<!-- 										<div class="box-tools pull-right"> -->
<!-- 											<button type="button" class="btn btn-box-tool" -->
<!-- 												data-widget="collapse"> -->
<!-- 												<i class="fa fa-chevron-down"></i> -->
<!-- 											</button> -->
<!-- 										</div> -->
<!-- 									</div> -->
									<div class="box-body">										
										<table class="table table-bordered" style="margin-top: 60px;" >
											<thead>
												<tr>
													<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="service" /></th>
													<th width="90px">助记码</th>
													<th width="250px">名称</th>
											        <th width="90px">单价（元）</th>
											        <th width="135px">类别</th>
											        <th width="60px">数量</th>
											        <th width="70px">挂账</th>
											        <th width="90px">小计</th>
											        <th>备注</th>
												</tr>
											</thead>
											<tbody id="service">
												
											</tbody>
											<tbody>
												<tr>
													<td style='width: 60px'>合计：</td>
													<td colspan='8' style='text-align: left;' >
													<font color='red' id="sFont">
													0元
													</font>
													<div style="float: right; margin-right: 50%;">
														<font color='black' >合计：共</font>
														<font color='red' id='serviceNumber' >0</font>
														<font color='black' >条记录</font>
													</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
<!-- 								</div> -->
							</div>
							<p class='p border-B'>丧葬用品信息</p>
							<div class="box-body">
<!-- 							<div class="box  collapsed-box"> -->
<!-- 								<div class="box-header ">						 -->
									<div class="col-md-12">
										<small class="btns-buy"  style='padding-left:0px;'>
											<a href="${url}?method=selectItems&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
											<button type="button" onclick="delHtml('articles')" class="btn btn-danger " >删除</button>
										</small>
									</div>
<!-- 									<div class="box-tools pull-right"> -->
<!-- 										<button type="button" class="btn btn-box-tool" -->
<!-- 											data-widget="collapse"> -->
<!-- 											<i class="fa fa-chevron-down"></i> -->
<!-- 										</button> -->
<!-- 									</div> -->
<!-- 								</div> -->
								<div class="box-body">
								<table class="table table-bordered" style="margin-top: 60px;" >
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="articles" /></th>
											<th width="90px">助记码</th>
											<th width="250px">名称</th>
									        <th width="90px">单价（元）</th>
									        <th width="135px">类别</th>
									        <th width="60px">数量</th>
									        <th width="70px">挂账</th>
									        <th width="90px">小计</th>
									         <th>备注</th>
										</tr>
									</thead>
									<tbody id="articles">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="aFont">
											0元
											</font>
											<div style="float: right; margin-right: 50%;">
												<font color='black' >合计：共</font>
												<font color='red' id='articlesNumber' >0</font>
												<font color='black' >条记录</font>
											</div>
											</td>
										</tr>
									</tbody>
								</table>
<!-- 								<div class="col-md-4"> -->
<!-- 									<label>备注</label>  -->
<%-- 									<textarea type="text" class="form-control" name="cComment" value="${commissionOrder.comment }" style="width: 100%;height: 200px"></textarea> --%>
<!-- 								</div> -->
								</div>
<!-- 							</div> -->
								
							</div>
						</div>
				<!-- 基本项目申免 -->
						<div class="tab-pane" id="c">
							<div class="box-body">
								<input type="hidden" name="baseReductionId" value="${baseReduction.id }">
								<div class="col-md-12" id="freePerson"></div>
								<div class="col-md-12" id="hardPerson"></div>
							</div>
							<div class="box-body">
								<div class="col-md-12">
									<small class="btns-buy"  style='padding-left:0px;'> 
										<button type="button" onclick="addBase()" class="btn btn-warning" >添加</button>
										<button type="button" onclick="delHtml('base')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="60px"><input type="checkbox" class="checkBoxCtrl" group="base" /></th>
											<th width="100px">助记码</th>
											<th width="260px">名称</th>
									        <th width="120px">单价（元）</th>
									        <th width="100px">数量</th>
									        <th width="120px">总价</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="base">
										
									</tbody>
									<tbody>
										<tr>
											<td >合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="baseFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="col-md-12">
									<label>备注</label> 
									<textarea type="text" class="form-control" name="bComment" value="${baseReduction.comment }" style="width: 100%;"></textarea>
								</div>
							</div>
						</div>
				
				
				
				
				
				
				
				
				
				<!-- 困难救助减免 -->
						<div class="tab-pane" id="d">
							<div class="box-body border-B">
								<div class="col-md-12" id="prove"></div>
								
								<div class="col-md-5 height-align">
									<label  class='lab-6'>申请日期：</label>
									<input data-id="reservation" readOnly='readOnly'  data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="createTime" name="createTime" value='<fmt:formatDate value="${hardReduction.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>申请人姓名：</label>
									<input type="text" class="list_select" name="applicant" value="${hardReduction.applicant }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>申请原因：</label>
									<input type="text" class="list_select" name="reason" value="${hardReduction.reason }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>联系电话：</label>
									<input type="text" class="list_select" name="phone" value="${hardReduction.phone }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-6'>证明单位：</label>
									<select class="list_select nopadding-R" name="hard_proveUnitId" id="hard_proveUnitId" >
										${hard_proveUnitOption}
									</select>&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-6'>与死者关系：</label>
									<select style="width: 169px" class="list_select nopadding-R" name="appellationId" id="appellationId"> 
									    ${hard_appellationOption}
									</select>
								</div>
								<div class="col-md-6 height-align">
								<input type="hidden" name="hardReductionId" value="${hardReduction.id }">
								<label   class='lab-6'>是否特殊减免：</label>
								<select  class='list_select nopadding-R' name="special" id="special">${specialOption}</select>
								</div>
							</div>
							<div class="box-body">
								<div class="col-md-12">
									<small class="btns-buy"  style='padding-left:0px;'> 
										<a href="${url}?method=selectItems&id=hard" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<!-- <button type="button" onclick="addHard()" class="btn btn-warning" >添加</button> -->
										<button type="button" onclick="delHtml('hard')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="60px"><input type="checkbox" class="checkBoxCtrl" group="hard" /></th>
											<th width="100px">助记码</th>
											<th width="260px">名称</th>
									        <th width="120px">单价（元）</th>
									        <th width="100px">数量</th>
									        <th width="120px">总价</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="hard">
										
									</tbody>
									<tbody>
										<tr>
											<td >合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="hardFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="col-md-12">
									<label>备注</label> 
									<textarea type="text" class="form-control" name="hComment" value="${hardReduction.comment }" style="width: 100%;"></textarea>
								</div>
							</div>
						</div>
			
			
			
			
					<!-- 拍照 -->			
						<div class="tab-pane" id="e">
						   <p class="p border-B">拍照存档</p>
							<div class="box-body border-B">
							
								<div class="col-md-12">
									<input class="hide" type="file" id="pFile">
									<div class="ID-pic ID-pic1" data-name="pName" style="width:329px; height:249px">
									<span>点击选择图片</span>
										<c:choose>
				    						<c:when test="${commissionOrder.id != null}">
				    							<img width=325px; height=249px; src="${commissionOrder.photoPic }" alt="images">
				    						</c:when>
			    						</c:choose>
									</div>
									<iframe width="290" style="border:0;" height="60px" scrolling="yes" src='demo.html'></iframe></br>
									<input type="hidden" name="filenameP" value="${commissionOrder.photoPic }">
									<button type="button" style="margin-top:10px;" class="btn btn-default" data-a="upload">确认上传</button>
								</div>
							</div>
						</div>
	 
						
						
					</div>
				</div>
			</div>
			<div class="box-body">
			<div  class="container-fluid">
				<div class='row padding-B'>
					<div class='col-md-6'>
						<div class='row'>
							<div class="col-md-5 height-align">
								<label>业务登记员：</label>
								<c:choose>
								    <c:when test="${commissionOrder.agentUser.name!=null}">
								    	${commissionOrder.agentUser.name}
								    </c:when>
								    <c:otherwise>
								   		${agentUser}
									</c:otherwise>
								</c:choose> 
								
							</div>
							<div class="col-md-6 height-align">
								<label>预约登记人：</label>
								<span>${commissionOrder.pbookAgentId }</span>
							</div>
							
							<div class="col-md-5 height-align">
								<label>到馆登记人：</label>
								<span>${commissionOrder.bookAgentId }</span>
							</div>
							<div class="col-md-7 height-align">
								<label class='lab-3'>编号：</label> 
								<input type="text" class="list_select" readonly="readonly"  value="${commissionOrder.code == null ? '由系统自动生成' : commissionOrder.code}">
							</div>
							<div class="col-md-6 height-align">
								<label >办理时间：</label> 
								${time }
							</div>
						</div>
					</div>
					<div class='col-md-6 height-align'>
						<div class='row'>
							<div class="col-md-12">
								<small class="pull-right btns-hometab">
								<c:if test="${checkFlag!=Check_Yse }">  <!-- 这个判断条件也不知道是啥意义，后台传过来应该是 Check_Yes?? -->
									<button type="button" onclick="checkSelect()" class="btn btn-info" style='margin-top:30px;'>保存</button>
			   					</c:if>
			<%-- 						<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="certificateId" warm="确认禁用吗" class="btn btn-danger " role="button">重置</a> --%>
								<a href="${url}?method=list" target="homeTab" style='margin-top:30px;' class="btn btn-default" role="button">返回</a>
<!-- 								 <a id="btn_exit" class="btn" href="javascript:void(0);">返回2</a>  -->
								</small>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</form>
		
	</section>
<script type="text/javascript">
// 	$('#btn_exit').click(function(){
// 		window.history.back(); 
// 	})
 	$('.ID-pic').click(function(){
		$(this).prev().click();
	})
	$('[type="file"]').change(function(){
		$(this).next().next().next().attr('disabled')&&$('[data-a="upload"]').removeAttr('disabled');
		$this=$(this);
		var file=this.files[0];
		if(file!==undefined){
			var reader= new FileReader();
			reader.readAsDataURL(file);
			$(reader).load(function(){
				$this.next().find('img').remove();
				var img=document.createElement('img');
				img.src=reader.result;
				img.width=325;
				img.height=415;
				$this.next().append(img);
			})
		}
		
	}) 
	
	
	$('#pFile').change(function(){
		$('button[data-a="upload"]').removeAttr('disabled');
	});
	
	$('[data-a="upload"]').click(function(){
		event.preventDefault();
		$this=$(this);
		//获取  input file对象
		var inputFiles = $this.siblings('[type="file"]');
		if( inputFiles.val() !== '' ){
			var id = inputFiles.attr('id');
			var fileObj = document.getElementById(id).files[0]; 
			var form = new FormData();
			form.append("file", fileObj); 
			var xhr = new XMLHttpRequest();
	        xhr.open("post", 'commissionOrder.do?method=fileupload', true);
	        xhr.onload = function () {
				if((xhr.readyState == 4) && (xhr.status == 200)){
					$this.prev().val(xhr.responseText);
					toastr["success"]("上传成功");
					$this.attr('disabled','disabled');
				}else{
					toastr["danger"]("上传失败");
				}
	        };
	        xhr.send(form);
		}else{
			toastr["warning"]("请选择图片");
		} 
	});
	/* $('[data-a="upload"]').click(function(){
		event.preventDefault();
		$this=$(this);
		if($this.prev().prev().prev().val()!==''){
			var id=$this.prev().prev().prev().attr('id');
			var fileObj = document.getElementById(id).files[0]; 
			var form = new FormData();
			form.append("file", fileObj); 
			var xhr = new XMLHttpRequest();
	        xhr.open("post", 'commissionOrder.do?method=fileupload', true);
	        xhr.onload = function () {
				if((xhr.readyState == 4) && (xhr.status == 200)){
					$this.prev().val(xhr.responseText);
					toastr["success"]("上传成功");
					$this.attr('disabled','disabled');
				}else{
					toastr["danger"]("上传失败");
				}
	        };
	        xhr.send(form);
		}else{
			toastr["warning"]("请选择图片");
		} 
	}); */
//若有基本减免，则必须勾选对象类别

		if('${id}'==""){
	 			$("#nation").val("汉族");
	 		} 
function checkSelect() {
	if(!$('#corpseUnitId').val()){
		toastr["warning"]("请填写接尸单位");
		$('#yeInfo').find('a').click();
		var top=$('#corpseUnitId').offset().top;
		window.scrollTo(0,top);
		return false;
	}
	
	if($("#baseIs").val()=='${Is_Yes}'){
		if($("input[name='freePersonId']:checked").size()===0 && $("input[name='hardPersonId']:checked").size()===0){
			toastr["warning"]("请选择免费对象类别或重点类别");
			$('#baseLi').find('a').click();
			var top=$('#baseLi').offset().top;
			window.scrollTo(0,top);
			return false;
		} 
	}
	if ($("#hardIs").val()=='${Is_Yes}') {
		if($("input[name='proveIds']:checked").size()===0){
			toastr["warning"]("请选择证件类型");
			$('#hardLi').find('a').click();
			var top=$('#hardLi').offset().top;
			window.scrollTo(0,top);
			return false;
		}
		if(!$('#hard_proveUnitId').val()){
			toastr["warning"]("请选择证明单位");
			$('#hardLi').find('a').click();
			var top=$('#hardLi').offset().top;
			window.scrollTo(0,top);
			return false;
		}
	}
	if($('input[type="radio"]:checked').val()==1&&$('#orderTime').val().trim()==''){
		toastr["warning"]("请填写火化预约时间");
		var top=$('#orderTime').offset().top;
		window.scrollTo(0,top);
		return false;
	}
	if($('input[type="radio"]:checked').val()==2&&$('#furnaceId').val().trim()==''){
		toastr["warning"]("请选择特约炉号");
		var top=$('#furnaceName').offset().top;
		window.scrollTo(0,top);
		return false;
	}
	//本页面另有一个检测方法，但不完善，新加一个
	//检测灵堂挽联和告别厅挽联的备注情况
	var isReturn=false;
	var x=0;
	$("input[name='service']").each(function(){
		var id = $(this).attr("value");
		var check=$("#helpCode_"+id).find("option:selected").text();
		if("538"===check||"535"===check){//礼厅挽联或者灵堂挽联
			var comment=$("#comment_"+id).val();
			if(comment===""){
				x=id;
				isReturn=true;
			}
		}
	});
	//假如条件成立，说明挽联没写备注
	if(isReturn){
		toastr["warning"]("请填写挽联备注");
		var top=$('#helpCode_'+x).offset().top;
		window.scrollTo(0,top);
		return false;
	}
	$("#homeForm").submit();
};

</script>
<script type="text/javascript" src='js/carId.js'></script>
<script type="text/javascript" src='js/jsAddress.js'></script>
<script>
	addressInit('province', 'city', 'area', '${province==null?"浙江省":province }', '${city==null?"温州市":city }', '${area==null?"":area }');
</script>
<inuput type="file" value="" />



