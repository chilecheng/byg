<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style type="text/css">
	
	#example2_wrapper table tbody td{
		height:37px;
	}
	#example2_wrapper table tbody>tr:first-child,#example2_wrapper table tbody>tr:nth-child(3){
		background-color:#EBEBEB;
	}
	#example2_wrapper table tbody>tr:nth-child(2)>td:first-child{
		vertical-align:middle;
	}
	#example2_wrapper table tbody>tr>td:not(:first-child):hover,#example2_wrapper table tbody>tr:last-child>td:hover{
		background-color:#EBEBEB;
	}
</style>
<script>
(function(){
	$('#example2_wrapper table tbody>tr td[data-num]').each(function(){
		//console.log(this);
		var data=$(this).attr('data-num');
		switch (data){
			case '1':
			$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
			break;
			case '6':
				$(this).html('进行').css({'backgroundColor':'#738ffe','color':'#fff'});
				break;
			case '3':
				$(this).html('锁定').css({'backgroundColor':'#fea625','color':'#fff'});
				break;
			case '4':
				$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
				break;
			case '5':
				$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
				break;
		}
	})
})();
//重置
$("#reset").click(function(){
	$("#freezerId").val("");
	$("#freezerName").val("");
});

//解锁
$("#unlock").click(function(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'unlock'},
	    success: function () {
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
});


//当前时间改变
function changeDate(){
	var date=$("#current_time").val();
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'html',
	    cache: false,
	    data:{method:'editFurnace',current_time:date},
	    success: function (json) {
	    	$("#myModal").html(json);
			$("#myModal").modal("show");
			initTab();
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
}


//单元格单击
function td_click(i,a){
	var date=$("#current_time").val();
	
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'furnace_click',col:a,row:i,date:date},
	    success: function (json) {
	    	$("#furnaceId").val(json.id);
	    	$("#furnaceName").val(json.name);
	    	$("#orderTime").val(json.time);
	    	$('#funeralNum').val(json.id);
	    	$("#myModal").modal("hide");
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
}

</script>
	<div class="modal-dialog" role="document" style="width:1200px;">
		<form action="${url}" id="form1" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				 <div style="text-align: center;">
					<h4 class="modal-title" id="myModalLabel">火化炉使用情况</h4>
				 </div>
			</div>
			<div class="box-body">
				<div class="col-md-12" style="text-align: center;">
					<a href="${url}?method=editFurnace&time=-1&current_time=${date}"  target="dialog" rel="myModal" id="front">前一天</a> 
					<label > ${date }</label> 
					<a href="${url}?method=editFurnace&time=1&current_time=${date}"  target="dialog" rel="myModal" id="back">后一天</a> 
					<input type="text" data-provide="datetimepicker" id="current_time" name="current_time" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  value="${date }">
					<i style="margin-left: -20px;" class="fa fa-calendar"></i>
				</div>
				<div class="col-md-12">
					<ul class="la_ul">
						<li style="background:#9ccc65;"><div>完成</div></li>
						<li style="background:#673ab7;"><div>预定</div></li>
						<li style="background:#738ffe;"><div>进行</div></li>
						<li style="background:#fea625;"><div>锁定</div></li>
						<li style="background:#29b6f6;"><div>占用</div></li>
						<li style="background:#e84e40;"><div>装修</div></li>
					</ul>
				</div>
			</div>
			<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<c:forEach begin="1" items="${list }" var="u" varStatus="i">
							<div class="col-sm-12">
							<c:if test="${u[0].code==7 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dOb }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dOb }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>											
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td  onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
								<c:if test="${u[0].code==8 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObEight }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dObEight }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>											
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td  onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
								<c:if test="${u[0].code==9 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObNine }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dObNine }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>											
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td  onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
								<c:if test="${u[0].code==10 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObTen }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dObTen }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>											
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="td_click(${i.index},${a })"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='维修中'>
																维修
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td  onclick="td_click(${i.index},${a })" data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
								
							</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	
													
