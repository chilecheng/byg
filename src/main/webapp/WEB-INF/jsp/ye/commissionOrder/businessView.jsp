<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<style type="text/css">
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
	.padding{
		padding:15px 30px;
	}
	.content:last-child{
		min-height:100px;
	}
	.btn-color-ab47bc{
		background-color:#ab47bc;
		color:#ffffff;
	}
</style>
<script type="text/javascript">

// 	$(timeMunite);
</script>
<form action="" id="pageForm" >
	<div class="modal-dialog" role="document">
	<%@ include file="/common/pageHead.jsp"%>
	<div class='modal-body nopadding-B nopadding-T' style='width: 800px; margin-left: -100px;'>
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
						<div class="box-body" style='padding:0px;'>
							<!--startprint0-->
							<div class='col-md-12'>
								<p class='col-md-12' style="text-align: center;color:#28b5f4;font-size: 20px;">${order.name }的业务视图</p>
								<div class='row'>
									<table class="table table-bordered-left table-hover">
										<thead>
											<tr>
												<th colspan="6">基本信息</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><b>死者姓名：</b></td>
												<td>${order.name }</td>
												<td><b>性别：</b></td>
							    				<td>${order.sexName }</td>
							    				<td><b>年龄：</b></td>
							    				<td>${order.age }</td>
											</tr>
											<tr>
												<td><b>户籍：</b></td>
												<td>${order.city} ${order.area }</td>
												<td><b>证件号：</b></td>
							    				<td colspan="3">${order.certificateCode }</td>
											</tr>
										</tbody>
									</table>
										<table class="table table-bordered table-hover">
											<thead>
												<tr>
													<th colspan="4">收费信息</th>
												</tr>
												<tr>
													<th style="width:280px">收费状态</th>
													<th>收费金额</th>
													<th>操作人员</th>													
													<th>收费时间</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items = "${businessList }" var="u">
													<tr>
														<td><font color="green">已收</font></td>
														<td><fmt:formatNumber value="${u.payAmount }" pattern="#" type="number"/></td>
														<td>${u.payeeId }</td>
														<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
													</tr>												
												</c:forEach>
												<c:choose>
												<c:when test="${ifPay==true }">
													<tr>
														<td><font color="Red">未收</font></td>
														<td><fmt:formatNumber value="${sumTotal-baseTotal-hardTotal}" pattern="#" type="number"/></td>
														<td></td>
														<td></td>
													</tr>												
												</c:when>
												</c:choose>
											</tbody>
										</table>
									<table class="table table-bordered-left table-hover">
										<thead>
											<tr>
												<th colspan="6">业务信息</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td width="80px"><b>冰柜号：</b></td>
												<td width="200px">${order.freezer.name }</td>
												<td width="60px"><b>时间：</b></td>
							    				<td colspan="3"><fmt:formatDate value="${freezer.beginDate }" pattern="yyyy-MM-dd HH:mm"/></td>
											</tr>
											<tr>
												<td><b>灵堂号：</b></td>
												<td>${order.mourning.name}</td>
												<td><b>时间：</b></td>
							    				<td colspan="3"><fmt:formatDate value="${order.mourningTime }" pattern="yyyy-MM-dd HH:mm"/>
													<span> 至  </span>
													<fmt:formatDate value="${order.mourningEndTime }" pattern="yyyy-MM-dd HH:mm"/></td>
											</tr>
											<tr>
												<td><b>告别厅：</b></td>
												<td>${order.farewell.name }</td>
												<td><b>时间：</b></td>
							    				<td colspan="3"><fmt:formatDate value="${order.farewellTime }" pattern="yyyy-MM-dd HH:mm"/></td>
											</tr>
											<tr>
												<td><b>火化炉：</b></td>
												<td>${order.furnace.name }</td>
												<td><b>时间：</b></td>
							    				<td colspan="3"><fmt:formatDate value="${order.cremationTime }" pattern="yyyy-MM-dd HH:mm"/></td>
											</tr>
										</tbody>
									</table>
										<table class="table table-bordered table-hover">
											<thead>
												<tr>
													<th colspan="4">车辆信息</th>
												</tr>
												<tr>
													<th style="width:280px">运输类型</th>
													<th>车辆类型</th>
													<th>发车时间</th>													
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(carList)==0 }">
													 <tr width="20">
													    <td colspan="5">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
													<c:forEach items = "${carList }" var="u">
														<tr>															
															<td>${u.transportType.name }</td>
															<td>${u.carType.name }</td>
															<td>${u.startTime }</td>
															<td>${u.comment }</td>
														</tr>												
													</c:forEach>
												</c:otherwise>
												</c:choose>
											</tbody>
										</table>
										<table class="table table-bordered table-hover">
											<thead>
												<tr>
													<th colspan="5">减免信息</th>
												</tr>
												<tr>
													<th style="width:280px">名称</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(baseList)==0 }">
													 <tr width="20">
													    <td colspan="5">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items = "${baseList }" var="u">
													<tr>															
														<td>${u.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
												<c:forEach items = "${hardList }" var="u">
													<tr>															
														<td>${u.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
													<td colspan='5' style='text-align: left;' >
													<font color='red'>合计：${baseTotal + hardTotal }元</font>
													</td>
													</tr>
												</c:otherwise>
										  		</c:choose>
											</tbody>
										</table>
								</div>
							</div>
								<!--endprint0-->
								<div class='col-md-12 btns-dialog' id="hiddenDown" >
									<button type="button" class="btn btn-default btn-margin"  data-dismiss="modal">返回</button>
									<a href="javascript:showprint()" class="btn  btn-color-ab47bc  btn-margin">打印</a>
								</div>
									
						</div>
						<!--endprint0-->
					</div>
				</div>
				<!-- /.col -->
			
			</div>
	</section>
	</div>
	</div>
</form>