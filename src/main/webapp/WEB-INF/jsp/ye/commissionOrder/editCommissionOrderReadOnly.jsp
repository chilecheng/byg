<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<!-- <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script> -->
<style type="text/css">
.ID-pic.ID-pic1 img {
    //position: absolute;
    height: auto !important;}
span.list_table{
	display:inline-block;
	text-align:left;
}
.PageNext{page-break-before: always;} 

</style>
<script type="text/javascript">
var reduce=0;
var preConfigList = new Array(); var previewJson = new Array(); 
var sum=0;//标识
var sTotal=0;//服务项目合计总金额	
var aTotal=0;//丧葬用品合计总金额
var baseTotal=0;//基本减免合计总金额
var hardTotal=0;//困难减免合计总金额
//排序项目
function sortItem(target){
	var trs=target.find('tr');
	
	//trs=Array.prototype.slice.call(trs);
	trs.sort(function(a,b){
		var type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		var type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		if(type1==type2){
			type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
			type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
		}
		return type2>type1?-1:type2<type1?1:0;
	})
	target.html(trs);
}
//收费项目方法
function item(sum,name,id,itemId,helpCode,pice,typeId,number,bill,total,comment,findexFlag,indexFlag){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input  readOnly='readOnly' type='hidden' name="+name+"_id value="+id+">";
	str+="<input disabled='disabled' data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"'  readOnly='readOnly' type='checkbox' id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<span   class='list_table'  name='helpCode' id='helpCode_"+sum+"'> "+helpCode+"</span></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<span   class='list_table'  name='itemId' id='itemId_"+sum+"'> "+itemId+"</span></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input   type='text' readonly='readonly' class='list_table' name=pice id='pice_"+sum+"' value="+parseFloat(pice)+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<span disabled='true'  class='list_table'  name='typeId' id='typeId_"+sum+"'>"+typeId+"</span></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input  readOnly='readOnly' type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=number id='number_"+sum+"' value="+number+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<span disabled='true'  class='list_table' name=bill id=bill_"+sum+"> "+bill+"</span></td>";
	str+="<td style='text-align: center; vertical-align: middle; '><input  readOnly='readOnly' type='text' readonly='readonly' name=total id=total_"+sum+" class='list_table' value="+parseFloat(total)+"></td>";
	str+="<td>"+comment+"</td></tr>";
	$("#"+name).append(str);
}

//减免项目方法
function reduction(sum,name,id,item,helpCode,number,pice,comment,total,findexFlag,indexFlag){
	var str="<tr><td><input disabled='true'  readOnly='readonly' type='hidden' name="+name+"_id value="+id+"><input disabled='disabled' data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"' readOnly='readOnly' type='checkbox'   id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td><span   class='list_table'  > "+helpCode+"</span></td>";
	str+="<td><span  disabled='true' class='list_table'  name=reduction_itemId id='itemId_"+sum+"'>"+item+"</span></td>";
	str+="<td><input   type='text' readonly='readonly' class='list_table' name=reduction_pice id=pice_"+sum+"  value="+parseFloat(pice)+"></td>";
	str+="<td><input  readOnly='readOnly' type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=reduction_number id=number_"+sum+"  value="+number+"></td>";
	str+="<td><input   type='text' readonly='readonly' class='list_table' name=reduction_total id=total_"+sum+" value="+parseFloat(total)+"></td>";
	str+="<td>"+comment+"</td></tr>";
	$("#"+name).append(str);
}
// str+="<td><seslect  disabled='true' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name=reduction_helpCode id='helpCode_"+sum+"'>"+helpCode+"</select><i class='che'></i></td>";

//车辆调度方法
function car(sum,id,transportTypeOption,carTypeOption,pickTime,comment){
	//alert(1);
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name='csr_id' value="+id+" >";
	str+="<input disabled='true' type='checkbox' name='carSchedulRecordId' ></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<span  class='list_table' name=transportTypeId  id=transportTypeId_"+sum+"> "+transportTypeOption+"</span></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<span   class='list_table' name=carTypeId id=typeId_"+sum+" >"+carTypeOption+"</span></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input editable='fasle' readOnly='readOnly'    style='width: 180px'  data-date-format='yyyy-mm-dd hh:ii:ss' class='list_table' id=dTime_"+sum+" name=pickTime ></td>";
	str+="<td>"+comment+"</td></tr>";
	$("#car").append(str);
	$("#dTime_"+sum).val(pickTime);
}

//初始化
$(document).ready(function(){
	//火化炉
	$("#furnaceType_div").hide();
	//是否基本或困难减免
	$("#baseLi").hide();
	$("#hardLi").hide();
	if(${base_Is==Is_Yes}){
		$("#baseLi").show();
	}
	if(${hard_Is==Is_Yes}){
		$("#hardLi").show();
	}
	//车辆调度
	<c:forEach var="u" items='${car_List}'>
		sum++;
		car(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}");
		reduce=sum;
	</c:forEach>
	
	//服务项目
	<c:forEach var="u" items='${service_list}'>
		sum++;
		item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
		sTotal+=parseFloat('${u[8]}');
		$("#sFont").html(sTotal+"元");
		var xx=sum-reduce;
		if(${fn:length(service_list)}==xx){
		var trs=$('#service tr');
		trs.sort(function(a,b){
			var type1=$(a).find('td:eq(4) span').html();
			var type2=$(b).find('td:eq(4) span').html();
			return type2>type1?-1:type2<type1?1:0;
		})
		$('#service').html(trs);
		reduce=sum;
		}
	</c:forEach>
	//葬用品项目
	<c:forEach var="u" items='${articles_list}'>
		sum++;
		item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
		aTotal+=parseFloat('${u[8]}');
		$("#aFont").html(aTotal+"元");
		var xx=sum-reduce;
		if(${fn:length(articles_list)}==xx){
			var trs=$('#articles tr');
			trs.sort(function(a,b){
				var type1=$(a).find('td:eq(4) span').html();
				var type2=$(b).find('td:eq(4) span').html();
				return type2>type1?-1:type2<type1?1:0;
			})
			$('#articles').html(trs);
		}
	</c:forEach>
//基本减免
	//免费对象类别
	var fStr="<label>免费对象类别：</label>";
// 	fStr+="<input readOnly=readOnly type='text' value='${Free}' style='border:none'>";
// 	<c:forEach var="u" items='${freePerson_list}'>
// 		$("#freePerson").html(fStr);
// 	</c:forEach>
	<c:forEach var="u" items='${freePerson_list}'>
	fStr+="<input  disabled='true' type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
	fStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
	$("#freePerson").html(fStr);
    </c:forEach>

	//重点救助对象类别
	var hStr="<label>重点救助对象类别：</label>";
// 	hStr+="<input readOnly=readOnly type='text' value='${Weightp}' style='border:none'>";
// 	<c:forEach var="u" items='${hardPerson_list}'>
// 		$("#hardPerson").html(hStr);
// 	</c:forEach>
	<c:forEach var="u" items='${hardPerson_list}'>
	hStr+="<input  disabled  type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
	hStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
	$("#hardPerson").html(hStr);
    </c:forEach>

	//基本减免项目
	<c:forEach var="u" items='${base_list}'>
		sum++;
		var pice=parseFloat('${u[5]}');
		reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[9]}","${u[6]}","${u[5]}","${u[7]}","${u[8]}")
		baseTotal+=pice;
		$("#baseFont").html(baseTotal+"元");
	</c:forEach>
//困难减免
	//证件类型
	var pStr="<label class='lable_b'>证件类型：</label>";
// 	    pStr+="<input type=text readOnly=readOnly value='${Cname}' style='border:none'>&nbsp;&nbsp;&nbsp;&nbsp;";
	<c:forEach var="u" items='${prove_list}'>
 		pStr+="<input  disabled type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
		pStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
		$("#prove").html(pStr);
	</c:forEach>
	//困难减免项目
	<c:forEach var="u" items='${hard_list}'>
		sum++;
		var pice=parseFloat('${u[5]}');
		reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[9]}","${u[6]}","${u[5]}","${u[7]}","${u[8]}")
		hardTotal+=pice;
		$("#hardFont").html(hardTotal+"元");
	</c:forEach>
	
	//火化炉
	var fun="<label class=lab-5>火化炉类型：</label> "
	fun+="<input  readOnly=readOnly style='border:none' type=text value='${tname}'>";
	<c:forEach var="u" items='${furnace_list}'>
// 		fun+="<input  disabled  ${u[2]} type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
			if("${u[2]}"!=""&&${u[0]==furnace_ty}){
				$("#furnaceType_div").show();
			}
// 	</c:forEach>
	$("#furnace_div").html(fun);
	sortItem($('#articles'));
	sortItem($('#service'));
	sortItem($('#hard'));
	sortItem($('#base'));
});
</script>
<style>

.container-fluid { 
	background: #fff;
} 
.list_table {
	border-radius: 6px;
	border: 1px rgb(169, 169, 169) solid;
	width: 100%;
	line-height: 30px;
	height: 30px;
	padding: 0 8px;
}
.white{
	display:inline-block;
	vertical-align:center;
	width:20px;
	height:20px;
	margin-left:-30px;
	background:#fff;
}
.cheng{
  display:inline-block;
	vertical-align:center;
	width:25px;
	height:20px;
	margin-left:-29px;
	background:#fff;
}
.che{
  display:inline-block;
	vertical-align:center;
	width:16px;
	height:13px;
	margin-left:-18px;
	background:#fff;
}
.list_table{
	border:none;
}
</style>

</head>
<body>
	<section class="content">
		<form action="${url}"  onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		<input  readOnly="readOnly" type="hidden" name="method" value="${method }" >
		<input  readOnly="readOnly" type="hidden" id="id" name="id" value="${commissionOrder.id}">
		<input  readOnly="readOnly" type="hidden" id="name" value="${commissionOrder.name }">
		<input readOnly="readonly" type="hidden" id="hardTotalCheck" name="hardTotalCheck" value="${hardTotal }" >
		<input readOnly="readonly" type="hidden" id="hardCheckIs" name="hardCheckIs" value="${hardCheckIs }" >
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab" aria-expanded="true">基本信息</a></li>
						<li class="" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" aria-expanded="false">业务信息</a></li>
						<li id="baseLi" class="" style="width: 150px; text-align: center;"><a href="#c" data-toggle="tab" aria-expanded="false">基本项目申免</a></li>
						<li id="hardLi" class="" style="width: 150px; text-align: center;"><a href="#d" data-toggle="tab" aria-expanded="false">困难救助减免</a></li>
						<li id="" class="" style="width: 150px; text-align: center;"><a href="#e" data-toggle="tab" aria-expanded="false">拍照存档</a></li>
					</ul>
					<div class="tab-content" style='padding:0px'>
			
			<!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						<!--startprint0-->
						<!-- 选择申免项目 -->
						<div class="box-body border-B">
							<div class="col-md-4">
								<label class='lab-6 height-align'>是否基本减免：</label>
									<span>${base }</span>							
							</div>
							<div class="col-md-4">
								<label class='lab-6 height-align'>是否困难减免：</label>
								<span>${ hard}</span>
							</div>
						</div>
							<p class='border-B p'>死者信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者姓名：</label>
									<span>${commissionOrder.name }</span>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死者地址：</label>
									<span>${commissionOrder.dAddr }</span> 
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件类型：</label>
									<span>${certificateOption}</span>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>接尸地址：</label>
									<span>${commissionOrder.pickAddr }</span> 
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件号码：</label>
									<span>${commissionOrder.certificateCode }</span> 
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡类型：</label>
									<span>${deadTypeOption}</span> 
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者年龄：</label>
									<span>${commissionOrder.age }</span> 
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡原因：</label>
									 <span>${deadReasonOption}</span>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者性别：</label>
									<span>${sexOption}</span> 
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡日期：</label>
									<span><fmt:formatDate value="${commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/></span> 
								</div>
								
								<div class="col-md-5 height-align">
									<label  class='lab-4'>民族：</label>
									<span>${commissionOrder.dNationId}</span> 
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死者地区：</label>
									<span>${deadArea }</span> 
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-4'>卡号：</label>
									<span>${commissionOrder.cardCode }</span> 
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>证明单位：</label>
									<span>${proveUnitOption}</span> 
									<input  readOnly="readOnly" style="border:none"  style="width: 200px" type="text" name="proveUnitContent" class="list_select" value="${commissionOrder.proveUnitContent }">
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡证明：</label>
									<span>${dFlagOption}</span> 
								</div>
								<!--endprint0-->
								<!--startprint6-->
								<div class="col-md-6">
									<label>死者身份证图片</label> 
									<div class='ID-pic' style="cursor:inherit" data-name='name'>
										
										<img width=325px;  src="${commissionOrder.dIdcardPic }" alt="images">
									</div>
									
								</div>
								<!--endprint6-->
								<!--startprint1-->
							</div>
						<p class='p border-B'>家属/经办人信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label  class='lab-5'>姓名：</label>
									<span>${commissionOrder.fName }</span> 
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-5'>与死者关系：</label>
									<span>${fAppellationOption}</span> 
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-5'>联系号码：</label>
									<span>${commissionOrder.fPhone }</span> 
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-5'>住址：</label>
									<span>${commissionOrder.fAddr }</span>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-5'>单位：</label>
									<span>${commissionOrder.fUnit }</span> 
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-5'>身份证号码：</label>
									<span>${commissionOrder.fCardCode }</span> 
								</div>
								<div class="col-md-7 height-align">
									<label>业务审核状态：</label>
									<c:if test="${checkFlag==Check_No }">
			    						<font color="red">${commissionOrder.checkFlagName}</font>
			    					</c:if>
			    					<c:if test="${checkFlag==Check_Yes }">
			    						<font color="green">${commissionOrder.checkFlagName}</font>
			    					</c:if>
								</div>
								<!--endprint1-->
								<!--startprint7-->
								<div class="col-md-6">
									<label>家属身份证图片</label>
									<div class='ID-pic' style="cursor:inherit" data-name='fName'>
										
										<img width=325px; src="${ commissionOrder.eIdcardPic }" alt="images"/>
									</div>
									       
								</div>
								<!--endprint7-->
								<!--startprint2-->
							</div>
								<!--endprint2-->
						</div>
				<!-- 业务信息  -->
						<!--startprint3-->
						<div style="page-break-after:always;"></div>
						<div class="tab-pane" id="b">
						<p class='p border-B'>业务调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label class="lab-4">到馆时间：</label>
									<span><fmt:formatDate value="${commissionOrder.arriveTime }" pattern="yyyy-MM-dd HH:mm"/></span> 
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-4">接尸单位：</label> 
									<span>${corpseUnitOption}</span>
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-4">冷藏柜号：</label>
									<span>${freezerRecord.freezer.name }</span> 
									<input  readOnly="readOnly" style="border:none"  type="hidden" name="freezerId" id="freezerId"  value="${freezerRecord.freezerId }">
								</div>
								<div class="col-md-7 height-align">
									<label class="lab-4">挂账单位：</label>
									<span>${billUnitOption}</span> 
<!-- 									<select disabled="disabled" style="border:1	·none" style="width: 169px" class="list_select" name="billUnitId" id="billUnitId"> -->
<%-- 										${billUnitOption} --%>
<!-- 									</select> -->
<!-- 									<i class='white'></i>	 -->
								</div>
<!-- 								<div class="col-md-7"> -->
<!-- 									<label class="lable_c">冷藏时间：</label>  -->
<%-- 									<input data-provide="datetimepicker" data-date-format="yyyy-mm-dd hh:ii:ss" class="required list_select" id="freezerBeginDate" name="freezerBeginDate" value="<fmt:formatDate value="${freezerRecord.beginDate}" pattern="yyyy-MM-dd"/>"> --%>
<!-- 									<label >--</label> -->
<%-- 									<input data-provide="datetimepicker" data-date-format="yyyy-mm-dd hh:ii:ss" class="required list_select" id="freezerEndDate" name="freezerEndDate" value="${freezerRecord.endDate }">&nbsp;<i class="fa fa-circle text-red"></i> --%>
<!-- 								</div> -->
								<div class="col-md-5 height-align"> 
									<label class="lab-4">守灵室号：</label>
									<span>${mourningRecord.mourning.name}</span> 
									<input  readOnly="readOnly" style="border:none" type="hidden" name="mourningId" id="mourningId"  value="${mourningRecord.mourningId }">
<%-- 									<input  readOnly="readOnly" style="border:none" name="mourningName" id="mourningName" class=" list_select" readonly="readonly" value="${mourningRecord.mourning.name}"> --%>
<!-- 									<select disabled="disabled" style="width: 169px" class="list_select" >  -->
<%-- 										${mourningOption} --%>
<!-- 									</select> -->
<%-- 									<a href="${url}?method=editMourning&current_time=${mourningRecord.beginTime }" target="dialog" rel="myModal" class="btn btn-success" role="button" style="height: 28px;line-height:1">选择</a> --%>
								</div>
								<div class="col-md-7 height-align">
									<label class="lab-4">守灵时间：</label>
									<span><fmt:formatDate value="${mourningRecord.beginTime }" pattern="yyyy-MM-dd HH:mm"/></span> 
<%-- 									<input  readOnly="readOnly" style="border:none"  data-date-format="yyyy-mm-dd hh:ii:ss" class="required list_select" id="mourningBeginTime" name="mourningBeginTime" value='<fmt:formatDate value="${mourningRecord.beginTime }" pattern="yyyy-MM-dd HH:mm:ss"/>'> --%>
									<label >--</label>
									<span><fmt:formatDate value="${mourningRecord.endTime }" pattern="yyyy-MM-dd HH:mm"/></span>
<%-- 									<input  readOnly="readOnly" style="border:none"  data-date-format="yyyy-mm-dd hh:ii:ss" class="required list_select" id="mourningEndTime" name="mourningEndTime" value="<fmt:formatDate value="${mourningRecord.endTime }" pattern="yyyy-MM-dd HH:mm:ss"/>">&nbsp; --%>
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-4">告别厅号：</label>
									<span>${ farewellRecord.farewell.name}</span>
									<input  readOnly="readOnly" style="border:none" type="hidden" name="farewellId" id="farewellId"  value="${ farewellRecord.farewellId}">
<%-- 									<input  readOnly="readOnly" style="border:none" name="farewellName" id="farewellName" class=" list_select" readonly="readonly" value="${ farewellRecord.farewell.name}"> --%>
<!-- 									<select style="width: 169px" class="list_select" name="farewellId" id="farewellId"> -->
<%-- 										${farewellOption} --%>
<!-- 									</select> -->
<%-- 									<a href="${url}?method=editFarewell&current_time=${farewellRecord.beginDate }" target="dialog" target="dialog" rel="myModal" class="btn btn-success" role="button" style="height: 28px;line-height:1">选择</a> --%>
								</div>
								<div class="col-md-7 height-align">
									<label class="lab-4">告别时间：</label>
									<span><fmt:formatDate value="${farewellRecord.beginDate }" pattern="yyyy-MM-dd HH:mm"/></span>
<%-- 									<input  readOnly="readOnly" style="border:none"  data-date-format="yyyy-mm-dd hh:ii:ss"  class="required list_select"   onchange="" id="farewellBeginDate" name="farewellBeginDate" value="<fmt:formatDate value="${farewellRecord.beginDate }" pattern="yyyy-MM-dd HH:mm:ss"/>">&nbsp; --%>
								</div>
								<div class="col-md-5 height-align" id="furnace_div">
<!-- 									<select onclick="furnaceType_click()" style="width: 169px" class="list_select" name="furnaceTypeId" id="furnaceTypeId">  -->
<%-- 										${furnaceTypeOption} --%>
<!-- 									</select> -->
								</div>
								<div class="col-md-5 height-align">
									<label>火化预约时间：</label>
									<span><fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm"/></span> 
<%-- 									<input  readOnly="readOnly" style="border:none"   data-date-format="yyyy-mm-dd hh:mm:ss" class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm:ss"/>"> --%>
								</div>
								<div class="col-md-6 height-align" id="furnaceType_div">
									<label class="lab-4">火化炉号：</label>
									<span>${furnaceRecord.furnace.name }</span> 
									<input  readOnly="readOnly" style="border:none" type="hidden" name="furnaceId" id="furnaceId"  value="${furnaceRecord.furnaceId }">
<%-- 									<input  readOnly="readOnly" style="border:none" name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${furnaceRecord.furnace.name }"> --%>
<%-- 									<a href="${url}?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-success" role="button" style="height: 28px">选择</a> --%>
								</div>
							</div>
							<p class='p border-B'>车辆调度信息</p>
							<div class="box-body border-B">
<!-- 								<div class="col-md-12"> -->
<!-- 									<small class="pull-right">  -->
<!-- 										<button type="button" onclick="addCar()" class="btn btn-success" >添加</button> -->
<!-- 										<button type="button" onclick="delHtml('carSchedulRecordId')" class="btn btn-danger " >删除</button> -->
<!-- 									</small> -->
<!-- 								</div> -->
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="30px"><input   readOnly="readOnly" type="checkbox" class="checkBoxCtrl" group="carSchedulRecordId" /></th>
											<th width="200px">运输类型</th>
											<th width="200px">车辆类型</th>
											<th width="200px">运送时间</th>
											<th>备注</th>
										</tr>
									</thead>
									<!-- 车辆调度信息 -->
									<tbody id="car">
										
									</tbody>
								</table>
							</div>
							<p class='p border-B'>服务项目信息</p>
							<div class="box-body border-B">
<!-- 								<div class="col-md-12"> -->
<!-- 									<small class="pull-right">  
<%-- 										<a href="${url}?method=selectItem&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<%-- 										<a href="${url}?method=selectItems&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<!-- 										<button type="button" onclick="delHtml('service')" class="btn btn-danger " >删除</button> -->
<!-- 									</small>  -->
<!-- 								</div> -->
<!-- 								</div> -->
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="20px"><input disabled="disabled"  readOnly="readOnly"   type="checkbox" class="checkBoxCtrl" group="service" /></th>
											<th width="70px">助记码</th>
											<th width="240px">名称</th>
									        <th width="90px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="60px">数量</th>
									        <th width="60px">挂账</th>
									        <th width="80px">小计</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="service">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="sFont">
											0元
											</font>
											<div style="float: right; margin-right: 50%;">
												<font color='black' >合计：共</font>
												<font color='red' >${fn:length(service_list)}</font>
												<font color='black' >条记录</font>
											</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p class='border-B p'>丧葬用品信息</p>
							<div class="box-body border-B">
<!-- 								<div class="col-md-12"> -->
<!-- 									<small class="pull-right">  -->
<%-- 										<a href="${url}?method=selectItem&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<%-- 										<a href="${url}?method=selectItems&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<!-- 										<button type="button" onclick="delHtml('articles')" class="btn btn-danger " >删除</button> -->
<!-- 									</small> -->
<!-- 								</div> -->
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="20px"><input disabled="disabled"  readOnly="readOnly" type="checkbox" class="checkBoxCtrl" group="articles" /></th>
											<th width="70px">助记码</th>
											<th width="240px">名称</th>
									        <th width="90px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="60px">数量</th>
									        <th width="60px">挂账</th>
									        <th width="80px">小计</th>
									         <th>备注</th>
										</tr>
									</thead>
									<tbody id="articles">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="aFont">
											0元
											</font>
											<div style="float: right; margin-right: 50%;">
												<font color='black' >合计：共</font>
												<font color='red' >${fn:length(articles_list)}</font>
												<font color='black' >条记录</font>
											</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!--endprint3-->
				<!-- 基本项目申免 -->
						<!--startprint4-->
						<div class="tab-pane" id="c">
							<div class="box-body">
								<input  readOnly="readOnly"  type="hidden" name="baseReductionId" value="${baseReduction.id }">
								<div class="col-md-12" id="freePerson"></div>
								<div class="col-md-12" id="hardPerson"></div>
							</div>
							<div class="box-body">
<!-- 								<div class="col-md-12"> -->
<!-- 									<small class="pull-right">  -->
<!-- 										<button type="button" onclick="addBase()" class="btn btn-success" >添加</button> -->
<!-- 										<button type="button" onclick="delHtml('base')" class="btn btn-danger " >删除</button> -->
<!-- 									</small> -->
<!-- 								</div> -->
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="60px"><input disabled="disabled"  readOnly="readOnly" type="checkbox" class="checkBoxCtrl" group="base" /></th>
											<th width="100px">助记码</th>
											<th width="230px">名称</th>
									        <th width="120px">单价（元）</th>
									        <th width="100px">数量</th>
									        <th width="150px">总价</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="base">
										
									</tbody>
									<tbody>
										<tr>
											<td >合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="baseFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="col-md-12">
									<label>备注</label> 
									<textarea readonly="true" type="text" class="form-control" name="bComment" value="${baseReduction.comment }" style="width: 100%;"></textarea>
								</div>
							</div>
						</div>
						<!--endprint4-->
				<!-- 困难救助减免 -->
						<!--startprint5-->
						<div class="tab-pane" id="d">
							<div class="box-body">
								<div class="col-md-12" id="prove"></div>
								<div class="col-md-5 height-align">
								<input  readOnly="readOnly" style="border:none" type="hidden" name="hardReductionId" value="${hardReduction.id }">
								<label>是否特殊减免：</label>
								<span>${special }</span>								
<%-- 								<select disabled="disabled" style="border:none"  class='list_select' name="special" id="special">${specialOption}</select><i class='cheng'></i> --%>
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-5 height-align">申请日期：</label>
									<input   editable="false"  style="border:none"     id="createTime" name="createTime" value='<fmt:formatDate value="${hardReduction.createTime }" pattern="yyyy-MM-dd HH:mm"/>'>
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-5">申请人姓名：</label>
									<input  readOnly="readOnly" style="border:none" type="text"  name="applicant" value="${hardReduction.applicant }">
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-5">申请原因：</label>
									<input  readOnly="readOnly" style="border:none" type="text"  name="reason" value="${hardReduction.reason }">
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-5">联系电话：</label>
									<input  readOnly="readOnly" style="border:none" type="text"  name="phone" value="${hardReduction.phone }">
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-5">证明单位：</label>
									<span>${hard_proveUnitOption}</span>
<!-- 									<select style="border:none" disabled="disabled" class="list_select" name="hard_proveUnitId" > -->
<%-- 										${hard_proveUnitOption} --%>
<!-- 									</select> -->
<!-- 									<i class='white'></i> -->
								</div>
								<div class="col-md-12">
									<label class="lab-5">与死者关系：</label>
									<span>${hard_appellationOption}</span>
<!-- 									<select disabled="disabled" style="border:none" style="width: 169px" class="list_select" name="appellationId" id="appellationId">  -->
<%-- 									    ${hard_appellationOption} --%>
<!-- 									</select> -->
<!-- 									<i class='white'></i> -->
								</div>
							</div>
							<div class="box-body">
<!-- 								<div class="col-md-12"> -->
<!-- 									<small class="pull-right">  -->
<!-- 										<button type="button" onclick="addHard()" class="btn btn-success" >添加</button> -->
<!-- 										<button type="button" onclick="delHtml('hard')" class="btn btn-danger " >删除</button> -->
<!-- 									</small> -->
<!-- 								</div> -->
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="60px"><input disabled="disabled"  readOnly="readOnly" type="checkbox" class="checkBoxCtrl" group="hard" /></th>
											<th width="100px">助记码</th>
											<th width="230px">名称</th>
									        <th width="120px">单价（元）</th>
									        <th width="100px">数量</th>
									        <th width="150px">总价</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="hard">
										
									</tbody>
									<tbody>
										<tr>
											<td >合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="hardFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="col-md-12">
									<label>备注</label> 
									<textarea readonly="true" type="text" class="form-control" name="hComment" value="${hardReduction.comment }" style="width: 100%;"></textarea>
								</div>
							</div>
						</div>
						<!--endprint5-->
						<!-- 拍照存档 -->
						<div class="tab-pane" id="e">
						   <p class="p border-B">拍照存档</p>
							<div class="box-body border-B">
							
								<div class="col-md-12">
									<label>家属身份证图片</label>
									<div class='ID-pic ID-pic1' style="width:329px; height:249px" data-name='pName'>
										
										<img width=325px; src="${ commissionOrder.photoPic }" alt="images"/>
<%-- 										<img   src="${commissionOrder.eIdcardPic}"> --%>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
			<div  class="container-fluid">
			<div class='row margin-B'>
				<div class='col-md-6'>
				<div class='row'>
							<div class="col-md-6 height-align">
								<label>业务登记员：</label>
								<c:choose>
								    <c:when test="${commissionOrder.agentUser.name!=null}">
								    	${commissionOrder.agentUser.name}
								    </c:when>
								    <c:otherwise>
								   		${agentUser}
									</c:otherwise>
								</c:choose> 
								
							</div>
							<div class="col-md-6 height-align">
								<label>预约登记人：</label>
								<span>${commissionOrder.pbookAgentId }</span>
							</div>
							
							<div class="col-md-6 height-align">
								<label>到馆登记人：</label>
								<span>${commissionOrder.bookAgentId }</span>
							</div>
							<div class="col-md-6 height-align">
								<label >编号：</label> 
								<input type="text" class="list_select" readonly="readonly" style="border:none" value="${commissionOrder.code == null ? '由系统自动生成' : commissionOrder.code}">
							</div>
							<div class="col-md-6 height-align">
								<label >更新时间：</label> 
								${time }
							</div>
						</div>
<!-- 					<div class='row'> -->
<!-- 						<div class="col-md-6"> -->
<!-- 					<label>操作员：</label> -->
<%-- 					<c:choose> --%>
<%-- 					    <c:when test="${commissionOrder.agentUser.name!=null}"> --%>
<%-- 					    	${commissionOrder.agentUser.name} --%>
<%-- 					    </c:when> --%>
<%-- 					    <c:otherwise> --%>
<%-- 					   		${agentUser} --%>
<%-- 						</c:otherwise> --%>
<%-- 					</c:choose>  --%>
					
<!-- 				</div> -->
<!-- 				<div class="col-md-6"> -->
<!-- 					<label >编号：</label>  -->
<%-- 					<input  readOnly="readOnly" style="border:none" type="text"    value="${commissionOrder.code }"> --%>
<!-- 				</div> -->
<!-- 				<div class="col-md-6"> -->
<!-- 					<label >办理时间：</label>  -->
<%-- 					${time } --%>
<!-- 				</div> -->
<!-- 					</div> -->
				</div>
				
				<div class='col-md-6'>
					<small class="btns-hometab btns-print pull-right">
<%-- 	 	             <a href="javascript:myprint('${base_Is}','${hard_Is }')" class="btn  btn-color-ab47bc  btn-margin">打印</a>	 --%>
                     <c:choose>
                     	<c:when test="${ifCheck=='checkYes' }">
                     		<button type='button' onclick="checkTotal(this)"  class="btn btn-info btn-margin">审核</button>
                     		<a href="${url}?method=checkFlag&checkFlag=${Check_Yes }&id=${commissionOrder.id}" target="ajaxTodo" onclick="checkTotal()" warm="确认审核吗" class="hide" role="button">审核</a>
                     	</c:when>
                     	<c:when test="${ifCheck=='checkNo' }">
                     		<a href="${url }?method=checkFlag&checkFlag=${Check_No }&id=${commissionOrder.id}"  target="ajaxTodo" <c:if test="${payFlag==1 }">warm="该死者已收费,确认取消吗"</c:if> warm="确认取消吗" class="btn btn-danger btn-margin" style='padding:5px 0px' role="button">取消审核</a>
                     	</c:when>
                     </c:choose>
                     <c:choose>
                     	<c:when test="${adr=='A' }">
							<a href="commissionOrder.do" target='homeTab'  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a>
                     	</c:when>
                        <c:when test="${adr=='B' }">
							<a href="commissionOrderCheck.do" target='homeTab'  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a>
                      	</c:when>
                        <c:when test="${adr=='C' }">
							<a href="commissionOrderCheck.do?method=show" target='homeTab'  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a>
                       </c:when>
                       <c:when test="${adr=='D' }">
                       		<a href="login.do?method=main"  style="font-size:15px;padding:5px 0px" class="btn btn-default btn-margin">返回首页</a>
                       </c:when>
                       <c:when test="${adr=='DB' }">
						    <a href="funeralRecordList.do" target='homeTab'  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a>
                       </c:when>
                       <c:when test="${adr=='E' }">
						    <a href="taskFuneral.do" target='homeTab'  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a>
                       </c:when>
                       <c:when test="${adr=='F' }">
						    <a href="cremationChanges.do" target='homeTab'  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a>
                       </c:when>
                       <c:otherwise>
                       		<a href="" target='homeTab'  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a>
                       </c:otherwise>
                     </c:choose>
<%-- 						<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="certificateId" warm="确认禁用吗" class="btn btn-danger " role="button">重置</a> --%>
					</small>
				</div>
			</div>
				
			</div>
			</div>
		</form>
	</section>
	<script type="text/javascript">
	function checkTotal(ele) {
		if($('#hardTotalCheck').val()>=1200 && $('#hardCheckIs').val()!=1){
			toastr["warning"]("必须先通过困难减免的申请,才可通过审核");
			return false;
		}else{
			$(ele).next().click();
		}
	};
	</script>
</body>
