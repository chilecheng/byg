<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style>
#example2_wrapper td:hover{
	background-color:#f39c12;
	cursor:pointer;
}
</style>
<script>
		(function(){
			$('#example2_wrapper table tbody>tr td[data-num]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-num');
				switch (data){
					case '1':$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '2':
						$(this).css({'backgroundColor':'#FFFFFF','color':'#000000'});
						break;
					case '3':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
					case '4':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
				}
			})
		})();
</script>
<script type="text/javascript">

//重置
$("#reset").click(function(){
	$("#freezerId").val("");
	$("#freezerName").val("");
	//这里添加 删除对应的 灵堂 服务项目记录 402,405,403
	var types="no";
	for(var i=1;i<=sum;i++){
		var text=$("#helpCode_"+i).find("option:selected").text();
		if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="402" ||text==="405"||text==="403")){
			$("#checkbox_"+i).attr('checked','checked');
			types="yes";
			break;
		}
	}
	if(types==="yes"){
		delHtml("service");
	}
});

//解锁
$("#unlock").click(function(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'unlock'},
	    success: function () {
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
});

//单元格单击
function td_click(i,a){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'freezer_click',type:i,freezer:a},
	    success: function (json) {
// 	    	if(before!==''){
// 	    		$("#"+before).attr('checked','checked');
// 	    		delHtml(json.serviceList[0])
// 	    	}else{//保存之后，首次更改冷藏柜的情况
	    		//这里添加 删除对应的 灵堂 服务项目记录 402,405,403
	    		var arrTime=ndate($('#arriveTime').val());
	    		var startTime=ndate($('#arriveTime').val());
	    		var haveMourning="no";
	    		var num=0;
	    		if($('#mourningBeginTime').val()!=undefined && $('#mourningBeginTime').val()!=''){
			    	startTime=ndate($('#mourningBeginTime').val());
			    	haveMourning="yes";
			    	num=(startTime-arrTime)/(24*60*60*1000);
	    		}else if($('#farewellBeginDate').val()!=undefined && $('#farewellBeginDate').val()!=''){
	    			startTime=ndate($('#farewellBeginDate').val());
			    	num=(startTime-arrTime)/(24*60*60*1000);
	    		}else if($('#orderTime').val()!=undefined && $('#orderTime').val()!=''){
	    			startTime=ndate($('#orderTime').val());
			    	num=(startTime-arrTime)/(24*60*60*1000);
	    		}else{
	    			num=1
	    		}
		    	if(num<=0){
		    		num=1;
		    	}
	    		
	    		var types="no";
	    		var ice="no";
	    		for(var i=1;i<=sum;i++){
	    			var text=$("#helpCode_"+i).find("option:selected").text();
	    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="405"||text==="403")){//先判断排除水晶棺之后冷藏记录
	    				$("#checkbox_"+i).attr('checked','checked');
	    				types="yes";
	    				ice="yes";
	    				break;
	    			}
	    		}
	    		if(ice==="no"){//若以上没有冷藏记录，则在水晶棺记录中删除。 现发现还需要排除灵堂的水晶棺
	    			var iceCheck;
	    			var iceNumber=0;
	    			for(var i=1;i<=sum;i++){	    				
		    			var text=$("#helpCode_"+i).find("option:selected").text();
		    			if(($("#helpCode_"+i).attr('name')==='helpCode') && (text==="402")){
// 		    				$("#checkbox_"+i).attr('checked','checked');
		    				iceNumber=iceNumber+1;
		    				types="yes";
		    				if(text=="501"||text=="502"||text=="503"){
		    					haveMourning="yes";
		    				}
		    				console.log($("#number_"+i).val());
		    				console.log(num);
		    				console.log(($("#number_"+i).val())===num);
		    				if(($("#number_"+i).val())==num){
			    				iceCheck=i;
		    				}
		    				
// 		    				break;
		    			}
		    		}
	    			
	    			if(iceNumber<2){
	    				types="no";
	    			}
		    		if(types==="yes"){//有两条及以上水晶棺，则
		    			$("#checkbox_"+iceCheck).attr('checked','checked');
		    			delHtml("service");
		    			types="no";
		    		}else if(haveMourning==="no" && types==="no" && iceNumber==1){//两条以下水晶棺，并且没有灵堂则
		    			$("#checkbox_"+iceCheck).attr('checked','checked');
		    			delHtml("service");
		    			types="no";
		    		}
	    		}
	    		if(types==="yes"){
	    			delHtml("service");
	    		}
	    		
// 	    	}
	    	$("#freezerId").val(json.id);
	    	$("#freezerName").val(json.name);
	    	$("#myModal").modal("hide");
	    	//判断冰柜天数，守灵》告别》火化》无
	    	if($("#mourningBeginTime").val()!=='' && $("#mourningBeginTime").val()!= undefined){
		    	var endT=ndate($("#mourningBeginTime").val());
	    		var beginT=ndate($("#arriveTime").val());
	    		var longTime=(endT-beginT)/(24*60*60*1000);
	    		if(longTime<=0){
	    			longTime=1;
	    		}
	    		sum++;
		    	before='checkbox_'+sum;
				item(sum,json.serviceList[0],"",json.serviceList[1],json.serviceList[2],json.serviceList[3],json.serviceList[4],longTime,json.serviceList[6],longTime*json.serviceList[3],"",json.serviceList[9],json.serviceList[10])
				sortItem($('#service'));
				sTotal+=parseFloat(longTime*json.serviceList[3]);
				$("#sFont").html(sTotal+"元");
				iceType="mourning";
				iceHave="yes";
	    	}else if($("#farewellBeginDate").val()!=='' && $("#farewellBeginDate").val()!= undefined){//告别厅时间
	    		var endT=ndate($("#farewellBeginDate").val());
	    		var beginT=ndate($("#arriveTime").val());
	    		var longTime=(endT-beginT)/(24*60*60*1000);
	    		if(longTime<=0){
	    			longTime=1;
	    		}
	    		sum++;
		    	before='checkbox_'+sum;
				item(sum,json.serviceList[0],"",json.serviceList[1],json.serviceList[2],json.serviceList[3],json.serviceList[4],longTime,json.serviceList[6],longTime*json.serviceList[3],"",json.serviceList[9],json.serviceList[10])
				sortItem($('#service'));
				sTotal+=parseFloat(longTime*json.serviceList[3]);
				$("#sFont").html(sTotal+"元");
				iceType="farewell";
				iceHave="yes";
	    	}else if($("#orderTime").val()!=='' && $("#orderTime").val()!= undefined){//火化预约时间
	    		var endT=ndate($("#orderTime").val());
	    		var beginT=ndate($("#arriveTime").val());
	    		var longTime=(endT-beginT)/(24*60*60*1000);
	    		if(longTime<=0){
	    			longTime=1;
	    		}
	    		sum++;
		    	before='checkbox_'+sum;
				item(sum,json.serviceList[0],"",json.serviceList[1],json.serviceList[2],json.serviceList[3],json.serviceList[4],longTime,json.serviceList[6],longTime*json.serviceList[3],"",json.serviceList[9],json.serviceList[10])
				sortItem($('#service'));
				sTotal+=parseFloat(longTime*json.serviceList[3]);
				$("#sFont").html(sTotal+"元");
				iceType="order";
				iceHave="yes";
	    	} else{
		    	sum++;
		    	before='checkbox_'+sum;
				item(sum,json.serviceList[0],"",json.serviceList[1],json.serviceList[2],json.serviceList[3],json.serviceList[4],json.serviceList[5],json.serviceList[6],json.serviceList[7],"",json.serviceList[9],json.serviceList[10])
				sortItem($('#service'));
				sTotal+=parseFloat(json.serviceList[7]);
				$("#sFont").html(sTotal+"元");
				iceType="null";
				iceHave="yes";
	    	}
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
}

</script>
	<div class="modal-dialog" role="document" style="width:1200px;">
		<form action="${url}" id="form1" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="col-md-12">
					<ul class="la_ul">
						<li style="background: #fea625;"><div>锁定</div></li>
						<li style="background: #29b6f6;"><div>占用</div></lFronti>
						<li style="background: #e84e40;"><div>装修</div></li>
					</ul>
				</div>
			</div>
			<div class="box-body nomargin-B">
				<div class="nav-tabs-custom nomargin-B">
					<ul class="nav nav-tabs">
					<c:forEach items="${typeList }" var="u" varStatus="i">
						<c:if test="${i.index==0 }">
							<li class="active" style="width: 150px; text-align: center;"><a href="#type_${i.index }" data-toggle="tab" aria-expanded="true">${u }</a></li>
						</c:if>
						<c:if test="${i.index!=0 }">
							<li class="" style="width: 150px; text-align: center;"><a href="#type_${i.index }" data-toggle="tab" aria-expanded="false">${u}</a></li>
						</c:if>
					</c:forEach>
					</ul>
					<div class="tab-content">
						<c:forEach items="${list }" var="u" varStatus="i">
						<c:if test="${i.index==0 }">
							<div class="tab-pane active" id="type_${i.index }">
						</c:if>
						<c:if test="${i.index!=0 }">
							<div class="tab-pane" id="type_${i.index }">
						</c:if>
							<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
								<div class="row">
								<div class="col-sm-12">
									<table class="table table-bordered table-hover1" >
											<tr role="row">
											<c:forEach items="${u }" var="f" varStatus="j">
												<c:choose>
													<c:when test="${f.flag == IsFlag_Decrate }">
														<td  style="width: 44px;height:40px;" data-num='${f.flag}' title='维修中'>
															<a style="color:#fff" href="repairRecord.do?method=info&type=freezer&id=${f.id}&itemId=${f.freezer.id }&createUserId=${f.createUserId}" target="dialog" rel="myModal">
												${f.freezer.name}												
															</a>		
														</td>
													</c:when>
													<c:when test="${f.flag == IsFlag_Lock }">
														<td  style="width: 44px;height:40px;" data-num='${f.flag}' title='${f.order.name}'>
															${f.freezer.name}
														</td>
													</c:when>
													<c:when test="${f.flag == IsFlag_Yse }">
														<td style="width: 44px;height:40px" data-num='${f.flag}' title='${f.order.name}'>
															${f.freezer.name}
														</td>
													</c:when>
													<c:otherwise>
														<td onclick="td_click(${i.index},${j.index})"  style="width: 44px;height:40px;" data-num='${f.flag}'>${f.name}</td>
													</c:otherwise>
												</c:choose>
											<c:if test="${(j.index+1)%8==0&&j.index!=0}">
												</tr>
												<tr role="row">
											</c:if>
											</c:forEach>
											</tr>
									</table>
								</div>
								</div>
							</div>
						</div>
						</c:forEach>
					</div>
				<div class='btns-dialog'>
					<input type="button" class='btn btn-color-9E8273 btn-margin' data-dismiss="modal" id="reset" value="重置" />
					<input id="unlock" class='btn btn-info btn-margin' type="button" data-dismiss="modal"  value="解锁" />
				</div>
				</div>
			</div>
			</div>
		</form>
	</div>
	
													
