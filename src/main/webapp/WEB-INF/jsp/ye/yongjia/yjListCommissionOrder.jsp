<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">永嘉登记</div>
</section>
<style>
.table td a.light{
		width:50px;
		height:20px;
		background-color:#ff7043;
		text-align:center;
		line-height:20px;
		color:#fff;
		border-radius:4px;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>类型：</label>
							<select class="list_select nopadding-R" name="searchType" style="width: 52%;">
<!-- 								<option value='Code'  id="" class="">卡号	</option> -->
<!-- 								<option value='Name'  id="" class="" >死者姓名</option> -->
								${searchOption }
							</select>
						</div>
						<div class="col-md-8" style="margin-left: -94px">
							<label>查询值：</label> <input type="text"
								class="list_input input-hometab" name="searchVal"
								value="${searchVal}" placeholder="查询值">
						</div>	 
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-print btns-buy'>						
					 <a href="${url}?method=edit" data-target='homeTab'  class="btn btn-warning"  role="button">永嘉登记</a>
					
<%-- 					<a href="${url}?method=edit&id=" target="homeTab" checkName="checkboxId" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>		 --%>
				</samall>
				
			<%-- 	<small class="btns-buy pull-right">
						<a href="${url }?method=readOnly&checkFlag=${Check_Yes }&adr=A&ifCheck=checkYes&id=" target="homeTab" checkName="checkboxId" checkone="true"  data-checkpay='true' data-checkFlag=1 class="btn btn-info" role="button">审核</a>
						<a href="${url }?method=readOnly&checkFlag=${Check_No }&adr=A&ifCheck=checkNo&id=" target="homeTab" checkName="checkboxId"  checkone="true" data-checkpay='true' data-checkFlag=2 class="btn btn-danger " role="button">取消审核</a>
				</small> --%>
				
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th></th>
											<th>死者编号</th>
											<th>死者姓名</th>
											<th>卡号</th>
											<th>死者性别</th>
											<th>年龄</th>
											<th>到馆时间</th>
											<th>是否审核</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="9">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td><input type="radio" name="checkboxId" value="${u.id}"></td>
													<td> ${u.code} </td>
							    					<td><a href="${url }?method=readOnly&id=${u.id}&adr=A" target='homeTab'>${u.name }</a></td>
							    					<td>${u.cardCode }</td>
							    					<td>${u.sexName }</td>
							    					<td>${u.age }</td>
							    					<td><fmt:formatDate value="${u.arriveTime }" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td>
								    					<c:if test="${u.checkFlag==Check_No }">
								    					<font color="red" class='state1'>${u.checkFlagName }</font>
								    					</c:if>
								    					<c:if test="${u.checkFlag==Check_Yes }">
								    					<font color="green" class='state1'>${u.checkFlagName }</font>
								    					</c:if>
							    					</td>
							    					<c:if test="${u.payNumber >'0'}">
							    						<td>
									    					<a href="javasrcipt:;" class='light' style='background:#aaa; cursor:text'>修改</a>
							    						</td>
							    					</c:if>
							    					<c:if test="${u.payNumber <='0' }">
							    						<td>
									    					<a href="${url }?method=edit&id=${u.id}" class='light' target="homeTab">修改</a>
							    						</td>
							    					</c:if>
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$('[data-target="homeTab"]').click(function(event){
		var $this = $(this);
		var href=$this.attr("href");
		var checkName=$this.attr('checkName');
		var inputs=$("input[name='"+checkName+"']:checked");
		if(inputs.size()===0){
			$.ajax({
				url:href,
				async:false,
				type:"POST",
				dataType:"html",
				success:function(json){
					$("#page-content").html(json);
					$('body').animate({scrollTop: '0px'}, 100 ); 
					initTab();
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {  
					if(XMLHttpRequest.status==500){
						toastr["error"]("程序内部出现错误"); 
					}else if(XMLHttpRequest.status==404){
						toastr["error"]("页面不存在"); 
					}else{
						toastr["error"](XMLHttpRequest.status); 
					}
				}
			});
		}else if(inputs.size()===1){
			href+=inputs.eq(0).val();
			$.ajax({
				url:href,
				async:false,
				type:"POST",
				dataType:"html",
				success:function(json){
					$("#page-content").html(json);
					$('body').animate({scrollTop: '0px'}, 100 ); 
					initTab();
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {  
					if(XMLHttpRequest.status==500){
						toastr["error"]("程序内部出现错误"); 
					}else if(XMLHttpRequest.status==404){
						toastr["error"]("页面不存在"); 
					}else{
						toastr["error"](XMLHttpRequest.status); 
					}
				}
			});
		}else{
			toastr["warning"]("选择总数不能大于1"); 
		}
		event.preventDefault();
	})
</script>
