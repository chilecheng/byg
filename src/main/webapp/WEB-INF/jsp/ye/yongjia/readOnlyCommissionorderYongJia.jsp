<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<!-- <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script> -->

<script type="text/javascript">
var preConfigList = new Array(); var previewJson = new Array(); 
var sum=0;//标识
var sTotal=0;//服务项目合计总金额	
var aTotal=0;//丧葬用品合计总金额
var baseTotal=0;//基本减免合计总金额
var hardTotal=0;//困难减免合计总金额
function sortItem(target){
	var trs=target.find('tr');
	
	//trs=Array.prototype.slice.call(trs);
	trs.sort(function(a,b){
		var type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		var type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		if(type1==type2){
			type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
			type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
		}
		return type2>type1?-1:type2<type1?1:0;
	})
	target.html(trs);
}
//收费项目方法
function item(sum,name,id,itemId,helpCode,pice,typeId,number,bill,total,comment,findexFlag,indexFlag){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input  readOnly='readOnly' type='hidden' name="+name+"_id value="+id+">";
	str+="<input  readOnly='readOnly'  data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"' type='checkbox' id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select disabled='true'  class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name='helpCode' id='helpCode_"+sum+"'> "+helpCode+"</select><i class='che'></i></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select disabled='true'  class='list_table' onchange=itemName("+sum+",'"+name+"') name='itemId' id='itemId_"+sum+"'> "+itemId+"</select><i class='che'></i></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input   type='text' readonly='readonly' class='list_table' name=pice id='pice_"+sum+"' value="+parseFloat(pice)+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select disabled='true'  class='list_table' onchange=itemType("+sum+",'"+name+"') name='typeId' id='typeId_"+sum+"'>"+typeId+"</select><i class='che'></i></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input  readOnly='readOnly' type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=number id='number_"+sum+"' value="+number+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select disabled='true'  class='list_table' name=bill id=bill_"+sum+"> "+bill+"</select><i class='che'></i></td>";
	str+="<td style='text-align: center; vertical-align: middle; '><input  readOnly='readOnly' type='text' readonly='readonly' name=total id=total_"+sum+" class='list_table' value="+parseFloat(total)+"></td>";
	str+="<td><textarea readonly='true' type='text' class='list_table' name='comment'  id='comment_'"+sum+" >"+comment+"</textarea></td></tr>";
	$("#"+name).append(str);
}


//车辆调度方法
function car(sum,id,transportTypeOption,carTypeOption,pickTime,comment){
	//alert(1);
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name='csr_id' value="+id+" >";
	str+="<input disabled='true' type='checkbox' name='carSchedulRecordId' ></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select disabled='true' class='list_table' name=transportTypeId  id=transportTypeId_"+sum+"> "+transportTypeOption+"</select><i class='cheng'></i></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select disabled='true'disabled='true'  class='list_table' name=carTypeId id=typeId_"+sum+" >"+carTypeOption+"</select><i class='cheng'></i></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input editable='fasle' data-provide='datetimepicker'  style='width: 180px' data-date-format='yyyy-mm-dd hh:ii:ss' class='list_table' id=dTime_"+sum+" name=pickTime ></td>";
	str+="<td><textarea readOnly='readOnly' type='text' class='list_table' name=carComment id=comment_"+sum+"  >"+comment+"</textarea></td></tr>";
	$("#car").append(str);
	$("#dTime_"+sum).val(pickTime);
}

//初始化
$(document).ready(function(){
	//火化炉
	$("#furnaceType_div").hide();
	//是否基本或困难减免
	$("#baseLi").hide();
	$("#hardLi").hide();
	if(${base_Is==Is_Yes}){
		$("#baseLi").show();
	}
	if(${hard_Is==Is_Yes}){
		$("#hardLi").show();
	}
	
	//添加或修改
	if('${id}'==""){
		//初始化服务项目
		<c:forEach var="u" items='${serviceList}'>
			sum++;
			item(sum,"${u[0]}","","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","","${u[7]}","${u[8]}")
			sTotal+=parseFloat("${u[6]}")
			$("#sFont").html(sTotal+"元");
		</c:forEach>
		//初始丧葬用品项目
		<c:forEach var="u" items='${articlesList}'>
			sum++;
			item(sum,"${u[0]}","","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","","${u[7]}","${u[8]}")
			aTotal+=parseFloat("${u[6]}")
			$("#aFont").html(aTotal+"元");
		</c:forEach>
		//初始化火化炉按钮
		var fun="<label class=lab-6>火化炉类型：</label> "
		<c:forEach var="u" items='${furnaceList}'>
			<c:choose>
				<c:when test="${u[0]==3}">
					fun+="<input type=radio name=radio onclick=radio_click('${u[0]}') checked value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
				</c:when>
				<c:otherwise>
					fun+="<input type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
				</c:otherwise>
			</c:choose>
		</c:forEach>
		$("#furnace_div").html(fun);  
		
	}else{
		//车辆调度
		<c:forEach var="u" items='${car_List}'>
			sum++;
			car(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}");
		</c:forEach>
		//服务项目
		<c:forEach var="u" items='${service_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			sTotal+=parseFloat('${u[8]}');
			$("#sFont").html(sTotal+"元");
		</c:forEach>
		//葬用品项目
		<c:forEach var="u" items='${articles_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			aTotal+=parseFloat('${u[8]}');
			$("#aFont").html(aTotal+"元");
		</c:forEach>	
		//火化炉按钮
		var fun="<label class=lab-6>火化炉类型：</label> "
		fun+="<input  readOnly=readOnly style='border:none' type=text value='${tname}'>";
		<c:forEach var="u" items='${furnace_list}'>
// 		fun+="<input  disabled  ${u[2]} type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
			if("${u[2]}"!=""&&${u[0]==furnace_ty}){
				$("#furnaceType_div").show();
			}
// 	</c:forEach>
	$("#furnace_div").html(fun);
	sortItem($('#articles'));
	sortItem($('#service'));
	sortItem($('#hard'));
	sortItem($('#base'));
	}
	
});

//死亡类型改变
function deadTypeChange(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'deadTypeChange',id:$("#deadTypeId").val()},
	    success: function (str) {
	    	$("#deadReasonId").html(str);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 


//项目类别改变
function itemType(id,type){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemTypeChange',id:$("#typeId_"+id).val()},
	    success: function (json) {
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
    		}
    		$("#pice_"+id).val(json.pice)
	    	$("#total_"+id).val(json.pice)
	    	$("#number_"+id).val(1)
	    	$("#itemId_"+id).html(json.str);
    		$("#comment_"+id).val("")
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 
//项目名称改变
function itemName(id,type){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice)
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice)
    		}
    		if(type=="base"){
    			baseTotal=baseTotal-$("#total_"+id).val();
    			baseTotal+=json.pice;
    			var b=baseTotal.toFixed(2);
				$("#baseFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice)
    		}
    		if(type=="hard"){
    			hardTotal=hardTotal-$("#total_"+id).val();
    			hardTotal+=json.pice;
    			var b=hardTotal.toFixed(2);
				$("#hardFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice)
    		}
    		$("#pice_"+id).val(json.pice);
    		$("#helpCode_"+id).html(json.str);
	    	$("#number_"+id).val(1);
	    	$("#comment_"+id).val("");
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 


 


//数目改变
function numbers(id,type){
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	if(a!=number){//表示是不是数字
		toastr["warning"]("请输入正确的数量！");
	}else{
		var pice=$("#pice_"+id).val();
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			var jg=sTotal.toFixed(2);
			$("#sFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val(pice*number);
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=pice*number;
			var jg=aTotal.toFixed(2);
			$("#aFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val(pice*number);
		}
		if(type=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			baseTotal+=pice*number;
			var jg=baseTotal.toFixed(2);
			$("#baseFont").html(parseFloat(jg)+"元");
	    	$("#total_"+id).val(pice*number)
		}
		if(type=="hard"){
			hardTotal=hardTotal-$("#total_"+id).val();
			hardTotal+=pice*number;
			var jg=hardTotal.toFixed(2);
			$("#hardFont").html(parseFloat(jg)+"元");
	    	$("#total_"+id).val(pice*number)
		}
		
	}
} 


//添加车辆信息调度
function addCar() {
	sum++;
// 	alert(1)
	car(sum,"","${transportTypeOption}","${carTypeOption}","","")
}



//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		if(str=="articles"){
			aTotal=aTotal-$("#total_"+id).val();
			$("#aFont").html(aTotal+"元");
		}
		if(str=="service"){
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		}
		if(str=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			$("#baseFont").html(baseTotal+"元");
		}
		if(str=="hard"){
			hardTotal=hardTotal-$("#total_"+id).val();
			$("#hardFont").html(hardTotal+"元");
		}
		$(this).parent().parent().remove();
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}





//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   { //author: meizz   
	var o = {   
	 "M+" : this.getMonth()+1,                 //月份   
	 "d+" : this.getDate(),                    //日   
	 "h+" : this.getHours(),                   //小时   
	 "m+" : this.getMinutes(),                 //分   
	 "s+" : this.getSeconds(),                 //秒   
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	 "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
	 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
	 if(new RegExp("("+ k +")").test(fmt))   
	fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
}  
</script>
<style>

.container-fluid { 
	background: #fff;
} 
.list_table {
	border-radius: 6px;
	border: 1px rgb(169, 169, 169) solid;
	width: 100%;
	line-height: 30px;
	height: 30px;
	padding: 0 8px;
}
.white{
	display:inline-block;
	vertical-align:center;
	width:20px;
	height:20px;
	margin-left:-30px;
	background:#fff;
}
.cheng{
  display:inline-block;
	vertical-align:center;
	width:25px;
	height:20px;
	margin-left:-29px;
	background:#fff;
}
.che{
  display:inline-block;
	vertical-align:center;
	width:16px;
	height:13px;
	margin-left:-18px;
	background:#fff;
}
</style>

</head>
<body>
	<section class="content">
		<form action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		<input  readOnly="readOnly" type="hidden" name="method" value="${method }" >
		<input  readOnly="readOnly" type="hidden" id="id" name="id" value="${commissionOrder.id}">
		<input  readOnly="readOnly" type="hidden" id="name" value="${commissionOrder.name }">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab" aria-expanded="true">基本信息</a></li>
						<li class="" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" aria-expanded="false">业务信息</a></li>
					</ul>
					<div class="tab-content" style='padding:0px'>
			
			<!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						<p class='p border-B'>死者信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者姓名：</label>
									<input readOnly="readOnly"  style="border:none"   type="text"  class="required list_select" name="name" value="${commissionOrder.name }">&nbsp;
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死者地址：</label> 
									<input  readOnly="readOnly"  style="width:330px;border:none"  type="text" class="required list_select" name="dAddr" value="${commissionOrder.dAddr }">&nbsp;
<!-- 									<i class="fa fa-circle text-red"></i> -->
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件类型：</label>
									<select   disabled="disabled" style="border:none;"  style="width: 169px" class="list_select" name="certificateId" id="certificateId">
										${certificateOption}
									</select>
									<i class='white'></i>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>接尸地址：</label> 
									<input  readOnly="readOnly" style="width: 330px;border:none"   type="text" class="required list_select" name="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件号码：</label> 
									<input  readOnly="readOnly" style="border:none"    type="text" class="required list_select" name="certificateCode" value="${commissionOrder.certificateCode }">&nbsp;
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡类型：</label> 
									<select  disabled="disabled" style="border:none"  style="width: 169px" class="list_select" name="deadTypeId" id="deadTypeId" onchange="deadTypeChange()">
										${deadTypeOption}
									</select>
								    <i class='white'></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者年龄：</label> 
									<input  readOnly="readOnly" style="border:none"   type="text" class="required list_select" name="age" value="${commissionOrder.age }">&nbsp;
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡原因：</label> 
									<select disabled="disabled"  style="border:none" style="width: 169px" class="list_select" name="deadReasonId" id="deadReasonId">
										${deadReasonOption}
									</select>
									<i class='white'></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者性别：</label> 
									<select  disabled="disabled" style="border:none" style="width: 169px" class="list_select" name="sex" id="sex">
										${sexOption}
									</select>
									<i class='white'></i>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡日期：</label> 
									<input  readOnly="readOnly" style="border:none"    data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="dTime" name="dTime" value='<fmt:formatDate value="${commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/>'>
								</div>
								<div class="col-md-5">
									<label  class='lab-2'>民族：</label> 
									<span>${commissionOrder.dNationId}</span> 
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死者地区：</label>
									<span>${deadArea }</span> 
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-2'>卡号：</label> 
									<span>${commissionOrder.cardCode }</span> 
									<%-- <input  readOnly="readOnly" style="border:none"  type="text" class="list_select" name="cardCode" value="${commissionOrder.cardCode }"> --%>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>证明单位：</label> 
									<select disabled="disabled" style="border:none" style="width: 169px" class="list_select" name="proveUnitId" id="proveUnitId">
										${proveUnitOption}
									</select> 
									<i class='white'></i>
									<input  readOnly="readOnly" style="border:none"  style="width: 200px" type="text" name="proveUnitContent" class="list_select" value="${commissionOrder.proveUnitContent }">
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死亡证明：</label> 
									<select disabled="disabled" style="border:none" style="width: 169px" class="list_select" name="dFlag" id="dFlag">
										${dFlagOption}
									</select>
									<i class='white'></i>
								</div>
								<div class="col-md-12">
									<label>死者身份证图片</label> 
									<div class='ID-pic' data-name='name'>
										<span>身份证图片</span>
										<img width=325px;  src="${commissionOrder.dIdcardPic }">
									</div>
									
<%-- 									<input type="hidden" name="filename" value="${commissionOrder.dIdcardPic }"> --%>
								</div>
							</div>
							<p class='p border-B'>家属/经办人信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label class="lab-3">姓名：</label> 
									<input  readOnly="readOnly" style="border:none"  type="text" class="required list_select" name="fName" value="${commissionOrder.fName }">&nbsp;
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-5">与死者关系：</label> 
									<select disabled="disabled" style="border:none" style="width: 169px" class="list_select" name="fAppellationId" id="fAppellationId" disabled="disabled"> 
									    ${fAppellationOption}
									</select>
									<i class='white'></i>
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-1">联系号码：</label> 
									<input  readOnly="readOnly"  style="border:none"  type="text" class="required list_select" name="fPhone" value="${commissionOrder.fPhone }">&nbsp;
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-3">住址：</label>
									<input  readOnly="readOnly" style="border:none"  type="text" class="required list_select" name="fAddr" value="${commissionOrder.fAddr }">&nbsp;
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-3">单位：</label> 
									<input  readOnly="readOnly" style="border:none"  type="text" class="required list_select" name="fUnit" value="${commissionOrder.fUnit }">&nbsp;
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-5">身份证号码：</label> 
									<input  readOnly="readOnly" style="border:none"  type="text" class="required list_select" name="fCardCode" value="${commissionOrder.fCardCode }">&nbsp;
								</div>
								<div class="col-md-5 height-align">
									<label>业务审核状态：</label>
									<c:if test="${checkFlag==Check_No }">
			    						<font color="red">${commissionOrder.checkFlagName}</font>
			    					</c:if>
			    					<c:if test="${checkFlag==Check_Yes }">
			    						<font color="green">${commissionOrder.checkFlagName}</font>
			    					</c:if>
								</div>
								<div class="col-md-12">
									<label>家属身份证图片</label>
									<div class='ID-pic' data-name='fName'>
										<span>身份证图片</span>
										<img width=325px; src="${ commissionOrder.eIdcardPic }" alt="images"/>
<%-- 										<img   src="${commissionOrder.eIdcardPic}"> --%>
									</div>
									       
								</div>
							</div>
						</div>
				<!-- 业务信息  -->
						<div class="tab-pane" id="b">
							<p class='p border-B'>业务调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label class="lab-4">到馆时间：</label> 
									<input  readOnly="readOnly" style="border:none"   data-date-format="yyyy-mm-dd hh:ii:ss" class="required list_select" id="arriveTime" name="arriveTime" value='<fmt:formatDate value="${commissionOrder.arriveTime }" pattern="yyyy-MM-dd HH:mm"/>'>&nbsp;
								</div>
								<div class="col-md-5 height-align">
									<label class="lab-4">接尸单位：</label> 
									<select disabled="disabled" style="border:none" style="width: 169px" class="list_select" name="corpseUnitId" id="corpseUnitId">
										${corpseUnitOption}
									</select>
									<i class='white'></i>
								</div>

							<%-- <div class="col-md-7">
									<label class="lab-4">挂账单位：</label> 
									<span> 	${billUnitOption} </span>
									<select disabled="disabled" style="border:none" style="width: 169px" class="list_select" name="billUnitId" id="billUnitId">
										${billUnitOption}
									</select>
									<i class='white'></i>	
							</div> --%>
								<div class="col-md-5 height-align">
									<label class='lab-6'>火化预约时间：</label> 
										<span><fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm"/></span> 
								<%-- 	<input data-id="reservation" readOnly='readOnly'  class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm:ss"/>"> --%>
									<%-- <input data-id="reservation" class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm:ss"/>"> --%>
								</div>
								<div class="col-md-6 height-align" id="furnaceType_div">
									<label  class='lab-2'>火化炉号：</label>
										<span>${furnaceRecord.furnace.name }</span> 
								<%-- 	<input type="hidden" name="furnaceId" id="furnaceId"  value="${furnaceRecord.furnaceId }">
									<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${furnaceRecord.furnace.name }">
									<a href="commissionOrder.do?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a> --%>
								</div>

				
							</div>
						<!-- 	<p class='p border-B'>车辆调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-12">
									<small class="pull-right"> 
										<button type="button" onclick="addCar()" class="btn btn-success" >添加</button>
										<button type="button" onclick="delHtml('carSchedulRecordId')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" >
									<thead>
										<tr>
											<th width="30px"><input   readOnly="readOnly" type="checkbox" class="checkBoxCtrl" group="carSchedulRecordId" /></th>
											<th width="200px">运输类型</th>
											<th width="200px">车辆类型</th>
											<th width="200px">运送时间</th>
											<th>备注</th>
										</tr>
									</thead>
									车辆调度信息
									<tbody id="car">
										
									</tbody>
								</table>
							</div> -->
							<p class='p border-B'>服务项目信息</p>
							<div class="box-body border-B">
								<div class="col-md-12">
									<small class="pull-right"> 
<%-- 										<a href="${url}?method=selectItem&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<%-- 										<a href="${url}?method=selectItems&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<!-- 										<button type="button" onclick="delHtml('service')" class="btn btn-danger " >删除</button> -->
									</small>
								</div>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="30px"><input  readOnly="readOnly"   type="checkbox" class="checkBoxCtrl" group="service" /></th>
											<th width="100px">助记码</th>
											<th width="200px">名称</th>
									        <th width="100px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="80px">数量</th>
									        <th width="80px">挂账</th>
									        <th width="100px">小计</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="service">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="sFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p class='border-B p'>丧葬用品信息</p>
							<div class="box-body border-B">
								<div class="col-md-12">
									<small class="pull-right"> 
<%-- 										<a href="${url}?method=selectItem&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<%-- 										<a href="${url}?method=selectItems&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<!-- 										<button type="button" onclick="delHtml('articles')" class="btn btn-danger " >删除</button> -->
									</small>
								</div>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="30px"><input  readOnly="readOnly" type="checkbox" class="checkBoxCtrl" group="articles" /></th>
											<th width="100px">助记码</th>
											<th width="200px">名称</th>
									        <th width="100px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="80px">数量</th>
									        <th width="80px">挂账</th>
									        <th width="100px">小计</th>
									         <th>备注</th>
										</tr>
									</thead>
									<tbody id="articles">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="aFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>

							</div>
						</div>
			
					</div>
				</div>
			</div>
			</div>
			<div class="box-body">
			<div  class="container-fluid">
				<div class='row'>
					<div class='col-md-6'>
				<!-- 	<div class='col-md-4'> -->
					<%-- 	<div class="col-md-5">
							<label class='lab-5'>预约登记人：</label>
							<span>${commissionOrder.pbookAgentId }</span>
						</div> --%>
						<div class="col-md-5 height-align">
							<label class='lab-5'>到馆登记人：</label>
											<span>${commissionOrder.bookAgentId }</span>
						</div>
						<div class="col-md-7 height-align">
							<label  class='lab-3'>编号：</label> 
							<input  readOnly="readOnly" style="border:none" type="text"    value="${commissionOrder.code }">
						</div>
						<div class="col-md-10">
							<label  class='lab-1'>办理时间：</label> 
							${time }
						</div>
					</div>
	<!-- 				<div class='col-md-4'>
						<small class="btns-hometab pull-right"> 
         
 						  <a href="yjcommissionOrder.do" target='homeTab'  rel="myModal"  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a> 
						</small>  
					</div> -->
					
					<div class='col-md-6' style='padding-bottom:5px;'>
					<small class="btns-hometab btns-print pull-right">
	 	             <a href="javascript:myprint()" class="btn  btn-color-ab47bc  btn-margin">打印</a>	
                    <%--  <c:choose>
                     	<c:when test="${ifCheck=='checkYes' }">
                     		<button type='button' onclick="checkTotal(this)"  class="btn btn-info btn-margin">审核</button>
                     		<a href="commissionOrder.do?method=checkFlag&checkFlag=${Check_Yes }&id=${commissionOrder.id}" target="ajaxTodo" onclick="checkTotal()" warm="确认审核吗" class="hide" role="button">审核</a>
                     	</c:when>
                     	<c:when test="${ifCheck=='checkNo' }">
                     		<a href="commissionOrder.do?method=checkFlag&checkFlag=${Check_No }&id=${commissionOrder.id}"  target="ajaxTodo"  warm="确认取消吗" class="btn btn-danger btn-margin" style='padding:5px 0px' role="button">取消审核</a>
                     	</c:when>
                     </c:choose> --%>

					  <a href="yjcommissionOrder.do" target='homeTab'  rel="myModal"  role="button" style="font-size:15px;" class="btn btn-default btn-margin">返回</a> 
					</small>
				</div>
					
					
					
					
				</div>
				
				



			</div>
			</div>
		</form>
	</section>
	<!-- 	<script type="text/javascript">
	function checkTotal(ele) {
			$(ele).next().click();
	};
	</script> -->
</body>
