<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<head>
<script type="text/javascript">
var preConfigList = new Array(); var previewJson = new Array(); 
var sum=0;//标识
var sTotal=0;//服务项目合计总金额	
var aTotal=0;//丧葬用品合计总金额
var state='${id}';
var funBefore='';
var before='';
timeMunite();

$(function(){
	timeRange();
	timeMunite();
	//火化炉
	$("#furnaceType_div").hide();
	if("${specialBusinessOption}"=="" || "${specialBusinessOption}"==null){
		$("#specialBusinessType").hide();
	}
	
	//添加或修改
	if('${id}'==""){
		//初始化服务项目
		<c:forEach var="u" items='${serviceList}'>
			sum++;
			item(sum,"${u[0]}","","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","","${u[9]}","${u[10]}")
			sTotal+=parseFloat("${u[7]}")
			$("#sFont").html(sTotal+"元");
		</c:forEach>
		//初始丧葬用品项目
		<c:forEach var="u" items='${articlesList}'>
			sum++;
			item(sum,"${u[0]}","","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","","${u[9]}","${u[10]}")
			aTotal+=parseFloat("${u[7]}")
			$("#aFont").html(aTotal+"元");
		</c:forEach>
		//初始免费对象类别
		var fStr="<label>免费对象类别：</label>";
		<c:forEach var="u" items='${freePersonList}'>
			fStr+="<input type='checkbox'  name='freePersonId' value='${u.id }'>";
			fStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#freePerson").html(fStr);
		</c:forEach>
		//初始重点救助对象类别
		var hStr="<label>重点救助对象类别：</label>";
		<c:forEach var="u" items='${hardPersonList}'>
			hStr+="<input type='checkbox'  name='hardPersonId' value='${u.id }'>";
			hStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#hardPerson").html(hStr);
		</c:forEach>
		//初始证件类型
		var pStr="<label class='lab-6'>证件类型：</label>";
		<c:forEach var="u" items='${certificateList}'>
			pStr+="<input type='checkbox'  name='proveIds' value='${u.certificateId }'>";
			pStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#prove").html(pStr);
		</c:forEach>
		
		
		//初始化火化炉按钮
		var fun="<label class=lab-6>火化炉类型：</label> "
		<c:forEach var="u" items='${furnaceList}'>
			<c:choose>
				<c:when test="${u[0]==1}">
					fun+="<input type=radio name=radio onclick=radio_click('${u[0]}') checked value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
					$("#specialBusinessType").hide();
				</c:when>
				<c:otherwise>
					fun+="<input type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
				</c:otherwise>
			</c:choose>
		</c:forEach>
		$("#furnace_div").html(fun);
		
		//初始化基本减免项目
		<c:forEach var="u" items='${baselist}'>
			sum++;
			var pice=parseFloat('${u[5]}');
			reduction(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}")
			baseTotal+=pice;
			$("#baseFont").html(baseTotal+"元");
		</c:forEach>
	
	}else{
		
		//服务项目
		<c:forEach var="u" items='${service_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			
			sTotal+=parseFloat('${u[8]}');
			$("#sFont").html(sTotal+"元");
		</c:forEach>
		//葬用品项目
		<c:forEach var="u" items='${articles_list}'>
			sum++;
			item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}","${u[9]}","${u[10]}","${u[11]}")
			aTotal+=parseFloat('${u[8]}');
			$("#aFont").html(aTotal+"元");
		</c:forEach>
	
	//困难减免
		//证件类型
		var pStr="<label class='lab-6'>证件类型：</label>";
		<c:forEach var="u" items='${prove_list}'>
			pStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			pStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#prove").html(pStr);
		</c:forEach>
	
		//火化炉按钮
// 		var fun="<label class=lab-6>火化炉类型：</label> "
// 		<c:forEach var="u" items='${furnace_list}'>
// 			fun+="<input ${u[2]} type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
// 			if("${u[2]}"!=""&&${u[0]==furnace_ty}){
// 				$("#furnaceType_div").show();
// 				$("#furnaceType_comment").hide();
// 			}
// 		</c:forEach>
// 		$("#furnace_div").html(fun);
		//火化炉按钮
		var fun="<label class=lab-6>火化炉类型：</label> "
		var flagType="x";
		<c:forEach var="u" items='${furnace_list}'>
			fun+="<input ${u[2]} type=radio name=radio onclick=radio_click('${u[0]}') value='${u[0]}'>${u[1]}&nbsp;&nbsp;&nbsp;&nbsp"
			if("${u[2]}"!=""){
				flagType="y";
			}
			if("${u[2]}"!=""&& ${u[0]==furnace_ty}){
				$("#furnaceType_div").show();
				$("#specialBusinessType").hide();
			}else if("${u[2]}"!=""&& ${u[0]=="1"}){
				$("#specialBusinessType").hide();
			}
		</c:forEach>
		$("#furnace_div").html(fun);
		if(flagType=="x"){
			$('[name="radio"][value="1"]').attr('checked','checked');
			$("#specialBusinessType").hide();
		}
		
	}
	sortItem($('#articles'));
	sortItem($('#service'));
});
//根据年龄来判断火化收费
function changeAge(age){
	var ageNumber=$(age).val();
	//儿童遗体火化费
	if(ageNumber<14){
		for(var i=1;i<=sum;i++){
			var text=$("#helpCode_"+i).find("option:selected").text();
			if(($("#checkbox_"+i).attr('name')==='service') && text==="301"){
				$("#helpCode_"+i+" option:selected").removeAttr('selected').siblings(':contains("302")').attr('selected','true');
// 				$("#helpCode_"+i+" option:selected").parent().change();
				itemHelpCode(i,"service");
			}
		}
	}
	if(ageNumber>=14){//成人遗体火化费
		for(var i=1;i<=sum;i++){
			var text=$("#helpCode_"+i).find("option:selected").text();
			if(($("#checkbox_"+i).attr('name')==='service') && text==="302"){
				$("#helpCode_"+i+" option:selected").removeAttr('selected').siblings(':contains("301")').attr('selected','true');
// 				$("#helpCode_"+i+" option:selected").parent().change();
				itemHelpCode(i,"service");
			}
		}
// 		$('option:selected:contains("302")').removeAttr('selected').siblings(':contains("301")').attr('selected','selected');
// 		$('option:selected:contains("301")').parent().change();
	}
}
//火化炉类型单击
function radio_click(type) {
	if (type==${furnace_ty}) {
		$("#furnaceType_div").show();
		$("#specialBusinessType").hide();
   		var types="no";
   		for(var i=1;i<=sum;i++){
   			var text=$("#helpCode_"+i).find("option:selected").text();
   			if(($("#helpCode_"+i).attr('name')==='helpCode') && (text==="301" ||text==="302"||text==="420")){
   				$("#checkbox_"+i).attr('checked','checked');
   				types="yes";
   			}
   		}
   		if(types==="yes"){
   			delHtml("service");
   		}
		sum++;
		item(sum,"${funListTY[0]}","","${funListTY[1]}","${funListTY[2]}","${funListTY[3]}","${funListTY[4]}","${funListTY[5]}","${funListTY[6]}","${funListTY[7]}","","${funListTY[9]}","${funListTY[10]}")
		sortItem($("#service"));
		sTotal+=parseFloat("${funListTY[7]}");
		$("#sFont").html(sTotal+"元");
		sum++;
		item(sum,"${funList[0]}","","${funList[1]}","${funList[2]}","${funList[3]}","${funList[4]}","${funList[5]}","${funList[6]}","${funList[7]}","","${funListTY[9]}","${funListTY[10]}")
		sortItem($("#service"));
		sTotal+=parseFloat("${funList[7]}");
		$("#sFont").html(sTotal+"元");
		var age=$("#age").val();
		var ageNumber=Number(age);
		if(ageNumber<14){
			$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("302")').attr('selected','selected');
			itemHelpCode(sum,"service");
		}else{
			$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("301")').attr('selected','selected');
			itemHelpCode(sum,"service");
		}
	}else{
		$("#furnaceType_div").hide();
		if(type!=='3'){
			$("#specialBusinessType").hide();
    		var types="no";
    		for(var i=1;i<=sum;i++){
    			var text=$("#helpCode_"+i).find("option:selected").text();
    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="301" ||text==="302"||text==="420")){
    				$("#checkbox_"+i).attr('checked','checked');
    				types="yes";
    			}
    		}
    		if(types==="yes"){
    			delHtml("service");
    		}
			sum++;
			funBefore='checkbox_'+sum;
			item(sum,"${funList[0]}","","${funList[1]}","${funList[2]}","${funList[3]}","${funList[4]}","${funList[5]}","${funList[6]}","${funList[7]}","","${funListTY[9]}","${funListTY[10]}")
			sortItem($("#service"));
			sTotal+=parseFloat("${funList[7]}");
			$("#sFont").html(sTotal+"元");
			var age=$("#age").val();
			var ageNumber=Number(age);
			if(ageNumber<14){
				$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("302")').attr('selected','selected');
				itemHelpCode(sum,"service");
			}else{
				$("#helpCode_"+sum+" option:selected").removeAttr('selected').siblings(':contains("301")').attr('selected','selected');
				itemHelpCode(sum,"service");
			}
		}else{
			$("#specialBusinessType").show();
			var types="no";
    		for(var i=1;i<=sum;i++){
    			var text=$("#helpCode_"+i).find("option:selected").text();
    			if(($("#helpCode_"+i).attr('name')==='helpCode') &&(text==="301" ||text==="302"||text==="420")){
    				$("#checkbox_"+i).attr('checked','checked');
    				types="yes";
    			}
    		}
    		if(types==="yes"){
    			delHtml("service");
    		}
		}
		
	}
}
// //火化炉类型单击
//   function radio_click(type) {
// 	if (type==${furnace_ty}) {
// 		$("#furnaceType_div").show();
// 		$("#specialBusinessType").hide();
// 		if(funBefore!==''){
//     		$("#"+funBefore).attr('checked','checked');
//     		delHtml("${funListTY[0]}")
//     	}
// 		sum++;
// 		funBefore='checkbox_'+sum;
// 		item(sum,"${funListTY[0]}","","${funListTY[1]}","${funListTY[2]}","${funListTY[3]}","${funListTY[4]}","${funListTY[5]}","${funListTY[6]}","${funListTY[7]}","","${funListTY[9]}","${funListTY[10]}")
// 		sortItem($("#service"));
// 		sTotal+=parseFloat("${funListTY[7]}");
// 		$("#sFont").html(sTotal+"元");
// 	}else{
// 		$("#furnaceType_div").hide();		
// 		if(type!=='3'){
// 			$("#specialBusinessType").hide();
// 			if(funBefore!==''){
// 	    		$("#"+funBefore).attr('checked','checked');
// 	    		delHtml("${funList[0]}")
// 	    	}
// 			sum++;
// 			funBefore='checkbox_'+sum;
// 			item(sum,"${funList[0]}","","${funList[1]}","${funList[2]}","${funList[3]}","${funList[4]}","${funList[5]}","${funList[6]}","${funList[7]}","","${funListTY[9]}","${funListTY[10]}")
// 			sortItem($("#service"));
// 			sTotal+=parseFloat("${funList[7]}");
// 			$("#sFont").html(sTotal+"元");			
// 		}
// 	}
// 	if(type=='3'){
// 		$('#orderTime').val('');
// 		$("#specialBusinessType").show();
// 		if(funBefore!==''){
//     		$("#"+funBefore).attr('checked','checked');
//     		delHtml("${funList[0]}");
//     		funBefore='';
//     	}
// 	}
// }   
//收费项目方法
function item(sum,name,id,itemId,helpCode,pice,typeId,number,bill,total,comment,findexFlag,indexFlag){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name="+name+"_id value="+id+">";
	str+="<input type='checkbox'  data-findexFlag='"+findexFlag+"' data-indexFlag='"+indexFlag+"' id=checkbox_"+sum+" name="+name+" value="+sum+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select readOnly='readOnly' class='list_table' onchange=itemHelpCode("+sum+",'"+name+"') name='helpCode' id='helpCode_"+sum+"'> "+helpCode+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemName("+sum+",'"+name+"') name='itemId' id='itemId_"+sum+"'> "+itemId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' onchange=priceChange("+sum+",'"+name+"') class='list_table' name=pice id='pice_"+sum+"' value="+parseFloat(pice)+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemType("+sum+",'"+name+"') name='typeId' id='typeId_"+sum+"'>"+typeId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='text' class='list_table' onchange=numbers("+sum+",'"+name+"') name=number id='number_"+sum+"' value="+number+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name=bill id=bill_"+sum+"> "+bill+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name=total id=total_"+sum+" class='list_table' value="+parseFloat(total)+"></td>";
	str+="<td><textarea type='text' class='list_table' name=comment id='comment_"+sum+"' >"+comment+"</textarea></td></tr>";
	$("#"+name).append(str);
}


//单价改变
function priceChange(id,type){
	var price=1;
	if(type=="hard"){
		price=$("#hard_pice_"+id).val();
	}else{
		price=$("#pice_"+id).val();
	}
	var a=parseFloat(price);
	if(a!=price){//表示不是数字
		toastr["warning"]("请输入正确的数量!");
	}else{
		var number=1;
		if(type=="hard"){
			number=$("#hard_number_"+id).val();
		}else{
			number=$("#number_"+id).val();
		}
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=price*number;
			var jg=sTotal.toFixed(2);
			$("#sFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=price*number;
			var jg=aTotal.toFixed(2);
			$("#aFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='base'){
			baseTotal=baseTotal-$("#total_"+id).val();
			baseTotal+=price*number;
			var jg=baseTotal.toFixed(2);
			$("#baseFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((price*number).toFixed(0));
		}
		if(type=='hard'){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			hardTotal+=price*number;
			var jg=hardTotal.toFixed(2);
			$("#hardFont").html(parseFloat(jg)+"元");
			$("#hard_total_"+id).val((price*number).toFixed(0));
		}
	}
}

 
//死亡类型改变
function deadTypeChange(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'deadTypeChange',id:$("#deadTypeId").val(),reasonId:$("#deadReasonId").val()},
	    success: function (json) {
	    	$("#deadReasonId").html(json.str);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 

//创建时间
function ndatet(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]+' '+arrTime[1]);
	return checkDate;
}
function ndate(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]);
	return checkDate;
}


//排序项目
function sortItem(target){
	var trs=target.find('tr');
	
	//trs=Array.prototype.slice.call(trs);
	trs.sort(function(a,b){
		var type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		var type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-findexFlag'));
		if(type1==type2){
			type1=Number($(a).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
			type2=Number($(b).find('td:eq(0) input[type="checkbox"]').attr('data-indexFlag'));
		}
		return type2>type1?-1:type2<type1?1:0;
	})
	target.html(trs);
}
//$('#service').on('change','tr select[name="typeId"]',sortItem.bind(this,$('#service')));
//$('#articles').on('change','tr select[name="typeId"]',sortItem.bind(this,$('#articles')));
$('#service').on('change','tr select[name="helpCode"]',function(){
	var txt=$(this).find('option:selected').text();
	var text=$(this).parent().siblings(':last').find('textarea');
	if('${id}'==""){
		if(txt=='535'||txt=='538'){
			if(text.hasClass('required')){return;}
			text.addClass('required');
		}else{
			if(text.hasClass('required')){text.removeClass('required');}
		}
	}
})
//项目类别改变
function itemType(id,type){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemTypeChange',id:$("#typeId_"+id).val()},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	$('#checkbox_'+id).attr('data-findexFlag',json.faIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
				sortItem($("#service"));
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
				sortItem($("#articles"));
    		}
    		$("#pice_"+id).val(json.pice);
	    	$("#total_"+id).val(json.pice);
	    	$("#helpCode_"+id).html(json.helpOption);
	    	$("#number_"+id).val(1);
	    	$("#itemId_"+id).html(json.str);
    		$("#comment_"+id).val("");
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 
//项目名称改变
function itemName(id,type){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#service"))
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#articles"));
    		}
    		if(type=="base"){
    			baseTotal=baseTotal-$("#total_"+id).val();
    			baseTotal+=json.pice;
    			var b=baseTotal.toFixed(2);
				$("#baseFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#base"));
    		}
    		if(type=="hard"){
    			hardTotal=hardTotal-$("#total_"+id).val();
    			hardTotal+=json.pice;
    			var b=hardTotal.toFixed(2);
				$("#hardFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#hard"));
    		}
    		$("#pice_"+id).val(json.pice);
    		$("#helpCode_"+id).html(json.str);
    		var txt=$("#helpCode_"+id).find('option:selected').text();
    		var text=$("#helpCode_"+id).parent().siblings(':last').find('textarea');
    		if(txt=='535'||txt=='538'){
    			if(!text.hasClass('required')){
    				text.addClass('required');
    			}
    		}else{
    			if(text.hasClass('required')){text.removeClass('required');}
    		}
	    	$("#number_"+id).val(1);
	    	$("#comment_"+id).val("");
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	})
} 


//助记码改变
function itemHelpCode(id,type){
	var xhelp=$("#helpCode_"+id).val();
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemHelpCodeChange',id:xhelp},
	    success: function (json) {
	    	$('#checkbox_'+id).attr('data-indexFlag',json.sonIndex);
	    	$("#pice_"+id).val(json.pice);
    		$("#itemId_"+id).html(json.str);
	    	$("#number_"+id).val(1);
	    	$("#comment_"+id).val("");
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
	    		var jg=sTotal.toFixed(2);
				$("#sFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#service"));
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
    			var jg=aTotal.toFixed(2);
				$("#aFont").html(parseFloat(jg)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#articles"));
    		}
    		if(type=="base"){
    			baseTotal=baseTotal-$("#total_"+id).val();
    			baseTotal+=json.pice;
    			var b=baseTotal.toFixed(2);
				$("#baseFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#base"));
    		}
    		if(type=="hard"){
    			hardTotal=hardTotal-$("#total_"+id).val();
    			hardTotal+=json.pice;
    			var b=hardTotal.toFixed(2);
				$("#hardFont").html(parseFloat(b)+"元");
		    	$("#total_"+id).val(json.pice);
		    	sortItem($("#hard"));
    		}
    		
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 


//数目改变
function numbers(id,type){
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	if(a!=number){//表示是不是数字
		toastr["warning"]("请输入正确的数量！");
	}else{
		var pice=$("#pice_"+id).val();
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			var jg=sTotal.toFixed(2);
			$("#sFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((pice*number).toFixed(0));
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=pice*number;
			var jg=aTotal.toFixed(2);
			$("#aFont").html(parseFloat(jg)+"元");
			$("#total_"+id).val((pice*number).toFixed(0));
		}
		if(type=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			baseTotal+=pice*number;
			var jg=baseTotal.toFixed(2);
			$("#baseFont").html(parseFloat(jg)+"元");
	    	$("#total_"+id).val((pice*number).toFixed(0))
		}
		if(type=="hard"){
			hardTotal=hardTotal-$("#total_"+id).val();
			hardTotal+=pice*number;
			var jg=hardTotal.toFixed(2);
			$("#hardFont").html(parseFloat(jg)+"元");
	    	$("#total_"+id).val((pice*number).toFixed(0))
		}
		
	}
} 


//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		if(str=="articles"){
			aTotal=aTotal-$("#total_"+id).val();
			$("#aFont").html(aTotal+"元");
		}
		if(str=="service"){
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		}
		if(str=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			$("#baseFont").html(baseTotal+"元");
		}
		if(str=="hard"){
			hardTotal=hardTotal-$("#total_"+id).val();
			$("#hardFont").html(hardTotal+"元");
		}
		$(this).parent().parent().remove();
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}

}	

//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   { //author: meizz   
	var o = {   
	 "M+" : this.getMonth()+1,                 //月份   
	 "d+" : this.getDate(),                    //日   
	 "h+" : this.getHours(),                   //小时   
	 "m+" : this.getMinutes(),                 //分   
	 "s+" : this.getSeconds(),                 //秒   
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	 "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
	 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
	 if(new RegExp("("+ k +")").test(fmt))   
	fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
}  
/* 保存时候的方法 */ 
function validateSub(form, callback) {
	$("button").prop("disabled", "disabled");
	var $form = $(form);
	/* var va = validHomeForm($form); */
	$(".error-label", form).remove();
	$("[errorFlag]", form).removeAttr("errorFlag");
	$(".error-input", form).removeClass("error-input");
	
	var errArr=[];
	$(".required", form)
	.each(
			function(i,ele) {
				var value = $(this).val();
				if (value == "") {
					var flag = $(this).attr("errorFlag");
					if (flag == undefined) {
						$(this).attr("errorFlag", "true");
						$(this).addClass("error-input");
						var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
						$(this).parent().append(err);
						errArr.push(i)
					}
				}
			});
	if(errArr.length!==0){
		var errNum=Math.min.apply(Math,errArr);
		var ele=$(".required", form).eq(errNum);
		var errName=$(ele).attr('name');
		var errMsg=errName=='pickTime'?'运送时间':errName=='comment'?'挽联备注':$(ele).prev().text().slice(0,-1);
		if($('#a [name="'+errName+'"]').size()!==0){$('[href="#a"]').click()}
		if($('#b [name="'+errName+'"]').size()!==0){$('[href="#b"]').click()}
		var height=$('.required[name="'+errName+'"]').eq(0).offset().top;
		window.scrollTo(0,height);
		toastr["warning"]("请填写"+errMsg);
		$("button").removeAttr("disabled");
		return false;
	}
	
// 	if (va == false) {
// 		$("button").removeAttr("disabled");
// 		toastr["warning"]("请确保填写完整并且格式正确");
// 		return false;
// 	}
	rel = $form.attr("rel");

	$.ajax({
		type : 'POST',
		url : $form.attr("action"),
		data : $form.serializeArray(),
		traditional: true,
		dataType : "json",
		cache : false,
		success : callback,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			toastr["error"](XMLHttpRequest.status);
			$("button").removeAttr("disabled");
		}
	});
	
	return false;
}

</script>
 <script type="text/javascript" src='js/carId.js'></script>
<style>
.container-fluid { 
	background: #fff;
} 
</style>

</head>
<body>
	<section class="content">
		<form action="${url}" id="homeForm" rel="myModal" onsubmit="return validateSub(this,homeAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" data-changeId="true" value="${commissionOrder.id}">
		<input type="hidden" id="name" value="${commissionOrder.name }">
		<input type="hidden"  id ="source" name ="sourcepage"   value="editCommissionOrder">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab" aria-expanded="true">基本信息</a></li>
						<li id='yeInfo' class="" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" aria-expanded="false">业务信息</a></li>
					</ul>
					<div class="tab-content" style='padding-left:0px;padding-right:0px;'>
			
			<!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						<P class='p border-B'>死者信息</P>
							<div class="box-body broder-B">
									<div style='opacity:0;'>
									<object id="plugin0" type="application/lagen-plugin" width="0" height="0">
									<param name="onload" value="pluginLoaded" />
									</object>
									<object id="plugin1" type="application/x-lathumbplugin" width="0" height="0">
 										    <param name="onload" value="pluginLoaded" />
 									</object> 
									<br />
									</div>
									<div class='hide'>
										当前设备：<select id="curDev" style="width: 90px" name="selDev"
																			onchange="changedev()"></select>
										当前分辨率：<select id="curRes" style="width: 90px" name="curRes"
																			onchange="changeres()"></select>
										颜色：<select id="curColor" style="width: 90px" name="curRes"
																			onchange="changeclr()"></select>
										拍照模式：<select id="capMode" style="width: 90px" name="curRes"
																			onchange="changemode()"></select>
									
										<input id="rotatecrop" checked type="checkbox" value="" onclick="RotateCrop(this)" />纠偏裁边
										<input id="drawrect" type="checkbox" value="" onclick="setmousemode(this)" />框选
										<br><br>    
									<input   TYPE="button"   VALUE="开始预览"   onClick="start_preview()"> 
									<input   TYPE="button"   VALUE="停止预览"   onClick="stop_preview()">
									<input   TYPE="button"   VALUE="左转90度"   onClick="rotleft()">
									<input   TYPE="button"   VALUE="右转90度"   onClick="rotright()">
									<input   TYPE="button"   VALUE="视频属性"   onClick="showprop()">
									<input   TYPE="button"   VALUE="条码识别"   onClick="readbarcode()">
									<input   TYPE="button"   VALUE="画面恢复"   onClick="resetvideo()">
									<input   TYPE="button"   VALUE="生成PDF"   onClick="makepdf()">
									<input   TYPE="button"   id=recvideo VALUE="开始录像"   onClick="startrecord()">
									<input   TYPE="button"   VALUE="拍照"   onClick="capture()">
									<input   TYPE="button"   VALUE="拍照为Base64"   onClick="capturebase64()"> <br><br>
									<input   TYPE="button"   id=autocap VALUE="开始智能连拍"   onClick="startautocap()">
									<input   TYPE="button"   id=tmcap VALUE="开始定时连拍"   onClick="starttmcap()">
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者姓名：</label>
									<input type="text" style='width:20%'  class="required list_select" name="name" value="${commissionOrder.name }">&nbsp;<i class="fa fa-circle text-red"></i>
									<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardD('name')">
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>死者地址：</label> 
									<input type="text" style="width:330px;" class="required list_select" name="dAddr" value="${commissionOrder.dAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件类型：</label>
									<select style="width: 169px" class="list_select" name="certificateId" id="certificateId">
										${certificateOption}
									</select>
								</div>
									<div class="col-md-7 height-align">
									<label class='lab-4'>接尸地址：</label> 
<%-- 									<input type="text" class="required list_select" name="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;<i class="fa fa-circle text-red"></i> --%>
									<input list="pickAddr" style="width:330px;" autocomplete='off' class="required  list_select" name="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
									<datalist  class="list_select"  id="pickAddr">
										${corpseAddOption}
									</datalist>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>证件号码：</label> 
									<input type="text" class="required list_select" name="certificateCode" value="${commissionOrder.certificateCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡类型：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="deadTypeId" id="deadTypeId" onchange="deadTypeChange()">
										${deadTypeOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者年龄：</label> 
									<input type="text" class="number list_select" onchange="changeAge(this)" name="age" id="age" value="${commissionOrder.age==0 ? '' : commissionOrder.age }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡原因：</label> 
									<select style="width: 169px" class="required list_select nopadding-R" name="deadReasonId" id="deadReasonId">
										${deadReasonOption}
									</select>
									&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死者性别：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="sex" id="sex">
										${sexOption}
									</select>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>死亡日期：</label> 
									<input  data-id="reservation" readOnly='readOnly'  data-date-format="yyyy-mm-dd hh:ii" class="required list_select" id="dTime" name="dTime" value='<fmt:formatDate value="${commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/>'><i style="margin-left: -20px;" class="fa fa-calendar"></i>&nbsp; &nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-2'>民族：</label> 
									<%-- <select style="width: 169px" class="list_select nopadding-R" name="dNation" >
										${nationOption}
									</select> --%>
									<input list="dNation" autocomplete='off' class="list_select required" name="dNation" id="nation" value="${ commissionOrder.dNationId }">
									<datalist  class="list_select"  id="dNation">
												${nationOption}
									</datalist>	
								</div>
								<div class="col-md-7 height-align">
									<label style='float:left;'>死者地区：</label> 
									<div class=' cl' style='float:left;'>
										<!-- <span style='float:left;'>省：</span> -->
										<div  style='float:left;margin-right:10px;'>
											<select  class="list_input" name="province" id="province">
										
											</select>
										</div>
										<!-- <span style='float:left;'>市：</span> -->
										<div  style='float:left;margin-right:10px;'>
											<select  class="list_input" name="city" id="city">
										
											</select>
										</div>
										<!-- <span style='float:left;'>区：</span> -->
										<div  style='float:left;'>
											<select  class="required list_input" name="area" id="area">
										
											</select>
											&nbsp;<i class="fa fa-circle text-red"></i>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<label class='lab-2'>卡号：</label> 
									<input type="text" class="required list_select" name="cardCode" value="${commissionOrder.cardCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-7">
									<label class='lab-4'>证明单位：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="proveUnitId" id="proveUnitId">
										${proveUnitOption}
									</select> 
									<input style="width: 200px" type="text" name="proveUnitContent" class="list_select" value="${commissionOrder.proveUnitContent }">
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死亡证明：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="dFlag" id="dFlag">
										${dFlagOption}
									</select>
								</div>
								<div class="col-md-12">
									<label>死者身份证图片</label> 
<!-- 									<div class='cl'> -->
									<input  type='file' class='hide' id='dFile' name="dfile">
									<div class='ID-pic' data-name='name'>
									<span>点击选择图片</span>
										<c:choose>
			    						<c:when test="${commissionOrder.id != null}">
			    							<img width=325px;  src="${commissionOrder.dIdcardPic }" alt="images">
			    						</c:when>
			    						<c:otherwise>
			    						</c:otherwise>
			    					</c:choose>
<!-- 										<object id="plugin1" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
									</div>
									
									<input type="hidden" name="filename" value='${commissionOrder.dIdcardPic }'>
									<button type='button'  style='margin-top:10px;' class='btn btn-default' data-a='upload'>确认上传</button>
<!-- 									<div class='ID-pic' onClick="capture(this,'F')"> -->
<!-- 										<span>点击上传反面</span> -->
<!-- 										<object id="plugin2" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
<!-- 									</div> -->
<!-- 									</div> -->
<%-- 									<textarea type="text" class="form-control" name="dIdcardPic" value="${commissionOrder.name }" style="width: 180px;height: 200px"></textarea> --%>
								</div>
							</div>
							<p class='p border-B'>家属/经办人信息</p>
							<div class="box-body">
								<div class="col-md-6 height-align">
									<label  class='label-4'>姓名：</label> 
									<input type="text" class="required list_select" name="fName" value="${commissionOrder.fName }">&nbsp;<i class="fa fa-circle text-red"></i>
									<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardDF('fName')">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-1'>与死者关系：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="fAppellationId" id="fAppellationId"> 
									    ${fAppellationOption}
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-2'>联系号码：</label> 
									<input type="text" class="required list_select" name="fPhone" value="${commissionOrder.fPhone }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label  class='label-4'>住址：</label>
									<input type="text" class="list_select" name="fAddr" value="${commissionOrder.fAddr }">
								</div>
								<div class="col-md-6 height-align">
									<label  class='label-4'>单位：</label> 
									<input type="text" class="list_select" name="fUnit" value="${commissionOrder.fUnit }">
								</div>
								<div class="col-md-5 height-align">
									<label  class='lab-1'>身份证号码：</label> 
									<input type="text" class="list_select" name="fCardCode" value="${commissionOrder.fCardCode }">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>业务审核状态：</label>
			    					<c:choose>
			    						<c:when test="${checkFlag==Check_Yes }">
			    							<font color="green">${commissionOrder.checkFlagName}</font>
			    						</c:when>
			    						<c:otherwise>
			    							<font color="red">未审核</font>
			    						</c:otherwise>
			    					</c:choose>
								</div>
								<div class="col-md-12">
									<label>家属身份证图片</label>
<!-- 									<div class='cl'> -->
									<input class='hide' class='hide' type='file' id='fFile'>
									<div class='ID-pic' data-name='fName'>
									<span>点击选择图片</span>
										<c:choose>
			    						<c:when test="${commissionOrder.id != null}">
			    							<img width=325px; src="${ commissionOrder.eIdcardPic }" alt="images"/>
			    						</c:when>
			    						<c:otherwise>
			    						</c:otherwise>
			    						</c:choose>
<!-- 										<object id="plugin3" type="application/x-lathumbplugin" width="0" height="0"> -->
<!-- 										    <param name="onload" valurequired Loaded" /> -->
<!-- 										</object> -->
									</div>
									<input type="hidden" name="filenameF" value='${commissionOrder.eIdcardPic }'/>
									<button type='button' style='margin-top:10px;' class='btn btn-default' data-a='upload'>确认上传</button>
<!-- 									<div class='ID-pic' onClick="capture(this,'F')"> -->
<!-- 										<span>点击上传反面</span> -->
<!-- 										<object id="plugin4" type="application/x-lathumbplugin" width="325" height="425"> -->
<!-- 										    <param name="onload" value="pluginLoaded" /> -->
<!-- 										</object> -->
<!-- 									</div> -->
<!-- 									</div> -->
<%-- 									<textarea type="text" class="form-control" name="eIdcardPic“ value="${commissionOrder.name }" style="width: 180px;height: 200px"></textarea> --%>
								</div>
							</div>
						</div>
				<!-- 业务信息 -->
						<div class="tab-pane" id="b">
						<p class='p border-B'>业务调度信息</p>
							<div class='box-body border-B'>
							<div class="col-md-5 height-align">
									<label  class='lab-2'>到馆时间：</label> 
									<c:choose>
										<c:when test="${commissionOrder.arriveTime  !=null}">
											<input data-id="reservation" readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii" class="list_select" id="arriveTime" name="arriveTime" 
											value='<fmt:formatDate value="${commissionOrder.arriveTime }" pattern="yyyy-MM-dd HH:mm"/>'>
										</c:when>
										<c:otherwise>
											<input data-id="reservation"  readOnly='readOnly' data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="arriveTime" name="arriveTime" 
											value="${now }">
										</c:otherwise>
									</c:choose>

							</div>
									
								<div class="col-md-5 height-align">
									<label  class='lab-2'>接尸单位：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="corpseUnitId" id="corpseUnitId">
										${corpseUnitOption}
									</select>
									<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5" id="furnace_div">

								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>火化预约时间：</label> 
									<input data-id="reservation" readOnly='readOnly'  class="required list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm"/>">&nbsp;<i class="fa fa-circle text-red"></i>
									<%-- <input data-id="reservation" class="list_select" id="orderTime" name="orderTime" value="<fmt:formatDate value="${furnaceRecord.orderTime }" pattern="yyyy-MM-dd HH:mm:ss"/>"> --%>
								</div>
								<div class="col-md-5 " id="furnaceType_div">
									<label  class='lab-2'>火化炉号：</label>
									<input type="hidden" name="furnaceId" id="furnaceId"  value="${furnaceRecord.furnaceId }">
									<input name="furnaceName" id="furnaceName" class=" list_select" readonly="readonly" value="${furnaceRecord.furnace.name }">
									<a href="commissionOrder.do?method=editFurnace&current_time=${commissionOrder.cremationTime }" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								</div>
								<div class="col-md-5 height-align" id="specialBusinessType">
									<label class='lab-6'>特殊业务类型:</label>
									<select style="width: 169px" class="nopadding-R list_select " name="specialBusinessValue" id="specialBusinessValue">
										${specialBusinessOption}
									</select>	
								</div>
						</div>	

								<p class='p border-B'>服务项目信息</p>
							<div class="box-body border-B">
								<div class="col-md-12"   style='padding-left:0px;'>
									<small class="btns-buy"> 
<%-- 										<a href="${url}?method=selectItem&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
										<a href="${url}?method=selectItems&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<button type="button" onclick="delHtml('service')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="service" /></th>
											<th width="100px">助记码</th>
											<th width="200px">名称</th>
									        <th width="100px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="80px">数量</th>
									        <th width="80px">挂账</th>
									        <th width="100px">小计</th>
									        <th>备注</th>
										</tr>
									</thead>
									<tbody id="service">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="sFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p class='p border-B'>丧葬用品信息</p>
							<div class="box-body">
								<div class="col-md-12">
									<small class="btns-buy"   style='padding-left:0px;'> 
<%-- 										<a href="${url}?method=selectItem&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
										<a href="${url}?method=selectItems&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<button type="button" onclick="delHtml('articles')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="articles" /></th>
											<th width="100px">助记码</th>
											<th width="200px">名称</th>
									        <th width="100px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="80px">数量</th>
									        <th width="80px">挂账</th>
									        <th width="100px">小计</th>
									         <th>备注</th>
										</tr>
									</thead>
									<tbody id="articles">
										
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='8' style='text-align: left;' >
											<font color='red' id="aFont">
											0元
											</font>
											</td>
										</tr>
									</tbody>
								</table>		
								
							</div>
						</div>
				
				
					</div>
				</div>
			</div>
			<div class="box-body">
			<div  class="container-fluid">
				<div class='row padding-B'>
					<div class='col-md-6'>
						<div class='row'>
							<%-- <div class="col-md-6">
								<label>预约登记人：</label>
									<span>${commissionOrder.pbookAgentId }</span>
							</div> --%>
							
							<div class="col-md-5 height-align">
								<label>到馆登记人：</label>
								<c:choose>
								    <c:when test="${commissionOrder.bookAgentId!=null}">
								    	${commissionOrder.bookAgentId}
								    </c:when>
								    <c:otherwise>
								   		${agentUser}
									</c:otherwise>
								</c:choose> 
								
							</div>
							<div class="col-md-7 height-align">
								<label class='lab-3'>编号：</label> 
								<input type="text" class="list_select" readonly="readonly"  value="${commissionOrder.code == null ? '由系统自动生成' : commissionOrder.code}">
							</div>
							<div class="col-md-6 height-align">
								<label class='lab-1'>办理时间：</label> 
								${time }
							</div>
						</div>
					</div>
					<div class='col-md-6' style='padding-bottom:5px'>
						<div class='row'>
							<div class="col-md-12">
								<small class="pull-right btns-hometab">
								<c:if test="${checkFlag!=Check_Yse }">
									<button type="button" onclick="checkSelect()" class="btn btn-info" style='margin-top:30px;'>保存</button>
			   					</c:if>
			<%-- 						<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="certificateId" warm="确认禁用吗" class="btn btn-danger " role="button">重置</a> --%>
								<a href="${url}?method=list" target="homeTab" style='margin-top:30px;' class="btn btn-default" role="button">返回</a>
								</small>
							
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			
		<!-- 	<script>
	 	(function(){
	 		/*选择按钮初始化*/
	 
	 		
	 		$('[data-choose="get"]').click(function(){
	 			data_group.name=$(this).prev().prev().attr('data-name');
	 			data_group.id=$(this).prev().attr('data-id');
	 			$(this).next().click();
	 		});
			var href=$('[data-dialog="dialog"]').attr('href');
	 		$('[data-name="search"]').click(function(){
	 			var dName = $("#dName").val().trim();
	 			if(dName==""){
	 				$('[data-warning="warning"]').html('请输入死者姓名').removeClass('nowarning').addClass('warning');
	 				return;
	 			}else{
	 				var $form = $(pageForm);
	 				$.ajax({
	 				    url: $form.attr("action"),
	 				    dataType:'json',
	 				    cache: false,
	 				    data:{method:'dNameSearch',dName:dName},
	 				    success: function (json) {
	 				    	var size = json.size;
		 				   	if(size===0){
		 		 				$('[data-warning="warning"]').html('无死者信息').removeClass('nowarning').addClass('warning');
		 		 			}else if(size===1){
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$("#certificateCode").val(typeof(json.certificateCode)!="undefined"?json.certificateCode:'');
		 		 				$("#dName").val(typeof(json.name)!="undefined"?json.name:'');
		 		 				$("#dNameId").val(typeof(json.name)!="undefined"?json.dNameId:'');
	                   			$("#dSex").val(typeof(json.dSex)!="undefined"?json.dSex:'');
	                   			$("#dAge").val(typeof(json.dAge)!="undefined"?json.dAge:'');
	                   			$("#mourningPlace").val(typeof(json.mourningPlace)!="undefined"?json.mourningPlace:'');
	                   			$("#beginDate").val(typeof(json.beginDate)!="undefined"?json.beginDate:'');
	                   			$("#endDate").val(typeof(json.endDate)!="undefined"?json.endDate:'');
	                   			$("#farewellPlace").val(typeof(json.farewellPlace)!="undefined"?json.farewellPlace:'');
	                   			$("#farewellDate").val(typeof(json.farewellDate)!="undefined"?json.farewellDate:'');
	                   			$("#cremationTime").val(typeof(json.cremationTime)!="undefined"?json.cremationTime:'');
		 		 				
		 		 			}else{
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$('[data-dialog="dialog"]').attr('href',href+"&dName="+dName);
		 		 				$('[data-dialog="dialog"]').click();
		 		 			}
		 				    	
		 				}
	 				});
	 			}
	 		});
	 	})()
	 </script> -->
	 	</form>
	</section>
</body>
<script type="text/javascript">
if('${id}'==""){
		$("#nation").val("汉族");
	} 

$('[name="toponymId"] option:contains("永嘉县")').attr('selected','selected');
 	$('.ID-pic').click(function(){
		$(this).prev().click();
	})
	$('[type="file"]').change(function(){
		$this=$(this);
		var file=this.files[0];
		if(file!==undefined){
			var reader= new FileReader();
			reader.readAsDataURL(file);
			$(reader).load(function(){
				$this.next().find('img').remove();
				//console.log(reader);
				var img=document.createElement('img');
				img.src=reader.result;
				img.width=325;
				//console.log($this);
				$this.next().append(img);
			})
		}
		
	}) 
	$('[data-a="upload"]').click(function(){
		event.preventDefault();
		$this=$(this);
		if($this.prev().prev().prev().val()!==''){
			var id=$this.prev().prev().prev().attr('id');
			var fileObj = document.getElementById(id).files[0]; 
			var form = new FormData();
			form.append("file", fileObj); 
			var xhr = new XMLHttpRequest();
	
	        xhr.open("post", 'commissionOrder.do?method=fileupload', true);
	
	        xhr.onload = function () {
				if((xhr.readyState == 4) && (xhr.status == 200)){
					$this.prev().val(xhr.responseText);
					toastr["success"]("上传成功");
				}else{
					toastr["danger"]("上传失败");
				}
	        };
	
	        xhr.send(form);
		}else{
			toastr["warning"]("请选择图片");
		} 
	});
//若有基本减免，则必须勾选对象类别
function checkSelect() {
	if(!$('#corpseUnitId').val()){
		toastr["warning"]("请填写接尸单位");
		$('#yeInfo').find('a').click();
		var top=$('#corpseUnitId').offset().top;
		window.scrollTo(0,top);
		return false;
	}
	if($("#baseIs").val()=='${Is_Yes}'){
		if($("input[name='freePersonId']:checked").size()===0 || $("input[name='hardPersonId']:checked").size()===0){
			toastr["warning"]("请选择免费对象类别或重点类别");
			$('#baseLi').find('a').click();
			var top=$('#baseLi').offset().top;
			window.scrollTo(0,top);
			return false;
		} 
	}
/* 	if ($("#hardIs").val()=='${Is_Yes}') {
		if($("input[name='proveIds']:checked").size()===0){
			toastr["warning"]("请选择证件类型");
			$('#hardLi').find('a').click();
			var top=$('#hardLi').offset().top;
			window.scrollTo(0,top);
			return false;
		}
	} */
	$("#homeForm").submit();

};
</script>
<script type="text/javascript">
	$("[data-a='dismiss']").click(function(){
		var tbody=[];
		$("#articles").find("tr").each(function(i,tr){
			var trs='';
			$(tr).find("td").each(function(j,td){
				if(j>0){
						trs+=',';
				}
				trs+=$(td.children[0]).val();	
			})
			tbody[i]=trs;
		})
		var $form = $('#detail');
		var va = validHomeForm($form);
		rel = $form.attr("rel");
		var datas= $form.serializeArray();
		var obj={};
		for(var key in datas){
			obj[datas[key].name]=datas[key].value;
		}
		obj.table=tbody;
		//datas.push({name:"table",value:tbody});
		//console.log(JSON.stringify(datas));
		$.ajax({
			type : 'POST',
			traditional: true,
			url : $form.attr("action"),
			data : obj,
			dataType : "json",
			cache : false,
			success :homeAjaxDone,
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				toastr["error"](XMLHttpRequest.status);
			}
		});
		$('[ data-dismiss="modal"]').click();
		return false;
	});
	</script>
<script type="text/javascript" src='js/jsAddress.js'></script>
<script>
	addressInit('province', 'city', 'area', '浙江省', '温州市', '')
</script>