<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">礼仪出殡列表查询</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
</style>
<script>
$(function(){
	timeRange();
});
</script>
<form action="${url }?method=listInfo" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="${method}" type="hidden">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input" id="name" name="searchType">
									${searchOption }
								</select>
							</div>
							<div class="col-md-6">
								<label>查询值：</label> 
								<input type="text" class="list_input" id='search' name='searchVal' value="${name}"  placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>火化时间：</label>                                                                        
								<input type="text" data-id='beginDate' class="list_select" id="beginDate" name="startTime" value="<fmt:formatDate value="${startTime }" pattern='yyyy-MM-dd'/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span> 
								<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${endTime}" pattern='yyyy-MM-dd'/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>
			<div class="row">
				<div class='col-md-12'>
					<span class='btns-buy'>
						<a href="${url }" target='homeTab' class='btn btn-default'>返回</a>
					</span>
				</div>
				<div class="col-md-12">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>告别时间</th>
								<th>
									死者姓名
								</th>
								<th>年龄</th>
								<th>死者性别</th>
								<th>纸馆</th>
								<th>告别厅号</th>
								<th>火化时间</th>
								<th>火化炉号</th>
								<th>状态</th>
							</tr>
						</thead>
						<tbody>
 						<c:choose>
							<c:when test="${fn:length(page.list)==0 }"> 
							 <tr width="20">
 								    <td colspan="9">
 										<font color="Red">还没有数据</font> 
									 </td> 
 								</tr> 
 							</c:when> 
						<c:otherwise> 
 							<c:forEach items="${page.list }" var="u"> 
									<tr>
										<td><fmt:formatDate value="${u.farewellTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>${u.name}</td>
										<td>${u.age}</td>
										<c:choose>
										  <c:when test="${u.sex==1}">
										   <td>男</td>
										  </c:when>
										   <c:when test="${u.sex==2}">
										   <td>女</td>
										  </c:when>
										</c:choose>
										
										<td>${u.price}</td>
										<td>${u.farewellId}</td>
										<td><fmt:formatDate value="${u.cremationTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<c:choose>
										<c:when test="${u.funeralName==null &&u.furnaceFlag==1 }">
										<td>普通炉</td>
										</c:when>
										<c:when test="${u.funeralName!=null &&u.furnaceFlag==2 }">
										<td>特约炉(${u.funeralName })</td>
										</c:when>
										<c:otherwise>
										<td>无</td>
										</c:otherwise>
										</c:choose>
										<c:choose>
										<c:when test="${u.checkFlag=='1' }">
										  <td class='state1'>锁定</td>
										</c:when>
										<c:when test="${u.checkFlag=='2' }">
										  <td class='state1'>占用</td>
										</c:when>
										<c:otherwise>
										<td>1</td>
										</c:otherwise>
										</c:choose>
									</tr>
							
						</c:forEach> 
 						</c:otherwise> 
 					  </c:choose>
						</tbody>
					</table>
					
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
		<!-- 报表 -->
<div id="reprot">

    <birt:viewer id="birtViewer" 
        reportDesign="testBirtAndSpring.rptdesign" format="HTML" 
        width="300" height="480" left="0" 
        top="0" showParameterPage="false"> 
       <birt:param name="unitsId" value="${unitsId}" /> 
    </birt:viewer> 
      
</div>
	</section>
</form>
