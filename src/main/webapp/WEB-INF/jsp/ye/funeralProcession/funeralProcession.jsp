<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">礼仪出殡调度</div>
</section>
<style type="text/css">
	.bg-efefef{
		background-color:#efefef;
	}
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}

</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url }"  id="pageForm" class="outerform" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>日期：</label> <!-- data-provide="datetimepicker" -->
							<input type="text" data-id='beginDate'  onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="beginDate" name="startTime" value="<fmt:formatDate value="${startTime }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
<!-- 							 <button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button> -->
						</div>
						<div class="col-md-6 btns-list">
						<!-- dispatcher.do?list=funeralProcession-listFuneralProcession -->
							<a href="${url}?method=begin" target="homeTab" rel="myModal" id="liebiao" class="btn btn-info" role="button">进入列表</a>
						</div>
						
						<div class="col-md-3">
							<ul class="la_ul">
								<li style="background:#7E57C2;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li>
							</ul>
						</div>
					</div>
						<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover1">
<%-- 							<% <c:forEach items="${flist }" var="u" varStatus="i"> %>	 --%>
<%-- 							<%  </c:forEach> %>		<td>思思不忘初心 </td> --%>
<%--                    <c:forEach items="${flist }" var="v"> --%>
<%--                       <tr style="border:1 solid red;background:#29b6f6;" > ${v.name }</tr> --%>
<%--                    </c:forEach> --%>
									<tbody>
									    <tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time7[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time7[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time7[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time7[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result7[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result7[a]!=null }">
														<c:choose>
															<c:when test="${result7[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"  >
																<a href="commissionOrder.do?method=readOnly&id=${result7[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result7[a].name }</a></td>
															</c:when>
															<c:when test="${result7[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result7[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result7[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result7[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result7[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result[a]!=null }">
														<c:choose>
															<c:when test="${result[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"  >
																<a href="commissionOrder.do?method=readOnly&id=${result[a].id}&adr=DB" target='homeTab' style="color:#fff;" >${result[a].name }</a></td>
															</c:when>
															<c:when test="${result[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time1[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time1[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time1[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time1[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result1[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result1[a]!=null }">
														<c:choose>
															<c:when test="${result1[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"   >
																<a href="commissionOrder.do?method=readOnly&id=${result1[a].id}&adr=DB" target='homeTab' style="color:#fff;" >${result1[a].name }</a></td>
															</c:when>
															<c:when test="${result1[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result1[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result1[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result1[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result1[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time2[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time2[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time2[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time2[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result2[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result2[a]!=null }">
														<c:choose>
															<c:when test="${result2[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"   >
																<a href="commissionOrder.do?method=readOnly&id=${result2[a].id}&adr=DB" target='homeTab' style="color:#fff;" >${result2[a].name }</a></td>
															</c:when>
															<c:when test="${result2[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result2[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result2[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result2[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result2[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time3[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time3[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time3[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time3[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result3[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result3[a]!=null }">
														<c:choose>
															<c:when test="${result3[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"   >
																<a href="commissionOrder.do?method=readOnly&id=${result3[a].id}&adr=DB" target='homeTab'style="color:#fff;" >${result3[a].name }</a></td>
															</c:when>
															<c:when test="${result3[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result3[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result3[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result3[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result3[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time4[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time4[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time4[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time4[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result4[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result4[a]!=null }">
														<c:choose>
															<c:when test="${result4[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"   >
																<a href="commissionOrder.do?method=readOnly&id=${result4[a].id}&adr=DB" target='homeTab' style="color:#fff;" >${result4[a].name }</a></td>
															</c:when>
															<c:when test="${result4[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result4[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result4[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result4[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result4[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time5[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time5[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time5[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time5[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result5[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result5[a]!=null }">
														<c:choose>
															<c:when test="${result5[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"   >
																<a href="commissionOrder.do?method=readOnly&id=${result5[a].id}&adr=DB" target='homeTab' style="color:#fff;" >${result5[a].name }</a></td>
															</c:when>
															<c:when test="${result5[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result5[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result5[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result5[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result5[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time6[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time6[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time6[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time6[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result6[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result6[a]!=null }">
														<c:choose>
															<c:when test="${result6[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"   >
																<a href="commissionOrder.do?method=readOnly&id=${result6[a].id}&adr=DB" target='homeTab' style="color:#fff;" >${result6[a].name }</a></td>
															</c:when>
															<c:when test="${result6[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result6[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result6[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result6[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result6[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
										<tr class='bg-efefef'>
											<td colspan='4'><fmt:formatDate value="${time8[0] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time8[1] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time8[2] }" pattern="HH.mm"/></td>
											<td colspan='4'><fmt:formatDate value="${time8[3] }" pattern="HH.mm"/></td>
										</tr>
										<tr>
										<c:forEach begin="0" end="15" var="a">
											<c:if test="${result8[a]==null }">
															<td></td>
														</c:if>
														<c:if test="${result8[a]!=null }">
														<c:choose>
															<c:when test="${result8[a].checkFlag == IsFlag_Yse }">
																<td id="tocolor"  style="background:#7E57C2;" width="100"  >
																<a href="commissionOrder.do?method=readOnly&id=${result8[a].id}&adr=DB" target='homeTab' style="color:#fff;" >${result8[a].name }</a></td>
															</c:when>
															<c:when test="${result8[a].checkFlag == IsFlag_No }">
																<td id="tocolor" style="background:#29b6f6;" width="100" >
																<a href="commissionOrder.do?method=readOnly&id=${result8[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result8[a].name }</a></td>
															</c:when>
															<c:otherwise>
																	<td id="tocolor" style="background:#29b6f6;"  width="100" >
																	<a href="commissionOrder.do?method=readOnly&id=${result8[a].id}&adr=DB" target='homeTab' style="color:#fff;">${result8[a].name }</a></td>
															</c:otherwise>
														</c:choose>	
														</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
		
<!-- 		<script> -->
<!--  		(function(){ -->
<!--  			$('#example2_wrapper table tbody>tr td[data-num]').each(function(){ -->
<!--  				//console.log(this); -->
<!--  				var data=$(this).attr('data-num'); -->
<!--  				switch (data){    -->
<!--  					case '1': -->
<!--  						$(this).html('占用').css({'backgroundColor':'#29b6f6','color':'#fff'}); -->
<!--  						break; -->
<!--  					case '3': -->
<!-- 						$(this).html('锁定').css({'backgroundColor':'#fea625','color':'#fff'}); -->
<!--  						break; -->
<!-- 				} -->
<!-- 			}) -->
<!--  		})(); -->
<!-- <!-- 		</script> -->
	</div>
</section>
