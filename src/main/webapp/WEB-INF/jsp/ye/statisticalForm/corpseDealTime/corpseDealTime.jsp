<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<style type="text/css">
</style>
<script type="text/javascript">
	function changeDate(){
		$('#pageForm').submit();
	}
</script>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">遗体处理安排表</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
					<iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe>
						
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	
</section>