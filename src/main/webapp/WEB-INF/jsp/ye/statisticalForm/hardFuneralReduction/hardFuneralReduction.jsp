<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">困难殡葬减免统计</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
	}
</style>
<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="${method }" type="hidden">

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					
						<div class="box-body">
						<iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=hardReduction.cpt&op=view"></iframe>
							
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>

</form>
