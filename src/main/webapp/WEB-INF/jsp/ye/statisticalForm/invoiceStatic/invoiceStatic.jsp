<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">发票统计</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
	}
</style>
<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							<div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<label class='timerange'>--</label> 
								<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<button type="submit" class="btn btn-info btn-search btn-search-left">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<p class="col-md-3">2016-5-14 -2016-6-6</p>
						<div class="col-md-9">
							<small class='pull-right btns-print'>
<!-- 								<a href="javascript:myprint()" class="btn btn-color-ab47bc" role="button">打印</a> -->
								<a href="${url}?method=exportExcel" class="btn btn-success" role="button">导出EXCEL</a>
								<a href="${url}?method=exportWord" class="btn btn-primary" role="button">导出WORD</a>
							</small>
						</div>
					</div>
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th colspan='7'>
									发票统计
								</th>
							</tr>
							<tr>
								
								<th>
									死者登记号
								</th>
								<th>死者姓名</th>
								<th>亲属姓名</th>
								<th>发票日期</th>
								<th>发票金额</th>
								<th>支付方式</th>
								<th>操作员</th>
							</tr>
							</tr>
						</thead>
						<tbody>
<%-- 						<c:choose> --%>
<%-- 							<c:when test="${fn:length(page.list)==0 }"> --%>
<!-- 								 <tr width="20"> -->
<!-- 								    <td colspan="6"> -->
<!-- 										<font color="Red">还没有数据</font> -->
<!-- 									 </td> -->
<!-- 								</tr> -->
<%-- 							</c:when> --%>
<%-- 						<c:otherwise> --%>
<%-- 							<c:forEach items="${page.list }" var="u"> --%>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
							
<%-- 							</c:forEach> --%>
<%-- 						</c:otherwise> --%>
<%-- 					  </c:choose> --%>
									<tr>
										<td>合计</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
						</tbody>
					</table>
					
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>