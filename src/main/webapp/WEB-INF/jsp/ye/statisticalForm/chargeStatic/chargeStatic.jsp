<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">收费统计报表</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
</style>
<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
					
						
						<div class="box-body">
						<iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=Charage.cpt&op=view"></iframe>
							
						</div>
					
				</div>
			
			</div>
		</div>
	</section>
	
</form>