<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">销卡记录查询</div>
</section>
<script type="text/javascript">
	function changeDate() {
		$('#pageForm').submit();
	}
</script>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				
				<div class="box-body">
				<iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=pincardRecord.cpt&op=view"></iframe>
					
				</div>

			</div>
		</div>
	</div>
	
</section>
