<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="box-body" style='overflow-x:auto;padding-left:0px'>
					<div id="example4_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<c:forEach begin="1" items="${list2 }" var="u" varStatus="i">
							<div class="col-sm-12">
								<c:if test="${u[0].code==7 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dOb2 }" var="ob" begin="0" end="24" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td style='white-space: nowrap;'>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td style='white-space: nowrap;'>${u[0].name }</td>
											<c:forEach begin="1" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="funeral_click( ${i.index},${a },this)" id="funeral_${i.index}_${a }"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_YY}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
 																${u[a].order.name} 
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
								</c:if>
								
								<c:if test="${u[0].code==8 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObEight }" var="ob" begin="0" end="24" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td style='white-space: nowrap;'>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td style='white-space: nowrap;'>${u[0].name }</td>
											<c:forEach begin="1" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="funeral_click( ${i.index},${a },this)" id="funeral_${i.index}_${a }"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_YY}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
								</c:if>
								<c:if test="${u[0].code==9 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObNine }" var="ob" begin="0" end="24" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td style='white-space: nowrap;'>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td style='white-space: nowrap;'>${u[0].name }</td>
											<c:forEach begin="1" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="funeral_click( ${i.index},${a },this)" id="funeral_${i.index}_${a }"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_YY}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
								</c:if>
								<c:if test="${u[0].code==10 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObTen }" var="ob" begin="0" end="24" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td style='white-space: nowrap;'>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td style='white-space: nowrap;'>${u[0].name }</td>
											<c:forEach begin="1" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td onclick="funeral_click( ${i.index},${a },this)" id="funeral_${i.index}_${a }"></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_YY}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
									</tbody>
								</table>
								</c:if>
							</div>
							</c:forEach>
						</div>
						</div>
					</div>
					<script>
(function(){
	$('#example4_wrapper table tbody>tr td[data-num]').each(function(){
		//console.log(this);
		var data=$(this).attr('data-num');
		switch (data){
			case '1':
				$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
				break;
			case '6':
				$(this).css({'backgroundColor':'#738ffe','color':'#fff'});
				break;
			case '3':
				$(this).css({'backgroundColor':'#fea625','color':'#fff'});
				break;
			case '4':
				$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
				break;
			case '5':
				$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
				break;
			case '8':
				$(this).css({'backgroundColor':'#673ab7','color':'#fff'});
				break;
		}
	})	 
})();

var funeralNum=0;
var funeralHere;

//单元格单击
function funeral_click(i,a,ele){
	var type=null;
	if(funeralNum==1){
		$("#"+funeralHere).css('background-color','#fff');
		if(funeralHere=='funeral_'+i+'_'+a  && type !='esc'){
			$("#"+funeralHere).css('background-color','#fff');
			data.funeralCode=null;
			data.funeralTime=null;
			type='esc';			
		}
	}
	if(type !='esc'){
		funeralHere="funeral_"+i+"_"+a;
		$(ele).css('background-color','#f39c12');
		funeralNum=1;
		data.funeralCode=getTarget($(ele));
		data.funeralTime=getTime('funeralDate',$(ele),1,false);		
	}
}
</script>