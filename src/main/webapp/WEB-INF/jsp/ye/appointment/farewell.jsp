<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="box-body" >
	<div id="example3_wrapper"
		class="dataTables_wrapper form-inline dt-bootstrap">
		<div class="row">
			<div class="col-sm-12 fix-table">
				<table class="table table-bordered table-hover1">
					<c:forEach items="${list }" var="u" varStatus="i">
						<c:choose>
							<c:when test="${i.index==0 }">
								<thead>
									<tr role="row">
										<th>${u[0]}</th>
										<c:forEach begin="1" end="13" var="a">
										<th colspan="2"><fmt:formatDate value="${u[a]}" pattern="HH:mm"/></th>
										</c:forEach>
									</tr>
								</thead>
							</c:when>
							<c:otherwise>
								<tr role="row">
									<td><font color="blue">${u[0].name }</font></td>
									<c:forEach begin="1" end="26" var="a">
										<c:if test="${u[a]==null }">
											<td onclick="farewell_click( ${i.index},${a },this)" id="farewell_${i.index}_${a }"></td>
										</c:if>
										<c:if test="${u[a]!=null }">
											<c:choose>
												<c:when test="${u[a].flag == IsFlag_Decrate }">
													<td  data-num='${u[a].flag}' data-bool='${u[a].isLIYI}' title='点击更改维修状态'>
														<a style="color:#fff" href="repairRecord.do?method=info&type=fare&id=${u[a].id}&&itemId=${u[0].id }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
													</td>
												</c:when>
												<c:when test="${u[a].flag == IsFlag_Lock }">
													<td  data-num='${u[a].flag}' data-bool='${u[a].isLIYI}' title='${u[a].order.name},<fmt:formatDate value="${u[a].beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
														${u[a].order.name}
													</td>
												</c:when>
												<c:when test="${u[a].flag == IsFlag_YY}">
													<td  data-num='${u[a].flag}' data-bool='${u[a].isLIYI}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
														${u[a].appointmentName }
													</td>
												</c:when>
												<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Bzwc || u[a].flag == 9}">
													<td data-num='${u[a].flag}' data-bool='${u[a].isLIYI}' title='${u[a].order.name},<fmt:formatDate value="${u[a].beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
<%-- 														<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&gbTime=${u[a].beginDate} --%>
<%-- 														&flag=${u[a].flag}&enterId=${u[a].enterId}&arrTime=${u[a].arrangeTime}&type=fare" target="dialog" rel="myModal">${u[a].order.name}</a> --%>
														${u[a].order.name}
												 	</td>
												</c:when>
												<c:otherwise>
													<td data-num='${u[a].flag}' data-bool='${u[a].isLIYI}'></td>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:forEach>
								</tr>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</table>				
			</div>
		</div>
	</div>
</div>
				
<script type="text/javascript">


(function(){
	$('#example3_wrapper table tbody>tr td[data-num]').each(function(){
		var data=$(this).attr('data-num');
		var bool = $(this).attr('data-bool');//是否礼仪出殡项
		var liyi = $(this);
		isFarewell(bool,liyi);
		switch (data){
			case '${IsFlag_Yse }':
				$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
				break;
			case '${IsFlag_Bzwc }':
				$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
				break;
			case '${IsFlag_Lock }':
				$(this).css({'backgroundColor':'#fea625','color':'#fff'});
				break;
			case '${IsFlag_YY}':
				$(this).css({'backgroundColor':'#673ab7','color':'#fff'});
				break;
			case '${IsFlag_Decrate }':
				$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
				break;
			case '9':
				$(this).css({'backgroundColor':'#336868','color':'#fff'});
				break;
			/* case '2':
				$(this).html('进行').css({'backgroundColor':'#738ffe','color':'#fff'});
				break; */
			/* case '5':
				$(this).html('维修完').css({'backgroundColor':'#9CCC65','color':'#fff'});
				break; */
		}
	})
	
	function isFarewell(bool,liyi){
		if(bool == 1){
			liyi.append("<span class='fristspan'><span class='lispan'> 礼 </span></span>");
			$(".lispan").parents("td").css("position","relative");
		}	
	}
})();

var onlyOne=0;
var farewellHere;

//单元格单击
function farewell_click(i,a,ele){
	var type=null;
	if(onlyOne==1){		
		$("#"+farewellHere).css('background-color','#fff');
		if(farewellHere=='farewell_'+i+'_'+a  && type !='esc'){
			$("#"+farewellHere).css('background-color','#fff');
			data.farewellCode=null;
			data.farewellTime=null;
			type='esc';			
		}
	}
	if(type !='esc'){
		farewellHere="farewell_"+i+"_"+a;
		$(ele).css('background-color','#f39c12');
		data.farewellCode=getTarget($(ele));
		data.farewellTime=getTime('farewellDate',$(ele),2,false);
		onlyOne=1;		
	}
}
</script>