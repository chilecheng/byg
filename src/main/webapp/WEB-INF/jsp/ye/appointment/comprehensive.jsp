<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<style>
	.fristspan{ width: 0;
    height: 0;
    border-bottom: 25px solid red;
    border-left: 25px solid transparent;
    position: absolute;
    right: 0;
    bottom: 0;}
	.lispan{   
	font-size: 12px;
    position: absolute;
    right: 0;
    top: 6px;}
</style>
<section class="content-header">
	<i class="title-line"></i>
	<c:if test="${dept!=2}">
		<div class="title">综合调度</div>
	</c:if>
	<c:if test="${dept==2}">
		<div class="title">三合一视图</div>
	</c:if>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning" style='margin-bottom:0px'>
				<div class="box-header with-border">
					<c:if test="${dept!=2}">	
						<h3 class="box-title">告别厅调度</h3>
					</c:if>
					<c:if test="${dept==2}">
						<h3 class="box-title">告别厅调度</h3>
					</c:if>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<input name="id" value="${id }" type="hidden"/>
					<div class="box-body">
						<div class="col-md-3">
							<label>日期：</label> <!-- data-provide="datetimepicker" -->
							<input type="text" data-id='beginDate' data-method='farewellDateChange' data-tar='farewell' onchange="changeDate(this)" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="farewellDate" name="farewellDate" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
<!-- 							 <button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button> -->
						</div>
						<div class="col-md-3 btns-list">
							<c:if test="${dept==1}">
								<c:if test="${isShow ==1 }">
									<a class='btn btn-info' href='appointment.do?method=listOrder' target='homeTab'>进入预约列表</a>
								</c:if>	
							</c:if>
						</div>
						
						<div class="col-md-9">
							<ul class="la_ul">
								<li style="background:#9ccc65;"><div>布置完成</div></li>
								<li style="background:#336868;"><div>清场</div></li>
								<li style="background:#673ab7;"><div>预定</div></li> 
								<li style="background:#fea625;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li>
								<li style="background:#e84e40;"><div>装修</div></li>
							</ul>
						</div>
					</div>
					<div id='farewell' style='padding:10px;'> 
						<%@ include file="/WEB-INF/jsp/ye/appointment/farewell.jsp" %>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning" style='margin-bottom:0px'>
				<div class="box-header with-border">
					<h3 class="box-title">火化炉调度</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm2" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>日期：</label> <!-- data-provide="datetimepicker" -->
							<input type="text" data-id='beginDate' data-method='funeralDateChange' data-tar='funeral' onchange="changeDate(this)" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="funeralDate" name="furnaceDate" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
<!-- 							 <button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button> -->
						</div>
						<div class="col-md-3 btns-list">
<%-- 							<a href="${url}?method=listInfo" target="homeTab" rel="myModal" id="liebiao" class="btn btn-info" role="button">进入列表</a> --%>
<!-- 							<a href='repairRecord.do?method=repair&type=furnace' target='dialog' rel='myModal' class='btn btn-danger' role='button'>维修</a> -->
<%-- 							<a href="${url}?method=edit" target="homeTab" rel="myModal" id="dayin" class="btn btn-primary" role="button">打印</a> --%>
						</div>
						
						<div class="col-md-6">
							<ul class="la_ul">
								<li style="background:#9ccc65;"><div>完成</div></li>
								<li style="background:#673ab7;"><div>预定</div></li>
								<li style="background:#fea625;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li>
								<li style="background:#e84e40;"><div>装修</div></li>
							</ul>
						</div>
					</div>
					<div id='funeral' style='padding:10px;'>
						
							<%@ include file="/WEB-INF/jsp/ye/appointment/funeral.jsp" %>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning" style='margin-bottom:0px'>
				<div class="box-header with-border">
					<h3 class="box-title">灵堂调度</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm3" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>日期：</label> <!-- data-provide="datetimepicker" -->
							<input type="text" data-id='beginDate' data-method='mourningDateChange' data-tar='mourning' onchange="changeDate(this)" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="mourningDate" name="mourningDate" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
<!-- 							 <button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button> -->
						</div>
						<div class="col-md-5 btns-list">
							<a data-method='mourningDateChange' data-tar='mourning' onclick="changeDate(this)"  data-change='go' class="btn btn-default" role="button">前一天</a>
							<a data-method='mourningDateChange' data-tar='mourning' onclick="changeDate(this)"  data-change='back' class="btn btn-default" role="button">后一天</a>
<%-- 							<a href="${url}?method=listInfo" target="homeTab" rel="myModal"  class="btn btn-info" role="button">进入列表</a> --%>
<!-- 							<a href='repairRecord.do?method=repair&type=furnace' target='dialog' rel='myModal' class='btn btn-danger' role='button'>维修</a> -->
<%-- 							<a href="${url}?method=edit" target="homeTab" rel="myModal" id="dayin" class="btn btn-primary" role="button">打印</a> --%>
						</div>
						
						<div class="col-md-4">
							<ul class="la_ul">
								<li style="background:#336868;"><div>清场</div></li>
								<li style="background:#9ccc65;"><div>布置完成</div></li>
								<li style="background:#673ab7;"><div>预定</div></li>
								<li style="background:#fea625;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li>
								<li style="background:#e84e40;"><div>装修</div></li>
							</ul>
						</div>
					</div>
					<div id='mourning' style="padding:10px;">
							<%@ include file="/WEB-INF/jsp/ye/appointment/mourning.jsp" %>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</section>
<c:if test="${dept!=2}">	
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default" style='margin-bottom:0px'>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-12">
								<c:choose>
									<c:when test="${isEdit !=null  }">
										<small class='pull-right btns-print'>
											<a class='btn btn-info' style='border-color:#673ab7;background-color:#673ab7' rel='myModal' target='dialog' href='appointment.do?method=order&id=${id }'>保存</a>
											<a class='btn btn-default btn-margin' href='appointment.do?method=listOrder' data-addData='true' target='homeTab'>返回</a>
										</small>
									</c:when>
									<c:otherwise>
										<c:if test="${isShow ==1 }">
											<small class='btns-print'>
												<a class='btn btn-info' href='appointment.do?method=listOrder' target='homeTab'>进入预约列表</a>
											</small>
										</c:if>
										<small class='pull-right btns-print'>
											<c:if test="${isShow ==1 }">
												<a class='btn btn-info' style='border-color:#673ab7;background-color:#673ab7' rel='myModal' target='dialog' href='appointment.do?method=order'>预定</a>
											</c:if>
											<a class='btn btn-warning' href='commissionOrder.do?method=edit' data-addData='true' target='homeTab'>洽谈</a>
										</small>
									</c:otherwise>
								</c:choose>
								
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	
</section>
</c:if>
<script>
//当前时间改变
var data={
	mourningCode:null,
	mourningBeginTime:null,
	mourningEndTime:null,
	farewellCode:null,
	farewellTime:null,
	funeralCode:null,
	funeralTime:null,
};
function getTarget(obj){
	return obj.siblings(':eq(0)').text().trim();
}
function getTime(str,obj,num,bol){
	var time=$('#'+str).val();
	var hour=' ';
	if(bol){
		var date=new Date(time)-0+(Math.ceil((obj.index())/2)-1)*86400000;
		time=new Date(date).toLocaleDateString().replace(/\//g,'-');
		hour+=obj.index()%2==0?'15:00':'11:00';
	}else if(num==1){
		hour+=obj.parent().siblings(':eq(0)').find('td:eq('+obj.index()+')').text().trim();
	}else{
		hour+=obj.parent().parent().siblings('thead').find('th:eq('+Math.ceil((obj.index())/2)+')').text().trim();
		hour=hour.split(':')[0]+((obj.index())%2==0?':30':':00');
	}
	return time+hour;
}
//时间控件改变时间
function changeDate(obj){
	var move=$(obj).attr('data-change');
	var target=$(obj).attr('data-tar');
	var method=$(obj).attr('data-method');
	var date=$('[data-tar='+target+']').val();
	var day=new Date(date);
	if(move==='go'){
		date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
		$('[data-tar='+target+']').val(date);
	}
	if(move==='back'){
		date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
		$('[data-tar='+target+']').val(date);
	}
	if(method=='funeralDateChange'){
		changeDate2('farewell','farewellDateChange',date);
	}
	if(method=='farewellDateChange'){
		changeDate2('funeral','funeralDateChange',date);
	}
	$.ajax({
	    url: "appointment.do",
	    dataType:'html',
	    cache: false,
	    data:{method:method,date:date,id:'${id}'},
	    success: function (json) {
	    	$("#"+target).html(json);
			initTab();
			
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
}

//火化炉时间与告别厅时间  相互关联
function changeDate2(a,b,c){
	var target=a;
	var method=b;
	var date=c;
	$.ajax({
	    url: "appointment.do",
	    dataType:'html',
	    cache: false,
	    data:{method:method,date:date,id:'${id}'},
	    success: function (json) {
	    	$("#"+target).html(json);
	    	$('[data-tar='+target+']').val(date);
			initTab();
			
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
}
</script>