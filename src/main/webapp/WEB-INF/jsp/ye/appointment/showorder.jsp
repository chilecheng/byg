<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form id="detail" action="appointment.do" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input name="method" value="${method}" type="hidden" />
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">预约登记</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
<!-- 								<div class="col-md-12 height-align"> -->
<!-- 									<label >预约流水号：</label> -->
<!-- 									<span id="">YYDJ20161201005</span> -->
<!-- 								</div> -->
								<div class="col-md-12  height-align">
									<label class="lab-2">姓名：</label>
									<input type="text" class="list_input input-hometab" id="dName" name="dName" value="${appointment.name }">
<!-- 									<button class="btn-psearch btn btn-info" type="button" data-name="search">搜索</button> -->
<!-- 									<input type="text" readonly="" class="list_input" id="dNameId" data-id="dNameId" name="dNameId" hidden="" value=""> -->
<!-- 									<a data-dialog="dialog" class="hide" target="dialog" rel="myModal2" href="dispatcher.do?list=comprehensive-choose">选择</a> -->
<!-- 									<span class="nowarning" data-warning="warning"></span> -->
								</div>
								<div class="col-md-12 height-align">
									<label >家属电话：</label>
									<input type="text" class="list_input input-hometab" id="fphone" name="fphone" value="${appointment.phone }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>守灵室号：</label>
									<input readonly name='mourningCode' class="list_input input-hometab" value="${appointment.mourningCode }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>守灵时间：</label>
									<input readonly name='mourningBeginTime' class="list_input input-hometab" value="<fmt:formatDate value='${appointment.mourningBeginTime }' pattern='yyyy-MM-dd HH:mm'/>">
									<span>--</span>
									<input readonly name='mourningEndTime' class="list_input input-hometab" value="<fmt:formatDate value='${appointment.mourningEndTime }' pattern='yyyy-MM-dd HH:mm'/>">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>告别厅号：</label>
									<input readonly name='farewellCode' class="list_input input-hometab" value="${appointment.farewellCode }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>告别时间：</label>
									<input readonly name='farewellTime' class="list_input input-hometab" value="<fmt:formatDate value='${appointment.farewellTime }' pattern='yyyy-MM-dd HH:mm'/>">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>火化炉号：</label>
									<input readonly name='funeralCode' class="list_input input-hometab" value="${appointment.funeralCode }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>火化预约时间：</label>
									<input readonly name='funeralTime' class="list_input input-hometab" value="<fmt:formatDate value='${appointment.funeralTime }' pattern='yyyy-MM-dd HH:mm'/>">
								</div>
								
								
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
<!-- 							<button type='submit' class='btn btn-info btn-margin'>保存</button> -->
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
				
	</form>
</body>
<script>
	
</script>