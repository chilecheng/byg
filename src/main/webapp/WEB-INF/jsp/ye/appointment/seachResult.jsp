<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
	<style>
		.wk-btns .btn{
			padding:3px 25px;
			border-radius:8px;
		}
		.wk-btns{
			text-align:center;
			margin-top:0px;
			padding:10px 0px;
			border-top:1px solid #ccc;
		}
		.nomargin{
			margin:0px;
		}
	</style>
</head>
<body>	
	<div class="modal-dialog" role="document" style="margin-top: 103.5px;width:800px;">
		<div class="modal-content">
			<div class='row nomargin'>
				<div class='col-md-12'>
					<section class="content-header">
						<i class="title-line"></i>
						<div class="title">查询结果</div>
					</section>	
				</div>	
				<div class='col-md-12'>
					<table class='table table-bordered table-hover'>
						<thead>
							<tr>
								<th><input type="checkbox" hidden group="id"></th>
								<th>流水号</th>
								<th>登记姓名</th>
								<th>联系电话</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${page.list}" var="u">
						<tr>
						<td><input  name='id' data-name='${u[2]}' data-phone='${u[3]}'  
						 value='${u[0]}' class="checkBoxCtrl" type='radio'/></td>
						<td>${u[1]}</td>
						<td>${u[2]}</td>
						<td>${u[3]}</td>
						</tr>
						</c:forEach>
						
						</tbody>
					</table>
					<form action="${url}" id="pageForm" onsubmit="return changeThis(this);">
							<input type="hidden" name="method" value="NameList">
							<%@ include file="/common/pageHead.jsp"%>
							<%@ include file="/common/pageFood.jsp"%>
					</form>
				</div>
			</div>
			<div class='row nomargin'>
				<div class='col-md-12 wk-btns'>
					<button type="submit" data-sure='yes'  class="btn btn-info">确定</button>
					<a checkone="true" checkname="id" data-check='one' class='hide' target="dialog" rel="myModal" role="button"></a>
					<button type="submit" hidden data-miss='miss' data-dismiss="modal"></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
				</div>
			</div>
				
		</div>
	</div>
<script>
	(function(){
		$('[data-sure="yes"]').click(function(){
			var size=$('[name="id"]:checked').size();
			if(size){
				var input=$('[name="id"]:checked');
				$('#dName').val(input.attr('data-name'));
				$("#id").val(input.val());
				$("#fphone").val(input.attr('data-phone'));
				$('[data-miss="miss"]').click();
			}else{
				$('[data-check="one"]').click();
			}
		})
	})();
	function changeThis(form) {
		$("button").prop("disabled", "disabled");
		var $form = $(form);
		$.ajax({
			type : 'POST',
			url : $form.attr("action"),
			data : $form.serializeArray(),
			dataType : "html",
			cache : false,
			success : function(json){
				$("#myModal").html(json);
				$("#"+rel).modal("show");
				initTab();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				toastr["error"](XMLHttpRequest.status);
				$("button").removeAttr("disabled");
			}
		});
		return false;
	};
</script>	
</body>