<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="box-body">
					<div id="example5_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover1">
									<c:forEach items="${list3 }" var="u" varStatus="i">
										<c:choose>
											<c:when test="${i.index==0 }">
												<thead>
													<tr role="row">
														<th>${u[0]}</th>
														<c:forEach begin="1" end="7" var="a">
															<th colspan="2"><fmt:formatDate value="${u[a]}" pattern="MM.dd"/></th>
														</c:forEach>
													</tr>
												</thead>
											</c:when>
											<c:otherwise>
												<tr role="row">
													<td><font color="blue">${u[0].name }</font></td>
													<c:forEach begin="1" end="14" var="a">
														<c:if test="${u[a]==null }">
															<td width="6%" onclick="mourning_click( ${i.index},${a },this)" id="mourning_${i.index}_${a }"></td>
														</c:if>
														<c:if test="${u[a]!=null }">
															<c:choose>
																<c:when test="${u[a].flag == IsFlag_Decrate }">
																	<td width="6%" data-num='${u[a].flag}' title="点击更改维修状态">
																		<a style="color:#fff" href="repairRecord.do?method=info&type=mour&id=${u[a].id}&itemId=${u[0].id }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Lock }">
																	<td width="6%" data-num='${u[a].flag}' title="${u[a].order.name},<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		${u[a].order.name }
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_YY}">
																	<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																		${u[a].appointmentName }
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Bzwc || u[a].flag == IsFlag_QC}">
																	<td width="6%" data-num='${u[a].flag }' title="${u[a].order.name },<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		${u[a].order.name}
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_QC}">
																	<td width="6%" data-num='${u[a].flag }' title="${u[a].order.name },<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		${u[a].order.name}
																	</td>
																</c:when>
																<c:otherwise>
																
																	<td width="6%" data-num='${u[a].flag }' ></td>
																</c:otherwise>
															</c:choose>
														</c:if>
													</c:forEach>
												</tr>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
<script>
(function(){	
	$('#example5_wrapper table tr td[data-num]').each(function(){
			//console.log(this);
			var data=$(this).attr('data-num');
			switch (data){
				case '${IsFlag_Yse }':
					$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
					break;
				case '${IsFlag_Bzwc}':
					$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
					break;
				case '${IsFlag_Lock }':
					$(this).css({'backgroundColor':'#fea625','color':'#fff'});
					break;
				case '8':
					$(this).css({'backgroundColor':'#673ab7','color':'#fff'});
					break;
				case '${IsFlag_Decrate }':
					$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
					break;
				case '${IsFlag_QC}':
					$(this).css({'backgroundColor':'#336868','color':'#fff'});
					break;
			}
	})
})();

var mourningNum=0;
var mourningBegin;
var mourningEnd;
//单元格单击
function mourning_click(i,a,ele){
	var type=null;
	if(mourningNum>=2){
		$("#"+mourningBegin).css('background-color','#fff');
		$("#"+mourningEnd).css('background-color','#fff');
		mourningNum=0;
	}
	if(mourningNum==1){
		mourningEnd="mourning_"+i+"_"+a;		
		data.mourningEndTime=getTime('mourningDate',$(ele),2,true);
		
		if(mourningBegin=='mourning_'+i+'_'+a  && type !='esc'){
			$("#"+mourningBegin).css('background-color','#fff');
			data.mourningCode=null;
			data.mourningBeginTime=null;
			data.mourningEndTime=null;
			type='esc';
			mourningNum=0;
		}
		
	}else if(mourningNum==0){
		mourningBegin="mourning_"+i+"_"+a;
		data.mourningBeginTime=getTime('mourningDate',$(ele),2,true);
		data.mourningCode=getTarget($(ele));
		data.mourningEndTime=null;
	}
	if(type !='esc'){
		$(ele).css('background-color','#f39c12');
		mourningNum=mourningNum+1;			
	}
}



</script>