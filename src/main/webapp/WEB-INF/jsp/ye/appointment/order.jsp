<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form id="Form" action="appointment.do" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input name="method" value="${method}" type="hidden" />
			<input name="id" id="id"  type="hidden"  value="${editId }"/>
 			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">预约登记</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-12  height-align">
									<label class="lab-2">姓名：</label>
									<input type="text" class="list_input input-hometab" id="dName" name="dName" value="${name }">
<!-- 									<button class="btn-psearch btn btn-info" type="button" data-name="search">搜索</button> -->
<!-- 									<input type="text" readonly class="list_input" id="dNameId" data-id="dNameId" name="dNameId" hidden="" value=""> -->
<%-- 									<a data-dialog="dialog" class="hide" target="dialog" rel="myModal2" href="${url}?method=NameList">选择</a> --%>
<!-- 									<span class="nowarning" data-warning="warning"></span> -->
								</div>
								<div class="col-md-12 height-align">
									<label >家属电话：</label>
									<input type="text" class="list_input input-hometab" id="fphone" name="fphone" value="${phone }">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>守灵室号：</label>
									<input readonly name='mourningCode' class="list_input input-hometab" >
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>守灵时间：</label>
									<input readonly name='mourningBeginTime' id="mourningBeginTime" class="list_input input-hometab">
									<span>--</span>
									<input readonly name='mourningEndTime'  id="mourningEndTime" class="list_input input-hometab">
									<button class="btn  btn-warning" style="padding:5px;" type="button" onclick="changeLong()" data-name="long">守灵延长</button>
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>告别厅号：</label>
									<input readonly name='farewellCode' class="list_input input-hometab" >
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>告别时间：</label>
									<input readonly name='farewellTime' class="list_input input-hometab">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>火化炉号：</label>
									<input readonly name='funeralCode' class="list_input input-hometab">
								</div>
								<div class="col-md-12 height-align">
									<label class='lab-6'>火化预约时间：</label>
									<input readonly name='funeralTime' class="list_input input-hometab">
								</div>
								
								
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<button type='submit' class='btn btn-info btn-margin'>保存</button>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
				
	</form>
</body>
<script>
//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   { //author: meizz   
	var o = {   
	 "M+" : this.getMonth()+1,                 //月份   
	 "d+" : this.getDate(),                    //日   
	 "h+" : this.getHours(),                   //小时   
	 "m+" : this.getMinutes(),                 //分   
	 "s+" : this.getSeconds(),                 //秒   
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	 "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
	 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
	 if(new RegExp("("+ k +")").test(fmt))   
	fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
}
//守灵时间延长一个月
function changeLong(){
	var str=$("#mourningBeginTime").val();
	var startTime=new Date(str);
	var stopTime="";
	if(str !=null && str !=""){
		var x=startTime.getTime()+(30*24*60*60*1000);
		stopTime=new Date(x).Format("yyyy-MM-dd hh:mm");
		$("#mourningEndTime").val(stopTime);
	}
}
function ndatet(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]+' '+arrTime[1]);
	return checkDate;
}
function ndate(checkTime){
	var arrTime=checkTime.split(' ');
	var arrDate=arrTime[0].split('-');
	var checkDate=new Date(arrDate[1]+' '+arrDate[2]+','+arrDate[0]);
	return checkDate;
}
	$(function(){
		//将选择的数据填写到表单中
		for(var key in data){
			if(key=='mourningEndTime'&&data[key]==null){
				$('[name='+key+']').val(data['mourningBeginTime'])
			}else{
				$('[name='+key+']').val(data[key]);
			}
		}
		//点击搜索按钮,,搜索预约登记中的记录
		var href=$('[data-dialog="dialog"]').attr('href');
		$('[data-name="search"]').click(function(){
 			var dName = $("#dName").val().trim();
 			if(dName==""){
 				$('[data-warning="warning"]').html('请输入登记姓名').removeClass('nowarning').addClass('warning');
 				return;
 			}else{
 				var $form = $("#Form");
 				$.ajax({
 				    url: $form.attr("action"),
 				    dataType:'json',
 				    cache: false,
 				    data:{method:'dNameSearch',dName:dName},
 				    success: function (json) {
 				    	var size = json.size;
	 				   	if(size===0){
	 		 				$('[data-warning="warning"]').html('无预约信息').removeClass('nowarning').addClass('warning');
	 		 				$("#id").val('');
	 		 				$("#fphone").val('');
	 		 			}else if(size===1){
	 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
	 		 				$("#dName").val(typeof(json.name)!="undefined"?json.name:'');
	 		 				$("#id").val(typeof(json.id)!="undefined"?json.id:'');
                   			$("#fphone").val(typeof(json.phone)!="undefined"?json.phone:'');
	 		 			}else{
	 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
	 		 				$('[data-dialog="dialog"]').attr('href',href+"&name="+dName);
	 		 				$('[data-dialog="dialog"]').click();
	 		 			}
	 				}
 				});
 			}
 		});
	})
</script>