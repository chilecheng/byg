<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<script type="text/javascript">


//死者基本信息
</script>


</head>
<body>


<section class="content">
<form action="${url}" id="form" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="currentTime" value="${currentTime}" >
		
			<div class="box box-warning" role="document">
				
				
					
			<!-- 基本信息 -->
						
						<p class='p border-B'>死者信息</p>
						
							<div class="box-body border-B padding-B">
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者姓名：</label>
<!-- 									<select style="width: 150px;position:absolute;" class="input-content" name="dName" id="dName"> -->
<%-- 										${dName} --%>
<!-- 									</select> -->
<!-- 									<input  type="text" autocomplete="off" name="input-box" class="input-show" id="show"/> -->
<!-- 									<div id="prompt-content" class="show-menu"></div> -->
									<input type="text" class="list_select input-hometab" id='dName' name='dName' value=''/>
									<button class='btn-psearch btn btn-info' type='button' data-name='search'>搜索</button>
									<input type="text" readonly class="list_input" id='dNameId' data-id="dNameId" name='dNameId' hidden value=''/>
									<a data-dialog='dialog' class='hide' target='dialog' href='${url}?method=dNameList'>选择</a>
									<span class='nowarning' data-warning='warning'></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>证件号码：</label> 
									<input type="text" class="list_select input-hometab" name="certificateCode" id="certificateCode" value="">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者性别：</label> 
									<input  type="text" class="list_select input-hometab" name="dSex" id="dSex" value="">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者年龄：</label>
									<input  type="text"  class="list_select  input-hometab" name="dAge" id="dAge" value="">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>火化日期：</label> 
									<input  type="text" data-provide="datetimepicker" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select input-hometab" id="cremationTime" name="cremationTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>存入时间：</label> 
									<input  type="text" data-provide="datetimepicker" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select input-hometab" id="beginDate" name="beginDate" value="<fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>存放位置：</label> 
									<input class="list_select input-hometab" name="placeCode" id="placeCode" value="${placeCode}">
										
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>到期时间：</label> 
									<input type="text" data-provide="datetimepicker" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select input-hometab" id="endDate" name="endDate" value="<fmt:formatDate value="${endTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>存放月数：</label> 
									<input  type="text" class="list_select input-hometab" name="depositLong" id="depositLong">
									
								</div>
						
							</div>
							
							<p class='p border-B'>寄存人信息</p>
						
						
						<!-- 寄存人信息 -->
							<div class="box-body border-B padding-B">
								<div class="col-md-6 height-align">
									<label  class='lab-5'>寄存人姓名：</label> 
									<input  type="text" class="list_select input-hometab" name="sName" value="">
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-5'>寄存证编号：</label> 
									<input  type="text" class="list_select input-hometab" name="sCode" value="">
	
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-5'>与死者关系：</label> 
									<select  class="list_select input-hometab" name="fAppellationId" id="fAppellationId">
										${fAppellationOption}
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-5'>寄存人电话：</label>
									<input  type="text" class="list_select input-hometab" name="sPhone" value="">
								</div>
								<div class="col-md-10">
									<label  class='lab-5'>寄存人住址：</label> 
									<input type="text" class="list_select input-hometab" name="sAddr" value="">
								</div>
								<div class="col-md-10">
									<label  class='lab-5'>备注：</label> 
									<textarea type="text" class="form-control textarea-hometab" name="comment" value=""></textarea>
								</div>
								
							</div>
					<div class="box-body">
							<div  class="container-fluid">
								<div class="col-md-5 height-align">
								<label class='lab-6'>经办人：</label>
								${agentUser}
								</div>
							
							
							
								<div class="col-md-7 height-align">
								<label class='lab-6'>办理时间：</label>
								${currentTime}
								</div>
								<div class="col-md-12">
								<label  class='lab-6'>单据号：</label> 
								<input  type="text" class="list_select input-hometab" name="code" value="${commissionOrder.code }">
								</div>
							</div>
							
						</div>
					
					<div class="modal-footer btns-hometab ">
						<small class='pull-right'>
							<button type="submit"  class="btn btn-info">保存</button>
							<input type="reset" class="btn btn-color-9E8273" value="重置" />
							<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
						</small>
					</div>
						
					</div>
					</form>
</section>
<!--  <script src="js/select.js"></script> -->
<script>
	 	(function(){
	 		/*选择按钮初始化*/
	 		$('[data-choose="get"]').click(function(){
	 			data_group.name=$(this).prev().prev().attr('data-name');
	 			data_group.id=$(this).prev().attr('data-id');
	 			$(this).next().click();
	 		});
	 		
			var href=$('[data-dialog="dialog"]').attr('href');
	 		$('[data-name="search"]').click(function(){
	 			var dName = $("#dName").val().trim();
	 			if(dName==""){
	 				$('[data-warning="warning"]').html('请输入死者姓名').removeClass('nowarning').addClass('warning');
	 				return;
	 			}else{
	 				var $form = $(pageForm);
	 				$.ajax({
	 				    url: 'buyWreathRecord.do',
	 				    dataType:'json',
	 				    cache: false,
	 				    data:{method:'dNameSearch',dName:dName},
	 				    success: function (json) {
	 				    	var size = json.size;
		 				   	if(size===0){
		 		 				$('[data-warning="warning"]').html('无死者信息').removeClass('nowarning').addClass('warning');
		 		 			}else if(size===1){
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$("#certificateCode").val(typeof(json.certificateCode)!="undefined"?json.certificateCode:'');
		 		 				$("#dName").val(typeof(json.name)!="undefined"?json.name:'');
		 		 				$("#dNameId").val(typeof(json.name)!="undefined"?json.dNameId:'');
	                   			$("#dSex").val(typeof(json.dSex)!="undefined"?json.dSex:'');
	                   			$("#dAge").val(typeof(json.dAge)!="undefined"?json.dAge:'');
	                   			$("#mourningPlace").val(typeof(json.mourningPlace)!="undefined"?json.mourningPlace:'');
	                   			$("#beginDate").val(typeof(json.beginDate)!="undefined"?json.beginDate:'');
	                   			$("#endDate").val(typeof(json.endDate)!="undefined"?json.endDate:'');
	                   			$("#farewellPlace").val(typeof(json.farewellPlace)!="undefined"?json.farewellPlace:'');
	                   			$("#farewellDate").val(typeof(json.farewellDate)!="undefined"?json.farewellDate:'');
	                   			$("#cremationTime").val(typeof(json.cremationTime)!="undefined"?json.cremationTime:'');
		 		 				
		 		 			}else{
		 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 		 				$('[data-dialog="dialog"]').attr('href',href+"&dName="+dName);
		 		 				$('[data-dialog="dialog"]').click();
		 		 			}
		 				    	
		 				}
	 				});
	 			}
	 		});
	 	})()
	 </script>
</body>