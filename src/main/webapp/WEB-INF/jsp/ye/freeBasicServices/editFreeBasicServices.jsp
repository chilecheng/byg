<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">基本服务项目申免</div>
</section>
<head>
<script type="text/javascript">

var sum=0;//标识
var sTotal=0;//减免项目合计总金额
var delNumber=[]//删除记录

//减免项目方法
function item(name,pice,number,total,comment,baseId){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='checkbox' id=checkbox_"+baseId+" name='checkId' value="+baseId+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemName('"+baseId+"') name='itemId' id='itemId_"+baseId+"'> "+name+"</select></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' readonly='readonly' class='list_table' id='pice_"+baseId+"' name='pice' value="+parseFloat(pice)+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='text' class='list_table' onkeyup=changeNumber('"+baseId+"') onkeydown='if(event.keyCode==13) return false' id='number_"+baseId+"' name='number' value="+number+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name='total' id=total_"+baseId+" class='list_table' value="+parseFloat(total)+"></td>";
	str+="<td><textarea type='text' class='list_table' id='comment_"+baseId+"'  name='comment'>"+comment+"</textarea></td></tr>";
	$("#deductions").append(str);
}

//增加收费记录
function addRow(){
	//sum 作为增加记录的暂时ID来区别		
	sum++;
	item("${defaultAdd.defaultName}","${defaultAdd.defaultPice}",1,"${defaultAdd.defaultPice}","",sum)
	sTotal+=parseFloat('${defaultAdd.defaultPice}')
	$("#sFont").html(sTotal+"元");
}

//初始化减免项目
$(document).ready(function(){	
	<c:forEach var="u" items='${ReductionDList}'>
		item("${u.name}","${u.pice}","${u.number}","${u.total}","${u.comment}","${u.baseId}")
		sTotal+=parseFloat('${u.total}')
		$("#sFont").html(sTotal+"元");
	</c:forEach>
	
});


//保存数据 
function saveDate(callback){
	
	var freePersonId = document.getElementById("freePersonId");
	var hardPersonId = document.getElementById("hardPersonId");
	var freeIdString="";//免费对象类别
	var hardIdString="";//重点救助对象类别
	$("input[name='freePersonId']:checked").each(function(){
		var id = $(this).attr("value")
		freeIdString+=id+",";
	});
	$("input[name='hardPersonId']:checked").each(function(){
		var id = $(this).attr("value")
		hardIdString+=id+",";
	});
	//减免表格数据
	var Table = document.getElementById("deductions");
	var saveOut=[];
	for(var i=0;i<Table.rows.length;i++){
		var inner=[];
		var allString="";
		for(var j=0;j<Table.rows[i].cells.length;j++){
			//找到所有表格里的元素
			var val=$(Table.rows[i].cells[j].children[0]).val();
			allString=allString+val+",";			
		}
		allString=allString+$("#id").val();
		
		saveOut[i]=allString;
	}
	var com_comment = $("#commission_comment").val();
	
	$.ajax({
		url:"freeBasicServices.do",
		traditional: true,
		data:{"method":"save",saveData:saveOut,hardIdString:hardIdString,com_comment:com_comment,
			delNumber:delNumber,freeIdString:freeIdString,baseReductionId:$("#id").val(),
			},
		type:"post",
		dataType:"json",

		success:callback
	});
}

//项目名称改变
function itemName(id){
	$.ajax({
	    url: "freeBasicServices.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
    		sTotal=sTotal-$("#total_"+id).val();
    		sTotal+=json.pice;
			$("#sFont").html(sTotal+"元");
	    	$("#total_"+id).val(json.pice);
    		$("#pice_"+id).val(json.pice);
	    	$("#number_"+id).val(1);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
} 

//数目改变
function changeNumber(id){
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	var pice=$("#pice_"+id).val();
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			$("#sFont").html(sTotal+"元");
			$("#total_"+id).val(pice*number);
	
} 

//删除内容
function delHtml() {
	var num = 0;	
	$("input[name='checkId']:checked").each(function(){
		var id = $(this).attr("value")
			//此处若删除的记录条ID为暂时ID，不作变动，若是数据库中ID，删除时记录，方便在后台删除数据库中的记录
			if(id.length>5){
				delNumber.push(id);
			};			
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		$(this).parent().parent().remove();
		sum--;
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
</script>
<script type="text/javascript">
	function addchecked(){
		
	}
</script>
</head>
<body>
	<section class="content">
		<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);" >
			
			<!-- 传值给后台java -->
			<input type="hidden" name="method" value="${method}">
			<input type="hidden" id="id" name="id" value="${id}">
<%-- 			<input type="hidden" id="name" name="name" value="${commissionOrder.name }"> --%>
<%-- 			<input type="hidden" name="commission_order_id" id="commission_order_id" value="${id}">		 --%>
				
					<div class='box box-warning'>
						<!-- 死者信息 -->
						<div id="b">
							<p class='p border-B'>死者信息</p>
							
								<div class="box-body border-B padding-B">
									<div class="col-md-5 height-align">
										
										<label class="lab-2">死者姓名：</label>
										<c:choose>
											<c:when test="${commission.name!=null}">
									    	${commission.name}
									    	</c:when>
										</c:choose>
									</div>

									<div class="col-md-5 height-align">
										<label class="lab-2">死亡日期：</label>
										<c:choose>
											<c:when test="${commission.dTime!=null}">
									    	<fmt:formatDate value="${commission.dTime}" pattern="yyyy-MM-dd HH:mm"/>
									   		</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="label-4">卡号：</label>
										<c:choose>
											<c:when test="${commission.cardCode!=null}">
									    	${commission.cardCode}
									    	</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-2">死者地址：</label>
										<c:choose>
											<c:when test="${commission.dAddr!=null}">
												 ${commission.dAddr}
											</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-2">死者年龄：</label>
										<c:choose>
											<c:when test="${commission.age!=null}">									    	
									    	${commission.age}
									    	</c:when>
										</c:choose>
									</div>

									<div class="col-md-5 height-align">
										<label class="lab-6">预约火化时间：</label>
										<c:choose>
											<c:when test="${commission.cremationTime!=null}">									    	
									    	<fmt:formatDate value="${commission.cremationTime}" pattern="yyyy-MM-dd HH:mm"/>
									    	</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-2">死者性别：</label>
										<c:choose>
											<c:when test="${commission.sexName!=null}">
									    	${commission.sexName}
									    	</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-3">身份证：</label>
										<c:choose>
											<c:when test="${commission.certificateCode!=null}">
									    	${commission.certificateCode}
									    	</c:when>
										</c:choose>
									</div>
									<div class="col-md-12">
										<label class="lab-6">免费对象类别：</label>										
											<c:forEach items="${freePersonList}" var="u">
												<c:choose>
													<c:when test="${fn:contains(freePerson,u.id) }">
														<input type="checkbox" name="freePersonId" checked="checked" value="${u.id }">
														${u.name }&nbsp;&nbsp;&nbsp;&nbsp;
													</c:when>
													<c:otherwise>
														<input type="checkbox" name="freePersonId" value="${u.id }">
														${u.name }&nbsp;&nbsp;&nbsp;&nbsp;
													</c:otherwise>
												</c:choose>
											</c:forEach>
									</div>
									<div class="col-md-12">
										<label class="lab-2" style='width:160px;margin-left:-30px;'>重点救助对象类别：</label>
											<c:forEach items="${hardPersonList}" var="h">
												<input type="hidden" value="${hardPerson }" name="222">
												<c:choose>
													<c:when test="${fn:contains(hardPerson,h.id) }">													
														<input type="checkbox" name="hardPersonId" checked="checked" value="${h.id }">
														${h.name }&nbsp;&nbsp;&nbsp;&nbsp;
													</c:when>
													<c:otherwise>
														<input type="checkbox" name="hardPersonId" value="${h.id }">
														${h.name }&nbsp;&nbsp;&nbsp;&nbsp;
													</c:otherwise>
												</c:choose>
											</c:forEach>
									</div>
								</div>
								<!-- 经办人信息  不可改-->
								<p class='p border-B'>经办人信息</p>
								
									<div class="box-body border-B padding-B">
									<div class="col-md-5 height-align">
										<label class="lab-3">姓名：</label>
										<c:choose>
											<c:when test="${commission.fName!=null}">
									    	${commission.fName}
									    	</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-6">与死者关系：</label>
										<c:choose>
											<c:when test="${relationShip!=null}">
									    	${relationShip}
									    	</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-1">联系号码：</label>
										<c:choose>
											<c:when test="${commission.fPhone!=null}">
									    	${commission.fPhone}
									    	</c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-1">身份证号：</label>
										<c:choose>
											<c:when test="${commission.fCardCode!=null}">
									    	${commission.fCardCode}
									    	</c:when>
										</c:choose>
									</div>	
								</div>
								<!-- 减免详情 -->
								<p class='p border-B'>减免详情</p>
								<div class="box-body border-B padding-B">	
									<div class="col-md-12">
										<small class="pull-right btns-buy">
<!-- 											<button type="button" onclick="addRow()" -->
<!-- 												class="btn btn-warning">添加</button> -->
											<a href="${url}?method=insert" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
											<button type="button" onclick="delHtml()"
												class="btn btn-danger">删除</button>
										</small>
									</div>
									<table class="table table-bordered" style="margin-top: 60px;">
										<thead>
											<tr>
												<th width="30px">全选<input type="checkbox"
													class="checkBoxCtrl" group="checkId" /></th>
												<th width="150px">名称</th>
												<th width="100px">单价（元）</th>
												<th width="80px">数量</th>
												<th width="100px">小计</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody id="deductions" >

										</tbody>
										<tbody>
											<tr>
												<td style='width: 60px'>合计：</td>
												<td colspan='6' style='text-align: left;'>
												<font color='red' id="sFont"> 0元 </font></td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- 备注 -->
								<div class="col-md-12">
									<label>备注:</label> 
									<textarea type="text" class="form-control" name="commission_comment" id="commission_comment"  style="width:1065px;height: 80px">${commission.comment}</textarea>
								</div>
								
								
								<div class="box-body border-B padding-B">
									<!-- 保存、重置 、返回-->
									<div class="container-fluid">
										<div class="col-md-12">
											<small class="pull-right btns-hometab"> 	
												<a href="javascript:void(0)" onclick="saveDate(homeAjaxDone)"

												class="btn btn-info" role="button">保存</a>								

												<a href="${url}?method=edit&id=${id}" target="homeTab" checkOne="true" rel="myModal" class="btn  btn-color-9E8273" role="button">重置</a>
											
												<a href="${url}?method=list" type="button"  target="homeTab" class="btn btn-default ">返回</a>
											</small>
										</div>
									</div>
								</div>
						</div>
					</div>
		</form>
	</section>
</body>


