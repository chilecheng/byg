<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<script type="text/javascript">

</script>

</head>
<body>
	<section class="content">
		<form id='detail' action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${commission.id}">
		<input type="hidden" id="name" value="${commission.name }">
			<div class="box-body">
				<div class="modal-dialog nav-tabs-custom" style='width:750px'>
					<div>
						<!-- 死者信息 -->
						<!--startprint0-->
						<div class="tab-pane" id="b">
						<p class='p border-B'>死者信息</p>
							<div class="box-body border-B padding-B">
								<div class="col-md-5 height-align">
									<label class='lab-2'>死者姓名：</label> 
									<c:choose>
									    <c:when test="${commission.name!=null}">
									    	${commission.name}
									    </c:when>
									</c:choose> 
								</div>
							
								<div class="col-md-7 height-align">
									<label class="lab-2">死亡日期：</label> 
									<c:choose>
									    <c:when test="${commission.dTime!=null}">
									    	<fmt:formatDate value="${commission.dTime}" pattern="yyyy-MM-dd HH:mm"/>
									    </c:when>
									</c:choose> 
								</div>
								
								
								<div class="col-md-5 height-align">
									<label class='lab-2'>死者年龄：</label>
									<c:choose>
									    <c:when test="${commission.age!=null}">									    	
									    	${commission.age}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-2'>死者性别：</label> 
									<c:choose>
									    <c:when test="${commission.sexName!=null}">
									    	${commission.sexName}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-5 height-align">
									<label class='label-4'>卡号：</label>
									<c:choose>
									    <c:when test="${commission.cardCode!=null}">
									    	${commission.cardCode}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-7 height-align">
									<label class="lab-3">身份证：</label> 
									<c:choose>
									    <c:when test="${commission.certificateCode!=null}">
									    	${commission.certificateCode}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-5 height-align">
									<label>重点救助对象：</label> 
									<c:choose>
									    <c:when test="${hardPerson!=null}">
									    	${hardPerson}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-7 height-align">
									<label class="">预约火化时间：</label> 
									<c:choose>
									    <c:when test="${commission.cremationTime!=null}">									    	
									    	<fmt:formatDate value="${commission.cremationTime}" pattern="yyyy-MM-dd HH:mm"/>
									    </c:when>
									</c:choose> 
								</div>		
								
								
								<div class="col-md-12 height-align">
									<label>免费对象类别：</label> 
									<c:choose>
									    <c:when test="${freePerson!=null}">
									    	${freePerson}
									    </c:when>
									</c:choose> 
								</div>
								
								<div class="col-md-12 height-align">
									<label class="lab-2">死者地址：</label> 
									<c:choose>
									    <c:when test="${commission.dAddr!=null}">
									    	${commission.dAddr}
									    </c:when>
									</c:choose> 
								</div>
							</div>
						<!--endprint0-->
						<!-- 经办人信息 -->
						<!--startprint1-->
						<div class="tab-pane" id="b">
						<p class='p border-B'>经办人信息</p>
							<div class="box-body border-B padding-B">
							
								<div class="col-md-5 height-align">
									<label class='lab-3'>姓名：</label> 
									<c:choose>
									    <c:when test="${commission.fName!=null}">
									    	${commission.fName}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-1'>联系号码：</label> 
									<c:choose>
									    <c:when test="${commission.fPhone!=null}">
									    	${commission.fPhone}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-5 height-align">
									<label class="">与死者关系：</label> 
									<c:choose>
									    <c:when test="${appellationName!=null}">
									    	${appellationName}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-7 height-align">
									<label class="lab-1">身份证号：</label> 
									<c:choose>
									    <c:when test="${commission.fCardCode!=null}">
									    	${commission.fCardCode}
									    </c:when>
									</c:choose> 
								</div>															
							</div>
								<div class="col-md-6">
									<label>死者身份证图片:</label>
									<div class='ID-pic' style="cursor:inherit" data-name='name'>
										<img width=325px;  src="${commission.dIdcardPic }" alt="images">
									</div>
								</div>
								<div class="col-md-6">
									<label>家属身份证图片:</label>
									<div class='ID-pic' style="cursor:inherit" data-name='fname'>
										<img width=325px;  src="${commission.eIdcardPic }" alt="images">
									</div>
								</div>	
							<p class='p border-B'>减免详情</p>
							<div class="box-body border-B padding-B">
								<table class="table table-bordered" style="margin-top: 20px;">
									<thead>
										<tr>
											<th width="150px">名称</th>
									        <th width="100px">单价（元）</th>
									        <th width="80px">数量</th>
									        <th width="100px">小计</th>
									         <th  width="100px">备注</th>
										</tr>
									</thead>
									<tbody id="articles">
										<c:choose>
										    <c:when test="${fn:length(ReductionDList)==0 }">
											    <tr>
									    			<td colspan="5">
												  		<font color="Red">没有相关信息</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${ReductionDList}" var="u">
												<tr role="row">
													<td>${u.name}</td>
													<td><fmt:formatNumber value="${u.pice}" pattern="#" type="number"/></td>
							    					<td>${u.number}</td>
							    					<td><fmt:formatNumber value="${u.total}" pattern="#" type="number"/></td>	
							    					<td>${u.comment }</td>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='5' style='text-align: left;' >
											<font color='red' id="aFont">
											
											<fmt:formatNumber value="${allTotal}" pattern="#" type="number"/>元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								
							</div>
						</div>
						<!--endprint1-->			
						<!--startprint2-->
					</div>
						<!--endprint2-->
					<div class="box-foot">
						<div  class="container-fluid">				
							<div class="col-md-12 btns-dialog btns-print">
									<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
									<a href="javascript:showprint()" class="btn  btn-color-ab47bc  btn-margin">打印</a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</form>
	</section>
</body>