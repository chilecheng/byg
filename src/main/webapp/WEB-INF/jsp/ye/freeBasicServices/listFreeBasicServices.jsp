<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">基本服务项目申免</div>
</section>
<style type="text/css">
	.table td a.light{
		width:50px;
		height:20px;
		/* background-color:lightblue; */
		text-align:center;
		line-height:20px;
		color:#fff;
		border-radius:4px;
	}
	.table td a.light.right{
		
		background-color:#ab47bc;
	}
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
$('#search').change(function(){
	if($("#find").find("option:selected").text()=="卡号"){
		homeSearch(pageForm);		
	}
})
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div>
						<div class="box-header with-border">
							<h3 class="box-title" style="color:#4EC2F6">查询</h3>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-chevron-down"></i>
								</button>
							</div>
							<!-- /.box-tools -->
						</div>
					</div>
					<div class="box-body border-B">
						
						
						<div class="col-md-3">
							<label>类型：</label> 
							<select class="list_input nopadding-R" id="find" name="find">
<!-- 								<option value='findByCode'>卡号</option> -->
<!-- 								<option value='findByName'>姓名</option> -->
								${searchOption }
							</select>
						</div>
						<div class="col-md-8">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" id='search' name='search' value='${search}' placeholder='单行输入'/>
						</div>
						<div class="col-md-12">
							<label>申请时间：</label> 
							<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime}">
							<span class='timerange'>--</span>  
							<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}">
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
					<div class="row">
					<div class="col-xs-12">
						<div class="box" style="border: 0;margin-bottom:0">
							
							<div class="box-body">
								<div class='cl'>
									<small class='btns-buy'>
										<a href="${url}?method=add" target="homeTab"  rel="myModal" class="btn btn-color-ff7043" role="button">添加</a>
										<a href="${url}?method=edit&id=" target="homeTab" checkName="id" checkOne="true" rel="myModal" class="btn btn-warning" role="button">修改</a>
									</small>
								</div>
								<div id="example2_wrapper"
									class="dataTables_wrapper form-inline dt-bootstrap margin-T">
									<div class="row">
										<div class="col-sm-12">
											<table class="table table-bordered table-hover">
												<thead>
													<tr role="row">
														<th width="10"></th>
														<th>申请日期</th>
														<th>死者姓名</th>
														<th>死亡日期</th>
														<th>火化时间</th>
														<th>经办人姓名</th>
														<th>结算时间</th>
														<th>操作</th>
													</tr>
												</thead>
												<tbody>
												 <c:choose>
												    <c:when test="${fn:length(page.list)==0 }">
													    <tr>
											    			<td colspan="7">
														  		<font color="Red">没有基本服务减免申请记录</font>
													 		</td>
													    </tr>
												    </c:when>
												    <c:otherwise>
													    <c:forEach items="${page.list }" var="u">
														<tr role="row">
															<td><input type="radio" name="id" value="${u.id}"></td>
															<td>${u.createTime}</td>
									    					<td id="name_Click"><a href="${url}?method=show&id=${u.id}"  target="dialog" rel="myModal" ><font color="blue">${u.commissionOrder.name }</font></a></td>
									    					<td><fmt:formatDate value="${u.commissionOrder.dTime}" pattern="yyyy-MM-dd HH:mm"/></td>
									    					<td><fmt:formatDate value="${u.commissionOrder.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
									    					<td>${u.commissionOrder.fName}</td>	
									    					<td><fmt:formatDate value="${u.payTime}" pattern="yyyy-MM-dd HH:mm"/></td>
									    					<td>
									    					<a href="${url}?method=show&id=${u.id}" class='light right' target="dialog" rel="myModal">打印</a>
									    					</td>								    					
														</c:forEach>
													</c:otherwise>
												</c:choose>
												</tbody>
											</table>
										</div>
									</div>
									<%@ include file="/common/pageFood.jsp"%>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>
				<!-- <div class="box-footer clearfix btns-print" style="border-top:none;">
	              <a href="javascript:void(0)" class="btn btn-color-ab47bc pull-right">打印</a>
	            </div> -->
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
</section>
<script>
$(function(){
	timeRange();
})
</script>