<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">基本服务项目申免</div>
</section>
<head>
<script type="text/javascript">

var sum=0;//标识
var sTotal=0;//减免项目合计总金额
var delNumber=[]//删除记录

//减免项目方法
function item(name,pice,number,total,comment,baseId){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='checkbox' id=checkbox_"+baseId+" name='checkId' value="+baseId+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemName('"+baseId+"') name='itemId' id='itemId_"+baseId+"'> "+name+"</select></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' readonly='readonly' class='list_table' id='pice_"+baseId+"' name='pice' value="+pice+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='text' class='list_table' onkeyup=changeNumber('"+baseId+"') onkeydown='if(event.keyCode==13) return false' id='number_"+baseId+"' name='number' value="+number+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name='total' id=total_"+baseId+" class='list_table' value="+total+"></td>";
	str+="<td><textarea type='text' class='list_table' id='comment_"+baseId+"'  name='comment'>"+comment+"</textarea></td></tr>";
	$("#deductions").append(str);
}
$(document).ready(function(){
	//初始化项目
	<c:forEach var="u" items='${baseList}'>	
		sum++;
		item("${u[1]}","${u[3]}","${u[2]}","${u[3]}","",sum);
		sTotal+=parseFloat('${u[3]}');
		$("#sFont").html(sTotal+"元");
	</c:forEach>	
});

//根据死者姓名查找对应的数据
function findByName(){
	$.ajax({
		url:"freeBasicServices.do",
		dataType:'json',
		data:{method:'findByName',name:$("#input-box").val()},
		success:function(nameData){
		},
		error:function(){
			alert("error");
		}
	});
}

//增加收费记录
function addRow(){
	//sum 作为增加记录的暂时ID来区别		
	sum++;
	item("${defaultAdd.defaultName}","${defaultAdd.defaultPice}",1,"${defaultAdd.defaultPice}","",sum)
	sTotal+=parseFloat('${defaultAdd.defaultPice}')
	$("#sFont").html(sTotal+"元");
}



//保存数据 
function saveDate(callback){
	
	var freePersonId = document.getElementById("freePersonId");
	var hardPersonId = document.getElementById("hardPersonId");
	var freeIdString="";//免费对象类别
	var hardIdString="";//重点救助对象类别
	$("input[name='freePersonId']:checked").each(function(){
		var id = $(this).attr("value")
		freeIdString+=id+",";
	});
	$("input[name='hardPersonId']:checked").each(function(){
		var id = $(this).attr("value")
		hardIdString+=id+",";
	});
	//减免表格数据
	var Table = document.getElementById("deductions");
	var saveOut=[];
	for(var i=0;i<Table.rows.length;i++){
		var inner=[];
		var allString="";
		for(var j=0;j<Table.rows[i].cells.length;j++){
			//找到所有表格里的元素
			var val=$(Table.rows[i].cells[j].children[0]).val();
			allString=allString+val+",";			
		}
		allString=allString+$("#id").val();
		
		saveOut[i]=allString;
	}
	//经办人身份证号
	var fCardCode=$("#fCardCode").val();
	
	$.ajax({
		url:"freeBasicServices.do",
		traditional: true,
		data:{"method":"addSave",saveData:saveOut,hardIdString:hardIdString,fCardCode:fCardCode,
			delNumber:delNumber,freeIdString:freeIdString,baseReductionId:$("#id").val(),dNameId:$("#dNameId").val()
			},
		type:"post",
		dataType:"json",
		success:callback
	});
}


//项目名称改变
function itemName(id){
	$.ajax({
	    url: "freeBasicServices.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
    		sTotal=sTotal-$("#total_"+id).val();
    		sTotal+=json.pice;
			$("#sFont").html(sTotal+"元");
	    	$("#total_"+id).val(json.pice);
    		$("#pice_"+id).val(json.pice);
	    	$("#number_"+id).val(1);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
} 

//数目改变
function changeNumber(id){
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	var pice=$("#pice_"+id).val();
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			$("#sFont").html(sTotal+"元");
			$("#total_"+id).val(pice*number);
	
} 

//删除内容
function delHtml() {
	var num = 0;	
	$("input[name='checkId']:checked").each(function(){
		var id = $(this).attr("value")
			//此处若删除的记录条ID为暂时ID，不作变动，若是数据库中ID，删除时记录，方便在后台删除数据库中的记录
			if(id.length>5){
				delNumber.push(id);
			};			
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		$(this).parent().parent().remove();
		sum--;
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
</script>

</head>
<body>
	<section class="content">
		<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);" >
			
			<!-- 传值给后台java -->
			<input type="hidden" name="method" value="${method}">
			<input type="hidden" id="id" name="id" value="">
				
					<div class='box box-warning'>
						<!-- 死者信息 -->
						<div id="b">
						<p class='p border-B'>死者信息</p>
								<div class="box-body border-B padding-B">
									<div class="col-md-6  height-align input-menu">

										<label class="lab-2">死者姓名：</label>										
										<input type="text" class="required list_select" id='dName' name='dName' value=''/>
										<button class='btn-psearch btn btn-info' type='button' data-name='search'>搜索</button>
										<input type="text" readonly class="required list_select" id='dNameId' data-id="dNameId" name='dNameId' hidden value=''/>
										<a data-dialog='dialog' class='hide' target='dialog' href='${url}?method=dNameList'>选择</a>
										<span class='nowarning' data-warning='warning'></span>
									</div>
									<div class="col-md-6 height-align">            

										<label class="lab-2">死亡日期：</label>
										<fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd HH:mm"/>
										<input data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="dieTime" name="dieTime" value="<fmt:formatDate value="${dieTime }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>  
<!-- 										<input type="text" class="list_input input-content" data-id="reservation" id="dieTime" name='dieTime' value=''> -->
									</div> <!-- data-provide="datetimepicker" -->
									<div class="col-md-6 height-align">

										<label class="label-4">卡号：</label>

										<input type="text"  class="required list_select" name="code" id="code" value="">
									</div>
									<div class="col-md-6 height-align">
										<label class="lab-2">死者地址：</label>
										<input type="text"  class="required list_select" name="address" id="address" value="">
									</div>
									<div class="col-md-6 height-align">

										<label class="lab-2">死者年龄：</label>

										<input type="text"  class="required list_select" name="age" id="age" value="">
									</div>

									<div class="col-md-6 height-align">

										<label class="lab-6">预约火化时间：</label>
										<!-- data-provide="datetimepicker" -->
										
										 <input data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="cremationTime" name="cremationTime" value="<fmt:formatDate value="${cremaTime }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>  
									  </div>
									<div class="col-md-6 height-align">

										<label class="lab-2">死者性别：</label>

										<input type="text"  class="required list_select" name="sex" id="sex" value="">
									</div>
									<div class="col-md-6 height-align">
										<label class="lab-3">身份证：</label>
										<input type="text"  class="required list_select" name="cardcode" id="cardcode" value="">
									</div>
									<div class="col-md-12">
										<label class="lab-6">免费对象类别：</label>										
											<c:forEach items="${freePersonList}" var="u">												
												<input type="checkbox" name="freePersonId" value="${u.id }">
												${u.name }&nbsp;&nbsp;&nbsp;&nbsp;
											</c:forEach>
									</div>
									<div class="col-md-12">
										<label class="lab-2" style='width:160px;margin-left:-30px'>重点救助对象类别：</label>
											<c:forEach items="${hardPersonList}" var="h">
												<input type="checkbox" name="hardPersonId" value="${h.id }">
												${h.name }&nbsp;&nbsp;&nbsp;&nbsp;
											</c:forEach>
									</div>
								</div>
								<!-- 经办人信息  不可改-->
								<p class='p border-B'>办人信息</p>
									<div class="box-body border-B padding-B">
									<div class="col-md-5 height-align">
										<label class="lab-3">姓名：</label>
										<input type="text"  class="required list_select" name="fName" id="fName" value="">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-6">与死者关系：</label>
										<select style="width: 169px" class="list_select" name="fAppellationId" id="fAppellationId"> 
									 		${fAppellationOption }
										</select>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-1">联系号码：</label>
										<input type="text"  class="required list_select" name="fPhone" id="fPhone"  value="">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-1">身份证号：</label>
										<input type="text"  class="required list_select" name="fCardCode" id="fCardCode" value="">
									</div>	
								</div>
								<!-- 减免详情 -->
								<p class='p border-B'>减免详情</p>
								<div class="box-body border-B padding-B">	
									<div class="col-md-12">
										<small class="pull-right btns-buy">
<!-- 											<button type="button" onclick="addRow()" -->
<!-- 												class="btn btn-warning">添加</button> -->
											<a href="${url}?method=insert" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
											<button type="button" onclick="delHtml()"
												class="btn btn-danger">删除</button>
										</small>
									</div>
									<table class="table table-bordered" style="margin-top: 60px;">
										<thead>
											<tr>
												<th width="30px">全选<input type="checkbox"
													class="checkBoxCtrl" group="checkId" /></th>
												<th width="230px">名称</th>
												<th width="100px">单价（元）</th>
												<th width="80px">数量</th>
												<th width="100px">小计</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody id="deductions" >

										</tbody>
										<tbody>
											<tr>
												<td style='width: 60px'>合计：</td>
												<td colspan='6' style='text-align: left;'>
												<font color='red' id="sFont"> 0元 </font></td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- 备注 -->
								<div class="col-md-4">
									<label>备注:</label> 
									<textarea type="text" class="form-control" name="comment"  style="width:1213px;height: 80px">
										<c:choose>
										    <c:when test="${commission.comment!=null}">
										    	${commission.comment}
										    </c:when>
										</c:choose> 
									</textarea>
								</div>
								
								
								<div class="box-body border-B padding-B">
									<!-- 保存、重置 、返回-->
									<div class="container-fluid">
										<div class="col-md-12">
											<small class="pull-right btns-hometab"> 	
												<a href="javascript:void(0)" onclick="saveDate(homeAjaxDone)"
												 class="btn btn-info" role="button">保存</a>								
												<a href="${url}?method=add" target="homeTab" checkOne="true" rel="myModal" class="btn btn-color-9E8273" role="button">重置</a>											
												<a href="${url}?method=list" type="button"  target="homeTab" class="btn btn-default">返回</a>
											</small>
										</div>
									</div>
								</div>
						</div>
					</div>
		</form>
	</section>
<script>
(function(){
	var href=$('[data-dialog="dialog"]').attr('href');
	$('[data-name="search"]').click(function(){
		var dName = $("#dName").val().trim();
		if(dName==""){
			$('[data-warning="warning"]').html('请输入死者姓名').removeClass('nowarning').addClass('warning');
			return;
		}else{
			var $form = $(pageForm);
			$.ajax({
			    url: $form.attr("action"),
			    dataType:'json',
			    cache: false,
			    data:{method:'dNameSearch',dName:dName},
			    success: function (json) {
			    	//转换时间格式
			    	function changeDate(a){
			    		var date=new Date(a);
			    		var year=date.getFullYear();
			    		var month=(date.getMonth()+1<10?"0"+(date.getMonth()+1):(date.getMonth()+1));
			    		var day=(date.getDate()<10?"0"+date.getDate():date.getDate());
			    		var time=date.toTimeString().split(" ")[0];
			    		return year+'-'+month+'-'+day+' '+time;
			    	}
			    	
			    	json.dieTime!=null&&(json.dieTime=changeDate(json.dieTime));
			    	json.cremaTime!=null&&(json.cremaTime=changeDate(json.cremaTime));
			    	var size = json.size;
				   	if(size===0){
		 				$('[data-warning="warning"]').html('无死者信息').removeClass('nowarning').addClass('warning');
		 			}else if(size===1){
		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 				//基本信息
		 				$("#code").val(typeof(json.cardCode)!="undefined"?json.cardCode:'');		 				
		 				$("#address").val(typeof(json.addr)!="undefined"?json.addr:'');
		 				$("#dName").val(typeof(json.dName)!="undefined"?json.dName:'');
		 				$("#dNameId").val(typeof(json.dNameId)!="undefined"?json.dNameId:'');
		       			$("#sex").val(typeof(json.sex)!="undefined"?json.sex:'');
		       			$("#age").val(typeof(json.dAge)!="undefined"?json.dAge:'');
		       			$("#cardcode").val(typeof(json.dieId)!="undefined"?json.dieId:'');
		       			$("#dieTime").val(typeof(json.dieTime)!="undefined"?json.dieTime:'');
		       			$("#cremationTime").val(typeof(json.cremaTime)!="undefined"?json.cremaTime:'');
		       			//死者家属
		       			$("#fName").val(typeof(json.fName)!="undefined"?json.fName:'');
		       			$("#fPhone").val(typeof(json.fPhone)!="undefined"?json.fPhone:'');
		       			$("#fAppellationId").html(typeof(json.fAppellationOption)!="undefined"?json.fAppellationOption:'');
		       			$("#fCardCode").val(typeof(json.fCardCode)!="undefined"?json.fCardCode:'');
	       			 		 				
		 			}else{
		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
		 				$('[data-dialog="dialog"]').attr('href',href+"&dName="+dName);
		 				$('[data-dialog="dialog"]').click();
		 			}
				    	
				}
			});
		}
	});
})()
</script>
</body>