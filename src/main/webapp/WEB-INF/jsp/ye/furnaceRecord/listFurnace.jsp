<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化炉列表</div>
</section>
<script>
$(function(){
	timeRange();
});	
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<c:if test="${type eq 'three' }"><!--业务三科进来的会传这个，区分入炉和转炉  -->
						<input type="hidden" name="type" value="${type }">
					</c:if>
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>类型：</label> 
							<select class="list_select" name="searchType" style="width:52%;">
								${searchOption }
							</select>
						</div>
						<div class="col-md-8" style="margin-left:-94px">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" name="searchVal" value="${searchVal}" placeholder="查询值">
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<label>预约火化日期：</label> 
							<input data-id='beginDate' class="list_select" id="dTime" name="beginDate" value="${beginDate }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<span class='timerange'>--</span>  
							<input data-id='endDate' class="list_select" id="dTime" name="endDate" value="${endDate }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<button type="submit" class="btn btn-info" style="line-height: 15px;width: 10%">搜索</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					
					<small class='btns-print'>
						<c:choose>
						<c:when test="${type eq 'three' }">
						<a href="${url }?method=confirm&id=" target="dialog" data-code="9" data-check="check" rel="myModal" checkOne="true" checkName="id" class="btn btn-info" role="button">火化确认</a>
						<a href="${url }?method=arrange&select=true&noRef=no&id=" target="dialog" data-code="6-9-11" data-check="check" rel="myModal2" checkOne="true" checkName="id" class="btn btn-warning" role="button">火化入炉</a>
						<a href="${url}?method=arrange&type=change&changeto=pt&noRef=no&id=" data-code="9-11" data-check="check" target="dialog" rel="myModal2" checkOne="true" checkName="id" class="btn btn-danger" role="button">转普通炉</a>	
						</c:when>
						<c:otherwise>
							<!--有change属性的能选择炉号  -->
							<a href="${url}?method=arrange&type=change&changeto=pt&noRef=no&id=" data-code="9-11" data-check="checkChangePt" target="dialog" rel="myModal2" checkOne="true" checkName="id" class="btn btn-danger" role="button">转普通炉</a>
							<a href="${url}?method=arrange&type=change&select=true&changeto=ty&noRef=no&id=" data-code="9-11" data-check="checkChangeTy" target="dialog" rel="myModal2" checkOne="true" checkName="id" class="btn btn-info"  role="button">转特约炉</a>
							<a href="${url}" target="homeTab" class="btn btn-default " role="button">返回</a>
						</c:otherwise>
					</c:choose>
					</small>
					<small class="pull-right btns-print">
<%-- 						<a href="${url}?method=" target="dialog" rel="myModal" class="btn btn-success" role="button">导出EXCEL</a> --%>
<%-- 						<a href="${url}?method=" target="dialog"  rel="myModal" class="btn btn-color-ab47bc" role="button" id="d">打印</a> --%>
					</small>
					
					
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"></th>
											<th>IC卡号</th>
											<th>死者姓名</th>
											<th>死者性别</th>
											<th>年龄</th>
											<th>火化炉号</th>
											<th>火化炉类型</th>
											<th>火化开始时间</th>
											<th>预约火化时间</th>
											<th>业务状态</th>
											<th>是否收费</th>
											<th class="hide">隐藏列</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr width="20">
								    			<td colspan="11">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row" width="10">
												<td><input type="radio" name="id" value="${u.order.id }"></td>
						    					<td>${u.order.cardCode }</td>
						    					<td><font color="blue">${u.order.name }</font></td>
						    					<td>${u.order.sexName }</td>
						    					<td>${u.order.age }</td>
						    					<td>${u.furnace.name }</td>
						    					<td class="furType">${u.order.furnaceTypeName }</td>
						    					<td><fmt:formatDate value="${u.beginTime }" pattern="yyyy-MM-dd HH:mm"/></td>
						    					<td><fmt:formatDate value="${u.orderTime }" pattern="yyyy-MM-dd HH:mm"/></td>
						    					<td><span class="flag" data-num="${u.flag }"></span></td>
						    					<td class="payFlag" class='state1'>${u.payFlag }</td>
						    					<td class="hide" class='state1'>${u.id }</td>
						    				</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<script>
	(function(){
		$('.flag').each(function(){
			var data=$(this).attr('data-num');
			switch (data){
				case '1':
					$(this).html('未火化').css({'color':'#fea625'});
				break;
				case '6':
					$(this).html('进行中').css({'color':'#738ffe'});
					break;
				case '5':
					$(this).html('已火化').css({'color':'#9ccc65'});
					break;
			}
		});
		$('.furType').each(function(){
			var data=$(this).text();
			switch (data){
				case '特约炉':
					$(this).css({'color':'#fea625'});
					break;
			}
		});
		$('.payFlag').each(function(){
			var data=$(this).text();
			switch (data){
				case '1':
					$(this).text('已收费').css({'color':'#9ccc65'});
					break;
				case '2':
					$(this).text('未收费').css({'color':'#fea625'});
					break;
			}
		});
	})();
</script>
</section>
