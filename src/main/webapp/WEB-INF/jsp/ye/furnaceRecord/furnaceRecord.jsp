<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">火化炉调度</div>
</section>
<style type="text/css">
	#example2_wrapper table tbody>tr:first-child,#example2_wrapper table tbody>tr:nth-child(3){
		background-color:#EBEBEB;
	}
	#example2_wrapper table tbody>tr:nth-child(2)>td:first-child{
		vertical-align:middle;
	}
	#example2_wrapper table tbody>tr>td:not(:first-child):hover,#example2_wrapper table tbody>tr:last-child>td:hover{
		background-color:#EBEBEB;
	}
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url }" id="pageForm"  class="outerform"  onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>日期：</label> <!-- data-provide="datetimepicker" -->
							<input type="text" data-id="beginDate" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="beginDate" name="date" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
<!-- 							 <button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button> -->
						</div>
						<div class="col-md-3 btns-list">
							<a href="${url}?method=listInfo" target="homeTab" rel="myModal" id="liebiao" class="btn btn-info" role="button">进入列表</a>
							<a href='repairRecord.do?method=repair&type=furnace' target='dialog' rel='myModal' class='btn btn-danger' role='button'>维修</a>
<%-- 							<a href="${url}?method=edit" target="homeTab" rel="myModal" id="dayin" class="btn btn-primary" role="button">打印</a> --%>
						</div>
						
						<div class="col-md-6">
							<ul class="la_ul">
								<li style="background:#9ccc65;"><div>完成</div></li>
								<!-- <li style="background:#738ffe;"><div>进行</div></li> -->
								<li style="background:#673ab7;"><div>预约</div></li>
								<li style="background:#fea625;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li>
								<li style="background:#e84e40;"><div>装修</div></li>
							</ul>
						</div>
					</div>
				</form>
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<c:forEach begin="1" items="${list }" var="u" varStatus="i">
							<div class="col-sm-12">
								<c:if test="${u[0].code==7 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dOb }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dOb }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
								
								<c:if test="${u[0].code==8 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObEight }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dObEight }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
								<c:if test="${u[0].code==9 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObNine }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dObNine }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
								<c:if test="${u[0].code==10 }">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<c:forEach items="${dObTen }" var="ob" begin="0" end="14" varStatus="in">
												<c:choose>
													<c:when test="${in.index==0 }">
														<td>${ob }</td>
													</c:when>
													<c:otherwise>
														<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</tr>
										<tr>
											<td rowspan='3'>${u[0].name }</td>
											<c:forEach begin="1" end="14" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
										</tr>
										<tr>
											<c:forEach items="${dObTen }" var="ob" begin="15" varStatus="in">
												<td><fmt:formatDate value="${ob }" pattern="HH:mm"/></td>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<c:forEach begin="15" end="24" var="a">
												<c:if test="${u[a]==null }">
													<td></td>
												</c:if>
												<c:if test="${u[a]!=null }">
													<c:choose>
														<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Wc || u[a].flag == IsFlag_Jx}">
															<td data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																<a style="color:#fff" href="${url}?method=detail&id=${u[a].commissionOrderId}&fname=${u[0].name }&o_time=${u[a].orderTime}&wkId=${u[a].workerId}&b_time=${u[a].beginTime}&crId=${u[a].createUserId}&cremationFlag=${u[a].order.cremationFlag}&comment=${u[a].comment}" target="dialog" rel="myModal">${u[a].order.name}</a>
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Lock }">
															<td  data-num='${u[a].flag}' title='${u[a].order.name},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].order.name}
															</td>
														</c:when>
														<c:when test="${u[a].flag == 8}">
															<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].orderTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																${u[a].appointmentName }
															</td>
														</c:when>
														<c:when test="${u[a].flag == IsFlag_Decrate }">
															<td  data-num='${u[a].flag}' title='点击更改维修状态'>
																<a style="color:#fff" href="repairRecord.do?method=info&type=furnace&id=${u[a].id}&iname=${u[0].name }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
															</td>
														</c:when>
														<c:otherwise>
															<td data-num='${u[a].flag}'></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:forEach>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</c:if>
							</div>
							</c:forEach>
						</div>
					</div>
				</div>
<!-- 				<div class="box-footer clearfix btns-print"> -->
<!-- 	              <a href="javascript:void(0)" class="btn btn-color-ab47bc pull-right">打印</a> -->
<!-- 	            </div> -->
				
			</div>
			<!-- /.col -->
		</div>
	</div>
</section>
<script>
	(function(){
		$('#example2_wrapper table tbody>tr td[data-num]').each(function(){
			//console.log(this);
			var data=$(this).attr('data-num');
			switch (data){
				case '1':
					$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
					break;
				case '6':
					$(this).css({'backgroundColor':'#738ffe','color':'#fff'});
					break;
				case '8':
					$(this).css({'backgroundColor':'#673ab7','color':'#fff'});
					break;
				case '3':
					$(this).css({'backgroundColor':'#fea625','color':'#fff'});
					break;
				case '4':
					$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
					break;
				case '5':
					$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
					break;
			}
		})
	})();
</script>