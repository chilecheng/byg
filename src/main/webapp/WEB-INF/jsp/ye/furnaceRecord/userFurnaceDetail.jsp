<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-T nopadding-B">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者姓名：</label>
									<span>${co.name }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者编号：</label>
									<span>${co.code }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>联系人：</label>
									<span>${co.fName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者性别：</label>
									<span>${co.sexName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>联系人电话：</label>
									<span>${co.fPhone }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者年龄：</label>
									<span>${co.age }</span>
								</div>
							</div>
							
						</div>
						<p class="col-md-12 p border-B">调度信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-2'>火化炉号：</label>
									<span>${fname }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>火化预约时间：</label>
									<span>${o_time }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-3'>火化工：</label>
									<span>${worker }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>火化开始时间：</label>
									<span>${b_time }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-3'>录入员：</label>
									<span>${creater }</span>
								</div>
								<div class="col-md-6 height-align">
									<label  class='lab-2' id="status">火化状态：</label>
									<span id="cremationFlag" data-num="${cremationFlag }"></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='label-4'>备注：</label>
									<span>${comment }</span>
								</div>
							</div>
						</div>
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>
<script>
	(function(){
		
		var data = $('#cremationFlag').attr('data-num');
		switch (data){
			case '1':
				$('#cremationFlag').html('已火化').css({'color':'#29b6f6'});
			break;
			case '2':
				$('#cremationFlag').html('未火化').css({'color':'#738ffe'});
				break;
	}
	})();
</script>