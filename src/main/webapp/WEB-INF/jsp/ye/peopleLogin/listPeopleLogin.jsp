
<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.sql.*,java.util.*,java.text.*"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">登记信息表</div>
</section>
<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
						<div class="box-body" style='padding:0px;'>
							<!-- <div class="row"  id='main-content'>
								<div class="col-md-12 border-B padding-B" style='padding-right:30px;'>
									<div class="row">
										<div class="col-md-12">
											<small class='btns-print pull-right'>
												<a href="" class="btn btn-primary" role="button">导出WORD</a>
												<a href="" class="btn btn-success" role="button">导出EXCEL</a>
												<a href="" class="btn btn-color-ab47bc" role="button">打印</a>
											</small>
											
										</div>
									</div>
								</div>
							</div> -->
							<P class='col-md-12 border-B p'>死者信息</P>
							<div class='col-md-12 border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者登记号：</label>
										<span>${listfc.code}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者姓名：</label>
										<span>${listfc.dname}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者年龄：</label>
										<span>${listfc.age}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者性别：</label>
										<span>${listfc_sex}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>身份证地址：</label>
										<span>${listfc.address}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>身份证号：</label>
										<span>${listfc.certificateCode}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>接运地址：</label>
										<span>${listfc.pickAddr}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死亡类型：</label>
										<span>${listfc.deadTypeName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死亡原因：</label>
										<span>${listfc.deadReasonName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>证明单位：</label>
										<span>${listfc.proveUnitName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死亡时间：</label>
										<span>${listfc.dTime}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>到馆时间：</label>
										<span>${listfc.aTime}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>火化预约时间：</label>
										<span>${listfc.cremationTime}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>火化时间：</label>
										<span>${listfc.fTime}</span>
									</div>
								</div>
							</div>
							<P class='col-md-12 border-B p'>家属信息</P>
							<div class='col-md-12 border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>家属姓名：</label>
										<span>${listfc.fname}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>住址：</label>
										<span>${listfc.fAddr}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-5'>联系方式：</label>
										<span>${listfc.fphone}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>与死者关系：</label>
										<span>${listfc.fAppellation}</span>
									</div>
								</div>
							</div>
							<P class='col-md-12 border-B p'>业务信息</P>
							<div class='col-md-12 border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>死者编号：</label>
										<span>${listfc.code}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>告别厅号：</label>
										<span>${listfc.fareWellName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>出殡礼仪：</label>
										<span>${poinfo}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>收费状态：</label>
										<span class='changeColor'>${listfc.payFlagName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-3'>纸馆：</label>
										<span>
										<c:if test="${fn:length(pinfo)==0}"> 
										     &nbsp;无
										</c:if>
										<c:forEach items="${pinfo}" var="u" varStatus="sta">
										     <c:if test="${sta.last==true}">
										        ${u.paperName}
										     </c:if>
										     <c:if test="${sta.last==false}">
										        ${u.paperName}
										     </c:if>
										</c:forEach>			
										</span>
									</div>
								</div>
							</div>
							<p class='col-md-12 border-B p'>调度信息</p>
							<div class='col-md-12 border-B padding-B nomargin-B'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>火化状态：</label>
										<span class='changeColor'>
										   ${cremation_flag}
										</span>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-5' style='width:130px;margin-left:-46px;'>火化预约开始时间：</label>
										<span><fmt:formatDate value="${listfc.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>火化炉类型：</label>
										<span>
											<c:if test="${fireType==1}">
												普通炉
											</c:if>
											<c:if test="${fireType==2}">
												特约炉
											</c:if> 
										</span>
									</div>
									<c:if test="${fireType==2}">
										<div class='col-md-6 height-align'>
											<label class='lab-1'>火化炉号：</label>
											<span>
											   ${furnaceName}
											</span>
										</div>
									</c:if>
								</div>
							</div>
						</div>
						
				</div>
				<!-- /.col -->
				<div class='box' style='padding:10px;'>
					<div class='row'>
						<div class='col-md-6  height-align'>
							<label  class='lab-1'>确认时间：</label>
							<span><%=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new java.util.Date())%></span>
						</div>
						<div class='col-md-6 height-align'>
							<small class='pull-right btns-hometab'>
							<%-- <a href="fireCheckIn.do?method=arrange&fphone=${listfc.fphone}&dname=${listfc.dname}&code=${listfc.code}&age=${listfc.age}&sex=${listfc_sex}&fname=${listfc.fname}&cremationtime=${listfc.cremationTime}&cremationFlag=${cremation_flag}&cid=${listfc.cid }&cardCode=${listfc.cardCode }&recordId=${recordId }"
								   target="homeTab" rel="myModal" id="sure" class="btn btn-info btn-right" role="button">确认</a> --%>
								<a href="peopleLogin.do" target="homeTab" id="return" class="btn btn-default btn-right" role="button">返回</a>
						    </small>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
</form>
<script>
	(function(){
		$('.changeColor').each(function(){
			var txt=$(this).html().trim();
			txt.indexOf('已')===0?($(this).css('color','#9CCC65')):($(this).css('color','#e84e40'));// e84e40  9CCC65
		})
	})()
</script>