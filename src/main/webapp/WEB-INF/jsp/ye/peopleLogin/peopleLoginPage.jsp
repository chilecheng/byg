<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">信息查询</div>
</section>
<style type="text/css">
	
	#pageForm label{
		font-size:16px;
	}
</style>
<form action="peopleLogin.do" id="pageForm" onsubmit="return validateHomeCallback(this,homeAjaxDone);"><!-- validateHomeCallback(this,homeAjaxDone) -->
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="people" type="hidden">
	<section class='content'>
		<div class='box box-warning'>
			<div class='row'  style='height:400px;padding-top:140px;'>
				<div class='col-md-8 col-md-offset-2 btns-list'>
					<label>卡号：</label>
					<input type="text" class="list_input" style='min-width:60%;' id='readCard' name='readCard' value=''/>
					<!-- <a href="fireCheckIn.do"  target="homeTab" rel="myModal" id="search" class="btn btn-info" role="button">搜索</a> -->
					<button type='submit' class="btn btn-info" style='margin-top:-6px;'>搜索</button>
					<button type='reset' class="btn btn-default" style='background-color:#9E8273;color:#fff;margin-top:-6px;'>重置</button>
				</div>
			</div>
		</div>
	</section>
</form>
