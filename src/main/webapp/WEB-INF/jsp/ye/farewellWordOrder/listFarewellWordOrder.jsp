<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">告别厅安排任务单</div>
</section>
<script type="text/javascript">
$(function(){
	$('#farewellNumber').text('');
});

function changeDate(){
	
	$('#pageForm').submit();
}

(function() {
	function changeColor() {
		var value = $(this).val();
		var txt = $(this).find("option[value='" + value + "']").text();
		switch (txt) {
		case '已处理':
			$(this).css('color', '#4DA2FD');
			break;
		case '未处理':
			$(this).css('color', '#D73925');
			break;
		/* case '已领用':
			$(this).css('color', '#00A65A');
			break; */
		}
	}
	$(".state").change(changeColor);
	$(".state").each(changeColor);
	$(".state").each(function() {
		$(this).find('option').each(function() {
			var txt = $(this).text();
			switch (txt) {
			case '已处理':
				$(this).css('color', '#4DA2FD');
				break;
			case '未处理':
				$(this).css('color', '#D73925');
				break;
			/* case '已领用':
				$(this).css('color', '#00A65A');
				break; */
			}
		})
	});
})();


function GaiBian(v) {
	var value = $(v).val();
	var cvalue = $(v).parent().parent().find('input:checkbox').val();

	$.ajax({
		type : 'POST',
		url : "farewellWordOrder.do?method=isdel&isdel=" + value + "&id=" + cvalue,
		dataType : "json",
		success : dialogAjaxDone,
	});
}
</script>

<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<%@ include file="/common/pageHead.jsp"%>
						<input name="method" value="${method}" type="hidden" />
						<div class="box-body">
						<%-- <iframe class='frame' id="reportFrame" width="100%" src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=farewellWordOrder.cpt&op=view"></iframe> --%>
							<div class="col-md-4">
								<label>类型：</label>
								<select class="list_select nopadding-R" name="searchType" id="searchType" style="width: 52%;">
									${searchOption }
								</select>
							</div>
							<div class="col-md-8" style="margin-left: -94px">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" name="searchVal" id="searchVal" value="${searchVal}" placeholder="查询值">
							</div>
							<div class="col-md-12">
								<label>告别时间：</label> 
								<input type="text"  data-id='beginDate' class="list_select"  name="beginDate" value="<fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span> 
								<input type="text"  data-id='endDate' class="list_select"  name="endDate" value="<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<input type="hidden" name="deadId" value="dac2a1a540a64403ad807f96a7813d86" >
							</div> 
							<div class="col-md-2">
								<button type="submit" class="btn btn-info btn-search btn-search-left">搜索</button>
							</div>
						</div>
				</div>
			</div>
		</div>
		 <div class="row">
			<div class="col-xs-12">
				<div class="box" style="border-top: 0">
					<div class="box-header with-border">
						${beginDate }——<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />
						<small class="pull-right btns-print">
							<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认处理吗" class="btn btn-info" role="button">批量处理</a>
							<a href="${url}?method=isdel&isdel=${Isdel_No }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认未处理吗" class="btn btn-danger" role="button">批量未处理</a>
<!-- 							<a href="javascript:myprint()" class="btn btn-color-ab47bc" role="button">打印</a> -->
							<%-- <a href="${url}?method=exportExcel" class="btn btn-success" role="button">导出EXCEL</a>
							<a href="${url}?method=exportWord" class="btn btn-primary" role="button">导出WORD</a>
							<a href="${url}?method=checkFlag&checkFlag=${Check_Yes }&id=" target="ajaxTodo" checkName="checkboxId"   warm="确认审核吗" class="btn btn-color-ab47bc" id="d" role="button">打印</a>
							<a href="${url}?method=checkFlag&checkFlag=${Check_No }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认取消吗" class="btn btn-success " role="button">导出Excel</a>
							<a href="${url}?method=checkFlag&checkFlag=${Check_No }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认取消吗" class="btn btn-primary " role="button">导出Word</a> --%>
						</small>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div id="example2_wrapper"
							class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-12 overx">
									<table class="table table-bordered table-hover table-width">
										<thead>
											<tr role="row"><!-- style='display:none;' -->
												<th ><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>
												<th>告别厅</th>
												<th>告别时间</th>
												<th>姓名</th>
												<th>性别</th>
												<th>年龄</th>
												<th>对联</th>
												<th>围花</th>
												<th>黑纱</th>
												<th>白纱</th>
												<th>孝球</th>
												<th>花门</th>
												<th>礼仪</th>
												<th>司仪</th>
												<th>地毯</th>
												<th>背景</th>
												<th>电子遗像</th>
												<th>摄像</th>
												<th>灵堂</th>
												<th>状态</th>
											</tr>
										</thead>
										<tbody>
											<c:choose>
											    <c:when test="${fn:length(page.list)==0 }">
												    <tr>
										    			<td colspan="20">
													  		<font color="Red">还没有数据</font>
												 		</td>
												    </tr>
											    </c:when>
											    <c:otherwise>
												    <c:forEach items="${page.list}" var="u">
													    <c:set value="0" var="flag"></c:set>
														<tr role="row">
															<c:forEach items="${u}" var="k">
																<c:choose>
																	<c:when test="${flag==0}">
																		<td ><input type="checkbox" name="checkboxId" value="${k}"></td>
																	</c:when>
																	<c:when test="${flag==2 }">
											    						<td><fmt:formatDate value="${k}" pattern="yyyy-MM-dd HH:mm"/></td>
											    					</c:when>
											    					<c:when test="${flag==3}">
																		<td>
																		<a href="threeAndOne.do?method=dead&&deadId=" data-add='true' target="homeTab" rel="myModal" id="liebiao"  role="button">
	                                                                       ${k }
	                                                                    </a>   
																	    </td>
																	</c:when>
											    					<c:when test="${flag==19}">
																	<td><select name="state" class="state"
																		onchange="GaiBian(this)"> ${k }
																	</select></td>
																	</c:when>
																	
																	<c:otherwise>
											    						<td>${k}</td>
											    					</c:otherwise>
										    					</c:choose>
										    					<c:set value="${flag+1}" var="flag"></c:set>
									    					</c:forEach>
														</tr>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div> 
	</section>
</form>
<script>
$(function(){
	timeRange();
});
</script>