<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">挂帐业务</div>
</section>
<style type="text/css">
	
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
$('#search').change(function(){
	if($("#find").find("option:selected").text()=="卡号"){
		homeSearch(pageForm);		
	}
})
$(function(){
	timeRange();
})
//将点击的常用搜索按钮对应值改变，用于后台判断
var checkType='checkAll';
$("#typeDiv").on('click','button',function(){	
	checkType=$(this).attr('name');
	$("#checkType").val(checkType);
})
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div>
						<div class="box-header with-border">
							<h3 class="box-title" style="color:#4EC2F6">查询</h3>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-chevron-down"></i>
								</button>
							</div>
							<!-- /.box-tools -->
						</div>
					</div>
					<div class="box-body border-B">
						<div class="col-md-3">
							<label>类型：</label> 
							<select class="list_input nopadding-R" id="find" name="find">
<!-- 								<option value='findByCode'>卡号</option> -->
<!-- 								<option value='findByName'>姓名</option> -->
								${searchOption }
							</select>
						</div>
						<div class="col-md-8">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" id='search' name='search' value='${search}' placeholder='单行输入'/>
						</div>
						<div class="col-md-12">
							<label>创建时间：</label> 
							<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime}"><i
								style="margin-left: -20px;" class="fa fa-calendar"></i>
							<span class='timerange'>--</span>
							<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}"><i
								style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
						<!-- 控制常用搜索状态（已审核等） -->
						<input type="hidden" id="checkType" name="checkType" value="${checkType }">
						<div class="col-md-12" id="typeDiv">
							<label>常用：</label>
							<button type="submit" class="btn btn-normally btn-all" name="checkAll">全部</button>
							<button type="submit" class="btn btn-normally" name="checkNo">未收费</button>
							<button type="submit" class="btn btn-normally" name="checkYes">已收费</button>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
					<div class="row">
					<div class="col-xs-12">
						<div class="box" style="border: 0;margin-bottom:0">
							<!--<div class="box-header with-border">
<%-- 								<a href="${url}?method=edit" target="dialog" rel="myModal" class="btn btn-primary" role="button">添加</a> --%>
								
								
							</div>-->
							<!-- /.box-header -->
							<div class="box-body">
								<div class='col-md-12' style='padding-left:0px;'>
								<small class=' btns-hometab'>
									<a href="${url}?method=edit&id=" target="homeTab" checkName="id" checkOne="true" rel="myModal" class="btn btn-warning" role="button">修改</a>
									<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="id" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
								</small>
								</div>
								<div id="example2_wrapper"
									class="dataTables_wrapper form-inline dt-bootstrap hometab-div">
									<div class="row">
										<div class="col-sm-12">
											<table class="table table-bordered table-hover">
												<thead>
													<tr role="row">
														<th width="10"><input type="checkbox" class="checkBoxCtrl" group="id" /></th>
														<th>流水号</th>
														<th>业务编号</th>
														<th>死者姓名</th>
														<th>挂账单位名称</th>
														<th>证件类型</th>
														<th>死者证件号</th>
													</tr>
												</thead>
												<tbody>
												 <c:choose>
												    <c:when test="${fn:length(page.list)==0 }">
													    <tr>
											    			<td colspan="7">
														  		<font color="Red">没有挂账信息</font>
													 		</td>
													    </tr>
												    </c:when>
												    <c:otherwise>
													    <c:forEach items="${page.list }" var="u">
														<tr role="row">
															<td><input type="checkbox" name="id" value="${u.id}"></td>
															<td>${u.code}${"GZYW"}</td>
															<td>${u.code}</td>															
															<!-- 添加连接到详细挂账业务 -->
									    					<td id="name_Click"><a href="${url}?method=show&id=${u.id}"  target="dialog" rel="myModal" ><font color="blue">${u.name }</font></a></td>
									    					<td>${u.billUnit.name}</td>
									    					<td>${u.certificate.name}</td>
									    					<td>${u.certificateCode}</td>									    					
														</c:forEach>
													</c:otherwise>
												</c:choose>
												</tbody>
											</table>
										</div>
									</div>
									<%@ include file="/common/pageFood.jsp"%>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>
<!-- 				<div class="box-footer clearfix btns-print" style="border-top:none;"> -->
<!-- 	              <a href="javascript:void(0)" class="btn btn-color-ab47bc pull-right">打印</a> -->
<!-- 	            </div> -->
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
</section>