<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<script type="text/javascript">
$(function(){ 
	var tree = [${itemTree}];
	var html='';
	$(tree).each(function(i,val){
		
	html+='<div class="box box-warning">';
      html+=  '<div class="box-header with-border">';
       html+=  (' <h3 class="box-title">'+val.text+'</h3>');
       html+=  ' <div class="box-tools">';
         html+='<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>';
           
         html+=' </div>';
        html+='</div>';
       html+=' <div class="box-body no-padding">';
         html+='<ul class="nav nav-pills nav-stacked">';
         	$(val.nodes).each(function(i,v){
         		var id=v.tags[0].split('_');
         		var num=Number(id[1]);
         		html+=("<li class='checkbox checkbox-S'><label><input type='checkbox' name='namager' data-num='"+num+"' value='"+id[0]+"'/>"+v.text+"</label></li>");
         	})
          html+='</ul>';
        html+='</div>';
      html+='</div>';
      
	});
	$('#accordion').append(html);
});
function save(id){
	var arr=[];
	$('.item-txt').each(function(){
		var index=Number($(this).attr('data-number'));
		arr[index]=$(this).attr('data-value');
	})
	var url="";
	if($("#typeName").val()=="articles"){
		url="chargeAccount.do?method=addItema&ids="+arr
	}
	if($("#typeName").val()=="service"){
		url="chargeAccount.do?method=addItems&ids="+arr
	}
	$.ajax({
	    url: url,
	    dataType:'json',
	    cache: false,
	    success: function (list) {
	    	for(var i=0;i<list.length;i++){
	    		var ob=list[i];
	    		sum++;
	    		var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	    		str+="<input type='checkbox' id=checkbox_"+sum+" name="+ob[6]+" value="+sum+"></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<select  class='list_table nopadding-R' onchange=itemName('"+sum+"','"+ob[6]+"') name='itemId' id='itemId_"+sum+"'> "+ob[0]+"</select></td>";
	    		str+="<td style='text-align: center; vertical-align: middle; '>";
	    		str+="<input type='text' readonly='readonly' class='list_table' id='pice_"+sum+"' name='pice' value="+ob[1]+"></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<select  class='list_table nopadding-R' onchange=itemType('"+sum+"','"+ob[6]+"') name='typeId' id='typeId_"+sum+"'>"+ob[2]+"</select></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<input type='text' class='list_table' onkeyup=changeNumber('"+sum+"','"+ob[6]+"') onkeydown='if(event.keyCode==13) return false' id='number_"+sum+"' name='number' value="+ob[3]+"></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<select  class='list_table nopadding-R' name='tickFlag' id=bill_"+sum+"> "+ob[5]+"</select></td>";
	    		str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name='total' id=total_"+sum+" class='list_table' value="+ob[4]+"></td>";
	    		str+="<td><textarea type='text' class='list_table' id='comment_"+sum+"'  name='comment'></textarea></td></tr>";
	    		
	    		if('${type}'=='service'){
	    			$("#service").append(str);
	    			sTotal+=ob[4]
	    		}
	    		if('${type}'=='articles'){
	    			$("#articles").append(str);
		    		aTotal+=ob[4]
	    		}
	    	}
	    	if('${type}'=='service'){
	    		$("#sFont").html(sTotal+"元");
	    	}
    		if('${type}'=='articles'){
    			$("#aFont").html(aTotal+"元");
	    	}
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
	$('.item-del').click();
}
</script>
<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
				<div class="modal-content ">
					<div class='row'>
						<div class='col-md-6 border-R height-500'>
							<section class="content-header content-header-S">
								<i class="title-line"></i>
								<div class="title">请选择服务项目</div>
							</section>
							<div class='panel-group panel-group-S' role="tablist" id='accordion'>

							</div>
						</div>
						<div class='col-md-6 height-500 margin-L-S'>
							<section class="content-header content-header-S">
								<i class="title-line"></i>
								<div class="title">已选服务项目</div>
							</section>
							<div class='chosed-item'>
								<div class='box box-warning' id='chosed'>
<!-- 									<p class='item-txt'>111111111<span class='item-del'>x</span></p> -->
								</div>
							</div>
						</div>
					</div>
					<div class='row nomargin'>
						<div class='col-md-12 wk-btns'>
							<input type="hidden" id="typeName" name="typeName" value="${type }">
							<button type="submit" onclick="save('id')" class="btn btn-info" data-dismiss="modal">确定</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
						</div>
					</div>
				
				</div>
			</div>
				
	
<script>
	(function(){
		$('.panel-group input:checkbox').click(function(){
			if($(this).is(':checked')){
				$('#chosed').append("<p class='item-txt' data-number='"+$(this).attr('data-num')+"' data-value='"+$(this).val()+"'><a>"+$(this).parent().text()+"</a><span class='item-del'>x</span></p>")
			}else{
				$("#chosed p[data-value='"+$(this).val()+"']").remove();
			}
		});
		$('#chosed').on('click','.item-del',function(){
			$(".panel-group input[value='"+$(this).parent().attr('data-value')+"']").removeAttr('checked');
			$(this).parent().remove();
		});
	})();
</script>	
</body>
