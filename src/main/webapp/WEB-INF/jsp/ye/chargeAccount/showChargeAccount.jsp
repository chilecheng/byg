<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<script type="text/javascript">

</script>

</head>
<body>
	<section class="content dialog-width">
		<form id='detail' action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${commission.id}">
		<input type="hidden" id="name" value="${commission.name }">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<div>
					
				<!-- 业务信息 -->
						<div class="tab-pane" id="b">
						<p class='p border-B'>挂账业务信息</p>
							<div class="box-body border-B">
								<div class="col-md-5 height-align">
									<label  class="lab-3">流水号：</label> 
									<c:choose>
									    <c:when test="${serialNumber!=null}">
									    	${serialNumber}
									    </c:when>
									</c:choose> 
								</div>								
							
								<div class="col-md-5 height-align">
									<label  class="lab-2">业务编号：</label> 
									<c:choose>
									    <c:when test="${code!=null}">
									    	${code}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-5 height-align">
									<label  class="lab-2">死者姓名：</label>
									<c:choose>
									    <c:when test="${name!=null}">
									    	${name}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-5 height-align">
									<label  class="lab-2">挂账单位：</label> 
									<c:choose>
									    <c:when test="${billUnitName!=null}">
									    	${billUnitName}
									    </c:when>
									</c:choose> 
								</div>
								<div class="col-md-5 height-align">
									<label  class="lab-6">死者证件类型：</label>
									<c:choose>
									    <c:when test="${commission.certificate.name!=null}">									    	
									    	${commission.certificate.name}
									    </c:when>
									</c:choose> 
								</div>
								
								<div class="col-md-5 height-align">
									<label  class="lab-6">死者证件号：</label> 
									<c:choose>
									    <c:when test="${certificateCode!=null}">
									    	${certificateCode}
									    </c:when>
									</c:choose> 
								</div>							
							</div>
							<p class='p border-B'>服务项目信息</p>					
						
							<div class="box-body border-B">
								<div class="col-md-12">
									<small class="pull-right"> 
<%-- 										<a href="${url}?method=selectItem&type=${Type_Service}&id=service" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<%-- 										<a href="${url}?method=selectItems" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<!-- 										<button type="button" onclick="delHtml('service')" class="btn btn-danger " >删除</button> -->
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 20px;">
									<thead>
										<tr>
<!-- 											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="service" /></th> -->
											<th width="150px">名称</th>
									        <th width="100px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="80px">数量</th>
									        <th width="80px">挂账</th>
									        <th width="100px">小计</th>
									        <th  width="100px">备注</th>
										</tr>
									</thead>
									<tbody id="service">
										 <c:choose>
										    <c:when test="${fn:length(serviceList)==0 }">
											    <tr>
									    			<td colspan="7">
												  		<font color="Red">没有相关收费</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${serviceList}" var="u">
												<tr role="row">
													<td>${u.name}</td>
													<td><fmt:formatNumber value='${u.pice}' pattern='#' type='number'/></td>													
							    					<td>${u.typeName }</td>
							    					<td>${u.number}</td>
							    					<td>${u.tickFlag}</td>
							    					<td><fmt:formatNumber value='${u.total}' pattern='#' type='number'/></td>		
							    					<td>${u.comment }</td>							    					
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='7' style='text-align: left;' >
											<font color='red' id="sFont">
											<fmt:formatNumber value='${serviceSum}' pattern='#' type='number'/>元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<p class='p border-B'>丧葬用品信息</p>
						
							<div class="box-body border-B">
								<div class="col-md-12">
									<small class="pull-right"> 
<%-- 										<a href="${url}?method=selectItem&type=${Type_Articles}&id=articles" target="dialog" rel="myModal" class="btn btn-success" role="button">添加</a> --%>
<!-- 										<button type="button" onclick="delHtml('articles')" class="btn btn-danger " >删除</button> -->
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 20px;">
									<thead>
										<tr>
<!-- 											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="articles" /></th> -->
											<th width="150px">名称</th>
									        <th width="100px">单价（元）</th>
									        <th width="150px">类别</th>
									        <th width="80px">数量</th>
									        <th width="80px">挂账</th>
									        <th width="100px">小计</th>
									        <th  width="100px">备注</th>
										</tr>
									</thead>
									<tbody id="articles">
										<c:choose>
										    <c:when test="${fn:length(articlesList)==0 }">
											    <tr>
									    			<td colspan="7">
												  		<font color="Red">没有相关收费</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${articlesList}" var="u">
												<tr role="row">
													<td>${u.name}</td>
													<td><fmt:formatNumber value='${u.pice}' pattern='#' type='number'/></td>													
							    					<td>${u.typeName }</td>
							    					<td>${u.number}</td>
							    					<td>${u.tickFlag}</td>
							    					<td><fmt:formatNumber value='${u.total}' pattern='#' type='number'/></td>	
							    					<td>${u.comment }</td>								    					
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
									<tbody>
										<tr>
											<td style='width: 60px'>合计：</td>
											<td colspan='7' style='text-align: left;' >
											<font color='red' id="aFont">											
											<fmt:formatNumber value='${articlesSum}' pattern='#' type='number'/>元
											</font>
											</td>
										</tr>
									</tbody>
								</table>
								
							</div>
						</div>			
						
					</div>
					<div class="box-foot border-T">
						<div  class="container-fluid">				
							<div class="col-md-12 btns-dialog">
									<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</form>
	</section>
</body>