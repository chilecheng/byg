<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">挂账业务</div>
</section>
<head>
<script type="text/javascript">

var sum=0;//标识
var sTotal=0;//服务项目合计总金额
var aTotal=0;//丧葬用品合计总金额
var delNumber=[]//删除记录

//收费项目方法
function item(sum,itemOrderId,name,itemId,pice,typeId,number,bill,total,comment){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='checkbox' id=checkbox_"+itemOrderId+" name="+name+" value="+itemOrderId+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table nopadding-R' onchange=itemName('"+itemOrderId+"','"+name+"') name='itemId' id='itemId_"+itemOrderId+"'> "+itemId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' readonly='readonly' class='list_table' id='pice_"+itemOrderId+"' name='pice' value="+pice+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table nopadding-R' onchange=itemType('"+itemOrderId+"','"+name+"') name='typeId' id='typeId_"+itemOrderId+"'>"+typeId+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='text' class='list_table' onkeyup=changeNumber('"+itemOrderId+"','"+name+"') onkeydown='if(event.keyCode==13) return false' id='number_"+itemOrderId+"' name='number' value="+number+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table nopadding-R' name='tickFlag' id=bill_"+itemOrderId+"> "+bill+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name='total' id=total_"+itemOrderId+" class='list_table' value='"+total+"'></td>";
	str+="<td><textarea type='text' class='list_table' id='comment_"+itemOrderId+"'  name='comment'>"+comment+"</textarea></td></tr>";
	$("#"+name).append(str);
}

//增加收费记录
function addRow(type){
	//sum 作为增加记录的暂时ID来区别
	//服务项目 
	if(type=='service'){		
		<c:forEach var="u" items='${defaultServiceList}'>
		sum++;
		item(sum,sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","")	
		sTotal+=parseFloat('${u[6]}')
		$("#sFont").html(sTotal+"元");
		</c:forEach>
	}
	//丧葬用品
	if(type=='articles'){
		<c:forEach var="u" items='${defaultArticlesList}'>
		sum++;
		item(sum,sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","")
		aTotal+=parseFloat('${u[6]}')
		$("#aFont").html(aTotal+"元");
		</c:forEach>
		}
}


$(document).ready(function(){
	//初始化服务项目
	<c:forEach var="u" items='${serviceList}'>
		sum++;
		item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}")
		sTotal+=parseFloat('${u[7]}')
		$("#sFont").html(sTotal+"元");
	</c:forEach>
	//初始丧葬用品项目
	<c:forEach var="u" items='${articlesList}'>
		sum++;
		item(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}","${u[5]}","${u[6]}","${u[7]}","${u[8]}")
		aTotal+=parseFloat('${u[7]}')
		$("#aFont").html(aTotal+"元");
	</c:forEach>
});


//保存数据 
function saveDate(callback){
	//服务项目数据
	var serviceTable = document.getElementById("service");
	var serviceOut=[];
	for(var i=0;i<serviceTable.rows.length;i++){
		var inner=[];
		var allString="";
		for(var j=0;j<serviceTable.rows[i].cells.length;j++){
			//找到所有表格里的元素
			var name=$(serviceTable.rows[i].cells[j].children[0]).attr('name');
			var val=$(serviceTable.rows[i].cells[j].children[0]).val();
			inner[name]=val;			
			allString=allString+val+",";
			
			
		}
		allString=allString+$("#commission_order_id").val();
		serviceOut[i]=allString;
	}
	//丧葬用品数据
	var articlesTable = document.getElementById("articles");
	var articlesOut=[];
	for(var i=0;i<articlesTable.rows.length;i++){
		var inner=[];
		var allString="";
		for(var j=0;j<articlesTable.rows[i].cells.length;j++){
			//找到所有表格里的元素
			var name=$(articlesTable.rows[i].cells[j].children[0]).attr('name');
			var val=$(articlesTable.rows[i].cells[j].children[0]).val();
			inner[name]=val;
			allString=allString+val+",";
		}
		allString=allString+$("#commission_order_id").val();		
		articlesOut[i]=allString;
	}
	
	$.ajax({
		url:"chargeAccount.do",
		traditional: true,
		data:{"method":"save",saveArticles:articlesOut,saveService:serviceOut,
			delNumber:delNumber,billUnitId:$("#billUnitOption").val(),
			commissionId:$("#commission_order_id").val()
			},
		type:"post",
		dataType:"json",
		success:callback		
	});
}

//项目类别改变
function itemType(id,type){
	$.ajax({
	    url: "chargeAccount.do",
	    dataType:"json",
	    cache: false,
	    data:{method:"itemTypeChange",id:$("#typeId_"+id).val()},
	    success: function (json) {
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
				$("#sFont").html(sTotal+"元");
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
				$("#aFont").html(aTotal+"元");
    		}
    		$("#pice_"+id).val(json.pice)
	    	$("#total_"+id).val(json.pice)
	    	$("#number_"+id).val(1)
	    	$("#itemId_"+id).html(json.str);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
} 

//项目名称改变
function itemName(id,type){
	$.ajax({
	    url: "chargeAccount.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
	    	if(type=='service'){
	    		sTotal=sTotal-$("#total_"+id).val();
	    		sTotal+=json.pice;
				$("#sFont").html(sTotal+"元");
		    	$("#total_"+id).val(json.pice)
    		}
    		if(type=='articles'){
    			aTotal=aTotal-$("#total_"+id).val();
    			aTotal+=json.pice;
				$("#aFont").html(aTotal+"元");
		    	$("#total_"+id).val(json.pice)
    		}
    		
    		$("#pice_"+id).val(json.pice)
	    	$("#number_"+id).val(1)
	    	
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
} 

//数目改变
function changeNumber(id,type){
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	var pice=$("#pice_"+id).val();
		if(type=='service'){
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			$("#sFont").html(sTotal+"元");
			$("#total_"+id).val(pice*number);
		}
		if(type=='articles'){
			aTotal=aTotal-$("#total_"+id).val();
			aTotal+=pice*number;
			$("#aFont").html(aTotal+"元");
			$("#total_"+id).val(pice*number);
		}
	
} 

//删除内容
function delHtml(str) {
	var num = 0;
	
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		if(str=="articles"){
			//此处若删除的记录条ID为暂时ID，不作变动，若是数据库中ID，删除时记录，方便在后台删除数据库中的记录
			if(id.length>5){
				delNumber.push(id);
			};
			
			aTotal=aTotal-$("#total_"+id).val();
			$("#aFont").html(aTotal+"元");
		}
		if(str=="service"){
			if(id.length>5){
				delNumber.push(id);
			};
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		}
		$(this).parent().parent().remove();
		sum--;
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
</script>
<style>


.container-fluid {
	background: #fff;
}

.list_table {
	border-radius: 6px;
	border: 1px rgb(169, 169, 169) solid;
	width: 100%;
	line-height: 30px;
	height: 30px;
	padding: 0 8px;
}

</style>

</head>
<body>
	<section class="content">
		<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);" >
			
			<!-- 传值给后台java -->
			<input type="hidden" name="method" value="${method}">
			<input type="hidden" id="id" name="id" value="${commissionOrder.id}">
			<input type="hidden" id="name" name="name" value="${commissionOrder.name }">
			<input type="hidden" name="commission_order_id" id="commission_order_id" value="${id}">


			
				
					<div class='box box-warning'>
						<!-- 业务信息 -->
						<div id="b">
								<div class="box-body border-B">
									<div class="col-md-5 height-align">
										
										<label class="lab-1">流水号：</label>
										<c:choose>
											<c:when test="${serialNumber!=null}">
									    	${serialNumber}
									    </c:when>
										</c:choose>
									</div>

									<div class="col-md-5 height-align">
										<label class="lab-6">业务编号：</label>
										<c:choose>
											<c:when test="${code!=null}">
									    	${code}
									    </c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-6">死者姓名：</label>
										<c:choose>
											<c:when test="${name!=null}">
									    	${name}
									    </c:when>
										</c:choose>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-6">挂账单位：</label>
											<select class="list_select nopadding-R" name="billUnitOption"
												id="billUnitOption">${billUnitOption}													
											</select>
											
											
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-6">死者证件类型：</label>
										<c:choose>
											<c:when test="${certificateId!=null}">									    	
									    	${certificateId}
									    </c:when>
										</c:choose>
									</div>

									<div class="col-md-5 height-align">
										<label class="lab-6">死者证件号：</label>
										<c:choose>
											<c:when test="${certificateCode!=null}">
									    	${certificateCode}
									    </c:when>
										</c:choose>
									</div>
								</div>
								<p class='p border-B'>
									服务项目信息
								</p>
								<div class="box-body border-B">
									<div class="col-md-12">
										<small class="pull-right btns-buy">
<!-- 											<button type="button" onclick="addRow('service')" -->
<!-- 												class="btn btn-warning">添加</button> -->
											<a href="${url}?method=insertService" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
											<button type="button" onclick="delHtml('service')"
												class="btn btn-danger">删除</button>
										</small>
									</div>
									<table class="table table-bordered" style="margin-top: 60px;">
										<thead>
											<tr>
												<th width="30px">全选<input type="checkbox"
													class="checkBoxCtrl" group="service" /></th>
												<th width="150px">名称</th>
												<th width="100px">单价（元）</th>
												<th width="150px">类别</th>
												<th width="80px">数量</th>
												<th width="80px">挂账</th>
												<th width="100px">小计</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody id="service" >

										</tbody>
										<tbody>
											<tr>
												<td style='width: 60px'>合计：</td>
												<td colspan='7' style='text-align: left;'><font
													color='red' id="sFont"> 0元 </font></td>
											</tr>
										</tbody>
									</table>
								</div>
								<p class='p border-B'>
									丧葬用品信息
								</p>
								<div class="box-body border-B">
									<div class="col-md-12">
										<small class="pull-right btns-buy">
<!-- 											<button type="button" onclick="addRow('articles')" -->
<!-- 												class="btn btn-warning">添加</button> -->
											<a href="${url}?method=insertArticles" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
											<button type="button" onclick="delHtml('articles')"
												class="btn btn-danger">删除</button>
										</small>
									</div>
									<table class="table table-bordered" style="margin-top: 60px;">
										<thead>
											<tr>
												<th width="30px">全选<input type="checkbox"
													class="checkBoxCtrl" group="articles" /></th>
												<th width="250px">名称</th>
												<th width="100px">单价（元）</th>
												<th width="150px">类别</th>
												<th width="80px">数量</th>
												<th width="80px">挂账</th>
												<th width="100px">小计</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody id="articles">

										</tbody>
										<tbody>
											<tr>
												<td style='width: 60px'>合计：</td>
												<td colspan='7' style='text-align: left;'><font
													color='red' id="aFont"> 0元 </font></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="box-body border-B">
									<!-- 保存、重置 、返回-->
									<div class="container-fluid">
										<div class="col-md-12">
											<small class="pull-right btns-hometab"> 	
												<a href="javascript:void(0)" onclick="saveDate(homeAjaxDone)"
												class="btn btn-info" role="button">保存</a>								
												<a href="${url}?method=edit&id=${id}" target="homeTab" checkOne="true" rel="myModal" class="btn btn-color-9E8273" role="button">重置</a>
											
												<a href="${url}?method=list" type="button"  target="homeTab" class="btn btn-default">返回</a>
											</small>
										</div>
									</div>
								</div>
						</div>
					</div>
		</form>
	</section>
</body>
<script type="text/javascript">	
	$(function(){
		$("#billUnitOption").val("${commission.billUnitId}");
	});
</script>
