<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 
<script>
//将点击的常用搜索按钮对应值改变，用于后台判断
var type='4';
$("#typeDiv").on('click','button',function(){	
	type=$(this).attr('name');
	$("#type").val(type);
})
$(function() {
	timeRange();
	$('#example2_wrapper table tr td[data-num]').each(function(){
		//console.log(this);
		var data=$(this).attr('data-num');
		switch (data){
			case '${IsFlag_Yse }':
				$(this).html('未布置').css({'color':'#f39c12'});
				break;
			case '${IsFlag_Bzwc }':
				$(this).html('布置完成').css({'color':'#00a65a'});
				break;
		}
	}); 
})
</script>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">告别厅列表</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>类型：</label> 
							<select class="list_select" id="searchType" name="searchType" style="width:52%;">	
								${searchOption }		
<!-- 								<option value="Name" name="Name">姓名</option> -->
<!-- 								<option value="Code" name="Code" >卡号</option> -->
							</select>
						</div>
						<div class="col-md-8" style="margin-left:-94px">
							<label>查询值</label> 
							<input type="text" class="list_input input-hometab" name="searchVal" value="${searchVal}" placeholder="查询值">
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<label>告别时间：</label> 
							<input data-id='beginDate' class="list_select" id="dTime" name="beginDate" value="${beginDate }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<span class='timerange'>--</span> 
							<input data-id='endDate' class="list_select" id="dTime" name="endDate" value="${endDate }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<!-- 控制常用搜索状态（已审核等） -->
						<input type="hidden" id="type" name="type" value="${checkType }">
						<div class="col-md-12" id="typeDiv">
							<label>常用：</label>
							<button type="submit" class="btn btn-normally btn-all" name="4">全部</button>
							<button type="submit" class="btn btn-normally" name="1">未布置</button>
							<button type="submit" class="btn btn-normally" name="2">进行中</button>
							<button type="submit" class="btn btn-normally" name="3">告别已完成</button>
							<button type="submit" class="btn btn-normally" name="5">近两日告别</button>
<%-- 							<a href="${url}?method=listInfo&type=1" target="homeTab" id="default" class="btn btn-normally " role="button">未布置</a> --%>
<%-- 							<a href="${url}?method=listInfo&type=2" target="homeTab" id="default" class="btn btn-normally " role="button">进行中</a> --%>
<%-- 							<a href="${url}?method=listInfo&type=3" target="homeTab" id="default" class="btn btn-normally " role="button">告别已完成</a> --%>
<%-- 							<a href="${url}?method=listInfo&type=4" target="homeTab" id="default" class="btn btn-normally " role="button">全部</a> --%>
<%-- 							<a href="${url}?method=listInfo&type=5" target="homeTab" id="default" class="btn btn-normally " role="button">近两日告别</a> --%>
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class=' btns-print'>
						<a href="${url}?method=edit&noRef=no&type=fare&id=" target="dialog" rel="myModal" checkOne="true" checkName="id" class="btn btn-warning" role="button">布置</a>
						<a href="${url}" target="homeTab" class="btn btn-default " role="button">返回</a>
					</small>
					<small class="pull-right btns-print">
<%-- 						<a href="${url}?method=" target="dialog"  rel="myModal" class="btn btn-color-ab47bc" role="button" id="d">打印</a> --%>
<%-- 						<a href="${url}?method=" target="dialog" rel="myModal" class="btn btn-success" role="button">导出EXCEL</a> --%>
						
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"></th>
											<th>卡号</th>
											<th>死者姓名</th>
											<th>告别厅号</th>
											<th>告别时间</th>
											<th>布置状态</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr width="20">
								    			<td colspan="6">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
												<tr role="row" width="10">
													<td><input type="radio" name="id" value="${u.commissionOrderId}"></td>
							    					<td>${u.order.cardCode }</td>
							    					<td>${u.order.name}</td>
							    					<td>${u.farewell.name}</td>
							    					<td><fmt:formatDate value="${u.beginDate}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td data-num="${u.flag}" class='state1'></td>
							    				</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
