<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<%-- <c:choose>
		<c:when test="${homeType !=null && homeType!='' }">
			<form  action="${url }" rel="myModal" data-type='${homeType }' onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
		</c:when>
		<c:otherwise>
			<form  action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
		</c:otherwise>
	</c:choose> --%>
		<form  action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			<input type="hidden" name="method" value="${method }" >
			<input type="hidden" name="id" value="${id }" >
			<input type="hidden" name="noRef" value="${noRef }" >
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<div class="modal-content">
				<div class="modal-body nopadding-T nopadding-B">
						<div class="row">
							<p class="col-md-12 p border-B">业务信息</p>
							<div class="col-md-12 nomargin-T border-B padding-B">
								<div class="row">
									<div class="col-md-6 height-align">
										<label class='lab-2'>死者姓名：</label>
										<span>${commissionOrder.name}</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-6'>预约火化时间：</label>
										<span><fmt:formatDate value="${commissionOrder.cremationTime }" pattern="yyyy-MM-dd HH:mm"/></span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-2'>死者编号：</label>
										<span>${commissionOrder.code}</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-3'>联系人：</label>
										<span>${commissionOrder.fName}</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-2'>死者性别：</label>
										<span>${commissionOrder.sexName}</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>联系人电话：</label>
										<span>${commissionOrder.fPhone}</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-2'>死者年龄：</label>
										<span>${commissionOrder.age}</span>
									</div>
								</div>
								
							</div>
							<p class="col-md-12 p border-B">布置信息</p>
							<div class="col-md-12 nomargin-T border-B padding-B">
								<div class="row">
									<div class="col-md-6 height-align">
										<label class='lab-5'>司仪人员：</label>
										<select class='list_input input-dialog nopadding-R input-dialog' name="emceeId">
											<c:if test="${farewellRecord.emceeId == null || farewellRecord.emceeId == '' }">
												<option selected="selected"></option>
											</c:if>
											<c:forEach items="${emceeGroupList }" var="e" >
												<c:choose>
													<c:when test="${farewellRecord.emceeId==e.userId }">
														<option value="${e.userId }" selected="selected">${e.name }</option>
													</c:when>
													<c:otherwise>
														<option value="${e.userId }">${e.name }</option>	
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>主持费：</label>
										<input class='number input-dialog list_input' name="chargeFee" value="${farewellRecord.chargeFee==0 ? '' : farewellRecord.chargeFee }" />
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-5'>录入人员：</label>
										<input value="${user.name }" class='required input-dialog list_input'  readonly="readonly" />
										<input type="hidden" name="enterId" value="${user.userId }" >
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-5'>布置时间：</label>
										<%-- <input type="text" data-provide="datetimepicker" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_input input_a" id="beginDate" name="date" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
										<input data-id="reservation" readOnly='readOnly' class="list_select  input-dialog"  name="arrangeTime" value="<fmt:formatDate value="${farewellRecord.arrangeTime }" pattern="yyyy-MM-dd HH:mm"/>">
									</div>
								</div>
								
							</div>
							<p class="col-md-12 p border-B">调度信息</p>
							<div class="col-md-12 nomargin-T border-B padding-B">
								<div class="row">
									<div class="col-md-6 height-align">
										<label class='lab-4'>告别厅号：</label>
										<span>${farewellRecord.farewell.name }</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-4'>告别时间：</label>
										<span><fmt:formatDate value="${farewellRecord.beginDate }" pattern="yyyy-MM-dd HH:mm"/></span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-4' id="status">布置状态：</label>
										<span id="flag" data-num="${flag }"></span>
									</div>
									<div class="col-md-12">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>名称</th>
													<th>单价（元）</th>
													<th>数量</th>
													<th>布置人员</th>
													<th width='150px;'>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${list }" var="a">
													<tr>
														<td>${a.item.name }</td>
														<td>${a.pice }</td>
														<td>${a.number }</td>
														<td>
															<!-- <select class='list_table nopadding-R' name="handler"> -->
															<select class='list_input area_b' name="handler">
																	<c:if test="${a.handlerId == null || a.handlerId == '' }">
																		<option selected="selected"></option>
																	</c:if>
																	<c:forEach items="${groupUserList }" var="g" >
																		<c:choose>
																			<c:when test="${a.handlerId==g.userId }">
																				<option value="${g.userId }" selected="selected">${g.name }</option>
																			</c:when>
																			<c:otherwise>
																				<option value="${g.userId }">${g.name }</option>	
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
															</select>
														</td>
														<td>
															<textarea class='list_table' rows="2" cols="5" name="comment">${a.comment }</textarea> 
														</td>
													</tr>
													<input class='list_table' type="hidden" name="oderId" value="${a.id }"/>
												</c:forEach>
											</tbody>
										</table>
									</div>
									<div class="col-md-12">
										<label class='lab-4'>备注：</label>
										<textarea rows="2" cols="80" class='list_select textarea-dialog' name="fcomment">${farewellRecord.comment }</textarea>
									</div>
								</div>
							</div>
							
								
							<div class="col-md-12 btns-dialog">
								<button type="submit" id="save" class="btn btn-info btn-margin">保存</button>
								<button  id="reset" type="reset" class="btn btn-color-9E8273 btn-margin">重置</button>
								<input type="button" id="back" class="btn btn-color-default btn-margin" data-dismiss="modal" value="返回"/>
							</div>
						</div>
				</div>
			</div>
		</div>
	</form>
	
</body>
<script>
(function(){
	var data = $('#flag').attr('data-num');
	switch (data){
		case '1':
			$('#flag').html('布置完成').css({'color':'#29b6f6'});
		break;
		case '0':
			$('#flag').html('未布置').css({'color':'#e84e40'});
			break;
	}
})()
if($('.daterangepicker.dropdown-menu.show-calendar.opensright').length!==0){
				$('.daterangepicker.dropdown-menu.show-calendar.opensright').remove();
			}
			$('[data-id="reservation"]').daterangepicker({timePicker: true, timePickerIncrement: 5, format: 'YYYY-MM-DD hh:mm A'});
			$('[data-id="reservation"]').change(function(){
				var time=$(this).val();
				var arr=time.split(' ');
				var hour=parseInt(arr[1].slice(0,2));
				arr.pop();
				if(time.lastIndexOf('AM')!==-1&&hour==12){
					arr[1]='0'+(hour-12)+arr[1].slice(2);
				}
				if(time.lastIndexOf('PM')!==-1){
					arr[1]=hour<12?(hour+12+arr[1].slice(2)):(arr[1]);
				}
				$(this).val(arr.join(' '));
			});
</script>