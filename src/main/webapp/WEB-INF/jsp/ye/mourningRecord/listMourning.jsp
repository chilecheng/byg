<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">灵堂列表</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm"
					onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>类型：</label> <select class="list_select nopadding-R"
								name="searchType" style="width: 52%;"> ${searchOption }
							</select>
						</div>
						<div class="col-md-8" style="margin-left: -94px">
							<label>查询值：</label> <input type="text"
								class="list_input input-hometab" name="searchVal"
								value="${name}" placeholder="查询值">
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<label>守灵开始日期：</label> 
							<input data-id='beginDate' class="list_select" id="dTime" name="beginDate" value="${beginDate }"><i	style="margin-left: -20px;" class="fa fa-calendar"></i> 
								<span class='timerange'>--</span> 
							<input data-id='endDate' class="list_select" id="dTime" name="endDate" value="${endDate }"><i	style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
					</div>
					<div class="box-body" style="margin-top: -10px">
						<!-- 控制常用搜索状态（已审核等） -->
						<input type="hidden" id="type" name="type" value="${checkType }">
						<div class="col-md-12" id="typeDiv">
							<label>常用：</label>
							<button type="submit" class="btn btn-normally btn-all" name="all">全部</button>
							<button type="submit" class="btn btn-normally" name="no">未完成</button>
							<button type="submit" class="btn btn-normally" name="doing">进行中</button>
							<button type="submit" class="btn btn-normally" name="yes">守灵已完成</button>
							<button type="submit" class="btn btn-normally" name="now">近两日设灵</button>
						</div>
<!-- 						<div class="col-md-12"> -->
<!-- 							<label>常用：</label>  -->
<%-- 							<a href="${url}?method=mourningList&type=1"	target="homeTab" rel="myModal" id="default"	class="btn btn-normally"	role="button">未完成</a>  --%>
<%-- 							<a href="${url}?method=mourningList&type=2" target="homeTab" rel="myModal" id="default" class="btn btn-normally"	role="button">进行中</a>  --%>
<%-- 							<a href="${url}?method=mourningList&type=3" target="homeTab" rel="myModal" id="default" class="btn btn-normally"	role="button">守灵已完成</a>  --%>
<%-- 							<a href="${url}?method=mourningList&type=4" target="homeTab" rel="myModal" id="default" class="btn btn-normally"	role="button">全部</a>  --%>
<%-- 							<a href="${url}?method=mourningList&type=5"	target="homeTab" rel="myModal" id="default"	class="btn btn-normally"	role="button">近两日设灵</a> --%>
<!-- 						</div> -->
					</div>
					
					<div class="box-body" style="margin-top: -10px">
						<div class="col-md-12">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
				<small class=' btns-print'>
					 <a
						href="${url}?method=edit&noRef=no&type=mour&id=" target="dialog"
						rel="myModal" checkOne="true" checkName="id"
						class="btn btn-warning" role="button">布置</a> <a href="${url}"
						target="homeTab" id="default" class="btn btn-default "
						role="button">返回</a>
				</small>
					 <small class="pull-right btns-print">
<%-- 					<a href="${url}?method=" target="dialog" rel="myModal" class="btn btn-color-ab47bc"	role="button" id="d">打印</a>  --%>
<%-- 						<a href="${url}?method=" target="dialog" rel="myModal" class="btn btn-success"	role="button">导出EXCEL</a> --%>
					
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"></th>
											<th>守灵开始时间</th>
											<th>守灵结束时间</th>
											<th>火化时间</th>
											<th>灵堂号</th>
											<th>死者姓名</th>
											<th>死者年龄</th>
											<th>死者性别</th>
											<th>布置状态</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${fn:length(page.list)==0 }">
												<tr width="20">
													<td colspan="9"><font color="Red">还没有数据</font></td>
												</tr>
											</c:when>
											<c:otherwise>
												<c:forEach items="${page.list}" var="u">
													<tr role="row" width="10">
														<td><input type="radio" name="id" value="${u.commissionOrderId}"></td>
														<td><fmt:formatDate value="${u.beginTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td><fmt:formatDate value="${u.endTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td><fmt:formatDate value="${u.order.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.mourning.name}</td>
														<td><font color="blue">${u.order.name}</font></td>
														<td>${u.order.age}</td>
														<td>${u.order.sexName}</td>
														<td data-num="${u.flag}" class='state1'></td>
													</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
<script>
//将点击的常用搜索按钮对应值改变，用于后台判断
var type='all';
$("#typeDiv").on('click','button',function(){	
	type=$(this).attr('name');
	$("#type").val(type);
})


$(function() {
	timeRange();
	$('#example2_wrapper table tr td[data-num]').each(function(){
		//console.log(this);
		var data=$(this).attr('data-num');
		switch (data){
			case '${IsFlag_Yse }':
				$(this).html('未布置').css({'color':'#f39c12'});
				break;
			case '${IsFlag_Bzwc }':
				$(this).html('布置完成').css({'color':'#00a65a'});
				break;
			case '9':
				$(this).html('清场').css({'color':'#336868'});
				break;
	}
})
	})
</script>