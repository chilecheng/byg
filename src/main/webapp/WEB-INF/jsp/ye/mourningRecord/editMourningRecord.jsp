<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style>
</style>
<div class="modal-dialog" role="document">
	<%-- <c:choose>
		<c:when test="${homeType !=null && homeType!='' }">
			<form action="${url}" rel="myModal" data-type='${homeType }' onsubmit="return validateHomeCallback(this,dialogAjaxDone);" >
		</c:when>
		<c:otherwise>
			<form action="${url}" rel="myModal"  onsubmit="return validateHomeCallback(this,dialogAjaxDone);" >
		</c:otherwise>
	</c:choose> --%>
	<form action="${url}" rel="myModal"  onsubmit="return validateHomeCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${commissionOrder.id }">
		<input type="hidden" name="noRef" value="${noRef }">
		<div class="modal-content">
			<div class="box-header with-border nopadding-T nopadding-B">
				<p class=' p'>业务信息</p>
				</div>
			<div class="modal-body border-B padding-B nopadding-T" >
				<div class='row'>
					<div class='col-md-6 height-align'>
						<label class='lab-2'>死者姓名:</label>
						<span>${commissionOrder.name}</span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-6'>预约火化时间:</label>
						<span><fmt:formatDate value="${commissionOrder.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-2'>死者编号:</label>
						<span>${commissionOrder.code}</span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-3'>联系人:</label>
						<span>${commissionOrder.fName}</span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-2'>死者性别:</label>
						<span>${commissionOrder.sexName}</span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-1'>联系人电话:</label>
						<span>${commissionOrder.fPhone}</span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-2'>死者年龄:</label>
						<span>${commissionOrder.age}</span>
					</div>
				</div>
			</div>
			
			<div class="modal-content" style='box-shadow:none;'>
			<div class="box-header with-border nopadding-T nopadding-B margin-B">
				<p class='p'>服务信息</p>
			</div>
			</div>
			
			<div class='border-B padding-B'>
				<div class="row nomargin-L nomargin-R nopadding-T">
				<div class="col-sm-12 padding-B nomargin-T">
						<div class="col-md-6 height-align">
							<label id="time" class='lab-4'>布置时间：</label> 
							<input data-id="reservation" readOnly='readOnly' style='min-width:60%;' class="required list_select input-dialog" id="arriveTime" name="arrangeTime" value="<fmt:formatDate value="${mourningRecord.arrangeTime }" pattern="yyyy-MM-dd HH:mm"/>">&nbsp;<i class="fa fa-circle text-red"></i>
						</div>
						<div class="col-md-6 height-align">
							<label class='lab-4'>录入人员：</label> 
							<input value="${user.name }" class='required list_input input-dialog' readonly="readonly" />
							<input type="hidden" name="enterId" value="${user.userId }" >
						</div>
				</div>		
					<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th>名称</th>
											<th>单价</th>
											<th>数量</th>
											<th>布置人员</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody>
											<c:forEach items="${list }" var="a">
												<tr>
													<td>${a.item.name }</td>
													<td>${a.item.pice }</td>
													<td>${a.number }</td>
													<td>
														<select class='list_input area_b' name="handler">
															<c:if test="${a.handlerId == null || a.handlerId == '' }">
																<option selected="selected"></option>
															</c:if>
															<c:forEach items="${groupUserList }" var="g" >
																<c:choose>
																	<c:when test="${a.handlerId==g.userId }">
																		<option value="${g.userId }" selected="selected">${g.name }</option>
																	</c:when>
																	<c:otherwise>
																		<option value="${g.userId }">${g.name }</option>	
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</select>
													</td>
													<td>
														<textarea class='list_input area_b' rows="2" cols="5" name="comment">${a.comment }</textarea> 
													</td>
												</tr>
												<input type="hidden" name="oderId" value="${a.id }"/>
											</c:forEach>
									</tbody>
								</table>
							</div>
				</div>
			</div>
			<div class="modal-content" style='box-shadow:none;'>
			<div class="box-header with-border nopadding-T nopadding-B">
				<p class=' p'>调度信息</p>
				</div>
			<div class="modal-body border-B padding-B nopadding-T" >
				<div class='row'>
					<div class='col-md-6 height-align'>
						<label class='lab-6'>设灵时间:</label>
						<span><fmt:formatDate value="${commissionOrder.mourningTime}" pattern="yyyy-MM-dd HH:mm"/></span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-6'>告别时间:</label>
						<span><fmt:formatDate value="${commissionOrder.farewellTime}" pattern="yyyy-MM-dd HH:mm"/></span>
					</div>
					<div class='col-md-6 height-align'>
						<label class='lab-1'>灵堂号:</label>
						<span>${mourningRecord.mourning.name }</span>
					</div>
					<%-- <div class='col-md-6'>
						<label class='lab-6'>状态:</label>
						<span  id="flag" data-num="${mourningRecord.flag }"></span>
					</div> --%>
				</div>
			</div>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit" id="save" class="btn btn-primary btn-margin">保存</button>
				<button type="reset" id="reset" class="btn btn-color-9E8273 btn-margin">重置</button>
				<button type="submit" id="back" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
			</div>
		</div>
	</form>
<script>
/* (function(){
	var data = $('#flag').attr('data-num');
	switch (data){
		case '1':
			$('#flag').html('完成').css({'color':'#29b6f6'});
		break;
		case '2':
			$('#flag').html('进行').css({'color':'#738ffe'});
			break;
		case '3':
			$('#flag').html('锁定').css({'color':'#fea625'});
			break;
	}
})() */
$(function(){
	timeMunite();
});
</script>
</div>