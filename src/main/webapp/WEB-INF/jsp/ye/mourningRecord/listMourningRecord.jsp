<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">灵堂调度</div>
</section>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url }" id="pageForm"  class="outerform" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>日期：</label>  <!-- data-provide="datetimepicker" -->
							<input type="text" data-id="beginDate" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="beginDate" name="date" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
<!-- 							 <button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button> -->
						</div>
						<div class="col-md-3 btns-list">
							<a href="${url}?method=mourningList" target="homeTab" rel="myModal" id="liebiao" class="btn btn-info" role="button">进入列表</a>
							<a href='repairRecord.do?method=repair&type=mour' target='dialog' rel='myModal'  class='btn btn-danger' role='button'>维修</a>
<%-- 							<a href="${url}?method=edit" target="homeTab" rel="myModal" id="dayin" class="btn btn-primary" role="button">打印</a> --%>
						</div>
						
						<div class="col-md-6">
							<ul class="la_ul">
								<li style="background:#336868;"><div>清场</div></li>
								<li style="background:#9ccc65;"><div>布置完成</div></li>
<!-- 								<li style="background:#738ffe;"><div>进行</div></li> -->
								<li style="background:#673ab7;"><div>预约</div></li>
								<li style="background:#fea625;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li>
								<li style="background:#e84e40;"><div>装修</div></li>
							</ul>
						</div>
					</div>
						<div class="box-body" style='padding:20px;'>
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover1">
									<c:forEach items="${list }" var="u" varStatus="i">
										<c:choose>
											<c:when test="${i.index==0 }">
												<thead>
													<tr role="row">
														<th>${u[0]}</th>
														<c:forEach begin="1" end="7" var="a">
															<th colspan="2"><fmt:formatDate value="${u[a]}" pattern="MM.dd"/></th>
														</c:forEach>
													</tr>
												</thead>
											</c:when>
											<c:otherwise>
												<tr role="row">
													<td><font color="blue">${u[0].name }</font></td>
													<c:forEach begin="1" end="14" var="a">
														<c:if test="${u[a]==null }">
															<td width="6%"></td>
														</c:if>
														<c:if test="${u[a]!=null }">
															<c:choose>
																<c:when test="${u[a].flag == IsFlag_Decrate }">
																	<td width="6%" data-num='${u[a].flag}' title="点击更改维修状态">
																		<a style="color:#fff" href="repairRecord.do?method=info&type=mour&id=${u[a].id}&itemId=${u[0].id }&createUserId=${u[a].createUserId}" target="dialog" rel="myModal">维修</a>
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Lock }">
																	<td width="6%" data-num='${u[a].flag}' title="${u[a].order.name},<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		${u[a].order.name }
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_Yse || u[a].flag == IsFlag_Bzwc || u[a].flag == IsFlag_QC }">
																	<td width="6%" data-num='${u[a].flag }' title="${u[a].order.name },<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>至<fmt:formatDate value="${u[a].endTime}" pattern="yyyy/MM/dd HH:mm"/>">
																		<a style="color:#fff" href="${url }?method=detail&id=${u[a].commissionOrderId}&mr_time=${u[a].beginTime}
																		&mname=${u[0].name }&flag=${u[a].flag }&arrTime=${u[a].arrangeTime }&enterId=${u[a].enterId}&type=mour" target="dialog" rel="myModal">${u[a].order.name}</a>
																	</td>
																</c:when>
																<c:when test="${u[a].flag == IsFlag_YY}">
																	<td  data-num='${u[a].flag}' title='${u[a].appointmentName},<fmt:formatDate value="${u[a].beginTime}" pattern="yyyy/MM/dd HH:mm"/>'>
																		${u[a].appointmentName }
																	</td>
																</c:when>
																<c:otherwise>
																	<td width="6%" data-num='${u[a].flag }' >${u[a].flag }</td>
																</c:otherwise>
															</c:choose>
														</c:if>
													</c:forEach>
												</tr>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
<!-- 				<div class="box-footer clearfix"> -->
<!-- 	              <a href="javascript:void(0)" class="btn btn-sm btn-info pull-right">打印</a> -->
<!-- 	            </div> -->
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<script>
		(function(){
			$('#example2_wrapper table tr td[data-num]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-num');
				switch (data){
					case '${IsFlag_Yse }':$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '${IsFlag_Bzwc }':$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '8':
						$(this).css({'backgroundColor':'#673ab7','color':'#fff'});
						break;
					case '${IsFlag_Lock }':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
					case '${IsFlag_Decrate }':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
					case '${IsFlag_QC }':
						$(this).css({'backgroundColor':'#336868','color':'#fff'});
						break;
				}
			})
		})();
	</script>
</section>