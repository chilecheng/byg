<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-T nopadding-B">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 nomargin-T border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者姓名：</label>
									<span>${co.name }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>预约火化时间：</label>
									<span><fmt:formatDate value="${co.cremationTime }" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者编号：</label>
									<span>${co.code }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>联系人：</label>
									<span>${co.fName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者性别：</label>
									<span>${co.sexName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>联系人电话：</label>
									<span>${co.fPhone }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者年龄：</label>
									<span>${co.age }</span>
								</div>
							</div>
							
						</div>
						<p class="col-md-12 p border-B">调度信息</p>
						<div class="col-md-12 nomargin-T border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-6'>设灵时间：</label>
									<span>${mr_time }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>告别时间：</label>
									<span id="gbtime">${gbTime }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>灵堂号：</label>
									<span>${mname }</span>
								</div>
								<div class="col-md-6 height-align">
									<label data-num="${flag }" class='lab-6'>状态：</label>
									<span id="flag" data-num="${flag }"></span>
								</div>
							</div>
						</div>
							
						<p class="col-md-12 p border-B">服务信息</p>
						<div class="col-md-12  nomargin-T border-B padding-B">
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>名称</th>
												<th>单价（元）</th>
												<th>数量</th>
												<th>布置人员</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${list }" var="a">
												<tr>
													<td>${a.item.name }</td>
													<td>${a.item.pice }</td>
													<td>${a.number }</td>
													<td>${a.handler.name }</td>
													<td>${a.comment }</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">服务信息</p>
						<div class="col-md-12 nomargin-T border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<labe class='lab-6'l>布置时间：</label>
									<span><fmt:formatDate value="${arrTime }" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>录入人员：</label>
									<span>${enterUser }</span>
								</div>
							</div>
							
						</div>
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>
<script>
(function(){
	var data = $('#flag').attr('data-num');
	switch (data){
		case '1':
			$('#flag').html('未布置').css({'color':'#29b6f6'});
		break;
		case '2':
			$('#flag').html('进行').css({'color':'#738ffe'});
			break;
		case '3':
			$('#flag').html('锁定').css({'color':'#fea625'});
			break;
		case '7':
			$('#flag').html('布置完成').css({'color':'#00a65a'});
			break;
	}
	var value = $('#gbtime').text();
	if (value == '' || value == undefined) {
		$('#gbtime').text('未填写');
	}
})()
</script>