
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<section class="content">
		<div class="row">
			<div class='col-md-12'>
				<small class='btns-charge'>
					<a class='btn btn-warning' checkone="true" checkname="id" href="${url}?method=edit&id=" target='homeTab' rel="myModal"  role="button">收费</a>
				</small>
			</div>
			<div class='col-md-12'>
				<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th width='70px;'>
								</th>
								<th>挂账单位</th>
								<th>数量</th>
								<th>金额</th>
							</tr>
						</thead>
						<tbody>
						<c:choose>
							<c:when test="${fn:length(page.list)==0 }">
								 <tr width="20">
								    <td colspan="4">
										<font color="Red">没有数据</font>
									 </td>
								</tr>
							</c:when>
						<c:otherwise>
							<c:forEach items="${page.list }" var="u">
									<tr>
										<td><input type='radio' class='checkBoxCtrl' name='id' value='${u.billId }'/></td>
										<td>${u.billName }</td>
										<td>${u.sumNumber }</td>
										<td><fmt:formatNumber value="${u.sumTotal }" pattern="#" type="number"/></td>
									</tr>							
							</c:forEach>
						</c:otherwise>
					  </c:choose>
							
						</tbody>
					</table>
			</div>
		</div>
	</section>
</form>