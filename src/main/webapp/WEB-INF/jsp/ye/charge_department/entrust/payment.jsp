<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">批量挂账收费</div>
</section>
<script type="text/javascript">
function save(callback){
	var itemTable = document.getElementById("billUnitItem");
	var total=0.0;
	var inner=[];
	var commissionId=[];
	for(var i=0;i<itemTable.rows.length;i++){		
		var allString="";
		var val=itemTable.rows[i].cells[0].innerText;
		commissionId[i]=itemTable.rows[i].cells[1].innerText;
		total+=parseFloat(itemTable.rows[i].cells[8].innerText);
		inner[i]=val;
	}
	var payTime=$("#payTIime").text();
	var billUnitId=$("#billUnitId").val();
	$.ajax({
		url:"batchLosses.do",
		traditional: true,
		data:{"method":"save",saveInner:inner,total:total,payTime:payTime,billUnitId:billUnitId,
			commissionId:commissionId
			},
		type:"post",
		dataType:"json",
		success:callback		
	});
}
</script>
<style type="text/css">
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
	.padding{
		padding:15px 30px;
	}
	.content:last-child{
		min-height:100px;
	}
</style>
<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input name="billUnitId" id="billUnitId" value="${id }" type="hidden" > 
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
						<div class="box-body" style='padding:0px;'>
							<div class='col-md-12'>
								<div class='row border-B'>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>结算单位：</label>
										<span>${billName }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>发票抬头：</label>
										<input class='list_input input-hometab' id="receiptor" name="receiptor"
										value="${billName }">
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>挂账个数：</label>
										<span>${size }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-1'>总金额：</label>
										<span><fmt:formatNumber value="${allTotal }" pattern="#" type="number"/> 元</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-4'>机打发票：</label>
										<span class='e84e40'><fmt:formatNumber value="${allTotal }" pattern="#" type="number"/> 元</span>
									</div>
<!-- 									<div class='col-md-6'> -->
<!-- 										<label class='lab-4'>手打发票：</label> -->
<!-- 										<span></span> -->
<!-- 									</div> -->
									<div class='col-md-6 height-align'>
										<label class='lab-4'>结算时间：</label>
										<span id="payTIime"><fmt:formatDate value="${nowTime }" pattern="yyyy-MM-dd HH:mm"/></span>
									</div>
<!-- 									<div class='col-md-6'> -->
<!-- 										<label class='lab-4'>发票记录：</label> -->
<!-- 										<span></span> -->
<!-- 									</div> -->
									<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>
														流水号
													</th>
													<th>姓名</th>
													<th>性别</th>
													<th>年龄</th>
													<th width='200px;'>项目</th>
													<th>数量</th>
													<th>金额</th>
													<th>创建时间</th>
												</tr>
											</thead>
											<tbody  id="billUnitItem">
											<c:choose>
												<c:when test="${fn:length(billList)==0 }">
													 <tr width="20">
													    <td colspan="8">
															<font color="Red">还没有数据</font>
														 </td>
													</tr>
												</c:when>
											<c:otherwise>
												<c:forEach items="${billList }" var="u">
														<tr>
															<td style="display:none;">${u.id }</td>
															<td style="display:none;" >${u.commissionOrderId }</td>
															<td>${u.commissionOrder.code }GZYW</td>
															<td>${u.commissionOrder.name }</td>
															<td>${u.commissionOrder.sexName }</td>
															<td>${u.commissionOrder.age }</td>
															<td>${u.item.name }</td>
															<td>${u.number }</td>
															<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
															<td><fmt:formatDate value="${u.commissionOrder.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														</tr>
												
												</c:forEach>
											</c:otherwise>
										  </c:choose>
											</tbody>
										</table>
									</div>
								</div>
								<div class='row margin-B'>
									<div class='col-md-12'>
										<small class='btns-hometab pull-right'>
											<a href="javascript:void(0)" onclick="save(homeAjaxDone)"  id="sure" class="btn btn-info" role="button">保存</a>
					<!-- 						<a href="javascript:void(0)" onclick="save(dialogAjaxDone1)" target="ajaxTodo" class="btn btn-info" role="button">保存</a> -->
					<!-- 						<button type='reset' class='btn btn-color-9E8273'>重置</button> target="homeTab" rel="myModal"-->
											<a href="${url}?method=list"  target="homeTab" rel="myModal" id="return" class="btn btn-default" role="button">返回</a>
										</small>
									</div> 
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- /.col -->
			</div>
		
	</section>
</form>