
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">批量挂账收费</div>
</section>
<style type="text/css">
	
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	.main-content{
		border:1px solid #ccc;
		border-radius:6px;
		background-color:#fff;
	}
</style>
<section class='content'>
		<div class='row'>
			<div class='col-md-12'>
				<div class='main-content'>
<!-- 					<ul class='nav nav-tabs'> -->
<!-- 						<li role="presentation" class="active"><a href="#goverment" aria-controls="goverment" role="tab" data-toggle="tab">政府单位挂账</a></li> -->
<!-- 						<li role="presentation"><a href="#flower" aria-controls="flower" role="tab" data-toggle="tab">花圈花篮挂账<i></i></a></li> -->
<!-- 						<li role="presentation" ><a href="#hospital" aria-controls="hospital" role="tab" data-toggle="tab">医院委托业务挂账</a></li> -->
<!-- 					</ul> -->
					<div class='tab-content'>
						<div role="tabpanel" class="tab-pane active" id="goverment">
 							<%@ include file='govermentEntrust.jsp'%> 
						</div>
<!-- 			    		<div role="tabpanel" class="tab-pane" id="flower"> -->
<%--  			    			<%@ include file='flowerEntrust.jsp'%> --%>
<!-- 			    		</div> -->
<!-- 			    		<div role="tabpanel" class="tab-pane" id="hospital"> -->
<%-- 			    			<%@ include file='hospitalEntrust.jsp'%> --%>
<!-- 			    		</div> -->
					</div>
				</div>
			</div>
		
		</div>
		
</section>