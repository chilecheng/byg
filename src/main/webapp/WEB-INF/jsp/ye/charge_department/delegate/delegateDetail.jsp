<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">委托业务收费</div>
</section>



<!-- 此页面经过修改已放弃不用！！！！！ -->


<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
	.btn-color-ab47bc{
		background-color:#ab47bc;
		color:#ffffff;
	}
</style>
<script type="text/javascript">
   function subtraction(value){
	   var price=parseFloat($('[data-price="price"]').html())
	   var v2 = parseFloat(value)-price;
	   if(v2>=0){
	  		 $('#changeMoney').val(v2);
	   }
	   else{
		   alert("输入金额不足！")
	   }
	   
	   
   }
   timeMunite();
   
   $(".btn.btn-box-tool").click(function(){
	   $(".collapse-block").slideToggle("slow");
   });
</script>
<form action="delegate.do?method=save" id="pageForm"  onsubmit="return validateHomeCallback(this,homeAjaxDone);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="hehe" type="hidden">
	<input name="orderId" id="orderId"  value="${orderId}" type="hidden">
	<input name="serviceCode" id="serviceCode"  value="${code}" type="hidden">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
					<!--startprint0-->
						<div class="box-body" style='padding:0px;'>
							<div class='col-md-12'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>死者姓名：</label>
										<span>${name}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>收款方式：</label>
										<select class="list_input input-hometab" id="payWay" name="payWay" >
											${payTypeOption }
										</select>
									</div>
								
									
									<div class='col-md-6 height-align'>
										<label  class='lab-3'>付款人：</label>
										<input class='list_input input-hometab' id="payName" name="payName" value="${fName }">
									</div>
									<div class='col-md-6 height-align'>									
										<label class='lab-2'>收款日期：</label> 
<%-- 										<input data-id="reservation"   class="list_select " id="payTime" name="payTime" value="<fmt:formatDate value="${currentTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
										<input readOnly='readOnly' data-id="reservation"  style='min-width:180px;' class="list_select " id="payTime" name="payTime" value="<fmt:formatDate value="${currentTime}" pattern="yyyy-MM-dd HH:mm"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
											
						
								
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>机打发票：</label> 
										<span class='e84e40'><fmt:formatNumber value="${stotal}" pattern="#" type="number"/>元</span>
									</div>
									<div class='col-md-12'>
										<label  class='lab-6'>前期实付金额：</label>						
										<span class='e84e40' ><fmt:formatNumber value="${frontPay}" pattern="#" type="number"/>元</span>
									</div>	
									<div class='col-md-12'>
										<label  class='lab-6'>总计应收金额：</label>						
										<span class='e84e40' ><fmt:formatNumber value="${stotal}" pattern="#" type="number"/>元</span>
									</div>		
										
																			
									<div class='col-md-12'>
										<label  class='lab-6'>本次应付金额：</label>			
								<%-- 		<input class='list_input input-hometab' id="pricrDiffer" name="pricrDiffer" value="${picrDiffer}"  /> --%>
											<span class='e84e40'  data-price='price' ><fmt:formatNumber value="${picrDiffer}" pattern="#" type="number"/> </span>								
									</div>
									
									<div class='col-md-12'>
										<label  class='lab-6'>本次实付金额：</label>
										<input class='list_input input-hometab' id="realMoney" name="realMoney" value="<fmt:formatNumber value='${picrDiffer}' pattern='#' type='number'/>" onblur = "subtraction(this.value)" >
									</div>
								
									<div class='col-md-12'>
										<label  class='label-4'>找零：</label>
										<input class='list_input input-hometab' id="changeMoney" name="changeMoney" value="0" >
									</div>
										
									
								

									<div class='col-md-12'>
										<label  class='label-4'>备注：</label>
										<textarea class='list_input textarea-hometab margin-B' id="comment" name="comment"> </textarea>
									</div>
									
									
									<p class='col-md-4 p '>丧葬用品</p>
									<div class="box-tools pull-right" style="padding-right:10px;">
										<button type="button" class="btn btn-box-tool">
											<i class="fa fa-chevron-down fa-plus"></i>
										</button>
									</div>
									<div class="collapse-block" style="display:none;">
											<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>
														 名称
													</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(list)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${list }" var="u">
													<tr>															
														<td>${u.item.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
														<td colspan="6">
															<font color='black'>共</font>
															<font color='red' >${fn:length(list)}</font>
															<font color='black'> 条记录</font>
														</td>
													</tr>
												</c:otherwise>
										  		</c:choose>														
											</tbody>
											
										</table>
									</div>
									<p class='col-md-12 p '>服务项目</p>
									<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>
														 名称
													</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(list4)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${list4 }" var="u">
													<tr>															
														<td>${u.item.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
														<td colspan="6">
															<font color='black'>共</font>
															<font color='red' >${fn:length(list4)}</font>
															<font color='black'> 条记录</font>
														</td>
													</tr>
												</c:otherwise>
										  		</c:choose>														
											</tbody>
											
										</table>
									</div>
									
													<p class='col-md-12 p'>困难减免项 </p>
									
									<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>
														 名称
													</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(list2)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${list2 }" var="u">
													<tr>															
														<td>${u.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td><fmt:formatDate value="${u.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
														<td colspan="6">
															<font color='black'>共</font>
															<font color='red' >${fn:length(list2)}</font>
															<font color='black'> 条记录</font>
														</td>
													</tr>
												</c:otherwise>
										  		</c:choose>														
											</tbody>
											
										</table>
									</div>
									
									
									<p class='col-md-12 p'> 基本减免项</p>
									
									<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>
														 名称
													</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
													<c:when test="${fn:length(list3)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
	
												<c:otherwise>
												<c:forEach items="${list3 }" var="u">
													<tr>															
														<td>${u.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td><fmt:formatDate value="${u.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
														<td colspan="6">
															<font color='black'>共</font>
															<font color='red' >${fn:length(list3)}</font>
															<font color='black'> 条记录</font>
														</td>
													</tr>
												</c:otherwise>
										  		</c:choose>														
											</tbody>
											
										</table>
									</div>
									
									
					                </div>
									</div>
									</div>
								</div>
								<!--endprint0-->
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
	<section class='content' style="min-height:100px">
				<!-- /.col -->
				<div class='box' style='padding:10px;'>
					<div class='row'>
						<div class='col-md-6' style='margin-top:0px;'>
							<div class='row'>
								<div class='col-md-7'>
									<label  class='lab-4'>业务单号：</label>
									<input class='list_input input-content' id="taskNum" name="taskNum"  value='${code}'>
								</div>
								<div class='col-md-5 '>
									<label  class='lab-1'>经办人：</label>
									<span>${payeeId}</span>
								</div>
							</div>
						</div>
						<div class='col-md-6'>
							<small class='pull-right btns-hometab'>
							<button type="submit" data-a='dismiss' class="btn btn-info">保存</button>
								<button type='reset' class='btn btn-color-9E8273'>重置</button>
<!-- 								<a href="javascript:payprint()"  class='btn btn-color-ab47bc'>打印</a> -->
								<!-- 								
								<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
								 -->
								 <c:choose>
								 	<c:when test="${position == 'home' }">
								 		<a href='login.do?method=main' rel="myModal" id="return" class="btn btn-default" role="button">返回</a> 
								 	</c:when>
								 	<c:otherwise>
										<a href="delegate.do?method=list" target="homeTab" rel="myModal" id="return" class="btn btn-default" role="button">返回</a> 
								 	</c:otherwise>
								 </c:choose>
							</small>
						</div>
					</div>
				</div>
	</section>
</form>
