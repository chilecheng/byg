<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header" id="s_hiddden">
	<i class="title-line"></i>
	<div class="title">委托业务收费</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
	.btn-color-ab47bc{
		background-color:#ab47bc;
		color:#ffffff;
	}
</style>
<script type="text/javascript">
   timeMunite();
   $(".btn.btn-box-tool").click(function(){
	   $(".collapse-block").slideToggle("slow");
   });
   function payprint(){ 
		$("#hh_hidden").hide();
		$("#s_hiddden").hide();
		$("#m_hiddden").hide();
		$("#f_hidden").hide();
		$("#buttonHidden").hide();
		$("#hiddenDown").hide();
		$("#page-content").css({"margin-left":"0px"});
		$("#page-content").css({"padding-top":"0px"});
		$("#showDiv").css({"display":"block"});
		$("#t_show").css({"top":"-80px"});
// 		$("#changeDiv").css({"top":"-100px"});
		$("#changeDiv").removeClass("col-md-7");
		$("#changeDiv").addClass("col-md-12");
		var comments=$("#comment").val();
		if(comments===null || comments==='undefined' || comments===''){
			$("#printHide").hide();
		}else{
			$("#comment").css({"border":"none"});
			$("#comment").css({"height":"auto"});
			$("#comment").removeClass("margin-B");
		}
		
	    window.print();  
	    	
	    $("#hh_hidden").show();
	    $("#s_hiddden").show(); 
	    $("#m_hiddden").show();
	    $("#f_hidden").show();
	    $("#buttonHidden").show();
	    $("#hiddenDown").show();
	    $("#page-content").css({"margin-left":"230px"});
	    $(".content-wrapper").css("padding-top","65px"); 
	    $("#t_show").css({"top":"0px"});
	    $("#changeDiv").css({"top":"0px"});
	    $("#changeDiv").removeClass("col-md-12");
		$("#changeDiv").addClass("col-md-7");
		$("#comment").addClass("margin-B");
		$("#comment").css({"border":"1px rgb(169, 169, 169) solid"});
		$("#printHide").show();
	}
</script>
</script>
<form action="delegate.do?method=save" id="pageForm"  onsubmit="return validateHomeCallback(this,homeAjaxDoneNo);">
<!-- <form action="delegate.do?method=save" id="pageForm"  onsubmit="return validateSub(this,homeAjaxDone);"> -->
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input name="orderId" id="orderId"  value="${orderId}" type="hidden">
	<input name="serviceCode" id="serviceCode"  value="${code}" type="hidden">
	<input name="payNumber" id="payNumber"  value="${payNumber}" type="hidden">
	<input name="orderNumber" id="orderNumber"  value="${orderNumber}" type="hidden">
	
	
	<section class="content" >
		<div class="row" >
			<div class="col-md-12">
				<div class="box box-warning" id="t_show">
					<!-- /.box-header -->
						<div class="box-body" style='padding:0px;'>
							<div class='col-md-12'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>死者姓名：</label>
										<span>${name}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>收款方式：</label>
										<select class="list_select" id="payWay" name="payWay" >
											${payTypeOption }
										</select>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-3'>付款人：</label>
										<input class='list_select' id="payName" name="payName" value="${fName }">
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>收款日期：</label> 
										<input readOnly='readOnly' data-id="reservation"  style='min-width:180px;' class="list_select " id="payTime" name="payTime" value="<fmt:formatDate value="${currentTime}" pattern="yyyy-MM-dd HH:mm"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
									<div class='col-md-6'>
										<label  class='lab-2'>应收金额：</label>
										<span class='e84e40' data-price='price'><fmt:formatNumber value="${stotal}" pattern="#" type="number"/>元</span>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>机打发票：</label> 
										<span class='e84e40'><fmt:formatNumber value="${stotal}" pattern="#" type="number"/>元</span>
									</div>
									<div class='col-md-12'>
										<label  class='lab-2'>实收金额：</label>
										<input class='list_select' id="realMoney" name="realMoney" value="<fmt:formatNumber value='${stotal}' pattern='#' type='number'/>"  >
									</div>
									<div class='col-md-12' id="printHide">
										<label  class='label-4'>备注：</label>
										<textarea class='list_input textarea-hometab margin-B' id="comment" name="comment"></textarea>
									</div>
								</div>
							</div>
									<div class="box-tools pull-right" id="buttonHidden" style="padding-right:10px;">
										<button type="button" class="btn btn-box-tool">
											<i class="fa fa-chevron-down fa-plus"></i>
										</button>
									</div>
									<div class='row box-body colum'>
										<div class='row box-body'>
											<div class="collapse-block " style="display:none;" id="showDiv">
												<div class='col-md-12'>
												<table class="table table-bordered">
													<thead>
														<tr>
															<th colspan="6">丧葬用品</th>
														</tr>
														<tr>
															<th>名称</th>
															<th class="dis">单价（元）</th>
															<th>数量</th>													
															<th>小计</th>
															<th class="dis">日期</th>
															<th>备注</th>
														</tr>
													</thead>
													<tbody>
														<c:choose>
														<c:when test="${fn:length(list)==0 }">
															 <tr width="20">
															    <td colspan="6">
																	<font color="Red">没有数据</font>
																 </td>
															</tr>
														</c:when>
														<c:otherwise>
														<c:forEach items="${list }" var="u">
															<tr>															
																<td>${u.item.name }</td>
																<td class="dis"><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
																<td>${u.number }</td>
																<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
																<td class="dis"><fmt:formatDate value="${u.commissionOrder.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
																<td>${u.comment }</td>
															</tr>												
														</c:forEach>
															<tr>
																<td colspan="6">
																	<font color='black'>共</font>
																	<font color='red' >${fn:length(list)}</font>
																	<font color='black'> 条记录</font>
																</td>
															</tr>
														</c:otherwise>
												  		</c:choose>														
													</tbody>
												</table>
											</div>
											<div class='col-md-12'>
												<table class="table table-bordered">
													<thead>
														<tr>
															<th colspan="6">服务项目</th>
														</tr>
														<tr>
															<th>名称</th>
															<th class="dis">单价（元）</th>
															<th>数量</th>													
															<th>小计</th>
															<th class="dis">日期</th>
															<th>备注</th>
														</tr>
													</thead>
													<tbody>
														<c:choose>
														<c:when test="${fn:length(list4)==0 }">
															 <tr width="20">
															    <td colspan="6">
																	<font color="Red">没有数据</font>
																</td>
															</tr>
														</c:when>
														<c:otherwise>
														<c:forEach items="${list4 }" var="u">
															<tr>
																<td>${u.item.name }</td>
																<td class="dis"><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
																<td>${u.number }</td>
																<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
																<td class="dis"><fmt:formatDate value="${u.commissionOrder.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
																<td>${u.comment }</td>
															</tr>
														</c:forEach>
															<tr>
																<td colspan="6">
																	<font color='black'>共</font>
																	<font color='red' >${fn:length(list4)}</font>
																	<font color='black'> 条记录</font>
																</td>
															</tr>
														</c:otherwise>
												  		</c:choose>														
													</tbody>
													
												</table>
											</div>
											
											<div class='col-md-12'>
												<table class="table table-bordered">
													<thead>
														<tr>
															<th colspan="6">困难减免项</th>
														</tr>
														<tr>
															<th>名称</th>
															<th class="dis">单价（元）</th>
															<th>数量</th>													
															<th>小计</th>
															<th class="dis">日期</th>
															<th>备注</th>
														</tr>
													</thead>
													<tbody>
														<c:choose>
														<c:when test="${fn:length(list2)==0 }">
															 <tr width="20">
															    <td colspan="6">
																	<font color="Red">没有数据</font>
																 </td>
															</tr>
														</c:when>
														<c:otherwise>
														<c:forEach items="${list2 }" var="u">
															<tr>
																<td>${u.name }</td>
																<td class="dis"><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
																<td>${u.number }</td>
																<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
																<td class="dis"><fmt:formatDate value="${u.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
																<td>${u.comment }</td>
															</tr>
														</c:forEach>
															<tr>
																<td colspan="6">
																	<font color='black'>共</font>
																	<font color='red' >${fn:length(list2)}</font>
																	<font color='black'> 条记录</font>
																</td>
															</tr>
														</c:otherwise>
												  		</c:choose>														
													</tbody>
													
												</table>
											</div>
											
											<div class='col-md-12'>
												<table class="table table-bordered">
													<thead>
														<tr>
															<th colspan="6">基本减免项</th>
														</tr>
														<tr>
															<th>名称</th>
															<th class="dis">单价（元）</th>
															<th>数量</th>													
															<th>小计</th>
															<th class="dis">日期</th>
															<th>备注</th>
														</tr>
													</thead>
													<tbody>
														<c:choose>
															<c:when test="${fn:length(list3)==0 }">
															 <tr width="20">
															    <td colspan="6">
																	<font color="Red">没有数据</font>
																 </td>
															</tr>
														</c:when>
			
														<c:otherwise>
														<c:forEach items="${list3 }" var="u">
															<tr>
																<td>${u.name }</td>
																<td class="dis"><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
																<td>${u.number }</td>
																<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
																<td class="dis"><fmt:formatDate value="${u.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
																<td>${u.comment }</td>
															</tr>
														</c:forEach>
															<tr>
																<td colspan="6">
																	<font color='black'>共</font>
																	<font color='red' >${fn:length(list3)}</font>
																	<font color='black'> 条记录</font>
																</td>
															</tr>
														</c:otherwise>
												  		</c:choose>														
													</tbody>
												</table>
											</div>					
											</div>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
	<section class='content' style="min-height:100px" id="hiddenDown">
				<!-- /.col -->
				<div class='box' style='padding:10px;'  >
					<div class='row' class='col-md-12' >
						<div class='col-md-7' style='margin-top:0px;' id="changeDiv" >
								<div class='col-md-7' >
									<label  class='lab-4' >业务单：</label>
									<input class='list_select' style="width: 120px"  id="taskNum" name="taskNum"  value='${code}'>
								</div>
								<div class='col-md-5' >
									<label  class='lab-4'>经办人：</label>
									<input class='list_select' style="width: 120px" value="${payeeId}">
								</div>
						</div>
						<div class='col-md-5' id="m_hiddden">
							<small class='pull-right btns-hometab'>
								<button type="submit" data-a='dismiss' class="btn btn-info">保存</button>
								<a href="javascript:payprint()"  class='btn btn-color-ab47bc'>打印</a>
								<c:choose>
									<c:when test="${position == 'home' }">
								 		<a href='login.do?method=main' rel="myModal" id="return" class="btn btn-default" role="button">返回</a> 
									</c:when>
									<c:otherwise>
										<a href="delegate.do?method=list" target="homeTab" rel="myModal" id="return" class="btn btn-default" role="button">返回</a> 
									</c:otherwise>
								</c:choose>
							</small>
						</div>
					</div>
				</div>
	</section>
</form>
