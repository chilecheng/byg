<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">委托业务收费</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
</style>
<script type="text/javascript" src="jquery.js">

</script>


	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input name="orderId" id="orderId"  value="${orderId}" type="hidden">
	
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
					<!--startprint0-->
						<div class="box-body" style='padding:0px;'>
						<div class='col-md-12'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>死者姓名：</label>
										<span>${name}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>收款方式：</label>
										<span>${businessFees.payTypeName }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-3'>付款人：</label>
										<span>${businessFees.payName}</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>收款日期：</label> 
										<span><fmt:formatDate value="${businessFees.payTime}" pattern="yyyy-MM-dd HH:mm"/></span>
									</div>
											
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>机打发票：</label> 
										<span><fmt:formatNumber value="${articlesTotal +serviceTotal -baseTotal -hardTotal}" pattern="#" type="number"/></span>
									</div>
									<div class='col-md-6'>
										<label  class='lab-6'>本次实收金额：</label>
										<span><fmt:formatNumber value="${businessFees.payAmount}" pattern="#" type="number"/></span>
									</div>
									<div class='col-md-12'>
										<label  class='label-4'>备注：</label>
										<span>${businessFees.comment }</span>
									</div>
									
									
									
								</div>
							</div>
							<div class='col-md-12 colum'>
								<div class='row box-body'>
									
								<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th colspan='2'>
														收费明细
													</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td width='40%'>服务项目</td>
													<td><fmt:formatNumber value="${serviceTotal}" pattern="#" type="number"/>元</td>
												</tr>	
												<tr>
													<td width='40%'>丧葬用品</td>
													<td><fmt:formatNumber value="${articlesTotal }" pattern="#" type="number"/>元</td>
												</tr>
												<tr>
													<td width='40%'>基本减免</td>
													<td><fmt:formatNumber value="${baseTotal }" pattern="#" type="number"/>元</td>
												</tr>
												<tr>
													<td width='40%'>困难减免</td>
													<td><fmt:formatNumber value="${hardTotal }" pattern="#" type="number"/>元</td>
												</tr>
												<tr>
													<td width='40%'>合计</td>
													<td><fmt:formatNumber value="${ articlesTotal +serviceTotal -baseTotal -hardTotal}" pattern="#" type="number"/>元</td>
												</tr>														
											</tbody>
											
										</table>
									</div>
										<p class='col-md-12 p '>服务项目</p>
									<div class='col-md-12'>
										<table class='table table-bordered'>
											<thead>
												<tr>
													<th>名称</th>
													<th class="dis">单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th class="dis">日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(serviceList)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${serviceList }" var="u">
													<tr>															
														<td>${u.item.name }</td>
														<td class="dis"><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td class="dis"><fmt:formatDate value="${u.payTime}" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
														<td colspan="6">
															<font color='black'>共</font>
															<font color='red' >${fn:length(serviceList)}</font>
															<font color='black'> 条记录</font>
														</td>
													</tr>
												</c:otherwise>
										  		</c:choose>					
											</tbody>
										</table>
									
									</div>
										<p class='col-md-12 p '>丧葬用品</p>
										<div class='col-md-12'>
										<table class='table table-bordered'>
											<thead>
												<tr>
													<th>名称</th>
													<th class="dis">单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th class="dis">日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(articlesList)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${articlesList }" var="u">
													<tr>															
														<td>${u.item.name }</td>
														<td class="dis"><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td class="dis"><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
														<td colspan="6">
															<font color='black'>共</font>
															<font color='red' >${fn:length(articlesList)}</font>
															<font color='black'> 条记录</font>
														</td>
													</tr>
												</c:otherwise>
										  		</c:choose>					
											</tbody>
										</table>
									
									</div>
								</div>
							</div>
						</div>
						<!--endprint0-->
					</div>
				</div>
			</div>
	</section>
	<section class='content' style="min-height:100px">
				<!-- /.col -->
				<div class='box' style='padding:10px;'>
					<div class='row'>
						<div class='col-md-7' style='margin-top:0px;'>
							<div class='row'>
								<div class='col-md-7'>
									<label  class='lab-4'>业务单号：</label>
									<span>${businessFees.serviceCode }</span>
								</div>
								<div class='col-md-5 '>
									<label  class='lab-4'>经办人：</label>
									<span>${businessFees.payeeId}</span>
								</div>
							</div>
						</div>
						<div class='col-md-5'>
							<small class='pull-right btns-hometab btns-print'>
							
								<a href="javascript:showprint()"  class='btn btn-color-ab47bc'>打印</a>
								<!-- 								
								<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
								 -->
								 <a href="delegate.do?method=list" target="homeTab" rel="myModal" id="return" class="btn btn-default" role="button">返回</a> 
							</small>
						</div>
					</div>
				</div>
	</section>

