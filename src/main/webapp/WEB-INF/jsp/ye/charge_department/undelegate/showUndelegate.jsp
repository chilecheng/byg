
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">非委托业务收费</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
	.btn-color-ab47bc{
		background-color:#ab47bc;
		color:#ffffff;
	}
</style>
<script type="text/javascript" src="jquery.js"></script>


	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input name="id" id="id"  value="${orderId}" type="hidden">
	
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
					<!--startprint0-->
						<div class="box-body" style='padding:0px;'>
							<div class='col-md-12'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>死者姓名：</label>
										<span>${name }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>收款方式：</label>
										<c:if test="${payName !=null}">
											<span>${payType }</span>
										</c:if>
									</div>
									<div class='col-md-6 height-align'>
										<label  class='lab-3'>付款人：</label>
										<c:if test="${payName !=null}">
											<span>${payName }</span>
										</c:if>
									</div>
									<div class='col-md-6 height-align'>
<!-- 										<label  class='lab-6'>收款日期：</label>  -->
<%-- 										<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select input-dialog" id="payTime" name="payTime" value="<fmt:formatDate value="${currentTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
										
										
										<label class='lab-2'>收款日期：</label>
										<c:if test="${payName !=null}"> 
											<span><fmt:formatDate value="${payTime }" pattern="yyyy-MM-dd HH:mm"/></span>
										</c:if>
									</div>
											
											 <!-- 
										<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-MM-dd" class="list_select input-hometab" id="payTime" name="payTime" value="${payTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>									
										 <input  type="text"  id="payTime" name="payTime" value="${payTime}"  data-date-format="yyyy-MM-dd HH:mm"  data-provide="datetimepicker"     data-min-view="1"    >  <i style="margin-left: -20px;" class="fa fa-calendar"  ></i>
											 -->
									<!-- data-provide="datetimepicker"   -->
										 <!-- 
									<input type="text" class="list_input" data-id="reservation" style='min-width:40%' id="payTime" name='payTime' value=''><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									 -->
								
									<div class='col-md-6 height-align'>
										<label  class='lab-2'>机打发票：</label> 
										<span><fmt:formatNumber value="${total}" pattern="#" type="number"/> 元</span>
									</div>
									
									<div class='col-md-12'>
										<label  class='lab-2'>应收金额：</label>
										<span><fmt:formatNumber value="${total}" pattern="#" type="number"/> 元</span>
									</div>
									<div class='col-md-12'>
										<label  class='lab-2'>实收金额：</label>
										<c:if test="${payName !=null}">
											<span><fmt:formatNumber value="${actualAmount }" pattern="#" type="number"/> 元</span>
										</c:if>
									</div>
									<div class='col-md-12'>
										<label  class='label-4'>找零：</label>
										<c:if test="${payName !=null}">
											<span><fmt:formatNumber value="${giveChange }" pattern="#" type="number"/> 元</span>
										</c:if>
									</div>
									<div class='col-md-12'>
										<label  class='label-4'>备注：</label>
										<span>${comment }</span>
									</div>
									<div class='col-md-12'>
										<table class='table table-bordered'>
											<thead>
												<tr>
													<th>名称</th>
													<th>单价（元）</th>
													<th>数量</th>
													<th>小计</th>
													<th>日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(list)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${list }" var="u">
													<tr>															
														<td>${u.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td><fmt:formatDate value="${u.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
												</c:otherwise>
										  		</c:choose>
											</tbody>
										</table>
									
									</div>
								</div>
							</div>
						</div>
						<!--endprint0-->
					</div>
				</div>
			</div>
	</section>
	<section class='content' style="min-height:100px">
				<!-- /.col -->
				<div class='box' style='padding:10px;'>
					<div class='row'>
						<div class='col-md-7' style='margin-top:0px;'>
							<div class='row'>
								<div class='col-md-7'>
									<label  class='lab-4'>业务单号：</label>
									<span>${serialNumber }</span>
								</div>
								<div class='col-md-5 '>
									<label  class='lab-1'>经办人：</label>
									<span>${userName }</span>
								</div>
							</div>
						</div>
						<div class='col-md-5'>
							<small class='pull-right btns-hometab btns-print'>
								<a href="javascript:payprint()"  class='btn btn-color-ab47bc'>打印</a>
<!-- 								<button type='button' class='btn btn-color-ab47bc'>打印</button> -->
								<!-- 								
								<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
								 -->
								 <a href="nonCommissioned.do?method=list" target="homeTab" rel="myModal" id="return" class="btn btn-default" role="button">返回</a> 
							</small>
						</div>
					</div>
				</div>
	</section>

