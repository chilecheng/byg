<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">非委托收费业务</div>
</section>
<form action="${url }" data-is="true" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" id="method" value="list" type="hidden">
	<input type="hidden" name="fid" value="11">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input nopadding-R" id="findBy" name="findBy">
									${searchOption }
								</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" id='searchVal' name='searchVal' value='${searchVal}' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>收费时间：</label> 
								<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span>
								<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								
							</div>
							<!-- 控制常用搜索状态（收费等） -->
							<input type="hidden" id="checkType" name="checkType" value="${checkType }">
							<div class="col-md-12" id="typeDiv">
								<label>常用：</label>
								<button type="submit" class="btn btn-normally btn-all" name="payAll">全部</button>
								<button type="submit" class="btn btn-normally" name="payYes">已收费</button>
								<button type="submit" class="btn btn-normally" name="payNo">未收费</button>
							</div>
							<div class="col-md-12">
								<button type="submit" id="timeRefresh" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>
			<div class="row nomargin-L nomargin-R padding-B">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 btns">
							<small class='btns-charge'>
								<a href="${url}?method=edit&id=" checkone="true" data-code='2-4-8' data-checkpay='true' checkname="id" class="btn btn-warning" rel="myModal" target='homeTab' >收费</a>
								<a href="${url}?method=revoke&id=" target="ajaxTodo" data-code='2' checkone="true" warm="确定撤消收费吗" checkname="id" rel="myModal"  class="btn btn-danger">撤销收费</a>
													
							</small>
						</div>
					</div>
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th>
									  
								</th>
								<th>购买单号</th>
								<th>流水号</th>
								<th>死者编号</th>
								<th>死者姓名</th>
								<th>年龄</th>
								<th>死者性别</th>
								<th>付款人</th>
								<th>总金额</th>
								<th>收费状态</th>
							</tr>
						</thead>
						<tbody>
						<c:choose>
							<c:when test="${fn:length(page.list)==0 }">
								 <tr width="20">
								    <td colspan="10">
										<font color="Red">还没有数据</font>
									 </td>
								</tr>
							</c:when>
						<c:otherwise>
							<c:forEach items="${page.list }" var="u">
									<tr>
										<td><input type='radio' class='checkBoxCtrl' name='id' value='${u.id }'/></td>
										<td>${u.buyCode }</td>
										<td>${u.serialNumber }</td>
										<td>${u.code }</td>
										<td><a href="${url}?method=show&id=${u.id}&serialNumber=${u.serialNumber}&total=${u.total}&name=${u.name}"   rel="myModal" target='homeTab' >${u.name }</a></td>
										<td>${u.age }</td>
										<td>${u.sexName }</td>
										<td>${u.payName }</td>
										<td><fmt:formatNumber value="${u.total}" pattern="#" type="number"/></td>
										<td class='changeColor state1'>${u.payFlagName }</td>
									</tr>
							
							</c:forEach>
						</c:otherwise>
					  </c:choose>
						</tbody>
					</table>
					
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>

<script type="text/javascript">
var fresh;
	(function(){
		//更改fid 以区别一科与收费科的非委托收费方式
		var fid=$(".treeview-menu.menu-open").attr('id');
		$('[name="fid"]').val(fid);
// 		alert(fid);
// 		setTimeout("this.location.href='nonCommissioned.do?method=list&fid=${fid}'",3000);
		$('.changeColor').each(function(){//修改状态样式
			var txt=$(this).text();
			txt==='已收费'&&$(this).css('color','#9CCC65');
			txt==='未收费'&&$(this).css('color','#e84e40');
		})
	})()
	//定时刷新此页面(好像可以了)
// 	fresh=setTimeout( "homeSearch2(pageForm);", 60000 );
// 	setTimeout(homeSearch.bind(document.getElementById('pageForm')),5000);

	$(function(){
		timeRange();
	});
	//将点击的常用搜索按钮对应值改变，用于后台判断
	var checkType='payAll';
	$("#typeDiv").on('click','button',function(){	
		checkType=$(this).attr('name');
		$("#checkType").val(checkType);
	})
</script>