<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">非委托收费业务</div>
</section>
<style type="text/css">
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
	.padding{
		padding:15px 30px;
	}
	.content:last-child{
		min-height:100px;
	}
	.btn-color-ab47bc{
		background-color:#ab47bc;
		color:#ffffff;
	}
</style>
<script type="text/javascript">

	function giveChange(real,total,changeMoney){
		var cash=$(real).val();
		var change=cash-total;
		$(changeMoney).val(change);
		
	}
	$(timeMunite);
	function saveDate(callback){
		var payTime=$("#payTime").val();
		var payName=$("#payName").val();
		var realMoney=$("#realMoney").val();
		var changeMoney=$("#changeMoney").val();
		var payType=$("#payWay").val();
		var comment=$("#comment").val();
		$.ajax({
			url:"nonCommissioned.do",
			data:{"method":"save",payTime:payTime,payName:payName,realMoney:realMoney,changeMoney:changeMoney,
				serialNumber:'${serialNumber }',comment:comment,payType:payType,id:'${id}'},
			type:"post",
			dataType:"json",
			success:callback
		});
	}
</script>
<form action="${URL }" id="pageForm" onsubmit="return homeSearch(this);">
	
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input name="id" id="id" value="${id }" type="hidden">
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
					<!--startprint0-->
						<div class="box-body" style='padding:0px;'>
							<div class='col-md-12'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>死者姓名：</label>
										<span>${name }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>收款方式：</label>
										<c:if test="${dept==2}">
											<select class="list_select nopadding-R " id="payWay" name="payWay">
												${payType }
											</select>
										</c:if>
										<c:if test="${dept==1}">
											<select class="list_select nopadding-R "  id="payWay" name="payWay">
												${aliPay }
											</select>
										</c:if>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-3'>付款人：</label>
										<input class='list_input input-hometab' id="payName" name="payName" value="${buyName }">
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>收款日期：</label> 
										<input data-id="reservation" readonly='readOnly'   class="list_select" id="payTime" name="payTime" value="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
<!-- 									<div class='col-md-6'> -->
<!-- 										<label class='lab-6'>作废发票：</label> -->
<!-- 										<span>11111111111111</span> -->
<!-- 									</div> -->
<!-- 									<div class='col-md-6'> -->
<!-- 										<label class='lab-6'>作废发票号码：</label> -->
<!-- 										<span>11111111111111</span> -->
<!-- 									</div> -->
									<div class='col-md-6 height-align'>
										<label class='lab-2'>机打发票：</label>
										<span class='e84e40'><fmt:formatNumber value="${total}" pattern="#" type="number"/>元</span>
									</div>
									<c:choose>
										<c:when test="${realMoney !=null }">
											<div class='col-md-12'>
												<label class='lab-6'>前期实付金额：</label>
												<span class='e84e40'><fmt:formatNumber value="${realMoney}" pattern="#" type="number"/>元</span>
											</div>
											<div class='col-md-12'>
												<label class='lab-6'>总计应收金额：</label>
												<span class='e84e40'><fmt:formatNumber value="${total}" pattern="#" type="number"/>元</span>
											</div>
											<div class='col-md-12'>
												<label class='lab-6'>本次应收金额：</label>
												<span class='e84e40'><fmt:formatNumber value="${total-realMoney}" pattern="#" type="number"/>元</span>
											</div>
											<div class='col-md-12'>
												<label class='lab-6'>本次实收金额：</label>
												<input class='list_input input-hometab' id="realMoney" name="realMoney" value="<fmt:formatNumber value='${total-realMoney}' pattern='#' type='number'/>" onkeyup="giveChange(this,${total},'#changeMoney')">
											</div>
										</c:when>
										<c:otherwise>
											<div class='col-md-12'>
												<label class='lab-2'>应收金额：</label>
												<span class='e84e40'><fmt:formatNumber value="${total}" pattern="#" type="number"/>元</span>
											</div>
											<div class='col-md-12'>
												<label class='lab-2'>实收金额：</label>
												<input class='list_input input-hometab' id="realMoney" name="realMoney" value="<fmt:formatNumber value='${total}' pattern='#' type='number'/>" onkeyup="giveChange(this,${total},'#changeMoney')">
											</div>
										</c:otherwise>
									</c:choose>							
									
									<div class='col-md-12'>
										<label class='label-4'>找零：</label>
										<input class='list_input input-hometab' id="changeMoney" name="changeMoney" value="0" >
									</div>
									<div class='col-md-12'>
										<label class='label-4'>备注：</label>
										<textarea class='list_input textarea-hometab' id="comment" name="comment" ></textarea>
									</div>
									<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>
														 名称
													</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>日期</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(list)==0 }">
													 <tr width="20">
													    <td colspan="6">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${list }" var="u">
													<tr>															
														<td>${u.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td><fmt:formatDate value="${u.creatTime }" pattern="yyyy-MM-dd HH:mm"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
												</c:otherwise>
										  		</c:choose>
											</tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
						<!--endprint0-->
					</div>
				</div>
				<!-- /.col -->
			</div>
		
	</section>
	<section class='content'>
		<div class='box padding'>
			<div class='row'>
				<div class='col-md-6' style='margin-top:0px;'>
					<div class='row'>
						<div class='col-md-7 height-align'>
							<label class='lab-4'>业务单号：</label>
							<input class='list_input ' id="taskNum" name="taskNum" value="${serialNumber }">
						</div>
						<div class='col-md-5 height-align'>
							<label class='lab-1'>经办人：</label>
							<span>${userName }</span>
						</div>
					</div>
				</div>
				<div class='col-md-6'>
					<small class='pull-right btns-hometab'>
						<a href="javascript:void(0);" onclick="saveDate(homeAjaxDone)"  id="sure" class="btn btn-info" role="button">保存</a>
						<button type='reset' class='btn btn-color-9E8273'>重置</button>
<!-- 						<a href="javascript:payprint()"  class='btn btn-color-ab47bc'>打印</a> -->
						<a href="${url}?method=list" target="homeTab" rel="myModal" id="return" class="btn btn-default" role="button">返回</a>
					</small>
				</div>
			</div>
		</div>			
	</section>
</form>