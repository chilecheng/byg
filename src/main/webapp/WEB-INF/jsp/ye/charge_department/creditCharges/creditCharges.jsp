<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">挂账收费</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
</style>

<form action="${url}" id="pageForm"	onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	
	<input name="method" value="list" type="hidden">
	
	
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input nopadding-R" id="select" 	name="select">
									${searchOption }
								</select>
							</div>
							<div class="col-md-4">
								<label>查询值：</label> 
								<input type="text" class="list_input"
									id='selectValue' name='selectValue' value= "${selectValue}" placeholder='单行输入' />
							</div>
				
							
							<div class="col-md-12">
								<label>收费时间：</label> 
								<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span>
								<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							</div>
							<!-- 控制  下面 button  的相关属性 -->
							<input type="hidden" id="checkType" name="checkType" value="${checkType }">
							<div class="col-md-12" id="typeDiv">
								<label>常用：</label>
								<button type="submit" class="btn btn-normally btn-all"  name="stationAll"> 全部</button>
								<button type="submit" class="btn btn-normally" name="stationYes">已收费</button>
								<button type="submit" class="btn btn-normally"  name="stationNo" >未收费</button>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content nopadding-T" id='main-content'>
		<div class='box'>
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 btns">
							<small class='btns-buy'>
								<button type='button'  class='btn btn-warning' checkone="true" checkname="id" data-fun='freezer' data-num='1'>收费</button>
								<a  href="${url}?method=edit&id=" checkone="true" checkname="id"	rel="myModal"  class="btn hide" target='homeTab'>收费 </a>		
<%-- 								<a  href="${url}?method=cancle&id=" target="ajaxTodo" data-code='code' checkone="true" warm="确定撤消收费吗" checkname="id" rel="myModal"  class="btn btn-danger">撤销收费</a>		 --%>
							</small>			
						</div>
					</div>
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th> </th>
								<th>收费流水号</th>
								<th>死者姓名</th>
								<th>挂账单位</th>
								<th>收费状态</th>
								<th>收费日期</th>
							</tr>
						</thead>
						<tbody>
						<c:choose> 
							<c:when test="${fn:length(page.list)==0 }"> 
								 <tr width="20"> 
								    <td colspan="6"> 
										<font color="Red">还没有数据</font> 
 									 </td> 
								</tr> 
 							</c:when> 
 						<c:otherwise>
							<c:forEach items="${page.list }" var="u"> 
									<tr>
										<td><input type='radio' class='checkBoxCtrl' name='id' value='${u.commissionOrderId} '/></td>
										<td>${u.serialNumber}GZYW</td>
										<td><a href="${url}?method=show&id=${u.commissionOrderId}" class="loser" target="homeTab" rel="myModal">${u.dName }</a></td>
										<td>${u.billUnitName }</td>
										<td class='changeColor' >
											<c:if test="${u.payFlag==1 }">
												<font color="#9CCC65" class='state1'>已收费</font>
											</c:if>
											<c:if test="${u.payFlag==2 }">
												<font color="#e84e40" class='state1'>未收费</font>
											</c:if>
										</td>
										<td><fmt:formatDate value="${u.payTime}" pattern="yyyy-MM-dd HH:mm"/></td>
									</tr>
							
							</c:forEach> 
						</c:otherwise>
						</c:choose>		
						</tbody>
					</table>
					
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>
<script>

	$(function(){
	timeRange();
	})
	//当前页状态改变，用于后台判断，页面刷新之后跳转到当前页
var type=active.parsent;
$("#clickId").val(type);
$('[aria-controls="'+type+'"]').click();
$("#liClick").on('click','li a',function(){
	type=$(this).attr('aria-controls');
	active.parsent=type;
	$("#clickId").val(type);
})
	
	var checkType='stationAll';
	$("#typeDiv").on('click','button',function(){	
		checkType=$(this).attr('name');
		$("#checkType").val(checkType);
	})
 		
	$('[data-fun="freezer"]').click(function(){
	var checkname=$(this).attr('checkname');
	var value=$('[name="'+checkname+'"]:checked').parent().siblings().eq(3).text().trim();
		if(value==='已收费'&&$('[name="'+checkname+'"]:checked').size()!==0){
			toastr["warning"]("不能进行此操作");
		}else{
			$(this).next().click();
		}
	})
	
	
	
</script>