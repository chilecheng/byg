<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">挂账收费</div>
</section>
<style type="text/css">
	.box div.col-md-6{
		height:32px;
		line-height:32px;
	}
	.e84e40{
		color:#e84e40;
	}
	.padding{
		padding:15px 30px;
	}
	.content:last-child{
		min-height:100px;
	}
	.btn-color-ab47bc{
		background-color:#ab47bc;
		color:#ffffff;
	}
</style>
<script type="text/javascript">

	function giveChange(real,total,changeMoney){
		var cash=$(real).val();
		var change=cash-total;
		$(changeMoney).val(change);
		
	}
// 	$(timeMunite);
</script>
<form action="${URL }" id="pageForm" onsubmit="return homeSearch(this);">
	
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<input name="id" id="id" value="${id }" type="hidden">
	<input name="billUnitId" id="billUnitId" value="${billUnitId }" type="hidden">
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<!-- /.box-header -->
					<!--startprint0-->
						<div class="box-body" style='padding:0px;'>
							<div class='col-md-12'>
								<div class='row'>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>死者姓名：</label>
										<span>${dName }</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>收款方式：</label>
											<select class="list_select nopadding-R " id="payWay" name="payWay">
												${payTypeOption }
											</select>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-3'>付款人：</label>
										<input class='list_input input-hometab' id="payName" name="payName" value="${credit.payName}">
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>收款日期：</label> 
										<input data-id="reservation" readonly='readOnly'  class="list_select " id="payTime" name="payTime" value="<fmt:formatDate value="${credit.payTime}" pattern="yyyy-MM-dd HH:mm"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
<!-- 									<div class='col-md-6 height-align'> -->
<!-- 										<label class='lab-2'>机打发票：</label> -->
<%-- 										<span class='e84e40'><fmt:formatNumber value="${total}" pattern="#" type="number"/>元</span> --%>
<!-- 									</div> -->
									<div class='col-md-6'>
										<label class='lab-2'>应收金额：</label>
										<span class='e84e40'><fmt:formatNumber value="${total}" pattern="#" type="number"/>元</span>
									</div>
									<div class='col-md-6 height-align'>
										<label class='lab-2'>挂账单位：</label>
										<span>${billName }</span>
									</div>
									<div class='col-md-7'>
										<label class='lab-2'>实收金额：</label>
										<input class='list_input input-hometab' id="realMoney" name="realMoney" value="<fmt:formatNumber value='${credit.payMoney}' pattern='#' type='number'/>" onkeyup="giveChange(this,${total},'#changeMoney')">
									</div>
									
									<div class='col-md-7'>
										<label class='label-4'>找零：</label>
										<input class='list_input input-hometab' id="changeMoney" name="changeMoney" value="${credit.changeMoney }" >
									</div>
<!-- 									<div class='col-md-12'> -->
<!-- 										<label class='label-4'>备注：</label> -->
<!-- 										<textarea class='list_input textarea-hometab' id="comment" name="comment" ></textarea> -->
<!-- 									</div> -->
									<p class='col-md-12 p'>服务项目</p>
									<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="width:280px">名称</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(listOrderService)==0 }">
													 <tr width="20">
													    <td colspan="5">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${listOrderService }" var="u">
													<tr>															
														<td>${u.item.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
													<td >合计：</td>
													<td colspan='4' style='text-align: left;' >
													<font color='red'>${totalService }元</font>
													<div style="float: right; margin-right: 50%;">
														<font color='black' >合计：共</font>
														<font color='red' id='serviceNumber' >${fn:length(listOrderService)}</font>
														<font color='black' >条记录</font>
													</div>
													</td>
													</tr>
												</c:otherwise>
										  		</c:choose>
											</tbody>
											
										</table>
									</div>
									<p class='col-md-12 p'>丧葬用品</p>
									<div class='col-md-12'>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th style="width:280px">名称</th>
													<th>单价（元）</th>
													<th>数量</th>													
													<th>小计</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody>
												<c:choose>
												<c:when test="${fn:length(listOrderArticles)==0 }">
													 <tr width="20">
													    <td colspan="5">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
												<c:otherwise>
												<c:forEach items="${listOrderArticles }" var="u">
													<tr>															
														<td>${u.item.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
													<tr>
													<td >合计：</td>
													<td colspan='4' style='text-align: left;' >
													<font color='red'>${totalArticles }元</font>
													<div style="float: right; margin-right: 50%;">
														<font color='black' >合计：共</font>
														<font color='red' id='serviceNumber' >${fn:length(listOrderArticles)}</font>
														<font color='black' >条记录</font>
													</div>
													</td>
													</tr>
												</c:otherwise>
										  		</c:choose>
											</tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
						<!--endprint0-->
					</div>
				</div>
				<!-- /.col -->
			</div>
		
	</section>
	<section class='content'>
		<div class='box padding'>
			<div class='row'>
<!-- 				<div class='col-md-6' style='margin-top:0px;'> -->
<!-- 					<div class='row'> -->
<!-- 						<div class='col-md-7 height-align'> -->
<!-- 							<label class='lab-4'>业务单号：</label> -->
<%-- 							<input class='list_input ' id="taskNum" name="taskNum" value="${serialNumber }"> --%>
<!-- 						</div> -->
<!-- 						<div class='col-md-5 height-align'> -->
<!-- 							<label class='lab-1'>经办人：</label> -->
<%-- 							<span>${userName }</span> --%>
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
				<div class='col-md-12'>
					<small class='pull-right btns-hometab'>
<!-- 						<a href="javascript:void(0);" onclick="saveDate(homeAjaxDone)"  id="sure" class="btn btn-info" role="button">保存</a> -->
<!-- 						<button type='reset' class='btn btn-color-9E8273'>重置</button> -->
<!-- 						<a href="javascript:payprint()"  class='btn btn-color-ab47bc'>打印</a> -->
						<a href="${url}?method=list" target="homeTab" rel="myModal" id="return" class="btn btn-default" role="button">返回</a>
					</small>
				</div>
			</div>
		</div>			
	</section>
</form>