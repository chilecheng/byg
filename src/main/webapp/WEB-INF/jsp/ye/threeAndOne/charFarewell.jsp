<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="content-header" id="s_hiddden">
	<i class="title-line"></i>
	<div class="title">礼厅服务质量反馈表</div>
</section>
<div class='content'>
	<div class='box box-info ' style='padding:0px 15px'>
		<div class='row' id='t_show'>
			<h2 class='col-md-12' id='p-title' style='text-align:center;font-weight:bold;'>礼厅服务质量反馈表</h2>
			<p class='col-md-12' style='text-align:center; font-size:16px;'><fmt:formatDate value="${printTime }" pattern="yyyy-MM-dd"/></p>
			<div class='col-md-12'>
			<input name="method" value="${method}" type="hidden" />
			<input name="deadId" value="dac2a1a540a64403ad807f96a7813d86" type="hidden" />
				<table class='table table-bordered' id='stable' style='margin-bottom:0px;'>
					<tbody>
						<tr>
							<td colspan='12'><b>【逝者基本信息】</b></td>
						</tr>
						<tr>
							<td><b>厅号</b></td>
							<td>${tao.farewellName}</td>
							
							<td><b>告别时间</b></td>
							<td><fmt:formatDate value="${printTime }" pattern="HH:mm"/></td>
							<td><b>逝者姓名</b></td>
							<td>${tao.name} </td>
							<td><b>性别</b></td>
							<td>${tao.sex }</td>
							<td><b>年龄</b></td>
							<td>${tao.age }</td>
							<td><b>灵堂</b></td>
							<td>${tao.mouringName }</td>
							
						</tr>
						<tr>
							<td colspan='12'><b>【挽联布置】</b></td>
						</tr>
						<tr>
							<td><b>主联</b></td>
							<td colspan='2'>${tao.contactPrice }</td>
							<td><b>边联</b></td>
							<td colspan='2'></td>
							<td><b>门联</b></td>
							<td colspan='5'></td>
						</tr>
						<tr>
							<td colspan='12'><b>【礼厅布置】</b></td>
						</tr>
						<tr>
							<td><b>围花</b></td>
							<td>${tao.waiFlowerPrice }</td>
							<td><b>黑白纱</b></td>
							<td>${tao.yarnPrice }</td>
<!-- 							<td>白纱</td> -->
<!-- 							<td></td> -->
							<td><b>孝球</b></td>
							<td>${tao.ballPrice }</td>
							<td><b>花门</b></td>
							<td>${tao.flowerDoorPrice }</td>
							<td><b>礼厅背景</b></td>
							<td>${tao.backGroundPrice }</td>
							<td><b>其他</b></td>
							<td></td>
						</tr>
						<tr>
							<td><b>花圈</b></td>
							<td colspan='4'></td>
							<td><b>花柱</b></td>
							<td colspan='2'></td>
							<td><b>花牌</b></td>
							<td colspan='3'></td>
						</tr>
						<tr>
							<td rowspan='2'><b>花篮</b></td>
							<td colspan='11'><b>1.鲜花篮：</b></td>
						</tr>
						<tr>
							<td><b>2.绢花篮：</b></td>
							<td colspan='4'></td>
							<td colspan='3'><b>花圈花篮个数统计人</b></td>
							<td colspan='3'>
								<select style="width: 165px" class="list_select nopadding-R">
									${flowersOption }
								</select><i class='print-i'></i>
							</td>
						</tr>
						<tr>
							<td colspan='12'><b>【礼厅主持】</b></td>
						</tr>
						<tr>
							<td><b>司仪</b></td>
							<td>${tao.masterPrice }</td>
							<td><b>单位致辞</b></td>
							<td></td>
							<td><b>家属致辞</b></td>
							<td></td>
							<td><b>其他</b></td>
							<td colspan='5'></td>
						</tr>
						<tr>
							<td colspan='12'><b>【其他项目】</b></td>
						</tr>
						<tr>
							<td><b>礼仪出殡</b></td>
							<td>${tao.funeralPrice }</td>
							<td><b>电子遗像</b></td>
							<td>${tao.electronicPortraitPrice }</td>
							<td><b>摄像</b></td>
							<td>${tao.cameraPrice }</td>
							<td><b>附加</b></td>
							<td></td>
							<td><b>场地卫生</b></td>
							<td colspan='3'></td>
						</tr>
						<tr>
							<td colspan='12'><b>【客户对礼厅服务评价】</b></td>
						</tr>
						<tr>
							<td><b>满意</b></td>
							<td colspan='2'>（  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ）</td>
							<td colspan='3'><b>基本满意</b></td>
							<td colspan='2'>（ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ）</td>
							<td colspan='2'><b>不满意</b></td>
							<td colspan='2'>（  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ）</td>
						</tr>
						<tr>
							<td colspan='8' rowspan='2' style='text-align:left;'>
								<b>意见与建议：</b>
							</td>
							<td><b>丧属签名</b></td>
							<td colspan='3'></td>
						</tr>
						<tr>
							<td><b>与逝者关系</b></td>
							<td colspan='3'></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class='col-md-12' style='margin:10px 0px 10px;'>
				<div class='row'>
					<span class='col-md-3' style=' height: 18px;margin-top: 5px;'>礼厅工作人员：
						<select style="width: 90px" class="list_select nopadding-R">
							${liOption }
						</select><i class='print-i'></i>
					</span>
					<span class='col-md-3 ' style=' height: 18px;margin-top: 5px;'>打印人员：
						<select style="width: 90px" class="list_select nopadding-R">
							${flowersOption }
						</select><i class='print-i'></i>
					</span>
					<span class='col-md-3 ' style=' height: 18px;margin-top: 5px;'>核对员：
						<select style="width: 90px" class="list_select nopadding-R">
							${flowersOption }
						</select><i class='print-i'></i>
					</span>
					<span class='col-md-3'>打印时间：<fmt:formatDate value="${nowTime }" pattern="yyyy-MM-dd HH:mm"/></span>
				</div>
			</div>
		</div>
		
		
	</div>
	
	<div class="box-body" style='background-color:#fff;' id='m_hiddden'>
		<div  class="container-fluid">
			<div class='row padding-B'>
				<div class='col-md-12 height-align'>
					<div class='row'>
						<div class="col-md-12">
							<small class="pull-right btns-hometab btns-print">
							<a href="javascript:showprint()" class="btn  btn-color-ab47bc  btn-margin"  style='margin-top:0px;'>打印</button>
							<a class='btn btn-default' href="farewellWordOrder.do?method=list"  target="homeTab"   role="button">返回</a>
							</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
//    function  print(){
// 	   window.print();  
//    }
function showprint(){ 
	$(".content-wrapper").css("padding-top","0px");   
	$("#hh_hidden").hide();
	$("#s_hiddden").hide();
	$("#t_show").show();
	$("#m_hiddden").hide();
	$("#f_hidden").hide();
	$("#t_show").css("top","80px");  
	
    window.print();  
    
    $("#hh_hidden").show();
    $("#s_hiddden").show(); 
    $("#m_hiddden").show();
    $("#f_hidden").show();
    $(".content-wrapper").css("padding-top","65px");  
}
</script>
