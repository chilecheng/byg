<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<script type="text/javascript">
function save(id){
	alert(1);
	var isOne=0;//判断","
	var ids="";//所有要添加的ID
	var idSum=0;
	$("[name="+id+"_id]").each(function(){
 		var idd = $(this).val();
 		if(idd!=""){
 			if(isOne!=0){
 				ids+=",";
 			}
 			ids+=idd;
 			idSum++;
 			isOne++;
 		}
 	});
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'addItem',ids:ids,type:'${type}'},
	    success: function (list) {
	    	for(var i=0;i<idSum;i++){
	    		var ob=list[i];
	    		sum++;
	    		var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	    		str+="<input type='checkbox' id=checkbox_"+sum+" name="+id+" value="+sum+"></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<select  class='list_table' onchange=itemName("+sum+",'${id}') name='itemId' id='itemId_"+sum+"'> "+ob[0]+"</select></td>";
	    		str+="<td style='text-align: center; vertical-align: middle; '>";
	    		str+="<input type='text' readonly='readonly' class='list_table' id='pice_"+sum+"' value="+ob[1]+"></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<select  class='list_table' onchange=itemType("+sum+",'${id}') name='typeId' id='typeId_"+sum+"'> "+ob[2]+"</select></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<input type='text' class='list_table' onchange=number("+sum+",'${id}') id='number_"+sum+"' value="+ob[3]+"></td>";
	    		str+="<td style='text-align: center; vertical-align: middle;'>";
	    		str+="<select  class='list_table' id=bill_"+sum+"> "+ob[4]+"</select></td>";
	    		str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' id=total_"+sum+" class='list_table' value="+ob[5]+"></td>";
	    		str+="<td><textarea type='text' class='list_table' id='comment_"+sum+"' value='' style='width: 100%'></textarea></td></tr>";
	    		if('${type}'=='${Type_Service}'){
	    			$("#service").append(str);
	    			sTotal+=ob[5]
	    		}
	    		if('${type}'=='${Type_Articles}'){
	    			$("#articles").append(str);
		    		aTotal+=ob[5]
	    		}
	    	}
	    	if('${type}'=='${Type_Service}'){
	    		$("#sFont").html(sTotal+"元");
	    	}
    		if('${type}'=='${Type_Articles}'){
    			$("#aFont").html(aTotal+"元");
	    	}
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )

	
	
}
</script>
<div class="modal-dialog" car="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="sTotal" value="" >
		<input type="hidden" name="aTotal" value="" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加服务项目</h4>
			</div>
			<div class="modal-body">
				<table style="width: 100%">
				<c:forEach items="${list }" var="u" varStatus="s">
				<c:if test="${s.index%2==0 }">
					<tr>
				</c:if>	
						<td align="right">${u[0]}：</td>
						<td>
							<select style="width: 169px"  class="list_table" name="${id }_id" > 
								${u[1]}
							</select>
						</td>
				<c:if test="${s.index%2!=0 }">
					</tr>
				</c:if>
				<c:if test="${s.index%2!=0 }">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</c:if>
				</c:forEach>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="save('${id}')" class="btn btn-primary" data-dismiss="modal">保存</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>