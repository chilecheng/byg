<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<%-- <%  
//String path = request.getContextPath();  
//获取当前页面的 使用协议   服务器名   端口号   应用名
//String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
//request.setAttribute("basePath", basePath);  
%> --%>

<head>
<script type="text/javascript">
var preConfigList = new Array(); var previewJson = new Array(); 
var sum=0;//标识
timeMunite();
$(function(){
	timeRange();
	timeMunite();
	//火化炉
	$("#furnaceType_div").hide();
 	if('${id}'==""){
			$("#nation").val("汉族");
		} 
	if('${id}'==""){
		//初始证件类型
		var pStr="<label class='lab-6'>证件类型：</label>";
		<c:forEach var="u" items='${certificateList}'>
			pStr+="<input type='checkbox'  name='proveIds' value='${u.certificateId }'>";
			pStr+="${u.name }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#prove").html(pStr);
		</c:forEach>
		//车辆调度
		<c:forEach var="u" items='${car_List}'>
			sum++;
			car(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}");
		</c:forEach>
		
		
	}else{
		//车辆调度
		<c:forEach var="u" items='${car_List}'>
			sum++;
			car(sum,"${u[0]}","${u[1]}","${u[2]}","${u[3]}","${u[4]}");
		</c:forEach>
		
		//证件类型
		var pStr="<label class='lab-6'>证件类型：</label>";
		<c:forEach var="u" items='${prove_list}'>
			pStr+="<input type='checkbox' "+'${u[1]}'+"  name='${u[0]}' value='${u[2] }'>";
			pStr+="${u[3] }&nbsp;&nbsp;&nbsp;&nbsp;";
			$("#prove").html(pStr);
		</c:forEach>
		
	}
});


//车辆调度方法
function car(sum,id,transportTypeOption,carTypeOption,pickTime,comment){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name='csr_id' value="+id+" >";
	str+="<input type='checkbox' name='carSchedulRecordId' ></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select class='list_table' name=transportTypeId  id=transportTypeId_"+sum+"> "+transportTypeOption+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name=carTypeId id=typeId_"+sum+" >"+carTypeOption+"</select></td>";
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input data-id='reservation' readOnly='readOnly'  style='width: 180px' data-date-format='yyyy-mm-dd hh:ii:ss' class='required list_table' id=dTime_"+sum+" name=pickTime ><i style='margin-left: -20px;' class='fa fa-calendar'></i></td>";
	str+="<td><textarea type='text' class='list_table' name=carComment id=comment_"+sum+"  >"+comment+"</textarea></td></tr>";
	$("#car").append(str);
	$("#dTime_"+sum).val(pickTime);
	timeMunite();
}



//死亡类型改变
function deadTypeChange(){
	$.ajax({
	    url: "commissionOrder.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'deadTypeChange',id:$("#deadTypeId").val()},
	    success: function (json) {
	    	$("#deadReasonId").html(json.str);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
} 

//添加车辆信息调度
function addCar() {
	sum++;
	car(sum,"","${transportTypeOption}","${carTypeOption}","","")
}
	

//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   { //author: meizz   
	var o = {   
	 "M+" : this.getMonth()+1,                 //月份   
	 "d+" : this.getDate(),                    //日   
	 "h+" : this.getHours(),                   //小时   
	 "m+" : this.getMinutes(),                 //分   
	 "s+" : this.getSeconds(),                 //秒   
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	 "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
	 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
	 if(new RegExp("("+ k +")").test(fmt))   
	fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
}  
//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		if(str=="articles"){
			aTotal=aTotal-$("#total_"+id).val();
			$("#aFont").html(aTotal+"元");
		}
		if(str=="service"){
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		}
		if(str=="base"){
			baseTotal=baseTotal-$("#total_"+id).val();
			$("#baseFont").html(baseTotal+"元");
		}
		if(str=="hard"){
			hardTotal=hardTotal-$("#hard_total_"+id).val();
			$("#hardFont").html(hardTotal+"元");
		}
		$(this).parent().parent().remove();
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}

}	
/* 保存时候的方法 */ 
function validateSub(form, callback) {
	alert(1);
	$("button").prop("disabled", "disabled");
	var $form = $(form);
	/* var va = validHomeForm($form); */
	$(".error-label", form).remove();
	$("[errorFlag]", form).removeAttr("errorFlag");
	$(".error-input", form).removeClass("error-input");
	
/* 	var errArr=[];
	$(".required", form)
	.each(
			function(i,ele) {
				var value = $(this).val();
				if (value == "") {
					var flag = $(this).attr("errorFlag");
					if (flag == undefined) {
						$(this).attr("errorFlag", "true");
						$(this).addClass("error-input");
						var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
						$(this).parent().append(err);
						errArr.push(i)
					}
				}
			});
	if(errArr){
		var errNum=Math.min.apply(Math,errArr);
		var ele=$(".required", form).eq(errNum);
		var errName=$(ele).attr('name');
		var errMsg=$(ele).prev().text().slice(0,-1);
		if(errName=='corpseUnitId'){$('[href="#b"]').click()}
		var height=$('[name="'+errName+'"]').offset().top;
		window.scrollTo(0,height);
		toastr["warning"]("请填写"+errMsg);
		$("button").removeAttr("disabled");
		return false;
	} */
	
	rel = $form.attr("rel");

	$.ajax({
		type : 'POST',
		url : $form.attr("action"),
		data : $form.serializeArray(),
		traditional: true,
		dataType : "json",
		cache : false,
		success : callback,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			toastr["error"](XMLHttpRequest.status);
			$("button").removeAttr("disabled");
		}
	});
	
	return false;
}
</script>
 <script type="text/javascript" src='js/carId.js'></script>
<style>
.container-fluid { 
	background: #fff;
} 
</style>

</head>
<body>
	<section class="content">
		<form action="${url}" id="homeForm" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${commissionOrder.id}">
		<input type="hidden" id="name" value="${commissionOrder.name }">
		<input  type="hidden"  id = "sourcepage"  name="sourcepage"  value="phoneBook">
			<div class="box-body">
				<div class="nav-tabs-custom">

					<div class="tab-content" style='padding-left:0px;padding-right:0px;'>
			
			<!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						
						
						<P class='p border-B'>死者信息</P>
							<div class="box-body broder-B">
									<div style='opacity:0;'>
									<object id="plugin0" type="application/lagen-plugin" width="0" height="0">
									<param name="onload" value="pluginLoaded" />
									</object>
									<object id="plugin1" type="application/x-lathumbplugin" width="0" height="0">
 										    <param name="onload" value="pluginLoaded" />
 									</object> 
									<br />
									</div>
									<div class='hide'>
										当前设备：<select id="curDev" style="width: 90px" name="selDev"
																			onchange="changedev()"></select>
										当前分辨率：<select id="curRes" style="width: 90px" name="curRes"
																			onchange="changeres()"></select>
										颜色：<select id="curColor" style="width: 90px" name="curRes"
																			onchange="changeclr()"></select>
										拍照模式：<select id="capMode" style="width: 90px" name="curRes"
																			onchange="changemode()"></select>
									
										<input id="rotatecrop" checked type="checkbox" value="" onclick="RotateCrop(this)" />纠偏裁边
										<input id="drawrect" type="checkbox" value="" onclick="setmousemode(this)" />框选
										<br><br>    
									<input   TYPE="button"   VALUE="开始预览"   onClick="start_preview()"> 
									<input   TYPE="button"   VALUE="停止预览"   onClick="stop_preview()">
									<input   TYPE="button"   VALUE="左转90度"   onClick="rotleft()">
									<input   TYPE="button"   VALUE="右转90度"   onClick="rotright()">
									<input   TYPE="button"   VALUE="视频属性"   onClick="showprop()">
									<input   TYPE="button"   VALUE="条码识别"   onClick="readbarcode()">
									<input   TYPE="button"   VALUE="画面恢复"   onClick="resetvideo()">
									<input   TYPE="button"   VALUE="生成PDF"   onClick="makepdf()">
									<input   TYPE="button"   id=recvideo VALUE="开始录像"   onClick="startrecord()">
									<input   TYPE="button"   VALUE="拍照"   onClick="capture()">
									<input   TYPE="button"   VALUE="拍照为Base64"   onClick="capturebase64()"> <br><br>
									<input   TYPE="button"   id=autocap VALUE="开始智能连拍"   onClick="startautocap()">
									<input   TYPE="button"   id=tmcap VALUE="开始定时连拍"   onClick="starttmcap()">
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死者姓名：</label>
									<input type="text" style='width:20%'  class="required list_select" name="name" value="${commissionOrder.name }">&nbsp;<i class="fa fa-circle text-red"></i>
									<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardD('name')">
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死者地址：</label> 
									<input type="text" class="required list_select" name="dAddr" value="${commissionOrder.dAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5">
									<label class='lab-4'>证件类型：</label>
									<select style="width: 169px" class="list_select" name="certificateId" id="certificateId">
										${certificateOption}
									</select>
								</div>
									<div class="col-md-5">
									<label class='lab-4'>接尸地址：</label> 
<%-- 									<input type="text" class="required list_select" name="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;<i class="fa fa-circle text-red"></i> --%>
									<input list="pickAddr" autocomplete='off' class="required  list_select" name="pickAddr" value="${commissionOrder.pickAddr }">&nbsp;<i class="fa fa-circle text-red"></i>
									<datalist  class="list_select"  id="pickAddr">
										${corpseAddOption}
									</datalist>
								</div>
								<div class="col-md-5">
									<label class='lab-4'>证件号码：</label> 
									<input type="text" class="required list_select" name="certificateCode" value="${commissionOrder.certificateCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死亡类型：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="deadTypeId" id="deadTypeId" onchange="deadTypeChange()">
										${deadTypeOption}
									</select>
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死者年龄：</label> 
									<input type="text" class="number list_select" name="age" value="${commissionOrder.age==0 ? '' : commissionOrder.age }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死亡原因：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="deadReasonId" id="deadReasonId">
										${deadReasonOption}
									</select>
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死者性别：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="sex" id="sex">
										${sexOption}
									</select>
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死亡日期：</label> 
									<input  data-id="reservation" readOnly='readOnly'  data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="dTime" name="dTime" value='<fmt:formatDate value="${commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm:ss"/>'><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-5">
									<label>死者地区：</label> 
									<select class="list_select nopadding-R" name="toponymId" id="toponymId">
										${toponymOption}
									</select> &nbsp;
								</div>
								<div class="col-md-5">
									<label class='lab-2'>民族：</label> 
									<input list="dNation" autocomplete='off' class="list_select required" id="nation" name="dNation" value="${ commissionOrder.dNationId }">
									<datalist  class="list_select"  id="dNation">
												${nationOption}
									</datalist>		
								</div>
								<div class="col-md-5">
									<label class='lab-2'>卡号：</label> 
									<input type="text" class="required list_select" name="cardCode" value="${commissionOrder.cardCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-7">
									<label class='lab-4'>证明单位：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="proveUnitId" id="proveUnitId">
										${proveUnitOption}
									</select> 
									<input style="width: 200px" type="text" name="proveUnitContent" class="list_select" value="${commissionOrder.proveUnitContent }">
								</div>
								<div class="col-md-5">
									<label class='lab-4'>死亡证明：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="dFlag" id="dFlag">
										${dFlagOption}
									</select>
								</div>
								
							</div>
							<p class='p border-B'>家属/经办人信息</p>
							<div class="box-body">
								<div class="col-md-6">
									<label  class='label-4'>姓名：</label> 
									<input type="text" class="required list_select" name="fName" value="${commissionOrder.fName }">&nbsp;<i class="fa fa-circle text-red"></i>
									<input   TYPE="button" style='height:28px;line-height:1px;margin-top:-2px;'  VALUE="读取身份证"  class='btn btn-default'  onClick="readidcardDF('fName')">
								</div>
								<div class="col-md-5">
									<label  class='lab-1'>与死者关系：</label> 
									<select style="width: 169px" class="list_select nopadding-R" name="fAppellationId" id="fAppellationId"> 
									    ${fAppellationOption}
									</select>
								</div>
								<div class="col-md-6">
									<label  class='lab-2'>联系号码：</label> 
									<input type="text" class="required list_select" name="fPhone" value="${commissionOrder.fPhone }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5">
									<label  class='label-4'>住址：</label>
									<input type="text" class="list_select" name="fAddr" value="${commissionOrder.fAddr }">
								</div>
								<div class="col-md-6">
									<label  class='label-4'>单位：</label> 
									<input type="text" class="list_select" name="fUnit" value="${commissionOrder.fUnit }">
								</div>
								<div class="col-md-5">
									<label  class='lab-1'>身份证号码：</label> 
									<input type="text" class="required list_select" name="fCardCode" value="${commissionOrder.fCardCode }">&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-6">
									<label class='lab-6'>业务审核状态：</label>
			    					<c:choose>
			    						<c:when test="${checkFlag==Check_Yes }">
			    							<font color="green">${commissionOrder.checkFlagName}</font>
			    						</c:when>
			    						<c:otherwise>
			    							<font color="red">未审核</font>
			    						</c:otherwise>
			    					</c:choose>
								</div>
								
							</div>
							<p class='border-B p'>车辆调度信息</p>
							<div class="box-body border-B">
								<div class="col-md-12">
									<small class="pull-right btns-buy"> 
										<button type="button" onclick="addCar()" class="btn btn-warning" >添加</button>
										<button type="button" onclick="delHtml('carSchedulRecordId')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<table class="table table-bordered" style="margin-top: 60px;">
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="carSchedulRecordId" /></th>
											<th width="200px">运输类型</th>
											<th width="200px">车辆类型</th>
											<th width="200px">运送时间</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody id="car">
										
									</tbody>
								</table>
							</div>
						</div>
				<!-- 业务信息 -->
						
				</div>
			</div>
					<div class="box-body">
					<div  class="container-fluid">
						<div class='row padding-B'>
							<div class='col-md-6'>
								<div class='row'>
									<div class="col-md-6">
										<label>预约登记人：</label>
										<c:choose>
										    <c:when test="${commissionOrder.agentUser.name!=null}">
										    	${commissionOrder.agentUser.name}
										    </c:when>
										    <c:otherwise>
										   		${agentUser}
											</c:otherwise>
										</c:choose> 
										
									</div>
									<div class="col-md-6">
										<label class='lab-3'>编号：</label> 
										<input type="text" class="list_select" readonly="readonly"  value="${commissionOrder.code == null ? '由系统自动生成' : commissionOrder.code}">
									</div>
									<div class="col-md-6">
										<label class='lab-1'>办理时间：</label> 
										${time }
									</div>
								</div>
							</div>
							<div class='col-md-6'>
								<div class='row'>
									<div class="col-md-12">
										<small class="pull-right btns-hometab">
										<c:if test="${checkFlag!=Check_Yse }">
											<button type="button" onclick="checkSelect()" class="btn btn-info" style='margin-top:30px;'>保存</button>
					   					</c:if>
					 
					<%-- 						<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="certificateId" warm="确认禁用吗" class="btn btn-danger " role="button">重置</a> --%>
										<a href="${url}?method=list" target="homeTab" style='margin-top:30px;'  class="btn btn-default" role="button">返回</a>
										</small>
									</div>
								</div>
							</div>
							
							
						</div>
					</div>
					</div>
				</div>
		</form>
		
	</section>
</body>
<script type="text/javascript">
function checkSelect() {

	$("#homeForm").submit();
	
}
</script>
