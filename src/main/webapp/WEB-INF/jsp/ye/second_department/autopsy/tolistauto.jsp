<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -解冻 &  验尸 查看页面 - -->
	<form id="todetail" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<input name="method" value="${method }" type="hidden">
			<input name="fid" value="${fid }" type="hidden"> 
			<input name="agentUserId" value="${u.userId }" type="hidden">
			<div class="modal-content">
				<div class="modal-body nopadding-B no-padding-T">
					<div class="row">
						<p class="col-md-12 p  border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者姓名：</label>
									<span>${listapr.commissionOrder.name }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>冷藏柜号：</label>
									<span>${listapr.freezer.code }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者编号：</label>
									<span>${listapr.commissionOrder.code }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>验尸单位：</label>
									<span>${listapr.proveUnit.getName() }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者年龄：</label>
									<span>${listapr.commissionOrder.age }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>验尸时间：</label>
									<span><fmt:formatDate value="${listapr.yTime }" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者性别：</label>
									<span>${listapr.commissionOrder.getSexName() }</span>
								</div>							
								<div class="col-md-6 height-align" >
									<label class='lab-4'>登记时间：</label>
									<span><fmt:formatDate value="${listapr.registerTime }" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-20">
									<label class='lab-2'>备注：</label>
									<textarea row='18' col='150' readonly="readonly"  class='list_input' style="border:none"   id="comment" name="comment" value="">${listapr.comment }</textarea>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								
								<div class="col-md-6 height-align">
									<label class='lab-4'>流水单号：</label>
									<span>${listapr.serialNumber }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>登记人：</label>
									<span>${listapr.user.name}</span>
								</div>
							</div>
						</div>
						
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>