<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- Y验尸 页面- -->
	<form id="pageForm" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<input name="method" value="${method }" type="hidden">
			<input name="fid" value="${fid }" type="hidden"> 
			<input name="agentUserId" value="${u.userId }" type="hidden">
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者姓名：</label>
									<span>${listapr.commissionOrder.name }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>冷藏柜号：</label>
									<span>${listapr.freezer.code }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者编号：</label>
									<span>${listapr.commissionOrder.code }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>验尸单位：</label>
									<span>${listapr.proveUnit.getName() }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者年龄：</label>
									<span>${listapr.commissionOrder.age }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>验尸时间：</label>
									<span>${listapr.yTime }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者性别：</label>
									<span>${listapr.commissionOrder.getSexName() }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>协助人员：</label>
									<select class="list_input content-input" id="checkpsy" name="checkpsy">
										${help_user }
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>登记人：</label>
									<span>${listapr.user.name}</span>
								</div>
								<div class="col-md-6 height-align">
									<labe class='lab-2'>备注：</label>
									<textarea row='6' col='150' class='list_input textarea-dialog' style='min-width:50%;width:50%;' placeholder='多行输入' id="comment" name="comment" value="">${listapr.comment }</textarea>
								</div>
								<div class="col-md-6 height-align" style='margin-top:-20px;'>
									<label class='lab-4'>登记时间：</label>
									<span>${listapr.registerTime }</span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								
								<div class="col-md-6 height-align">
									<label class='lab-4'>流水单号：</label>
									<span>${listapr.serialNumber }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>经办人：</label>
									<span>${u.name }</span>
								</div>
							</div>
						</div>
						
						<div class="col-md-12 btns-dialog">
							<button type="submit" class="btn btn-info" >保存</button>
							<button type='reset' class="btn btn-color-9E8273">重置</button>
							
							<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>