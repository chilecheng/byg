
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<!-- 验尸页面 -->
<script type="text/javascript">
 function changeDate(){	
	$("#pageForm3").submit();
}; 

</script>
<form action="autopsyRecord.do" id="pageForm3" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
		<div class="row">
			<div class="col-md-12">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
							
							<div class="col-md-12 border-bot">
								<label>时间：</label> 
								<input type="text" data-id="beginDate" data-a='dismiss' data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="selectTime3" name="selectTime3" value="${selectTime3 }" onchange="changeDate()"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<!-- ${date} -->
							</div>
						
							<div class="row"  id='main-content'>
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-12">
											<small class='btns-print'>
												<a href="autopsyRecord.do?method=detail&id=" data-check="ifCheckBody" checkOne="true" checkname="id3" rel="myModal" class="btn btn-danger" target='dialog'>验尸</a>
											</small>
											<small class='btns-print pull-right'>
<!-- 												<a href="javascript:myprint()" class="btn btn-color-ab47bc" role="button">打印</a> -->
<%-- 												<a href="${url}?method=exportExcel" class="btn btn-success" role="button">导出EXCEL</a> --%>
<%-- 												<a href="${url}?method=exportWord" class="btn btn-primary" role="button">导出WORD</a> --%>
											</small>
										</div>
									</div>
									<table class="table table-bordered margin-T">
										<thead>
											<tr>
												<th width='80px'>
													
												</th>
												<th>死者姓名</th>
												<th>死者性别</th>
												<th>年龄</th>
												<th>验尸单位</th>
												<th>协助人员</th>
											</tr>
										</thead>
										<tbody>
											<c:choose> 
 												<c:when test="${fn:length(page3.list)==0 }"> 
 													<tr> 
 													    <td colspan="6"> 
 															<font color="Red">还没有数据</font> 
 														</td> 
 													 </tr> 
 												</c:when> 
 												<c:otherwise> 							
													<c:forEach items="${page3.list}" var="f">
														<tr role="row">
															<td><input type='radio' class='checkBoxCtrl' name='id3' value='${f.id}'/></td>
															<td><a href="autopsyRecord.do?method=todetail&id=${f.id}" class="loser" target="dialog" rel="myModal">${f.commissionOrder.name}</a></td>
															<td>${f.commissionOrder.getSexName()}</td>
															<td>${f.commissionOrder.age}</td>
															<td>${f.proveUnit.getName() }</td>
															<td>${f.helpUserNames }</td>
														</tr>
													</c:forEach>
													
 												</c:otherwise> 
 											</c:choose> 
										</tbody>
									</table>
									<%@ include file="/common/pageFood3.jsp"%>
								</div>
							</div>
				<!-- /.col -->
			</div>		
		</div>	
</form>