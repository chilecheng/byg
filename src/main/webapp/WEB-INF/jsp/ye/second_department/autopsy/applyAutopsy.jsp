<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<script type="text/javascript">
timeRange();
</script>
<form action="autopsyRecord.do" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
	<section>
		<div class="row">
			<div class="col-md-12">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input" id="select_kind" name="select_kind">
<!-- 									<option value='k1'>死者卡号</option> -->
<!-- 									<option value='k2'>死者姓名</option> -->
<!-- 									<option value='k3'>死者编号</option> -->
<!-- 									<option value='k4'>冷藏柜号</option> -->
									${searchOption }
								</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" id='search_val' name='search_val' value='${search_val }' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>到馆时间：</label> 
								<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="${startTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<label class='timerange'>--</label> 
								<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="${endTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section id='main-content'>
		<div class='box nomargin-B'>
		
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 ">
							<small class='btns-buy'>
							<a href="autoFreezer.do?method=add&id=" checkOne="true" checkname="id1" rel="myModal" class="btn btn-warning" target='dialog'>验尸申请</a>
							<a href="autopsyRecord.do?method=delete&id1=" target="ajaxTodo" checkName="id1" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
							</small>
						</div>
					</div>
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th>
									 
								</th>
								<th>死者编号</th>
								<th>死者姓名</th>
								<th>死者性别</th>
								<th>年龄</th>
								<th>冰柜号</th>
								<th>死亡原因</th>
								<th>到馆时间</th>
								<th>验尸时间</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${fn:length(apage.list)==0 }">
									<tr>
									    <td colspan="9">
											<font color="Red">还没有数据</font>
										</td>
									 </tr>
								</c:when>
								 <c:otherwise>
									<c:forEach items="${apage.list}" var="u">
									<tr role="row">
										<td><input type='radio' class='checkBoxCtrl' name='id1' value='${u.order.id}'/></td>
										<td>${u.order.code}</td>
										<td><a href="autopsyRecord.do?method=todetail12&id=${u.order.id}" class="loser" target="dialog" rel="myModal">${u.order.name}</a></td>
										<td>${u.order.getSexName()}</td>
										<td>${u.order.age}</td>
										<td>${u.freezer.code}</td>
										<td>${u.deadReason.name}</td>
										<td><fmt:formatDate value="${u.order.arriveTime}" pattern="yyyy-MM-dd HH:mm"/></td>
										<td><fmt:formatDate value="${u.autopsyRecord.yTime}" pattern="yyyy-MM-dd HH:mm"/></td>
									</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<%@ include file="/common/pageFood1.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>
