<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -验尸 申请页面  点 名字链接详细- -->
	<form id="todetail" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<input name="method" value="${method }" type="hidden">
			<input name="fid" value="${fid }" type="hidden"> 
			<input name="agentUserId" value="${u.userId }" type="hidden">
			<div class="modal-content">
				<div class="modal-body nopadding-B no-padding-T">
					<div class="row">
						<p class="col-md-12 p  border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6">
									<label class='lab-4'>死者姓名：</label>
									<span>${name }</span>
								</div>
								<div class="col-md-6">
									<label class='lab-4'>冷藏柜号：</label>
									<span>${freezerName }</span>
								</div>
								<div class="col-md-6">
									<label class='lab-4'>死者编号：</label>
									<span>${code }</span>
								</div>
								<div class="col-md-6">
									<label class='lab-4'>验尸单位：</label>
									<span>${proveName}</span>
								</div>
								<div class="col-md-6">
									<label class='lab-4'>死者年龄：</label>
									<span>${age }</span>
								</div>
								<div class="col-md-6">
									<label class='lab-4'>验尸时间：</label>
									<span>${yTime }</span>
								</div>
								<div class="col-md-6">
									<label class='lab-4'>死者性别：</label>
									<c:if  test="${sex==2}">
										<span> 女
										</span>	
									</c:if>
									
									<c:if  test="${sex==1}">
										<span> 男
										</span>	
									</c:if>
									</span>
								</div>							
								<div class="col-md-6" >
									<label class='lab-4'>登记时间：</label>
									<span>${registerTime }</span>
								</div>
								<div class="col-md-12">
									<label class='lab-4'>备注：</label>
									<textarea row='8' col='150' readOnly="readOnly" class='list_input' readonly="readonly"  style="border:none ; width:350px"   id="comment" name="comment" value="">${comment } </textarea> 
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								
								<div class="col-md-6">
									<label class='lab-4'>流水单号：</label>
									<span>${serialNumber }</span>
								</div>
								<div class="col-md-6">
									<label class='lab-1'>登记人：</label>
									<span>${registerUser}</span>
								</div>
							</div>
						</div>
						
						<div class="col-md-12 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>