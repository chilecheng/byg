
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<script type="text/javascript">
function changeDate2(){
	$("#pageForm2").submit();
}
</script>
<form action="autopsyRecord.do" id="pageForm2" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="list" type="hidden">
		<div class="row">
			<div class="col-md-12">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
							
							<div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="selectTime2" name="selectTime2" value="${selectTime2}"  onchange="changeDate2()"><i style="margin-left: -20px;" class="fa fa-calendar"></i>							
								
							</div>
							
							<div class="row"  id='main-content'>
								<div class="col-md-12">
									<div class="row">
										<p class="col-md-2"></p>
										<div class="col-md-10">
											<small class='btns-print pull-right'>
<!-- 											<a href="" class="btn btn-color-ab47bc" role="button">打印</a> -->
<!-- 											<a href="" class="btn btn-success" role="button">导出EXCEL</a> -->
<!-- 											<a href="" class="btn btn-primary" role="button">导出WORD</a> -->
											
											
											</small>
										</div>
									</div>
									<table class="table table-bordered margin-T">
										<thead>
											<tr>
												<th width='60px'>
													
												</th>
												<th>解冻时间</th>
												<th>死者姓名</th>
												<th>死者性别</th>
												<th>年龄</th>
												<th>冰柜号</th>
											</tr>
										</thead>
										<tbody>
											<c:choose> 
 												<c:when test="${fn:length(page2.list)==0 }"> 
 													 <tr> 
 													    <td colspan="6"> 
 															<font color="Red">暂时没有需要解冻的记录</font> 
 														</td> 
 													 </tr> 
 												</c:when> 
 												<c:otherwise> 
														<c:forEach items="${page2.list}" var="b">
														<tr role="row">
															<td><input type='checkbox' class='checkBoxCtrl' name='id2' value='${b.id}'/></td>
													
															<td><fmt:formatDate value="${b.autoFreezerTime }" pattern="yyyy-MM-dd"/></td>
															<td><a href="autopsyRecord.do?method=todetail&id=${b.id}" class="loser" target="dialog" rel="myModal">${b.commissionOrder.name}</a></td>
															<td>${b.commissionOrder.getSexName()}</td>
															<td>${b.commissionOrder.age}</td>
															<td>${b.freezerName }</td>
														</tr>
													</c:forEach>
 												</c:otherwise> 
 											</c:choose> 
										</tbody>
									</table>
								  <%@ include file="/common/pageFood2.jsp"%>
								</div>
							</div>
				<!-- /.col -->
			</div>	
		</div>
</form>
<script>
$(function(){
	$('#autoFreezerNumber').text('');
});
</script>