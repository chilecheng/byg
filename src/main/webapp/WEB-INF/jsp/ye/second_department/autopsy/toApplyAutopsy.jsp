<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<body>
<%-- 	<form id="pageForm" action="${url }" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);"> --%>
<form action="${url }" id="pageForm"  rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<input name="method" value="${method}" type="hidden">
			<input name="id" value="${id}" type="hidden">
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-B">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者姓名：</label>
									<span>${name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>冷藏柜号：</label>
									<span>${freezerName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者编号：</label>
									<span>${code }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>验尸单位：</label>
									<select class="list_input content-input" id="checkpsy" name="checkpsy">
										${pUption }
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者性别：</label>
									<span >${sex }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>验尸时间：</label>
									<input type="text" data-id="reservation"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="yTime" name="yTime" value="${yTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>死者年龄：</label>
									<span>${age }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>备注：</label>
									<textarea row='6' col='150' class='list_input textarea-dialog' style='width:50%;width:50%' placeholder='多行输入' id="comment" name="comment">${comment}</textarea>


								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-4'>登记人员：</label>
									<span >${u.name }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>登记时间：</label>
									<span><fmt:formatDate value="${registerTime}" pattern="yyyy-MM-dd HH:mm:ss"/></span>
									<input name="registerTime" value="${registerTime}" type="hidden">
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>流水单号：</label>
									<input type="text" readonly="readonly" class="list_select" id='fNumber' name='fNumber' value='' placeholder='由系统自动生成'/>
								</div>
							</div>
						</div>
						
						<div class="col-md-12 btns-dialog">
							<button type="submit" class="btn btn-info btn-margin" >保存</button>
							<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
							
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>
<script>
timeMunite();
</script>