<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">法政验尸</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	.main-content{
		border:1px solid #ccc;
		border-radius:6px;
		background-color:#fff;
	}
	.nav.nav-tabs>li{
		width:20%;
		text-align:center;
	}
	.nav.nav-tabs>li.active{
		border-top:3px solid #29B6F6;
		border-radius:4px; 
	}
	
	#main-content>div.col-md-12{
		padding:10px 25px;
	}
	.hint {
	width: 26px;
	height: 26px;
	border-radius: 26px;
	background-color: #ffa726;
	color: #fff;
	position: absolute;
	right: 7px;
	top: 7px;
	text-align: center;
	line-height: 24px;
	font-size: 14px;
	border: 1px solid #eb9b25;
	font-style: normal;
}
</style>
<script >
$(function(){
	$('#autoFreezerNumber').text('');
})

$(".unfreeze2").click(function(){
	$('#autoFreezerNumber').text('');
/* 	对session 值进行刷新 */
	$.ajax({
		type : 'POST',
		url : "autopsyRecord.do?method=refreshNumber",
		dataType : "json", 
	
	 	success : function(json) {
			$('#autoFreezerNumber2').html('');
		 	$('#autoFreezerNumber2').attr("class","ss"); 	
	 		initTab();	 		
 		}, 
	 
	});
});
	/*刷新页面 并保留到 当前页面   刷新  去 消息提示  */
	
	
</script>
	<section class='content'>
		<div class='row'>
			<div class='col-md-12'>
				<div class='main-content'>
				
					<input type="hidden" id="clickId" name="clickId" value="qicao">
					
					<ul class='nav nav-tabs' id='liClick'>
						<li role="presentation" id="shenqing"  class="active"><a href="#apply" aria-controls="apply" role="tab" data-toggle="tab">验尸申请</a></li>
						<li role="presentation" id="jiedong" ><a href="#unfreeze" class="unfreeze2" aria-controls="unfreeze" role="tab" data-toggle="tab" style='position:relative'>验尸解冻
						<i id='autoFreezerNumber2'    class='${sessionScope.autoFreezerNumber==0?"diplay": "hint"}'>${sessionScope.autoFreezerNumber==0?"":sessionScope.autoFreezerNumber}</i></a></span>
						</li>
						<li role="presentation" id="yanshi" ><a href="#fazheng" aria-controls="fazheng" role="tab" data-toggle="tab">法政验尸</a></li>
					</ul>
					<div class='tab-content'>
						<div role="tabpanel" class="tab-pane active" id="apply">
							<%@ include file='applyAutopsy.jsp'%>
						</div>
			    		<div role="tabpanel" class="tab-pane " id="unfreeze" >
			    			<%@ include file='unfreezerAutopsy.jsp'%>
			    		</div>
			    		<div role="tabpanel" class="tab-pane " id="fazheng">
			    			<%@ include file='fazhengAutopsy.jsp'%>
			    		</div>
					</div>
				</div>
			</div>
		
		</div>
		
	</section>
	<script type="text/javascript">
		//当前页状态改变，用于后台判断，页面刷新之后跳转到当前页
		var type=active.parsent;
		$("#clickId").val(type);
		$('[aria-controls="'+type+'"]').click();
		$("#liClick").on('click','li a',function(){
			type=$(this).attr('aria-controls');
			active.parsent=type;
			$("#clickId").val(type);
		// 	alert($("#clickId").val());
		});
		

</script>


