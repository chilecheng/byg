<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<script type="text/javascript">
$(function() {
	timeRange();
	$('#freezerThawNumber').text('');
});
</script>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">遗体解冻时间安排</div>
</section>

<%-- <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
			<div class="box-body">
						<iframe id="reportFrame" class='frame' width="100%"  src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=freezerThawTime.cpt&op=view"></iframe>
							

						</div>
					</div>
				</div>
			</div>
		</section> --%>
		
<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="${method }" type="hidden">
	
	<section class="content">

		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<div class="box-body">
						<iframe id="reportFrame" class='frame' width="100%"  src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=freezerThawTime.cpt&op=view"></iframe>
							

						</div>
				</div>
			</div>
		</div>
	</section>
</form>
		
				<%-- <div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
					<iframe id="reportFrame" width="100%" height="1400" src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=freezerThawTime.cpt&op=view"></iframe>
						<div class="col-md-12">
							<label>日期：</label> 
							<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  name="beginDate" value="<fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<label class='timerange'>--</label> 
							<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  name="endDate" value="<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<button type='submit' class='btn btn-info btn-search btn-search-left'>搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					${beginDate }——<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />
					<small class="pull-right btns-print">
<!-- 					<a href="javascript:myprint()" class="btn btn-color-ab47bc" role="button">打印</a> -->
<<<<<<< HEAD
					<a href="${url}?method=exportExcel" class="btn btn-success" role="button">导出EXCEL</a>
					<a href="${url}?method=exportWord" class="btn btn-primary" role="button">导出WORD</a>
					<a href="${url}?method=checkFlag&checkFlag=${Check_Yes }&id=" target="ajaxTodo" checkName="checkboxId"   warm="确认审核吗" class="btn btn-color-ab47bc" id="d" role="button">打印</a>
=======
<%-- 					<a href="${url}?method=exportExcel" class="btn btn-success" role="button">导出EXCEL</a> --%>
<%-- 					<a href="${url}?method=exportWord" class="btn btn-primary" role="button">导出WORD</a> --%>
					<%-- <a href="${url}?method=checkFlag&checkFlag=${Check_Yes }&id=" target="ajaxTodo" checkName="checkboxId"   warm="确认审核吗" class="btn btn-color-ab47bc" id="d" role="button">打印</a>
>>>>>>> branch 'develop' of http://114.55.52.217/root/FuneralM.git
					<a href="${url}?method=checkFlag&checkFlag=${Check_No }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认取消吗" class="btn btn-success " role="button">导出Excel</a>
					<a href="${url}?method=checkFlag&checkFlag=${Check_No }&id=" target="ajaxTodo" checkName="checkboxId"  warm="确认取消吗" class="btn btn-primary " role="button">导出Word</a>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">	
												<th>时间</th>
												<th>死者姓名</th>
												<th>年龄</th>
												<th>死者性别</th>
												<th>告别厅</th>
												<th>灵堂</th>
												<th>冰柜</th>
												<th>穿衣化妆</th>
												<th>纸馆</th>
												<th>整容</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="20">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list}" var="u">
											    <c:set value="0" var="flag"></c:set>
												<tr role="row">
													<c:forEach items="${u}" var="k">
													<c:choose>
													<c:when test="${flag==0}">
														<td><fmt:formatDate value="${k}" pattern="yyyy-MM-dd"/></td>
													</c:when>
													<c:otherwise>
														<td>${k}</td>
													</c:otherwise>
													</c:choose>
													<c:set value="${flag+1}" var="flag"></c:set>
													</c:forEach>
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div> --%>

