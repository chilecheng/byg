<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">车辆信息安排表</div>
</section>
<script>
$(function(){
	$('#carNumber').text('');
	timeRange();
	changeDates();
});
function changeDates(){
	var beginTime=$('[data-id="beginDate"]').val();
	var endTime=$('[data-id="endDate"]').val();
	$('[data-date="change"]').html(beginTime+'---'+endTime)
}
</script>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
</style>

<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="${method }" type="hidden">
	
	<section class="content">
	<!-- -车辆信息表- -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<div class="box-body"><!-- WebReport/ReportServer?reportlet=carList2.cpt&op=view -->
						<iframe id="reportFrame" class='frame' width="100%"  src="${sessionScope.reportPath }/frameset?__report=carList2.rptdesign&__parameterpage=false"></iframe>
							

						</div>
				</div>
			</div>
		</div>
	</section>
</form>
