<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<body>
	<form id="detail" action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">		
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-T nopadding-B">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-12">
									<label class='lab-1'>冰柜号：</label> 
									<select class="list_input input-dialog" id="repareFreezer" name="repareFreezer" style='min-width:30%;'>
										<option>  ${freezerOption} </option>
									</select>
								</div>
								<div class="col-md-12">
									<label class='lab-4'>维修时间：</label> 
									<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span style="margin:8px;">至</span> 
									<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									
								</div>
								<div class="col-md-12">
									<label class='lab-4'>维修费用：</label> 
									<input type="text" class="list_input input-dialog" id='fix' name='fix' value='' style='min-width:30%;'/>
								</div>
								
								<div class="col-md-12">
									<label class='lab-4'>维修原因：</label> 
									<textarea id='reason' class='textarea-dialog' name='reason'></textarea>
								</div>
							</div>
							
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-4'>报修人员：</label>
									<span>${agentUser}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-4'>保修时间：</label>
									<span>${creatTime} }</span>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit" class="btn btn-info btn-margin" >保存</button>
							<button type='reset' class="btn btn-margin btn-color-9E8273">重置</button>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>