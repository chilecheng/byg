<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style type="text/css">
#dayin {
	background: #7e57c2;
}
/*****进入列表按钮****/
#liebiao {
	background: #29b6f6;
}

.box-body>div.col-md-3 a.btn {
	padding: 5px 20px;
	border-radius: 10px;
	margin-top: -3px;
}

.la_ul {
	list-style: none;
	float: right;
}

.la_ul li {
	width: 20px;
	height: 20px;
	border-radius: 6px;
	margin: 0 0 0 20px;
	float: left;
}

.la_ul li>div {
	width: 40px;
	font-size: 5px;
	margin-top: 23px;
	margin-left: -1px;
	position: absolute;
}
</style>
<script>
		(function(){
			$('#example2_wrapper table tbody>tr td[data-num]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-num');
				switch (data){
					case '1':$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '2':
						$(this).css({'backgroundColor':'#FFFFFF','color':'#000000'});
						break;
					case '3':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
					case '4':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
				}
			})
		})();
</script>
<script type="text/javascript">

//重置
$("#reset").click(function(){
	$("#freezerId").val("");
	$("#freezerName").val("");
});

//解锁
$("#unlock").click(function(){
	$.ajax({
	    url: "freezerRecord.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'unlock'},
	    success: function () {
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
});

//单元格单击
function td_click(i,a){
	$.ajax({
	    url: "freezerRecord.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'freezer_click',type:i,freezer:a},
	    success: function (json) {
	    	$("#freezerId").val(json.id);
	    	$("#freezerName").val(json.name);
	    	$("#myModalTwo").modal("hide");
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
}

</script>
	<div class="modal-dialog" role="document" style="width:1200px;">
	<form action="${url}" id="form1" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);"> 
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="col-md-12">
					<ul class="la_ul">
						<li style="background: #fea625;"><div>锁定</div></li>
						<li style="background: #29b6f6;"><div>占用</div></lFronti>
						<li style="background: #e84e40;"><div>装修</div></li>
					</ul>
				</div>
			</div>
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					<c:forEach items="${typeList }" var="u" varStatus="i">
						<c:if test="${i.index==0 }">
							<li class="active" style="width: 150px; text-align: center;"><a href="#type_${i.index }" data-toggle="tab" aria-expanded="true">${u }</a></li>
						</c:if>
						<c:if test="${i.index!=0 }">
							<li class="" style="width: 150px; text-align: center;"><a href="#type_${i.index }" data-toggle="tab" aria-expanded="false">${u}</a></li>
						</c:if>
					</c:forEach>
					</ul>
					<div class="tab-content">
						<c:forEach items="${list }" var="u" varStatus="i">
						<c:if test="${i.index==0 }">
							<div class="tab-pane active" id="type_${i.index }">
						</c:if>
						<c:if test="${i.index!=0 }">
							<div class="tab-pane" id="type_${i.index }">
						</c:if>
							<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
								<div class="row">
								<div class="col-sm-12">
									<table class="table table-bordered table-hover" >
											<tr role="row">
											<c:forEach items="${u }" var="f" varStatus="j">
												<c:choose>
													<c:when test="${f.flag == IsFlag_Decrate }">
														<td  style="width: 44px;height:40px;" data-num='${f.flag}' title='维修中'>
															${f.freezer.name}
														</td>
													</c:when>
													<c:when test="${f.flag == IsFlag_Lock }">
														<td  style="width: 44px;height:40px;" data-num='${f.flag}' title='${f.order.name},<fmt:formatDate value="${f.beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
															${f.freezer.name}
														</td>
													</c:when>
													<c:when test="${f.flag == IsFlag_Yse }">
														<td style="width: 44px;height:40px" data-num='${f.flag}' title='${f.order.name},<fmt:formatDate value="${f.beginDate}" pattern="yyyy/MM/dd HH:mm"/>'>
															${f.freezer.name}
														</td>
													</c:when>
													<c:otherwise>
														<td onclick="td_click(${i.index},${j.index})"  style="width: 44px;height:40px;" data-num='${f.flag}'>${f.name}</td>
													</c:otherwise>
												</c:choose>
											<c:if test="${(j.index+1)%8==0&&j.index!=0}">
												</tr>
												<tr role="row">
											</c:if>
											</c:forEach>
											</tr>
									</table>
								</div>
								</div>
							</div>
						</div>
						</c:forEach>
					</div>
				<div>
					<input type="button" data-dismiss="modal" id="reset" value="重置" />
					<input id="unlock" type="button" data-dismiss="modal"  value="解锁" />
				</div>
				</div>
			</div>
			</div>
		</form>
	</div>
	
													
