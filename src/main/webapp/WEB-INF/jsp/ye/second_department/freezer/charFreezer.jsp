<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="${url} " rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者姓名：</label>
									<span>${name} </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>联系人：</label>
									<span>${fName} </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者编号：</label>
									<span>${code}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>联系人电话：</label>
									<span>${fPhone} </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者性别：</label>
									<span>${sex}  </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者年龄：</label>
									<span>${age}  </span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">调度信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-2'>冷藏柜号：</label>
									<span> ${freezerName} </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>开始冷藏时间：</label>
									<%-- <span>${beginDate}</span> --%>
								<span>	<fmt:formatDate value="${beginDate }" pattern="yyyy-MM-dd HH:mm"/> </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>告别厅号：</label>
									<span>${farewellName}</span>
									
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>告别时间：</label>
								
										<span>	<fmt:formatDate value="${farewellRecordBD }" pattern="yyyy-MM-dd HH:mm"/> </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-3'>灵堂号：</label>
									<span>${mourningName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>灵堂摆灵时间：</label>
											<span>	<fmt:formatDate value="${mourningRecordBD }" pattern="yyyy-MM-dd HH:mm"/> </span>
								<!-- 注意灵堂开始时间同火化时间的混淆 -->

								</div>
							</div>
						</div>
						<p class="col-md-12 border-B p">入柜信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-6'>入柜经办人：</label>
									<span>${intoStaff}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>录入员：</label>
									<span>${intoOperator}</span>
								</div>
							</div>
						</div>
						<if test="${flag==IsFlag_No}">
									<p class="col-md-12 p border-B">出柜信息</p>
									<div class="col-md-12 border-B paddin-B">
										<div class="row">
											<div class="col-md-6 height-align">
												<label class='lab-6 height-align'>出柜经办人：</label>
												<span>${outStaff}</span>
											</div>
											<div class="col-md-6 height-align">
												<label class='lab-1'>出柜日期：</label>
												<span><fmt:formatDate value="${outTime }" pattern="yyyy-MM-dd HH:mm"/> </span>
											</div>
											<div class="col-md-6 height-align">
												<label class='lab-2'>录入员：</label>
												<span>${outOperator }</span>
											</div>
											<div class="col-md-6 height-align">
												<label class='lab-3'>备注：</label>
												<span>${outRemark }</span>
											</div>
										</div>
									</div>
						</if>
						
						<!--  						
							<button type="submit" class="btn btn-info btn-margin" >保存</button>
							<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
								-->
								<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
					
						
						
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>