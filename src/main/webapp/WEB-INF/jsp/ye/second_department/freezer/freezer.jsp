<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">冰柜调度视图</div>
</section>
<style type="text/css">
	#example2_wrapper{
		margin-top:10px;
		border:1px solid #ddd;
		padding:0px;
		border-radius:6px;
	}
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover,
	 .nav-tabs > li.active > a:focus{
		margin-right:0px;
		border-top:3px solid #29B6F6;
	}
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
}
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="editFreezer" type="hidden">
					<div class="box-body">
						<div class="col-md-6 btns-list" style="padding-left:0px;">
			
							<a href="freezerRecord.do?method=list2"  rel="myModal" target='homeTab'    id="liebiao" class='btn btn-info' role="button">进入列表</a>
						
							<a href="repairRecord.do?method=repair&type=freezer" target="dialog" rel="myModal" role="button" class="btn btn-danger">维修</a>
						</div>
						
						<div class="col-md-6">
							<ul class="la_ul">
								<li style="background:#fff;"><div>空闲</div></li>
								<li style="background:#FFA726;"><div>预定</div></li>
								<li style="background:#29b6f6;"><div>已入库</div></li>
								<li style="background:#e84e40;"><div>维修</div></li>
							</ul>
						</div>
					</div>
						<div class="box-body margin-T">
				<div class="nav-tabs-custom" style='border-top:1px solid #efefef;'>
					<ul class="nav nav-tabs">
					<c:forEach items="${typeList }" var="u" varStatus="i">
						<c:if test="${i.index==0 }">
							<li class="active" style="width: 150px; text-align: center;"><a href="#type_${i.index }" data-toggle="tab" aria-expanded="true">${u }</a></li>
						</c:if>
						<c:if test="${i.index!=0 }">
							<li class="" style="width: 150px; text-align: center;"><a href="#type_${i.index }" data-toggle="tab" aria-expanded="false">${u}</a></li>
						</c:if>
					</c:forEach>
					</ul>
					<div class="tab-content padding-L padding-R">
						<c:forEach items="${list }" var="u" varStatus="i">
						<c:if test="${i.index==0 }">
							<div class="tab-pane active" id="type_${i.index }">
						</c:if>
						<c:if test="${i.index!=0 }">
							<div class="tab-pane" id="type_${i.index }">
						</c:if>
									<p style='font-size:20px; color:blue;' align="center">闲置中：${number[i.index].idleNumber } &nbsp; &nbsp; &nbsp;    使用中：${number[i.index].usingNumber }</p>
							<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
								<div class="row">
								<div class="col-sm-12">
									<table class="table table-bordered table-hover1" >
											<tr role="row">
											<c:forEach items="${u }" var="f" varStatus="j">
												<c:choose>
													<c:when test="${f.flag == IsFlag_Decrate }">
														<td data-num='${f.flag}' title='装修'>													
															<a style="color:#fff" href="repairRecord.do?method=info&type=freezer&id=${f.id}&itemId=${f.freezer.id }&createUserId=${f.createUserId}" target="dialog" rel="myModal">
												${f.freezer.name}												
															</a>										
														</td>
													</c:when>
													<c:when test="${f.flag == 3 }">
														<td data-num='${f.flag}'  title='${f.order.name}'>
													
																<a style="color:#fff" href="freezerRecord.do?method=showFreeRecord&id=${f.id}"	 target="dialog"  rel="myModal" target='dialog'> ${f.freezer.name} </a>			
														</td>
													</c:when>
													<c:when test="${f.flag == 1 }">
														<td data-num='${f.flag}'  title='${f.order.name}'>
														
															<a style="color:#fff"  href="freezerRecord.do?method=showFreeRecord&id=${f.id}"	 target="dialog"  rel="myModal" target='dialog'>${f.freezer.name}</a>
														</td>
													</c:when>
											
													<c:otherwise>
														<td id='no' name='no'  data-num='${f.flag}' >${f.name}</td>
													</c:otherwise>
												
												
												</c:choose>
											<c:if test="${(j.index+1)%8==0&&j.index!=0}">
												</tr>
										<tr role="row">
											</c:if>
											</c:forEach>
											</tr>
									</table>
								</div>
								</div>
							</div>
						</div>
						</c:forEach>
					</div>
			
				</form>
			</div>
	
		</div>
	</div>
</section>
<script>
	//window.onload=function(){
		//console.log(1);
		
		/**
		(function(){
			$('#example2_wrapper table tbody>tr td[data-num]').each(function(){
				var data=$(this).attr('data-num');
				switch (data){
					case '1':
					$(this).html('<a href="#">完成</a>').css({'backgroundColor':'#9CCC65','color':'#fff'}).find('a').css('color','#fff');
					break;
					case '2':
						$(this).html('进行').css({'backgroundColor':'#738ffe','color':'#fff'});
						break;
					case '3':
						$(this).html('锁定').css({'backgroundColor':'#fea625','color':'#fff'});
						break;
				
					case '4':
						$(this).html('占用').css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '5':
						$(this).html('装修').css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
					
					default:
						break;
				}
			})
		})();
		
		*/
		(function(){
			$('#example2_wrapper table tbody>tr td[data-num]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-num');
				switch (data){
					case '1':$(this).css({'backgroundColor':'#29b6f6','color':'#fff'});
						break;
					case '2':
					
						break;
					case '3':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
					case '4':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
				}
			})
		})();
</script>
<script type="text/javascript">

//重置
$("#reset").click(function(){
	$("#freezerId").val("");
	$("#freezerName").val("");
});

//解锁
$("#unlock").click(function(){
	$.ajax({
	    url: "freezerRecord.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'unlock'},
	    success: function () {
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	} )
});

</script>

