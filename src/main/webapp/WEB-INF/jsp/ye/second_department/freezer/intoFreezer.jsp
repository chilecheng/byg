<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
	
</head>
<body>
		<form id="detail" action="freezerRecord.do" rel="myModal"  onsubmit="return readData(this,homeAjaxDone);">
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			
			<input type="hidden" name="goWhere" value="${goWhere }" >
			<input type="hidden" name="noRef" value="${noRef }" >
			<input type="hidden" name="method" value="${method }" >
			<input type="hidden" name="id" value="${id }" >
			<!-- 
			<input type="hidden" name="currentTime" value="${currentTime}" >
			 -->
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-B">
					<div class="row">
						<p class="col-md-12 p bordor-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者姓名：</label>
									<span>${name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>联系人：</label>
									<span>${fName} </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者编号：</label>
									<span>${code}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>联系人电话：</label>
									<span>${fPhone}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者性别：</label>
									<span>${sex }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者年龄：</label>
									<span>${age }</span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">调度信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-2'>冷藏柜号：</label>
									<span>${freezerName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>开始冷藏时间：</label>
									<span><fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">入柜信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								
								
								
								 <div class="col-md-12 height-align wk-out">
									<label class='lab-2'>入柜人员：</label> 
									<input type="text" readonly class="list_input input-dialog" id='assiginUser' data-name="workerName" name='assiginUser'  value=''/>
									<input type="text" readonly class="list_input" id='assiginUserId' data-id="workerId" name='assiginUserId'  hidden value=''/>
									<a class='wk-choose btn btn-default' data-choose='get'>选择</a>
									<a class='hide' data-fun="wk"  target='dialog' href='${url}?method=choose&groupType=5'>选择</a>
								</div>
			
								<div class="col-md-6 height-align">
									<label class='lab-3'>录入员：</label>
									<span>${agentUser}</span>
									
							
								</div>
								
						</div>
						</div>
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit" data-a='dismiss'  class="btn btn-info btn-margin" >保存</button>				
							<button type='reset' class="btn btn-color-9E8273 btn-margin" >重置</button>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					
				</div>
			</div>
		</div>
				 <script>
	 	(function(){
	 		/*选择按钮初始化*/
	 		$('[data-choose="get"]').click(function(){
	 			data_group.name=$(this).prev().prev().attr('data-name');
	 			data_group.id=$(this).prev().attr('data-id');
	 			$(this).next().click();
	 		});
	 	})()
	 </script>
	</form>
	
</body>
	
<script type="text/javascript">
	$("[data-a='dismiss']").click(function(){
		var tbody=[];
		$("#articles").find("tr").each(function(i,tr){
			var trs='';
			$(tr).find("td").each(function(j,td){
				if(j>0){
						trs+=',';
				}
				trs+=$(td.children[0]).val();	
			})
			tbody[i]=trs;
		})
		var $form = $('#detail');
		var va = validHomeForm($form);
		rel = $form.attr("rel");
		var datas= $form.serializeArray();
		var obj={};
		for(var key in datas){
			obj[datas[key].name]=datas[key].value;
		}
		obj.table=tbody;
		$.ajax({
			type : 'POST',
			traditional: true,
			url : $form.attr("action"),
			data : obj,
			dataType : "json",
			cache : false,
			success :dialogAjaxDone,
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				toastr["error"](XMLHttpRequest.status);
			}
		});
		$('[data-dismiss="modal"]').click();
		return false;
	})

$(function(){
	timeRange();
	timeMunite();
});
</script>