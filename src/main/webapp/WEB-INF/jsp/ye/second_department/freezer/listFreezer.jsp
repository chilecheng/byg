<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">冰柜调度</div>
</section>
<style type="text/css">

#main-content>div.box>div.row {
	background-color: #fff;
	width: 100%;
	margin-left: 0px;
	padding-bottom: 15px;
	border-radius: 6px;
}

</style>
<script type="text/javascript">
	function changeDate() {
		$("#pageForm").submit();
	}
	timeRange();
</script>
<form action="${url}" id="pageForm"	onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	
	<input name="method" value="list2" type="hidden">
	
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color: #4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool"
								data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->

					<form action="freezerRecord?method=list2" id="pageForm" 	onsubmit="return homeSearch(this);">
					
						<div class="box-body">
							<div class="col-md-4">
								<label>类型：</label>
							 <select class="list_input" id="select" 	name="select">
									${searchOption }
							</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
							<input type="text" class="list_input input-hometab"
									id='search' name='selectValue' value= "${selectValue}" placeholder='单行输入' />
							</div>
							
							
							<div class="col-md-12">
								<label>到馆时间：</label> 
								<input type="text"	data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="${startTime}">
									<i	style="margin-left: -20px;" class="fa fa-calendar"></i>
							 	<label	class='timerange'>--</label> 
							 	<input type="text"	data-id="endDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime"	name="endTime"  value="${endTime}">
									<i	style="margin-left: -20px;" class="fa fa-calendar"></i>

							</div>
					
							<!-- 控制常用搜索状态（已审核等） -->
							<input type="hidden" id="checkType" name="checkType" value="${checkType }">
							<div class="col-md-12" id="typeDiv">
								<label>常用：</label>
								<button type="submit" class="btn btn-normally btn-all" name="stationAll">全部</button>
								<button type="submit" class="btn btn-normally" name="stationNo">未冷藏</button>
								<button type="submit" class="btn btn-normally" name="stationIng">冷藏中</button>
								<button type="submit" class="btn btn-normally" name="stationEnd">冷藏结束</button>
								<button type="submit" style='width:110px;' class="btn btn-normally" name="stationWait">最近两天出柜</button>
								<button type="submit" class="btn btn-normally" name="stationOut">需出柜</button>
							</div>
							
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search"
									>搜索</button>
							</div>
						</div>
					</form>
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>

			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<small class='  btns-buy'>
								<button class='btn btn-info' target='dialog' checkone="true" checkname="id" data-fun='freezer' data-num='3'>入柜</button>
								<a	href="freezerRecord.do?method=into&noRef=no&goWhere=inner&id=" checkone="true" checkname="id" id="click" class="btn btn-warning hide"
								rel="myModal" target='dialog'>入柜</a>
								<button  class='btn btn-color-ff7043' checkone="true" checkname="id" target='dialog' data-fun='freezer' data-num='1'>转柜</button>
								<a  href="freezerRecord.do?method=transfer&id=" checkone="true" checkname="id"	rel="myModal"  class="btn hide" target='dialog'>转柜</a>
								<button  class='btn btn-danger' checkone="true" checkname="id" target='dialog' data-fun='freezer' data-num='1'>出柜</button>
								 <a href="freezerRecord.do?method=out&id=" checkone="true" checkname="id" target="dialog"  rel="myModal" class="btn btn-danger hide"   >出柜</a> 
								<a href="freezerRecord.do?method=editFreezer" class="btn btn-default" target='homeTab' rel="myModal">返回</a>
							</small>	
						</div>
					</div>
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th>
								</th>
								<th>死者姓名</th>
								<th>死者性别</th>
								<th>年龄</th>
								<th>到馆时间</th>
								<th>冰柜号</th>
								<th>火化时间</th>
								<th>冷藏柜状态</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${fn:length(page.list)==0 }">
									<tr>
										<td colspan="20"><font color="Red">还没有数据</font></td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${page.list }" var="u">
										<tr role="row">
										<tr>
									    <td><input type='hidden' name='flag' value='${u.flag}' id='flag'><input type='radio' class='checkBoxCtrl' name='id' value='${u.id}'/></td>
											<td>
												<a	href="freezerRecord.do?method=showFreeRecord&id=${u.id}"
													class="loser" target="dialog" rel="myModal">${u.name}</a>
											</td>
											<td>
												<c:if test="${u.sex==1 }">
															男
												   </c:if>
												   <c:if test="${u.sex==2 }">
															女
												   </c:if>
											</td>
											<td>${u.age}</td>
											<td><fmt:formatDate value="${u.arriveTime }" pattern="yyyy-MM-dd HH:mm"/></td>
											<td>${u.freezerName }</td>		
											<td><fmt:formatDate value="${u.cremationTime }" pattern="yyyy-MM-dd HH:mm"/></td>
											<td class='state1'>
												<c:if test="${u.flag==1 }">
														已入库
											   </c:if>
											   <c:if test="${u.flag == 2 }">
														已出库
											   </c:if>
											   <c:if test="${u.flag==3 }">
														预定
											   </c:if>
											   
											</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>
<script>
	(function() {
		$('table.table.table-bordered tbody tr td:last-child').each(function() {
			var text = $(this).text().trim();
			switch (text) {
			case '已入库':
				$(this).css('color', '#8EC44F');
				break;
			case '已出库':
				$(this).css('color', '#FAC16E');
				break;
			case '预定':
				$(this).css('color', '#E84E40');
				break;
			}
		})
	})()
	$(function(){
	timeRange();
	})
//当前页状态改变，用于后台判断，页面刷新之后跳转到当前页
var type=active.parsent;
$("#clickId").val(type);
$('[aria-controls="'+type+'"]').click();
$("#liClick").on('click','li a',function(){
	type=$(this).attr('aria-controls');
	active.parsent=type;
	$("#clickId").val(type);
// 	alert($("#clickId").val());
})
//将点击的常用搜索按钮对应值改变，用于后台判断
var checkType='';
$("#typeDiv").on('click','button',function(){	
	checkType=$(this).attr('name');
	$("#checkType").val(checkType);
})
$('[data-fun="freezer"]').click(function(){
	var checkname=$(this).attr('checkname');
	var value=$('[name="'+checkname+'"]:checked').prev().val();
	if(value==1 || value ==3){
		if(value!==$(this).attr('data-num')&&$('[name="'+checkname+'"]:checked').size()!==0){
			toastr["warning"]("不能进行此操作");
		}
		else{
			$(this).next().click();
		}
	}
	// 已出柜后 再次入柜的判断 
	if(value==2){
		if(1==$(this).attr('data-num')&&$('[name="'+checkname+'"]:checked').size()!==0  ){
			toastr["warning"]("不能进行此操作");
		}else{
			$(this).next().click();
		}
	}
})


</script>