<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<body>
	<!-- -查看丧葬用品- -->
	<form id="detail"  action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
				<input type="hidden" name="method" value="${method}" >
			<input type="hidden" name="id" value="${id}" >
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-5 height-align">
									<label class='lab-6'>死者姓名：</label>
									<span>${name}</span>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-6'>原冰柜号：</label>
									<span>${freezerName}</span>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>死者编号：</label>
									<span>${code }</span>
								</div>
								<!-- 
								<div class="col-md-7">
									<label class='lab-6'>新冰柜号：</label>
									<select class="list_input input-dialog" id="newFreezer" name="newFreezer">
										<option>  ${freezerOption} </option>
									</select>
								</div>
								 -->
								<div class="col-md-7 height-align">
									<label  class='lab-6'>冷藏柜号：</label> 
									<input type="hidden" name="freezerId" id="freezerId"  value="${newFreezer}">
									<input name="freezerName" id="freezerName" class="list_select input-dialog"  style='width:40%;min-width:40%'  readonly="readonly" value="${freezerName}">
									 
									<a href="${url}?method=editFreezer&type=transfer" target="dialog" rel="myModalTwo" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								 
								 
				<!-- 					<a href="commissionOrder.do?method=editFreezer" target="dialog" rel="myModal" class="btn btn-info" role="button" style="height: 28px;line-height:1">选择</a>
								 -->
								</div>
								
								
								
								
								
								
								<div class="col-md-5 height-align">
									<label class='lab-6'>死者性别：</label>
									<span>${sex }</span>
									
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-2'>备注：</label>
									<textarea row='6' col='150' class='list_input textarea-dialog' style='min-width:50%;width:50%' placeholder='多行输入' id='remark' name='remark'></textarea>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-6'>死者年龄：</label>
									<span>${age }</span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">服务信息</p>
						<div class="col-md-12 broder-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-6'>操作时间：</label>
									<span>${currentTime }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>录入员：</label>
									<span>${agentUser}</span>
								</div>
							</div>
						</div>
						
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit"  data-a='dismiss'  class="btn btn-info btn-margin" >保存</button>
							<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
							
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>
<script>
	$("[data-a='dismiss']").click(function(){
		
		var $form = $('#detail');
		var va = validHomeForm($form);
		rel = $form.attr("rel");
		
		var datas= $form.serializeArray();
		
		var obj={};
		for(var key in datas){
			obj[datas[key].name]=datas[key].value;
		}
		$.ajax({
			type : 'POST',
			traditional: true,
			url : $form.attr("action"),
			data : obj,
			dataType : "json",
			cache : false,
			success :homeAjaxDone,
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				toastr["error"](XMLHttpRequest.status);
			}
		});
		$('[ data-dismiss="modal"]').click();
		return false;
	})
</script>