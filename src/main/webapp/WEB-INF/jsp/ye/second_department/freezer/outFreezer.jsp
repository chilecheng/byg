<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<body>
	<!-- -出柜- -->
	<c:choose>
		<c:when test="${homeType !=null && homeType!='' }">
			<form id="detail"  action="${url}" rel="myModal" data-type='${homeType }' onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		</c:when>
		<c:otherwise>
			<form id="detail"  action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		</c:otherwise>
	</c:choose>
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
				<input type="hidden" name="method" value="${method}" >
			<input type="hidden" name="id" value="${id}" >
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-2'>死者姓名：</label>
									<span>${name}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-3'>联系人：</label>
									<span>${fName}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>死者编号：</label>
									<span>${code}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>联系人电话：</label>
									<span>${fPhone}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>死者性别：</label>
									<span>${sex}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>死者年龄：</label>
									<span>${age}</span>
								</div>
							</div>
						</div>
						<p class='col-md-12 p  border-B '>出柜信息</p>
						<div class='col-md-12 border-B padding-B'>
							<div class='row'>
								<div class="col-md-5 height-align">
									<label class='lab-1'>出柜经办人：</label>
									<select class="list_input input-dialog" id="outer" name="outer">
										<option>${staffOption}</option>
									</select>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-2'>出柜日期：</label>
									<input type="text" data-id="reservation" readOnly='readOnly' data-min-view="2" data-date-format="yyyy-mm-dd  HH:mm" class="list_select input-dialog" id="outTime" name="outTime" value="<fmt:formatDate value="${outTime}" pattern="yyyy-MM-dd HH:mm"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-3'>录入员：</label>
									<span>${agentUser}</span>
								</div>
								
								<div class="col-md-7 height-align">
									<label class='label-4'>备注：</label>
									<textarea row='6' col='150' class='list_input textarea-dialog' style='min-width:50%;width:50%;' placeholder='多行输入' id="outRemark" name= 'outRemark'></textarea>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">调度信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-2'>冷藏柜号：</label>
									<span>${freezerName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>开始冷藏时间：</label>
									<span>	<fmt:formatDate value="${beginDate }" pattern="yyyy-MM-dd HH:mm"/> </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>告别厅号：</label>
									<span>${farewellName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>告别时间：</label>
									<span>	<fmt:formatDate value="${farewellRecordBD }" pattern="yyyy-MM-dd HH:mm"/> </span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-3'>灵堂号：</label>
									<span> ${mourningName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>灵堂摆灵时间：</label>
								
									<span>	<fmt:formatDate value="${mourningRecordBD }" pattern="yyyy-MM-dd HH:mm"/> </span>
								</div>
							</div>
						</div> 
						
						<p class="col-md-12 p border-B ">入柜信息</p>
						<div class="col-md-12 border-B padding-B">
							<div class="row">
								<div class="col-md-8 height-align">
									<label class='lab-6'>入柜经办人：</label>
									<span>${intoStaff}</span>
								</div>
								<div class="col-md-4 height-align">
									<label class='lab-2'>录入员：</label>
									<span>${intoOperator}</span>
								</div>
							</div>
						</div>
						
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit" data-a='dismiss' class="btn btn-info btn-margin">保存</button>
							
							<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
							
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>
<script>
	$("[data-a='dismiss']").click(function(){
		$('[ data-dismiss="modal"]').click();
	})
	timeMunite();
</script>