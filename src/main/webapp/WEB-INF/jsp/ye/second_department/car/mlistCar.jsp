<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">车辆调度</div>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
</style>
<script type="text/javascript">
var checkType='1';
$("#typeDiv").on('click','button',function(){	
	checkType=$(this).attr('name');
	$("#checkType").val(checkType);
});
function changeDate(){
	$("#pageForm").submit();
}
</script>
<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead.jsp"%>
	<input name="method" value="${method }" type="hidden">
	<section class="content">
	<!-- -购买丧葬用品（主）- -->
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
					</div>
						<div class="box-body">
							<div class="col-md-4">
								<label>类型：</label> 
								<select class="list_input" id="name" name="searchType">
<!-- 									<option value='dName'  id="" class="" >姓名</option> -->
<!-- 									<option value='Card'  id="" class="">卡号	</option> -->
<!-- 									<option value='fName' id="" class=""> 联系人姓名	</option> -->
									${searchOption }
								</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" id='search' name='searchVal' value='' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="beginDate" value="<fmt:formatDate value="${beginDate }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<label class='timerange'>--</label> 
								<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endDate" value="<fmt:formatDate value="${endDate }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								
							</div>
							<!-- 控制  下面 button  的相关属性 -->
							<input type="hidden" id="checkType" name="checkType" value="${checkType }">
							<div class="col-md-12" id="typeDiv">
								<label>常用：</label>
								<button type="submit" class="btn btn-normally btn-all" name="1">全部</button>
								<button type="submit" class="btn btn-normally" name="4">未派车</button>
								<button type="submit" class="btn btn-normally" name="3">已派车</button>
								<button type="submit" class="btn btn-normally" name="2">接运完成</button>
<%-- 								<a href="${url}?method=listInfo&type=4" target="homeTab" class="btn btn-normally">未派车</a> --%>
<%-- 								<a href="${url}?method=listInfo&type=2" target="homeTab" class="btn btn-normally">已派车</a> --%>
<%-- 								<a href="${url}?method=listInfo&type=3" target="homeTab" class="btn btn-normally">接运完成</a> --%>
<%-- 								<a href="${url}?method=listInfo&type=1" target="homeTab" class="btn btn-normally btn-all">全部</a> --%>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class="content nopadding-T" id='main-content'>
		<div class='box'>
		
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<small class='btns-buy'>

								<a href="${url }?method=dispatch&noRef=no&id=" checkname="id" data-check="checkCar" class="btn btn-warning" rel="myModal" target='dialog'>派车</a>
								<a href="${url }?method=change&type=complete&id=" target="ajaxTodo" warm="确认完成？" checkname="id" class="btn btn-info" rel="myModal" target='dialog'>完成</a>
								<%-- <a href="${url }?method=dispatch&id=" target="dialog" checkname="id" rel="myModal"  class="btn btn-success">休息</a> --%>
								<a href="${url }?method=change&type=cancel&id=" target="ajaxTodo" warm="确认撤销？" checkname="id" class="btn btn-danger" rel="myModal" target='dialog'>撤销</a>
								<a href="${url }?" class="btn btn-default" target='homeTab' rel="myModal">返回</a>
							</small>
						</div>
					</div>
					<table class="table table-bordered margin-T">
						<thead>
							<tr>
								<th width="10"></th>
								<th>死者姓名</th>
								<th>车辆牌照</th>
								<th>接运时间</th>
								<th width='200px'>接尸地址</th>
								<th>车辆类型</th>
								<th>运输类型</th>
								<th>联系人姓名</th>
								<th>联系人手机</th>
								<th>状态</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
							    <c:when test="${fn:length(page.list)==0 }">
								    <tr width="20">
						    			<td colspan="10">
									  		<font color="Red">还没有数据</font>
								 		</td>
								    </tr>
							    </c:when>
							    <c:otherwise>
									<c:forEach items="${page.list }" var="list">
										<tr>
											<td><input type='radio' name='id' value='${list.id }'/></td>
											<td>${list.commissionOrder.name }</td>
											<td>${list.car.carNumber }</td>
											<td><fmt:formatDate value="${list.pickTime }" pattern="yyyy-MM-dd HH:mm"/></td>
											<td>${list.commissionOrder.pickAddr }</td>
											<td>${list.carType.name }</td>
											<td>${list.transportType.name }</td>
											<td>${list.commissionOrder.fName }</td>
											<td>${list.commissionOrder.fPhone }</td>
											<td data-num='${list.flag}' class='state1'></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<%@ include file="/common/pageFood.jsp"%>
				</div>
			</div>
		</div>
	</section>
</form>
<script>

	(function(){
		$('table.table.table-bordered tbody tr td[data-num]').each(function(){
			var data=$(this).attr('data-num');
			switch (data){
			case '${CarSFlag_OK}':
				$(this).html('已完成').css('color','#E84E40');
				break;
			case '${CarSFlag_NO}':
				$(this).html('未派车').css('color','#FAC16E');
				break;
			case '${CarSFlag_GO}':
				$(this).html('已派车').css('color','#29B6F6');
				break;
			}
		})
		timeRange();
	})()
</script>