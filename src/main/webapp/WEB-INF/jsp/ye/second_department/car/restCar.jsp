<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-T nopadding-B">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B padding-B nomargin-B">
							<div class="row">
								<div class="col-md-12">
									<label class='lab-5'>车牌号码：</label> 
									<select class="list_input input-dialog" id="carNum" name="carNum">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>运输类型：</label> 
									<select class="list_input input-dialog" id="transType" name="transType">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>车辆类型：</label> 
									<select class="list_input input-dialog" id="carType" name="carType">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>驾驶人员：</label> 
									<select class="list_input input-dialog" id="driver" name="driver">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>殡仪人员：</label> 
									<select class="list_input input-dialog" id="fDirector" name="fDirector">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-12">
									<label class='lab-2'>说明：</label> 
									<textarea id='reason' name='reason' class='textare-dialog' style='height:50px'></textarea>
								</div>
							</div>
							
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B padding-B nomargin-T">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-5'>录入时间：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-5'>经办人员：</label>
									<span>111111111111111</span>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit" class="btn btn-info btn-margin" >保存</button>
							<button type='reset' class="btn btn-margin btn-color-9E8273">重置</button>
							
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>