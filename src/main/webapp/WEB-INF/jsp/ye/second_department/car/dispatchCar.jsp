<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
<div>
	<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<%-- <c:choose>
						<c:when test="${homeType !=null && homeType!='' }">
							<form id="detail" action="${url }" rel="myModal" data-type='${homeType }' onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
						</c:when>
						<c:otherwise>
							<form id="detail" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
						</c:otherwise>
					</c:choose> --%>
					<form id="detail" action="${url }" rel="myModal" onsubmit="return validateHomeCallback(this,dialogAjaxDone);">
						<input type="hidden" value="${id }" name="id">
						<input type="hidden" value="${noRef }" name="noRef">
						<input type="hidden" value="${method }" name="method">
						<div class="row">
							<p class="col-md-12 nomargin-B p border-B">业务信息</p>
							<div class="col-md-12 nomargin-T border-B padding-B">
								<div class="row">
									<div class="col-md-6 height-align">
										<label class='lab-5'>死者姓名：</label>
										<span>${info.commissionOrder.name } </span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-5'>联系人姓名：</label>
										<span>${info.commissionOrder.fName }</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>死者编号：</label>
										<span>${info.commissionOrder.code }</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-5'>联系人电话：</label>
										<span>${info.commissionOrder.fPhone }</span>
									</div>
								</div>
							</div>
							<p class="col-md-12 nomargin-B border-B p">预定信息</p>
							<div class="col-md-12 nomargin-T border-B">
								<div class="row">
									<div class="col-md-12">
										<label class='lab-1'>车牌号码：</label> 
										<c:if test="${type ne 'carInfo' }">
											<input id="carName" value="${info.car.carNumber }" class="required list_select" >
											<input name="carId" type="hidden" value="" id="carId" />
											<a href="${url}?method=list&type=select" target="dialog" rel="myModalTwo" class="btn btn-info" style='padding:3px 10px' role="button">选择</a>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span>${info.car.carNumber }</span>
										</c:if>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>运输类型：</label> 
										<c:if test="${type ne 'carInfo' }">
											<select class="list_input input-dialog nopadding-R" style='color:#000;' id="transType" name="transType">
												${trOption }
											</select>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span>${info.transportType.name }</span>
										</c:if>
									</div>
									<div class="col-md-6">
										<label class='lab-1'>车辆类型：</label> 
										<c:if test="${type ne 'carInfo' }">
											<select class="list_input input-dialog nopadding-R" style='color:#000;' id="carType" name="carType">
												${carOption }
											</select>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span>${info.carType.name }</span>
										</c:if>
									</div>
									<div class="col-md-6">
										<label class='lab-1'>死亡原因：</label>
										<span>${info.commissionOrder.deadReason.name }</span>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-3'>状态：</label>
										<span id="carFlag" data-flag="${info.flag }"></span>
									</div>
									<div class="col-md-12">
										<label class='lab-1'>接运地址：</label>
										<span>${info.commissionOrder.pickAddr }</span>
									</div>
									<div class="col-md-12">
										<label class='lab-3'>备注：</label>
										<c:if test="${type ne 'carInfo' }">
											<textarea row='6' col='150' class='list_input textarea-dialog' placeholder='多行输入' name="comment"></textarea>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span>${info.comment }</span>
										</c:if>
									</div>
								</div>
							</div>
							<p class="col-md-12 nomargin-B border-B p">派车调度</p>
							<div class="col-md-12 nomargin-T border-B">
								<div class="row">
									<div class="col-md-6 height-align">
										<label class='lab-1'>发车时间：</label> 
										<c:if test="${type ne 'carInfo' }">
											<input value='<fmt:formatDate value="${info.startTime }" pattern="yyyy-MM-dd HH:mm"/>' class="required list_select  input-dialog  startTime" name="startTime" readonly="readonly" >
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span><fmt:formatDate value="${info.startTime }" pattern="yyyy-MM-dd HH:mm"/></span>
										</c:if>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>返回时间：</label> 
										<c:if test="${type ne 'carInfo' }">
											<input value='<fmt:formatDate value="${info.returnTime }" pattern="yyyy-MM-dd HH:mm"/>' class="required list_select  input-dialog  returnTime" name="returnTime" readonly="readonly">
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span><fmt:formatDate value="${info.returnTime }" pattern="yyyy-MM-dd HH:mm"/></span>
										</c:if>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>驾驶人员：</label> 
										<c:if test="${type ne 'carInfo' }">
											<select class="list_input required input-dialog nopadding-R" id="driver" name="driverId">
												<option></option>
												${driverOption }
											</select>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span>${driver }</span>
										</c:if>
									</div>
									<div class="col-md-12 height-align">
										<label class='lab-1'>殡仪人员：</label> 
										<c:if test="${type ne 'carInfo' }">
											<%-- <select class="list_input required input-dialog nopadding-R" id="fDirector" name="funeralId">
												<option></option>
												${funeralOption }
											</select> --%>
											<input type="text" readonly class="list_input input-dialog" id='assiginUser' data-name="workerName" name='assiginUser'  value=''/>
											<input type="text" readonly class="list_input" id='assiginUserId' data-id="workerId" name='assiginUserId'  hidden value=''/>
											<a class='wk-choose btn btn-default' data-choose='get'>选择</a>
											<a class='hide' data-fun="wk"  target='dialog' href='${url}?method=choose&groupType=5'>选择</a>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											${info.funeralId }
										</c:if>
									</div>
								</div>
							</div>
							<p class="col-md-12 nomargin-B border-B p">办理信息</p>
							<div class='col-md-12 nomargin-T border-B padding-B'>
								<div class='row'>
									<div class="col-md-6 height-align">
										<label class='lab-1'>登记人员：</label>
										<c:if test="${type ne 'carInfo' }">
											<span>${enter.name }</span>
											<input type="hidden" value="${enter.userId }" class='list_input  input-dialog nopadding-R' name="enterId">
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											${enter }
										</c:if>
									</div>
									<div class="col-md-6 height-align">
										<label class='lab-1'>登记时间：</label>
										<c:if test="${type ne 'carInfo' }">
											<span>系统自动取当前时间</span>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span><fmt:formatDate value="${info.regTime }" pattern="yyyy-MM-dd HH:mm"/></span>
										</c:if>
									</div>
									<div class="col-md-6">
										<label class='lab-1'>流水单号：</label>
										<c:if test="${type ne 'carInfo' }">
											<span>由系统自动生成</span>
										</c:if>
										<c:if test="${type eq 'carInfo' }">
											<span>${info.flowNumber }</span>
										</c:if>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-md-offset-2 btns-dialog nomargin-T">
								<c:if test="${type ne 'carInfo' }">
									<button type="submit" class="btn btn-info btn-margin" >保存</button>
									<button type='reset' class="btn btn-color-9E8273 btn-margin" >重置</button>
								</c:if>
								<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
							</div>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>
	<script>
		(function(){
			$('#carFlag[data-flag]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-flag');
				switch (data){
				case '${CarSFlag_OK}':
					$(this).html('已完成').css('color','#E84E40');
					break;
				case '${CarSFlag_NO}':
					$(this).html('未派送').css('color','#FAC16E');
					break;
				case '${CarSFlag_GO}':
					$(this).html('已派送').css('color','#29B6F6');
					break;
				}
			});
			/*选择按钮初始化*/
	 		$('[data-choose="get"]').click(function(){
	 			data_group.name=$(this).prev().prev().attr('data-name');
	 			data_group.id=$(this).prev().attr('data-id');
	 			$(this).next().click();
	 		});
		})();
	</script>
</body>