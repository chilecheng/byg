<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<p class="col-md-12 border-B p">业务信息</p>
						<div class="col-md-12 border-B nomargin-T padding-B">
							<div class="row">
								<div class="col-md-6">
									<label class='lab-5'>死者姓名：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>联系人姓名：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>死者编号：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>联系人电话：</label>
									<span>111111111111111</span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">预定信息</p>
						<div class="col-md-12 border-B nomargin-B padding-B">
							<div class="row">
								<div class='col-md-12'>
									<label class='lab-5'>车牌号码：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>运输类型：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>车辆类型：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>死亡原因：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>状态：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-12">
									<label class='lab-5'>接运地址：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-12">
									<label class='lab-5'>备注：</label>
									<span>111111111111111</span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">派车调度</p>
						<div class="col-md-12 border-B nomargin-B padding-B">
							<div class="row">
								<div class="col-md-6">
									<label class='lab-5'>发车时间：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>返回时间：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>驾驶人员：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>殡仪人员：</label>
									<span>111111111111111</span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B nomargin-B padding-B">
							<div class="row">
								<div class="col-md-6">
									<label class='lab-5'>登记人员：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>登记时间：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>流水单号：</label>
									<span>111111111111111</span>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit" class="btn btn-info btn-margin" >保存</button>
							<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
							
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>