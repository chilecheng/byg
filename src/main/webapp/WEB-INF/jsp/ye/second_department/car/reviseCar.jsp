<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-T nopadding-B">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B nomargin-T padding-B">
							<div class="row">
								<div class="col-md-6">
									<label class='lab-5'>死者姓名：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>联系人姓名：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>死者编号：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>联系人电话：</label>
									<span>111111111111111</span>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">预定信息</p>
						<div class="col-md-12 nomargin-T padding-B border-B">
							<div class="row">
								<div class="col-md-12">
									<label class='lab-5'>车牌号码：</label> 
									<select class="list_input content-input" id="carNum" name="carNum">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>运输类型：</label> 
									<select class="list_input content-input" id="transType" name="transType">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>车辆类型：</label> 
									<select class="list_input content-input" id="carType" name="carType">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>死亡原因：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>状态：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-12">
									<labe class='lab-5'l>接运地址：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-12">
									<label class='lab-5'>备注：</label>
									<textarea row='6' col='150' class='list_input area' placeholder='多行输入'></textarea>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">派车调度</p>
						<div class="col-md-12 border-B nomargin-T padding-B">
							<div class="row">
								<div class="col-md-6">
									<label class='lab-5'>发车时间：</label> 
									<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select content-input" id="goTime" name="goTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>返回时间：</label> 
									<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select content-input" id="backTime" name="backTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6">
									<label>驾驶人员：</label> 
									<select class="list_input content-input" id="driver" name="driver">
										<option value=''></option>
									</select>
								</div>
								<div class="col-md-6">
									<label>殡仪人员：</label> 
									<select class="list_input content-input" id="fDirector" name="fDirector">
										<option value=''></option>
									</select>
								</div>
							</div>
						</div>
						<p class="col-md-12 p broder-B">办理信息</p>
						<div class='col-md-12 border-B padding-B nomargin-T'>
							<div class='row'>
								<div class="col-md-6">
									<label>登记人员：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label>登记时间：</label>
									<span>111111111111111</span>
								</div>
								<div class="col-md-6">
									<label class='lab-5'>流水单号：</label>
									<span>111111111111111</span>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<button type="submit" class="btn btn-info btn-margin" >保存</button>
							<button type='reset' class="btn btn-color-9E8273 btn-margin">重置</button>
							
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
	
</body>