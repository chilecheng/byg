<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">车辆调度</div>
</section>
<style type="text/css">
</style>
<script type="text/javascript">
function changeDate(){
	$("#pageForm2").submit();
};

function td_click(i,a){//单元格单击
	var seDate = $("#seDate").val();
	$.ajax({
	    url: "${url }",
	    dataType:'json',
	    cache: false,
	    data:{method:'selectedTime',col:a,row:i,date:seDate},
	    success: function (json) {
	    	var startR= $('input:radio[id="start_radio"]:checked').val();
	    	var endR= $('input:radio[id="end_radio"]:checked').val();
	    	if(endR!=null){
	    		$(".returnTime").val(json.time);
	    		var start=$(".startTime").val();
	    		var end=$(".returnTime").val();
	    		if(start>=end){
	    			toastr["error"]("出发时间不能大于等于返回时间！");
	    		} else {
	    			$("#myModalTwo").modal("hide");
	    		}
	    	}
	    	if(startR!=null){
	    		$(".startTime").val(json.time);
	    		$("#end_radio").attr("checked","checked");
	    	}
			$("#carId").val(json.carId);
			$("#carName").val(json.carNumber);
	    	$("#cNumber").val(json.carNumber);
	    	$("#cId").val(json.carId);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	})
};

function carChangeTime(form) {
	var $form = $(form);
	$.ajax({
		type : 'POST',
		url : $form.attr("action"),
		data : $form.serializeArray(),
		traditional: true,
		dataType : "html",
		cache : false,
		success : function(json) {
			$("#myModalTwo").html(json);
			initTab();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			toastr["error"](XMLHttpRequest.status);
		}
	});
	return false;
}
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url }" id="pageForm2" onsubmit="return carChangeTime(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<input name="type" value="${type}" type="hidden" />
					<div class="box-body">
						<div class="col-md-12">
							<label>日期：</label> 
							<input type="text" data-provide="datetimepicker" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="seDate" name="date" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<label>车牌号：</label> 
							<input name="carNumber" id="cNumber" value="">
							<input name="carId" type="hidden" id="cId" value="">
							<label>出发时间：</label> 
							<input id="start_radio" name="radio" type="radio" checked="checked" /> 
							<input  class="required list_select input_a startTime" name="startTime" >
							<label>返回时间：</label> 
							<input id="end_radio" name="radio" type="radio" />
							<input class="required list_select input_a returnTime" name="returnTime" >
						</div>
						<div class="col-md-3">
							<ul class="la_ul">
 								<li style="background:#9ccc65;"><div>完成</div></li> 
 								<li style="background:#738ffe;"><div>进行</div></li> 
								<!-- <li style="background:#fea625;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li> -->
								<li style="background:#e84e40;"><div>维修</div></li>
							</ul>
						</div>
					</div>
						<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<c:forEach items="${list}" var="a" varStatus="i">
										<c:choose>
											<c:when test="${i.index==0}">
												<thead>
													<tr>
														<th width='112px'>${a[0] }</th>
														<c:forEach items="${a}" var="aa" begin="1" varStatus="i">
															<th><fmt:formatDate value="${aa}" pattern="HH"/></th>
														</c:forEach>												
													</tr>
												</thead>	
											</c:when>
											<c:otherwise>
												<tr>
													<td>${a[0].carNumber }</td>
													<c:forEach begin="1" items="${a }" var="s" varStatus="j">
														<c:if test="${s==null }">
															<td onclick="td_click(${i.index},${j.index})"></td>
														</c:if>
														<c:if test="${s!=null }">
															<td data-num='${s.flag}'></td>
														</c:if>
													</c:forEach>
												</tr>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
		(function(){
			$('#example2_wrapper table tr td[data-num]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-num');
				switch (data){
					case '${CarSFlag_GO}':
						$(this).css({'backgroundColor':'#738ffe','color':'#fff'});
						break;
					case '${CarSFlag_OK}':
						$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
						break;
					case '${CarSFlag_RP}':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
					case '${CarSFlag_NO}':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
				}
			})
		})();
</script>