<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
<script>
$(function(){
		if($('.daterangepicker.dropdown-menu.show-calendar.opensright').length!==0){
		$('.daterangepicker.dropdown-menu.show-calendar.opensright').remove();
	}
	$('[data-id="reservation"]').daterangepicker({timePicker: true, timePickerIncrement: 5, format: 'YYYY-MM-DD hh:mm A'});
	$('[data-id="reservation"]').change(function(){
		var time=$(this).val();
		var arr=time.split(' ');
		var hour=parseInt(arr[1].slice(0,2));
		arr.pop();
		if(time.lastIndexOf('AM')!==-1&&hour==12){
			arr[1]='0'+(hour-12)+arr[1].slice(2);
		}
		if(time.lastIndexOf('PM')!==-1){
			arr[1]=hour<12?(hour+12+arr[1].slice(2)):(arr[1]);
		}
		$(this).val(arr.join(' '));
	})
});

</script>
</head>
<body>
	<form id="detail" action="${url }" target="dialog" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
		<input type="hidden" name=id value="${repairInfo.id }" />
		<input type="hidden" name="method" value="${method}" />
		<input type="hidden" name="type" value="${type }" />
		<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
			<div class="modal-content">
				<div class="modal-body nopadding-B nopadding-T">
					<div class="row">
						<p class="col-md-12 p border-B">业务信息</p>
						<div class="col-md-12 border-B nomargin-T padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-5'>车牌号码：</label>
									<select class="list_input input-dialog" id="itemID" name="itemId">
										<option></option>
										${itemOption }
									</select>
								</div>
								<div class="col-md-12">
									<label class='lab-3'>时间：</label> 
									<input data-id="reservation" class="required list_select input_a" name="startTime" value="<fmt:formatDate value='${repairInfo.beginTime }' pattern='yyyy-MM-dd HH:mm'/>">&nbsp;<i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<label style="margin:8px;width:14px;">至</label> 
									<input data-id="reservation" class="required list_select input_a"  name="endTime" value="<fmt:formatDate value='${repairInfo.endTime }' pattern='yyyy-MM-dd HH:mm'/>">&nbsp;<i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>维修费用：</label>
									<input type="text" class="list_input input-dialog" id='fix' name='fee' value='${repairInfo.repairFee }'/>
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-5'>内部检测人：</label>
									<input type="text" class="list_input input-dialog" id='checker' name='repairName' value='${repairInfo.repairName }'/>
								</div>
								<div class="col-md-12">
									<label class='lab-1'>维修原因：</label>
									<c:if test="${type=='carInfo' }">
										<span>${repairInfo.comment }</span>
									</c:if>
									<c:if test="${type=='car' }">
										<textarea row='6' col='150' class='list_input textarea-dialog' style='height:50px' placeholder='多行输入' name="reason"></textarea>
									</c:if>
								</div>
							</div>
						</div>
						<p class="col-md-12 p border-B">办理信息</p>
						<div class="col-md-12 border-B nomargin-T padding-B">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-1'>报修日期：</label>
									<c:if test="${type=='carInfo' }">
										<span><fmt:formatDate value='${repairInfo.createTime }' pattern='yyyy-MM-dd HH:mm'/></span>
									</c:if>
									<c:if test="${type=='car' }">
										<span>系统自动取当前时间</span>
									</c:if>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>报修人员：</label>	
									<c:if test="${type=='carInfo' }">
										<span>${userName }</span>
										<label class='lab-1'>修改人员：</label>
										<span>${user.name }</span>
									</c:if>
									<c:if test="${type=='car' }">
										<span>${user.name }</span>
									</c:if>
									<input type="hidden" value="${user.userId }" name="reportId">
								</div>
							</div>
						</div>
						
						<div class="col-md-8 col-md-offset-2 btns-dialog">
							<c:if test="${type=='car' }">
								<button type="submit" class="btn btn-info btn-margin" >保存</button>
							</c:if>
							<c:if test="${type=='carInfo' }">
								<button type="button" class="btn btn-info btn-margin" onclick="click_change('确定修改吗')">确认修改 </button>
							</c:if>
								<button type='reset' class="btn btn-margin btn-color-9E8273">重置</button>
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</form>
</body>