<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">车辆调度</div>
</section>
<script type="text/javascript">
function changeDate(){
	$("#pageForm").submit();
};
function clickTd(i,k,itemId,createUserId) {
	var dialogDiv = "<div class='modal fade' id='myModal' tabindex='-1' role='dialog'></div>";
	$("#page-content").append(dialogDiv);
	if(i== ${CarSFlag_RP} ) {
		$.ajax({
			url:"repairRecord.do",
			data:{method:'info',id:k,type:"carInfo",itemId:itemId,createUserId:createUserId},
			type:"POST",
			dataType:"html",
			success:function(json){
				$("#myModal").html(json);
				$("#myModal").modal("show");
				initTab();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {  
				if(XMLHttpRequest.status==500){
					toastr["error"]("程序内部出现错误"); 
				}else if(XMLHttpRequest.status==404){
					toastr["error"]("页面不存在"); 
				}else{
					toastr["error"](XMLHttpRequest.status); 
				}
			}
		});
	} else {
		$.ajax({
			url:"${url }",
			data:{method:'info',id:k,type:"carInfo",itemId:itemId},
			type:"POST",
			dataType:"html",
			success:function(json){
				$("#myModal").html(json);
				$("#myModal").modal("show");
				initTab();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {  
				if(XMLHttpRequest.status==500){
					toastr["error"]("程序内部出现错误"); 
				}else if(XMLHttpRequest.status==404){
					toastr["error"]("页面不存在"); 
				}else{
					toastr["error"](XMLHttpRequest.status); 
				}
			}
		});
	}
}
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>日期：</label> 
							<input type="text" data-id="beginDate" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  name="date" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
					<!--	<button type="submit" class="btn btn-info" style="line-height: 15px">搜索</button> -->
						</div>
						<div class="col-md-6 btns-list">
							<a href="${url }?method=listInfo" target="homeTab" rel="myModal" id="liebiao" class="btn btn-info" role="button">进入列表</a>
							<a href='repairRecord.do?method=repair&type=car' target='dialog' rel='myModal' id='weixiu' class='btn btn-danger' role='button'>维修</a>
 							<%-- <a href="${url}?method=edit" target="homeTab" rel="myModal" id="dayin" class="btn btn-color-ab47bc" role="button">打印</a> --%> 
						</div>
						
						<div class="col-md-3">
							<ul class="la_ul">
 								<li style="background:#9ccc65;"><div>完成</div></li> 
 								<li style="background:#738ffe;"><div>进行</div></li> 
								<!-- <li style="background:#fea625;"><div>锁定</div></li>
								<li style="background:#29b6f6;"><div>占用</div></li> -->
								<li style="background:#e84e40;"><div>维修</div></li>
							</ul>
						</div>
					</div>
						<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover1">
									<c:forEach items="${list}" var="a" varStatus="i">
										<c:choose>
											<c:when test="${i.index==0}">
												<thead>
													<tr>
														<th width='112px'>${a[0] }</th>
														<c:forEach items="${a}" var="aa" begin="1" varStatus="i">
															<th><fmt:formatDate value="${aa}" pattern="HH"/></th>
														</c:forEach>												
													</tr>
												</thead>	
											</c:when>
											<c:otherwise>
												<tr>
													<td>${a[0].carNumber }</td>
													<c:forEach begin="1" items="${a }" var="s">
														<c:if test="${s==null }">
															<td></td>
														</c:if>
														<c:if test="${s!=null }">
															<td data-num='${s.flag}' style="cursor:pointer" onclick="clickTd(${s.flag},'${s.id }','${s.carId }','${s.enterId }')">${s.commissionOrder.name }</td>
														</c:if>
													</c:forEach>
												</tr>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
		(function(){
			$('#example2_wrapper table tr td[data-num]').each(function(){
				//console.log(this);
				var data=$(this).attr('data-num');
				switch (data){
					case '${CarSFlag_GO}':
						$(this).css({'backgroundColor':'#738ffe','color':'#fff'});
						break;
					case '${CarSFlag_OK}':
						$(this).css({'backgroundColor':'#9ccc65','color':'#fff'});
						break;
					case '${CarSFlag_RP}':
						$(this).css({'backgroundColor':'#e84e40','color':'#fff'});
						break;
					case '${CarSFlag_NO}':
						$(this).css({'backgroundColor':'#fea625','color':'#fff'});
						break;
				}
			})
		})();
</script>