<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
	<style>
		
		.modal-content>div.row>div.col-md-6:first-child{
			border-right:1px solid #ccc;
			
		}
		.modal-content>div.row>div.col-md-6{
			height:500px;
			overflow-y:scroll;
		}
		.modal-content>div.row>div.col-md-6:last-child{
			margin-left:-15px;
		}
		.content-header{
			padding-bottom:30px;
			border-bottom:1px solid #ccc;
		}
		.wk-btns .btn{
			padding:3px 25px;
			border-radius:8px;
		}
		.wk-btns{
			text-align:center;
			margin-top:0px;
			padding:10px 0px;
			border-top:1px solid #ccc;
		}
		.nomargin{
			margin:0px;
		}
		.panel-group,.chosed-item{
			margin-top:10px;
		}
		/****右侧选项*****/
		.item-txt{
			margin:5px auto;
			padding:0px 10px;
			height:40px;
			line-height:40px;
			width:268px;
			font-size:16px;
			border:1px solid #ddd;
			border-radius:8px;
			position:relative;
		}
		.item-del{
			position:absolute;
			right:10px;
			top:5px;
			height:28px;
			line-height:22px;
			width:28px;
			text-align:center;
			background-color:#bbb;
			color:#fff;
			border-radius:28px;
			border:1px solid #bbb;
			font-size:18px;
		}
	</style>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
				<div class="modal-content">
					<div class='row'>
						<div class='col-md-6'>
							<section class="content-header">
								<i class="title-line"></i>
								<div class="title">人员选择</div>
							</section>
							<div class='panel-group' role="tablist" id='accordion'>
								<div class='panel box box-warning'>
									<div class='panel-heading' role='tab'>
										<p class='panel-title'>
											<a role='button' data-toggle='collapse' data-parent='#accordion' href='#manager'>管理员</a>
										</p>
										
									</div>
									<div id='manager' class='panel-collapse collapse in'>
										<div class='panel-body'>
											<div class='checkbox'><label><input type='checkbox' name='namager' value='1'/>管理员1</label></div>
											<div class='checkbox'><label><input type='checkbox' name='namager' value='2'/>管理员2</label></div>
											<div class='checkbox'><label><input type='checkbox' name='namager' value='3'/>管理员3</label></div>
										</div>
									</div>
								</div>
								<div class='panel box box-warning'>
									<div class='panel-heading' role='tab'>
										<p class='panel-title'>
											<a role='button' data-toggle='collapse' data-parent='#accordion' href='#worker'>员工</a>
										</p>
										
									</div>
									<div id='worker' class='panel-collapse collapse'>
										<div class='panel-body'>
											<div class='checkbox'><label><input type='checkbox' name='namager' value='4'/>员工1</label></div>
											<div class='checkbox'><label><input type='checkbox' name='namager' value='5'/>员工2</label></div>
											<div class='checkbox'><label><input type='checkbox' name='namager' value='6'/>员工3</label></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class='col-md-6'>
							<section class="content-header">
								<i class="title-line"></i>
								<div class="title">已选项目</div>
							</section>
							<div class='chosed-item'>
								<div class='box box-warning' id='chosed'>
<!-- 									<p class='item-txt'>111111111<span class='item-del'>x</span></p> -->
								</div>
							</div>
						</div>
					</div>
					<div class='row nomargin'>
						<div class='col-md-12 wk-btns'>
							<button type="submit"  class="btn btn-info">确定</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
						</div>
					</div>
				
				</div>
			</div>
				
	</form>
<script>
	(function(){
		$('.panel-group input:checkbox').click(function(){
			if($(this).is(':checked')){
				$('#chosed').append("<p class='item-txt' data-value='"+$(this).val()+"'>"+$(this).parent().text()+"<span class='item-del'>x</span></p>")
			}else{
				$("#chosed p[data-value='"+$(this).val()+"']").remove();
			}
		})
		$('#chosed').on('click','.item-del',function(){
			$(".panel-group input[value='"+$(this).parent().attr('data-value')+"']").removeAttr('checked');
			$(this).parent().remove();
		})
	})()
</script>	
</body>