<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<!-- -查看丧葬用品- -->
	<form id="detail" action="first_department.do" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
			
			<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<%@ include file="/common/pageHead.jsp"%>
					<!--startprint0-->
			<div class="modal-content">
				<div class="modal-body nopadding-T nopadding-B">
					<div class="row">
						<div class="col-md-12 padding-B border-B nopadding-T">
							<div class="row">
								<div class="col-md-6 height-align">
									<label class='lab-2'>申请日期：</label>
									<span><fmt:formatDate value="${hardReduction.createTime }" pattern="yyyy-MM-dd HH:mm"/></span>
								</div>
								<div class="col-md-6 height-align">
									<label class='label-4'>卡号：</label>
									<span>${hardReduction.commissionOrder.cardCode }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>申请人姓名：</label>
									<span>${hardReduction.applicant}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>死者姓名：</label>
									<span>${hardReduction.commissionOrder.name }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>联系电话：</label>
									<span>${hardReduction.phone }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>死者性别：</label>
									<span>${hardReduction.commissionOrder.sexName }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>与死者关系：</label>
									<span>${hardReduction.commissionOrder.appellation.name }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>死者年龄：</label>
									<span>${hardReduction.commissionOrder.age }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>证件类型：</label>
									<c:forEach items="${proveName }" var="u">
										<span>${u}</span>&nbsp; &nbsp;
									</c:forEach>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者身份证：</label>
									<span>${hardReduction.commissionOrder.certificateCode }</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>其他证明：</label>
									<span>${hardReduction.comment}</span>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-2'>特殊减免：</label>
									<c:choose>
										<c:when test="${hardReduction.special ==1 }">
											<span>是</span>
										</c:when>
										<c:otherwise>
											<span>否</span>
										</c:otherwise>
									</c:choose>
									
								</div>
								<div class="col-md-12">
									<label class='lab-2'>证明单位：</label>
									<span>${proveUnitName}</span>
								</div>
								
								<div class="col-md-12">
									<label class='lab-2'>申请理由：</label>
									<span>${hardReduction.reason }</span>
								</div>
							</div>							
						</div>
						<!--endprint0-->
						<!--startprint1-->
						<div class="col-md-12 padding-B border-B nopadding-T">
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>名称</th>
												<th>单价（元）</th>
												<th>数量</th>
												<th>总价</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody>
											<c:choose>
												<c:when test="${fn:length(ReductionDList)==0 }">
													 <tr width="20">
													    <td colspan="5">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
											<c:otherwise>
												<c:forEach items="${ReductionDList }" var="u">
													<tr>
														<td>${u.name }</td>
														<td><fmt:formatNumber value="${u.pice }" pattern="#" type="number"/></td>
														<td>${u.number }</td>
														<td><fmt:formatNumber value="${u.total }" pattern="#" type="number"/></td>
														<td>${u.comment }</td>
													</tr>												
												</c:forEach>
											</c:otherwise>
										  </c:choose>
											<tr>
												<td>小计</td>
												<td colspan="5" style="text-align:left">
													<span><fmt:formatNumber value="${allTotal }" pattern="#" type="number"/>元</span>
												</td>
											</tr>
										</tbody>
									</table>
									<div>
										<label class='lab-6'>备注：</label>
										<span>${hardReduction.commissionOrder.comment }</span>
									</div>
								</div>
							</div>
						</div>
						<!--endprint1-->
						
<!-- 						<div class="col-md-12"> -->
<!-- 							<div class="row"> -->
<!-- 								<div class="col-md-12"> -->
<!-- 									<label>业务部门意见：</label> -->
<!-- 									<span>111111111111111</span> -->
<!-- 								</div> -->
<!-- 								<div class="col-md-12"> -->
<!-- 									<label>主管领导审批：</label> -->
<!-- 									<span>111111111111111</span> -->
<!-- 								</div> -->
<!-- 							</div> -->
							
<!-- 						</div> -->
						<div class="col-md-12 btns-dialog btns-print">
							<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
							<a href="javascript:showprint()" class="btn  btn-color-ab47bc  btn-margin">打印</a>
						</div>
						<!--startprint2-->
					</div>
				</div>
					<!--endprint2-->
				
				
				
			</div>
		</div>
				
	</form>
	
</body>