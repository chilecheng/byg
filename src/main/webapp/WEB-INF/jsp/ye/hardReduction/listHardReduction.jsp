<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">困难群众救助减免</div>
</section>
<style type="text/css">
	.main-content{
		background-color:#fff;
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
		border-radius:6px;
	}
	.table td a.light{
		width:50px;
		height:20px;
		/* background-color:lightblue; */
		text-align:center;
		line-height:20px;
		color:#fff;
		border-radius:4px;
	}
	.table td a.light.right{
		background-color:#ab47bc;
	}
	/*标签页*/
	.nav.nav-tabs>li{
		width:20%;
		text-align:center;
	}
	.nav.nav-tabs>li.active{
		border-top:3px solid #29B6F6;
		border-radius:4px; 
	}
	.width-s{
		padding:10px 25px;
	}
</style>
<form action="hardReduction.do" id="pageForm" onsubmit="return homeSearch(this);">
	<%@ include file="/common/pageHead2.jsp"%>
	<input name="method" value="list" type="hidden">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title" style="color:#4EC2F6">查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					
						
						<div class="box-body">
							<div class="col-md-3">
								<label>类型：</label> 
								<select class="list_input nopadding-R" id="find" name="find">
<!-- 									<option value='findByCode'>卡号</option> -->
<!-- 									<option value='findByName'>姓名</option> -->
									${searchOption }
								</select>
							</div>
							<div class="col-md-8">
								<label>查询值：</label> 
								<input type="text" class="list_input input-hometab" id='search' name='search' value='${search }' placeholder='单行输入'/>
							</div>
							<div class="col-md-12">
								<label>申请时间：</label> 
								<input type="text" data-id='beginDate' class="list_select" id="startTime" name="startTime" value="${startTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span>  
								<input type="text" data-id='endDate' class="list_select" id="endTime" name="endTime" value="${endTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
<%-- 								<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd hh:ii:ss" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${endTime }" pattern="yyyy-MM-dd HH:mm:ss"/>">								 --%>
							</div>
							<!-- 控制常用搜索状态（已审核等） -->
							<input type="hidden" id="checkType" name="checkType" value="${checkType }">
							<div class="col-md-12" id="typeDiv">
								<label>常用：</label>
								
								<button type="submit" class="btn btn-normally btn-all" name="checkAll">全部</button>
								<button type="submit" class="btn btn-normally" name="checkNo">未审核</button>
								<button type="submit" class="btn btn-normally" name="checkYes">已审核</button>
								<button type="submit" class="btn btn-normally" name="checkEsc">被取消</button>
								<button type="submit" class="btn btn-normally" name="checkSpecial">特殊减免</button>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
						</div>
					
				</div>
				<!-- /.col -->
			</div>
		</div>
	</section>
	<section class='content nomargin-T'>
		<div class='row'>
			<div class='col-md-12'>
				<div class='main-content'>
					<!-- 控制当前的分页（待办事宜等） -->
					<input type="hidden" id="clickId" name="clickId" value="qicao">
					<ul class='nav nav-tabs' id='liClick'>
						<li role="presentation" class="active"><a href="#qicao" aria-controls="qicao" role="tab" data-toggle="tab">我的起草</a></li>
						<li role="presentation"><a href="#wait" aria-controls="wait" role="tab" data-toggle="tab">待办事宜</a></li>
						<li role="presentation" ><a href="#shenpi" aria-controls="shenpi" role="tab" data-toggle="tab">我的审批</a></li>
						<li role="presentation" ><a href="#lookUp" aria-controls="lookUp" role="tab" data-toggle="tab">查阅事宜</a></li>
					</ul>
					<div class='tab-content'>
						<div role="tabpanel" class="tab-pane active" id="qicao">
							<div class="row">
								<div class='col-md-12'>
									<small class='btns-charge btns-charge-right'>
										<a class='btn btn-warning' href='${url}?method=add' target='homeTab' rel="myModal"  role="button">申请</a>
										<a onclick='desc(dialogAjaxDone1,"descId")' checkname="descId" rel="myModal"  class="btn btn-danger" role="button">删除</a>
									</small>
								</div>
								<div class='col-md-12 width-s'>
									<table class="table table-bordered">
											<thead>
												<tr>
													<th width='70px;'>
														  <div class="form-group">
														      <div class="checkbox">
														        <label>
														         	<input type="checkbox" group='descId' class='checkBoxCtrl'>全选 
														        </label>
														      </div>
														  </div>
													</th>
													<th>申请人姓名</th>
													<th>死者姓名</th>
													<th>死者年龄</th>
													<th>身份证号</th>
													<th>死亡日期</th>
													<th>联系电话</th>
													<th>申请时间</th>
													<th>办理状态</th>
												</tr>
											</thead>
											<tbody>
											<c:choose>
												<c:when test="${fn:length(page.list)==0 }">
													 <tr width="20">
													    <td colspan="9">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
											<c:otherwise>
												<c:forEach items="${page.list }" var="u">
														<tr>
															<td><input type='checkbox' class='checkBoxCtrl' name='descId' value='${u.id }'/></td>
															<td>${u.applicant }</td>
															<td><a href="${url}?method=show&id=${u.id}" class="loser" target="dialog" rel="myModal">${u.commissionOrder.name }</a></td>
															<td>${u.commissionOrder.age }</td>
															<td>${u.commissionOrder.certificateCode }</td>
															<td><fmt:formatDate value="${u.commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td>${u.phone }</td>
															<td><fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td class='changeColor state1' ><c:if test="${u.checkFlag ==2 }">未审核</c:if>
															<c:if test="${u.checkFlag ==1 }">已审核</c:if><c:if test="${u.checkFlag ==3 }">被取消</c:if>
															
															</td>													
															
														</tr>
												
												</c:forEach>
											</c:otherwise>
										  </c:choose>
												
											</tbody>
										</table>
										<%@ include file="/common/pageFood1.jsp"%>
								</div>
							</div>
						</div>
			    		<div role="tabpanel" class="tab-pane" id="wait">
			    			<div class="row">
								<div class='col-md-12 btns'>
									<c:if test="${isShenP eq 'yes' }">
										<small class='btns-charge btns-charge-right'>
											<a class='btn btn-info' onclick='pose(dialogAjaxDone1)' target='ajaxTodo' rel="myModal"   role="button">审批</a>
											<a onclick='desc(dialogAjaxDone1,"checkUp")' checkname="checkUp"  rel="myModal"  class="btn btn-danger" role="button">不通过</a>
										</small>
									</c:if>
								</div>
								<div class='col-md-12 width-s'>
									<table class="table table-bordered">
											<thead>
												<tr>
													<th width='70px;'>
														  <div class="form-group">
														      <div class="checkbox">
														        <label>
														         	<input type="checkbox" group='checkUp'  class='checkBoxCtrl'>全选 
														        </label>
														      </div>
														  </div>
													</th>
													<th>申请人姓名</th>
													<th>死者姓名</th>
													<th>死者年龄</th>
													<th>身份证号</th>
													<th>死亡日期</th>
													<th>联系电话</th>
													<th>申请时间</th>
													<th>办理状态</th>
												</tr>
											</thead>
											<tbody>
											<c:choose>
												<c:when test="${fn:length(upcoming.list)==0 }">
													 <tr width="20">
													    <td colspan="9">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
											<c:otherwise>
												<c:forEach items="${upcoming.list }" var="u">
														<tr>
															<td><input type='checkbox' class='checkBoxCtrl' name='checkUp' value='${u.id }'/></td>
															<td>${u.applicant }</td>
															<td><a href="${url}?method=show&id=${u.id}" class="loser" target="dialog" rel="myModal">${u.commissionOrder.name }</a></td>
															<td>${u.commissionOrder.age }</td>
															<td>${u.commissionOrder.certificateCode }</td>
															<td><fmt:formatDate value="${u.commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td>${u.commissionOrder.fPhone }</td>
															<td><fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td class='changeColor state1' ><c:if test="${u.checkFlag ==2 }">未审核</c:if>
															<c:if test="${u.checkFlag ==1 }">已审核</c:if><c:if test="${u.checkFlag ==3 }">被取消</c:if>
															</td>			
														</tr>												
													</c:forEach>
												</c:otherwise>
											  </c:choose>
												
											</tbody>
										</table>
										<%@ include file="/common/pageFood2.jsp"%>
								</div>
							</div>
			    		</div>
			    		<div role="tabpanel" class="tab-pane" id="shenpi">
			    			<div class="row">
								<div class='col-md-12 width-s'>
									<table class="table table-bordered">
											<thead>
												<tr>
													<th width='70px;'>
														  <div class="form-group">
														      <div class="checkbox">
														        <label>
														         	<input type="checkbox" group='id' class='checkBoxCtrl'>全选 
														        </label>
														      </div>
														  </div>
													</th>
													<th>申请人姓名</th>
													<th>死者姓名</th>
													<th>死者年龄</th>
													<th>身份证号</th>
													<th>死亡日期</th>
													<th>联系电话</th>
													<th>申请时间</th>
													<th>办理状态</th>
												</tr>
											</thead>
											<tbody>
											<c:choose>
												<c:when test="${fn:length(myList.list)==0 }">
													 <tr width="20">
													    <td colspan="9">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
											<c:otherwise>
												<c:forEach items="${myList.list }" var="u">
														<tr>
															<td><input type='checkbox' class='checkBoxCtrl' name='id' value='${u.id }'/></td>
															<td>${u.applicant }</td>
															<td><a href="${url}?method=show&id=${u.id}" class="loser" target="dialog" rel="myModal">${u.commissionOrder.name }</a></td>
															<td>${u.commissionOrder.age }</td>
															<td>${u.commissionOrder.certificateCode }</td>
															<td><fmt:formatDate value="${u.commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td>${u.commissionOrder.fPhone }</td>
															<td><fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td class='changeColor state1'><c:if test="${u.checkFlag ==2 }">未审核</c:if>
															<c:if test="${u.checkFlag ==1 }">已审核</c:if><c:if test="${u.checkFlag ==3 }">被取消</c:if>
															</td>		
														</tr>												
													</c:forEach>
												</c:otherwise>
											  </c:choose>
												
											</tbody>
										</table>
										<%@ include file="/common/pageFood3.jsp"%>
								</div>
							</div>
			    		</div>
			    		<div role="tabpanel" class="tab-pane" id="lookUp">
			    			<div class="row">
								<div class='col-md-12 width-s'>
									<table class="table table-bordered">
											<thead>
												<tr>
													<th width='70px;'>
														  <div class="form-group">
														      <div class="checkbox">
														        <label>
														         	<input type="checkbox" group='id' class='checkBoxCtrl'>全选 
														        </label>
														      </div>
														  </div>
													</th>
													<th>申请人姓名</th>
													<th>死者姓名</th>
													<th>死者年龄</th>
													<th>身份证号</th>
													<th>死亡日期</th>
													<th>联系电话</th>
													<th>申请时间</th>
													<th>办理状态</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody>
					 						<c:choose>
												<c:when test="${fn:length(allList.list)==0 }">
													 <tr width="20">
													    <td colspan="10">
															<font color="Red">没有数据</font>
														 </td>
													</tr>
												</c:when>
											<c:otherwise>
												<c:forEach items="${allList.list }" var="u">
														<tr>
															<td><input type='checkbox' class='checkBoxCtrl' name='id' value='${u.id }'/></td>
															<td>${u.applicant }</td>
															<td><a href="${url}?method=show&id=${u.id}" class="loser" target="dialog" rel="myModal">${u.commissionOrder.name }</a></td>
															<td>${u.commissionOrder.age }</td>
															<td>${u.commissionOrder.certificateCode }</td>
															<td><fmt:formatDate value="${u.commissionOrder.dTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td>${u.commissionOrder.fPhone }</td>
															<td><fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd HH:mm"/></td>
															<td class='changeColor state1' ><c:if test="${u.checkFlag ==2 }">未审核</c:if>
															<c:if test="${u.checkFlag ==1 }">已审核</c:if><c:if test="${u.checkFlag ==3 }">被取消</c:if>
															</td>
															<td><a href="${url}?method=show&id=${u.id}" class='light right' target="dialog" rel="myModal">打印</a>
															</td>			
														</tr>												
													</c:forEach>
												</c:otherwise>
											  </c:choose>
												
											</tbody>
										</table>
										<%@ include file="/common/pageFood4.jsp"%>
								</div>
							</div>
			    		</div>
					</div>
				</div>
			</div>
		
		</div>
		
</section>
</form>
<script type="text/javascript">
//当前页状态改变，用于后台判断，页面刷新之后跳转到当前页
var type=active.parsent;
$("#clickId").val(type);
$('[aria-controls="'+type+'"]').click();
$("#liClick").on('click','li a',function(){
	type=$(this).attr('aria-controls');
	active.parsent=type;
	$("#clickId").val(type);
// 	alert($("#clickId").val());
})
//将点击的常用搜索按钮对应值改变，用于后台判断
var checkType='';
$("#typeDiv").on('click','button',function(){	
	checkType=$(this).attr('name');
	$("#checkType").val(checkType);
})
$(function(){
	timeRange();
});
$('#search').change(function(){
	if($("#find").find("option:selected").text()=="卡号"){
		homeSearch(pageForm);		
	}
})
//通过审批
function pose(callback) {
	 var num = 0;
	 var allPoseIds=[];
	 $("input[name='checkUp']:checked").each(function(){
			var id = $(this).attr("value")
			allPoseIds.push(id);
			num++;
		});	
	 if(num>=1){
	    $.ajax({
	    	url:'hardReduction.do',
	    	type:'post',
	    	traditional: true,
	    	data:{method:'pose',allPoseIds:[allPoseIds]},
	    	dataType:'json',
	    	success:callback
	    });		 
	 }
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
//取消申请通过
function desc(callback,checkname) {
	 var num = 0;
	 var allDescIds=[];
	 $("input[name='"+checkname+"']:checked").each(function(){
			var id = $(this).attr("value")
			allDescIds.push(id);
			num++;
		});
	 if(num>=1){
	    $.ajax({
	    	url:'hardReduction.do',
	    	type:'post',
	    	traditional: true,
	    	data:{method:'desc',allDescIds:[allDescIds]},
	    	dataType:'json',
	    	success:callback
	    });		 
	 }	 
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
(function(){//修改状态样式
	$('.changeColor').each(function(){
		var txt=$(this).text().trim();
		txt==='已审核'&&$(this).css('color','#9CCC65');
		txt==='被取消'&&$(this).css('color','#e84e40');
		txt==='未审核'&&$(this).css('color','#0000FF');			
	})
})()
</script>