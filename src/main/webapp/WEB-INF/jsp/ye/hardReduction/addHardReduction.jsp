<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">困难群众救助减免</div>
</section>
<script type="text/javascript">

var sum=0;//标识
var sTotal=0;//减免项目合计总金额
var delNumber=[]//删除记录


//保存数据 
function saveDate(callback){	
	if(!$('#certify').val()){
		toastr["warning"]("请选择证明单位");
		return false;
	}
	if(!$('#dName').val()){
		toastr["warning"]("请填写死者姓名");
		return false;
	}
	
	var dNameId=$("#dNameId").val();
	var dName=$("#dName").val();
	var phone=$("#phone").val();
	var connect=$("#connect").val();
	var other=$("#other").val();
	var spscial=$("#spscial").val();
	var certify=$("#certify option:selected").val();
	var proveUnitContent=$("#certify option:selected").text().trim();
	var reason=$("#reason").val();
	var remark=$("#remark").val();
	var fName=$("#fName").val();
	var creatTime=$("#creatTime").val();
	var identifyType = "";
	$("input[name='identifyType']:checked").each(function(){
		var id = $(this).attr("value")
		identifyType+=id+",";
	});
	//减免表格数据
	var Table = document.getElementById("deductions");
	var saveOut=[];
	for(var i=0;i<Table.rows.length;i++){
		var inner=[];
		var allString="";
		for(var j=0;j<Table.rows[i].cells.length;j++){
			//找到所有表格里的元素
			var val=$(Table.rows[i].cells[j].children[0]).val();
			allString=allString+val+",";			
		}
		allString=allString+$("#id").val();
		
		saveOut[i]=allString;
	}
	
	$.ajax({
		url:"hardReduction.do",
		traditional: true,
		data:{"method":"save",identifyType:identifyType,dName:dName,phone:phone,connect:connect,
			other:other,spscial:spscial,certify:certify,reason:reason,remark:remark,
			sTotal:sTotal,fName:fName,creatTime:creatTime,saveData:saveOut,dNameId:dNameId,
			proveUnitContent:proveUnitContent
			},
		type:"post",
		dataType:"json",
		success:callback		
	});
}

//减免项目方法
function item(name,pice,number,total,comment,baseId){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='checkbox' id=checkbox_"+baseId+" name='checkId' value="+baseId+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' onchange=itemName('"+baseId+"') name='itemId' id='itemId_"+baseId+"'> "+name+"</select></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' readonly='readonly' class='list_table' id='pice_"+baseId+"' name='pice' value="+pice+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='text' class='list_table' onkeyup=changeNumber('"+baseId+"') onkeydown='if(event.keyCode==13) return false' id='number_"+baseId+"' name='number' value="+number+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '><input type='text' readonly='readonly' name='total' id=total_"+baseId+" class='list_table' value="+total+"></td>";
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<textarea type='text' class='list_table' id='comment_"+baseId+"'  name='comment'>"+comment+"</textarea></td></tr>";
	$("#deductions").append(str);
}
$(document).ready(function(){
	//初始化项目
	<c:forEach var="u" items='${hardlist}'>	
		sum++;
		item("${u[1]}","${u[3]}","${u[2]}","${u[3]}","",sum);
		sTotal+=parseFloat('${u[3]}');
		$("#sFont").html(sTotal+"元");
	</c:forEach>	
});

//增加收费记录
function addRow(){
	//sum 作为增加记录的暂时ID来区别		
	sum++;
	item("${defaultAdd.defaultName}","${defaultAdd.defaultPice}",1,"${defaultAdd.defaultPice}","",sum)
	sTotal+=parseFloat('${defaultAdd.defaultPice}')
	$("#sFont").html(sTotal+"元");
}

//项目名称改变
function itemName(id){
	$.ajax({
	    url: "freeBasicServices.do",
	    dataType:'json',
	    cache: false,
	    data:{method:'itemNameChange',id:$("#itemId_"+id).val()},
	    success: function (json) {
    		sTotal=sTotal-$("#total_"+id).val();
    		sTotal+=json.pice;
			$("#sFont").html(sTotal+"元");
	    	$("#total_"+id).val(json.pice);
    		$("#pice_"+id).val(json.pice);
	    	$("#number_"+id).val(1);
		},
	   error: function (e) {
	      alert("错误"+e);
	   }
	});
} 

//数目改变
function changeNumber(id){
	var number=$("#number_"+id).val();
	var a=parseFloat(number);
	var pice=$("#pice_"+id).val();
			sTotal=sTotal-$("#total_"+id).val();
			sTotal+=pice*number;
			$("#sFont").html(sTotal+"元");
			$("#total_"+id).val(pice*number);
	
} 

//删除内容
function delHtml() {
	var num = 0;	
	$("input[name='checkId']:checked").each(function(){
		var id = $(this).attr("value")
			//此处若删除的记录条ID为临时ID，不作变动，若是数据库中ID，删除时记录，方便在后台删除数据库中的记录
			if(id.length>5){
				delNumber.push(id);
			};			
			sTotal=sTotal-$("#total_"+id).val();
			$("#sFont").html(sTotal+"元");
		$(this).parent().parent().remove();
		sum--;
		num++;
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}
}
$(function(){
	timeMunite();
});

</script>
<form action="${url}" id="pageForm" onsubmit="return homeSearch(this);">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<%@ include file="/common/pageHead.jsp"%>
						<input name="method" value="list" type="hidden">
						<div class="box-body">
							<div class="row padding-B">
								<div class="col-md-6 height-align">
									<label class='lab-1'>申请日期：</label> 
<%-- 									<input type="text" data-provide="datetimepicker"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="applyTime" name="applyTime" value="${creatTime }"> --%>
								<input data-id="reservation" readOnly='readOnly' class="list_select" id="creatTime" name="creatTime" value="<fmt:formatDate value="${creatTime}" pattern="yyyy-MM-dd HH:mm"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">

									<label class='lab-1'>死者姓名：</label> 

									<input type="text" class="list_select" id='dName' name='dName' value=''/>
									<button class='btn-psearch btn btn-info' type='button' data-name='search'>搜索</button>
									<input type="text" readonly class="list_input" id='dNameId' data-id="dNameId" name='dNameId' hidden value=''/>
									<a data-dialog='dialog' class='hide' target='dialog' href='${url}?method=dNameList'>选择</a>
									<span class='nowarning' data-warning='warning'></span>
								</div>
								
								<div class="col-md-6 height-align">
									<label class='lab-6'>申请人姓名：</label>
									<input type="text" class="list_select" id='fName' name='fName' />
								</div>
								<div class="col-md-6 height-align input-menu">
									<label class='lab-3'>卡号：</label>
									<input  type="text" name="code" class="list_select" id="code" />
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>联系电话：</label>
									<input type="text" class="list_select" id='phone' name='phone' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者性别：</label> 
									<input type="text" class="list_select" id='gender' name='gender' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>与死者关系：</label> 
									<select class="list_select nopadding-R" style='width:171px' id="connect" name="connect">
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>死者年龄：</label> 
									<input type="text" class="list_select" ' id='age' name='age' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>证件类型：</label> 
									<c:forEach items="${certificateList }" var="u">
										<input type='checkbox' name='identifyType' value='${u.certificateId }'/>${u.name }
									</c:forEach>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-6'>死者身份证：</label> 
									<input type="text" class="list_select" id='loserID' name='loserID' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>其他证明：</label> 
									<input type="text" class="list_select" id='other' name='other' value=''/>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>特殊减免：</label> 
									<select class="list_select nopadding-R"  style='width:171px' id="spscial" name="spscial">
										<option value="1">是</option>
										<option value="2">否</option>
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class='lab-1'>证明单位：</label>
									<select class="list_select nopadding-R"  style='width:171px' id="certify" name="certify">
										${proveOption }
									</select>&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-12">
									<label class='lab-1'>申请理由：</label> 
									<textarea class='list_input textarea-hometab' rows="5"  name="reason" id="reason" cols="150"></textarea>									
								</div>
							</div>
							<div class="row border-B padding-B">
								<div class="col-md-12">
									<small class='pull-right btns-charge'>
<!-- 										<button type="button" onclick="addRow()" class="btn btn-warning">添加</button>										 -->
										<a href="${url}?method=insert" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
										<button type="button" onclick="delHtml()" class="btn btn-danger">删除</button>
									</small>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="padding-right:15px;">
									<table class="table table-bordered" style="margin-top: 5px;">
										<thead>
											<tr>
												<th width="30px">全选<input type="checkbox"	class="checkBoxCtrl" group="checkId" /></th>
												<th width="230px">名称</th>
												<th width="100px">单价（元）</th>
												<th width="80px">数量</th>
												<th width="100px">小计</th>
												<th>备注</th>
											</tr>
										</thead>
										<tbody id="deductions" >

										</tbody>
										<tbody>
											<tr>
												<td style='width: 60px'>合计：</td>
												<td colspan='6' style='text-align: left;'>
												<font color='red' id="sFont"> 0元 </font></td>
											</tr>
										</tbody>
									</table>
									
								</div>
							</div>
							<div class='row padding-B'>
								<div class="col-md-12" style="padding-left:15px;">
									<div class="row">
										<span class="col-md-1">备注：</span> 
										<textarea class="col-md-11" style="margin-left:-15px;" rows="5" cols="150" name="remark" id="remark" placeholder="多行输入"></textarea>
									</div>									
								</div>
<!-- 								<div class="col-md-12"> -->
<!-- 									<label>业务部门意见：</label>  -->
<!-- 									<input type="text" class="list_input_c" id='yewu' name='yewu' value=''/> -->
<!-- 								</div> -->
<!-- 								<div class="col-md-12"> -->
<!-- 									<label>主管领导审批：</label>  -->
<!-- 									<input type="text" class="list_input_c" id='mainCharge' name='mainCharge' value=''/> -->
<!-- 								</div> -->
							</div>
						</div>
					
					
				</div>
				
			</div>
		</div>
	</section>
	<section class="content" id='main-content'>
		<div class='box'>
			<div class="row  padding-B">
				<div class="col-md-6 btns-print">
<!-- 					<a href="" class="btn btn-color-ab47bc" role="button">打印</a> -->
				</div>
				<div class="col-md-6">
<!-- 					<button type='button' class="btn btn-default">返回</button> -->
<!-- 					<button type='reset' class="btn btn-default" style='background-color:#9E8273;'>重置</button> -->
<!-- 					<button type="submit" class="btn btn-info" >保存</button> -->
					<small class='btns-hometab pull-right'>
						<a href="javascript:void(0);" onclick="saveDate(homeAjaxDone)" class="btn btn-info" role="button">保存</a>	
						<a href="${url}?method=add" target="homeTab" checkOne="true" rel="myModal" class="btn btn-color-9E8273" role="button">重置</a>					
						<a href="${url}?method=list" type="button"  target="homeTab" class="btn btn-default btn-right">返回</a>
						
						
					</small>
				</div>
			</div>
		</div>
	</section>
</form>
<script>
(function(){
		
	var href=$('[data-dialog="dialog"]').attr('href');
		$('[data-name="search"]').click(function(){
			var dName = $("#dName").val().trim();
			if(dName==""){
				$('[data-warning="warning"]').html('请输入死者姓名').removeClass('nowarning').addClass('warning');
				return;
			}else{
				var $form = $(pageForm);
				$.ajax({
				    url: $form.attr("action"),
				    dataType:'json',
				    cache: false,
				    data:{method:'dNameSearch',dName:dName},
				    success: function (json) {
				    	var size = json.size;
 				   	if(size===0){
 		 				$('[data-warning="warning"]').html('无死者信息').removeClass('nowarning').addClass('warning');
 		 			}else if(size===1){
 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
 		 				$("#code").val(typeof(json.cardCode)!="undefined"?json.cardCode:'');
 		 				$("#dName").val(typeof(json.dName)!="undefined"?json.dName:'');
 		 				$("#dNameId").val(typeof(json.dNameId)!="undefined"?json.dNameId:'');
               			$("#gender").val(typeof(json.sex)!="undefined"?json.sex:'');
               			$("#age").val(typeof(json.age)!="undefined"?json.age:'');
               			$("#fName").val(typeof(json.fName)!="undefined"?json.fName:'');
               			$("#phone").val(typeof(json.fPhone)!="undefined"?json.fPhone:'');
               			$("#connect").html(typeof(json.fAppellationOption)!="undefined"?json.fAppellationOption:'');
               			$("#loserID").val(typeof(json.dieId)!="undefined"?json.dieId:'');
               			$("#certify").val(json.proveUnitId);
               			 		 				
 		 			}else{
 		 				$('[data-warning="warning"]').removeClass('warning').addClass('nowarning');
 		 				$('[data-dialog="dialog"]').attr('href',href+"&dName="+dName);
 		 				$('[data-dialog="dialog"]').click();
 		 			}
 				    	
 				}
				});
			}
		});
	})()

</script>