<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">补卡</div>
</section>
<script type="text/javascript">
function changeDate(){
	$("#Form").submit();
}
</script>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title" style="color:#4EC2F6">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
				    <%@ include file="/common/pageHead.jsp"%>
<!-- 					<input type="hidden" id="Size" name="Size" value="10">
					<input type="hidden" id="Num" name="Num" value="1">
					<input type="hidden" id="Order" name="Order" value=""> -->
					
					<div class="box-body">
						<div class="col-md-4 height-align">
							<label class='lab-6'>死者姓名：</label> 
							<input type="text" class="list_input" id='deadName' name='deadName' value='${deadName}' placeholder='输入死者姓名'/>
						</div>
						<div class="col-md-4 height-align">
							<label class='lab-6'>身份证号：</label> 
							<input type="text" class="list_input " id='dieID' name='dieID' value='${dieID}' placeholder='输入身份证号'/>
						</div>
						<div class="col-md-4 height-align">
							<label class='lab-6'>联系人姓名：</label> 
							<input type="text" class="list_input" id='linkName' name='linkName' value='${linkName}' placeholder='输入联系人姓名'/>
						</div>
						<div class="col-md-4 height-align">
							<label class='lab-6'>到馆时间：</label> 
							<input type="text" data-id="beginDate" onchange="changeDate()" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_input" id="toTime" name="toTime" value="${toTime}" ><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						</div>
						<div class="col-md-3 btns-dialog">
							<button type="submit" class="btn btn-info btn-search" style='margin-left:-130px;'>搜索</button>
<!-- 							<button type="reset" class="btn btn-color-9E8273 btn-search" style='margin-left:15px;'>重置</button> -->
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
</section>
<section class="content" id='main-content' style="padding-top:0px;">
	<div class='box'>
	
		<div class="row row-width">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
					<small class='btns-buy'>
						<a href="buka.do?method=edit&id=" checkone="true" checkname="id" target="dialog" rel="myModal" class="btn btn-color-ff7043" role="button">补卡</a>
					
					</small>
					
					</div>
				</div>
				<table class="table table-bordered margin-T">
					<thead>
						<tr>
							<th>
								  <div class="form-group nomargin-B">
								      <div class="checkbox nomargin-B  nomargin-T">
								        
								      </div>
								  </div>
							</th>
							<th>死者姓名</th>
							<th>卡号</th>
							<th>死者性别</th>
							<th>年龄</th>
							<th>到馆时间</th>
							<th>接尸地址</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
						    <c:when test="${fn:length(page.list)==0 }">
							    <tr>
					    			<td colspan="7">
								  		<font color="Red">还没有数据</font>
							 		</td>
							    </tr>
						    </c:when>
						    <c:otherwise>
						    	<c:forEach items="${page.list }" var="u">
									<tr role="row">
										<td><input type="radio" class="checkBoxCtrl" name="id"  value="${u.id}"/></td>
										<td><a  class="loser" >${u.name}</a></td>
										<td>${u.cardCode}</td>
										<td>${u.sexName}</td>
										<td>${u.age}</td>
										<td><fmt:formatDate value="${u.arriveTime}" pattern="yyyy-MM-dd HH:mm"/></td>
										<td>${u.pickAddr}</td>
									</tr>
								</c:forEach>
						  	</c:otherwise>
						  </c:choose>
					</tbody>
				</table>
				<%@ include file="/common/pageFood.jsp"%>
			</div>
		</div>
	</div>
</section>
<script>
	(function(){
		$('table.table.table-bordered tbody tr:odd').css('backgroundColor','#efefef');
	})()
</script>