<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<div class="modal-dialog" role="document">
	<form action="${url}?method=save&ids=" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${fillCardRecord.id}">
		<input type="hidden" name="cid" value="${commissionOrder.id}">
		<div class="modal-content">
			<p class='p border-B'>办卡信息</p>
			<div class="modal-body border-B padding-B" >
				<div class="row">
					<div class="col-md-6 height-align">
						<label class='lab-2'>死者姓名：</label>
						<span>${commissionOrder.name}</span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-3'>业务员：</label>
						<span>${u.name}</span><!-- code不是密码，password才是？（密文保存）-->
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-2'>死者年龄：</label>
						<span>${commissionOrder.age}</span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-2'>办理时间：</label>
						<span><fmt:formatDate value="${commissionOrder.creatTime}" pattern="yyyy-MM-dd HH:mm"/></span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-2'>死者性别：</label>
						<span>${commissionOrder.sexName}</span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-3'>原卡号：</label>
						<span>${commissionOrder.cardCode}</span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-2'>到馆时间：</label>
						<span><fmt:formatDate value="${commissionOrder.arriveTime}" pattern="yyyy-MM-dd HH:mm"/></span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-3'>新卡号：</label>
						<input type="text" class="list_input input-dialog" id='newCard' name='newCard' value=''/>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-2'>死者编号：</label>
						<span>${commissionOrder.code}</span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-3'>办理人：</label>
						<input type="text" class="list_input input-dialog" id='transactor' name='transactor' value=''/>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-2'>接尸地址：</label>
						<span>${commissionOrder.pickAddr}</span>
					</div>
					<div class="col-md-6 height-align">
						<label class='lab-6' style='margin-left:-32px;'>办理人联系方式：</label>
						<input type="text" class="list_input input-dialog" id='tscPhone' name='tscPhone' value=''/>
					</div>
				</div>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin" >保存</button>
				<button type="reset"  class="btn btn-color-9E8273 btn-margin" >重置</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
			</div>
		</div>
	</form>
</div>