<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">委托业务综合查询</div>
</section>
<script type="text/javascript">
//当前页状态改变，用于后台判断，页面刷新之后跳转到当前页
var type=active.parsent2;
$("[href='"+type+"']").click();
$("#liClick").on('click','li a',function(){
	type=$(this).attr('href');
	active.parsent2=type;
	
// 	alert($("#clickId").val());
});
</script>
<section class="content">
	<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
			<div class="box-body">
			<input type="hidden" id="clickId" name="clickId" value="qicao">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs"  id='liClick'>
						<li class="active" role="presentation" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab"  aria-expanded="true"  >基本信息</a></li>
						<li class=""  role="presentation" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" >业务信息</a></li>
					</ul>
				<div class="tab-content">
			<!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						
							<%@ include file="/common/pageHead.jsp"%>
							<input name="method" value="${method}" type="hidden" />
								<div class="box-body">
									<div class="col-md-5 height-align">
										<label class="lab-2">卡号：</label>
										<input type="text"  class="required list_select" name="cardCode" value="${cardCode}">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死者姓名：</label>
										<input type="text"  class="required list_select" name="name" value="${name}">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">接尸地址：</label> 
										<input type="text" class="required list_select" name="pickAddr" value="${pickAddr }">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死者编号：</label> 
										<input type="text" class="required list_select" name="code" value="${code}">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">接尸单位：</label> 
										<select style="width: 169px" class="list_select nopadding-R" name="corpseUnitId" id="sex">
											<option>${corpseUnitOption}</option>
										</select>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">身份证号：</label> 
										<input type="text" class="required list_select" name="certificateCode" value="${certificateCode}">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死亡原因：</label> 
										<select style="width: 169px" class="list_select nopadding-R" name="deadReasonId" id="deadReasonId">
											<option>${deadReasonOption}</option>
										</select>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死者性别：</label> 
										<select style="width: 169px" class="list_select nopadding-R" name="sex" id="sex">
											<option>${sexOption}</option>
										</select>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死亡类型：</label> 
										<select style="width: 169px" class="list_select nopadding-R" name="deadTypeId" >
											<option>${deadTypeOption}</option>
										</select>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死者户籍：</label> 
										<select style="width: 169px" class="list_select nopadding-R" name="toponymId" >
											<option>${toponymOption}</option>
										</select>
									</div>
									<div class="col-md-5">
										<label class="lab-1">联系人：</label> 
										<input type="text" class="list_select" name="fName" value="${fName}">
									</div>
									<div class="col-md-5">
										<label  class="lab-4">联系电话：</label> 
										<input type="text" class="list_select" name="fPhone" value="${fPhone}">
									</div>
									<div class="col-md-5">
										<label class="lab-4">证明单位：</label> 
										<select class="list_input input-hometab" id="proveUnitId" name="proveUnitId" >
											<option>${proveUnitOption}</option>
										</select>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死亡日期：</label> 
										<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="dTime" name="dTime" value="${dTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
										<span>&nbsp;--</span>
										<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="dTimeEnd" name="dTimeEnd" value="${dTimeEnd}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
									<div class="col-md-12">
										<button type="submit" class="btn btn-info btn-search">搜索</button>
										<button type="reset" class="btn btn-default btn-search" style='background:#9E8273;color:#fff;'>重置</button>
										<a href="commissionOrder.do?method=editBase&menu=check&id=" target="homeTab" checkName="checkboxId" checkOne="true" class="btn btn-color-green" role="button">更改信息</a>
									</div>
								</div>
							
							
					<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>
											<th>死者姓名</th>
											<th>性别</th>
											<th>年龄</th>
											<th>身份证</th>
											<th>户籍</th>
											<th>住址</th>
											<th>炉号</th>
											<th>预约火化时间</th>
											<th>火化时间</th>
											<th>目前状态</th>
											<th>操作</th>
										
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="20">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td><input type="checkbox" name="checkboxId" value="${u.id}"></td>	
													<td><a href="commissionOrder.do?method=readOnly&id=${u.id}&checkflag=${u.checkFlag}&adr=B" target='homeTab'>${u.name }</a></td>				
							    					<td>${u.sexName }</td>
							    					<td>${u.age }</td>
							    					
							    					<td>${u.certificateCode }</td>
							    					<td>${u.province }${u.city }${u.area }</td>
							    					<td>${u.dAddr }</td>
							    					<td>${u.furnace.name }</td>
							    					<td><fmt:formatDate value="${u.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td><fmt:formatDate value="${u.furnaceRecord.beginTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					
							    					
							    					 <c:if test="${u.scheduleFlag==1 }">
														<td style='color:#e84e40;' class='state'>已到馆未收费</td>
											  		 </c:if>
							    					 <c:if test="${u.scheduleFlag==2 }">
														<td style='color:#fea625;' class='state'>收费结清</td>
											  		 </c:if>
											  		 <c:if test="${u.scheduleFlag==3 }">
														<td style='color:#29b6f6;' class='state'>已火化未打印</td>
											  		 </c:if>
											  		 <c:if test="${u.scheduleFlag==4 }">
														<td style='color:#9ccc65;' class='state'>已打印火化证明</td>
											  		 </c:if>
											  		 <td>
											  		 	<a href="${url }?method=print&id=${u.id}" rel='myModal'  target="dialog">打印</a>
											  		 </td>

							    					
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
						</div>
						
						
						
						
						
						
				<!-- 业务调度信息 -->
						<div class="tab-pane" id="b">
<%-- 							<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);"> --%>
						<%@ include file="/common/pageHead.jsp"%>
						<input name="method" value="${method}" type="hidden" />
							<div class="box-body">
								
								<div class="col-md-6 height-align">
									<label class="lab-1">冰柜号：</label>
									<input type="text"  class="required list_select" name="freezerName" value="${freezerName}">
								</div>
								<div class="col-md-6 height-align">
									<label class="lab-1">灵堂号：</label> 
									<input type="text" class="required list_select" name="mourningName" value="${mourningName }">
								</div>
								<div class="col-md-6 height-align">
									<label class="lab-4">告别厅号：</label> 
									<input type="text" class="required list_select" name="farewellName" value="${farewellName }">
								</div>
								<div class="col-md-6 height-align">
									<label class="lab-4">火化炉号：</label> 
									<input type="text" class="required list_select" name="furnaceName" value="${furnaceName }">
								</div>
								<div class="col-md-6 height-align">
									<label class="lab-4">火化炉类：</label> 
									<select class="list_input input-hometab" id="furnaceTypeId" name="furnaceTypeId" >
												<option>${furnaceTypeOption} </option>
									</select>
								</div>
								<div class="col-md-6 height-align">
									<label class="lab-4">登记日期：</label>
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="creatTime" name="creatTime" value="${creatTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span>&nbsp;--</span>
									<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="creatTimeEnd" name="creatTimeEnd" value="${creatTimeEnd }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class="lab-4">到馆时间：</label> 
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="arriveTime" name="arriveTime" value="${arriveTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span>&nbsp;--</span>
									<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="arriveTimeEnd" name="arriveTimeEnd" value="${arriveTimeEnd }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label  class="lab-6" style="margin-left: -28px">火化预约时间：</label> 
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="cremationTime" name="cremationTime" value="${cremationTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span>&nbsp;--</span>
									<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="cremationTimeEnd" name="cremationTimeEnd" value="${cremationTimeEnd}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-12 height-align">
									<label  class="lab-4">火化时间：</label> 
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="furnaceTime" name="furnaceTime" value="${furnaceTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span>&nbsp;--</span>
									<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="furnaceTimeEnd" name="furnaceTimeEnd" value="${furnaceTimeEnd}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6">
									<button type="submit" class="btn btn-info btn-search">搜索</button>
									<button type="reset" class="btn btn-success btn-search" style='background:#9E8273;color:#fff;'>重置</button>
									<a href="commissionOrder.do?method=editBase&menu=check&id=" target="homeTab" checkName="checkboxId" checkOne="true" class="btn btn-color-green" role="button">更改信息</a>
								</div>
							</div>
							
								<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
										<th><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>  	
												<th>死者姓名</th>
											<th>性别</th>
											<th>年龄</th>
											<th>身份证</th>
											<th>户籍</th>
											<th>住址</th>
											<th>炉号</th>
											<th>预约火化时间</th>
											<th>火化时间</th>
											<th>目前状态</th>
									
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="20">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td>
													<input type="checkbox" name="checkboxId" value="${u.id}"></td>					
							    					<td><a href="commissionOrder.do?method=readOnly&id=${u.id}&checkflag=${u.checkFlag}&adr=B" target='homeTab'> ${u.name }</a></td>
							    					<td>${u.sexName }</td>
							    					<td>${u.age }</td>
							    					
							    					<td>${u.certificateCode }</td>
							    					<td>${u.province }${u.city }${u.area }</td>
							    					
							    					<td>${u.dAddr }</td>
							    					<td>${u.furnace.name }</td>
							    					<td><fmt:formatDate value="${u.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td><fmt:formatDate value="${u.furnaceRecord.beginTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					 <c:if test="${u.scheduleFlag==1 }">
																<td style='color:#e84e40;' class='state1'>已到馆未收费</td>
													  		 </c:if>
									    					 <c:if test="${u.scheduleFlag==2 }">
																<td style='color:#fea625;' class='state1'>收费结清</td>
													  		 </c:if>
													  		 <c:if test="${u.scheduleFlag==3 }">
																<td style='color:#29b6f6;' class='state1'>已火化未打印</td>
													  		 </c:if>
													  		 <c:if test="${u.scheduleFlag==4 }">
																<td style='color:#9ccc65;' class='state1'>已打印火化证明</td>
													  		 </c:if>		
											

							    					
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>
