<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">委托业务综合查询</div>
</section>
<section class="content">
			<div class="box-body">
<!-- 				<div class="nav-tabs-custom"> -->
<!-- 					<ul class="nav nav-tabs"> -->
<!-- 						<li class="active" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab" aria-expanded="true">基本信息</a></li> -->
<!-- 						<li class="" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" aria-expanded="false">业务信息</a></li> -->
<!-- 					</ul> -->
<!-- 				<div class="tab-content"> -->
			<!-- 基本信息 -->
			<div class='box box-default'>
						<div class="tab-pane active" id="a">
						<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
							<%@ include file="/common/pageHead.jsp"%>
							<input name="method" value="${method}" type="hidden" />
								<div class="box-body">
									
								</div>
							</form>
							
					<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							
							<div class="col-sm-12 margin-T">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
									
											<th> 死者姓名</th>
											<th> 性别</th>
											<th> 年龄</th>
											<th>身份证</th>
											<th>户籍</th>
											<th>住址</th>
											<th>炉号</th>
											<th>预约火化时间</th>
											<th>火化时间</th>
											<th>目前状态</th>
										
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="20">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td   id="hidden" class="hidden"  style="display= ">
													<input type="checkbox" name="checkboxId" value="${u.id}"></td>	
													<td><a href="commissionOrder.do?method=readOnly&id=${u.id}&checkflag=${u.checkFlag}&adr=C" target='homeTab'>${u.name }</a></td>				
							    					<td>${u.sexName }</td>
							    					<td>${u.age }</td>
							    					
							    					<td>${u.certificateCode }</td>
							    							<td>${u.toponym.name }</td>
							    					
							    					<td>${u.dAddr }</td>
							    					<td>${u.furnace.name }</td>
							    					<td><fmt:formatDate value="${u.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td><fmt:formatDate value="${u.furnaceRecord.beginTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					
							    					
							    					
									    					 <c:if test="${u.scheduleFlag==1 }">
																<td style='color:#e84e40;' class='state1'>已到馆未收费</td>
													  		 </c:if>
									    					 <c:if test="${u.scheduleFlag==2 }">
																<td style='color:#fea625;' class='state1'>收费结清</td>
													  		 </c:if>
													  		 <c:if test="${u.scheduleFlag==3 }">
																<td style='color:#29b6f6;' class='state1'>已火化为打印</td>
													  		 </c:if>
													  		 <c:if test="${u.scheduleFlag==4 }">
																<td style='color:#9ccc65;' class='state1'>已打印火化证明</td>
													  		 </c:if>		
							    					 

							    					
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<div class='col-sm-12 margin-B' >
								<small class='btns-print pull-right'>
									<a href="login.do?method=main"  class='btn btn-default'>返回首页</a>
								</small>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
						
					</div>
				</div>
						</div>
						
						
					
				
				</div>
			</div>
	</section>
