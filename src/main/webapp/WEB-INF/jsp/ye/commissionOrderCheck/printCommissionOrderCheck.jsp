<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style type="text/css">
td 
{
text-align:left;
}
</style>
	<section  class="content">
		<form id='myModal' action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		<input type="hidden" name="method" value="${method }" >
			<div class="box-body">
				<div class="modal-dialog nav-tabs-custom" style='width:900px'>
					<div>
						<!--startprint0-->
						<div class='col-md-12'>
								<p class='col-md-12' style="text-align: center;color:#28b5f4;font-size: 35px;">温州市殡仪馆</p>
								<p class='col-md-12' style="text-align: center;color:#28b5f4;font-size: 25px;">火化登记表</p>
								<div class='row'>
									<div class='col-md-12'>
										<table class="table table-bordered-left table-hover">
											<thead>
												<tr role="row">
													<th colspan="6">死者信息栏</th>
												</tr>
											</thead>
											<tbody>
												<tr role="row">
													<td><b>死者登记号：</b></td>
							    					<td>${com.code }</td>
							    					<td><b>姓名：</b></td>
							    					<td>${com.name }</td>
							    					<td><b>性别：</b></td>
							    					<td>${com.sexName }</td>
												</tr>
												<tr role="row">
							    					<td><b>年龄：</b></td>
							    					<td>${com.age }</td>
							    					<td colspan="1"><b>身份证号：</b></td>
							    					<td colspan="3">${com.certificateCode }</td>
												</tr>
												<tr role="row">
												
							    					<td colspan="1"><b>身份证地址：</b></td>
							    					<td colspan="5">${com.province}${com.city}${com.area}${com.dAddr }</td>
												</tr>
												<tr role="row">
							    					<td colspan="1"><b>接运地址：</b></td>
							    					<td colspan="5">${com.pickAddr }</td>
												</tr>
												<tr role="row">
													<td><b>死亡类型：</b></td>
							    					<td>${com.deadType.name }</td>
							    					<td><b>死亡原因：</b></td>
							    					<td>${com.deadReason.name }</td>
							    					<td><b>证明单位：</b></td>
							    					<td>${com.proveUnit.name }</td>
												</tr>
												<tr role="row">
													<td><b>死亡时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${com.dTime }" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td><b>到馆时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${com.arriveTime }" pattern="yyyy-MM-dd HH:mm"/></td>
												</tr>
												<tr role="row">
													<td><b>预约火化时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${com.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td><b>火化时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${com.cremationTime }" pattern="yyyy-MM-dd HH:mm"/></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class='col-md-12'>
										<table class="table table-bordered-left table-hover">
											<thead>
												<tr role="row">
													<th colspan="6">经办人信息栏</th>
												</tr>
											</thead>
											<tbody>
												<tr role="row">
													<td><b>经办人姓名：</b></td>
							    					<td colspan="2">${com.fName }</td>
							    					<td><b>与死者关系：</b></td>
							    					<td colspan="2">${com.appellation.name }</td>
												</tr>
												<tr role="row">
							    					<td><b>住址：</b></td>
							    					<td colspan="2">${com.fAddr }</td>
							    					<td><b>联系方式：</b></td>
							    					<td colspan="2">${com.fPhone }</td>
												</tr>
												<tr role="row">
							    					<td><b>单位：</b></td>
							    					<td colspan="2">${com.fUnit }</td>
							    					<td><b>身份证号码：</b></td>
							    					<td colspan="2">${com.fCardCode }</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
					</div>
					<!--endprint0-->
					<div class="box-foot">
						<div  class="container-fluid">				
							<div class="col-md-12 btns-dialog btns-print">
									<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
									<a href="javascript:showprint()" class="btn  btn-color-ab47bc  btn-margin">打印</a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</form>
	</section>
