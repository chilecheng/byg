<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">骨灰寄存提醒到期</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
				</form>
					<div class="box" style="border-top: 0">
						<div class="box-header with-border">
							<small class="pull-right btns-buy">
<!-- 								<a class="btn btn-warning" role="button">群发短信提醒</a> -->
<%-- 								<a href="${url}?method=renew&ashesRecordId=" target="dialog" checkName="ashesRecordId"  checkOne="true" rel="myModal"  class="btn btn-success" role="button">续费</a> --%>
<%-- 								<a href="${url}?method=out&ashesRecordId=" target="dialog" checkName="ashesRecordId" checkOne="true" rel="myModal"  class="btn btn-info" role="button">领出</a> --%>
							</small>
						</div>
					<div class="box-body">
						<div id="example2_wrapper"
							class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="row">
								<div class="col-sm-12">
									<table class="table table-bordered table-hover">
										<thead>
											<tr role="row">
												<th width="10"></th>
												<th>业务流水号</th>
												<th>死者编号</th>
												<th>死者姓名</th>
												<th>性别</th>
												<th>年龄</th>
												<th>寄存到期时间</th>
												<th>寄存人电话</th>
<!-- 												<th>骨灰到期短信提醒</th> -->
											</tr>
										</thead>
										<tbody>
										 <c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr width="20">
									    			<td colspan="9">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
										     	<c:set var="index" value="0"></c:set>
											    <c:forEach items="${page.list}" var="u">
											   		
											    	<tr role="row" width="10">
													<td><input type="radio" name="ashesRecordId" value="${u.id}"></td>
							    					<td>${u.serialNumber}</td>
							    					<td>${u.dieCode}</td>
							    					<td>${u.dName}</td>
							    					<td>${u.sexName}</td>
							    					<td>${u.dAge}</td>
							    					<c:choose>
							    					<c:when test="${u.endDate<=currentDate}">
							    					<td style="color:red">${u.endDate}</td>
							    					</c:when>
							    					<c:otherwise>
							    					<td style="color:orange">${u.endDate}</td>
							    					</c:otherwise>
							    					</c:choose>
							    					<td>${u.sPhone}</td>
<!-- 							    					<td><a class="btn btn-warning" role="button">短信提醒</a></td> -->
							    					</tr>
							    					<c:set var="index" value="${index+1}"></c:set>
												</c:forEach>
											</c:otherwise>
										</c:choose>
										</tbody>
									</table>
								</div>
							</div>
							<%@ include file="/common/pageFood.jsp"%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
