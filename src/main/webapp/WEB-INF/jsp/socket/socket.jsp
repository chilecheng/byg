<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String wsPath = "ws://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="js/sockjs-0.3.min.js"></script>
<script>
$(function(){
    //建立socket连接
    var sock;
    if ('WebSocket' in window) {
      sock = new WebSocket("<%=wsPath%>socketServer.do");
      } else if ('MozWebSocket' in window) {
        sock = new MozWebSocket("<%=wsPath%>socketServer.do");
      } else {
        sock = new SockJS("<%=basePath%>sockjs/socketServer.do");
		}
		sock.onopen = function(e) {
			console.log(e);
		};
		sock.onmessage = function(e) {
			console.log(e)
			$("#message").append("<p><font color='red'>" + e.data + "</font>")
		};
		sock.onerror = function(e) {
			console.log(e);
		};
		sock.onclose = function(e) {
			console.log(e);
		}
	});
</script>
</head>
<body>
	<h1>
		Hello world! This is a WebSocket demo!
		<div id="message"></div>
	</h1>
</body>
</html>