<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ include file="/common/common.jsp" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>殡葬管理系统</title>
<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){ 
	var hei = $(window).height();
	$(".panel").height(hei/2);
});

function enterLogin(e){
	var ev = e || event
	if (ev.key !== 'Enter') {
		return
	}
	login()
}

function login(){
	var userCode = $("#userCode").val();
	var password = $("#password").val();
	$.ajax({
		url:"login.do",
		data: { method: "login",userCode: userCode,password: password},
		async:false,
		type:"POST",
		dataType:"json",
		success:function(json){
			if(json.status==200){
				window.location.href="login.do?method=main";
			}else{
				var showId ;
				if(json.status==300){
					showId = "userCode";
				}else if(json.status==400){
					showId = "password";
				}
				$('#'+showId).popover(
					{	
						trigger:'hover', //触发方式
						title:"",//设置 弹出框 的标题
						html: true, // 为true的话，data-content里就能放html代码了
						content:"<div style='width:70px;'>"+json.message+"</div>",//这里可以直接写字符串，也可以 是一个函数，该函数返回一个字符串；
					}
				);
				$("#"+showId).popover('show');
			} 
		},
		error:function(e){ 
			alert(e);
		}
	})
		
}
</script>
<style type="text/css">
*{
	padding:0;
	margin:0;
	}
ul{
	list-style:none;
	}
a{
	text-decoration:none;
	}
body{
	background-color:white;
	font-family:Arial,"微软雅黑";
	}
.panel{
	background-color:#009be1;
	position:relative;
		}
.login{
	border: 1px #cfcfcf solid;
	position: absolute;
	background-color:#fff;
	box-shadow:1px 5px 10px #8dafc5;
	bottom:-162px;
	left: 50%;	
	border-radius:20px;
	margin: -200px auto auto -201px;
	padding:20px 20px;	
}
.form-horizontal .form-group{
    margin:0;
    font-size:28px;
    color:#8a8a8a;
    width:360px;
    margin-bottom:20px;
    }

.form-group img{
    margin-top:2px;
    float:left;
    margin-right:12px;
    }
.col-sm-9{
    padding:0;
    width:100%;
    position:relative;
    }
.form-control{
    padding-left:46px;
    height:37px;
    }
#pic{
    position:absolute;
    top:3px;
    left:1px;
    margin:0;
}
#pic img{
    margin:0;
    }
.col-sm-9{
    margin:0;
    }
.form-horizontal .form-group .checkbox {
    float:right;
    font-size:14px;
    }
.btn-default{
    width:360px;
    background-color:#00b7ee;
    color:#fff;
    font-size:20px;   
    }
.btn-default:hover{
    background-color:#009ecd;
    color:#eeeeee;
    }
.name{
    width:510px;
    height:76px;
    position:absolute;
    bottom:180px;
    left:50%;
    margin: 0 auto auto -255px;
    padding-left:86px;
    background-image:url(images/5_03.png);
    background-repeat:no-repeat;
    }
.chinese{
    color:#baecff;
    font-size:30px;
    line-height:50px;
    }
.english{
    float:left;
    color:#baecff;
    font-size:12px;
    }
.technology{
    font-size:14px;
    color:#6c6c6c;
    position:absolute;
    bottom:-220px;
    left:50%;
    margin: 0 auto auto -120px;
    }
</style>
</head>
<body>
<div class="panel">
    <div class="name">
        <span class="chinese">温州市殡仪馆信息管理平台</span>
        <span class="english">WENZHOU CITY FUNERAL HOME INFORMATION MANAGEMENT PLATFORM</span>
    </div>
	<div class="login">
		<form class="form-horizontal">
		  <div class="form-group">
		  <img src="images/1.png">
		  	登录
		  </div>
		  <div class="form-group">		    
		    <div class="col-sm-9">
		      <div id="pic"><img src="images/4.png"></div>
		      <input type="text" id="userCode" class="form-control" placeholder="请输入用户名" >
		    </div>
		  </div>
		  <div class="form-group">		    
		    <div class="col-sm-9">
		      <div id="pic"><img src="images/3.png"></div>
		      <input type="password" id="password" class="form-control" onkeypress="enterLogin()" placeholder="请输入密码" >
		    </div>
		  </div>
		<!--   <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9">
		      <div class="checkbox">
		        <label>
		          <input type="checkbox"> 保存密码
		        </label>
		      </div>
		    </div>
		  </div> -->
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9">
		      <button type="button" onclick="login()" class="btn btn-default">登&nbsp&nbsp&nbsp陆</button>
		    </div>
		  </div>
		</form>
	</div>
	<span class="technology">技术支持：浙江汇智智能科技有限公司</span>
</div>
</body>
</html>