<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                  	<th style="width: 3px"></th>
                    <th>死者姓名</th>
                    <th>灵堂号</th>
                    <th>灵堂日期</th>
                  </tr>
                  </thead>
                  <tbody>  
                  	<c:choose>
					<c:when test="${fn:length(comList)==0 }">
						<tr>
							<td colspan="4"><font color="Red">还没有数据</font></td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${comList }" var="u">
							<tr>
								<td>
									<c:if test="${u.viewsMourning==0 }">
									<b style="color: red; font-style: italic; transform: scale(1.2);" name="doHidden">New</b>
									</c:if>
								</td>
								<td><a href="mourningRecord.do?method=edit&type=mour&homeType=${type }&id=${u.id }"  data-startInterval='true' onclick="changeNumber(this)"  rel="myModal" target="dialog" >${u.name}</a></td>
								<td>${u.mourning.name }</td>
								<td><fmt:formatDate value="${u.mourningTime }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>      
                  </tbody>
                </table>
            <div class="more">
              <a href="mourningRecord.do?method=mourningList&type=1" target="homeTab">更多>></a>
            </div>                 
              </div>
              <!-- /.table-responsive -->
            </div> 