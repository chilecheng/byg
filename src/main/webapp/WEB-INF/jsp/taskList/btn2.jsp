<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
	<div class="box-body">
              <div class="table-responsive">
             	<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
<%-- 					<input name="method" value="${method}" type="hidden" /> --%>
					<input name="id"  type="hidden" />
				</form>
                <table class="table no-margin">
                  <thead>
                  <tr>
                  	<th style="width: 3px"></th>
                    <th>死者姓名</th>
                    <th>联系人号码</th>
                    <th>冰柜号</th>
                  </tr>
                  </thead>
                  <tbody>        
                    <c:choose>
					<c:when test="${fn:length(comList)==0 }">
						<tr>
							<td colspan="4"><font color="Red">还没有数据</font></td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${comList }" var="u">
							<tr>
								<td>
									<c:if test="${u.viewsOut==0 }">
										<b style="color: red; font-style: italic; transform: scale(1.2);" name="doHidden">New</b>
									</c:if>
								</td>
								<td><a href="freezerRecord.do?method=out&id=${u.id }&homeType=${type}" rel="myModal" data-startInterval='true' onclick="changeNumber(this)" target="dialog"  id="nameClick">${u.name}</a></td>
								<td>${u.fPhone }</td>
								<td>${u.freezer.name }</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>                                                                                  
                  </tbody>
                </table>
             
            <div class="more">
              <a href="freezerRecord.do?method=list2&checkType=stationIng" target="homeTab">更多>></a>
           </div>                
         </div>
         <!-- /.table-responsive -->
       </div>
