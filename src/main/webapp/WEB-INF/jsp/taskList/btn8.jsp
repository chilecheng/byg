<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                  	<th style="width: 3px"></th>
                    <th>死者姓名</th>
                    <th>联系人电话</th>
                    <th>接尸时间</th>
                    <th>接尸地址</th>
                  </tr>
                  </thead>
                  <tbody>        
                    <c:choose>
					<c:when test="${fn:length(comList)==0 }">
						<tr>
							<td colspan="4"><font color="Red">还没有数据</font></td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${comList }" var="u">
							<tr>
								<td>
									<c:if test="${u.viewsCar==0 }">
										<b style="color: red; font-style: italic; transform: scale(1.2);" name="doHidden">New</b>
									</c:if>
								</td>
								<td><a href="carCarSchedul.do?method=dispatch&id=${u.id }&homeType=${type}" data-startInterval='true' rel='myModal' onclick="changeNumber(this)" target="dialog"  id="nameClick">${u.commissionOrder.name}</a></td>
								<td>${u.commissionOrder.fPhone }</td>
								<td><fmt:formatDate value="${u.pickTime }" pattern="yyyy-MM-dd HH:mm"/></td>
								<td>${u.commissionOrder.pickAddr }</td>
							</tr>
						</c:forEach>
					</c:otherwise>
					</c:choose>                                                                              
                  </tbody>
                </table>
            <div class="more">
              <a href="carCarSchedul.do?method=listInfo&type=1" target="homeTab">更多>></a>
            </div>               
              </div>
              <!-- /.table-responsive -->
            </div>  