<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<section class="taskList">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<i class="fa fa-tasks"></i>
					<h3 class="box-title">任务列表</h3>
				</div>
				<div class="box-body" style='border:1px solid #ccc;padding:0px;margin:10px'>
					<div class="nav-tabs-custom">
						<div class="piece">
							<div class="">
								<ul class="index_ul col-xs-2 no-margin">
									<%-- <li><a href="#" class="active" id="btn1">业务未收费列表</a>
										<c:choose>
											<c:when test="${fn:length(comViewsList)!=0 }">
												<div class="hint">${fn:length(comViewsList)}</div>
											</c:when>
										</c:choose>
									</li> --%>
									<c:forEach items="${listTask }" var="u">
										<li><a href="${u.url }" target="taskMenu" >${u.name }</a> 
											<c:choose>
												<c:when test="${u.newNumber !=0 }">
													<div class="hint" name="isShow">${u.newNumber}</div>
												</c:when>
											</c:choose>
										</li>
									</c:forEach>
								</ul>
								<div id="tabs" class='col-xs-10 no-margin'>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style='display:table'></div>
			</div>
		</div>
</section>