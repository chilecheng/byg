<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style type="text/css">
td 
{
text-align:left;
}
</style>
	<section  class="content">
		<form id='myModal' action="${url}" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);">
		<input type="hidden" name="method" value="${method }" >
			<div class="box-body">
				<div class="modal-dialog nav-tabs-custom" style='width:900px'>
					<div>
						<!--startprint0-->
						<div class='col-md-12'>
								<p class='col-md-12' style="text-align: center;color:#28b5f4;font-size: 35px;">温州市殡仪馆</p>
								<p class='col-md-12' style="text-align: center;color:#28b5f4;font-size: 25px;">火化登记表</p>
								<div class='row'>
									<div class='col-md-12'>
										<table class="table table-bordered-left table-hover">
											<thead>
												<tr role="row">
													<th colspan="6">死者信息栏</th>
												</tr>
											</thead>
											<tbody>
												<tr role="row">
													<td><b>死者登记号：</b></td>
							    					<td>${oldData.deadID }</td>
							    					<td><b>姓名：</b></td>
							    					<td>${oldData.name }</td>
							    					<td><b>性别：</b></td>
							    					<td>${oldData.sex }</td>
												</tr>
												<tr role="row">
							    					<td><b>年龄：</b></td>
							    					<td>${oldData.age }</td>
							    					<td colspan="1"><b>身份证号：</b></td>
							    					<td colspan="3">${oldData.idCard }</td>
												</tr>
												<tr role="row">
												
							    					<td colspan="1"><b>身份证地址：</b></td>
							    					<td colspan="5">${oldData.address }</td>
												</tr>
												<tr role="row">
							    					<td colspan="1"><b>接运地址：</b></td>
							    					<td colspan="5">${oldData.pickAddress }</td>
												</tr>
												<tr role="row">
													<td><b>死亡类型：</b></td>
							    					<td>${oldData.deadType }</td>
							    					<td><b>死亡原因：</b></td>
							    					<td>${oldData.death }</td>
							    					<td><b>证明单位：</b></td>
							    					<td>${oldData.certifyingAuthority }</td>
												</tr>
												<tr role="row">
													<td><b>死亡时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${oldData.deathDate }" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td><b>到馆时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${oldData.arriveTime }" pattern="yyyy-MM-dd HH:mm"/></td>
												</tr>
												<tr role="row">
													<td><b>预约火化时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${oldData.cremationTime}" pattern="yyyy-MM-dd HH:mm"/></td>
							    					<td><b>火化时间：</b></td>
							    					<td colspan="2"><fmt:formatDate value="${oldData.fireTime }" pattern="yyyy-MM-dd HH:mm"/></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class='col-md-12'>
										<table class="table table-bordered-left table-hover">
											<thead>
												<tr role="row">
													<th colspan="6">经办人信息栏</th>
												</tr>
											</thead>
											<tbody>
												<tr role="row">
													<td><b>经办人姓名：</b></td>
							    					<td colspan="2">${oldData.fName }</td>
							    					<td><b>与死者关系：</b></td>
							    					<td colspan="2">${oldData.relation }</td>
												</tr>
												<tr role="row">
							    					<td><b>住址：</b></td>
							    					<td colspan="2">${oldData.fAddress }</td>
							    					<td><b>联系方式：</b></td>
							    					<td colspan="2">${oldData.fPhone }</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
					</div>
					<!--endprint0-->
					<div class="box-foot">
						<div  class="container-fluid">				
							<div class="col-md-12 btns-dialog btns-print">
									<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">返回</button>
									<a href="javascript:showprint()" class="btn  btn-color-ab47bc  btn-margin">打印</a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</form>
	</section>
