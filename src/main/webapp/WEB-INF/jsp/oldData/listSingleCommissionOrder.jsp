<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">单机数据综合查询</div>
</section>
<script type="text/javascript">
//当前页状态改变，用于后台判断，页面刷新之后跳转到当前页
var type=active.parsent2;
$("[href='"+type+"']").click();
$("#liClick").on('click','li a',function(){
	type=$(this).attr('href');
	active.parsent2=type;
	
// 	alert($("#clickId").val());
});
</script>
<section class="content">
	<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
			<div class="box-body">
			<input type="hidden" id="clickId" name="clickId" value="qicao">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs"  id='liClick'>
						<li class="active" role="presentation" style="width: 150px; text-align: center;"><a href="#a" data-toggle="tab"  aria-expanded="true"  >基本信息</a></li>
						<li class=""  role="presentation" style="width: 150px; text-align: center;"><a href="#b" data-toggle="tab" >业务信息</a></li>
					</ul>
				<div class="tab-content">
			<!-- 基本信息 -->
						<div class="tab-pane active" id="a">
						
							<%@ include file="/common/pageHead.jsp"%>
							<input name="method" value="${method}" type="hidden" />
								<div class="box-body">
									<div class="col-md-5 height-align">
										<label class="lab-4">死者编号：</label> 
										<input type="text" class="required list_select" name="deadID" value="${deadID}">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死者姓名：</label>
										<input type="text"  class="required list_select" name="name" value="${name}">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死亡地址：</label> 
										<input type="text" class="required list_select" name="dieAddress" value="${dieAddress }">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死亡原因：</label> 
										<input type="text" class="required list_select" name="death" value="${death }">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死者性别：</label> 
										<select style="width: 169px" class="list_select nopadding-R" name="sex" id="sex">
											<option>${sexOption}</option>
										</select>
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死者户籍：</label> 
										<input type="text" class="required list_select" name="registerAdd" value="${registerAdd }">
									</div>
									<div class="col-md-5">
										<label class="lab-1">联系人：</label> 
										<input type="text" class="list_select" name="fName" value="${fName}">
									</div>
									<div class="col-md-5">
										<label  class="lab-4">联系电话：</label> 
										<input type="text" class="list_select" name="fPhone" value="${fPhone}">
									</div>
									<div class="col-md-5">
										<label class="lab-4">证明单位：</label> 
										<input type="text" class="list_select" name="certifyingAuthority" value="${certifyingAuthority}">
									</div>
									<div class="col-md-5 height-align">
										<label class="lab-4">死亡日期：</label> 
										<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="dTime" name="dTime" value="${dTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
										<span>&nbsp;--</span>
										<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="dTimeEnd" name="dTimeEnd" value="${dTimeEnd}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									</div>
									<div class="col-md-12">
										<button type="submit" class="btn btn-info btn-search">搜索单机系统</button>
										<button type="reset" class="btn btn-default btn-search" style='background:#9E8273;color:#fff;'>重置</button>
									</div>
								</div>
							
							
					<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>
											<th>死者姓名</th>
											<th>性别</th>
											<th>年龄</th>
											<th>户籍</th>
											<th>住址</th>
											<th>火化时间</th>
										
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="20">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td><input type="checkbox" name="checkboxId" value="${u.deadID}"></td>	
													<td><a rel='myModal' href="singleCommissionOrder.do?method=show&id=${u.deadID}" target='dialog'> ${u.name }</a></td>
							    					<td>${u.sexName }</td>
							    					<td>${u.age }</td>
							    					<td>${u.registerAdd }</td>
							    					<td>${u.address }</td>
							    					<td><fmt:formatDate value="${u.fireTime}" pattern="yyyy-MM-dd HH:mm"/></td>
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
			</div>
						
						
						
						
						
						
				<!-- 业务调度信息 -->
						<div class="tab-pane" id="b">
<%-- 							<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);"> --%>
						<%@ include file="/common/pageHead.jsp"%>
						<input name="method" value="${method}" type="hidden" />
							<div class="box-body">
								<div class="col-md-6 height-align">
									<label class="lab-4">登记日期：</label>
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="creatTime" name="creatTime" value="${creatTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span>&nbsp;--</span>
									<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="creatTimeEnd" name="creatTimeEnd" value="${creatTimeEnd }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6 height-align">
									<label class="lab-4">到馆时间：</label> 
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="arriveTime" name="arriveTime" value="${arriveTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span>&nbsp;--</span>
									<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="arriveTimeEnd" name="arriveTimeEnd" value="${arriveTimeEnd }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-12 height-align">
									<label  class="lab-4">火化时间：</label> 
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="furnaceTime" name="furnaceTime" value="${furnaceTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<span>&nbsp;--</span>
									<input type="text" data-id="endDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="furnaceTimeEnd" name="furnaceTimeEnd" value="${furnaceTimeEnd}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</div>
								<div class="col-md-6">
									<button type="submit" class="btn btn-info btn-search">搜索单机系统</button>
									<button type="reset" class="btn btn-success btn-search" style='background:#9E8273;color:#fff;'>重置</button>
								</div>
							</div>
							
								<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
										<th><input type="checkbox" class="checkBoxCtrl" group="checkboxId" /></th>  	
											<th>死者姓名</th>
											<th>性别</th>
											<th>年龄</th>
											<th>户籍</th>
											<th>住址</th>
											<th>火化时间</th>
									
										</tr>
									</thead>
									<tbody>
										<c:choose>
										    <c:when test="${fn:length(page.list)==0 }">
											    <tr>
									    			<td colspan="20">
												  		<font color="Red">还没有数据</font>
											 		</td>
											    </tr>
										    </c:when>
										    <c:otherwise>
											    <c:forEach items="${page.list }" var="u">
												<tr role="row">
													<td>
													<input type="checkbox" name="checkboxId" value="${u.deadID}"></td>					
							    					<td><a rel='myModal' href="singleCommissionOrder.do?method=show&id=${u.deadID}" target='dialog'> ${u.name }</a></td>
							    					<td>${u.sexName}</td>
							    					<td>${u.age }</td>
							    					<td>${u.registerAdd }</td>
							    					<td>${u.address }</td>
							    					<td><fmt:formatDate value="${u.fireTime}" pattern="yyyy-MM-dd HH:mm"/></td>
												</tr>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>
