<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${warehouse.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加仓库信息</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td width="100px">仓库名称：</td>
						<td><input type="text" id="name" name="name" class="form-control"  value="${warehouse.name }"></td>
						<td width="20px"></td>
						<td width="100px">备注信息：</td>
						<td><input type="text" class="required form-control" id="comment" name="comment" value="${warehouse.comment }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">是否启用：</td>
						<td>
							<select class="list_select nopadding-R" name="isdel">
								${wareOption}
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>