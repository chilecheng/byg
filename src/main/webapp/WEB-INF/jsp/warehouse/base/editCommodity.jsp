<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${commodity.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加商品信息</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td width="100px">商品代码：</td>
						<td><input type="text" class="required form-control" id="commodityId" name="commodityId" value="${commodity.commodityId }"></td>
						<td width="20px"></td>
						<td width="100px">商品名称：</td>
						<td><input type="text" id="name" name="name" class="form-control"  value="${commodity.name }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">供应商：</td>
						<td>
							<select class="list_select nopadding-R" name="supplierId">
								${supplierOption}
							</select>
						</td>
						<td width="20px"></td>
						<td width="100px">单价：</td>
						<td><input type="text" id="price" name="price" class="form-control"  value="${commodity.price }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">对应商品：</td>
						<td>
							<select class="list_select nopadding-R" name="itemId">
								${itemOption}
							</select>
						</td>
						<td width="20px"></td>
						<td width="100px">是否启用：</td>
						<td>
							<select class="list_select nopadding-R" name="isdel">
								${isDelOption}
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>