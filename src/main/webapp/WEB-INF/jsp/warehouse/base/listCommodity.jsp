<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">商品信息</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>商品名：</label> 
							<input type="text" class="list_input" name="name" value="${name}">
						</div>
						<div class="col-md-6">
							<label>是否启用：</label> 
							<select class="list_select nopadding-R" name="isdel">
								${isdelOption}
							</select>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-buy'>
					<a href="${url}?method=edit" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
					<a href="${url}?method=edit&id=" target="dialog" checkName="id" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>
					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="id" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="id" /></th>
											<th>商品名称</th>
											<th>商品代码</th>
											<th>供应商</th>
											<th>单价</th>
											<th>对应商品</th>
											<th>启用状态</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="7">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="id" value="${u.id }"></td>
												<td>${u.name }</td>
												<td>${u.commodityId }</td>
												<td>${u.supplier.name }</td>
												<td>${u.price }</td>
												<td>${u.item.name }</td>
												<td>
													<c:if test="${u.isDel==Isdel_No }">
							    					<font color="red" class='state1'>禁用</font>
							    					</c:if>
							    					<c:if test="${u.isDel==Isdel_Yes}">
							    					<font color="green" class='state1'>启用</font>
							    					</c:if>
												</td>
												
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
