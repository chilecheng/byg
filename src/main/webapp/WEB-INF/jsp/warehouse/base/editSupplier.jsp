<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${supplier.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加供应商信息</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td width="100px">供应商代码：</td>
						<td><input type="text" class="required form-control" id="supplierId" name="supplierId" value="${supplier.supplierId }"></td>
						<td width="20px">
<!-- 						&nbsp;<i class="fa fa-circle text-red fa-middle "></i> -->
						&nbsp;
						</td>
						<td width="100px">供应商名称：</td>
						<td><input type="text" id="name" name="name" class="form-control"  value="${supplier.name }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">联系人：</td>
						<td><input type="text" id="contact" name="contact" class="form-control"  value="${supplier.contact }"></td>
						<td width="20px"></td>
						<td width="100px">联系电话：</td>
						<td><input type="text" id="phoneNumber" name="phoneNumber" class="form-control"  value="${supplier.phoneNumber }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">开户银行：</td>
						<td><input type="text" id="bank" name="bank" class="form-control"  value="${supplier.bank }"></td>
						<td width="20px"></td>
						<td width="100px">开户帐号：</td>
						<td><input type="text" id="accountNumber" name="accountNumber" class="form-control"  value="${supplier.accountNumber }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">税号：</td>
						<td><input type="text" id="taxId" name="taxId" class="form-control"  value="${supplier.taxId }"></td>
						<td width="20px"></td>
						<td width="100px">余额：</td>
						<td><input type="text" id="balance" name="balance" class="form-control"  value="${supplier.balance }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">是否启用：</td>
						<td>
							<select class="list_select nopadding-R" name="isdel">
								${isDelOption}
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>