<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加出库信息</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td width="60px">移出仓库：</td>
						<td>
							<select class="list_select nopadding-R" name="warehouseId" style="width: 120px;">
								${warehouseOption}
							</select>
						</td>
						<td width="20px"></td>
						<td width="100px">商品选择：</td>
						<td>
							<select class="list_select nopadding-R" name="commodityId" style="width: 120px;">
								${commodityOption}
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">出库数量：</td>
						<td><input type="text" id="number" name="number" class="form-control"  value="${commodity.price }"></td>
						<td width="20px"></td>
						<td width="100px">出库方式：</td>
						<td>
							<select class="list_select nopadding-R" name="outWay" style="width: 120px;">
								${outWayOption}
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>
