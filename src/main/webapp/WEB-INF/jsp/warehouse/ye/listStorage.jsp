<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">入库信息管理</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>名称：</label> 
							<input type="text" class="list_input" name="name" value="${name}">
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-buy'>
					<a href="${url}?method=edit" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
<%-- 					<a href="${url}?method=edit&id=" target="dialog" checkName="id" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a> --%>
<%-- 					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="id" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a> --%>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="id" /></th>
											<th>入库单号</th>
											<th>供应商</th>
											<th>商品</th>
											<th>数量</th>
											<th>仓库</th>
											<th>入库方式</th>
											<th>入库时间</th>
											<th>操作人员</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="9">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="id" value="${u.id }"></td>
												<td>${u.serialNumber }</td>
												<td>${u.supplier.name }</td>
												<td>${u.commodity.name }</td>
												<td>${u.number }</td>
												<td>${u.warehouse.name }</td>
												<td>
													<c:if test="${u.intoWay==1 }">
							    					<font color="green" class='state1'>买入</font>
							    					</c:if>
							    					<c:if test="${u.intoWay==2}">
							    					<font color="green" class='state1'>调拨</font>
							    					</c:if>
							    					<c:if test="${u.intoWay==4}">
							    					<font color="green" class='state1'>回收</font>
							    					</c:if>
												</td>
												<td><fmt:formatDate value="${u.intoTime }" pattern="yyyy-MM-dd HH:mm"/></td>
												<td>${u.operator }</td>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
