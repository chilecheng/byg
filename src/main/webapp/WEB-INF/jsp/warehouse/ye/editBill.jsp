<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${bill.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">结算信息</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td width="100px">商品选择：</td>
						<td>
							<select class="list_select nopadding-R" name="commodityId" style="width: 120px;">
								${commodityOption}
							</select>
						</td>
						<td width="20px"></td>
<!-- 						<td width="100px">供应商：</td> -->
<!-- 						<td> -->
<!-- 							<select class="list_select nopadding-R" name="supplierId" style="width: 120px;"> -->
<%-- 								${supplierOption} --%>
<!-- 							</select> -->
<!-- 						</td> -->
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">售出数量：</td>
						<td><input type="text" id="sell" name="sell" class="form-control"  value="${bill.sell }"></td>
						<td width="20px"></td>
						<td width="100px">回收数量：</td>
						<td><input type="text" id="recover" name="recover" class="form-control"  value="${bill.recover }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">损耗数量：</td>
						<td><input type="text" id="loss" name="loss" class="form-control"  value="${bill.loss }"></td>
						<td width="20px"></td>
						<td width="100px">单价：</td>
						<td><input type="text" id="price" name="price" class="form-control"  value="${bill.price }"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>
