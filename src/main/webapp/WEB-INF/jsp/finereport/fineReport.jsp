<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<c:if test="${which=='elePortrait' }">
		<div class="title">电子遗像报表</div>
	</c:if>
	<c:if test="${which=='danrielePortrait' }">
		<div class="title">电子遗像报表(单日)</div>
	</c:if>
	<c:if test="${which=='liyicb' }">
		<div class="title">礼仪出殡报表</div>
	</c:if>
	<c:if test="${which=='danriliyicb' }">
		<div class="title">礼仪出殡报表(单日)</div>
	</c:if>
	<c:if test="${which=='faP' }">
		<div class="title">发票统计报表</div>
	</c:if>
	<c:if test="${which=='szyprwd' }">
		<div class="title">丧葬用品任务单</div>
	</c:if>
	<c:if test="${which=='danriszyprwd' }">
		<div class="title">丧葬用品任务单(单日)</div>
	</c:if>
	<c:if test="${which=='ytjdsjap' }">
		<div class="title">遗体解冻时间安排</div>
	</c:if>
	<c:if test="${which=='danriytjdsjap' }">
		<div class="title">遗体解冻时间安排(单日)</div>
	</c:if>
	<c:if test="${which=='ytclap' }">
		<div class="title">遗体处理安排表</div>
	</c:if>
	<c:if test="${which=='danriytclap' }">
		<div class="title">遗体处理安排表(单日)</div>
	</c:if>
	<c:if test="${which=='hhtj' }">
		<div class="title">各炉火化统计表</div>
	</c:if>
	<c:if test="${which=='sftjbb' }">
		<div class="title">收费统计报表</div>
	</c:if>
	<c:if test="${which=='jbbzjmtj' }">
		<div class="title">基本殡葬减免统计</div>
	</c:if>
	<c:if test="${which=='lttj' }">
		<div class="title">灵堂统计</div>
	</c:if>
	<c:if test="${which=='gbttj' }">
		<div class="title">告别厅统计</div>
	</c:if>
	<c:if test="${which=='knbzjmtj' }">
		<div class="title">困难殡葬减免统计</div>
	</c:if>
	<c:if test="${which=='hhltj' }">
		<div class="title">各地区火化量统计(火化时间)</div>
	</c:if>
	<c:if test="${which=='hhltjyy' }">
		<div class="title">各地区火化量统计(预约火化时间)</div>
	</c:if>
	<c:if test="${which=='guoshui' }">
		<div class="title">收费日报表-国税</div>
	</c:if>
	<c:if test="${which=='guoshuiLS' }">
		<div class="title">收费日报表-国税(历史)</div>
	</c:if>
	<c:if test="${which=='guoshuigai' }">
		<div class="title">收费日报表-国税(改16)</div>
	</c:if>
	<c:if test="${which=='guoshuigaiLS' }">
		<div class="title">收费日报表-国税(改16历史)</div>
	</c:if>
	<c:if test="${which=='guoshuigai2' }">
		<div class="title">收费日报表-国税(改13)</div>
	</c:if>
	<c:if test="${which=='guoshuigai2LS' }">
		<div class="title">收费日报表-国税(改13历史)</div>
	</c:if>
	<c:if test="${which=='ddkcllb' }">
		<div class="title">调度科车辆列表</div>
	</c:if>
	<c:if test="${which=='danriclapb' }">
		<div class="title">车辆安排表(单日)</div>
	</c:if>
	<c:if test="${which=='feishui' }">
		<div class="title">收费日报表-非税</div>
	</c:if>
	<c:if test="${which=='feishuiLS' }">
		<div class="title">收费日报表-非税(历史)</div>
	</c:if>
	<c:if test="${which=='ngslyysj' }">
		<div class="title">纳骨送灵预约时间</div>
	</c:if>
	<c:if test="${which=='fzcswdwzmbb' }">
		<div class="title">非正常死亡单位证明报表</div>
	</c:if>
	<c:if test="${which=='tylyysjb' }">
		<div class="title">特约炉预约时间表</div>
	</c:if>
	<c:if test="${which=='danritylyysjb' }">
		<div class="title">特约炉预约时间表(单日)</div>
	</c:if>
	<c:if test="${which=='zshqhl' }">
		<div class="title">租售花圈花篮统计</div>
	</c:if>
	<c:if test="${which=='xkjlcx' }">
		<div class="title">销卡记录查询</div>
	</c:if>
	<c:if test="${which=='lcgjcktj' }">
		<div class="title">冷藏柜进出统计(预约火化时间)</div>
	</c:if>
	<c:if test="${which=='lcgjcktjck' }">
		<div class="title">冷藏柜进出统计(出库时间)</div>
	</c:if>
	
	<c:if test="${which=='zfbmdszmd' }">
		<div class="title">政法部门经手的死者名单</div>
	</c:if>
	<c:if test="${which=='jp' }">
		<div class="title"></div>
	</c:if>
	<c:if test="${which=='ytclylb' }">
		<div class="title"></div>
	</c:if>
	<c:if test="${which=='bgytylb' }">
		<div class="title">殡仪馆冰柜遗体一览表</div>
	</c:if>
	<c:if test="${which=='hhqktjb' }">
		<div class="title">火化情况和告别厅情况统计表(火化时间)</div>
	</c:if>
	<c:if test="${which=='hhqktjbyy' }">
		<div class="title">火化情况和告别厅情况统计表(预约火化时间)</div>
	</c:if>
	<c:if test="${which=='bhhrydjtk' }">
		<div class="title">被火化人员情况登记台卡(火化时间)</div>
	</c:if>
	<c:if test="${which=='bhhrydjtkyy' }">
		<div class="title">被火化人员情况登记台卡(预约火化时间)</div>
	</c:if>
	<c:if test="${which=='hhrysjhzb' }">
		<div class="title">火化人员数据汇总表(火化日期)</div>
	</c:if>
	<c:if test="${which=='hhrysjhzbyy' }">
		<div class="title">火化人员数据汇总表(预约火化日期)</div>
	</c:if>
	<c:if test="${which=='ywskyjb' }">
		<div class="title"></div>
	</c:if>
	<c:if test="${which=='zfyttjhzb' }">
		<div class="title">政法遗体统计汇总表</div>
	</c:if>
	<c:if test="${which=='ytmdjfytjb' }">
		<div class="title">政法遗体火化遗体名单及费用统计表</div>
	</c:if>
	<c:if test="${which=='syclfytjb' }">
	<%-- <c:if test="${which=='yywt' }"> --%>
		<div class="title">医院委托火化处理费用统计表</div>
	</c:if>
	<c:if test="${which=='zbccapb' }">
		<div class="title">业务一科早班场次安排表</div>
	</c:if>
	<c:if test="${which=='ltsyapb' }">
		<div class="title">业务一科礼厅司仪安排表</div>
	</c:if>
	<c:if test="${which=='fun' }">
		<div class="title">礼仪出殡数量统计表</div>
	</c:if>
	<c:if test="${which=='ghh' }">
		<div class="title">骨灰盒销售数量统计表(委托)</div>
	</c:if>
	<c:if test="${which=='ghhfw' }">
		<div class="title">骨灰盒销售数量统计表(非委托)</div>
	</c:if>
	<c:if test="${which=='zg' }">
		<div class="title">纸棺销售数量统计表</div>
	</c:if>
	<c:if test="${which=='szyp' }">
		<div class="title">丧葬用品销售数量统计表</div>
	</c:if>
	<c:if test="${which=='danrigbtaprwd' }">
		<div class="title">告别厅安排任务单(单日)</div>
	</c:if>
	<c:if test="${which=='danriltaprwd' }">
		<div class="title">灵堂安排任务单(单日)</div>
	</c:if>
	<c:if test="${which=='gbtaprwd' }">
		<div class="title">告别厅安排任务单</div>
	</c:if>
	<c:if test="${which=='ltaprwd' }">
		<div class="title">灵堂安排任务单</div>
	</c:if>
	<c:if test="${which=='zbzztjb' }">
		<div class="title">早班最早统计表</div>
	</c:if>
	<c:if test="${which=='ltbststjb' }">
		<div class="title">灵堂摆设天数统计表</div>
	</c:if>
	<c:if test="${which=='ddlyzbhzb' }">
		<div class="title">调度礼仪早班汇总表</div>
	</c:if>
	<c:if test="${which=='skgbthljdb' }">
		<div class="title">三科告别厅和炉校对表</div>
	</c:if>
	<c:if test="${which=='danrenFTT' }">
		<div class="title">遗体解冻时间安排(打印)</div>
	</c:if>
	<c:if test="${which=='danrenCDT' }">
		<div class="title">遗体处理安排表(打印)</div>
	</c:if>
	<c:if test="${which=='bzygszsj' }">
		<div class="title">殡葬严管逝者数据表</div>
	</c:if>
	<c:if test="${which=='danriZGRecode' }">
		<div class="title">纸棺校对表(单日)</div>
	</c:if>
	<c:if test="${which=='sdltczhzb' }">
		<div class="title">试点街道灵堂出租汇总表</div>
	</c:if>
	<c:if test="${which=='tjmx' }">
		<div class="title">非税国税统计明细表</div>
	</c:if>
</section>
<style type="text/css">
	#main-content>div.box>div.row{
		width:100%;
		margin-left:0px;
		padding-bottom:15px;
	}
</style>
<script type="text/javascript">
function elePortraitSearch(){ //电子遗像
	//var startTime = document.getElementById("startTime").value; //开始时间
	//var endTime = document.getElementById("endTime").value; //结束时间
	//debugger;
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var time = $("#time").val();
	var projectName = "${sessionScope.reportPath }";
	//if(startTime == " " && endTime == " " && time == " "){    +"&time="+time
	if(startTime == "" && endTime == ""){
		//return projectName+"/frameset?__report=elePortrait.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		return false;
	}else{
		//alert(startTime);
		//alert(endTime);
		var url = projectName+"/frameset?__report=elePortrait.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#elePortrait").attr("src", url);
	}
	/* window.location.href = "frameset?__report=elePortrait.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
/* function elePortraitChangeDate(obj){ //时间改变
	var move=$(obj).attr('data-change');
	var target=$(obj).attr('data-tar');
	var date=$('[data-tar='+target+']').val();
	var day=new Date(date);
	if(move==='go'){
		date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
		$('[data-tar='+target+']').val(date);
		return elePortraitSearch(); 
	}
	if(move==='back'){
		date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
		$('[data-tar='+target+']').val(date);
		return elePortraitSearch();
	}
} */
function danriElePortraitSearch(){ //电子遗像danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriElePortrait.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriElePortrait").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriElePortraitDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriElePortraitSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriElePortraitSearch();
}
}

function funeralSearch(){ //礼仪出殡
	//debugger;
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=funeral.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#funeral").attr("src", url);
	}
}
function danriFuneralSearch(){ //礼仪出殡danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriFuneral.rptdesign&__parameterpage=false&startTime="+startTime+"&name="+name;
		$("#danriFuneral").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriFuneralDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriFuneralSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriFuneralSearch();
}
}

function invoiceStaticSearch(){ //发票统计
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //死者姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=invoiceStatic.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#invoiceStatic").attr("src", url);
	}
	/* window.location.href = "frameset?__report=invoiceStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function taskFuneralSearch(){ //丧葬用品任务单
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=taskFuneral.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#taskFuneral").attr("src", url);
	}
	/* window.location.href = "frameset?__report=taskFuneral.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function freezerThawTimeSearch(){ //遗体解冻时间安排
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=freezerThawTime.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#freezerThawTime").attr("src", url);
	}
	/* window.location.href = "frameset?__report=freezerThawTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriFreezerThawTimeSearch(){ //遗体处理时间安排danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriFreezerThawTime.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriFreezerThawTime").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
/* function danriFreezerThawTimeDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriFreezerThawTimeSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriFreezerThawTimeSearch();
}
} */ 

function corpseDealTimeSearch(){ //遗体处理时间安排
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=corpseDealTime.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#corpseDealTime").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriCorpseDealTimeSearch(){ //遗体处理时间安排danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriCorpseDealTime.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriCorpseDealTime").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriCorpseDealTimeDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriCorpseDealTimeSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriCorpseDealTimeSearch();
}
} 

function fireFurnaceSearch(){ //火化统计
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=fireFurnace.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#fireFurnace").attr("src", url);
	}
	/* window.location.href = "frameset?__report=fireFurnace.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function charageSearch(){ //收费统计表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=charage.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#charage").attr("src", url);
	}
	/* window.location.href = "frameset?__report=charage.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function baseReductionSearch(){ //基本殡葬减免
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var addr = $("#province").val()+$("#city").val()+$("#area").val(); //所在地
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == "" && addr == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=baseReduction.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name+"&addr="+addr;
		$("#baseReduction").attr("src", url);
	}
	/* window.location.href = "frameset?__report=baseReduction.rptdesign&startTime="+startTime+"&endTime="+endTime+"&name="+name+"&area="+area; */
}

function mouringRecordSearch(){ //灵堂统计
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=mouringRecord.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#mouringRecord").attr("src", url);
	}
	/* window.location.href = "frameset?__report=mouringRecord.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function farewellSearch(){ //告别厅统计
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=farewell.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#farewell").attr("src", url);
	}
	/* window.location.href = "frameset?__report=farewell.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function hardReductionSearch(){ //困难殡葬减免
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var addr = $("#province").val()+$("#city").val()+$("#area").val(); //所在地
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == "" && addr == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=hardReduction.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name+"&addr="+addr;
		$("#hardReduction").attr("src", url);
	}
	/* window.location.href = "frameset?__report=hardReduction.rptdesign&startTime="+startTime+"&endTime="+endTime+"&name="+name+"&area="+area; */
}

function areaFuneralStaticSearch(){ //火化量统计(火化时间)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=areaFuneralStatic.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#areaFuneralStatic").attr("src", url);
	}
	/* window.location.href = "frameset?__report=areaFuneralStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function areaFuneralStaticyySearch(){ //火化量统计(预约火化时间)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=areaFuneralStaticyy.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#areaFuneralStaticyy").attr("src", url);
	}
	/* window.location.href = "frameset?__report=areaFuneralStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function taxChargeSearch(){ //国税
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=taxCharge.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#taxCharge").attr("src", url);
	}
	/* window.location.href = "frameset?__report=taxCharge.rptdesign&startTime="+startTime; */
}
function taxChargeLSSearch(){ //国税LS
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=taxChargeLS.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#taxChargeLS").attr("src", url);
	}
	/* window.location.href = "frameset?__report=taxCharge.rptdesign&startTime="+startTime; */
}
function taxChargeGAISearch(){ //国税(改16)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=taxChargeGAI.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#taxChargeGAI").attr("src", url);
	}
	/* window.location.href = "frameset?__report=taxCharge.rptdesign&startTime="+startTime; */
}
function taxChargeGAILSSearch(){ //国税(改16)LS
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=taxChargeGAILS.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#taxChargeGAILS").attr("src", url);
	}
	/* window.location.href = "frameset?__report=taxCharge.rptdesign&startTime="+startTime; */
}
function taxChargeGAI2Search(){ //国税(改13)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=taxChargeGAI2.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#taxChargeGAI2").attr("src", url);
	}
	/* window.location.href = "frameset?__report=taxCharge.rptdesign&startTime="+startTime; */
}
function taxChargeGAI2LSSearch(){ //国税(改13)LS
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=taxChargeGAI2LS.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#taxChargeGAI2LS").attr("src", url);
	}
	/* window.location.href = "frameset?__report=taxCharge.rptdesign&startTime="+startTime; */
}
/* function taxChangeDate(obj){ //时间改变
	var move=$(obj).attr('data-change');
	var target=$(obj).attr('data-tar');
	var date=$('[data-tar='+target+']').val();
	var day=new Date(date);
	if(move==='go'){
		date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
		$('[data-tar='+target+']').val(date);
		return taxChargeSearch(); 
	}
	if(move==='back'){
		date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
		$('[data-tar='+target+']').val(date);
		return taxChargeSearch();
	}
} */

function carListSearch(){ //调度科车辆列表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=carList.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#carList").attr("src", url);
	}
	/* window.location.href = "frameset?__report=carList.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function freezerStaticSearch(){ //冷藏柜进出库统计(预约火化时间)
	var startTime = $("#startTime").val(); //开始时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=freezerStatic.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#freezerStatic").attr("src", url);
	}
	/* window.location.href = "frameset?__report=freezerStatic.rptdesign&startTime="+startTime; */
}
function freezerStaticCKSearch(){ //冷藏柜进出库统计(出库时间)
	var startTime = $("#startTime").val(); //开始时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=freezerStaticCK.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#freezerStaticCK").attr("src", url);
	}
	/* window.location.href = "frameset?__report=freezerStatic.rptdesign&startTime="+startTime; */
}
/* function freezerChangeDate(obj){ //时间改变
	var move=$(obj).attr('data-change');
	var target=$(obj).attr('data-tar');
	var date=$('[data-tar='+target+']').val();
	var day=new Date(date);
	if(move==='go'){
		date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
		$('[data-tar='+target+']').val(date);
		return freezerStaticSearch(); 
	}
	if(move==='back'){
		date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
		$('[data-tar='+target+']').val(date);
		return freezerStaticSearch();
	}
} */

function nonTaxChargeSearch(){ //非税
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=nonTaxCharge.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#nonTaxCharge").attr("src", url);
	}
	/* window.location.href = "frameset?__report=nonTaxCharge.rptdesign&startTime="+startTime; */
}
function nonTaxChargeLSSearch(){ //非税
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=nonTaxChargeLS.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#nonTaxChargeLS").attr("src", url);
	}
	/* window.location.href = "frameset?__report=nonTaxCharge.rptdesign&startTime="+startTime; */
}
/* function nonTaxChangeDate(obj){ //时间改变
	var move=$(obj).attr('data-change');
	var target=$(obj).attr('data-tar');
	var date=$('[data-tar='+target+']').val();
	var day=new Date(date);
	if(move==='go'){
		date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
		$('[data-tar='+target+']').val(date);
		return nonTaxChargeSearch(); 
	}
	if(move==='back'){
		date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
		$('[data-tar='+target+']').val(date);
		return nonTaxChargeSearch();
	}
} */

function boneSoulOrderSearch(){ //纳骨送灵预约时间
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=boneSoulOrder.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#boneSoulOrder").attr("src", url);
	}
	/* window.location.href = "frameset?__report=boneSoulOrder.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function unormaldeadSearch(){ //非正常死亡证明表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=unormaldead.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#unormaldead").attr("src", url);
	}
	/* window.location.href = "frameset?__report=unormaldead.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function specialTimeSearch(){ //特约炉预约时间表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var code = $("#code").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && code == ""){
		return false;
	 }else if(code == "0"){
		//startTime = " ";
		//endTime = " ";
		var url = projectName+"/frameset?__report=specialTime.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#specialTime").attr("src", url); 
	}else{
		var url = projectName+"/frameset?__report=specialTime.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&code="+code;
		$("#specialTime").attr("src", url);
	}
	/* window.location.href = "frameset?__report=specialTime.rptdesign&startTime="+startTime+"&endTime="+endTime+"&code="+code; */
}
function danriSpecialTimeSearch(){ //特约炉预约时间表danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var code = $("#code").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && code == ""){
		return false;
	 }else if(code == "0"){
		//startTime = " ";
		//endTime = " ";
		var url = projectName+"/frameset?__report=danriSpecialTime.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriSpecialTime").attr("src", url); 
	}else{
		var url = projectName+"/frameset?__report=danriSpecialTime.rptdesign&__parameterpage=false&startTime="+startTime+"&code="+code;
		$("#danriSpecialTime").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriSpecialTimeDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriSpecialTimeSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriSpecialTimeSearch();
}
} 

function rentFlowerSearch(){ //租售花圈花篮
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=rentFlower.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#rentFlower").attr("src", url);
	}
	/* window.location.href = "frameset?__report=rentFlower.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function pincardRecordSearch(){ //销卡记录表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var card = $("#card").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && card == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=pincardRecord.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&card="+card;
		$("#pincardRecord").attr("src", url);
	}
	/* window.location.href = "frameset?__report=pincardRecord.rptdesign&startTime="+startTime+"&endTime="+endTime+"&card="+card; */
}

function zfbmdszmdSearch(){ //政法部门的死者名单
	debugger;
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var sTime = $("#sTime").val(); //开始时间
	//var eTime = $("#eTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=zfbmdszmd.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#zfbmdszmd").attr("src", url);
	}/* else if(startTime == "" && endTime == "" && name == ""){
		var url = projectName+"/frameset?__report=zfbmdszmd.rptdesign&__parameterpage=false&sTime="+sTime+"&eTime="+eTime;
		$("#zfbmdszmd").attr("src", url);
	} else{
		var url = projectName+"/frameset?__report=zfbmdszmd.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&sTime="+sTime+"&eTime="+eTime+"&name"+name;
		$("#zfbmdszmd").attr("src", url);
	}  */
	
	/* window.location.href = "frameset?__report=zfbmdszmd.rptdesign&startTime="+startTime; */
}
/* function zfbmdszmdChangeDate(obj){ //时间改变
	var move=$(obj).attr('data-change');
	var target=$(obj).attr('data-tar');
	var date=$('[data-tar='+target+']').val();
	var day=new Date(date);
	if(move==='go'){
		date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
		$('[data-tar='+target+']').val(date);
		return zfbmdszmdSearch(); 
	}
	if(move==='back'){
		date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
		$('[data-tar='+target+']').val(date);
		return zfbmdszmdSearch();
	}
} */

/* function jpSearch(){ //解剖
	var startTime = $("#startTime").val(); //开始时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=jp.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#jp").attr("src", url);
	}
	// window.location.href = "frameset?__report=jp.rptdesign&startTime="+startTime; 
} */
/* function jpChangeDate(obj){ //时间改变
	var move=$(obj).attr('data-change');
	var target=$(obj).attr('data-tar');
	var date=$('[data-tar='+target+']').val();
	var day=new Date(date);
	if(move==='go'){
		date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
		$('[data-tar='+target+']').val(date);
		return jpSearch();
	}
	if(move==='back'){
		date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
		$('[data-tar='+target+']').val(date);
		return jpSearch();
	}
} */

/* function ytclylbSearch(){ //遗体处理一览表
	var name = $("#name").val(); //名称
	var projectName = "${sessionScope.reportPath }";
	if(name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ytclylb.rptdesign&__parameterpage=false&name="+name;
		$("#ytclylb").attr("src", url);
	}
	 //window.location.href = "frameset?__report=ytclylb.rptdesign&name="+name; 
} */

function bgytylb(){ //冰柜遗体一览表
	
}

function hhqktjbSearch(){ //火化情况统计表(火化时间)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=hhqktjb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#hhqktjb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=hhqktjb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function hhqktjbyySearch(){ //火化情况统计表(预约火化时间)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=hhqktjbyy.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#hhqktjbyy").attr("src", url);
	}
	/* window.location.href = "frameset?__report=hhqktjb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function bhhrydjtkSearch(){ //被火化人员登记台卡
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=bhhrydjtk.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#bhhrydjtk").attr("src", url);
	}
	/* window.location.href = "frameset?__report=bhhrydjtk.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function bhhrydjtkyySearch(){ //被火化人员登记台卡
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=bhhrydjtkyy.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#bhhrydjtkyy").attr("src", url);
	}
	/* window.location.href = "frameset?__report=bhhrydjtk.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function hhrysjhzbSearch(){ //火化人员数据汇总(火化日期)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	//var csTime = $("#csTime").val(); //开始时间
	//var ceTime = $("#ceTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=hhrysjhzb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#hhrysjhzb").attr("src", url);
	}
	/* if(csTime == "" && ceTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=hhrysjhzb.rptdesign&__parameterpage=false&csTime="+csTime+"&ceTime="+ceTime;
		$("#hhrysjhzb").attr("src", url);
	} */
	/* window.location.href = "frameset?__report=hhrysjhzb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function hhrysjhzbyySearch(){ //火化人员数据汇总(预约火化日期)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=hhrysjhzbyy.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#hhrysjhzbyy").attr("src", url);
	}
	/* window.location.href = "frameset?__report=hhrysjhzb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

/* function ywskyjbSearch(){ //业务三科业绩表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ywskyjb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#ywskyjb").attr("src", url);
	}
	// window.location.href = "frameset?__report=ywskyjb.rptdesign&startTime="+startTime+"&endTime="+endTime; 
} */

function zfyttjhzbSearch(){ //政法遗体汇总表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=zfyttjhzb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#zfyttjhzb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=zfyttjhzb.rptdesign&startTime="+startTime+"&endTime="+endTime+"&name="+name; */
}

function ytmdjfytjbSearch(){ //政法遗体火化遗体名单
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ytmdjfytjb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#ytmdjfytjb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=ytmdjfytjb.rptdesign&startTime="+startTime+"&endTime="+endTime+"&name="+name; */
}

function syclfytjbSearch(){ //医院委托火化处理费用表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //医院名称
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=yywt.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#yywt").attr("src", url);
	}
	/* window.location.href = "frameset?__report=syclfytjb.rptdesign&startTime="+startTime+"&endTime="+endTime+"&name="+name; */
}

function zbccapbSearch(){ //早班场次安排表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=zbccapb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#zbccapb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=zbccapb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function ltsyapbSearch(){ //礼厅司仪安排表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //司仪人员
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ltsyapb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#ltsyapb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=ltsyapb.rptdesign&startTime="+startTime+"&endTime="+endTime+"&name="+name; */
}

function funSearch(){ //礼仪出殡统计表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=fun.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#fun").attr("src", url);
	}
	/* window.location.href = "frameset?__report=zbccapb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function ghhSearch(){ //骨灰盒统计表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ghh.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#ghh").attr("src", url);
	}
	/* window.location.href = "frameset?__report=zbccapb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function ghhfwSearch(){ //骨灰盒统计表(非委托)
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ghhfw.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#ghhfw").attr("src", url);
	}
	/* window.location.href = "frameset?__report=zbccapb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function zgSearch(){ //纸棺统计表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=zg.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#zg").attr("src", url);
	}
	/* window.location.href = "frameset?__report=zbccapb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function szypSearch(){ //丧葬用品统计表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=szyp.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#szyp").attr("src", url);
	}
	/* window.location.href = "frameset?__report=zbccapb.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function danriFarewellWordOrderSearch(){ //告别厅安排danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriFarewellWordOrder.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriFarewellWordOrder").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriFarewellWordOrderDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriFarewellWordOrderSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriFarewellWordOrderSearch();
}
}

function danriMouringWordOrderSearch(){ //灵堂安排danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	//var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriMouringWordOrder.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriMouringWordOrder").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriMouringWordOrderDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriMouringWordOrderSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriMouringWordOrderSearch();
}
}

function danriCarList2Search(){ //车辆danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriCarList2.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriCarList2").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriCarList2Date(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriCarList2Search(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriCarList2Search();
} 
}

function danriTaskFuneralSearch(){ //丧葬用品danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriTaskFuneral.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriTaskFuneral").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function danriTaskFuneralDate(obj){ //时间改变
var move=$(obj).attr('data-change');
var target=$(obj).attr('data-tar');
var date=$('[data-tar='+target+']').val();
var day=new Date(date);
if(move==='go'){
	date=new Date(day-86400000).toLocaleDateString().replace(/\//g,'-');		
	$('[data-tar='+target+']').val(date);
	return danriTaskFuneralSearch(); 
}
if(move==='back'){
	date=new Date(day-0+86400000).toLocaleDateString().replace(/\//g,'-');
	$('[data-tar='+target+']').val(date);
	return danriTaskFuneralSearch();
} 
}

function farewellWordOrderSearch(){ //告别厅安排任务单
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=farewellWordOrder.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#farewellWordOrder").attr("src", url);
	}
	/* window.location.href = "frameset?__report=invoiceStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function mouringWordOrderSearch(){ //灵堂安排任务单
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=mouringWordOrder.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#mouringWordOrder").attr("src", url);
	}
	/* window.location.href = "frameset?__report=invoiceStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function zbzztjbSearch(){ //早班最早统计表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=zbzztjb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#zbzztjb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=invoiceStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function ltbststjbSearch(){ //灵堂摆设天数统计表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ltbststjb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#ltbststjb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=invoiceStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function ddlyzbhzbSearch(){ //调度礼仪早班汇总表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=ddlyzbhzb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#ddlyzbhzb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=invoiceStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function skgbthljdbSearch(){ //三科告别厅和炉校对表
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=skgbthljdb.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#skgbthljdb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function danrenFTTSearch(){ //单人遗体解冻
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danrenFTT.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danrenFTT").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function danrenCDTSearch(){ //单人遗体处理
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danrenCDT.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danrenCDT").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}

function bzygszsjSearch(){ //殡葬严管逝者数据表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var area = $("#area").val(); //地区
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && area == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=bzygszsj.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&area="+area;
		$("#bzygszsj").attr("src", url);
	}
	/* window.location.href = "frameset?__report=baseReduction.rptdesign&startTime="+startTime+"&endTime="+endTime+"&name="+name+"&area="+area; */
}

function danriZGRecodeSearch(){ //纸棺校对表danri
	var startTime = $("#startTime").val(); //开始时间
	//var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" ){
		return false;
	}else{
		var url = projectName+"/frameset?__report=danriZGRecode.rptdesign&__parameterpage=false&startTime="+startTime;
		$("#danriZGRecode").attr("src", url);
	}
	/* window.location.href = "frameset?__report=corpseDealTime.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function sdltczhzbSearch(){ //试点街道灵堂出租汇总表
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=sdltczhzb.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime;
		$("#sdltczhzb").attr("src", url);
	}
	/* window.location.href = "frameset?__report=invoiceStatic.rptdesign&startTime="+startTime+"&endTime="+endTime; */
}
function tjmxSearch(){ //礼仪出殡
	//debugger;
	var startTime = $("#startTime").val(); //开始时间
	var endTime = $("#endTime").val(); //结束时间
	var name = $("#name").val(); //姓名
	var projectName = "${sessionScope.reportPath }";
	if(startTime == "" && endTime == "" && name == ""){
		return false;
	}else{
		var url = projectName+"/frameset?__report=tjmx.rptdesign&__parameterpage=false&startTime="+startTime+"&endTime="+endTime+"&name="+name;
		$("#tjmx").attr("src", url);
	}
}
</script>
<style>
.basicArea {
	border-radius: 6px;
	border: 1px rgb(169, 169, 169) solid;
	line-height: 30px;
	height: 30px;
	padding: 0 8px;
	width: 20%;
}
.hardArea {
	border-radius: 6px;
	border: 1px rgb(169, 169, 169) solid;
	line-height: 30px;
	height: 30px;
	padding: 0 8px;
	width: 20%;
}
</style>
<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
	<input name="method" value="list" type="hidden">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
						<div class="box-body">
						
							<%-- <div class="col-md-12">
								<label>时间：</label> 
								<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<label style="margin:8px;">至</label> 
								<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<button type="submit" class="btn btn-info btn-search btn-search-left">搜索</button>
							</div> --%>
							
					<!-- 电子遗像 -->
							<c:if test="${which=='elePortrait' }"> 
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-12">
												<label>日期：</label>
												<input type="text" data-id="beginDate" data-method='elePortraitDateChange' data-tar='elePortrait' onchange="elePortraitChangeDate(this)" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="time" name="time" value=""><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="elePortraitSearch()">
												<!-- <a data-method='elePortraitDateChange' data-tar='elePortrait' onclick="elePortraitChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='elePortraitDateChange' data-tar='elePortrait' onclick="elePortraitChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=elePortrait.cpt&op=view"></iframe> --%>
								<iframe id="elePortrait" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=elePortrait.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 电子遗像 danri-->
							<c:if test="${which=='danrielePortrait' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriElePortraitDateChange' data-tar='ElePortrait' onchange="danriElePortraitDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriElePortraitSearch()">
												 <a data-method='danriElePortraitDateChange' data-tar='ElePortrait' onclick="danriElePortraitDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriElePortraitDateChange' data-tar='ElePortrait' onclick="danriElePortraitDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriElePortrait" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriElePortrait.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 礼仪出殡 -->
							<c:if test="${which=='liyicb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value='<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> '><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value='<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> '><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>姓名：</label> 
												<input type="text" class="list_input " id='name' name='name' value='' placeholder='请输入'/><!-- <i class="fa text-red fa-middle"></i> -->
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="funeralSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=funeral.cpt&op=view"></iframe> --%>
								<iframe id="funeral" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=funeral.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 礼仪出殡 danri-->
							<c:if test="${which=='danriliyicb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriFuneralDateChange' data-tar='Funeral' onchange="danriFuneralDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-3">
												<label>姓名：</label> 
												<input type="text" class="list_input " id='name' name='name' value='' placeholder='请输入'/><!-- <i class="fa text-red fa-middle"></i> -->
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriFuneralSearch()">
												 <a data-method='danriFuneralDateChange' data-tar='Funeral' onclick="danriFuneralDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriFuneralDateChange' data-tar='Funeral' onclick="danriFuneralDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriFuneral" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriFuneral.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 发票统计 -->
							<c:if test="${which=='faP' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="invoiceStaticSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=invoiceStatic.cpt&op=view"></iframe> --%>
								<iframe id="invoiceStatic" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=invoiceStatic.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 丧葬用品任务单 -->
							<c:if test="${which=='szyprwd' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="taskFuneralSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taskFuneral.cpt&op=view"></iframe> --%>
								<iframe id="taskFuneral" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=taskFuneral.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 遗体解冻时间安排 -->
							<c:if test="${which=='ytjdsjap' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>解冻时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="freezerThawTimeSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=freezerThawTime.cpt&op=view"></iframe> --%>
								<iframe id="freezerThawTime" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=freezerThawTime.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 遗体解冻安排 danri-->
							<c:if test="${which=='danriytjdsjap' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>解冻时间：</label> 
												 <%-- <input type="text" data-id="beginDate" data-method='danriFreezerThawTimeDateChange' data-tar='FreezerThawTime' onchange="danriFreezerThawTimeDate(this)" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriFreezerThawTimeSearch()">
												 <!-- <a data-method='danriFreezerThawTimeDateChange' data-tar='FreezerThawTime' onclick="danriFreezerThawTimeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriFreezerThawTimeDateChange' data-tar='FreezerThawTime' onclick="danriFreezerThawTimeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> --> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriFreezerThawTime" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriFreezerThawTime.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 遗体处理安排 -->
							<c:if test="${which=='ytclap' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="corpseDealTimeSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="corpseDealTime" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=corpseDealTime.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 遗体处理安排 danri-->
							<c:if test="${which=='danriytclap' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriCorpseDealTimeDateChange' data-tar='CorpseDealTime' onchange="danriCorpseDealTimeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriCorpseDealTimeSearch()">
												 <a data-method='danriCorpseDealTimeDateChange' data-tar='CorpseDealTime' onclick="danriCorpseDealTimeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriCorpseDealTimeDateChange' data-tar='CorpseDealTime' onclick="danriCorpseDealTimeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriCorpseDealTime" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriCorpseDealTime.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 火化统计 -->
							<c:if test="${which=='hhtj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="fireFurnaceSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=fireFurnace.cpt&op=view"></iframe> --%>
								<iframe id="fireFurnace" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=fireFurnace.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 收费统计报表 -->
							<c:if test="${which=='sftjbb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="charageSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=Charage.cpt&op=view"></iframe> --%>
								<iframe id="charage" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=charage.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 基本殡葬减免 -->
							<c:if test="${which=='jbbzjmtj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-5">
												<label>所在地：</label> 
												<!-- <select class="list_input" id="area" name="area" onChange="seclectValue()">
													<option selected="selected">请选择</option>
													<option value="">杭州</option>
													<option value="">上海</option>
												</select> -->
												<input type="text" class="basicArea" id='province' name='province' value='' placeholder='请输入'/> 省
												<input type="text" class="basicArea" id='city' name='city' value='' placeholder='请输入'/> 市
												<input type="text" class="basicArea" id='area' name='area' value='' placeholder='请输入'/> 区/县
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="baseReductionSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=baseReduction.cpt&op=view"></iframe> --%>
								<iframe id="baseReduction" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=baseReduction.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 灵堂统计 -->
							<c:if test="${which=='lttj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="mouringRecordSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=mouringRecord.cpt&op=view"></iframe> --%>
								<iframe id="mouringRecord" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=mouringRecord.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 告别厅统计 -->
							<c:if test="${which=='gbttj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="farewellSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=farewell.cpt&op=view"></iframe> --%>
								<iframe id="farewell" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=farewell.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 困难殡葬减免 -->
							<c:if test="${which=='knbzjmtj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-2">
												<label>姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-5">
												<label>所在地：</label> 
												<!-- <select class="list_input" id="area" name="area" onChange="seclectValue()">
													<option selected="selected">请选择</option>
													<option value="">杭州</option>
													<option value="">上海</option>
												</select> -->
												<input type="text" class="hardArea" id='province' name='province' value='' placeholder='请输入'/> 省
												<input type="text" class="hardArea" id='city' name='city' value='' placeholder='请输入'/> 市
												<input type="text" class="hardArea" id='area' name='area' value='' placeholder='请输入'/> 区/县
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="hardReductionSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=hardReduction.cpt&op=view"></iframe> --%>
								<iframe id="hardReduction" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=hardReduction.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 火化量统计(火化时间) -->
							<c:if test="${which=='hhltj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="areaFuneralStaticSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=areaFuneralStatic.cpt&op=view"></iframe> --%>
								<iframe id="areaFuneralStatic" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=areaFuneralStatic.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 火化量统计(预约火化时间) -->
							<c:if test="${which=='hhltjyy' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>预约火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="areaFuneralStaticyySearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=areaFuneralStatic.cpt&op=view"></iframe> --%>
								<iframe id="areaFuneralStaticyy" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=areaFuneralStaticyy.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 国税 -->
							<c:if test="${which=='guoshui' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='taxDateChange' data-tar='tax' onchange="taxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="taxChargeSearch()">
												<!-- <a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taxCharge.cpt&op=view"></iframe> --%>
								<iframe id="taxCharge" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=taxCharge.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 国税 LS-->
							<c:if test="${which=='guoshuiLS' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='taxDateChange' data-tar='tax' onchange="taxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="taxChargeLSSearch()">
												<!-- <a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taxCharge.cpt&op=view"></iframe> --%>
								<iframe id="taxChargeLS" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=taxChargeLS.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 国税(改16) -->
							<c:if test="${which=='guoshuigai' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='taxDateChange' data-tar='tax' onchange="taxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="taxChargeGAISearch()">
												<!-- <a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taxCharge.cpt&op=view"></iframe> --%>
								<iframe id="taxChargeGAI" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=taxChargeGAI.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 国税(改16)LS -->
							<c:if test="${which=='guoshuigaiLS' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='taxDateChange' data-tar='tax' onchange="taxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="taxChargeGAILSSearch()">
												<!-- <a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taxCharge.cpt&op=view"></iframe> --%>
								<iframe id="taxChargeGAILS" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=taxChargeGAILS.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 国税(改13) -->
							<c:if test="${which=='guoshuigai2' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='taxDateChange' data-tar='tax' onchange="taxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="taxChargeGAI2Search()">
												<!-- <a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taxCharge.cpt&op=view"></iframe> --%>
								<iframe id="taxChargeGAI2" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=taxChargeGAI2.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 国税(改13)LS -->
							<c:if test="${which=='guoshuigai2LS' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='taxDateChange' data-tar='tax' onchange="taxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="taxChargeGAI2LSSearch()">
												<!-- <a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='taxDateChange' data-tar='tax' onclick="taxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=taxCharge.cpt&op=view"></iframe> --%>
								<iframe id="taxChargeGAI2LS" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=taxChargeGAI2LS.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 调度科车辆列表 -->
							<c:if test="${which=='ddkcllb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="carListSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=carList.cpt&op=view"></iframe> --%>
								<iframe id="carList" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=carList.rptdesign&__parameterpage=false">
							</c:if>
					<!-- 冷藏柜进出统计(预约火化时间) -->
							<c:if test="${which=='lcgjcktj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate" data-method='freezerDateChange' data-tar='freezerStatic'  data-min-view="2" data-date-format="yyyy-m" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="freezerStaticSearch()">
												<!-- <a data-method='freezerDateChange' data-tar='freezerStatic' onclick="freezerChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='freezerDateChange' data-tar='freezerStatic' onclick="freezerChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=freezerStatic.cpt&op=view"></iframe> --%>
								<iframe id="freezerStatic" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=freezerStatic.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 冷藏柜进出统计(出库时间) -->
							<c:if test="${which=='lcgjcktjck' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate" data-method='freezerDateChange' data-tar='freezerStatic'  data-min-view="2" data-date-format="yyyy-m" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="freezerStaticCKSearch()">
												<!-- <a data-method='freezerDateChange' data-tar='freezerStatic' onclick="freezerChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='freezerDateChange' data-tar='freezerStatic' onclick="freezerChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=freezerStatic.cpt&op=view"></iframe> --%>
								<iframe id="freezerStaticCK" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=freezerStaticCK.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 非税 -->
							<c:if test="${which=='feishui' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='nonTaxDateChange' data-tar='nonTax' onchange="nonTaxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="nonTaxChargeSearch()">
												<!-- <a data-method='nonTaxDateChange' data-tar='nonTax' onclick="nonTaxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='nonTaxDateChange' data-tar='nonTax' onclick="nonTaxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%"  class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=nonTaxCharge.cpt&op=view"></iframe> --%>
								<iframe id="nonTaxCharge" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=nonTaxCharge.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 非税 -->
							<c:if test="${which=='feishuiLS' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='nonTaxDateChange' data-tar='nonTax' onchange="nonTaxChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="nonTaxChargeLSSearch()">
												<!-- <a data-method='nonTaxDateChange' data-tar='nonTax' onclick="nonTaxChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='nonTaxDateChange' data-tar='nonTax' onclick="nonTaxChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%"  class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=nonTaxCharge.cpt&op=view"></iframe> --%>
								<iframe id="nonTaxChargeLS" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=nonTaxChargeLS.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 纳骨送灵预约时间 -->
							<c:if test="${which=='ngslyysj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="boneSoulOrderSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=boneSoulOrder.cpt&op=view"></iframe> --%>
								<iframe id="boneSoulOrder" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=boneSoulOrder.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 非正常死亡单位证明报表 -->
							<c:if test="${which=='fzcswdwzmbb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="unormaldeadSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=Unormaldead.cpt&op=view"></iframe> --%>
								<iframe id="unormaldead" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=unormaldead.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 特约炉预约时间表 -->
							<c:if test="${which=='tylyysjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>炉号：</label> 
												<select class="list_input" id="code" name="code" onChange="seclectValue()">
													<!-- <option value="">请选择</option> -->
													<option value="0">请选择</option>
													<!-- <option value="1">1号</option>
													<option value="2">2号</option>
													<option value="3">3号</option>
													<option value="4">4号</option>
													<option value="5">5号</option>
													<option value="6">6号</option> -->
													<option value="7">7号</option>
													<option value="8">8号</option>
													<option value="9">9号</option>
													<option value="10">10号</option>
												</select>
												<!-- <input type="text" class="list_input" id='code' name='code' value='0' placeholder='请输入'/> -->
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="specialTimeSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=specialTime.cpt&op=view"></iframe> --%>
								<iframe id="specialTime" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=specialTime.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 特约炉预约时间表 danri-->
							<c:if test="${which=='danritylyysjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriSpecialTimeDateChange' data-tar='SpecialTime' onchange="danriSpecialTimeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-3">
												<label>炉号：</label> 
												<select class="list_input" id="code" name="code" onChange="seclectValue()">
													<!-- <option value="">请选择</option> -->
													<option value="0">请选择</option>
													<!-- <option value="1">1号</option>
													<option value="2">2号</option>
													<option value="3">3号</option>
													<option value="4">4号</option>
													<option value="5">5号</option>
													<option value="6">6号</option> -->
													<option value="7">7号</option>
													<option value="8">8号</option>
													<option value="9">9号</option>
													<option value="10">10号</option>
												</select>
												<!-- <input type="text" class="list_input" id='code' name='code' value='0' placeholder='请输入'/> -->
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriSpecialTimeSearch()">
												 <a data-method='danriSpecialTimeDateChange' data-tar='SpecialTime' onclick="danriSpecialTimeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriSpecialTimeDateChange' data-tar='SpecialTime' onclick="danriSpecialTimeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriSpecialTime" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriSpecialTime.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 租售花圈花篮统计 -->
							<c:if test="${which=='zshqhl' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="rentFlowerSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%"  class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=rentFlower.cpt&op=view"></iframe> --%>
								<iframe id="rentFlower" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=rentFlower.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 销卡记录查询 -->
							<c:if test="${which=='xkjlcx' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>旧卡号：</label> 
												<input type="text" class="list_input" id='card' name='card' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="pincardRecordSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=pincardRecord.cpt&op=view"></iframe> --%>
								<iframe id="pincardRecord" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=pincardRecord.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 政法部门的死者名单 -->
							<c:if test="${which=='zfbmdszmd' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>到馆日期：</label> 
												<%-- <input type="text" data-id="beginDate" data-method='zfbmdszmdDateChange' data-tar='zfbmdszmd' onchange="zfbmdszmdChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<%-- <label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<%-- <div class="col-md-12">
												<label>验尸申请日期：</label>
												<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="sTime" name="sTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="eTime" name="eTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div> --%>
											<div class="col-md-3">
												<label>协助人员：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="zfbmdszmdSearch()">
												<!-- <a data-method='zfbmdszmdDateChange' data-tar='zfbmdszmd' onclick="zfbmdszmdChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='zfbmdszmdDateChange' data-tar='zfbmdszmd' onclick="zfbmdszmdChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zfbmdszmd.cpt&op=view"></iframe> --%>
								<iframe id="zfbmdszmd" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=zfbmdszmd.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 解剖 -->
							<%-- <c:if test="${which=='jp' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate" data-method='jpDateChange' data-tar='jp' onchange="jpChangeDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/>  "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="jpSearch()">
												<a data-method='jpDateChange' data-tar='jp' onclick="jpChangeDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='jpDateChange' data-tar='jp' onclick="jpChangeDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a>
											</div>
										</div>
									</div>
								</div>
								<iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=jp.cpt&op=view"></iframe>
								<iframe id="jp" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=jp.rptdesign&__parameterpage=false"></iframe>
							</c:if> --%>
					<!-- 遗体处理一览表 -->
							<%-- <c:if test="${which=='ytclylb' }">
									<div class="col-md-12">
										<div class="box box-warning">
											<div class="box-header with-border">
												<h3 class="box-title" style="color:#4EC2F6">查询</h3>
												<div class="box-tools pull-right">
													<button type="button" class="btn btn-box-tool" data-widget="collapse">
														<i class="fa fa-chevron-down"></i>
													</button>
												</div>
											</div>
											<div class="box-body">
												<div class="col-md-3">
													<label>联系单位：</label> 
													<!-- <select class="list_input" id="name" name="name" onChange="seclectValue()">
														<option selected="selected">请选择</option>
														<option value="">杭州</option>
														<option value="">上海</option>
													</select> -->
													<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
												</div>
												<div class="col-md-12">
													<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
													<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ytclylbSearch()">
												</div>
											</div>
										</div>
									</div>
								<iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=ytclylb.cpt&op=view"></iframe>
								<iframe id="ytclylb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ytclylb.rptdesign&__parameterpage=false"></iframe>
							</c:if> --%>
					<!-- 冰柜遗体一览表 -->
							<c:if test="${which=='bgytylb' }">
								<%-- <div class="col-md-12">
									<label>时间：</label> 
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<label style="margin:8px;">至</label> 
									<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${cremationTime}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
									<button type="submit" class="btn btn-info btn-search btn-search-left">搜索</button>
								</div> --%>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=bgytylb.cpt&op=view"></iframe> --%>
								<iframe id="bgytylb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=bgytylb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 火化情况统计表(火化时间) -->
							<c:if test="${which=='hhqktjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="hhqktjbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=hhqktjb.cpt&op=view"></iframe> --%>
								<iframe id="hhqktjb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=hhqktjb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 火化情况统计表(预约火化时间) -->
							<c:if test="${which=='hhqktjbyy' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>预约火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="hhqktjbyySearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=hhqktjb.cpt&op=view"></iframe> --%>
								<iframe id="hhqktjbyy" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=hhqktjbyy.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 被火化人员登记台卡(火化时间) -->
							<c:if test="${which=='bhhrydjtk' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="bhhrydjtkSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=bhhrydjtk.cpt&op=view"></iframe> --%>
								<iframe id="bhhrydjtk" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=bhhrydjtk.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 被火化人员登记台卡(预约火化时间) -->
							<c:if test="${which=='bhhrydjtkyy' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>预约火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="bhhrydjtkyySearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=bhhrydjtk.cpt&op=view"></iframe> --%>
								<iframe id="bhhrydjtkyy" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=bhhrydjtkyy.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 火化人员数据汇总表 (火化日期)-->
							<c:if test="${which=='hhrysjhzb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-12">
												<label>火化预约时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="csTime" name="csTime" value=" "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="ceTime" name="ceTime" value=" "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="hhrysjhzbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=hhrysjhzb.cpt&op=view"></iframe> --%>
								<iframe id="hhrysjhzb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=hhrysjhzb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 火化人员数据汇总表 (预约火化日期)-->
							<c:if test="${which=='hhrysjhzbyy' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>预约火化时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<!-- <div class="col-md-12">
												<label>火化预约时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="csTime" name="csTime" value=" "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="ceTime" name="ceTime" value=" "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div> -->
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="hhrysjhzbyySearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=hhrysjhzb.cpt&op=view"></iframe> --%>
								<iframe id="hhrysjhzbyy" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=hhrysjhzbyy.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 业务三科业绩表 -->
							<%-- <c:if test="${which=='ywskyjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ywskyjbSearch()">
											</div>
										</div>
									</div>
								</div>
								<iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=ywskyjb.cpt&op=view"></iframe>
								<iframe id="ywskyjb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ywskyjb.rptdesign&__parameterpage=false"></iframe>
							</c:if> --%>
					<!-- 政法遗体统计汇总表 -->
							<c:if test="${which=='zfyttjhzb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>经办单位：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="zfyttjhzbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zfyttjhzb.cpt&op=view"></iframe> --%>
								<iframe id="zfyttjhzb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=zfyttjhzb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 政法遗体火化遗体名单 -->
							<c:if test="${which=='ytmdjfytjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>经办单位：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ytmdjfytjbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=ytmdjfytjb.cpt&op=view"></iframe> --%>
								<iframe id="ytmdjfytjb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ytmdjfytjb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 医院委托火化处理费用统计表 -->
							<c:if test="${which=='syclfytjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>医院名称：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="syclfytjbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=syclfytjb.cpt&op=view"></iframe> --%>
								<iframe id="yywt" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=yywt.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 早班场次安排表 -->
							<c:if test="${which=='zbccapb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="zbccapbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zbccapb.cpt&op=view"></iframe> --%>
								<iframe id="zbccapb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=zbccapb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
				<!-- 礼厅司仪人员 -->
							<c:if test="${which=='ltsyapb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>礼厅人员：</label> 
												<input type="text" class="list_input" id='name' name='name' value='' placeholder='请输入'/>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ltsyapbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=ltsyapb.cpt"></iframe> --%>
								<iframe id="ltsyapb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ltsyapb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
						<!-- 礼仪出殡统计表 -->
							<c:if test="${which=='fun' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="funSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zbccapb.cpt&op=view"></iframe> --%>
								<iframe id="fun" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=fun.rptdesign&__parameterpage=false"></iframe>
							</c:if>
						<!-- 骨灰盒统计表(委托) -->
							<c:if test="${which=='ghh' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ghhSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zbccapb.cpt&op=view"></iframe> --%>
								<iframe id="ghh" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ghh.rptdesign&__parameterpage=false"></iframe>
							</c:if>
						<!-- 骨灰盒统计表(非委托) -->
							<c:if test="${which=='ghhfw' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ghhfwSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zbccapb.cpt&op=view"></iframe> --%>
								<iframe id="ghhfw" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ghhfw.rptdesign&__parameterpage=false"></iframe>
							</c:if>
						<!-- 纸棺统计表 -->
							<c:if test="${which=='zg' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="zgSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zbccapb.cpt&op=view"></iframe> --%>
								<iframe id="zg" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=zg.rptdesign&__parameterpage=false"></iframe>
							</c:if>	
						<!-- 丧葬用品统计表 -->
							<c:if test="${which=='szyp' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="szypSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=zbccapb.cpt&op=view"></iframe> --%>
								<iframe id="szyp" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=szyp.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 告别厅安排 danri-->
							<c:if test="${which=='danrigbtaprwd' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriFarewellWordOrderDateChange' data-tar='FarewellWordOrder' onchange="danriFarewellWordOrderDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriFarewellWordOrderSearch()">
												 <a data-method='danriFarewellWordOrderDateChange' data-tar='FarewellWordOrder' onclick="danriFarewellWordOrderDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriFarewellWordOrderDateChange' data-tar='FarewellWordOrder' onclick="danriFarewellWordOrderDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriFarewellWordOrder" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriFarewellWordOrder.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 灵堂安排 danri-->
							<c:if test="${which=='danriltaprwd' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriMouringWordOrderDateChange' data-tar='MouringWordOrder' onchange="danriMouringWordOrderDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriMouringWordOrderSearch()">
												 <a data-method='danriMouringWordOrderDateChange' data-tar='MouringWordOrder' onclick="danriMouringWordOrderDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriMouringWordOrderDateChange' data-tar='MouringWordOrder' onclick="danriMouringWordOrderDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriMouringWordOrder" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriMouringWordOrder.rptdesign&__parameterpage=false"></iframe>
							</c:if>	
					<!-- 车辆 danri-->
							<c:if test="${which=='danriclapb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriCarList2DateChange' data-tar='CarList2' onchange="danriCarList2Date(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriCarList2Search()">
												 <a data-method='danriCarList2DateChange' data-tar='CarList2' onclick="danriCarList2Date(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriCarList2DateChange' data-tar='CarList2' onclick="danriCarList2Date(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriCarList2" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriCarList2.rptdesign&__parameterpage=false"></iframe>
							</c:if>	
					<!-- 丧葬用品 danri-->
							<c:if test="${which=='danriszyprwd' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriTaskFuneralDateChange' data-tar='TaskFuneral' onchange="danriTaskFuneralDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriTaskFuneralSearch()">
												 <a data-method='danriTaskFuneralDateChange' data-tar='TaskFuneral' onclick="danriTaskFuneralDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='danriTaskFuneralDateChange' data-tar='TaskFuneral' onclick="danriTaskFuneralDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a> 
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriTaskFuneral" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriTaskFuneral.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 告别厅安排任务单 -->
							<c:if test="${which=='gbtaprwd' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="farewellWordOrderSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=invoiceStatic.cpt&op=view"></iframe> --%>
								<iframe id="farewellWordOrder" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=farewellWordOrder.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 灵堂安排任务单 -->
							<c:if test="${which=='ltaprwd' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="mouringWordOrderSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=invoiceStatic.cpt&op=view"></iframe> --%>
								<iframe id="mouringWordOrder" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=mouringWordOrder.rptdesign&__parameterpage=false"></iframe>
							</c:if>	
					<!-- 早班最早统计表 -->
							<c:if test="${which=='zbzztjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="zbzztjbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=invoiceStatic.cpt&op=view"></iframe> --%>
								<iframe id="zbzztjb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=zbzztjb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 灵堂摆设天数统计表 -->
							<c:if test="${which=='ltbststjb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ltbststjbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=invoiceStatic.cpt&op=view"></iframe> --%>
								<iframe id="ltbststjb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ltbststjb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 调度礼仪早班汇总表 -->
							<c:if test="${which=='ddlyzbhzb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="ddlyzbhzbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=invoiceStatic.cpt&op=view"></iframe> --%>
								<iframe id="ddlyzbhzb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=ddlyzbhzb.rptdesign&__parameterpage=false"></iframe>
							</c:if>	
					<!-- 三科告别厅和炉校对表-->
							<c:if test="${which=='skgbthljdb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='skgbthljdbDateChange' data-tar='skgbthljdb' onchange="skgbthljdbDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="skgbthljdbSearch()">
												 <!-- <a data-method='skgbthljdbDateChange' data-tar='skgbthljdb' onclick="skgbthljdbDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='skgbthljdbDateChange' data-tar='skgbthljdb' onclick="skgbthljdbDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a>  -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="skgbthljdb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=skgbthljdb.rptdesign&__parameterpage=false"></iframe>
							</c:if>	
					<!-- 单人遗体解冻 -->
							<c:if test="${which=='danrenFTT' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danrenFTTDateChange' data-tar='danrenFTT' onchange="danrenFTTDate(this)" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danrenFTTSearch()">
												 <!-- <a data-method='skgbthljdbDateChange' data-tar='skgbthljdb' onclick="skgbthljdbDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='skgbthljdbDateChange' data-tar='skgbthljdb' onclick="skgbthljdbDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a>  -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danrenFTT" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danrenFTT.rptdesign&__parameterpage=false"></iframe>
							</c:if>			
						
				<!-- 单人遗体处理-->
							<c:if test="${which=='danrenCDT' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danrenCDTDateChange' data-tar='danrenCDT' onchange="danrenCDTDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danrenCDTSearch()">
												 <!-- <a data-method='skgbthljdbDateChange' data-tar='skgbthljdb' onclick="skgbthljdbDate(this)"  data-change='go' class="btn btn-info btn-search" role="button">前一天</a>
												<a data-method='skgbthljdbDateChange' data-tar='skgbthljdb' onclick="skgbthljdbDate(this)"  data-change='back' class="btn btn-info btn-search" role="button">后一天</a>  -->
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danrenCDT" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danrenCDT.rptdesign&__parameterpage=false"></iframe>
							</c:if>	
							
						<!-- 殡葬严管逝者数据表 -->
							<c:if test="${which=='bzygszsj' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>登记时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											
											<div class="col-md-5">
												<label>逝者地区：</label> 
												<!-- <select class="list_input" id="area" name="area" onChange="seclectValue()">
													<option selected="selected">请选择</option>
													<option value="">杭州</option>
													<option value="">上海</option>
												</select> -->
												<!-- <input type="text" class="basicArea" id='province' name='province' value='' placeholder='请输入'/> 省
												<input type="text" class="basicArea" id='city' name='city' value='' placeholder='请输入'/> 市 -->
												<input type="text" class="basicArea" id='area' name='area' value='' placeholder='请输入'/> 区/县
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="bzygszsjSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=baseReduction.cpt&op=view"></iframe> --%>
								<iframe id="bzygszsj" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=bzygszsj.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 纸棺校对表 danri-->
							<c:if test="${which=='danriZGRecode' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												 <input type="text" data-id="beginDate" data-method='danriElePortraitDateChange' data-tar='ElePortrait' onchange="danriElePortraitDate(this)" data-min-view="2" data-date-format="yyyy-m-d" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-M-d"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<%-- <input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i> --%>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="danriZGRecodeSearch()">
												
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=corpseDealTime.cpt&op=view"></iframe> --%>
								<iframe id="danriZGRecode" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=danriZGRecode.rptdesign&__parameterpage=false"></iframe>
							</c:if>
						<!-- 试点街道灵堂出租汇总表 -->
							<c:if test="${which=='sdltczhzb' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>到馆时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value="<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> "><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="sdltczhzbSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=invoiceStatic.cpt&op=view"></iframe> --%>
								<iframe id="sdltczhzb" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=sdltczhzb.rptdesign&__parameterpage=false"></iframe>
							</c:if>
					<!-- 统计明细 -->
							<c:if test="${which=='tjmx' }">
								<div class="col-md-12">
									<div class="box box-warning">
										<div class="box-header with-border">
											<h3 class="box-title" style="color:#4EC2F6">查询</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-chevron-down"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12">
												<label>时间：</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="startTime" name="startTime" value='<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> '><i style="margin-left: -20px;" class="fa fa-calendar"></i>
												<label style="margin:8px;">至</label> 
												<input type="text" data-id="beginDate"  data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select" id="endTime" name="endTime" value='<fmt:formatDate value="${date }" pattern="yyyy-MM-dd"/> '><i style="margin-left: -20px;" class="fa fa-calendar"></i>
											</div>
											<div class="col-md-3">
												<label>死者姓名：</label> 
												<input type="text" class="list_input " id='name' name='name' value='' placeholder='请输入'/><!-- <i class="fa text-red fa-middle"></i> -->
											</div>
											<div class="col-md-12">
												<!-- <button type="submit" class="btn btn-info btn-search">搜索</button> -->
												<input type="button" class="btn btn-info btn-search" value="搜索" onclick="tjmxSearch()">
											</div>
										</div>
									</div>
								</div>
								<%-- <iframe id="reportFrame" width="100%" class='frame' src="${sessionScope.reportPath }WebReport/ReportServer?reportlet=funeral.cpt&op=view"></iframe> --%>
								<iframe id="tjmx" width="100%" class='frame' src="${sessionScope.reportPath }/frameset?__report=tjmx.rptdesign&__parameterpage=false"></iframe>
							</c:if>
							</div>		
						</div>		
						</div>
				</div>
			</div>
		</div>
	</section>
</form>
