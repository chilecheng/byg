<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
      <div class="error-page" style="padding-top: 150px;">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> 抱歉，您访问的页面不存在...</h3>

          <p>
          	您要查看的网页不存在或暂不可用.<br>
			点击下列链接继续浏览网页或联系管理员<br>
			<i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;<a href="javascript :;" onClick="javascript :history.back(-1);">返回上一页</a><br>
			<i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;<a href="login.do?method=main">返回首页</a>
          </p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
