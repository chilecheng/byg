<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
      <div class="error-page" style="padding-top: 150px;">
        <h2 class="headline text-red">500</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> 抱歉，服务器发生错误！</h3>

          <p>
          	您要查看的网页出现错误.<br>
			点击下列链接继续浏览网页或联系管理员<br>
			<i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;<a href="javascript :;" onClick="javascript :history.back(-1);">返回上一页</a><br>
			<i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;<a href="login.do?method=main">返回首页</a>
          </p>

        </div>
      </div>
      <!-- /.error-page -->
