<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">错误</div>
</section>

    <div class="pad margin no-print">
      <div class="callout callout-warning" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> 错误内容:</h4>
        &nbsp;&nbsp;&nbsp;&nbsp;${message }
      </div>
    </div>
