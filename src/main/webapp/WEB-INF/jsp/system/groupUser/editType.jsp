<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${type.id}">
		<input type="hidden" name="typeold" value="${type.type}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<c:if test="${tp == 1 }">
						添加分组
					</c:if>
					<c:if test="${tp == 2 }">
						修改分组
					</c:if>
				</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">编号:</td>
						<c:if test="${tp == 1 }">
							<td><input type="text" class="required form-control" id="type" name="type" value=""></td>
						</c:if>
						<c:if test="${tp == 2 }">
							<td><input type="text" class="required form-control" id="type" name="type" value="${type.type }"  readonly="readonly"></td>
						</c:if>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">组名:</td>
						<td><input type="text" name="name" class="required form-control" id="name" value="${type.name}"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>