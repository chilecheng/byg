<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<style>
		.modal-body{
    		overflow-y: scroll;
		}
		.tableUser{
			width:100%
		}
		
		.tableUser tr td,.tableUser tr th{
		    width:100px;
		    line-height:30px;
		    text-align:center;
		}
		.tableUser tr td:first-child,.tableUser tr th:first-child{
			width:30px;		
		}
/* 		.tableUser tr th{
			background-color:#ccc;
		}
 */	</style>
	<script>
	function addUserHere(obj) {
			var checkName = $(obj).attr("checkName");
			var checkOne = $(obj).attr("checkOne");
			var rel = $(obj).attr("rel");
			if(checkName!=undefined){
				var ids = "";
				var num = 0;
				$("input[name='"+checkName+"']:checked").each(function(){
					var id = $(this).val();
					if(ids!=""){
						ids+=",";
					}
					ids+=id;
					num++;
				});
				if(checkOne){
					if(num!=1){
						toastr["warning"]("请选择一条记录");
						return false;
					}
				}else{
					if(num<1){
						toastr["warning"]("请选择记录");
						return false;
					}
				}
				var href= "${url}?method=${method}&gtype=${type}&ids=" + ids;
			}
			if(href==""){
				return false;
			}
			$.ajax({
				url:href,
				async:false,
				type:"POST",
				dataType:"json",
				success:function(json){
					if (json.statusCode == 200) {
						toastr["success"](json.message);
						$("#" + rel).modal('hide');
						if (json.isRefresh) {
							setTimeout('homeRefreshNull()', 500);
						}
					} else {
						toastr["error"](json.message);
					}
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {  
					if(XMLHttpRequest.status==500){
						toastr["error"]("程序内部出现错误"); 
					}else if(XMLHttpRequest.status==404){
						toastr["error"]("页面不存在"); 
					}else{
						toastr["error"](XMLHttpRequest.status); 
					}
				}
			});
	}
	</script>
	
	<%-- <form action="${url}" rel="myModal"  onsubmit="return adduser(this);" > --%>
		<input type="hidden" name="method" value="${method}" >
		<input type="hidden" name="gtype" value="${type}" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">为分组添加用户</h4> 
			</div>
			<div class="modal-body" style="height:200px;" >
				<table  class="table table-bordered table-hover tableUser" >
					<thead>
						<tr role="row">
							<th width="100px"><input type="checkbox" class="checkBoxCtrl" group="userIds" /></th>
							<th width="100px">用户名</th>
							<th width="100px">真实姓名</td>
						</tr>
					</thead>	
					<c:choose>
					    <c:when test="${fn:length(list)==0 }">
						    <tr>
				    			<td colspan="3">
							  		<font color="Red">还没有数据</font>
						 		</td>
						    </tr>
					    </c:when>
					    <c:otherwise>
						    <c:forEach items="${list }" var="u">
								<tr role="row">
									<td><input type="checkbox" name="userIds" value="${u.userId}"></td>
			    					<td width="100px">${u.userName }</td>
			    					<td width="100px">${u.name }</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<a href="javascript:void(0);" checkName="userIds" onclick="addUserHere(this)" rel="myModal">
					<button class="btn btn-info btn-margin">保存</button>
				</a>
				<%-- <a href="${url}?method=${method}&gtype=${type}&ids=" target="dialog" checkName="userIds" rel="myModal" warm="确认删除吗" class="btn btn-primary" role="button">
					<button class="btn btn-primary">保存</button>
				</a> --%>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	<!-- </form> -->
	
</div>