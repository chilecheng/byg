<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">修改用户组信息</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>组名：</label> 
							<input type="text" class="list_input" name="name" value="${name}">
						</div>
						<%-- <div class="col-md-7">
							<label>是否启用：</label> 
							<select class="list_select" name="isdel">
								${isdelOption}
							</select>
						</div> --%>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search" style="line-height: 15px">搜索</button>
						</div>
					</div>
				</form>
				
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border btns-buy">
					<a href="${url}?method=edit&tp=1" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加分组</a>
					<a href="${url}?method=edit&tp=2&id=" target="dialog" checkName="userId" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>
					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="userId" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
					<%-- <small class="pull-right">
					<a href="${url}?method=isdel&isdel=${Isdel_No }&id=" target="ajaxTodo" checkName="userId"   warm="确认启用吗" class="btn btn-success" role="button">启用</a>
					<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="userId"  warm="确认禁用吗" class="btn btn-danger " role="button">禁用</a>
					</small> --%>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="userId" /></th>
											<th>编号</th>
											<th>组名</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="6">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="type">
											<tr role="row">
												<td><input type="checkbox" name="userId" value="${type.id}"></td>
												<td><font >${type.type }</font></td>
												<td><a href="groupDetail.do?type=${type.type}" target="homeTab" ><font color="blue">${type.name }</font></a></td>
											</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
