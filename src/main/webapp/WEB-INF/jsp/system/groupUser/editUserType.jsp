<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<script>
		$(function() {
			$('#gname').val(0);
		})
		function changeGroup(id) {
			var y=document.getElementById(id).value;
			$('#gname').val(y);
		}
	</script>
	<form action="${url}" rel="myModal"  onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="ids" value="${userId }">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">修改分组</h4> 
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">分组编号:</td>
						<td>
							<select id="gtype" class="list_select" style="width:150px" name="gtype" onchange="changeGroup(this.id)" >
								<c:forEach items="${map }" var="m">
									<c:choose>
										<c:when test="${m.key==type}">
											<option value="${m.key}" selected>${m.key}</option>
										</c:when>									
										<c:otherwise>
											<option value="${m.key}">${m.key}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">组名:</td>
						<td>
							<select class="list_select" style="width:150px" name="groupName"   id="gname" >
								<c:forEach items="${map }" var="m">
									<c:choose>
										<c:when test="${m.key==type}">
											<option value="${m.key}" selected>${m.value}</option>
										</c:when>									
										<c:otherwise>
											<option value="${m.key}">${m.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>