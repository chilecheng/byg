<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="userId" value="${user.userId}">
		<div class="modal-content">
			<div class='row'>
				<div class='col-md-6 border-R height-500'>
					<section class="content-header content-header-S">
						<i class="title-line"></i>
						<div class="title" id="myModalLabel">设置职位</div>
					</section>
					<div class='panel-group panel-group-S'  id='accordion4'>
						<c:set var="dept" value=""/>
						<c:forEach items="${list }" var="u">
						<c:if test="${!(dept eq u.deptId) }">
							<div class="box box-warning nomargin-B">
								<div class="box-header with-border">
						             <h3 class="box-title">${u.dept.name }</h3>
						        </div>
						        
							</div>
						</c:if>
						
						 <div  class='checkbox checkbox-S'><label><input type='checkbox' name="roleIds"  value='${u.id }' <c:if test="${u.isCheck >0 }">checked="checked"</c:if>/>${u.name}</label></div>   
						     	
<!-- 						<div class="col-sm-3" style="height: 30px;border: 1px solid #c0c0c0;line-height: 30px;"> -->
<%-- 							<input type="checkbox" name="roleIds" value="${u.id }" <c:if test="${u.isCheck >0 }">checked="checked"</c:if>> --%>
<%-- 							${u.name} --%>
<!-- 						</div> -->
						<c:set var="dept" value="${u.deptId }"/>
						</c:forEach>
					</div>
				</div>
				<div class='col-md-6 height-500 margin-L-S'>
							<section class="content-header content-header-S">
								<i class="title-line"></i>
								<div class="title">已选职位</div>
							</section>
							<div class='chosed-item panel-group-S'>
								<div class='box box-warning' id='chosed'>
<!-- 									<p class='item-txt'>111111111<span class='item-del'>x</span></p> -->
								</div>
							</div>
						</div>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>
<script>
	(function(){
		$('.panel-group input:checkbox').click(function(){
			if($(this).is(':checked')){
				$('#chosed').append("<p class='item-txt' data-value='"+$(this).val()+"'><a>"+$(this).parent().text()+"</a><span class='item-del'>x</span></p>")
			}else{
				$("#chosed p[data-value='"+$(this).val()+"']").remove();
			}
		});
		$('#chosed').on('click','.item-del',function(){
			$(".panel-group input[value='"+$(this).parent().attr('data-value')+"']").removeAttr('checked');
			$(this).parent().remove();
		});
	})();
</script>	
