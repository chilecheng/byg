<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">用户</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-3">
							<label>用户名：</label> 
							<input type="text" class="list_input" name="userName" value="${userName}">
						</div>
						<div class="col-md-2">
							<label>部门：</label> 
							<select class="list_select " name="dept" id="dept" value="">
								${deptOption}
							</select>
						</div>
						<div class="col-md-5">
							<label>是否启用：</label> 
							<select class="list_select " name="isdel">
								${isdelOption}
							</select>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search" >搜索</button>
						</div>
					</div>
				</form>
				
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-print btns-buy'>
					<a href="${url}?method=edit&tp=2" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
					<a href="${url}?method=edit&tp=1&userId=" target="dialog" checkName="userId" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>
					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="userId" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
					<a href="${url}?method=listUserRole&userId=" target="dialog" checkName="userId" checkOne="true" rel="myModal" class="btn btn-color-ab47bc" role="button">设置职位</a>
					</small>
					<small class="pull-right btns-buy">
					<a href="${url}?method=isdel&isdel=${Isdel_No }&id=" target="ajaxTodo" checkName="userId"   warm="确认启用吗" class="btn btn-info" role="button">启用</a>
					<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="userId"  warm="确认禁用吗" class="btn btn-danger " role="button">禁用</a>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="userId" /></th>
											<th>编号</th>
											<th>用户名</th>
											<th>真实姓名</th>
											<th>联系方式</th>
											<th>电话</th>
											<th>性别</th>
											<th>职位</th>
											<th>部门</th>
											<th>创建时间</th>
											<th>是否启用</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="11">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="userId" value="${u.userId}"></td>
												<td><font color="blue">${u.code }</font></td>
						    					<td>${u.userName }</td>
						    					<td>${u.name }</td>
						    					<td>${u.tel }</td>
						    					<td>${u.phone }</td>
						    					<td>${u.sexStr}</td>
						    					<td>${u.roleNames}</td>
						    					<td>${u.deptName}</td>
						    					<td><fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd"/></td>
						    					<td>
													<c:if test="${u.isdel==Isdel_Yes }">
							    					<font color="red" class='state1'>${u.isdelName }</font>
							    					</c:if>
							    					<c:if test="${u.isdel==Isdel_No }">
							    					<font color="green" class='state1'>${u.isdelName }</font>
							    					</c:if>
												</td>
											</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
