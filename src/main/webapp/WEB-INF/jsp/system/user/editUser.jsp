<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method}" >
		<input type="hidden" name="userId" value="${user.userId}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<c:if test="${tp == 1 }">
						修改用户
					</c:if>
					<c:if test="${tp == 2 }">
						添加用户
					</c:if>
				</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">编号:</td>
						<c:if test="${tp == 1 }">
							<td><input type="text" class="required integer form-control" id="code" name="code" value="${user.code }"></td>
						</c:if>
						<c:if test="${tp == 2 }">
							<td><input type="text" class="required integer form-control" id="code" name="code" value="${user.code }"></td>
						</c:if>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">用户名:</td>						
						<td>
						
						<input type="text" name="userName" class="required form-control" id="userName" value="${user.userName}">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<c:if test="${tp == 1 }">
						<td width="100px">密码:</td>
						<td  style='position:relative'>
						<input type="password" name="faeiergt" class="form-control" style='position:absolute;bottom:-7px;'><input type="password" class="form-control" name="fajeilhjiioaeg"  style='position:absolute;bottom:-7px;'>
						<input type="password" name="password" class="required form-control" id="password" value="${user.password}"   style='position:absolute;bottom:-7px;'></td>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</c:if>
					<tr>
						<td width="100px">所属部门:</td>
						<td>
							<select class="list_select" style="width:150px" name="deptId"  >
								<c:forEach items="${dList }" var="d">
									<c:choose>
										<c:when test="${d.name==dname}">
											<option value="${d.id}" selected>${d.name}</option>
										</c:when>									
										<c:otherwise>
											<option value="${d.id}">${d.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">真实姓名:</td>
						<td><input type="text" name="name" class="required form-control" value="${user.name}"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">联系方式:</td>
						<td><input type="text" name="tel" class="form-control" value="${user.tel}"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">电话:</td>
						<td><input type="text" name="phone" class="required form-control"  value="${user.phone}"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">用户性别:</td>
						<td>
							<select class="list_select" style="width: 150%" name="sex">
								${sexOption}
							</select>
						</td>
					</tr>
				
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>