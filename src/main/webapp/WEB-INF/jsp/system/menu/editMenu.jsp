<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${menu.id}">
		<input type="hidden" name="menuId" value="${fatherMenu.id}" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
				<c:if test="${id ne '' && id ne null }">修改菜单</c:if>
				<c:if test="${id eq '' || id eq null }">添加菜单</c:if>
				</h4>
			</div>
			<div class="modal-body">
				<table>
				<c:if test="${fatherMenu.id!=null&&fatherMenu.id!=''}">
		      		<tr>
		      			<td width="100px">上级菜单名称:  </td>
		      			<td><input type="text"  class="form-control" readonly="readonly" value="${fatherMenu.name }">  </td>
		      		</tr>
	      		</c:if>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">菜单名称:</td>
						<td><input type="text" class="required form-control"  id="name" name="name" value="${menu.name }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">地址:</td>
						<td><input type="text" class="form-control"  id="url" name="url" value="${menu.url }"></td>
					</tr>
					<%-- <tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">权限:</td>
						<td><input type="text" class="form-control" id="powerStr" name="powerStr" value="${menu.powerStr }"></td>
					</tr> --%>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">图标:</td>
						<td><input type="text" class="form-control"  id="icon" name="icon" value="${menu.icon }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">顺序号:</td>
						<td><input type="text" class="integer form-control" name="indexFlag" id="indexFlag" value="${menu.indexFlag}"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>