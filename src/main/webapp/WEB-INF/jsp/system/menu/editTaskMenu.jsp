<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${menu.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
				<c:if test="${id ne '' && id ne null }">修改任务菜单</c:if>
				<c:if test="${id eq '' || id eq null }">添加任务菜单</c:if>
				</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="100px">任务菜单名称:</td>
						<td><input type="text" class="required form-control"  id="name" name="name" value="${menu.name }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">地址:</td>
						<td><input type="text" class="form-control"  id="url" name="url" value="${menu.url }"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>