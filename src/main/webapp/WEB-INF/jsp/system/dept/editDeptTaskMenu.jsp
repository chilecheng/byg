<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<head>
</head>
<body>
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method}" >
		<input type="hidden" name="deptId" value="${deptId}">
		<div class="modal-dialog" role="document" style="margin-top: 103.5px;">
			<div class="modal-content">
				<div class='row'>
					<div class='col-md-6 border-R height-500'>
						<section class="content-header content-header-S">
							<i class="title-line"></i>
							<div class="title">任务列表选择</div>
						</section>
						<div class='panel-group panel-group-S' role="tablist" id='accordion3'>
							<div class="box box-warning">
					            <div class="box-header with-border">
					              <h3 class="box-title">任务列表</h3>
					              <div class="box-tools">
					                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					                </button>
					              </div>
					            </div>
					            <div class="box-body no-padding">
					              <ul class="nav nav-pills nav-stacked">
									<c:forEach items="${list}" var="u">
										<li class='checkbox checkbox-S'><label><input type='checkbox' ${u.deptMenuNum > 0 ? "checked" : "" } name='menuId' value='${u.id }'/>${u.name }</label></li>
					              	</c:forEach>
					              </ul>
					            </div>
					          </div>
						</div>
					</div>
					<div class='col-md-6 height-500 margin-L-S'>
						<section class="content-header content-header-S">
							<i class="title-line"></i>
							<div class="title">已选项目</div>
						</section>
						<div class='chosed-item panel-group-S'>
							<div class='box box-warning' id='chosed'>
							</div>
						</div>
					</div>
				</div>
				<div class='row nomargin'>
					<div class='col-md-12 wk-btns'>
						<button type="submit"  class="btn btn-info" data-submit='submit' >确定</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">返回</button>
					</div>
				</div>
			
			</div>
		</div>
	</form>
<script>
	(function(){
		$('.panel-group input[checked]').each(function(){
			$('#chosed').append("<p class='item-txt' data-value='"+$(this).val()+"'><a>"+$(this).parent().text()+"</a><span class='item-del'>x</span></p>")

		})
		$('.panel-group input:checkbox').click(function(){
			if($(this).is(':checked')){
				$('#chosed').append("<p class='item-txt' data-value='"+$(this).val()+"'><a>"+$(this).parent().text()+"</a><span class='item-del'>x</span></p>")
			}else{
				$("#chosed p[data-value='"+$(this).val()+"']").remove();
			}
		});
		$('#chosed').on('click','.item-del',function(){
			$(".panel-group input[value='"+$(this).parent().attr('data-value')+"']").removeAttr('checked');
			$(this).parent().remove();
		});
	})();
</script>	
</body>