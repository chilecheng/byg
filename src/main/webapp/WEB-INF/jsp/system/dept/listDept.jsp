<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">部门</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>名称：</label> 
							<input type="text" class="list_input" id="name" name="name" value="${name}">
						</div>
						<div class="col-md-6">
							<label>是否启用：</label> 
							<select class="list_select nopadding-R" name="isdel" id="isdel">
								${isdelOption}
							</select>
						</div>
						<div class="col-md-6">
							<label>部门：</label> 
							<select class="list_select nopadding-R" name="dep" id="dep">
								${depOption}
							</select>
							<input type="hidden"  id="id" name="id">
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search" >搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					<small class='btns-buy btns-print'>
					<a href="${url}?method=edit&fatherId=" target="dialog" rel="myModal" checkName="deptId" check="true" class="btn btn-warning" role="button">添加</a>
					<a href="${url}?method=edit&id=" target="dialog" checkName="deptId" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>
					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="deptId" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
					<a href="${url}?method=listDeptMenu&deptId=" target="dialog" checkName="deptId" checkOne="true" rel="myModal" class="btn btn-color-ab47bc" role="button">菜单权限</a>
					<a href="${url}?method=listDeptTaskMenu&deptId=" target="dialog" checkName="deptId" checkOne="true" rel="myModal" class="btn btn-success" role="button">任务权限</a>
					</small>
					<small class="pull-right btns-buy">
					<a href="${url}?method=isdel&isdel=${Isdel_No }&id=" target="ajaxTodo" checkName="deptId"  warm="确认启用吗" class="btn btn-info" role="button">启用</a>
					<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="deptId" warm="确认禁用吗" class="btn btn-danger " role="button">禁用</a>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="deptId" /></th>
											<th>部门名称</th>
											<th>上级部门</th>
											<th>顺序号</th>
											<th>是否启用</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="6">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="deptId" value="${u.id}"></td>
						    					<td>${u.name }</td>
												<td><font color="blue">${u.dept.name }</font></td>
						    					<td>${u.indexFlag }</td>
						    					<td>
						    						<c:if test="${u.isdel==Isdel_Yes }">
							    					<font color="red" class='state1'>${u.isdelName }</font>
							    					</c:if>
							    					<c:if test="${u.isdel==Isdel_No }">
							    					<font color="green" class='state1'>${u.isdelName }</font>
							    					</c:if>
						    					</td>
											</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>

<script>
		/*  $("#dep").click(function(){
			 $("#dep").val();
			/* var $pageForm = $(pageForm);
			$form.attr($("#dep").val()); */
			/* $("#pageForm").submit();
			
		})  */ 
		
		
		$("#dep").on ("change" ,function(){
			 
			 $("#id").val($("#dep").val());
			/*  var $pageForm = $("#pageForm");
			 $pageForm.attr("id" : $("#dep").val()  ) */
				$("#pageForm").submit();
		});
		
		
		
		/* $("#dep").on ("change" ,function(){
			alert($("#dep").val());	
			alert(1);
		}  ,function(){
			alert(2);
			$("#pageForm").submit();
		}   ); */
		/* 	alert($("#dep").val()); */
			/* $.ajax({
				type : 'POST',
			    url: "dept.do?method=list",
			    dataType:'json',
			    cache: false,
			    data:{depId:$("#dep").val()},
			 	success: function (json) {
			    	initTab();	
			    	alert("success");
				},  
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					toastr["error"](XMLHttpRequest.status);
					$("button").removeAttr("disabled");
				}
			} ) */
			
			

	
		
</script>








