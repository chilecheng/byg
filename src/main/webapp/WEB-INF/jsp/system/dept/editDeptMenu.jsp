<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<script src="lte/plugins/treeview/bootstrap-treeview.js"></script>
<script type="text/javascript">
$(function(){ 
	var tree = [${content}];
	
	$('#tree').treeview({data: tree,showCheckbox:true,
		onNodeChecked: function(event, data){
			var inNode = data;
			checkedChild(inNode);
//			debugger;
//  			while(inNode.nodeId!=undefined){
//  				inNode = $('#tree').treeview('getParent',inNode.nodeId);
// 				addDeptMenu(inNode.nodeId,$("#deptId").val(),inNode.tags);
  // 				$('#tree').treeview('checkNode', [inNode.nodeId, { silent: true } ]);
//  			}
		},
		onNodeUnchecked: function(event, data){
			uncheckedChild(data)
		}
	});
});
function checkedChild(data){
	//debugger;
	var inNode = data;
	addDeptMenu(inNode.nodeId,$("#deptId").val(),inNode.tags);
	if(inNode.nodes!=undefined){
		for(var i=0;i<inNode.nodes.length;i++){
			var chilNode = inNode.nodes[i];
// 				$('#tree').treeview('checkNode', [chilNode.nodeId, { silent: true } ]);
				var node = $('#tree').treeview('getNode', chilNode.nodeId);
				//addDeptMenu(node.nodeId,$("#deptId").val(),node.tags);
				checkedChild(node);
				//debugger;
		}
	}
}
function uncheckedChild(data){
	var inNode = data;
	delDeptMenu(inNode.nodeId,$("#deptId").val(),inNode.tags);
	if(inNode.nodes!=undefined){
		for(var i=0;i<inNode.nodes.length;i++){
			var chilNode = inNode.nodes[i];
				var node = $('#tree').treeview('getNode', chilNode.nodeId);
				delDeptMenu(node.nodeId,$("#deptId").val(),node.tags);
// 				$('#tree').treeview('uncheckNode', [chilNode.nodeId, { silent: true } ]);
				uncheckedChild(node);
		}
	}
}

function addDeptMenu(nodeId,deptId,menuId){
	$.ajax({
		 type:'POST',
		 url:"dept.do?method=addDeptMenu&deptId="+deptId+"&menuId="+menuId+"",
		 dataType:"json",
		 cache:false,
		 success:function(json){
			 if(json.statusCode==200){
				 $('#tree').treeview('checkNode', [nodeId, { silent: true } ]);
			 }else{
				 toastr["error"](json.message); 
			 }
		 },
		 error:function() {  
			 toastr["error"]("网络错误，如一直失败，请联系管理员"); 
		 }
	});
}

function delDeptMenu(nodeId,deptId,menuId){
	$.ajax({
		 type:'POST',
		 url:"dept.do?method=delDeptMenu&deptId="+deptId+"&menuId="+menuId+"",
		 dataType:"json",
		 cache:false,
		 success:function(json){
			 if(json.statusCode==200){
				 $('#tree').treeview('uncheckNode', [nodeId, { silent: true } ]);
			 }else{
				 toastr["error"](json.message); 
			 }
		 },
		 error:function() {  
			 toastr["error"]("网络错误，如一直失败，请联系管理员"); 
		 }
	});
}

// function submitDeptMenu(){
// 	var node = $('#tree').treeview('getChecked');
// 	var ids="";
// 	for(var i=0;i<node.length;i++){
// 		if(ids!=null&&ids!=""){
// 			ids+=",";
// 		}
// 		ids+=node[i].tags;
// 	}
// 	$("#ids").val(ids);
// 	$("#form1").submit();
// }
</script>
<div class="modal-dialog" role="document">
	<form action="${url}" id="form1" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<%-- <input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="ids" name="ids" value=""> --%>
		<input type="hidden" id="deptId" name="deptId" value="${dept.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">${dept.name }&nbsp;菜单设置</h4>
			</div>
			<div class="modal-body">
				<div id="tree"></div>
			</div>
			<div class="modal-footer btns-dialog">
<!-- 				<button type="button" onclick="submitRoleMenu()"  class="btn btn-primary">保存</button> -->
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>