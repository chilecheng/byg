<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 
<script type="text/javascript">
	timeRange();
	function changeDate(){
		$('#pageForm').submit();
	}
</script>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">公开公告</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-12">
						<label>日期：</label> 
							<input type="text" data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  name="beginDate" value="<fmt:formatDate value="${beginDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<label class='timerange'>--</label> 
							<input type="text" data-id="endDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  name="endDate" value="<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd"/>"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
							<button type='submit' class='btn btn-info btn-search btn-search-left'>搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
					
					<small class="btns-buy">
					<a href="${url}?method=edit&type=${Type_Add}" target="homeTab" rel="myModal" class="btn btn-warning"  role="button">添加</a>
					<a href="${url}?method=edit&type=${Type_Edit}&id=" target="homeTab" rel="myModal" checkName="id" checkOne="true"  class="btn btn-color-ff7043" role="button">修改</a>
					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="id" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
					</small>
					<%-- <small class='pull-right btns-print'>
					<a href="${url}?method" target="dialog" rel="myModal" class="btn btn-primary" role="button">导出Excel</a>
					<a href="${url}?method" target="dialog" checkName="id" checkOne="true"  rel="myModal" class="btn btn-color-ab47bc" role="button">打印</a>
					</small> --%>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="id" /></th>
											<th>通告标题</th>
											<th>发布部门</th>
											<th>发布时间</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="6">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="id" value="${u.id}"></td>
						    					<td>
						    					<a href="${url}?method=listedit&id=${u.id}" target="homeTab"><font color="blue">${u.title}</font></a>
						    					</td>
						    					<td>${u.dept.name}</td>
						    					<td>${u.createTime}</td>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>

