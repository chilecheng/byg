<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>

<link rel="stylesheet" href="lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script src="lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

    <script>
      $(function () {
        $(".textarea").wysihtml5();
      });
      function a(){
          $(".textarea").wysihtml5();
    	  
      }
    </script>

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">公开公告</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<!-- /.box-header -->
				<form id="uploadForm" name="uploadForm" action="${url }" enctype="multipart/form-data" onsubmit="return uploadCallback(this,dialogAjaxDone1);" >
<!-- 				enctype="multipart/form-data" -->
		<input type="hidden" name="method" value="save" >
		<input type="hidden" name="type" value="${type }">
		<input type="hidden" name="id" value="${notification.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<c:choose>
					<c:when test="${type eq Type_Edit  }">
						<h4 class="modal-title" id="myModalLabel" onclick="a()">修改公开公告</h4>
					</c:when>
					<c:when test="${type eq Type_Add  }">
						<h4 class="modal-title" id="myModalLabel" onclick="a()">添加公开公告</h4>
					</c:when>
				</c:choose>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">公告标题:</td>
						<td width="80%"><input type="text" class="required form-control" style="width:100%" id="title" name="title" value="${notification.title }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red"></i>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">添加时间:</td>
						<td>
							<c:choose>
								<c:when test="${type eq Type_Edit  }"> <!-- data-provide="datetimepicker" -->
									<input data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  name="createTime" value="${notification.createTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</c:when>
								<c:when test="${type eq Type_Add  }">
									<input data-id="beginDate" data-min-view="2" data-date-format="yyyy-mm-dd" class="list_select"  name="createTime" value="${nowTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								</c:when>
							</c:choose>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">发布部门:</td>
						<td width="80%">
							<c:choose>
								<c:when test="${type eq Type_Edit }">								
									<input type="text" readonly class=" list_input"  id="title" data-name='deptName'  name="dept" value="${notification.dept.name}">
									<input type="text" readonly  id='assiginUserId' data-id="deptId" name='deptId' hidden value='${notification.dept.id }'/>
								</c:when>
								<c:when test="${type eq Type_Add  }">
									<input type="text" readonly class=" list_input"  id="title" data-name='deptName'  name="dept" value="${deptName}">
									<input type="text" readonly  id='assiginUserId' data-id="deptId" name='deptId' hidden value='${deptId} '/>
								</c:when>
							</c:choose>
							<%-- <a class='wk-choose btn btn-default' data-choose='get'>选择</a>
							<a class='hide' data-fun="wk"  target='dialog' href='${url}?method=choose&groupType=1'>选择</a> --%>
					
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">发布人员:</td>
						<td width="80%">
							<c:choose>
								<c:when test="${type eq Type_Edit }">
									<input type="text" readonly class="list_input" name="title" data-name="workerName" name="createUserId" value="${notification.user.name }">
									<input type="text" readonly  id='assiginUserId' data-id="workerId" name='assiginUserId' hidden value='${notification.user.userId }'/>
								</c:when>
								<c:when test="${type eq Type_Add  }">
									<input type="text" readonly class="list_input" name="title" data-name="workerName" name="createUserId" value="${userName }">
									<input type="text" readonly  id='assiginUserId' data-id="workerId" name='assiginUserId' hidden value='${userId }'/>
								</c:when>
							</c:choose>
							<%-- <a class='wk-choose btn btn-default' data-choose='get'>选择</a>
							<a class='hide' data-fun="wk" data-dept='dept' target='dialog' href='${url}?method=chooseFb&deptId=${notification.dept.id}'>选择</a> --%>
						</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">发布内容:</td>
						<td>
							<textarea class="textarea" name="content" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">${notification.content}</textarea>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
<%-- 					<form action="${url}" enctype="multipart/form-data" method="post" > --%>
					<input type="hidden" name="method" value="upload" >
					<tr>
						<td width="100px">附件:</td>
						<td>
							<input id="file" name="file" type="file" value="1212">
							<div class="input-append">
								<c:choose>
									<c:when test="${notification.fileName !=null  }">
										原文件:
										<input type="text" value="${notification.fileName } " readonly style="height:30px;">
									</c:when>
								</c:choose>
<!-- 								<input id="photoCover" class="input-large" type="text" name="ininin"  style="height:30px;"> -->
<!-- 								<a class="btn" onclick="$('input[id=lefile]').click();">上传文件</a> -->
							</div>
						</td>
<%-- 						<td><button type="submit"  class="btn btn-info" onClick="javascript:uploadForm.action='${url}?method=upload';">确定上传</button></td> --%>
					</tr>
<!-- 					</form> -->
					
				</table>
			</div>
			<div class="modal-footer btns-hometab">
				<button type="submit"  class="btn btn-info">保存</button>
				<button type="reset"  class="btn btn-color-9E8273" data-dismiss="modal">重置</button>
				
				<a id="fh" href="notification.do" target="homeTab"  class="btn btn-default">
				返回
				</a>
			</div>
		</div>
	</form>
			</div>
		</div>
	</div>
</section>



<script type="text/javascript">
$('input[id=lefile]').change(function() {
$('#photoCover').val($(this).val());
});
/*选择按钮初始化*/
	$('[data-choose="get"]').click(function(){
		data_group.name=$(this).prev().prev().attr('data-name');
		data_group.id=$(this).prev().attr('data-id');
		$(this).next().click();
	});
</script>