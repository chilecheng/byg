
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>
<section class="content-header">
	<i class="title-line"></i>
	<div class="title">通知公告</div>
</section>
<style type="text/css">
	.inform-title{
		font-weight:bold;
		text-align:center;
	}
</style>
<form action="${url}" id="pageForm" onsubmit="return readData(this,homeAjaxDone);">
	<input type="hidden" name="method" value="${method }" >
	<input type="hidden" name="id" value="${notification.id}">
	<section class='content'>
		<div class='row'>
			<div class='col-md-8'>
				<div class="box box-warning">
		            <div class="box-header">
		              <h3 class='inform-title'>${notification.title}</h3>
		              <h5 style='text-align:center;'>发布部门：<span>${notification.dept.name}</span> &nbsp;&nbsp;&nbsp;发布时间：<span>${notification.createTime}</span></h5>
		            </div>
		            <div class="box-body">
		            	<div class='row'>
		            		<div class='col-md-12'>${notification.content }</div>
		            	</div>
		            </div>
		            <div class='box-footer'>
		            	<div class='row'>
		            		<div class='col-md-12'>&nbsp;&nbsp;&nbsp;&nbsp;附件：
		            		<a href="notification.do?method=downLoad&fileName=${notification.fileName }&saveAddress=${notification.saveAddress}">${notification.fileName}</a>
		            		<input type="hidden" name="saveAddress" value="${notification.saveAddress }" >
		            		</div>
		            	</div>
		            </div>
		         </div>
		         <div class='box box-warning' style='text-align:right'>
		         	<c:choose>
		         		<c:when test="${position eq 'home' }">
		         			<a  href="login.do?method=main"    class="btn btn-default" style="margin:5px 10px;">返回首页</a>
		         		</c:when>
		         		<c:otherwise>
			         		<a href="notification.do" target="homeTab" rel="myModal" class="btn btn-default" style="margin:5px 10px;">返回</a>
		         		</c:otherwise>
		         	</c:choose>
		         		
		         
		         </div>
			</div>
			<div class='col-md-4'>
				<div class="box box-solid" style='border-radius:6px;'>
		            <div class="box-header with-border" style="background-color:#FFA726;border-radius:6px;">
		              <h3 class="box-title" style="color:#fff;">浏览人数：<span>${notification.number}</span></h3>
		
		              <div class="box-tools">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus" style='color:#fff;'></i>
		                </button>
		              </div>
		            </div>
		            <div class="box-body no-padding">
		              <ul class="nav nav-pills nav-stacked">
		                <li><a href="#">${user.name}
		                  <span class="pull-right">${date}</span></a></li>
		              </ul>
		            </div>
		            <!-- /.box-body -->
		         </div>
			</div>
		</div>
	</section>
</form>
<script>

</script>