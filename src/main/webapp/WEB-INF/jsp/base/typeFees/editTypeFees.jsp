<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="id" value="${typeFees.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加收费项目类型信息</h4>
			</div>
			<div class="modal-body" >
				<table>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">名称:</td>
						<td><input type="text" class="required form-control" id="name" name="name" value="${typeFees.name }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle "></i>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">顺序号:</td>
						<td><input type="text" id="indexFlag" name="indexFlag" class="integer form-control"  value="${typeFees.indexFlag}"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>