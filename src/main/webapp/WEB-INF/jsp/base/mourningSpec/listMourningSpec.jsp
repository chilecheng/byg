<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">灵堂规格</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-4">
							<label>灵堂名称：</label> 
							<input type="text" class="list_input" name="name" value="${name}">
						</div>
						<div class="col-md-6">
							<label>是否启用：</label> 
							<select class="list_select nopadding-R" name="isdel">
								${isdelOption}
							</select>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
				<small class='btns-buy'>
					<a href="${url}?method=edit" target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
					<a href="${url}?method=edit&id=" target="dialog" checkName="id" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>
					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="id" rel="myModal" warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
				</small>
					<small class="pull-right btns-buy">
					<a href="${url}?method=isdel&isdel=${Isdel_No }&id=" target="ajaxTodo" checkName="id"   warm="确认启用吗" class="btn btn-info" role="button">启用</a>
					<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="id"  warm="确认禁用吗" class="btn btn-danger " role="button">禁用</a>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="mourningId" /></th>
											<th>灵堂规格名称</th>
											<th>顺序号</th>
											<th>是否启用</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="6">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="id" value="${u.id}"></td>
						    					<td><font color="blue">${u.name }</font></td>
						    					<td>${u.indexFlag}</td>
						    					<td>
						    						<c:if test="${u.isdel==Isdel_Yes }">
							    						<font color="red" class='state1'>${u.isdelName}</font>
							    					</c:if>
							    					<c:if test="${u.isdel==Isdel_No }">
							    						<font color="green" class='state1'>${u.isdelName}</font>
							    					</c:if>
												</td>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
