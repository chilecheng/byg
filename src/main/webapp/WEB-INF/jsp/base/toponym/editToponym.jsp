<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" role="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="toponymId" value="${toponym.toponymId}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<c:if test="${toponymId != null }">
					<h4 class="modal-title" id="myModalLabel">修改地名</h4>
				</c:if>
				<c:if test="${toponymId == null }">
					<h4 class="modal-title" id="myModalLabel">新增地名</h4>				
				</c:if>
			</div>
			<div class="modal-body" >
				<table>
				<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">上级名称:</td>
							
						<td>
						<!--  <input type="text" class="required form-control" id="fatherId" name="fatherId" value=""> -->
							<select class="required form-control" id="fatherId" name="fatherId" style="width:200px;">
								<option value=""></option>
								${registerOption }
							</select>
						</td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
						
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">地名名称:</td>
						<td><input type="text" class="required form-control" id="name" name="name" value="${toponym.name }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">顺序号:</td>
						<td><input type="text" id="indexFlag" name="indexFlag" class="integer form-control"  value="${toponym.indexFlag}"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
<!-- 					<tr> -->
<!-- 						<td width="100px">是否基本减免:</td> -->
<!-- 						<td> -->
<!-- 							<select class="required form-control" id="isBase" name="isBase" style="width:80px;"> -->
<%-- 								${isOption } --%>
<!-- 							</select> -->
<!-- 						</td> -->
<!-- 						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td> -->
<!-- 					</tr> -->
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
	
</div>