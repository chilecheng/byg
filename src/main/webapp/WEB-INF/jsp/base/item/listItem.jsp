<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">收费项目</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<%-- <div class="col-md-5">
							<label>名称：</label> 
							<input type="text" class="list_input input-hometab" id="name" name="name" value="${name }">
						</div> --%>
						<div class="col-md-4">
							<label>查询类型：</label> 
							<select class="list_select nopadding-R" name="searchType" style="width:52%;">
								${searchOption }
							</select>
						</div>
						<div class="col-md-8" style="margin-left:-94px">
							<label>查询值：</label> 
							<input type="text" class="list_input input-hometab" name="searchVal" value="${searchVal}" placeholder="查询值">
						</div>
						<div class="col-md-2">
							<label>是否启用：</label> 
							<select class="list_select nopadding-R" name="isdel" id="isdel">
								${isdelOption}
							</select>
						</div>

						<div class="col-md-3">
							<label>项目类别：</label> 
							<select class="list_select nopadding-R" name="typeId" id="typeId">
								${typeIdOption}
							</select>
						</div>
						<div class="col-md-4">
							<label>详细分类：</label> 
							<select class="list_select" name="sort" id="sort">
								${sortOption}
							</select>
						</div>
						<div class="col-md-8">
							<button type="submit" class="btn btn-info btn-search" >搜索</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-header with-border">
				<small class='btns-buy'>
					<a href="${url}?method=edit" target="dialog" rel="myModal"  class="btn btn-warning" role="button">添加</a>
					<a href="${url}?method=edit&id=" target="dialog" checkName="roleId" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>
					<a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="roleId"  warm="确认删除吗" class="btn btn-danger" role="button">删除</a>
				
				</small>	
					<small class="pull-right btns-buy">
					<a href="${url}?method=isdel&isdel=${Isdel_No }&id=" target="ajaxTodo" checkName="roleId"   warm="确认启用吗" class="btn btn-info" role="button">启用</a>
					<a href="${url}?method=isdel&isdel=${Isdel_Yes }&id=" target="ajaxTodo" checkName="roleId"   warm="确认禁用吗" class="btn btn-danger " role="button">禁用</a>
					</small>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId" /></th>
											<!-- <th>编号</th> -->
											<th>助记码</th>
											<th>名称</th>
											<th>价格</th>
											<th>计量单位</th>
											<th>类别</th>
											<th>详细分类</th>
											<th>基本减免</th>
											<th>困难减免</th>
											<th>默认项目</th>
											<th>备注</th>
											<th>顺序号</th>
											<th>是否启用</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan="15">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="roleId" value="${u.id}"></td>
						    					<%-- <td>${u.code }</td> --%>
						    					<td>${u.helpCode }</td>
						    					<td>${u.name }</td>
						    					<td><fmt:formatNumber value="${u.pice }" pattern="#.#"></fmt:formatNumber></td>
						    					<td>${u.measure }</td>
						    					<td>${u.itemType.name}</td>
						    					<td>${u.sortName}</td>
						    					<td>
							    					<c:if test="${u.baseFlag==Is_No }">
							    					<font color="red" class='state1'>${u.baseFlagName }</font>
							    					</c:if>
							    					<c:if test="${u.baseFlag==Is_Yes}">
							    					<font color="green" class='state1'>${u.baseFlagName }</font>
							    					</c:if>
						    					</td>
						    					<td>
							    					<c:if test="${u.hardFlag==Is_No }">
							    					<font color="red" class='state1'>${u.hardFlagName }</font>
							    					</c:if>
							    					<c:if test="${u.hardFlag==Is_Yes}">
							    					<font color="green" class='state1'>${u.hardFlagName }</font>
							    					</c:if>
						    					</td>
						    					<td>
							    					<c:if test="${u.defaultFlag==Is_No }">
							    					<font color="red" class='state1'>${u.defaultFlagName }</font>
							    					</c:if>
							    					<c:if test="${u.defaultFlag==Is_Yes}">
							    					<font color="green" class='state1'>${u.defaultFlagName }</font>
							    					</c:if>
						    					</td>
						    					<td>${u.comment }</td>
						    					<td>${u.indexFlag }</td>
						    					<td>
							    					<c:if test="${u.isdel==Isdel_Yes }">
							    					<font color="red">${u.isdelName }</font>
							    					</c:if>
							    					<c:if test="${u.isdel==Isdel_No }">
							    					<font color="green">${u.isdelName }</font>
							    					</c:if>
						    					</td>
											</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
