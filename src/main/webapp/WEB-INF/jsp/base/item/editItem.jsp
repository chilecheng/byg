<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" item="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${item.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加收费项目</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="100px">名称:</td>
						<td><input type="text" class="required form-control"  id="name" name="name" value="${item.name }"></td>
						<td width="30px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
						<td width="100px">类别:</td>
						<td>
							<select class="list_select" name="typeId" id="typeId">
								${itemTypeOption}
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">助记码:</td>
						<td><input type="text" class="required form-control"  id="helpCode" name="helpCode" value="${item.helpCode }"></td>
						<td width="30px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
						<td width="100px">详细分类:</td>
						<td>
							<select class="list_select" name="sort" id="sort">
								${sortOption}
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<%-- <td width="100px">编号:</td>
						<td><input type="text" class="required form-control"  id="code" name="code" value="${item.code }"></td> --%>
						<%-- <td width="100px">名称简拼:</td>
						<td><input type="text" class="required form-control"  id="nameJp" name="nameJp" value="${item.nameJp }"></td>
						<td width="30px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td> --%>
						<td width="100px">收费项目类型:</td>
						<td>
							<select class="list_select" name="typeFees" id="typeFees">
								${typeFeesOption}
							</select>
						</td>
						<td width="30px">&nbsp;</td>
						<td width="100px">价格:</td>
						<td><input type="text" class="number  form-control"  id="pice" name="pice" value="${item.pice==0 ? '' : item.pice }"></td>
						<td width="30px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">计量单位:</td>
						<td><input type="text" class="form-control"  id="measure" name="measure" value="${item.measure }"></td>
						<td width="30px">&nbsp;</td>
						<td width="100px">备注:</td>
						<td><input type="text" class="form-control"  id="comment" name="comment" value="${item.comment }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">基本减免项目:</td>
						<td>
							<select class="list_select" name="baseFlag" id="baseFlag">
								${baseFlagOption}
							</select>
						</td>
						<td width="30px">&nbsp;</td>
						<td width="100px">困难减免项目:</td>
						<td>
							<select class="list_select" name="hardFlag" id="hardFlag">
								${hardFlagOption}
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">默认服务项目:</td>
						<td>
							<select class="list_select" name="defaultFlag" id="defaultFlag">
								${defaultFlagOption}
							</select>
						</td>
						<td width="30px">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					<tr>
						<td width="100px">顺序号:</td>
						<td><input type="text" class="integer form-control" name="indexFlag" id="indexFlag" value="${item.indexFlag}"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>

</div>
