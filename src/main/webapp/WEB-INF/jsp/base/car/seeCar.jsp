<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">查看车辆信息</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="80px">车牌号:</td>
						<td><input type="text" readonly="readonly" class="form-control" value="${car.carNumber }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">名称:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.name }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">车辆类型:</td>
						<td><input type="text" readonly="readonly" class="form-control" value="${car.carType.name }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">服务价格:</td>
						<td><input type="text" readonly="readonly" class="form-control" value="${car.servicePice }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">车内附属品:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.additional }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">驾驶员姓名:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.driverName }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">驾驶员电话:</td>
						<td><input type="text" readonly="readonly" class="form-control" value="${car.driverPhone }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">驾驶员地址:</td>
						<td><input type="text" readonly="readonly" class="form-control" value="${car.driverAddr }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">购买日期:</td>
						<td><input type="text" readonly="readonly" class="form-control" value="${car.buyDate }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">购买价格:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.buyPice }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">初检日期:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.firstInspect }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">复检日期:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.retestInspect }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">保险公司:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.insurerCompany }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">保险证号:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.insurerCode }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">保险期限:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.insurerTime }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">保险内容:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.insurerContent }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">经销商:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.sellerName }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">经销商电话:</td>
						<td><input type="text" readonly="readonly" class="form-control" value="${car.sellerPhone }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">顺序号:</td>
						<td><input type="text" readonly="readonly"  class="form-control" value="${car.indexFlag}"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>