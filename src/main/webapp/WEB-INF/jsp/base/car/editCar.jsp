<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${car.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加车辆信息</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="80px">车牌号:</td>
						<td><input type="text" class="required form-control"  id="carNumber" name="carNumber" value="${car.carNumber }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
						<td width="80px">名称:</td>
						<td><input type="text" class="required form-control"  id="name" name="name" value="${car.name }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">车辆类型:</td>
						<td>
							<select class="list_select" name="typeId" id="typeId">
								${typeOption}
							</select>
						</td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">服务价格:</td>
						<td><input type="text" class="number form-control"  id="servicePice" name="servicePice" value="${car.servicePice }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">车内附属品:</td>
						<td><input type="text" class="form-control"  id="additional" name="additional" value="${car.additional }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">驾驶员姓名:</td>
						<td><input type="text" class="required form-control"  id="driverName" name="driverName" value="${car.driverName }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">驾驶员电话:</td>
						<td><input type="text" class="integer form-control"  id="driverPhone" name="driverPhone" value="${car.driverPhone }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">驾驶员地址:</td>
						<td><input type="text" class="form-control"  id="driverAddr" name="driverAddr" value="${car.driverAddr }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">购买日期:</td><!-- data-provide="datetimepicker" -->
						<td><input data-id='beginDate' data-min-view="2"  data-date-format="yyyy-mm-dd" class="form-control"  id="buyDate" name="buyDate" value="${car.buyDate }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">购买价格:</td>
						<td><input type="text" class="form-control"  id="buyPice" name="buyPice" value="${car.buyPice }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">初检日期:</td><!-- data-provide="datetimepicker" -->
						<td><input data-id='beginDate' data-min-view="2"  data-date-format="yyyy-mm-dd" class="form-control"  id="firstInspect" name="firstInspect" value="${car.firstInspect }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">复检日期:</td><!-- data-provide="datetimepicker" -->
						<td><input data-id='beginDate' data-min-view="2"  data-date-format="yyyy-mm-dd" class="form-control"  id="retestInspect" name="retestInspect" value="${car.retestInspect }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">保险公司:</td>
						<td><input type="text" class="form-control"  id="insurerCompany" name="insurerCompany" value="${car.insurerCompany }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">保险证号:</td>
						<td><input type="text" class="form-control"  id="insurerCode" name="insurerCode" value="${car.insurerCode }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">保险期限:</td><!-- data-provide="datetimepicker" -->
						<td><input data-id='beginDate' data-min-view="2"  data-date-format="yyyy-mm-dd" class="form-control"  id="insurerTime" name="insurerTime" value="${car.insurerTime }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">保险内容:</td>
						<td><input type="text" class="form-control"  id="insurerContent" name="insurerContent" value="${car.insurerContent }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">经销商:</td>
						<td><input type="text" class="form-control"  id="sellerName" name="sellerName" value="${car.sellerName }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">经销商电话:</td>
						<td><input type="text" class="form-control"  id="sellerPhone" name="sellerPhone" value="${car.sellerPhone }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">顺序号:</td>
						<td><input type="text" class="integer form-control" name="indexFlag" id="indexFlag" value="${car.indexFlag}"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>