<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" freezer="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${freezer.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加冷藏柜信息</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="80px">编号:</td>
						<td><input type="text" class="required form-control"  id="code" name="code" value="${freezer.code }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
						<td width="80px">名称:</td>
						<td><input type="text" class="required form-control"  id="name" name="name" value="${freezer.name }"></td>
						<td width="20px">&nbsp;<i class="fa fa-circle text-red fa-middle"></i>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">冷藏柜类型:</td>
						<td>
							<select class="list_select" name="typeId" id="typeId">
								${typeOption}
							</select>
						</td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">费用:</td>
						<td><input type="text" class="number form-control"  id="pice" name="pice" value="${freezer.pice }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<%-- <tr>
						<td width="80px">是否外送:</td>
						<td>
							<select class="list_select" name="outFlag" id="outFlag">
								${outOption}
							</select>
						</td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">是否分体:</td>
						<td>
							<select class="list_select" name="splitFlag" id="splitFlag">
								${splitOption}
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr> --%>
					<tr>
						<td width="80px">购买日期:</td><!-- data-provide="datetimepicker" -->
						<td><input data-id='beginDate' data-min-view="2"  data-date-format="yyyy-mm-dd" class="form-control"  id="buyTime" name="buyTime" value="${freezer.buyTime }"></td>
						<td width="20px">
							&nbsp;
						</td>
						<td width="80px">备注:</td>
						<td><input type="text" class="form-control"  id="comment" name="comment" value="${freezer.comment }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">顺序号:</td>
						<td><input type="text" class="integer form-control" name="indexFlag" id="indexFlag" value="${freezer.indexFlag}"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>