<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/11
  Time: 8:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" car="document" style="width:50%">
    <form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <input type="hidden" name="method" value="${method }">
        <input type="hidden" id="type" name="type" value="${type}">
        <input type="hidden" id="tombId" name="tombId" value="${buyRecord.tombId}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">墓穴维修登记表</h4>
            </div>
            <div class="modal-body">
                <div>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr role="row">
                            <th colspan="6">公墓持证人信息</th>
                        </tr>
                        <tr role="row">
                            <th>持证人姓名</th>
                            <th>公墓证号</th>
                            <th>身份证号</th>
                            <th>家庭住址</th>
                            <th>手机</th>
                            <th>联系电话</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr role="row">
                            <td>${buyRecord.cardHolder}</td>
                            <td>${buyRecord.tombsCard}</td>
                            <td>${buyRecord.IDNumber}</td>
                            <td>${buyRecord.address}</td>
                            <td>${buyRecord.phoneNumber}</td>
                            <td>${buyRecord.contactNumber}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <table>
                    <tr>
                        <td width="120px">创建日期:</td>
                        <td><input type="text" readonly="readonly"
                                   style="outline-style: none; border-style: none; width:150px;" id="createTime"
                                   name="createTime"></td>
                        <td width="30px"></td>
                        <td width="120px">经办人:</td>
                        <td width="150px">${user}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="120px">墓穴:</td>
                        <td width="400px">${buyRecord.graveInfo}</td>
                        <td width="30px"></td>
                        <td width="120px">报备人:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="name" name="name">
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="120px">报备人电话:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="phoneNumber"
                                   name="phoneNumber"></td>
                        <td width="30px"></td>
                        <td width="120px">维修状态:</td>
                        <td>
                            <select style="width:150px" class="list_table" name="repairType" id="repairType">
                                ${repairTypeOption }
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="120px">维修开始时间:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="beginTime"
                                   name="beginTime"></td>
                        <td width="30px"></td>
                        <td width="120px">预计结束时间:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="endTime"
                                   name="endTime"></td>
                    </tr>
<%--                    <tr>--%>
<%--                        <td>&nbsp;</td>--%>
<%--                    </tr>--%>
<%--                    <tr>--%>
<%--                        <td width="30px"></td>--%>
<%--                        <td width="120px">维修完成时间:</td>--%>
<%--                        <td><input type="text" style="width:150px" class="required form-control" id="completeTime"--%>
<%--                                   name="completeTime"></td>--%>
<%--                    </tr>--%>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="120px">维修内容:</td>
                        <td colspan="5">
                            <textarea class="form-control" id="comment" name="comment"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr role="row">
                        <th colspan="4">缴纳维修费</th>
                    </tr>
                    <tr role="row">
                        <th>序号</th>
                        <th>缴费人</th>
                        <th>发票号</th>
                        <th>金额</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--                    <tr role="row">--%>
                    <%--                        <td>${buyRecord.cardHolder}</td>--%>
                    <%--                        <td>${buyRecord.tombsCard}</td>--%>
                    <%--                        <td>${buyRecord.IDNumber}</td>--%>
                    <%--                        <td>${buyRecord.address}</td>--%>
                    <%--                    </tr>--%>
                    <tr role="row">
                        <td>1</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="payerName"
                                   name="payerName"></td>
                        <td><input type="text" style="width:150px" class="required form-control" id="invoiceNumber"
                                   name="invoiceNumber"></td>
                        <td><input type="text" style="width:150px" class="required form-control" id="total"
                                   name="total"></td>
                    </tr>
                    </tbody>
                </table>
                <div class="modal-footer btns-dialog">
                    <button type="submit" class="btn btn-info btn-margin">保存</button>
                    <button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    //执行一个laydate实例
    laydate.render({
        elem: '#beginTime' //指定元素
    });
    //执行一个laydate实例
    laydate.render({
        elem: '#endTime' //指定元素
    });
    //执行一个laydate实例
    laydate.render({
        elem: '#completeTime' //指定元素
    });
    $(function () {
        var date = new Date();
        var sign1 = "-";
        var sign2 = ":";
        var year = date.getFullYear() // 年
        var month = date.getMonth() + 1; // 月
        var day = date.getDate(); // 日
        var hour = date.getHours(); // 时
        var minutes = date.getMinutes(); // 分
        var seconds = date.getSeconds() //秒
        // 给一位数数据前面加 “0”
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (day >= 0 && day <= 9) {
            day = "0" + day;
        }
        if (hour >= 0 && hour <= 9) {
            hour = "0" + hour;
        }
        if (minutes >= 0 && minutes <= 9) {
            minutes = "0" + minutes;
        }
        if (seconds >= 0 && seconds <= 9) {
            seconds = "0" + seconds;
        }
        var createTime = year + sign1 + month + sign1 + day + " " + hour + sign2 + minutes + sign2 + seconds;
        document.getElementById("createTime").value = createTime;
    });
</script>
