<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">墓穴信息</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do?" target="homeTab" rel="myModal" class="btn btn-warning" role="button">返回首页</a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：
                        <a href="cemeteryInterface.do?" target="homeTab">公墓销售</a>/
                        <a href="cemeteryArea.do?type=show&method=list&cemeteryId=${cemeteryId}&areaId=${areaId}"
                           target="homeTab">${cemetery.name }</a>/
                        <a href="tombAreaView.do?method=list&selectCemetery=${cemeteryId}&selectArea=${areaId}"
                           target="homeTab">${cemeteryArea.name }</a>/
                        <a href="" target="homeTab">总览图</a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                </div>
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <input name="method" value="${method}" type="hidden"/>
                    <input name="cemeteryId" value="${cemeteryId }" type="hidden"/>
                    <input name="areaId" value="${areaId }" type="hidden"/>
                    <input name="tid" value="${tomb.id }" type="hidden"/>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <div class="box-body">
                    <div class="col-xs-5">
                        <div class="ID-pic" style="width:424px; height:380px">
                            <img width=420px; height=380px; src="${tomb.photo }" alt="${u.name }">
                        </div>
                        <input type="hidden" name="filename" value="${tomb.photo }">
                    </div>
                    <div class="col-xs-7">
                        <p><label>墓穴位置：${tomb.name }</label></p>
                        <p><label>墓穴费： ${tomb.price }元</label></p>
                        <p><label>维护费： ${tomb.maintenance }元/年</label></p>
                        <p><label>墓穴状态： ${tomb.stateName }</label></p>
                        <p><label>墓穴描述：${tomb.comment }</label></p>
                        <p><label>操作：<br/>
                            <c:choose>
                                <c:when test="${tomb.state==1 }">
                                    <a href="${url }?method=yuding&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">预定</a>
                                    <a href="${url }?method=sale&tid=${tomb.id}&areaId=${areaId}&cemeteryId=${cemeteryId}"
                                       target="homeTab" rel="myModal" class="btn btn-warning" role="button">购买</a>
                                    <a href="${url }?method=repair&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">修缮</a>
                                </c:when>
                                <c:when test="${tomb.state==2 }">
                                    <a href="${url }?method=transfer&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">迁出</a>
                                    <a href="${url }?method=change&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">换证补证</a>
                                    <a href="${url }?method=items&type=wxdj&tid=${tomb.id}" target="dialog"
                                       rel="myModal" class="btn btn-warning" role="button">维修登记</a>
                                    <a href="${url }?method=items&type=lxd&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">安葬事项联系单</a>
                                    <br/>
                                    <br/>
                                    <a href="${url }?method=maintain&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">维护续费</a>
                                    <a href="${url }?method=items&type=mxf&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">交纳墓穴费</a>
                                    <a href="${url }?method=items&type=ptsf&tid=${tomb.id}" target="dialog"
                                       rel="myModal" class="btn btn-warning" role="button">普通收费</a>
                                    <a href="${url }?method=items&type=dkafflm&tid=${tomb.id}" target="dialog"
                                       rel="myModal" class="btn btn-warning" role="button">缴纳雕刻安放封龙门费</a>
                                </c:when>
                                <c:when test="${tomb.state==3 }">
                                    <a href="${url }?method=completeRepair&tid=${tomb.id}" target="dialog" rel="myModal"
                                       class="btn btn-warning" role="button">修缮完成</a>
                                </c:when>
                                <c:when test="${tomb.state==6 }">
                                    <a href="${url}?method=escYuding&tid=${tomb.id}" target="ajaxTodo" warm="确认取消预定吗"
                                       class="btn btn-danger">取消预定</a>
                                    <a href="${url }?method=sale&tid=${tomb.id}&ischange=1" target="homeTab"
                                       rel="myModal" class="btn btn-warning" role="button">转为购买</a>
                                </c:when>
                            </c:choose>
                        </label></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <div class="box-body">
                    <form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
                        <div class="modal-body">
                            <div>
                                <div class="col-md-5 height-align">
                                    <label class='lab-4'>持证人员：</label>
                                    <span>${buyRecord.cardHolder }</span>
                                </div>
                                <div class="col-md-7 height-align">
                                    <label class='lab-4'>墓穴证号：</label>
                                    <span>${buyRecord.tombsCard }</span>
                                </div>
                                <div class="col-md-5 height-align">
                                    <label class='lab-4'>手机号码：</label>
                                    <span>${buyRecord.phoneNumber }</span>
                                </div>
                                <div class="col-md-7 height-align">
                                    <label class='lab-4'>身份证号：</label>
                                    <span>${buyRecord.IDNumber }</span>
                                </div>
                                <%--					<div class="col-md-5 height-align">--%>
                                <%--						<label class='lab-4'>联系电话：</label>--%>
                                <%--						<span>${buyRecord.contactNumber }</span>--%>
                                <%--					</div>--%>
                                <div class="col-md-7 height-align">
                                    <label class='lab-4'>家庭地址：</label>
                                    <span>${buyRecord.address }</span>
                                </div>
                            </div>
                            <div>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th colspan="8">墓穴使用者信息</th>
                                    </tr>
                                    <tr role="row">
                                        <th>死者姓名</th>
                                        <th>死者性别</th>
                                        <th>寿故</th>
                                        <th>生于</th>
                                        <th>逝于</th>
                                        <th>安放时间</th>
                                        <th>封龙门</th>
                                        <th>状态</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${userList}" var="u">
                                        <tr role="row">
                                            <td>${u.userName }</td>
                                            <td>${u.sexName }</td>
                                            <td>${u.deadTypeName }</td>
                                            <td><fmt:formatDate value="${u.borthTime }" pattern="yyyy-MM-dd"/></td>
                                            <td><fmt:formatDate value="${u.deadTime }" pattern="yyyy-MM-dd"/></td>
                                            <td><fmt:formatDate value="${u.releaseTime }"
                                                                pattern="yyyy-MM-dd"/></td>
                                            <td><fmt:formatDate value="${u.longmenTime }"
                                                                pattern="yyyy-MM-dd"/></td>
                                            <td>
                                                <c:if test="${u.isTransfer==1 }">
                                                    <font color="red" class='state1'>已迁出</font>
                                                </c:if>
                                                <c:if test="${u.isTransfer==2 }">
                                                    <font color="green" class='state1'>使用中</font>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th colspan="4">墓穴购买信息</th>
                                    </tr>
                                    <tr role="row">
                                        <th>缴费人姓名</th>
                                        <th>发票号</th>
                                        <th>缴费时间</th>
                                        <th>缴费金额</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${payList}" var="u">
                                        <tr role="row">
                                            <td>${u.name }</td>
                                            <td>${u.invoiceNumber }</td>
                                            <td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd"/></td>
                                            <td>${u.totalAmount }</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th colspan="8">墓穴维护费信息</th>
                                    </tr>
                                    <tr role="row">
                                        <th>缴费人姓名</th>
                                        <th>发票号</th>
                                        <th>年限</th>
                                        <th>开始时间</th>
                                        <th>结束时间</th>
                                        <th>缴费时间</th>
                                        <th>缴费金额</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${mainList}" var="u">
                                        <tr role="row">
                                            <td>${u.payName }</td>
                                            <td>${u.invoiceNumber }</td>
                                            <td>${u.longTime }</td>
                                            <td><fmt:formatDate value="${u.beginTime }" pattern="yyyy-MM-dd"/></td>
                                            <td><fmt:formatDate value="${u.endTime }" pattern="yyyy-MM-dd"/></td>
                                            <td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd"/></td>
                                            <td>${u.payMoney }</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
