<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document" style="width:50%">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${tid}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">更换墓穴证</h4>
			</div>
			<div class="modal-body"> 
				<table>
					<tr>
						<td width="100px">原墓穴证号码:</td>
						<td width="200px">${buyrecord.tombsCard }</td>
						<td width="30px"></td>
						<td width="100px">新墓穴证号码:</td>
						<td width="200px"><input type="text" style="width:200px" class="required form-control"  id="tombNumber" name="tombNumber" value="${buyrecord.tombsCard }">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">原有效期:</td>
						<td width="200px">${buyrecord.validityPeriod }</td>
						<td width="30px"></td>
						<td width="100px">新有效期:</td>
						<td><input type="text" style="width:200px" class="required form-control"  id="endTime" name="endTime">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">原持证人:</td>
						<td width="200px">${buyrecord.cardHolder}</td>
						<td width="30px"></td>
						<td width="100px">新持证人:</td>
						<td><input type="text" style="width:200px" class="required form-control"  id="name" name="name" value="${buyrecord.cardHolder}">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">原身份证号:</td>
						<td width="200px">${buyrecord.IDNumber }</td>
						<td width="30px"></td>
						<td width="100px">新身份证号:</td>
						<td><input type="text" style="width:200px" class="required form-control"  id="IDCard" name="IDCard" value="${buyrecord.IDNumber }">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">原身份证地址:</td>
						<td width="200px">${buyrecord.address }</td>
						<td width="30px"></td>
						<td width="100px">新身份证地址:</td>
						<td><input type="text" style="width:200px" class="form-control"  id="address" name="address" value="${buyrecord.address }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">原联系电话:</td>
						<td width="200px">${buyrecord.contactNumber }</td>
						<td width="30px"></td>
						<td width="100px">新联系电话:</td>
						<td><input type="text"  style="width:200px" class="required form-control"  id="phoneNumber" name="phoneNumber" value="${buyrecord.contactNumber }">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="100px">备注信息:</td>
						<td colspan="5">
							<textarea class="form-control"  id="comment" name="comment" ></textarea>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
//执行一个laydate实例
laydate.render({
  elem: '#endTime' //指定元素
});
</script>
	