<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" car="document" style="width:50%">
    <form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <input type="hidden" name="method" value="${method }">
        <input type="hidden" id="type" name="type" value="${type}">
        <input type="hidden" id="tombId" name="tombId" value="${tombId}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">缴纳墓穴费</h4>
            </div>
            <div class="modal-body">
                <div>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr role="row">
                            <th colspan="8">墓穴费</th>
                        </tr>
                        <tr role="row">
                            <th>缴费人员</th>
                            <th>发票号</th>
                            <th>缴费日期</th>
                            <th>缴费金额</th>
                            <th>备注</th>
                            <th>操作人</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr role="row">
                            <td>${payRecord.name }</td>
                            <td>${ payRecord.invoiceNumber}</td>
                            <td><fmt:formatDate value="${ payRecord.payTime}" pattern="yyyy-MM-dd"/></td>
                            <td>${ payRecord.totalAmount}</td>
                            <td>${ payRecord.remark}</td>
                            <td>${ payRecord.operator}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 height-align">
                    <label class='lab-4'>缴费人员：</label>
                    <input type="text" class="required list_select" name="name" id="name" value="${payRecord.name }">
                </div>
                <div class="col-md-6 height-align">
                    <label class='lab-4'>发票号 ：</label>
                    <input type="text" class="required list_select" name="invoiceNumber" id="invoiceNumber"
                           value="${ payRecord.invoiceNumber}">
                </div>
                <div class="col-md-6 height-align">
                    <label class='lab-4'>缴费日期：</label>
                    <input type="text" class="required list_select" name="payTime" id="payTime"
                           value="${ payRecord.payTime}">
                </div>
                <div class="col-md-6 height-align">
                    <label class='lab-4'>缴费金额：</label>
                    <input type="text" class="required list_select" name=totalAmount id="totalAmount"
                           value="${ payRecord.totalAmount}">
                </div>
                <div class="col-md-6 height-align">
                    <label class='lab-4'>备注：</label>
                    <input type="text" class="required list_select" name="remark" id="remark"
                           value="${ payRecord.remark}">
                </div>
                <div class="col-md-6 height-align">
                    <label class='lab-4'>操作人：</label>
                    <input type="text" class="required list_select" name="operator" id="operator"
                           value="${ payRecord.operator}">
                </div>
            </div>
            <div class="modal-footer btns-dialog">
                <button type="submit" class="btn btn-info btn-margin">保存</button>
                <button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    //执行一个laydate实例
    laydate.render({
        elem: '#payTime' //指定元素
    });
</script>
	