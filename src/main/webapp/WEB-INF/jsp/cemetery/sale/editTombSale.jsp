<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<style>
</style>
<script type="text/javascript">
var useNum=-1;//使用者标识
var maiNum=-1;//维护费标识

//使用者记录
function item(sum,id,deadName,sex,deadType,borthTime,deadTime,releaseTime,longMenTime){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='hidden' name='information_id' value="+id+">";
	str+="<input type='checkbox' id=checkbox_"+sum+" name='dead' value="+sum+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input type='text' class='list_table' name='deadName' id='deadName_"+sum+"' value="+deadName+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select readOnly='readOnly' class='list_table' name='sex' id='sex_"+sum+"'>"+sex+"</select></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name='deadType' id='deadType_"+sum+"'> "+deadType+"</select></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input  type='text' placeholder='请选择日期' class='list_select' id='borthTime_"+sum+"'   name='borthTime'  value="+borthTime+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input  type='text' placeholder='请选择日期' class='list_select' id='deadTime_"+sum+"' name='deadTime' value="+deadTime+"></td>";

	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input  type='text' placeholder='请选择日期' class='list_select' id='releaseTime_"+sum+"' name='releaseTime' value="+releaseTime+"></td>";

	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input  type='text' placeholder='请选择日期' class='list_select' id='longMenTime_"+sum+"' name='longMenTime' value="+longMenTime+"></td>";

	$("#information").append(str);
}
//维护费记录
function item2(sum,id,payName,invoiceNumber,maintenanceFee,longTime,beginTime,endTime,payTime,payMoney){
	var str="<tr><td style='text-align: center; vertical-align: middle;'>";
	str+="<input type='checkbox' id=checkbox_"+sum+" name='maintainBox' value="+sum+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input style='width:80px' type='text'  name='maipayName' id='maipayName_"+sum+"' value="+payName+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle; '>";
	str+="<input style='width:90px' type='text'  name='invoiceNumber' id='invoiceNumber_"+sum+"' value="+invoiceNumber+"></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+=""+maintenanceFee+"元/年</td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<select  class='list_table' name='longTime' id='longTime_"+sum+"'> "+longTime+"</select></td>";
	
	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input style='width:120px' placeholder='请选择日期' id='beginTime_"+sum+"' name='beginTime' value="+beginTime+"></td>";

	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input style='width:120px' placeholder='请选择日期' id='endTime_"+sum+"' name='endTime' value="+endTime+"></td>";

	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input style='width:120px' placeholder='请选择日期' id='maiPayTime_"+sum+"' name='maiPayTime' value="+payTime+"></td>";

	str+="<td style='text-align: center; vertical-align: middle;'>";
	str+="<input style='width:70px' id='payMoney_"+sum+"' name='payMoney' value="+payMoney+"></td>";

	$("#maintain").append(str);
}
//添加使用者信息
function addUser() {
	useNum++;
	item(useNum,"","","${sexOption}","${deadOption}","","","","");
	//执行一个laydate实例
	laydate.render({
	  elem: '#borthTime_'+useNum //指定元素
	});
	laydate.render({
		elem: '#deadTime_'+useNum 
	});
	laydate.render({
		elem: '#releaseTime_'+useNum 
	});
	laydate.render({
		elem: '#longMenTime_'+useNum 
	});
}
//添加使用者信息
function addMain() {
	//维护费记录
	maiNum++;
	item2(maiNum,"","","","${tomb.maintenance}","${longTimeOption}","","","${nowTime}","");
	laydate.render({
		elem: '#maiPayTime_'+maiNum 
	});
	laydate.render({
		elem: '#beginTime_'+maiNum 
	});
	laydate.render({
		elem: '#endTime_'+maiNum 
	});
}
//删除内容
function delHtml(str) {
	var num = 0;
	$("input[name='"+str+"']:checked").each(function(){
		var id = $(this).attr("value")
		$(this).parent().parent().remove();
		num++;
		var nn=parseFloat($("#"+str+"Number").text());	
		$("#"+str+"Number").html(nn-1);
	});
	if(num<1){
		toastr["warning"]("请选择记录");
	}

}

</script>
	<section class="content-header">
	<i class="title-line"></i>
	<div class="title">墓穴购买表</div>
	</section>
	<section class="content">
		<form action="${url}" id="homeForm" rel="myModal" onsubmit="return validateHomeCallback(this,homeAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" name="tombId" value="${tid }" > 

			<div class="box-body">
				<div class="nav-tabs-custom" style="overflow:hidden">
					<div class="tab-content" style='padding-left:0px;padding-right:0px;'>
						<P class='p border-B'>基本信息</P>
							<div class="box-body broder-B">
								<div class="col-md-5 height-align">
									<label class='lab-4'>持证人姓名：</label>
									<input type="text" style='width:38%'  class="required list_select" name="cardHolder" >&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>身份证号：</label>
									<input type="text"  style="width:330px;" class="required list_select" name="IDNumber" id="IDNumber" >&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>墓穴证号：</label> 
									<input type="text"  class="required list_select" name="tombsCard" id="tombsCard" >&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-7 height-align">
									<label class='lab-4'>家庭住址：</label> 
									<input type="text"  style="width:330px;" class="list_select" name="address" id="address">
								</div>
								<div class="col-md-5 height-align">
									<label class='lab-4'>联系电话：</label> 
									<input type="text" class="required list_select"  name="phoneNumber" id="phoneNumber" >&nbsp;<i class="fa fa-circle text-red"></i>
								</div>
								<div class="col-md-12 ">
									<label class='lab-4'>备注信息：</label> 
									<textarea  name="comment" id="comment" style="width: 75.5%;border-radius: 6px;"></textarea>
								</div>
							</div>
							<p class='p border-B'>使用者信息</p>
							<div class="box-body">
								<div class="col-md-12">
									<small class="btns-buy"  style='padding-left:0px;'>
										<button type="button" onclick="addUser()" class="btn btn-warning " >添加</button>
										<button type="button" onclick="delHtml('dead')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<div class="box-body">
								<table class="table table-bordered" style="margin-top: 60px;" >
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="dead" /></th>
											<th width="100px">姓名</th>
											<th width="75px">性别</th>
									        <th width="75px">寿故</th>
									        <th width="120px">生于</th>
									        <th width="120px">逝于</th>
									        <th width="120px">安放时间</th>
									        <th width="120px">封龙门</th>
										</tr>
									</thead>
									<tbody id="information">
									
									</tbody>
								</table>
								</div>
							</div>
							<p class='p border-B'>墓穴费</p>
							<div class="box-body">
								<div class="box-body">
								<table class="table table-bordered" style="margin-top: 10px;" >
									<thead>
										<tr>
											<th width="100px">缴费人姓名</th>
											<th width="100px">发票号</th>
									        <th width="150px">缴费时间</th>
									        <th width="90px">墓穴定价</th>
									        <th width="90px">销售价格</th>
										</tr>
									</thead>
									<tbody id="tombsFee">
										<tr>
											<td><input type='text' id='payName' name='payName'></td>
											<td><input type='text' id='payNumber' name='payNumber'></td>
											<td><input type='text' placeholder="请选择日期" id='tombPayTime' name='tombPayTime'></td>
											<td>${tomb.price }元</td>
											<td><input type='text' id='payPrice' name='payPrice'></td>
										</tr>
									</tbody>
								</table>
								</div>
							</div>
							<p class='p border-B'>维护费</p>
							<div class="box-body">
								<div class="col-md-12">
									<small class="btns-buy"  style='padding-left:0px;'>
										<button type="button" onclick="addMain()" class="btn btn-warning " >增加</button>
										<button type="button" onclick="delHtml('maintainBox')" class="btn btn-danger " >删除</button>
									</small>
								</div>
								<div class="box-body">
								<table class="table table-bordered" style="margin-top: 60px;" >
									<thead>
										<tr>
											<th width="30px"><input type="checkbox" class="checkBoxCtrl" group="maintainBox" /></th>
											<th width="90px">缴费人姓名</th>
											<th width="100px">发票号</th>
											<th width="70px">维护费</th>
											<th width="60px">年限</th>
											<th width="130px">开始时间</th>
											<th width="130px">结束时间</th>
									        <th width="130px">缴费时间</th>
									        <th width="60px">金额</th>
										</tr>
									</thead>
									<tbody id="maintain">
										
									</tbody>
								</table>
								</div>
							</div>
							<div  class="container-fluid">
							<div class='row padding-B'>
								<div class="col-md-12">
									<small class="pull-right btns-hometab">
										<button type="submit"  class="btn btn-info" style='margin-top:30px;'>保存</button>
										<a href="tombType.do?method=list&tid=${tid}&cemeteryId=${cemeteryId }&areaId=${areaId}" target="homeTab" style='margin-top:30px;' class="btn btn-default" role="button">返回</a>
									</small>
								</div>
							</div>
							</div>
						</div>
				</div>
			</div>
		</form>
	</section>
<script>
laydate.render({
	elem: '#tombPayTime'
});
/* 保存时候的方法 */ 
function validateSub(form, callback) {
	var $form = $(form);
	$(".error-label", form).remove();
	$("[errorFlag]", form).removeAttr("errorFlag");
	$(".error-input", form).removeClass("error-input");
	var errArr=[];
	$(".required", form)
	.each(
			function(i,ele) {
				var value = $(this).val();
				if (value == "") {
					var flag = $(this).attr("errorFlag");
					if (flag == undefined) {
						$(this).attr("errorFlag", "true");
						$(this).addClass("error-input");
						var err = "<label class='control-label error-label' style='color: #dd4b39;'>不能为空</label>";
						$(this).parent().append(err);
						errArr.push(i)
					}
				}
			});
	if(errArr.length!==0){
		var errNum=Math.min.apply(Math,errArr);
		var ele=$(".required", form).eq(errNum);
		var errName=$(ele).attr('name');
		var errMsg=errName;
		if($('#a [name="'+errName+'"]').size()!==0){$('[href="#a"]').click()}
		if($('#b [name="'+errName+'"]').size()!==0){$('[href="#b"]').click()}
		var height=$('.required[name="'+errName+'"]').eq(0).offset().top;
		window.scrollTo(0,height);
		toastr["warning"]("请填写"+errMsg);
		$("button").removeAttr("disabled");
		return false;
	}
	rel = $form.attr("rel");
	//保存时发出请求
	var ajax = function(){
		$.ajax({
    		type : 'POST',
    		url : $form.attr("action"),
    		data : $form.serializeArray(),
    		traditional: true,
    		dataType : "json",
    		cache : false,
    		success : callback,
    		error : function(XMLHttpRequest, textStatus, errorThrown) {
    			toastr["error"](XMLHttpRequest.status);
    			$("button").removeAttr("disabled");
    		}
    	});
	}
}
</script>



