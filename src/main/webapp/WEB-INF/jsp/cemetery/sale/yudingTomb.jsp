<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document" style="width:50%">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${tid}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">预定墓穴</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="80px">客户名称:</td>
						<td><input type="text" style="width:150px" class="required form-control"  id="name" name="name">
						</td>
						<td width="30px"></td>
						<td width="80px">身份证号:</td>
						<td><input type="text" style="width:300px" class="required form-control"  id="IDCard" name="IDCard">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">联系电话:</td>
						<td><input type="text" style="width:150px" class="required form-control"  id="phoneNumber" name="phoneNumber"></td>
						<td width="30px"></td>
						<td width="80px">家庭地址:</td>
						<td><input type="text" style="width:300px" class="form-control"  id="address" name="address"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">定金:</td>
						<td><input type="text"  style="width:150px" class="required form-control"  id="earnestMoney" name="earnestMoney">
						</td>
						<td width="30px"></td>
						<td width="80px">预约到期:</td>
						<td><input type="text"  style="width:150px" class="required form-control"  id="endTime" name="endTime">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">备注信息:</td>
						<td colspan="5">
							<textarea class="form-control"  id="comment" name="comment" ></textarea>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
//执行一个laydate实例
laydate.render({
  elem: '#endTime' //指定元素
});
</script>
	