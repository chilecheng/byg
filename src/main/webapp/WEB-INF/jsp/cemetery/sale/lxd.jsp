<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/11
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" car="document" style="width:50%">
    <form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <input type="hidden" name="method" value="${method }">
        <input type="hidden" id="type" name="type" value="${type}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">墓穴安葬联系单</h4>
            </div>
            <div class="modal-body">
                <table>
                    <tr>
                        <td>墓位:</td>
                        <td colspan="4">${tomb.name}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>逝者:</td>
                        <td colspan="4">
                            <input type="text" class="required form-control" id="name" name="name">
                        </td>
                        <td><a style="text-align: center;border: 1px solid #c0c0c0;">选择</a></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">铭志雕刻:</td>
                        <td>
                            <input type="radio" name="carving" value="有" checked/>有
                            <input type="radio" name="carving" value="无" style="margin-left: 50px;"/>无
                        </td>
                        <td width="30px"></td>
                        <td width="120px">遗像雕刻(安装):</td>
                        <td>
                            <select style="width:150px" class="list_table" name="carvingInstallation"
                                    id="carvingInstallation">
                                <option value="1">无</option>
                                <option value="2" selected="selected">彩色</option>
                                <option value="3">黑白</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">铭志字体:</td>
                        <td>
                            <select style="width:150px" class="list_table" name="repairType" id="repairType">
                                <option value="1">宋体</option>
                                <option value="2">仿宋</option>
                                <option value="3">新宋体</option>
                                <option value="4">楷体</option>
                                <option value="5">隶书</option>
                                <option value="6">黑体</option>
                                <option value="7">其他</option>
                            </select>
                        </td>
                        <td width="30px"></td>
                        <td width="80px">入葬时间:</td>
                        <td>
                            <input type="text" style="width:150px" class="required form-control" id="releaseTime"
                                   name="releaseTime" placeholder="日期" autocomplete="off">
                            <select style="width:150px;margin-top: 2px" class="list_table" name="beginTime"
                                    id="beginTime">
                                <c:forEach var="i" begin="0" end="23">
                                    <option value="${i+1}">${i}</option>
                                </c:forEach>
                            </select>
                            <p style="margin-bottom: 0;">时至</p>
                            <select style="width:150px" class="list_table" name="endTime" id="endTime">
                                <c:forEach var="i" begin="0" end="23">
                                    <option value="${i+1}">${i}</option>
                                </c:forEach>
                            </select>
                            <p style="margin-bottom: 0;">时</p>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>备注:</td>
                        <td colspan="4">
                            <textarea class="form-control" id="remark" name="remark"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer btns-dialog">
                <button type="submit" class="btn btn-info btn-margin">保存</button>
                <button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    //执行一个laydate实例
    laydate.render({
        elem: '#releaseTime' //指定元素
    });
</script>
