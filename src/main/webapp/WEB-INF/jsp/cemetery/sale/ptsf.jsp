<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/5
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" car="document" style="width:50%">
    <form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <input type="hidden" name="method" value="${method }">
        <input type="hidden" id="type" name="type" value="${type}">
        <input type="hidden" id="tombId" name="tombId" value="${tomb.id}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">普通收费</h4>
            </div>
            <div class="modal-body">
                <table>
                    <tr>
                        <td width="80px">创建日期:</td>
                        <td><input type="text" readonly="readonly"
                                   style="outline-style: none; border-style: none; width:150px;" id="createTime"
                                   name="createTime"></td>
                        <td width="30px"></td>
                        <td width="80px">经办人:</td>
                        <td width="150px">${user}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">墓穴:</td>
                        <td width="150px">${tomb.name}</td>
                        <td width="30px"></td>
                        <td width="80px">收费类型:</td>
                        <td>
                            <select class="list_table" name="selectToll" id="selectToll">
                                ${selectTollOption }
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">缴费人:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="payerName"
                                   name="payerName">
                        </td>
                        <td width="30px"></td>
                        <td width="80px">发票号:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="invoiceNumber"
                                   name="invoiceNumber"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">金额:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="total"
                                   name="total">
                        </td>
                        <td width="30px"></td>
                        <td width="80px">付款日期:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="payTime"
                                   name="payTime"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">联系电话:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="phone"
                                   name="phone">
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">备注:</td>
                        <td colspan="5">
                            <textarea class="form-control" id="remark" name="remark"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer btns-dialog">
                <button type="submit" class="btn btn-info btn-margin">保存</button>
                <button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    //执行一个laydate实例
    laydate.render({
        elem: '#payTime' //指定元素
    });
    $(function () {
        var date = new Date();
        var sign1 = "-";
        var sign2 = ":";
        var year = date.getFullYear() // 年
        var month = date.getMonth() + 1; // 月
        var day = date.getDate(); // 日
        var hour = date.getHours(); // 时
        var minutes = date.getMinutes(); // 分
        var seconds = date.getSeconds() //秒
        // 给一位数数据前面加 “0”
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (day >= 0 && day <= 9) {
            day = "0" + day;
        }
        if (hour >= 0 && hour <= 9) {
            hour = "0" + hour;
        }
        if (minutes >= 0 && minutes <= 9) {
            minutes = "0" + minutes;
        }
        if (seconds >= 0 && seconds <= 9) {
            seconds = "0" + seconds;
        }
        var createTime = year + sign1 + month + sign1 + day + " " + hour + sign2 + minutes + sign2 + seconds;
        document.getElementById("createTime").value = createTime;
        var payTime = year + sign1 + month + sign1 + day;
        document.getElementById("payTime").value = payTime;
    });
</script>

