<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/9
  Time: 16:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" car="document" style="width:50%">
    <form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <input type="hidden" name="method" value="${method }">
        <input type="hidden" id="type" name="type" value="${type}">
        <input type="hidden" id="tombId" name="tombId" value="${tomb.id}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">缴纳雕刻安放维护费</h4>
            </div>
            <div class="modal-body">
                <table>
                    <tr>
                        <td width="80px">墓穴:</td>
                        <td width="150px">${tomb.name}</td>
                        <td width="30px"></td>
                        <td width="80px">缴费人姓名:</td>
                        <td><input type="text" style="width:150px" class="required form-control" name="payerName"
                                   id="payerName"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">发票号:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="invoiceNumber"
                                   name="invoiceNumber"></td>
                        <td width="30px"></td>
                        <td width="80px">缴费日期:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="payTime"
                                   name="payTime"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">金额:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="total"
                                   name="total"></td>
                        <td width="30px"></td>
                        <td width="80px">经办人:</td>
                        <td width="150px">${user}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">最后经办人:</td>
                        <td width="150px">${user}</td>
                        <td width="30px"></td>
                        <td width="80px">联系电话:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="phone"
                                   name="phone"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80px">备注:</td>
                        <td colspan="5">
                            <textarea class="form-control" id="remark" name="remark"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer btns-dialog">
                <button type="submit" class="btn btn-info btn-margin">保存</button>
                <button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    //执行一个laydate实例
    laydate.render({
        elem: '#payTime' //指定元素
    });
    $(function () {
        var date = new Date();
        var sign1 = "-";
        var sign2 = ":";
        var year = date.getFullYear() // 年
        var month = date.getMonth() + 1; // 月
        var day = date.getDate(); // 日
        // 给一位数数据前面加 “0”
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (day >= 0 && day <= 9) {
            day = "0" + day;
        }
        var payTime = year + sign1 + month + sign1 + day;
        document.getElementById("payTime").value = payTime;
    });
</script>
