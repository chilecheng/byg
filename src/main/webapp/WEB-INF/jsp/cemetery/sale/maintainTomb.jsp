<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document" style="width:50%">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${tid}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">墓穴维护续费</h4>
			</div>
			<div class="modal-body">
				<div>
					<table class="table table-bordered table-hover">
						<thead>
						<tr role="row">
							<th colspan="8">墓穴维护费信息</th>
						</tr>
						<tr role="row">
							<th>缴费人姓名</th>
							<th>发票号</th>
							<th>年限</th>
							<th>开始时间</th>
							<th>结束时间</th>
							<th>缴费时间</th>
							<th>缴费金额</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${mainList}" var="u">
							<tr role="row">
		    					<td>${u.payName }</td>
		    					<td>${u.invoiceNumber }</td>
		    					<td>${u.longTime }</td>
		    					<td><fmt:formatDate value="${u.beginTime }" pattern="yyyy-MM-dd"/></td>
		    					<td><fmt:formatDate value="${u.endTime }" pattern="yyyy-MM-dd"/></td>
		    					<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd"/></td>
		    					<td>${u.payMoney }</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="col-md-6 height-align">
					<label class='lab-4'>缴费人员：</label>
					<input type="text"  class="required list_select" name="payName" id="payName" >
				</div>
				<div class="col-md-6 height-align">
					<label class='lab-4'>发票号  ：</label>
					<input type="text"  class="required list_select" name="invoiceNumber" id="invoiceNumber" >
				</div>
				<div class="col-md-6 height-align">
					<label class='lab-4'>维护年限：</label>
					<select readOnly='readOnly' style="width:100px"  class='list_table' name='longTime' id='longTime'>
						${longTimeOption }
					</select>
				</div>
				<div class="col-md-6 height-align">
					<label class='lab-4'>开始时间：</label>
					<input type="text"  class="required list_select" name="beginTime" id="beginTime" >
				</div>
				<div class="col-md-6 height-align">
					<label class='lab-4'>缴费金额：</label>
					<input type="text"  class="required list_select" name="payMoney" id="payMoney" >
				</div>
				<div class="col-md-6 height-align">
					<label class='lab-4'>结束时间：</label>
					<input type="text"  class="required list_select" name="endTime" id="endTime" >
				</div>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
//执行一个laydate实例
laydate.render({
  elem: '#endTime' //指定元素
});
//执行一个laydate实例
laydate.render({
  elem: '#beginTime' //指定元素
});
</script>
	