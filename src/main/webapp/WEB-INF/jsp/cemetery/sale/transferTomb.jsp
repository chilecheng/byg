<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document" style="width:50%">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${tid}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">迁出墓穴</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="80px">申请人:</td>
						<td><input type="text" style="width:150px" class="required form-control"  id="applicant" name="applicant">
						</td>
						<td width="30px"></td>
						<td width="80px">联系电话:</td>
						<td><input type="text" style="width:150px" class="required form-control"  id="phoneNumber" name="phoneNumber"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">迁出死者：</td>
						<td>
						<div  style="margin-top:7px;">
                            <select id="sel_search_orderstatus" name="userName"  style="width:150px" multiple="multiple">
                            	${userNameOption }
                            </select>
                        </div>
						</td>
						<td width="30px"></td>
						<td width="80px">是否回收证:</td>
						<td>
							<select  class='list_table' name='isRecovery' id='isRecovery'>
							${isRecoveryOption }
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">迁出时间:</td>
						<td><input type="text"  style="width:150px" class="required form-control"  id="transferTime" name="transferTime">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">备注信息:</td>
						<td colspan="5">
							<textarea class="form-control"  id="comment" name="comment" ></textarea>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
$(function () {
    $('#sel_search_orderstatus').multipleSelect();
})
//执行一个laydate实例
laydate.render({
  elem: '#transferTime' //指定元素
});
</script>
	