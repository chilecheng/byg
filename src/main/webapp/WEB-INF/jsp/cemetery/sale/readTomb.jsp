<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document" style="width:50%">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${tid}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">墓穴购买信息查看预览</h4>
			</div>
			<div class="modal-body">
				<div >
					<div class="col-md-5 height-align">
						<label class='lab-4'>持证人员：</label>
						<span>${buyRecord.cardHolder }</span>
					</div>
					<div class="col-md-7 height-align">
						<label class='lab-4'>墓穴证号：</label>
						<span>${buyRecord.tombsCard }</span>
					</div>
					<div class="col-md-5 height-align">
						<label class='lab-4'>手机号码：</label>
						<span>${buyRecord.phoneNumber }</span>
					</div>
					<div class="col-md-7 height-align">
						<label class='lab-4'>身份证号：</label>
						<span>${buyRecord.IDNumber }</span>
					</div>
<%--					<div class="col-md-5 height-align">--%>
<%--						<label class='lab-4'>联系电话：</label>--%>
<%--						<span>${buyRecord.contactNumber }</span>--%>
<%--					</div>--%>
					<div class="col-md-7 height-align">
						<label class='lab-4'>家庭地址：</label>
						<span>${buyRecord.address }</span>
					</div>
				</div>
				<div >
					<table class="table table-bordered table-hover">
						<thead>
						<tr role="row">
							<th colspan="8">墓穴使用者信息</th>
						</tr>
						<tr role="row">
							<th>死者姓名</th>
							<th>死者性别</th>
							<th>寿故</th>
							<th>生于</th>
							<th>逝于</th>
							<th>安放时间</th>
							<th>封龙门</th>
							<th>状态</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${userList}" var="u">
							<tr role="row">
		    					<td>${u.userName }</td>
		    					<td>${u.sexName }</td>
		    					<td>${u.deadTypeName }</td>
		    					<td><fmt:formatDate value="${u.borthTime }" pattern="yyyy-MM-dd"/></td>
		    					<td><fmt:formatDate value="${u.deadTime }" pattern="yyyy-MM-dd"/></td>
		    					<td><fmt:formatDate value="${u.releaseTime }" pattern="yyyy-MM-dd"/></td>
		    					<td><fmt:formatDate value="${u.longmenTime }" pattern="yyyy-MM-dd"/></td>
		    					<td>
			    					<c:if test="${u.isTransfer==1 }">
			    					<font color="red" class='state1'>已迁出</font>
			    					</c:if>
			    					<c:if test="${u.isTransfer==2 }">
			    					<font color="green" class='state1'>使用中</font>
			    					</c:if>
		    					</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
				<div>
					<table class="table table-bordered table-hover">
						<thead>
						<tr role="row">
							<th colspan="4">墓穴购买信息</th>
						</tr>
						<tr role="row">
							<th>缴费人姓名</th>
							<th>发票号</th>
							<th>缴费时间</th>
							<th>缴费金额</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${payList}" var="u">
							<tr role="row">
		    					<td>${u.name }</td>
		    					<td>${u.invoiceNumber }</td>
		    					<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd"/></td>
		    					<td>${u.totalAmount }</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
				<div>
					<table class="table table-bordered table-hover">
						<thead>
						<tr role="row">
							<th colspan="8">墓穴维护费信息</th>
						</tr>
						<tr role="row">
							<th>缴费人姓名</th>
							<th>发票号</th>
							<th>年限</th>
							<th>开始时间</th>
							<th>结束时间</th>
							<th>缴费时间</th>
							<th>缴费金额</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${mainList}" var="u">
							<tr role="row">
		    					<td>${u.payName }</td>
		    					<td>${u.invoiceNumber }</td>
		    					<td>${u.longTime }</td>
		    					<td><fmt:formatDate value="${u.beginTime }" pattern="yyyy-MM-dd"/></td>
		    					<td><fmt:formatDate value="${u.endTime }" pattern="yyyy-MM-dd"/></td>
		    					<td><fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd"/></td>
		    					<td>${u.payMoney }</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
</script>
	