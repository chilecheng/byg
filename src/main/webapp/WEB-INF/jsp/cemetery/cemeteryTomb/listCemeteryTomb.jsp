<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">陵园墓穴信息</div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：
                        <a href="cemetery.do?method=list&cemeteryId=${cemeteryId}"
                           target="homeTab">${cemetery.name }</a>/
                        <a href="cemeteryArea.do?method=list&cemeteryId=${cemeteryId}&areaId=${areaId}"
                           target="homeTab">${cemeteryArea.name }</a>/
                        <a href="cemeteryRow.do?method=list&cemeteryId=${cemeteryId}&areaId=${areaId}&rowId=${rowId}"
                           target="homeTab">${cemeteryRow.name }</a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp" %>
                    <input name="method" value="${method}" type="hidden"/>
                    <input type="hidden" name="areaId" value="${areaId }"/>
                    <input type="hidden" name="cemeteryId" value="${cemeteryId }"/>
                    <input type="hidden" name="rowId" value="${rowId }"/>
                    <div class="box-body">
                        <div class="col-md-6">
                            <label>墓穴名称：</label>
                            <input type="text" class="list_input" id="name" name="name" value="${name }">
                        </div>
                        <div class="col-md-4">
                            <label>是否启用：</label>
                            <select class="list_select nopadding-R" name="isdel" id="isdel">
                                ${isDelOption}
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info btn-search">搜索</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <div class="box-header with-border">
                    <small class='btns-buy'>
                        <a href="${url}?method=edit&cemeteryId=${cemeteryId}&areaId=${areaId}&rowId=${rowId}"
                           target="dialog" rel="myModal" class="btn btn-warning" role="button">添加</a>
                        <a href="${url}?method=edit&cemeteryId=${cemeteryId}&areaId=${areaId}&rowId=${rowId}&id="
                           target="dialog" checkName="roleId" checkOne="true" rel="myModal" class="btn btn-color-ff7043"
                           role="button">修改</a>
                        <a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="roleId" warm="确认删除吗"
                           class="btn btn-danger">删除</a>
                        <a href="${url}?method=batchAdd&cemeteryId=${cemeteryId}&areaId=${areaId}&rowId=${rowId}"
                           target="dialog" rel="myModal" class="btn btn-warning" role="button">批量添加</a>
                    </small>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId"/></th>
                                        <th>墓穴名称</th>
                                        <th>所属排</th>
                                        <th>单价</th>
                                        <th>顺序号</th>
                                        <th>使用状态</th>
                                        <th>是否启用</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="7">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${page.list }" var="u">
                                                <tr role="row">
                                                    <td><input type="checkbox" name="roleId" value="${u.id }"></td>
                                                    <td>${u.name }</td>
                                                    <td>${cemeteryRow.name }</td>
                                                    <td>${u.price }</td>
                                                    <td>${u.index }</td>
                                                    <td>${u.stateName }</td>
                                                    <td>
                                                        <c:if test="${u.isdel==Isdel_Yes }">
                                                            <font color="red" class='state1'>${u.isdelName }</font>
                                                        </c:if>
                                                        <c:if test="${u.isdel==Isdel_No }">
                                                            <font color="green" class='state1'>${u.isdelName }</font>
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <%@ include file="/common/pageFood.jsp" %>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
