<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp"%>
<div class="modal-dialog" car="document">
	<form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);" >
		<input type="hidden" name="method" value="${method }" >
		<input type="hidden" id="id" name="id" value="${cemetery.id}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<c:choose>
					<c:when test="${cemetery.id != null && cemetery.id !=''}">
						<h4 class="modal-title" id="myModalLabel">修改陵园信息</h4>
					</c:when>
					<c:otherwise>
						<h4 class="modal-title" id="myModalLabel">添加陵园信息</h4>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td width="80px">陵园名称:</td>
						<td><input type="text" style="width:180px;" class="required form-control"  id="lyname" name="lyname" value="${cemetery.name }">
						
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">陵园地址:</td>
						<td colspan="2"><input type="text" class="required form-control"  id="address" name="address" value="${cemetery.address }"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">陵园照片:</td>
						<td colspan="3">
							<div class="col-xs-12" style="margin-left: -28px;">
								<input class="hide" type="file" id="dFile" name="dFile">
								<div class="ID-pic" data-name="dName" style="width:329px; height:249px">
								<span>点击选择图片</span>
									<c:choose>
			    						<c:when test="${cemetery.id != null}">
			    							<img width=325px; height=249px; src="${cemetery.photo }" data-status='1' alt="images">
			    						</c:when>
			    						<c:otherwise>
			    						</c:otherwise>
		    						</c:choose>
								</div>
								<input type="hidden" name="filename" value="${cemetery.photo }">
								<button type="button" style="margin-top:10px;" class="btn btn-default" data-a="upload">确认上传</button>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="80px">是否启用:</td>
						<td>
							<select class="list_select" name="isDel" id="isDel">
								${isDelOption }
							</select>
						</td>
						<td width="80px">顺序号:</td>
						<td><input type="text" style="width:100px;" class="integer form-control" name="index" id="index" value="${cemetery.index }"></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer btns-dialog">
				<button type="submit"  class="btn btn-info btn-margin">保存</button>
				<button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
$('.ID-pic').click(function(){
	$(this).prev().click();
})
$('[type="file"]').change(function(){
	$(this).next().next().next().attr('disabled')&&$('[data-a="upload"]').removeAttr('disabled');
	$this=$(this);
	var file=this.files[0];
	if(file!==undefined){
		var reader= new FileReader();
		reader.readAsDataURL(file);
		$(reader).load(function(){
			$this.next().find('img').remove();
			var img=document.createElement('img');
			img.src=reader.result;
			img.width=325;
			img.height=245;
			$this.next().append(img);
		})
	}
	
}) 
	//图片上传
	$('[data-a="upload"]').click(function(){
		event.preventDefault();
		$this=$(this);
		//获取  input file对象
		var inputFiles = $this.siblings('[type="file"]');
		if( inputFiles.val() !== '' ){
			var id = inputFiles.attr('id');
			var fileObj = document.getElementById(id).files[0]; 
			var form = new FormData();
			form.append("file", fileObj); 
			var xhr = new XMLHttpRequest();
			var index= $("#index").val();
	        xhr.open("post", 'cemetery.do?method=fileupload&index='+index, true);
	        xhr.onload = function () {
				if((xhr.readyState == 4) && (xhr.status == 200)){
					$this.prev().val(xhr.responseText);
					toastr["success"]("上传成功");
					$this.attr('disabled','disabled');
				}else{
					toastr["danger"]("上传失败");
				}
	        };
	        xhr.send(form);
		}else{
			toastr["warning"]("请选择图片");
		} 
	});
</script>
<inuput type="file" value="" />
	