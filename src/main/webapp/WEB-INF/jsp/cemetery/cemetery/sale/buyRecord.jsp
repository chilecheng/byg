<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/17
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">墓穴购买记录</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do" target="homeTab" rel="myModal" class="btn btn-warning" role="button">返回首页</a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：
                        <a href="javascript:void(0);">公墓销售/墓穴购买</a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp" %>
                    <input name="method" value="${method}" type="hidden"/>
                    <div class="box-body">
                        <div class="col-md-2">
                            <label>类型：</label>
                            <select class="list_select nopadding-R" name="searchType" id="searchType"
                                    style="width: 60%;">
                                <option value="1">姓名</option>
                                <option value="2">身份证</option>
                            </select>
                        </div>
                        <div class="col-md-5">
                            <label>查询值：</label>
                            <input type="text" class="list_input" name="searchVal" id="searchVal" value="${searchVal}"
                                   placeholder="查询值">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info btn-search">搜索</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId"/></th>
                                        <th>持证人姓名</th>
                                        <th>墓穴证号</th>
                                        <th>家庭地址</th>
                                        <th>身份证号</th>
                                        <th>联系电话</th>
                                        <th>备注信息</th>
                                        <th>经办人</th>
                                        <th>创建时间</th>
                                        <th>有效期至</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="10">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${page.list }" var="u">
                                                <tr role="row">
                                                    <td><input type="checkbox" name="roleId" value="${u.id }"></td>
                                                    <td>${u.cardHolder }</td>
                                                    <td>${u.tombsCard }</td>
                                                    <td>${u.address }</td>
                                                    <td>${u.IDNumber }</td>
                                                    <td>${u.contactNumber }</td>
                                                    <td>${u.comment }</td>
                                                    <td>${u.createPeople }</td>
                                                    <td>
                                                        <fmt:formatDate value="${u.createTime }"
                                                                        pattern="yyyy-MM-dd HH:mm"/>
                                                    </td>
                                                    <td>
                                                        <fmt:formatDate value="${u.validityPeriod }"
                                                                        pattern="yyyy-MM-dd HH:mm"/>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <%@ include file="/common/pageFood.jsp" %>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script>
    $(function () {
        if (${searchType != ''}) {
            $("#searchType").val("${searchType}");
        }
    });
</script>