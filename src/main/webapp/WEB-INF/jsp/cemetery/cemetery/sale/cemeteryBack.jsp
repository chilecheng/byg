<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/6/27
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">已退墓穴记录</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do" target="homeTab" rel="myModal" class="btn btn-warning" role="button">返回首页</a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：
                    	<a href="cemeteryInterface.do?" target="homeTab">公墓销售</a>/
						<a href="" target="homeTab">已退墓穴</a>
					</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp" %>
                    <input name="method" value="${method}" type="hidden"/>
                    <div class="box-body">
                        <div class="col-md-2">
                            <label>类型：</label>
                            <select class="list_select nopadding-R" name="searchType" id="searchType"
                                    style="width: 60%;">
                                <option value="1">购买者</option>
                                <option value="2">死者</option>
                            </select>
                        </div>
                        <div class="col-md-5">
                            <label>查询值：</label>
                            <input type="text" class="list_input" name="searchVal" id="searchVal" value="${searchVal}"
                                   placeholder="查询值">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info btn-search">搜索</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId"/></th>
                                        <th>购买者姓名</th>
                                        <th>死者姓名</th>
                                        <th>开始使用时间</th>
                                        <th>退回时间</th>
                                        <th>操作人</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="6">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${page.list }" var="u">
                                                <tr role="row">
                                                    <td><input type="checkbox" name="roleId" value="${u.id }"></td>
                                                    <td>${u.buyerName }</td>
                                                    <td>${u.deadName }</td>
                                                    <td>
                                                        <fmt:formatDate value="${u.startTime }"
                                                                        pattern="yyyy-MM-dd HH:mm"/>
                                                    </td>
                                                    <td>
                                                        <fmt:formatDate value="${u.endTime }"
                                                                        pattern="yyyy-MM-dd HH:mm"/>
                                                    </td>
                                                    <td>${u.operator }</td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <%@ include file="/common/pageFood.jsp" %>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script>
    $(function () {
        if (${searchType != ''}) {
            $("#searchType").val("${searchType}");
        }
    });
</script>