<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/24
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">资料查询</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do" target="homeTab" class="btn btn-warning" role="button">返回首页</a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：
                        <a href="cemeteryInterface.do?" target="homeTab">公墓销售</a>/
                        <a href="" target="homeTab">资料查询</a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp" %>
                    <input name="method" value="${method}" type="hidden"/>
                    <div class="box-body">
                        <div class="col-md-6">
                            <label>选择陵园：</label>
                            <select class="list_select nopadding-R" name="selectCemetery"
                                    id="selectCemetery">
                                <c:forEach items="${cemeteryList}" var="c">
                                    <option value="${c.id}">${c.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <label>查询值：</label>
                            <input type="text" class="list_input" name="searchVal" id="searchVal" value="${searchVal}"
                                   placeholder="查询值">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info btn-search">搜索</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6" style="width: 100%">
                            <input name="radioType" id="graveInfo" type="radio" value="1"/>墓穴
                            <input name="radioType" id="cardHolder" type="radio" value="2"/>持证人
                            <input name="radioType" id="user" type="radio" value="3"/>使用者
                            <input name="radioType" id="tombsCard" type="radio" value="4"/>墓穴证号
                            <input name="radioType" id="idNumber" type="radio" value="5"/>身份证
                            <input name="radioType" id="idAddress" type="radio" value="6"/>身份证地址
                            <input name="radioType" id="address" type="radio" value="7"/>家庭地址
                            <input name="radioType" id="contactNumber" type="radio" value="8"/>联系电话
                            <input name="radioType" id="phoneNumber" type="radio" value="9"/>手机
                            <input name="radioType" id="endTime" type="radio" value="10"/>维护费过期
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId"/></th>
                                        <th>墓穴</th>
                                        <th>持证人</th>
                                        <th>使用者</th>
                                        <th>墓穴证号</th>
                                        <th>维护费过期</th>
                                        <th>联系地址</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="7">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${page.list }" var="u">
                                                <tr role="row">
                                                    <td><input type="checkbox" name="roleId" value="${u.id }"></td>
                                                    <td>
                                                        <a href="tombType.do?method=list&tid=${u.tombId }&cemeteryId=${u.tomb.cId}&areaId=${u.tomb.areaId}"
                                                           target='homeTab'>${u.graveInfo }</a></td>
                                                    <td>${u.cardHolder }</td>
                                                    <td>${u.userName }</td>
                                                    <td>${u.tombsCard }</td>
                                                    <td>
                                                        <fmt:formatDate value="${u.maintainRecord.endTime }"
                                                                        pattern="yyyy-MM-dd"/>
                                                    </td>
                                                    <td>${u.address }</td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <%@ include file="/common/pageFood.jsp" %>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        setTimeout(() => {
            if (`${selectCemetery}`) {
                $("#selectCemetery").val("${selectCemetery}");
            } else {
                $("#selectCemetery").val("${cemeteryList[0].id}");
            }
            if (${entries != ''}) {
                $("#searchVal").val("");
            }
            $("input[name='radioType']").each(function(i, el){
                $(el).val(i + 1 + '');
                if ($(el).val() === '${radioType}') {
                    $(el).prop('checked', true)
                }
            });
        }, 0);
    });
</script>