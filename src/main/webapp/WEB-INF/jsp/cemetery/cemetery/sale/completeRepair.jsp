<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/18
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>
<div class="modal-dialog" car="document" style="width:50%">
    <form action="${url}" rel="myModal" onsubmit="return validateCallback(this,dialogAjaxDone);">
        <input type="hidden" name="method" value="${method }">
        <input type="hidden" id="repairRecordId" name="repairRecordId" value="${repairRecord.id}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">墓穴维修登记表</h4>
            </div>
            <div class="modal-body">
                <table>
                    <tr>
                        <td width="120px">报备人:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="name" name="name"
                                   value="${repairRecord.name}">
                        </td>
                        <td width="30px"></td>
                        <td width="120px">报备人电话:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="phoneNumber"
                                   name="phoneNumber" value="${repairRecord.phoneNumber}"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="120px">维修状态:</td>
                        <td><span>已维修</span></td>
                        <td width="30px"></td>
                        <td width="120px">维修开始时间:</td>
                        <td><fmt:formatDate value="${repairRecord.beginTime }" pattern="yyyy-MM-dd HH:mm"/></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="120px">预计结束时间:</td>
                        <td><fmt:formatDate value="${repairRecord.endTime }" pattern="yyyy-MM-dd HH:mm"/></td>
                        <td width="30px"></td>
                        <td width="120px">维修完成时间:</td>
                        <td><input type="text" style="width:150px" class="required form-control" id="completeTime"
                                   name="completeTime"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="120px">维修内容:</td>
                        <td colspan="5">
                            <textarea class="form-control" id="comment"
                                      name="comment">${repairRecord.comment}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <div class="modal-footer btns-dialog">
                    <button type="submit" class="btn btn-info btn-margin">保存</button>
                    <button type="button" class="btn btn-default btn-margin" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    //执行一个laydate实例
    laydate.render({
        elem: '#completeTime' //指定元素
    });
</script>
