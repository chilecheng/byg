<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/6/27
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">未售查询</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do" target="homeTab" rel="myModal" class="btn btn-warning"
           role="button"><span>返回首页</span></a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：
                        <a href="cemeteryInterface.do?" target="homeTab">公墓销售</a>/
                        <a href="" target="homeTab">未售</a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp" %>
                    <input name="method" value="${method}" type="hidden"/>
                    <div class="box-body">
                        <label>选择陵园：</label>
                        <select class="list_select nopadding-R" name="selectCemetery"
                                id="selectCemetery">
                            <c:forEach items="${cemeteryList}" var="c">
                                <option value="${c.id}">${c.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <p style="text-align: center; font-size: 30px;" id="selectCemetery-p"></p>
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th>墓区</th>
                                        <th>已售</th>
                                        <th>未售</th>
                                        <th>已预约</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(cemeteryAreaList)==0 }">
                                            <tr>
                                                <td colspan="4">
                                                    <font color="Red">该陵园还没创建区域信息</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${cemeteryAreaList }" var="u">
                                                <tr role="row">
                                                    <td>
                                                        <a href="tombAreaView.do?method=list&selectCemetery=${cemeteryId}&selectArea=${u.id}"
                                                           target='homeTab'>${u.name }</a></td>
                                                    <td>${u.soldCount }</td>
                                                    <td>${u.notSoldCount }</td>
                                                    <td>${u.bookedCount }</td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        setTimeout(() => {
            if (`${selectCemetery}`) {
                $("#selectCemetery").val("${selectCemetery}");
                let text = $("#selectCemetery").children('option[value="${selectCemetery}"]').text();
                $("#selectCemetery-p").text(text);
            } else {
                $("#selectCemetery").val("${cemeteryList[0].id}");
            }
        }, 0);
        // var obj = document.getElementById('selectCemetery');
        // var index = obj.selectedIndex;
        // var val = obj.options[index].text;
    });
    $('#selectCemetery').change(function () {
        homeSearch(pageForm);
    })
</script>