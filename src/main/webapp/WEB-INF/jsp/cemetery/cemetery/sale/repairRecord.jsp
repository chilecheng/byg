<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/18
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">维修登记</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do" target="homeTab" rel="myModal" class="btn btn-warning" role="button">返回首页</a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：
                        <a href="javascript:void(0);">公墓销售/维修登记</a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp" %>
                    <input name="method" value="${method}" type="hidden"/>
                    <div class="box-body">
                        <div class="col-md-2">
                            <label>类型：</label>
                            <select class="list_select nopadding-R" name="searchType" id="searchType"
                                    style="width: 68%;">
                                <option value="1">墓穴</option>
                                <option value="2">联系人</option>
                                <option value="3">状态</option>
                                <%--                                ${searchOption }--%>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>条件：</label>
                            <select class="list_select nopadding-R" name="searchCondition" id="searchCondition"
                                    style="width: 60%;">
                                <option value="a1">包含</option>
                                <option value="a2">不包含</option>
                                <option value="b1">等于</option>
                                <option value="b2">不等于</option>
                                <option value="c1">为空</option>
                                <option value="c2">不为空</option>
                            </select>
                        </div>
                        <div class="col-md-5">
                            <label id="queryValue">查询值：</label>
                            <input type="text" class="list_input" name="searchVal" id="searchVal" value="${searchVal}"
                                   placeholder="查询值">
                            <select class="list_select nopadding-R" name="repairType" id="repairType"
                                    style="width: 68%;display: none;">
                                <option value="2">未维修</option>
                                <option value="1">已维修</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info btn-search">搜索</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId"/></th>
                                        <th>墓穴</th>
                                        <th>联系人</th>
                                        <th>联系电话</th>
                                        <th>状态</th>
                                        <th>维修时间</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="7">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${page.list }" var="u">
                                                <tr role="row">
                                                    <td><input type="checkbox" name="roleId" value="${u.id }"></td>
                                                    <td>${u.tomb.name }</td>
                                                    <td>${u.name }</td>
                                                    <td>${u.phoneNumber }</td>
                                                    <td><c:choose>
                                                        <c:when test="${u.repairType==GMTYPE_NO }">
                                                            <font color="red"
                                                                  class='state1'>${u.isRepairName }</font>
                                                        </c:when>
                                                        <c:when test="${u.repairType==GMTYPE_YES }">
                                                            <font color="green"
                                                                  class='state1'>${u.isRepairName }</font>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <font color="yellow"
                                                                  class='state1'>${u.isRepairName }</font>
                                                        </c:otherwise>
                                                    </c:choose></td>
                                                    <td>
                                                        <fmt:formatDate value="${u.completeTime }"
                                                                        pattern="yyyy-MM-dd HH:mm"/>
                                                    </td>
                                                    <td>

                                                        <c:choose>
                                                            <c:when test="${u.repairType!=1}">
                                                                <a target="dialog" rel="myModal"
                                                                   class="btn btn-warning" role="button"
                                                                   href="${url}?method=complete&id=${u.id}">完成维修</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a class='light middle' href="javascript:void(0);"
                                                                   style='background:#aaa; color:white; cursor:text'>不需要维修</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <%@ include file="/common/pageFood.jsp" %>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script type="text/javascript">
    var searchCondition = $('#searchCondition');
    var options = $('#searchCondition').prop('options');
    setCollection(searchCondition, 0, 4);
    $(function () {
        $("#repairType").val("");
        if (${searchType != '' && searchCondition != ''}) {
            $("#searchType").val("${searchType}");
            $("#searchCondition").val("${searchCondition}");
        }
        if (${repairType != 0}) {
            $("#searchVal").css("display", "none");
            setCollection(searchCondition, -4, options.length);
            $("#repairType").css("display", "inline");
            $("#repairType").val("${repairType}");
        }
        setTimeout(() => {
            if (${entries != ''}) {
                $("#searchType").val("1");
                $("#searchCondition").val("a1");
                $("#searchVal").val("");
            }
        }, 0);
    });
    //当查询类型是状态时，查询值为下拉选择框，其他为输入框。
    $('#searchType').change(function () {
        if ($(this).val() === '3') {
            $("#searchVal").css("display", "none");
            $("#repairType").css("display", "inline");
            searchCondition.val('b1');
            setCollection(searchCondition, -4, options.length);
            $("#repairType").val("2");
        } else {
            $("#searchVal").css("display", "inline");
            $("#repairType").css("display", "none");
            searchCondition.val('a1');
            setCollection(searchCondition, 0, 4);
            $("#repairType").val("");
        }
    });

    /*
    * @el dom
    * @start number
    * @stop number
    * */
    function setCollection($el, start, stop) {
        $el.find('option').css('display', 'none');
        $el.find('option').slice(start, stop).css('display', 'block');
    }
</script>
