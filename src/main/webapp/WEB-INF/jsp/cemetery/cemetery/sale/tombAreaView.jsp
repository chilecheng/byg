<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/7/3
  Time: 10:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">墓区查看</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do" target="homeTab" rel="myModal" class="btn btn-warning"
           role="button"><span>返回首页</span></a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title" id="box-title"></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp" %>
                    <input name="method" value="${method}" type="hidden"/>
                    <div class="box-body">
                        <label>选择陵园：</label>
                        <select class="list_select nopadding-R" name="selectCemetery"
                                id="selectCemetery">
                            ${selectCemeteryOption }
                        </select>
                        <label>选择区域：</label>
                        <select class="list_select nopadding-R" name="selectArea"
                                id="selectArea">
                            ${selectAreaOption }
                        </select>
                        <c:if test="${selectRowOption!=null}">
                            <label>选择排：</label>
                            <select class="list_select nopadding-R" name="selectRow"
                                    id="selectRow">
                                    ${selectRowOption }
                            </select>
                        </c:if>
                        <c:if test="${cemeteryRowList != null}">
                            <span>当前有墓穴${total}对</span>
                        </c:if>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <ul class="la_ul">
                                <li>
                                    <div>空闲</div>
                                </li>
                                <li style="background:#29b6f6;">
                                    <div>已售</div>
                                </li>
                                <li style="background:#e84e40;">
                                    <div>修缮</div>
                                </li>
                                <li style="background:yellow;">
                                    <div>迁出</div>
                                </li>
                                <li style="background:#fea625;">
                                    <div>过期</div>
                                </li>
                                <li style="background:#673ab7;">
                                    <div>预定</div>
                                </li>
                                <li style="background:#9ccc65;">
                                    <div>使用</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <!-- /.box-header -->
                <div class="box-body cemetery-table-wrapper-right"
                     style="${selectRowOption!=null?'width: 100%;overflow-x: auto':null}">
                    <c:choose>
                        <c:when test="${cemeteryRowList == null}"></c:when>
                        <c:when test="${fn:length(cemeteryRowList)==0 }">
                            <font color="Red">该区域还没创建排信息</font>
                        </c:when>
                        <c:when test="${selectRowOption!=null}">
                            <c:choose>
                                <c:when test="${specialRow=='NineteenToTwenty-nineRows'}">
                                    <div style="overflow: visible;white-space: nowrap;" class="cemetery-table">
                                        <c:forEach items="${tomb }" var="x">
                                            <c:if test="${x.state!=0}">
                                                <a <c:choose>
                                                    <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                    <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                    <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                    <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                    <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                    <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                    <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                </c:choose>
                                                        href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                        target='homeTab'>${x.index }</a>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </c:when>
                                <c:when test="${specialRow=='ThirtyRows'}">
                                    <table style="table-layout:fixed; text-align:center; width: 250px"
                                           class="cemetery-table">
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td style="width:30px;text-align:center;border-right-color: black; border-left-color: black; border-right-width: 1px; border-left-width: 1px; border-right-style: solid; border-left-style: solid;"
                                                rowspan="4">通道
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="cemetery-table-right">
                                                <c:forEach items="${tomb }" var="x">
                                                    <c:if test="${x.index<=6 && x.state!=0}">
                                                        <a <c:choose>
                                                            <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                            <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                            <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                            <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                            <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                            <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                            <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                        </c:choose>
                                                                href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                target='homeTab'>${x.index }</a>
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                            <td class="cemetery-table-left" style="display: block;">
                                                <c:forEach items="${tomb }" var="x">
                                                    <c:if test="${x.index>6 && x.state!=0}">
                                                        <a <c:choose>
                                                            <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                            <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                            <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                            <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                            <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                            <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                            <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                        </c:choose>
                                                                href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                target='homeTab'>${x.index }</a>
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </c:when>
                                <c:when test="${specialRow=='ChasingPavilion'}">
                                    <div class="overview">
                                        <img src="./images/chasingPavilion.png" usemap="#Map2"/>
                                        <map name="Map2" id="Map2">
                                            <area href="tombType.do?method=list&tid=14296&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="16, 25, 84, 86" alt="南边" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14302&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="255, 22, 323, 84" alt="3-3" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14301&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="353, 23, 420, 85" alt="3-2" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14300&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="451, 22, 519, 85" alt="3-1" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14299&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="256, 120, 323, 182" alt="2-2" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14298&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="353, 117, 421, 181" alt="2-1" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14297&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="256, 218, 323, 280" alt="1-1" target="homeTab">
                                        </map>
                                    </div>
                                </c:when>
                                <c:when test="${specialRow=='SouthOneToFiveRows'||specialRow=='SouthSixToNineRows'||specialRow=='SouthElevenToFourteenRows'||specialRow=='SouthFifteenToEighteen'}">
                                    <table style="table-layout:fixed; text-align:center; width: ${specialRow=='SouthFifteenToEighteen'?1160:'100'}${specialRow=='SouthFifteenToEighteen'?'px':'%'}"
                                           class="cemetery-table">
                                        <tbody>
                                        <c:forEach items="${cemeteryRowList1 }" var="u">
                                            <tr>
                                                <td>
                                                    <c:forEach begin="1" end="${fn:length(u.tomb)}"
                                                               varStatus="x">
                                                        <c:if test="${u.tomb[fn:length(u.tomb)-x.index].state!=0}">
                                                            <a <c:choose>
                                                                <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==1}">style="color: blue;"</c:when>
                                                                <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==2}">style="background:#29b6f6;"</c:when>
                                                                <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==3}">style="background:#e84e40;"</c:when>
                                                                <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==4}">style="background:yellow;"</c:when>
                                                                <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==5}">style="background:#fea625;"</c:when>
                                                                <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==6}">style="background:#673ab7;"</c:when>
                                                                <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==7}">style="background:#9ccc65;"</c:when>
                                                            </c:choose>
                                                                    href="tombType.do?method=list&tid=${u.tomb[fn:length(u.tomb)-x.index].id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                    target='homeTab'>${u.tomb[fn:length(u.tomb)-x.index].index }</a>
                                                        </c:if>
                                                    </c:forEach>
                                                </td>
                                                <td style="width:80px; height:50px;border:none;">${u.name }</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </c:when>
                                <c:when test="${specialRow=='SiyuanPavilion'}">
                                    <div class="overview">
                                        <img src="./images/siyuanPavilion.png" usemap="#Map">
                                        <map name="Map" id="Map">
                                            <area href="tombType.do?method=list&tid=14291&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="12,16,89,91" alt="亭边左前" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14292&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="211,15,290,91" alt="亭边右前" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14293&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="106,74,184,225" alt="亭中" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14294&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="6,214,88,288" alt="亭边左后" target="homeTab">
                                            <area href="tombType.do?method=list&tid=14295&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                  shape="rect" coords="209,215,289,289" alt="亭边右后" target="homeTab">
                                        </map>
                                    </div>
                                </c:when>
                                <c:when test="${specialRow=='EastAndWestWallCemetery'}">
                                    <table style="table-layout:fixed; text-align:center; width: 100%"
                                           class="cemetery-table">
                                        <tbody>
                                        <c:forEach items="${tombMap }" var="tm">
                                            <tr>
                                                <td>
                                                    <c:forEach items="${tm.value }" var="x">
                                                        <c:if test="${x.state!=0}">
                                                            <a <c:choose>
                                                                <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                                <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                                <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                                <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                                <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                                <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                                <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                            </c:choose>
                                                                    href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                    target='homeTab'>${x.index }</a>
                                                        </c:if>
                                                    </c:forEach>
                                                </td>
                                                <td style="width:80px; height:50px;border:none;">${fn:substring(tm.value[0].name, 16, fn:indexOf(tm.value[0].name, "排")) }排</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </c:when>
                                <c:when test="${specialRow=='MiddleRoad'}">
                                    <table style="table-layout:fixed; text-align:center; width: 100%"
                                           class="cemetery-table-middle-road">
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td style="width:30px;text-align:center;border-right-color: black; border-left-color: black; border-right-width: 1px; border-left-width: 1px; border-right-style: solid; border-left-style: solid;"
                                                rowspan="9">通道
                                            </td>
                                            <td></td>
                                            <td style="width:50px; height:50px;border:none;"></td>
                                        </tr>
                                        <c:forEach items="${tombMap }" var="tm">
                                            <tr>
                                                <td class="cemetery-table-right">
                                                    <c:forEach items="${tm.value }" var="x">
                                                        <c:if test="${fn:indexOf(x.name, '东')!=-1&&x.state!=0}">
                                                            <a <c:choose>
                                                                <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                                <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                                <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                                <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                                <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                                <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                                <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                            </c:choose>
                                                                    href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                    target='homeTab'>${fn:substringAfter(x.name, "路") }</a>
                                                        </c:if>
                                                    </c:forEach>
                                                </td>
                                                <td class="cemetery-table-left">
                                                    <c:forEach items="${tm.value }" var="x">
                                                        <c:if test="${fn:indexOf(x.name, '西')!=-1&&x.state!=0}">
                                                            <a <c:choose>
                                                                <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                                <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                                <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                                <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                                <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                                <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                                <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                            </c:choose>
                                                                    width="100px"
                                                                    href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                    target='homeTab'>${fn:substringAfter(x.name, "路") }</a>
                                                        </c:if>
                                                    </c:forEach>
                                                </td>
                                                <td style="width:80px; height:50px;border:none;">${fn:substring(tm.value[0].name, 11, fn:indexOf(tm.value[0].name, "排")) }排</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </c:when>
                                <c:when test="${specialRow=='MiddleRow'}">
                                    <table style="table-layout:fixed; text-align:center; width: 2436px"
                                           class="cemetery-table">
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td style="width:30px;text-align:center;border-right-color: black; border-left-color: black; border-right-width: 1px; border-left-width: 1px; border-right-style: solid; border-left-style: solid;"
                                                rowspan="3">通道
                                            </td>
                                            <td></td>
                                            <td style="width:50px; height:50px;border:none;"></td>
                                        </tr>
                                        <c:forEach items="${tombMap }" var="tm">
                                            <tr>
                                                <td class="cemetery-table-right">
                                                    <c:forEach begin="1" end="${fn:length(tm.value)}"
                                                               varStatus="x">
                                                        <c:if test="${tm.value[fn:length(tm.value)-x.index].index%2 == 0&&tm.value[fn:length(tm.value)-x.index].state!=0}">
                                                            <a <c:choose>
                                                                <c:when test="${tm.value[fn:length(tm.value)-x.index].state==1}">style="color: blue;"</c:when>
                                                                <c:when test="${tm.value[fn:length(tm.value)-x.index].state==2}">style="background:#29b6f6;"</c:when>
                                                                <c:when test="${tm.value[fn:length(tm.value)-x.index].state==3}">style="background:#e84e40;"</c:when>
                                                                <c:when test="${tm.value[fn:length(tm.value)-x.index].state==4}">style="background:yellow;"</c:when>
                                                                <c:when test="${tm.value[fn:length(tm.value)-x.index].state==5}">style="background:#fea625;"</c:when>
                                                                <c:when test="${tm.value[fn:length(tm.value)-x.index].state==6}">style="background:#673ab7;"</c:when>
                                                                <c:when test="${tm.value[fn:length(tm.value)-x.index].state==7}">style="background:#9ccc65;"</c:when>
                                                            </c:choose>
                                                                    href="tombType.do?method=list&tid=${tm.value[fn:length(tm.value)-x.index].id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                    target='homeTab'>${tm.value[fn:length(tm.value)-x.index].index }</a>
                                                        </c:if>
                                                    </c:forEach>
                                                </td>
                                                <td class="cemetery-table-left">
                                                    <c:forEach items="${tm.value }" var="x">
                                                        <c:if test="${x.index%2 != 0&&x.state!=0}">
                                                            <a <c:choose>
                                                                <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                                <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                                <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                                <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                                <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                                <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                                <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                            </c:choose>
                                                                    href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                    target='homeTab'>${x.index }</a>
                                                        </c:if>
                                                    </c:forEach>
                                                </td>
                                                <td style="width:80px; height:50px;border:none;">${fn:substring(tm.value[0].name, 11, 14) }</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </c:when>
                                <c:otherwise>
                                    <div style="overflow: visible;white-space: nowrap;" class="cemetery-table">
                                        <c:forEach begin="1" end="${fn:length(tomb)}"
                                                   varStatus="x">
                                            <c:if test="${tomb[fn:length(tomb)-x.index].index%2 == 0 && tomb[fn:length(tomb)-x.index].state!=0}">
                                                <a <c:choose>
                                                    <c:when test="${tomb[fn:length(tomb)-x.index].state==1}">style="color: blue;"</c:when>
                                                    <c:when test="${tomb[fn:length(tomb)-x.index].state==2}">style="background:#29b6f6;"</c:when>
                                                    <c:when test="${tomb[fn:length(tomb)-x.index].state==3}">style="background:#e84e40;"</c:when>
                                                    <c:when test="${tomb[fn:length(tomb)-x.index].state==4}">style="background:yellow;"</c:when>
                                                    <c:when test="${tomb[fn:length(tomb)-x.index].state==5}">style="background:#fea625;"</c:when>
                                                    <c:when test="${tomb[fn:length(tomb)-x.index].state==6}">style="background:#673ab7;"</c:when>
                                                    <c:when test="${tomb[fn:length(tomb)-x.index].state==7}">style="background:#9ccc65;"</c:when>
                                                </c:choose>
                                                        href="tombType.do?method=list&tid=${tomb[fn:length(tomb)-x.index].id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                        target='homeTab'>${tomb[fn:length(tomb)-x.index].index }</a>
                                            </c:if>
                                        </c:forEach>
                                        <span>通道</span>
                                        <c:forEach items="${tomb }" var="x">
                                            <c:if test="${x.index%2 != 0 && x.state!=0}">
                                                <a <c:choose>
                                                    <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                    <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                    <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                    <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                    <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                    <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                    <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                </c:choose>
                                                        href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                        target='homeTab'>${x.index }</a>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <table style="table-layout:fixed; text-align:center; width: ${maxNum>20?maxNum*58:'100'}${maxNum>20?'px':'%'}"
                                   class="cemetery-table">
                                <tbody>
                                <c:choose>
                                    <c:when test="${sort==1 }">
                                        <c:forEach items="${cemeteryRowList }" var="u">
                                            <tr>
                                                <td>
                                                    <c:forEach items="${u.tomb }" var="x">
                                                        <c:if test="${x.state!=0}">
                                                            <a <c:choose>
                                                                <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                                <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                                <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                                <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                                <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                                <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                                <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                            </c:choose>
                                                                    href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                    target='homeTab'>${x.index }</a>
                                                        </c:if>
                                                    </c:forEach>
                                                </td>
                                                <td style="width:80px; height:50px;border:none;">${u.name }</td>
                                            </tr>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${aisle==1}">
                                                <tr>
                                                    <td></td>
                                                    <td style="width:30px;text-align:center;border-right-color: black; border-left-color: black; border-right-width: 1px; border-left-width: 1px; border-right-style: solid; border-left-style: solid;"
                                                        rowspan="14">通道
                                                    </td>
                                                    <td></td>
                                                    <td style="width:50px; height:50px;border:none;"></td>
                                                </tr>
                                                <c:forEach items="${cemeteryRowList2 }" var="u">
                                                    <tr>
                                                        <td class="cemetery-table-right">
                                                            <c:forEach items="${u.tomb }" var="x">
                                                                <c:if test="${x.index%2 == 0 && x.state!=0}">
                                                                    <a <c:choose>
                                                                        <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                                        <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                                        <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                                        <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                                        <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                                        <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                                        <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                                    </c:choose>
                                                                            href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                            target='homeTab'>${x.index }</a>
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                        <td class="cemetery-table-left">
                                                            <c:forEach begin="1" end="${fn:length(u.tomb)}"
                                                                       varStatus="x">
                                                                <c:if test="${u.tomb[fn:length(u.tomb)-x.index].index%2 != 0 && u.tomb[fn:length(u.tomb)-x.index].state!=0}">
                                                                    <a <c:choose>
                                                                        <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==1}">style="color: blue;"</c:when>
                                                                        <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==2}">style="background:#29b6f6;"</c:when>
                                                                        <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==3}">style="background:#e84e40;"</c:when>
                                                                        <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==4}">style="background:yellow;"</c:when>
                                                                        <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==5}">style="background:#fea625;"</c:when>
                                                                        <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==6}">style="background:#673ab7;"</c:when>
                                                                        <c:when test="${u.tomb[fn:length(u.tomb)-x.index].state==7}">style="background:#9ccc65;"</c:when>
                                                                    </c:choose>
                                                                            href="tombType.do?method=list&tid=${u.tomb[fn:length(u.tomb)-x.index].id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                            target='homeTab'>${u.tomb[fn:length(u.tomb)-x.index].index }</a>
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                        <td style="width:50px; height:50px;border:none;">${u.name }</td>
                                                    </tr>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach items="${cemeteryRowList2 }" var="u">
                                                    <tr>
                                                        <td>
                                                            <c:forEach items="${u.tomb }" var="x">
                                                                <c:if test="${x.state!=0}">
                                                                    <a <c:choose>
                                                                        <c:when test="${x.state==1}">style="color: blue;"</c:when>
                                                                        <c:when test="${x.state==2}">style="background:#29b6f6;"</c:when>
                                                                        <c:when test="${x.state==3}">style="background:#e84e40;"</c:when>
                                                                        <c:when test="${x.state==4}">style="background:yellow;"</c:when>
                                                                        <c:when test="${x.state==5}">style="background:#fea625;"</c:when>
                                                                        <c:when test="${x.state==6}">style="background:#673ab7;"</c:when>
                                                                        <c:when test="${x.state==7}">style="background:#9ccc65;"</c:when>
                                                                    </c:choose>
                                                                            href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}"
                                                                            target='homeTab'>${x.index }</a>
                                                                </c:if>
                                                            </c:forEach>
                                                        </td>
                                                        <td style="width:80px; height:50px;border:none;">${u.name }</td>
                                                    </tr>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                                </tbody>
                            </table>
                        </c:otherwise>
                    </c:choose>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        var obj = document.getElementById('selectCemetery');
        var index = obj.selectedIndex;
        var val = obj.options[index].text;
        document.getElementById("box-title").innerHTML = val;
    });
    $('#selectCemetery').change(function () {
        var type = "selectCemeteryChange";
        homeSearch(pageForm, type);
    });
    $('#selectArea').change(function () {
        var type = "selectAreaChange";
        homeSearch(pageForm, type);

    });
    $('#selectRow').change(function () {
        homeSearch(pageForm);
    })
</script>
