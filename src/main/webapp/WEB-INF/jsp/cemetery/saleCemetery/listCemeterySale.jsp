<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">陵园信息</div>
	<div class="pull-right">
		<a href="cemeteryInterface.do?" target="homeTab" rel="myModal"  class="btn btn-warning" role="button">返回首页</a>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">查询</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
				</div>
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-6">
							<label>陵园名称：</label> 
							<input type="text" class="list_input" id="name" name="name" value="${name }">
						</div>
						<div class="col-md-4">
							<label>是否启用：</label> 
							<select class="list_select nopadding-R" name="isdel" id="isdel">
								${isDelOption}
							</select>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-info btn-search">搜索</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-body">
					<c:choose>
					    <c:when test="${fn:length(cemeteryList)==0 }">
							  <font color="Red">还没有创建陵园</font>
					    </c:when>
					    <c:otherwise>
						    <c:forEach items="${cemeteryList }" var="u">
						    	<div class="col-xs-4" >
									<div class="ID-pic"  style="width:329px; height:249px">
				    					<a href="cemeteryArea.do?type=show&method=list&cid=${u.id}" target='homeTab'>
					    					<img width=325px; height=249px; src="${u.photo }"  alt="${u.name }">
				    					</a>
									</div>
									<div style="text-align:center;">
										<input type="hidden" name="filename" value="${u.photo }">
										<a href="cemeteryArea.do?type=show&method=list&cid=${u.id}" target='homeTab'>${u.name }</a>
									</div>
								</div>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</section>
