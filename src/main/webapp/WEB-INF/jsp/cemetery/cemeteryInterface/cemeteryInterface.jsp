<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">陵园信息</div>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <div class="col-xs-5 cemetery-container" style="background-color:white; margin: 15px;padding:15px;">
                    <div class="cemetery-title">
                        <label>公墓销售</label>
                    </div>
                    <div>
                        <c:forEach items="${cemetery}" var="u">
                            <c:choose>
                                <c:when test="${u.name eq '基安山陵园'}">
                                    <p>
                                        <i class="fa fa-globe"></i>
                                        <a href="cemeteryArea.do?type=show&method=list&show=1&cid=${u.id}"
                                           target='homeTab'>${u.name}</a>
                                    </p>
                                </c:when>
                                <c:otherwise>
                                    <p>
                                        <i class="fa fa-globe"></i>
                                        <a href="cemeteryArea.do?type=show&method=list&show=2&cid=${u.id}"
                                           target='homeTab'>${u.name}</a>
                                    </p>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <p>
                            <i class="fa fa-search"></i>
                            <a href="tombSearch.do?method=list&entries=first" target='homeTab'>墓穴查询</a>
                        </p>
                        <p>
                            <i class="fa fa-search"></i>
                            <a href="buyRecord.do?method=data&entries=first" target='homeTab'>资料查询</a>
                        </p>
                        <p>
                            <i class="fa fa-search"></i>
                            <a href="tombAreaView.do?method=list&entries=first" target='homeTab'>墓区查看</a>
                        </p>
                        <p>
                            <i class="fa fa-search"></i>
                            <a href="notSaleTomb.do?method=list" target='homeTab'>未售查询</a>
                        </p>
                        <p>
                            <i class="fa fa-search"></i>
                            <a href="userRecord.do?method=list&type=1&entries=first" target='homeTab'>安放查询</a>
                        </p>
                        <p>
                            <i class="fa fa-search"></i>
                            <a href="userRecord.do?method=list&type=2&entries=first" target='homeTab'>使用者查询</a>
                        </p>
                        <p>
                            <i class="fa fa-file-archive-o"></i>
                            <a href="appointmentRecord.do?method=list&entries=first" target='homeTab'>墓穴预约记录</a>
                        </p>
                        <p>
                            <i class="fa fa-file-archive-o"></i>
                            <a href="buyRecord.do?method=list" target='homeTab'>墓穴购买记录</a>
                        </p>
                        <p>
                            <i class="fa fa-file-archive-o"></i>
                            <a href="transferRecord.do?method=list" target='homeTab'>已迁出的购买记录</a>
                        </p>
                        <p>
                            <i class="fa fa-file-archive-o"></i>
                            <a href="cemeteryBack.do?method=list" target='homeTab'>已退墓穴记录</a>
                        </p>
                        <p>
                            <i class="fa fa-file-archive-o"></i>
                            <a href="reductionApproval.do?method=list" target='homeTab'>减免审批</a>
                        </p>
                        <p>
                            <i class="fa fa-file-archive-o"></i>
                            <a href="gmRepairRecord.do?method=list&entries=first" target='homeTab'>维修登记</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-5 cemetery-container" style="background-color:white; margin: 15px;padding:15px;">
                    <div class="cemetery-title">
                        <label>公墓管理</label>
                    </div>
                    <div>
                        <p>
                            <i class="fa fa-tv"></i>
                            <a href="cemetery.do" target="homeTab">陵园公墓管理</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-5 cemetery-container" style="background-color:white; margin: 15px;padding:15px;">
                    <div class="cemetery-title">
                        <label>缴费记录</label>
                    </div>
                    <div>
                        <p>
                            <i class="fa fa-cny"></i>
                            <a href="paymentedRecord.do?method=cemeteryBuyedRecord" target="homeTab">墓穴购买</a>
                        </p>
                        <p>
                            <i class="fa fa-cny"></i>
                            <a href="paymentedRecord.do?method=maintenancePaymentRecord" target="homeTab">墓穴维护费</a>
                        </p>
                        <p>
                            <i class="fa fa-cny"></i>
                            <a href="paymentedRecord.do?method=payRecord&type=1" target="homeTab">墓穴雕刻安放费</a>
                        </p>
                        <p>
                            <i class="fa fa-cny"></i>
                            <a href="paymentedRecord.do?method=payRecord&type=5" target="homeTab">雕刻费</a>
                        </p>
                        <p>
                            <i class="fa fa-cny"></i>
                            <a href="paymentedRecord.do?method=payRecord&type=4" target="homeTab">迁移费</a>
                        </p>
                    </div>
                </div>

                <div class="col-xs-5 cemetery-container" style="background-color:white; margin: 15px;padding:15px;">
                    <div class="cemetery-title">
                        <label>报表统计</label>
                    </div>
                    <div>
                        <p>
                            <i class="fa fa-area-chart"></i>
                            <a href="reportStatistics.do?method=paymentStatistics" target="homeTab">墓穴费统计</a>
                        </p>
                        <p>
                            <i class="fa fa-bar-chart"></i>
                            <a href="reportStatistics.do?method=maintenanceStatistics" target="homeTab">维护费统计</a>
                        </p>
                        <p>
                            <i class="fa fa-line-chart"></i>
                            <a href="reportStatistics.do?method=carvingFeeStatistics" target="homeTab">雕刻安放费统计</a>
                        </p>
                        <p>
                            <i class="fa fa-pie-chart"></i>
                            <a href="reportStatistics.do?method=placementStatistics" target="homeTab">安放量统计</a>
                        </p>
                        <p>
                            <i class="fa fa-area-chart"></i>
                            <a href="reportStatistics.do?method=salesStatistics" target="homeTab">墓穴销售统计</a>
                        </p>
                        <p>
                            <i class="fa fa-file-text-o"></i>
                            <a href="reportStatistics.do?method=summaryOfCemeteries" target="homeTab">墓区汇总表</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('.cemetery-container').hover(function () {
        $(this).animate({
            top: '-5px'
        }, 200)
    }, function () {
        $(this).animate({
            top: '0'
        }, 200)
    })
</script>
