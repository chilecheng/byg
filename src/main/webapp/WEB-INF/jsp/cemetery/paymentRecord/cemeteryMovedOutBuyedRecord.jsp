<%--
  Created by IntelliJ IDEA.
  User: ZhixiangWang
  Date: 2019/6/25
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">墓穴已迁出购买记录</div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <!-- /.box-header -->
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <%@ include file="/common/pageHead.jsp"%>
                    <input name="method" value="${method}" type="hidden" />
                    <input type="hidden" name="areaId" value="${areaId }" />
                    <input type="hidden" name="cemeteryId" value="${cemeteryId }" />
                    <div class="box-body">
                        <div class="col-md-6">
                            <label>墓地查询：</label>
                            <input type="text" class="list_input" id="name" name="name" value="${name }">
                        </div>
                        <div class="col-md-4">
                            <label>是否启用：</label>
                            <select class="list_select nopadding-R" name="isdel" id="isdel">
                                ${isDelOption}
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info btn-search">搜索</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <div class="box-header with-border">
                    <small class='btns-buy'>
                        <a href="${url}?method=edit&areaId=${areaId}" target="dialog" rel="myModal"  class="btn btn-warning" role="button">添加</a>
                        <a href="${url}?method=edit&areaId=${areaId}&id=" target="dialog" checkName="roleId" checkOne="true" rel="myModal" class="btn btn-color-ff7043" role="button">修改</a>
                        <a href="${url}?method=delete&ids=" target="ajaxTodo" checkName="roleId"  warm="确认删除吗" class="btn btn-danger" >删除</a>
                        <a href="${url}?method=batchAdd&areaId=${areaId}" target="dialog" rel="myModal"  class="btn btn-warning" role="button">批量添加</a>
                    </small>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId" /></th>
                                        <th>墓地名称</th>
                                        <th>所属区域</th>
                                        <th>所属陵园</th>
                                        <th>购买时间</th>
                                        <th>是否启用</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="6">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${page.list }" var="u">
                                                <tr role="row">
                                                    <td><input type="checkbox" name="roleId" value="${u.id }"></td>
                                                    <td><a href="cemeteryTomb.do?method=list&rid=${u.id}&cemeteryId=${cemeteryId}&areaId=${areaId}" target='homeTab'>${u.name }</td>
                                                    <td>${cemeteryArea.name}</td>
<%--                                                    <td>${u.comment }</td>--%>
<%--                                                    <td>${u.index }</td>--%>
<%--                                                    <td>--%>
<%--                                                        <c:if test="${u.isdel==Isdel_Yes }">--%>
<%--                                                            <font color="red" class='state1'>${u.isdelName }</font>--%>
<%--                                                        </c:if>--%>
<%--                                                        <c:if test="${u.isdel==Isdel_No }">--%>
<%--                                                            <font color="green" class='state1'>${u.isdelName }</font>--%>
<%--                                                        </c:if>--%>
<%--                                                    </td>--%>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <%@ include file="/common/pageFood.jsp"%>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

