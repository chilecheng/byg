<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">
		<c:if test="${type==1 }">墓穴雕刻安放费用记录</c:if>
		<c:if test="${type==4 }">墓穴迁移费用记录</c:if>
		<c:if test="${type==5 }">墓穴雕刻费用记录</c:if>
	</div>
	<div class="pull-right">
		<a href="cemeteryInterface.do?" target="homeTab" rel="myModal"  class="btn btn-warning" role="button">返回首页</a>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">当前所在位置:
						<a href="" target="homeTab">
							<c:if test="${type==1 }">缴费记录/墓穴雕刻安放</c:if>
							<c:if test="${type==4 }">缴费记录/墓穴迁移</c:if>
							<c:if test="${type==5 }">缴费记录/墓穴雕刻</c:if>
						</a> 
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
<%-- 					<input type="hidden" name="cemeteryId" value="${cemnetery.id }" /> --%>
						<input type="hidden" name="method" value="payRecord">
						<input type="hidden" name="type" value="${type }">
						<div class="box-body">
							<div class="col-md-4">
							<label>类型：</label>
									<select class="list_select nopadding-R" name="searchType" id="searchType" style="width: 52%;">
										${searchOption }
									</select>
							</div>
							<div class="col-md-6" >
									<label>查询值：</label> 
									<input type="text" class="list_input" name="searchVal" id="searchVal" value="${searchVal}" placeholder="查询值">
							</div>
						
							<div class="col-md-12">
									<button type="submit" class="btn btn-info btn-search">搜索</button>
							</div>
					</div>
				</form>
			</div>
			<!-- /.col -->
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th width="10"><input type="checkbox" class="checkBoxCtrl" group="roleId" /></th>
											<th>支付人姓名</th>
											<th>支付类别</th>
											<th>总金额</th>
											<th>联系电话</th>
											<th>发票号</th>
											<th>操作人</th>
											<th>费用名称</th>
											<th>支付时间</th>
											<th>订单创建时间</th>
										</tr>
									</thead>
									<tbody>
									 <c:choose>
									    <c:when test="${fn:length(page.list)==0 }">
										    <tr>
								    			<td colspan=10">
											  		<font color="Red">还没有数据</font>
										 		</td>
										    </tr>
									    </c:when>
									    <c:otherwise>
										    <c:forEach items="${page.list }" var="u">
											<tr role="row">
												<td><input type="checkbox" name="roleId" value="${u.id }"></td>
						    					<td>${u.payerName }</td>
						    					<td>${u.type }</td>
						    					<td>${u.total }</td>
						    					<td>${u.phone }</td>
						    					<td>${u.invoiceNumber }</td>
						    					<td>${u.operator }</td>
						    					<td>${u.costName }</td>
						    					<td>
						    						<fmt:formatDate value="${u.payTime }" pattern="yyyy-MM-dd HH:mm"/>
						    					</td>
						    					<td>
						    						<fmt:formatDate value="${u.createTime }" pattern="yyyy-MM-dd HH:mm"/>
						    					</td>
						    					
											</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
