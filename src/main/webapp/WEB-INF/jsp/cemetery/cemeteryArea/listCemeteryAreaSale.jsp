<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/common.jsp" %>

<section class="content-header">
    <i class="title-line"></i>
    <div class="title">陵园区域信息</div>
    <div class="pull-right">
        <a href="cemeteryInterface.do?" target="homeTab" rel="myModal" class="btn btn-warning" role="button">返回首页</a>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">当前所在位置：公墓销售/
                        <a href="cemeterySale.do?type=show&method=list&cemeteryId=${cemeteryId}"
                           target="homeTab">${cemetery.name }</a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse">
                            <i class="fa fa-chevron-down"></i>
                        </button>
                    </div>
                </div>
                <form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
                    <input name="method" value="${method}" type="hidden"/>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <div class="box-body">
                    <c:if test="${show==1 }">
                        <div class="overview">
                            <div class="preview">
                                <img src="images/jianshan.png"></img>
                            </div>
                            <div class="details">
                                <img src="images/jasly3.png" usemap="#map"></img>
                                <map id=map name=map>
                                    <area title=y区 shape=rect alt=y区 coords="933, 357, 996, 482" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0034">
                                    <area title=g区 shape=rect alt=g区 coords="607, 370, 743, 492" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0010">
                                    <area title=g区树葬区 shape=rect alt=g区树葬区 coords="613, 505, 687, 529" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0020">
                                    <area title=下a区 shape=rect alt=下a区 coords="393, 97, 474, 160" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0007">
                                    <area title=骨灰西廊(南单) shape=rect alt=骨灰西廊(南单) coords="502, 90, 527, 122"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0029">
                                    <area title=骨灰西廊(南双) shape=rect alt=骨灰西廊(南双) coords="531, 89, 602, 122"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0028">
                                    <area title=骨灰西廊(东单) shape=rect alt=骨灰西廊(东单) coords="502, 124, 535, 152"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0027">
                                    <area title=骨灰西廊(西单) shape=rect alt=骨灰西廊(西单) coords="565, 124, 601, 152"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0030">
                                    <area title=骨灰东廊(东单) shape=rect alt=骨灰东廊(东单) coords="619, 123, 655, 154"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0023">
                                    <area title=骨灰东廊(南双) shape=rect alt=骨灰东廊(南双) coords="618, 88, 749, 121"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0024">
                                    <area title=骨灰东廊(西单) shape=rect alt=骨灰东廊(西单) coords="709, 123, 750, 155"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0025">
                                    <area title=c区 shape=rect alt=c区 coords="545, 156, 677, 219" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0003">
                                    <area title=b区 shape=rect alt=b区 coords="397, 202, 522, 262" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0002">
                                    <area title=d区 shape=rect alt=d区 coords="508, 273, 601, 338" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0004">
                                    <area title=e区 shape=rect alt=e区 coords="615, 284, 718, 346" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0008">
                                    <area title=f区 shape=rect alt=f区 coords="385, 329, 507, 385" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0009">
                                    <area title=草坪生态区 shape=rect alt=草坪生态区 coords="424, 412, 527, 493" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0021">
                                    <area title=z5区 shape=rect alt=z5区 coords="22, 223, 182, 325" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0018">
                                    <area title=z6区 shape=rect alt=z6区 coords="206, 206, 266, 331" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0019">
                                    <area title=照壁(单右) shape=rect alt=照壁(单右) coords="129, 126, 163, 159"
                                          target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0031">
                                    <area title=上a区 shape=rect alt=上a区 coords="42, 398, 147, 503" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0005">
                                    <area title=z3区 shape=rect alt=z3区 coords="275, 214, 340, 334" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0017">
                                    <area title=z2区 shape=rect alt=z2区 coords="796, 210, 902, 293" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0016">
                                    <area title=z1区 shape=rect alt=z1区 coords="773, 335, 899, 468" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0015">
                                    <area title=上a东区 shape=rect alt=上a东区 coords="198, 414, 314, 465" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0006">
                                    <area title=x区 shape=rect alt=x区 coords="1019, 367, 1124, 473" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0014">
                                    <area title=w区 shape=rect alt=w区 coords="1151, 366, 1246, 473" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0013">
                                    <area title=t区 shape=rect alt=t区 coords="1269, 364, 1362, 492" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0012">
                                    <area title=h区 shape=rect alt=h区 coords="689, 190, 753, 268" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0011">
                                    <area title=照壁(双) shape=rect alt=照壁(双) coords="84, 125, 126, 158" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0033">
                                    <area title=照壁(单左) shape=rect alt=照壁(单左) coords="52, 125, 81, 158" target="homeTab"
                                          href="tombAreaView.do?method=list&selectCemetery=CID001&selectArea=CAID0032">
                                </map>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${show!=1 }">
                        <c:choose>
                            <c:when test="${fn:length(cemeteryAreaList)==0 }">
                                <!-- <font color="Red">该陵园还没创建区域信息</font> -->
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${cemeteryAreaList }" var="u">
                                    <div class="col-xs-4">
                                        <div class="ID-pic" style="width:329px; height:249px">
                                            <a href="tombAreaView.do?method=list&selectCemetery=${cemeteryId}&selectArea=${u.id}"
                                               target='homeTab'>
                                                <img width=325px; height=249px; src="${u.photo }" alt="${u.name }">
                                            </a>
                                        </div>
                                        <div style="text-align:center;">
                                            <input type="hidden" name="filename" value="${u.photo }">
                                            <a href="tombAreaView.do?method=list&selectCemetery=${cemeteryId}&selectArea=${u.id}"
                                               target='homeTab'>${u.name }</a>
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </c:if>

                    <script>
                        $('.overview .preview img').one('click', function (e) {
                            var ev = e || event
                            $('.overview .preview').css({
                                display: 'none'
                            })
                            $('.overview .details').css({
                                display: 'block'
                            })
                        })
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>
