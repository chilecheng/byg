<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">区域排信息</div>
	<div class="pull-right">
		<a href="cemeteryInterface.do?" target="homeTab" rel="myModal"  class="btn btn-warning" role="button">返回首页</a>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">当前所在位置：
						<a href="cemeterySale.do?method=list&cemeteryId=${cemeteryId}" target="homeTab">${cemetery.name }</a>/
						<a href="cemeteryArea.do?type=show&method=list&cemeteryId=${cemeteryId}&areaId=${areaId}" target="homeTab">${cemeteryArea.name }</a>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
				</div>
				<form action="${url }" id="pageForm" onsubmit="return homeSearch(this);">
					<input name="method" value="${method}" type="hidden" />
					<div class="box-body">
						<div class="col-md-12">
							<ul class="la_ul">
								<li><div>空闲</div></li>
								<li style="background:#29b6f6;"><div>已售</div></li>
								<li style="background:#e84e40;"><div>修缮</div></li>
								<li style="background:yellow;"><div>迁出</div></li>
								<li style="background:#fea625;"><div>过期</div></li>
								<li style="background:#673ab7;"><div>预定</div></li>
								<li style="background:#9ccc65;"><div>使用</div></li>
							</ul>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				<div class="box-body">
					<c:choose>
					    <c:when test="${fn:length(cemeteryRowList)==0 }">
							  <font color="Red">该区域还没创建排信息</font>
					    </c:when>
					    <c:otherwise>
						    <table  style="table-layout:fixed;text-align:center;">
					    		<tbody>
					    			<c:choose>
					    			<c:when test="${sort==1 }">
									    <c:forEach items="${cemeteryRowList }" var="u">
								    	<tr>
								    		<td style="width:80px; height:50px;border:none;" >${u.name }</td>
									    	<c:forEach items="${u.tomb }" var="x">
									    		<c:choose>
									    		<c:when test="${x.state==1}">   
										    		<td style="width:50px; border:1px solid #c0c0c0" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==2}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#29b6f6;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==3}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#e84e40;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==4}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background: yellow;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==5}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#fea625;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==6}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#673ab7;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==7}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#9ccc65;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
									    		</c:choose>
									    	</c:forEach>
								  		</tr>
										</c:forEach>
					    			</c:when>
					    			<c:otherwise>
					    				<c:forEach items="${cemeteryRowList2 }" var="u">
								    	<tr>
								    		
									    	<c:forEach items="${u.tomb }" var="x">
									    		<c:choose>
									    		<c:when test="${x.state==1}">   
										    		<td style="width:50px; border:1px solid #c0c0c0" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==2}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#29b6f6;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==3}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#e84e40;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==4}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background: yellow;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==5}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#fea625;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==6}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#673ab7;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==7}">   
										    		<td style="width:50px; border:1px solid #c0c0c0;background:#9ccc65;" title="${x.index }">
														<a href="tombType.do?method=list&tid=${x.id }&cemeteryId=${cemeteryId}&areaId=${areaId}" style="color: white;" target='homeTab'>${x.index }</a>
										    		</td>
				   								</c:when>
				   								<c:when test="${x.state==0}">
				   									<td style="width:50px; background:#ffffff;" title="${x.index }">
										    		</td>
				   								</c:when>
									    		</c:choose>
									    	</c:forEach>
									    	<td style="width:80px; height:50px;border:none;" >${u.name }</td>
								  		</tr>
										</c:forEach>
					    			</c:otherwise>
					    			</c:choose>
					    		</tbody>
						    </table>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</section>
