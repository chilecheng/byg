<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">墓穴销售统计</div>
	<div class="pull-right">
		<a href="cemeteryInterface.do?" target="homeTab" rel="myModal"  class="btn btn-warning" role="button">返回首页</a>
	</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">当前所在位置:
						<a href="cemeteryInterface.do?" target="homeTab">统计报表</a>/
						<a href="" target="homeTab">墓穴销售
						</a> 
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<form action="reportStatistics.do"  id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<div class="box-body">
					<input type="hidden" name="method" value="salesStatistics"/>
						起始时间:<input type="text" data-id='beginDate' autocomplete="off" class="list_select" id="startTime" name="startTime" value="${startTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span>
						结束时间:<input type="text" data-id='endDate' autocomplete="off" class="list_select" id="endTime" name="endTime" value="${endTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						<label>选择陵园：</label>
                        	<select class="list_select nopadding-R" name="selectCemetery"
                                id="selectCemetery">
                            	${selectCemeteryOption }
                        	</select>
                        <label>选择区域：</label>
                        	<select class="list_select nopadding-R" name="selectArea"
                                id="selectArea">
                            	${selectAreaOption }
                        	</select>
					<button type="submit" class="btn btn-info btn-search">搜索</button>  
                  </div>
				</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
        <div class="col-xs-12">
            <div class="box" style="border-top: 0">
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example2_wrapper"
                         class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr role="row">
                                        <th width="10"><input type="checkbox" class="checkBoxCtrl" group="Id"/></th>
                                        <th>区域</th>
                                        <th>墓穴费</th>
                                        <th>维护费</th>
                                        <th>雕刻安放龙门费</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="5">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                         <c:otherwise>
                                            <c:forEach items="${page.list }" var="u">
                                                <tr role="row">
                                                <td><input type="checkbox" name="roleId" value="${u.id }"></td>
                                                <td>${u.graveInfo}</td>
						    					<td>${u.totalAmount }</td>
						    					<td>${u.payMoney }</td>
						    					<td>${u.crave }+${u.dragon }</td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                                <table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th>销售墓穴数量</th>
											<th>墓穴费总计</th>
											<th>维护费总计</th>
											<th>雕刻安放费总计</th>
											<th>迁移费总计</th>
											<th>封龙门次数</th>
											<th>骨灰安放次数</th>
										</tr>
									</thead>
									<tbody>
									 
                                        
											<tr role="row">
						    					<td>${paymentedTimeresult}</td>
						    					<td>${paymentedCemeteryresult}</td>
						    					<td>${maintenanceCemeteryresult}</td>
						    					<td>${payCemeteryresult}</td>
						    					<td>${moveCemeteryresult}</td>
						    					<td>${closeCemeteryresult}</td>
						    					<td>${placementCemeteryresult}</td>
											</tr>
									
									
									</tbody>
								</table>
                            </div>
                        </div>
                        <%@ include file="/common/pageFood.jsp" %>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        var obj = document.getElementById('selectCemetery');
        var index = obj.selectedIndex;
        var val = obj.options[index].innerText;
        console.log(val)
    });
    $('#selectCemetery').change(function () {
        var type = "1";
        homeSearch(pageForm, type);
    })
</script>