<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/common.jsp" %> 

<section class="content-header">
	<i class="title-line"></i>
	<div class="title">墓区汇总表</div>
	<div class="pull-right">
		<a href="cemeteryInterface.do?" target="homeTab" rel="myModal"  class="btn btn-warning" role="button">返回首页</a>
	</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">当前所在位置:
						<a href="cemeteryInterface.do?" target="homeTab">统计报表</a>/
						<a href="" target="homeTab">墓区汇总
						</a> 
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-chevron-down"></i>
						</button>
					</div>
					<form action="reportStatistics.do"  id="pageForm" onsubmit="return homeSearch(this);">
					<%@ include file="/common/pageHead.jsp"%>
					<div class="box-body">
					<input type="hidden" name="method" value="summaryOfCemeteries"/>
						起始时间:<input type="text" data-id='beginDate' autocomplete="off" class="list_select" id="startTime" name="startTime" value="${startTime }"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
								<span class='timerange'>--</span>
						结束时间:<input type="text" data-id='endDate' autocomplete="off" class="list_select" id="endTime" name="endTime" value="${endTime}"><i style="margin-left: -20px;" class="fa fa-calendar"></i>
						<label>选择陵园：</label>
                        	<select class="list_select nopadding-R" name="selectCemetery"
                                id="selectCemetery">
                            	${selectCemeteryOption }
                        	</select>
                        <label>选择区域：</label>
                        	<select class="list_select nopadding-R" name="selectArea"
                                id="selectArea">
                            	${selectAreaOption }
                        	</select>
					<button type="submit" class="btn btn-info btn-search">搜索</button>  
                  </div>
				</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="border-top: 0">
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div id="example2_wrapper"
						class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
                                <table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
										<th width="10"><input type="checkbox" class="checkBoxCtrl" group="Id"/></th>
											<th>墓区</th>
											<th>当前销售量</th>
											<th>当前销售额</th>
											<th>当前维护费收入</th>
											
										</tr>
									</thead>
									<tbody>
										<c:choose>
                                        <c:when test="${fn:length(page.list)==0 }">
                                            <tr>
                                                <td colspan="5">
                                                    <font color="Red">还没有数据</font>
                                                </td>
                                            </tr>
                                        </c:when>
                                         <c:otherwise>
                                         <c:forEach items="${page.list }" var="u">
		                                              <tr role="row">
		                                              	<td><input type="checkbox" name="roleId" value="${u.id }"></td>
		                                                <td>${cemeteryArea.name }</td>
		                                                <td>${u.numbers }</td>
								    					<td>${u.totalAmount }</td>
								    					<td>${u.payMoney }</td>
	                                                </tr>
	                                      </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
									</tbody>
														
								</table>
								<table class="table table-bordered table-hover">
									<thead>
										<tr role="row">
											<th>已建成墓穴数</th>
											<th>已售出量</th>
											<th>未售量</th>
											<th>墓价</th>
											<th>销售金额</th>
											<th>安放量</th>
										</tr>
									</thead>
									<tbody>                                       
											<tr role="row">
						    							<td>${tombresult}</td>
								    					<td>${paymentedNumber}</td>
								    					<td>${unsaleNumber}</td>
								    					<td>${paymentedMinMoney}-${paymentedMaxMoney}</td>
								    					<td>${paymentedMoney}</td>
								    					<td>${placement}</td>
											</tr>
									
									
									</tbody>
								</table>
							</div>
						</div>
						<%@ include file="/common/pageFood.jsp"%>
					</div>
				</div>
</section>
<script type="text/javascript">
    $(function () {
        var obj = document.getElementById('selectCemetery');
        var index = obj.selectedIndex;
        var val = obj.options[index].innerText;
        console.log(val)
        // document.getElementById("box-title").innerHTML = val;
        // $("#selectCemetery option[value='3b23e2d1c3ea40498cb3ff3a3c519648']").attr("selected", "selected")
    });
    $('#selectCemetery').change(function () {
        var type = "1";
        homeSearch(pageForm, type);
    })
</script>